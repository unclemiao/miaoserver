package excel

import (
	"archive/zip"
	"fmt"
	"io/ioutil"
	"log"
	"math"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"

	"github.com/tealeg/xlsx"

	"goproject/engine/assert"
)

type XlsConfigReaderState struct {
	Sheet   string
	Row     int
	Col     int
	Field   string
	Content string
	Element interface{}
}

func (this *XlsConfigReaderState) Error(str string) string {
	return fmt.Sprintf("%+v(%d, %d) => %+v: %s", this.Sheet, this.Row, this.Col, this.Field, str)
}

var intTypes = map[reflect.Kind]bool{
	reflect.Uint:   true,
	reflect.Uint8:  true,
	reflect.Uint16: true,
	reflect.Uint32: true,
	reflect.Uint64: true,
	reflect.Int:    true,
	reflect.Int8:   true,
	reflect.Int16:  true,
	reflect.Int32:  true,
	reflect.Int64:  true,
}

var xlsxMap map[string]*xlsx.File
var fileSheetMap map[string]*xlsx.Sheet

func XlsConfigInit(path string) {
	xlsxMap = make(map[string]*xlsx.File)
	fileSheetMap = make(map[string]*xlsx.Sheet)

	files, err := ioutil.ReadDir(path)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		fname := file.Name()
		if filepath.Ext(fname) == ".xlsx" && !strings.HasPrefix(fname, "~") {

			ffname := filepath.Join(path, fname)
			xlFile, err := xlsx.OpenFile(ffname)
			assert.Assert(err == nil, err)
			xlsxMap[fname] = xlFile
			for _, s := range xlFile.Sheet {
				if strings.HasPrefix(s.Name, "Sheet") {
					continue
				}
				if fileSheetMap[s.Name] != nil {
					assert.Assert(false, "配置名重复:%s, %s", fname, fileSheetMap[s.Name].Name)
				}
				fileSheetMap[s.Name] = s
			}
		}
	}
}

func XlsConfigInitZip(path string) error {
	xlsxMap = make(map[string]*xlsx.File)
	fileSheetMap = make(map[string]*xlsx.Sheet)

	fileName := filepath.Join(path, "excel.zip")
	z, err := zip.OpenReader(fileName)
	if err != nil {
		return err
	}

	for _, file := range z.File {
		fname := file.Name
		if filepath.Ext(fname) == ".xlsx" && !strings.HasPrefix(fname, "~") {
			reader, err := file.Open()
			if err != nil {
				return err
			}
			bs, err := ioutil.ReadAll(reader)
			if err != nil {
				return err
			}
			reader.Close()
			f, err := xlsx.OpenBinary(bs)
			if err != nil {
				return err
			}
			xlsxMap[fname] = f
			for _, s := range f.Sheet {
				fileSheetMap[s.Name] = s
			}
		}
	}
	return nil
}

func XlsConfigDestroy() {
	xlsxMap = make(map[string]*xlsx.File)
	fileSheetMap = make(map[string]*xlsx.Sheet)
}

func XlsConfigLoad(sheetName string, dest interface{}, cb func(state *XlsConfigReaderState)) error {
	destV := reflect.ValueOf(dest)
	if destV.Kind() != reflect.Ptr {
		return fmt.Errorf("load sheet [%s] error: dest must be pointer type", sheetName)
	}

	destV = destV.Elem()
	if destV.Kind() != reflect.Slice ||
		destV.Type().Elem().Kind() != reflect.Ptr ||
		destV.Type().Elem().Elem().Kind() != reflect.Struct {

		return fmt.Errorf("load sheet [%s] error: dest must be pointer to slice of pointer to struct", sheetName)
	}

	elmT := destV.Type().Elem().Elem()
	dataV := reflect.MakeSlice(reflect.SliceOf(reflect.PtrTo(elmT)), 0, 0)

	sheet, _ := fileSheetMap[sheetName]
	if sheet == nil {
		return fmt.Errorf("load [%s] error: sheet not exist", sheetName)
	}

	if sheet.MaxRow < 3 {
		return fmt.Errorf("load [%s] error: only has [%d] rows", sheetName, sheet.MaxRow)
	}

	availNames := make(map[string]string)
	tagNames := make(map[string]string)
	for i := 0; i < elmT.NumField(); i++ {
		f := elmT.Field(i)
		name := strings.ToLower(f.Name)
		availNames[name] = f.Name
		tagName := f.Tag.Get("json")
		tagNames[tagName] = f.Name

	}

	var names []string
	head := sheet.Rows[1]
	for i := 0; i < len(head.Cells); i++ {
		cell := head.Cells[i]
		if cell.Type() != xlsx.CellTypeString {
			break
		}

		splits := strings.Split(strings.TrimSpace(cell.String()), "#")
		if len(splits) == 0 {
			return fmt.Errorf("load [%s] error: row [%d] col [%d] cannot be empty", sheetName, 2, i+1)
		}

		name := strings.TrimSpace(splits[0])
		if len(name) == 0 {
			break
		}
		if fname, ok := availNames[name]; ok {
			name = fname
		}
		if fname, ok := tagNames[name]; ok {
			name = fname
		}
		names = append(names, name)
	}

	for i := 3; i < sheet.MaxRow; i++ {
		row := sheet.Rows[i]
		if len(row.Cells) == 0 || len(row.Cells[0].String()) == 0 {
			continue
		}

		pelmV := reflect.New(elmT)
		elmV := pelmV.Elem()
		for j := 0; j < len(names); j++ {
			if j >= len(row.Cells) {
				break
			}

			cell := row.Cells[j]
			name := names[j]
			state := &XlsConfigReaderState{
				Sheet:   sheetName,
				Row:     i + 1,
				Col:     j + 1,
				Field:   name,
				Content: cell.String(),
				Element: pelmV.Interface(),
			}
			if _, ok := elmT.FieldByName(name); !ok {
				if cb != nil {
					cb(state)
				}
				continue
			}

			field := elmV.FieldByName(name)
			kind := field.Kind()

			switch kind {
			case reflect.Bool:
				if cell.Value == "" {
					field.SetBool(false)
				} else {
					v, err := strconv.Atoi(cell.String())
					if err != nil {
						return fmt.Errorf(state.Error(err.Error()))
					}
					field.SetBool(v != 0)
				}
			case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
				if cell.Value == "" {
					field.SetUint(0)
				} else {

					v, err := strconv.ParseUint(cell.String(), 10, 64)
					if err != nil {
						return fmt.Errorf("%s, max:%d", state.Error(err.Error()), uint64(math.MaxUint64))
					}

					field.SetUint(uint64(v))
				}
			case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
				if cell.Value == "" {
					field.SetInt(0)
				} else {
					v, err := strconv.ParseInt(cell.String(), 10, 64)
					if err != nil {
						return fmt.Errorf("%s, max:%d", state.Error(err.Error()), int64(math.MaxInt64))
					}

					field.SetInt(v)
				}

			case reflect.Float32, reflect.Float64:
				v, err := cell.Float()
				if err != nil {
					return fmt.Errorf(state.Error(err.Error()))
				}
				field.SetFloat(v)
			case reflect.String:
				field.SetString(cell.String())
			case reflect.Map, reflect.Slice, reflect.Struct, reflect.Ptr:
				if cb == nil {
					return fmt.Errorf(state.Error(fmt.Sprintf("field is complex type [%s], but callback function is nil", field.Type())))
				}
				if kind != reflect.Ptr || field.Type().Elem().Kind() == reflect.Struct {
					cb(state)
					break
				}
				fallthrough
			default:
				return fmt.Errorf(state.Error(fmt.Sprintf("field has unsupported type [%s]", field.Type())))
			}
		}
		dataV = reflect.Append(dataV, pelmV)
	}
	destV.Set(dataV)
	return nil
}
