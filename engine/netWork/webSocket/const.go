package webSocket

import (
	"time"

	"github.com/gorilla/websocket"
)

const DefaultWriteTimeOut = time.Second * 30
const DefaultReadTimeOut = time.Second * 30

var DefaultOnConnectFunc OnConnectFunc = func(conn *websocket.Conn) {}
var DefaultOnDataFunc OnDataFunc = func(conn *websocket.Conn, raw []byte) {}
var DefaultOnCloseFunc OnCloseFunc = func(conn *websocket.Conn) {}
