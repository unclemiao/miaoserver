package webSocket

import (
	"net/http"
	"time"

	"github.com/gorilla/websocket"
)

type NetServer struct {
	Ip           string
	Port         int32
	Route        string
	WriteTimeout time.Duration
	ReadTimeout  time.Duration
	IdleTimeOut  time.Duration
	OnData       OnDataFunc
	OnConnect    OnConnectFunc
	OnClose      OnCloseFunc
	Log          ILog
	stopChan     chan chan struct{}
	mux          *http.ServeMux
	Pprof        bool
}

type OnConnectFunc func(conn *websocket.Conn)
type OnDataFunc func(conn *websocket.Conn, raw []byte)
type OnCloseFunc func(conn *websocket.Conn)
type CheckAuthCallback func(r *http.Request) error

type ILog interface {
	Errorf(f string, v ...interface{})
	Warnf(f string, v ...interface{})
	Infof(f string, v ...interface{})
	Debugf(f string, v ...interface{})
}
