package webSocket

import (
	"context"
	"fmt"
	"net/http"
	"net/http/pprof"
	"time"

	"github.com/gorilla/websocket"

	"goproject/engine/logs"
)

func New(route string, Ip string, Port int32) *NetServer {
	server := &NetServer{
		Ip:           Ip,
		Port:         Port,
		WriteTimeout: DefaultWriteTimeOut,
		ReadTimeout:  DefaultReadTimeOut,
		Log:          &NetLog{},
		Route:        route,
		mux:          http.NewServeMux(),
		OnConnect:    DefaultOnConnectFunc,
		OnData:       DefaultOnDataFunc,
		OnClose:      DefaultOnCloseFunc,
		Pprof:        true,
	}
	return server
}

// block
func (this *NetServer) Run(route string) error {
	upgrader := websocket.Upgrader{
		HandshakeTimeout:  3 * time.Second,
		ReadBufferSize:    4096,
		WriteBufferSize:   4096,
		EnableCompression: true,
		CheckOrigin:       func(r *http.Request) bool { return true },
	}
	if this.Pprof {
		this.initPprof()
	}
	logs.Infof("Run %s", route+this.Route)
	this.mux.HandleFunc(route+this.Route, func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if r.Body != nil {
				r.Body.Close()
			}
			err := recover()
			if err != nil {
				this.Log.Errorf("handle err:%v", err)
			}

		}()
		wsconn, err := upgrader.Upgrade(w, r, nil)

		if err != nil {
			this.Log.Errorf("Upgrade error（%+v） %v", r.Header, err)
			return
		}

		this.OnConnect(wsconn)

		this.Read(wsconn)
		this.OnClose(wsconn)
		err = wsconn.Close()
		if err != nil {
			this.Log.Errorf("conn.Close error %v", err)
		}
	})

	httpServer := &http.Server{
		Addr:         fmt.Sprintf("%s:%d", this.Ip, this.Port),
		Handler:      this.mux,
		ReadTimeout:  this.ReadTimeout,
		WriteTimeout: this.WriteTimeout,
		IdleTimeout:  this.IdleTimeOut,
	}
	errChan := make(chan error)
	go func(errChan chan error) {
		errChan <- httpServer.ListenAndServe()
	}(errChan)

	this.Log.Infof("websocket server started at %s:%d", this.Ip, this.Port)
	select {
	case stopFinished := <-this.stopChan:
		httpServer.Shutdown(context.Background())
		stopFinished <- struct{}{}
		return nil
	case err := <-errChan:
		this.Log.Errorf("ListenAndServeErr: %s", err)
		return err
	}
}

func (this *NetServer) Read(conn *websocket.Conn) {
	for {
		_, raw, err := conn.ReadMessage()

		if err != nil {
			this.Log.Errorf("conn read err: %p, %s ", conn, err.Error())
			return
		}
		err = conn.SetReadDeadline(time.Now().Add(this.ReadTimeout))
		if err != nil {
			this.Log.Errorf("conn SetReadDeadline err: %p, %s ", conn, err.Error())
			return
		}
		this.OnData(conn, raw)
	}

}

func (this *NetServer) HandleFunc(pattern string, handle func(w http.ResponseWriter, r *http.Request)) error {

	this.mux.HandleFunc(pattern, handle)
	return nil
}

func (this *NetServer) Stop() {
	stopDone := make(chan struct{}, 1)
	this.stopChan <- stopDone
	<-stopDone
}

func (this *NetServer) SetLog(Log ILog) {
	this.Log = Log
}

func (this *NetServer) SetWriteTimeOut(Due time.Duration) {
	this.WriteTimeout = Due
}

func (this *NetServer) SetReadTimeOut(Due time.Duration) {
	this.ReadTimeout = Due
}

func (this *NetServer) SetIdleTimeOut(Due time.Duration) {
	this.IdleTimeOut = Due
}

func (this *NetServer) SetOnConnectFunc(OnConnect OnConnectFunc) {
	this.OnConnect = OnConnect
}

func (this *NetServer) SetOnDataFunc(OnData OnDataFunc) {
	this.OnData = OnData
}

func (this *NetServer) SetOnCloseFunc(OnClose OnCloseFunc) {
	this.OnClose = OnClose
}

func (this *NetServer) initPprof() {
	if this.mux != nil {
		this.mux.HandleFunc("/debug/pprof/", pprof.Index)
		this.mux.HandleFunc("/debug/pprof/cmdline", pprof.Cmdline)
		this.mux.HandleFunc("/debug/pprof/profile", pprof.Profile)
		this.mux.HandleFunc("/debug/pprof/symbol", pprof.Symbol)
		this.mux.HandleFunc("/debug/pprof/trace", pprof.Trace)
	}
}
