package webSocket

import (
	"fmt"
	"log"
	"runtime"
)

type NetLog struct {
}

func (this *NetLog) Errorf(f string, v ...interface{}) {
	log.Println(fmt.Sprintf(f, v...))
}
func (this *NetLog) TraceError(f string, v ...interface{}) {
	this.Errorf(f, v...)
	this.Errorf(string(this.Stack()))

}
func (this *NetLog) Warnf(f string, v ...interface{}) {
	log.Println(fmt.Sprintf(f, v...))
}
func (this *NetLog) Infof(f string, v ...interface{}) {
	log.Println(fmt.Sprintf(f, v...))
}
func (this *NetLog) Debugf(f string, v ...interface{}) {
	log.Println(fmt.Sprintf(f, v...))
}
func (this *NetLog) Stack() []byte {
	buf := make([]byte, 4096)
	buf = buf[:runtime.Stack(buf, false)]
	return buf
}
