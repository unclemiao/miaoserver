package netWork

import (
	"goproject/engine/netWork/webSocket"
)

func NewWebSocketServer(head string, ip string, port int32) *webSocket.NetServer {
	return webSocket.New(head, ip, port)
}
