package snowflake

import (
	"testing"
	"time"

	"github.com/stretchr/testify/require"
)

func BenchmarkSnowflake_NextID(b *testing.B) {
	settings := Settings{
		Epoch: time.Now().AddDate(0, 0, -1),
		MachineId: func() (uint16, error) {
			return uint16(1), nil
		},
	}
	snowflake, err := NewSnowflake(settings)
	require.NoError(b, err)
	b.ReportAllocs()
	b.ResetTimer()
	for n := 0; n < b.N; n++ {
		snowflake.NextId()
	}
}
