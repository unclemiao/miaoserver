package snowflake

import (
	"errors"
	"sync"
	"time"
)

const (
	BitLenTime      = 39                               // bit length of time
	BitLenSequence  = 8                                // bit length of sequence number
	BitLenMachineId = 63 - BitLenTime - BitLenSequence // bit length of machine id
)

type Settings struct {
	Epoch          time.Time
	MachineId      func() (uint16, error)
	CheckMachineId func(uint16) bool
}

type Snowflake struct {
	mutex       *sync.Mutex
	epoch       int64
	elapsedTime int64
	sequence    uint16
	machineId   uint16
}

func NewSnowflake(settings Settings) (*Snowflake, error) {
	sf := new(Snowflake)
	sf.mutex = new(sync.Mutex)
	sf.sequence = uint16(1<<BitLenSequence - 1)

	if settings.Epoch.After(time.Now()) {
		return nil, errors.New("start time should not after current time")
	}
	if settings.Epoch.IsZero() {
		sf.epoch = toSnowflakeTime(
			time.Date(2018, 8, 24, 0, 0, 0, 1, time.UTC), // 贝尔纪元
		)
	} else {
		sf.epoch = toSnowflakeTime(settings.Epoch)
	}

	var err error
	if settings.MachineId == nil {
		return nil, errors.New("no machine id provided")
	} else {
		sf.machineId, err = settings.MachineId()
	}
	if err != nil || (settings.CheckMachineId != nil && !settings.CheckMachineId(sf.machineId)) {
		return nil, errors.New("machine id invalid")
	}

	return sf, nil
}

func (sf *Snowflake) NextId() uint64 {
	// Note: fix clock moving backwards.
	const maskSequence = uint16(1<<BitLenSequence - 1)
	sf.mutex.Lock()
	defer sf.mutex.Unlock()

	current := currentElapsedTime(sf.epoch)
	if sf.elapsedTime < current {
		sf.elapsedTime = current
		sf.sequence = 0
	} else { // sf.elapsedTime >= current
		sf.sequence = (sf.sequence + 1) & maskSequence
		if sf.sequence == 0 {
			sf.elapsedTime++
			overtime := sf.elapsedTime - current
			time.Sleep(sleepTime(overtime))
		}
	}

	return sf.toId()
}

const snowflakeTimeUnit = 1e7 // nsec, i.e. 10 msec

func toSnowflakeTime(t time.Time) int64 {
	return t.UTC().UnixNano() / snowflakeTimeUnit
}

func currentElapsedTime(startTime int64) int64 {
	return toSnowflakeTime(time.Now()) - startTime
}

func sleepTime(overtime int64) time.Duration {
	return time.Duration(overtime)*10*time.Millisecond -
		time.Duration(time.Now().UTC().UnixNano()%snowflakeTimeUnit)*time.Nanosecond
}

func (sf *Snowflake) toId() uint64 {
	if sf.elapsedTime >= 1<<BitLenTime {
		panic(errors.New("over the time limit"))
	}
	return uint64(sf.elapsedTime)<<(BitLenSequence+BitLenMachineId) |
		uint64(sf.sequence)<<BitLenMachineId |
		uint64(sf.machineId)
}

func Decompose(id uint64) map[string]uint64 {
	const maskSequence = uint64((1<<BitLenSequence - 1) << BitLenMachineId)
	const maskMachineId = uint64(1<<BitLenMachineId - 1)

	msb := id >> 63
	u := id >> (BitLenSequence + BitLenMachineId)
	sequence := id & maskSequence >> BitLenMachineId
	machineId := id & maskMachineId
	return map[string]uint64{
		"id":         id,
		"msb":        msb,
		"u":          u,
		"sequence":   sequence,
		"machine-id": machineId,
	}
}
