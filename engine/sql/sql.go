package sql

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"

	"goproject/engine/assert"
)

type Dsn struct {
	Driver       string `db:"db_driver"`
	Host         string `db:"db_host"`
	Port         int32  `db:"db_port"`
	Username     string `db:"db_user"`
	Password     string `db:"db_password"`
	DatabaseName string `db:"db_database"`
}

type CleanCallback func(int64, ...interface{})

type Sql struct {
	Dsn           *Dsn
	Db            *sqlx.DB
	Tx            *sqlx.Tx
	IsBegin       bool
	MarkCleanIds  map[int64]struct{} // key=role_id
	CleanCallback CleanCallback      // sql rollback 事件回调
}

func LoadSql(dsn *Dsn) (*Sql, error) {
	db := new(Sql)
	db.MarkCleanIds = make(map[int64]struct{})
	var err error
	switch dsn.Driver {
	case "mysql":
		err = db.initMysql(dsn)
	case "postgres":
		err = db.initPostgres(dsn)
	}
	return db, err
}

func (sql *Sql) initMysql(dsn *Dsn) error {
	var dsnStr string
	if dsn.DatabaseName != "" {
		dsnStr = fmt.Sprintf("%s:%s@tcp(%s)/%s?multiStatements=true&charset=utf8mb4&parseTime=true&loc=Local",
			dsn.Username,
			dsn.Password,
			dsn.Host,
			dsn.DatabaseName,
		)
	} else {
		dsnStr = fmt.Sprintf("%s:%s@tcp(%s)/?multiStatements=true&charset=utf8mb4&parseTime=true&loc=Local",
			dsn.Username,
			dsn.Password,
			dsn.Host,
		)
	}

	Db, err := sqlx.Open("mysql", dsnStr)
	if err != nil {
		return err
	}
	sql.Db = Db
	sql.Dsn = dsn
	return nil
}

func (sql *Sql) initPostgres(dsn *Dsn) error {
	dsnStr := fmt.Sprintf("%s:%s@tcp(%s)/%s?multiStatements=true&charset=utf8mb4&parseTime=true&loc=Local",
		dsn.Username,
		dsn.Password,
		dsn.Host,
		dsn.DatabaseName,
	)
	Db, err := sqlx.Connect("mysql", dsnStr)
	if err != nil {
		return err
	}
	sql.Db = Db
	sql.Dsn = dsn
	return nil
}

func (sql *Sql) Ping() error {
	return sql.Db.Ping()
}

func (sql *Sql) Close() {
	if sql.IsBegin {
		err := sql.Tx.Rollback()
		assert.Assert(err == nil, err)
		sql.IsBegin = false
	}
	err := sql.Db.Close()
	assert.Assert(err == nil, err)
}

func (sql *Sql) SetRollBackEventFunc(callBack CleanCallback) {
	sql.CleanCallback = callBack
}

func (sql *Sql) Begin() *Sql {
	if !sql.IsBegin {
		tx, err := sql.Db.Beginx()
		assert.Assert(err == nil, err)
		sql.Tx = tx
		sql.IsBegin = true
		sql.MustExcel(fmt.Sprintf("USE %s", sql.Dsn.DatabaseName))
	}
	return sql
}

func (sql *Sql) Commit() {
	if sql.IsBegin {
		err := sql.Tx.Commit()
		assert.Assert(err == nil, err)
		sql.Tx = nil
		sql.IsBegin = false
	}
	sql.MarkCleanIds = make(map[int64]struct{})
}

func (sql *Sql) Clean(id int64) *Sql {
	sql.MarkCleanIds[id] = struct{}{}
	return sql
}

func (sql *Sql) Rollback(args ...interface{}) {
	for id := range sql.MarkCleanIds {
		if sql.CleanCallback != nil {
			sql.CleanCallback(id, args...)
		}
	}
	sql.MarkCleanIds = make(map[int64]struct{})

	if sql.IsBegin {
		err := sql.Tx.Rollback()
		assert.Assert(err == nil, err)
		sql.Tx = nil
		sql.IsBegin = false
	}
}

func (sql *Sql) Excel(query string, args ...interface{}) (sql.Result, error) {
	if sql.IsBegin {
		return sql.Tx.Exec(query, args...)
	} else {
		return sql.Db.Exec(query, args...)
	}
}

func (sql *Sql) MustExcel(query string, args ...interface{}) sql.Result {
	if sql.IsBegin {
		return sql.Tx.MustExec(query, args...)
	} else {
		return sql.Db.MustExec(query, args...)
	}
}

func (sql *Sql) Do(query string, args ...interface{}) sql.Result {
	return sql.Db.MustExec(query, args...)
}

func (sql *Sql) Select(data interface{}, query string, args ...interface{}) error {
	if sql.IsBegin {
		return sql.Tx.Select(data, query, args...)
	} else {
		return sql.Db.Select(data, query, args...)
	}
}

func (sql *Sql) MustSelect(data interface{}, query string, args ...interface{}) {
	err := sql.Select(data, query, args...)
	assert.Assert(err == nil, err)
}
