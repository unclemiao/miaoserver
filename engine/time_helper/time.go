package time_helper

import (
	"regexp"
	"strconv"
	"strings"
	"time"

	"goproject/engine/assert"
)

const (
	Second int64 = 1
	Minute       = 60 * Second
	Hour         = 60 * Minute
	Day          = 24 * Hour
	Week         = 7 * Day
)

var (
	Regs = make([]*regexp.Regexp, 20)
	Zone int64
)

func init() {
	Regs[0] = regexp.MustCompile(`^(\d\d\d\d)\-(\d?\d)\-(\d?\d) (\d?\d):(\d?\d)$`)
	Regs[1] = regexp.MustCompile(`^(\d\d\d\d)\-(\d?\d)\-(\d?\d)$`)
	Regs[2] = regexp.MustCompile(`^(\d?\d):(\d?\d)`)
	Regs[3] = regexp.MustCompile(`[?\-\w]+`)
	Regs[4] = regexp.MustCompile(`-?\d+`)

	_, z := time.Now().Zone()
	Zone = int64(z)
}

// Beginning of Day
func Bod(t time.Time) time.Time {
	year, month, day := t.Date()
	return time.Date(year, month, day, 0, 0, 0, 0, t.Location())

}

// End of Day
func Eod(t time.Time) time.Time {
	y, m, d := t.Date()
	return time.Date(y, m, d, 23, 59, 59, int(time.Second-time.Nanosecond), t.Location())
}

func UnixMicro(t time.Time) int64 {
	return t.UnixNano() / 1e3
}

func UnixMill(t time.Time) int64 {
	return t.UnixNano() / 1e6
}

func UnixSec(t time.Time) int64 {
	return t.Unix()
}

func UnixMin(t time.Time) int64 {
	return t.Unix() / Minute
}

func UnixHour(t time.Time) int64 {
	return t.Unix() / Hour
}

func UnixDay(t time.Time) int64 {
	return t.Unix() / Day
}

// 获取当前时间 精确到微秒
func NowMicro() int64 {
	return time.Now().UnixNano() / 1e3
}

// 获取当前时间 精确到毫秒
func NowMill() int64 {
	return time.Now().UnixNano() / 1e6
}

func NowSec() int64 {
	return UnixSec(time.Now())
}

func NowMin() int64 {
	return UnixMin(time.Now())
}

func NowDay(nowSec int64) int64 {
	return (nowSec + Zone) / Day
}

func DayWeek(nowDay int64) int64 {
	w := (nowDay - 3) % 7
	if w == 0 {
		w = 7
	}
	return w
}

func Date(format string, date string, err bool) (int64, string) {

	var y, m, d, h, min int

	if strings.Compare(format, "") == 0 {
		ss := Regs[0].FindStringSubmatch(date)
		if err {
			assert.Assert(len(ss) > 1, "invalid datetime")
		} else if len(ss) <= 1 {
			return 0, "invalid datetime"
		}
		y, _ = strconv.Atoi(ss[1])
		m, _ = strconv.Atoi(ss[2])
		d, _ = strconv.Atoi(ss[3])
		h, _ = strconv.Atoi(ss[4])
		min, _ = strconv.Atoi(ss[5])
	} else if strings.Compare(format, "date") == 0 {
		ss := Regs[1].FindStringSubmatch(date)
		if err {
			assert.Assert(len(ss) > 1, "invalid datetime")
		} else if len(ss) <= 1 {
			return 0, "invalid datetime"
		}
		y, _ = strconv.Atoi(ss[1])
		m, _ = strconv.Atoi(ss[2])
		d, _ = strconv.Atoi(ss[3])
	} else if strings.Compare(format, "time") == 0 {
		ss := Regs[2].FindStringSubmatch(date)
		if err {
			assert.Assert(len(ss) > 1, "invalid datetime")
		} else if len(ss) <= 1 {
			return 0, "invalid datetime"
		}
		y, m, d = 1970, 1, 1
		h, _ = strconv.Atoi(ss[1])
		min, _ = strconv.Atoi(ss[2])
	} else {
		if err {
			assert.Assert(false, "invalid datetime")
		} else {
			return 0, "invalid datetime"
		}
	}
	n := UnixSec(time.Date(y, time.Month(m), d, h, min, 0, 0, time.Local))
	return n, ""
}

func DateNoErr(format string, date string) (int64, string) {
	return Date(format, date, false)
}

func DateErr(format string, date string) int64 {
	t, _ := Date(format, date, true)
	return t
}
