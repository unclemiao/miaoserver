package sdk

import (
	"fmt"
	"io"
	"io/ioutil"

	"github.com/huaweicloud/huaweicloud-sdk-go-obs/obs"
)

var (
	huawei_keyId  = "TIDRIORTMYDE9BPKOBEU"
	huawei_secret = "HxBFtQlO4q08UTPaxF92h1FxTzbJh44Vt0T7NY2c"
	huawei_bucket = "konglong2"
	huawei_url    = "https://obs.cn-south-1.myhuaweicloud.com"
)

func InitHuaWei(url string, keyId string, keySecret string, bucket string) {
	huawei_keyId = keyId
	huawei_secret = keySecret
	huawei_bucket = bucket
	huawei_url = url
}

func UpLoadToHuaWeiOSS(bucket string, filename string, fd io.Reader) (*obs.PutObjectOutput, error) {
	// 创建OSSClient实例。
	client, err := obs.New(huawei_keyId, huawei_secret, huawei_url)
	if err != nil {
		return nil, err
	}

	// 上传文件流。
	input := &obs.PutObjectInput{}
	input.Bucket = bucket
	input.Key = filename
	input.Body = fd
	output, err2 := client.PutObject(input)
	if err2 != nil {
		e, ok := err2.(obs.ObsError)
		if ok {
			return nil, fmt.Errorf("PutObject err: %s", e.Message)
		} else {
			return nil, e
		}
	}
	return output, nil
}

func GetFileFromHuaWeiOSS(bucket string, filename string) ([]byte, error) {

	client, err := obs.New(huawei_keyId, huawei_secret, huawei_url)
	if err != nil {
		return nil, err
	}

	// 下载文件流。
	input := &obs.GetObjectInput{}
	input.Bucket = bucket
	input.Key = filename
	output, err2 := client.GetObject(input)

	if err2 == nil {
		defer output.Body.Close()
		fmt.Printf("StorageClass:%s, ETag:%s, ContentType:%s, ContentLength:%d, LastModified:%s\n",
			output.StorageClass, output.ETag, output.ContentType, output.ContentLength, output.LastModified)
		bs, readErr := ioutil.ReadAll(output.Body)
		if readErr != nil {
			return nil, readErr
		}
		return bs, nil

	} else if obsError, ok := err.(obs.ObsError); ok {
		fmt.Printf("Code:%s\n", obsError.Code)
		fmt.Printf("Message:%s\n", obsError.Message)
		return nil, fmt.Errorf("GetObject err: %s", obsError.Message)
	}
	return nil, fmt.Errorf("GetObject err")
}
