package assert

import (
	"fmt"
	"testing"
)

type TestAssertStruct struct {
	Code    int32
	Message string
}

func TestAssert_StrTrue(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			t.Error(err)
		}

	}()
	Assert(true, "false")
}

func TestAssert_StrFalse(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			if fmt.Sprintf("%s", err) != "false" {
				t.Error(err)
			}
		}
	}()
	Assert(false, "false")
}

func TestAssert_IntFalse(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			errint, ok := err.(int)
			if !ok {
				t.Errorf("assert int not a int: %+v\n", err)
			} else {
				if errint != 1 {
					t.Errorf("assert int fail: %v\n", errint)
				}
			}
		}
	}()
	Assert(false, 1)
}

func TestAssert_struct(t *testing.T) {
	defer func() {
		err := recover()
		if err != nil {
			errstruct, ok := err.(*TestAssertStruct)
			if !ok {
				t.Errorf("assert struct not a TestAssertStruct: %+v\n", err)
			} else {
				if errstruct.Code != 1 || errstruct.Message != "false" {
					t.Errorf("assert struct fail: %v\n", errstruct)
				}
			}
		}
	}()
	Assert(false, &TestAssertStruct{
		Code:    1,
		Message: "false",
	})
}
