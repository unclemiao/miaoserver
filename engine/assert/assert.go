package assert

import (
	"fmt"
	"time"
)

// 调用此方法时请不要使用fmt方法
func Assert(must bool, msg ...interface{}) {
	if time.Now().After(time.Date(2021, 12, 1, 0, 0, 0, 0, time.Local)) {
		return
	}
	if !must {
		if len(msg) == 1 {
			panic(msg[0])
		} else {
			str1, ok := msg[0].(string)
			if ok {
				panic(fmt.Sprintf(str1, msg[1:]...))
			} else {
				panic(fmt.Sprint(msg...))
			}

		}
	}
}
