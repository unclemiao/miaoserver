package util

import (
	"math"
	"strconv"

	"goproject/engine/assert"
)

// //////////////////--LimitInt16--/////////////////////////
func LimitInt16(value int16, min int16, max int16) int16 {
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

func LimitInt16Min(value int16, min int16) int16 {
	if value < min {
		return min
	}
	return value
}

func LimitInt16Max(value int16, max int16) int16 {
	if value > max {
		return max
	}
	return value
}

// //////////////////--LimitInt32--/////////////////////////
func LimitInt32(value int32, min int32, max int32) int32 {
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

func LimitInt32Min(value int32, min int32) int32 {
	if value < min {
		return min
	}
	return value
}

func LimitInt32Max(value int32, max int32) int32 {
	if value > max {
		return max
	}
	return value
}

// //////////////////--LimitInt--/////////////////////////
func LimitInt(value int, min int, max int) int {
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

func LimitIntMin(value int, min int) int {
	if value < min {
		return min
	}
	return value
}

func LimitIntMax(value int, max int) int {
	if value > max {
		return max
	}
	return value
}

// //////////////////--LimitInt64--/////////////////////////
func LimitInt64(value int64, min int64, max int64) int64 {
	if value < min {
		return min
	}
	if value > max {
		return max
	}
	return value
}

func LimitInt64Min(value int64, min int64) int64 {
	if value < min {
		return min
	}
	return value
}

func LimitInt64Max(value int64, max int64) int64 {
	if value > max {
		return max
	}
	return value
}

// value = value * (1+bonusRate)
func CalcBonusRate(value int32, bonusRate int32) int32 {
	if bonusRate == 0 {
		return value
	}

	return CalcBonusRateUnit(value, bonusRate, RATE_DENOMINATOR)
}

// value = value * (1+bonusRate)
func CalcBonusRateUnit(value int32, bonusRate int32, unit int32) int32 {
	if bonusRate == 0 {
		return value
	}

	return int32(int64(value) * (int64(bonusRate + unit)) / int64(unit))
}

// value = value * (1+bonusRate)
func CalcBonusRateUnit64(value int64, bonusRate int32, unit int32) int64 {
	if bonusRate == 0 {
		return value
	}

	return value * int64(bonusRate+unit) / int64(unit)
}

// value = value * percent
func CalcCoefficient(value int32, coefficient int32) int32 {
	if coefficient == 0 {
		return 0
	}

	return int32(int64(value) * int64(coefficient) / int64(RATE_DENOMINATOR))
}

// value = value * percent
func CalcCoefficientUnit(value int32, coefficient int32, unit int32) int32 {
	if coefficient == 0 {
		return 0
	}

	return int32(int64(value) * int64(coefficient) / int64(unit))
}

func CalcCoefficientUnit64(value int64, coefficient int32, unit int32) int64 {
	if coefficient == 0 {
		return 0
	}

	return value * int64(coefficient) / int64(unit)
}

func CalcPercent(value int32, maxValue int32) int32 {
	if value == 0 {
		return 0
	}

	return int32(int64(value) * int64(RATE_DENOMINATOR) / int64(maxValue))
}

func AbsI16(val int16) int16 {
	if val > 0 {
		return val
	}
	return -val
}

func AbsI32(val int32) int32 {
	if val > 0 {
		return val
	}
	return -val
}

func MaxInt(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func MinInt(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func MaxInt64(a, b int64) int64 {
	if a > b {
		return a
	}
	return b
}

func MinInt64(a, b int64) int64 {
	if a < b {
		return a
	}
	return b
}

func MaxInt32(a, b int32) int32 {
	if a > b {
		return a
	}
	return b
}

func MinInt32(a, b int32) int32 {
	if a < b {
		return a
	}
	return b
}

func MaxInt16(a, b int16) int16 {
	if a > b {
		return a
	}
	return b
}

func MinInt16(a, b int16) int16 {
	if a < b {
		return a
	}
	return b
}

func AtoInt(str string) int {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return i
}
func AtoUInt(str string) uint {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return uint(i)
}
func AtoInt64(str string) int64 {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return int64(i)
}
func AtoUInt64(str string) uint64 {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return uint64(i)
}

func AtoInt32(str string) int32 {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return int32(i)
}

func AtoUInt32(str string) uint32 {
	if str == "" {
		return 0
	}
	i, err := strconv.Atoi(str)
	assert.Assert(err == nil, err)
	return uint32(i)
}

func Ceil(v float64) int32 {
	return int32(math.Ceil(v))
}

func Floor(v float64) int32 {
	return int32(math.Floor(v))
}

func Round(v float64) int32 {
	return int32(math.Floor(v + 0.5))
}

func ChoiceInt(cond bool, choice1 int, choice2 int) int {
	if cond {
		return choice1
	}
	return choice2
}

func ChoiceInt32(cond bool, choice1 int32, choice2 int32) int32 {
	if cond {
		return choice1
	}
	return choice2
}

func ChoiceInt64(cond bool, choice1 int64, choice2 int64) int64 {
	if cond {
		return choice1
	}
	return choice2
}

func ChoiceStr(cond bool, str1 string, str2 string) string {
	if cond {
		return str1
	}
	return str2
}

func ChoiceFunc(cond bool, f1 func(), f2 func()) {
	if cond {
		f1()
	} else {
		f2()
	}
}
