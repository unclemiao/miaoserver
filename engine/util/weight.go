package util

type WeightItr interface {
	GetCfgId() int32
	GetWeight() int64
	SetWeight(int64)
	GetTotalWeight() int64
	SetTotalWeight(int64)
}

// 随机权重
func RandomWeight(list []WeightItr) int32 {
	totalWeight := list[0].GetTotalWeight()
	hit := Random64(0, totalWeight-1)
	for index, cfg := range list {
		if hit < cfg.GetWeight() {
			return int32(index)
		}
	}
	return 0
}

type WeightSelecter struct {
	totalWeight int64
	weightMap   map[int32]WeightItr
}

func NewWeightSelecter() *WeightSelecter {
	return &WeightSelecter{
		weightMap: make(map[int32]WeightItr),
	}
}

func (slt *WeightSelecter) Add(weight WeightItr) {
	slt.totalWeight += weight.GetWeight()
	slt.weightMap[weight.GetCfgId()] = weight
}

func (slt *WeightSelecter) Remove(weight WeightItr) {
	_, ok := slt.weightMap[weight.GetCfgId()]
	if !ok {
		return
	}
	slt.totalWeight -= weight.GetWeight()
	delete(slt.weightMap, weight.GetCfgId())
}

func (slt *WeightSelecter) Random() WeightItr {
	hit := Random64(0, slt.totalWeight)
	for _, cfg := range slt.weightMap {
		if hit <= cfg.GetWeight() {
			return cfg
		}
		hit -= cfg.GetWeight()
	}
	return nil
}
