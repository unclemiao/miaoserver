package util

import (
	"bytes"
	"encoding/binary"
)

func Bit64(bit uint8) uint64 {
	if bit >= 64 {
		return 0
	}
	return uint64(1) << bit
}

func CheckBit64(mark uint64, bit uint8) bool {
	if bit >= 64 {
		return false
	}
	return (mark & Bit64(bit)) > 0
}

func SetBit64(mark *uint64, bit uint8, bSet bool) {
	if bit >= 64 {
		return
	}
	if bSet {
		*mark |= Bit64(bit)
	} else {
		*mark &= ^Bit64(bit)
	}
}

func Bit32(bit uint8) uint32 {
	if bit >= 32 {
		return 0
	}
	return uint32(1) << bit
}

func CheckBit32(mark uint32, bit uint8) bool {
	if bit >= 32 {
		return false
	}
	return (mark & Bit32(bit)) > 0
}

func SetBit32(mark *uint32, bit uint8, bSet bool) {
	if bit >= 32 {
		return
	}
	if bSet {
		*mark |= Bit32(bit)
	} else {
		*mark &= ^Bit32(bit)
	}
}

func Bit16(bit uint8) uint16 {
	if bit >= 16 {
		return 0
	}
	return uint16(1) << bit
}

func CheckBit16(mark uint16, bit uint8) bool {
	if bit >= 16 {
		return false
	}
	return (mark & Bit16(bit)) > 0
}

func SetBit16(mark *uint16, bit uint8, bSet bool) {
	if bit >= 16 {
		return
	}
	if bSet {
		*mark |= Bit16(bit)
	} else {
		*mark &= ^Bit16(bit)
	}
}

func Bit8(bit uint8) uint8 {
	if bit >= 8 {
		return 0
	}
	return uint8(1) << bit
}

func CheckBit8(mark uint8, bit uint8) bool {
	if bit >= 8 {
		return false
	}
	return (mark & Bit8(bit)) > 0
}

func SetBit8(mark *uint8, bit uint8, bSet bool) {
	if bit >= 8 {
		return
	}
	if bSet {
		*mark |= Bit8(bit)
	} else {
		*mark &= ^Bit8(bit)
	}
}

func CheckBitArray(arr *[]uint8, bit uint32) bool {
	return ((*arr)[bit>>3] & uint8(1<<(bit&0x7))) > 0
}

func SetBitArray(arr *[]uint8, bit uint32, bSet bool) {
	if bSet {
		(*arr)[bit>>3] |= uint8(1 << (bit & 0x7))
	} else {
		(*arr)[bit>>3] &= uint8(^(1 << (bit & 0x7)))
	}
}

func MakeUint64(h, l uint32) uint64 {
	return (uint64(h)<<32 | uint64(l))
}

func MakeUint32(h, l uint16) uint32 {
	return (uint32(h)<<16 | uint32(l))
}

func MakeUint16(h, l uint8) uint16 {
	return (uint16(h)<<8 | uint16(l))
}

func U64_H(value uint64) uint32 {
	return uint32(value >> 32)
}

func U64_L(value uint64) uint32 {
	return uint32(value & 0xffffffff)
}

func U32_H(value uint32) uint16 {
	return uint16(value >> 16)
}

func U32_L(value uint32) uint16 {
	return uint16(value & 0xffff)
}

func U16_H(value uint16) uint8 {
	return uint8(value >> 8)
}

func U16_L(value uint16) uint8 {
	return uint8(value & 0xff)
}

func MakeInt64(h, l int32) int64 {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, h)
	binary.Write(buf, binary.LittleEndian, l)
	val := int64(0)
	binary.Read(buf, binary.LittleEndian, &val)
	return val
}

func MakeInt32(h, l int16) int32 {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, h)
	binary.Write(buf, binary.LittleEndian, l)
	val := int32(0)
	binary.Read(buf, binary.LittleEndian, &val)
	return val
}

func MakeInt16(h, l int8) int16 {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, h)
	binary.Write(buf, binary.LittleEndian, l)
	val := int16(0)
	binary.Read(buf, binary.LittleEndian, &val)
	return val
}

func ResolveInt64(val int64) (h, l int32) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, val)
	binary.Read(buf, binary.LittleEndian, &h)
	binary.Read(buf, binary.LittleEndian, &l)
	return
}

func ResolveInt32(val int32) (h, l int16) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, val)
	binary.Read(buf, binary.LittleEndian, &h)
	binary.Read(buf, binary.LittleEndian, &l)
	return
}

func ResolveInt16(val int16) (h, l int8) {
	buf := new(bytes.Buffer)
	binary.Write(buf, binary.LittleEndian, val)
	binary.Read(buf, binary.LittleEndian, &h)
	binary.Read(buf, binary.LittleEndian, &l)
	return
}
