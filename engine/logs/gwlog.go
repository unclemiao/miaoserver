package logs

import (
	"runtime/debug"

	"github.com/kataras/golog"
)

func New() *golog.Logger {
	return golog.New()
}

func GetLogger() *golog.Logger {
	return gLogger
}

// Error will print only when logger's Level is error, warn, info or debug.
func Error(v ...interface{}) {
	gLogger.Error(v...)
	gLogger.Error(string(debug.Stack()))
}

// Errorf will print only when logger's Level is error, warn, info or debug.
func Errorf(format string, args ...interface{}) {
	gLogger.Errorf(format, args...)
	gLogger.Error(string(debug.Stack()))
}

// Warn will print when logger's Level is warn, info or debug.
func Warn(v ...interface{}) {
	gLogger.Warn(v...)
}

// Warnf will print when logger's Level is warn, info or debug.
func Warnf(format string, args ...interface{}) {
	gLogger.Warnf(format, args...)
}

// Warningf exactly the same as `Warnf`.
// It's here for badger integration:
// https://github.com/dgraph-io/badger/blob/ef28ef36b5923f12ffe3a1702bdfa6b479db6637/logger.go#L25
func Warningf(format string, args ...interface{}) {
	gLogger.Warningf(format, args...)
}

// Info will print when logger's Level is info or debug.
func Info(v ...interface{}) {
	gLogger.Info(v...)
}

// Infof will print when logger's Level is info or debug.
func Infof(format string, args ...interface{}) {
	gLogger.Infof(format, args...)
}

// Debug will print when logger's Level is debug.
func Debug(v ...interface{}) {
	gLogger.Debug(v...)
}

// Debugf will print when logger's Level is debug.
func Debugf(format string, args ...interface{}) {
	gLogger.Debugf(format, args...)
}
