package logs

import (
	"os"
	"path/filepath"
	"time"

	"github.com/kataras/golog"

	"goproject/engine/assert"
)

var gLogger *golog.Logger
var gLogFile *os.File

func InitLog(path string) {
	if gLogger == nil {
		gLogger = golog.New()
		gLogger.TimeFormat = "2006-01-02 15:04:05.000"
	}
	var err error
	logPath, e := filepath.Abs(path)
	assert.Assert(e == nil, e)

	err = os.MkdirAll(logPath, 0755)
	assert.Assert(err == nil, err)
	if gLogFile != nil {
		err = gLogFile.Close()
		assert.Assert(err == nil, err)
	}
	gLogFile = newLogFile(logPath)
	gLogger.SetOutput(gLogFile)
	gLogger.AddOutput(os.Stderr)
	gLogger.SetLevel("debug")

	gLogger.Infof("InitLog finish")
}

// get a filename based on the date, file logs works that way the most times
// but these are just a sugar.
func todayFilename() string {
	today := time.Now().String()[:10]
	return today + ".log"
}

func newLogFile(path string) *os.File {
	filename := filepath.Join(path, todayFilename())
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	assert.Assert(err == nil, err)

	return f
}
