package data

type ServerNode struct {
	Sid        int32  `db:"sid"`
	IsMerge    bool   `db:"is_merge"`
	IsMaster   bool   `db:"is_merge"`
	BeginTime  int64  `db:"begin_time"`
	LastMinute int64  `db:"last_minute"`
	DataVer    int32  `db:"data_ver"`
	Host       string `db:"host"`
	SqlVersion int64  `db:"sql_version"`
	Ver        int32  `db:"version"`
}
