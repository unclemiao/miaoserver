package data

import (
	"goproject/src/server/protocol"
)

type MsgNode struct {
	Msg      *protocol.Envelope
	NextNode *MsgNode
}

type MsgBuff struct {
	Head *MsgNode
	Last *MsgNode
}
