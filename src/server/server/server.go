package server

import (
	"goproject/engine/netWork/webSocket"
	"goproject/src/server/common"
	"goproject/src/server/data"
)

type Server struct {
	Node     *data.ServerNode
	NodeList []*data.ServerNode

	// 网络底层接口
	Net *webSocket.NetServer

	// 逻辑数据
	sessionMgr *common.SessionManager

	// 动态数据----------
	IsStarted   bool
	Stop        chan struct{}
	ReceiveChan chan *common.Context
	StopChan    chan struct{}
}
