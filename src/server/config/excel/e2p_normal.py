﻿# -*- coding: utf-8 -*-
import sys
import os.path
sys.path.append(sys.path[0] + "/..")
sys.path.append(sys.path[0] + "/../tools")
from pyExcelerator import *
from openpyxl import *
import shutil
import zipfile

# 通用的转换函数 通过.bat传入五个参数
# 0.py文件名
# 1.xls文件位置
# 2.转换后的py文件位置
# 3.keyrow 列键所在行
# 4.keycol 行键所在列

def xls2json(fullname, fname):
	global SRC_DIR, DEST_FILE, gZipList
	if (not os.path.isfile(fullname)) or (not fullname.endswith('xls') and not fullname.endswith('xlsx')):
		print "-------------fail" + fname
		sys.exit(1)

	name = fname.replace('.xlsx', "")
	descFile = os.path.join(DEST_FILE, name + ".json")
	keyrow = 3
	keycol = 1
	# if name != "monster" and name != "boss":
	gZipList.append(name + ".json")
	
	# 2.自定义导出表名，此处为DATA
	try:
		f = open(descFile, "w")
	except:
		print "打开(创建)文件失败" % descFile
		sys.exit(1)

	srcfile = descFile.replace(DEST_FILE, "")
	print "file... %s" % srcfile
	# 3.指定键所在行列，获取规范化的导表数据-字典
	scc = u"Sheet1"
	data = GetSheetData(fullname, None, keyrow=keyrow, keycol=keycol)
	# 4.通过pprint输出至文件DEST_FILE
	ptools = PrettyPrinter(stream=f, indent=1, width=1)
	ptools.pprint(data)
	f.close()

	lines = []
	fp = open(descFile, "rt+")
	s = fp.readline()
	while (s):
		s = s.replace(': ', ':')
		s = s.replace(', ', ',')
		lines.append(s)
		s = fp.readline()
	fp.close()

	fp = open(descFile, "wt+")
	fp.writelines(lines)
	fp.close()


def doXlsx(srcDir):
	pathdir = os.listdir(srcDir)
	data = {}
	for fname in pathdir:
		fullname = os.path.join(srcDir, fname)
		n, ext = os.path.splitext(fullname)
		if fullname.find(".svn") >= 0 or fullname.find("$") >= 0:
			continue
		if os.path.isdir(fullname):
			doXlsx(fullname)
			continue
		if ext != ".xlsx":
			continue
		xls2json(fullname, fname)

if (__name__=="__main__"):
	global SRC_DIR, DEST_FILE, gZipList
	SRC_DIR = sys.argv[1]
	DEST_FILE = sys.argv[2]
	gZipList = []
	doXlsx(SRC_DIR)

	zip = zipfile.ZipFile(DEST_FILE + "config.zz", "w", zipfile.ZIP_DEFLATED)
	for filename in gZipList:
		zip.write(os.path.join(DEST_FILE, filename), filename)
	zip.close()
    
	fullname = DEST_FILE + "config.zz"
	fp = open(fullname, "rb")
	s = fp.read()
	fp.close()
	print "---------s", len(s)
	b = s[0:467]
	s = b[::-1] + s[::-1] + b[0:22]
	fp = open(fullname, "wb")
	fp.write(s)
	fp.close()


