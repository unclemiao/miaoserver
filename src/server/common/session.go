package common

import (
	"goproject/src/server/data"
	"sync"

	"github.com/gorilla/websocket"
	"goproject/src/konglong/protocol"
)

type Session struct {
	Conn       *websocket.Conn
	Id         int64
	Ip         string
	AccountId  string
	RoleId     int64
	From       string
	Young      bool
	Sid        int32
	SignInTime int64

	// 消息缓冲
	MsgBuff *data.MsgBuff
	// 消息发送管道
	WriteChan chan *protocol.Envelope
	StopChan  chan chan struct{}
	IsStop    bool

	Lock sync.RWMutex
}

type SessionManager struct {
	SessionMap    map[*websocket.Conn]Session
	SessionLock   sync.RWMutex
	SessionNumber int64
}

func NewSessionManager() *SessionManager {
	return &SessionManager{
		SessionMap: make(map[*websocket.Conn]Session),
	}
}
