package models

import (
	"time"
)

const (
	NODE_STATE_TYPE_CLOSE  int32 = iota // 关服
	NODE_STATE_TYPE_HOT                 // 火热推荐
	NODE_STATE_TYPE_FREE                // 空闲
	NODE_STATE_TYPE_BUSY                // 繁忙
	NODE_STATE_TYPE_FULL                // 满员
	NODE_STATE_TYPE_REPAIR              // 维护
)

type NodeInfo struct {
	Id              int64     `orm:"auto;pk"`
	Sid             int64     `orm:"description(服务器号)"`
	Name            string    `orm:"size(128);description(服务器名)"`
	Desc            string    `orm:"size(128);description(描述)"`
	Platform        string    `orm:"size(16);description(平台)"`
	Channel         string    `orm:"size(16);description(渠道名字)"`
	State           int32     `orm:"description(服务器状态)"` // 从 0 开始 [ 关服, 运营推荐, 空闲, 繁忙, 满员, 停服维护 ] 如果等于停服维护则表示。需要开服但是没有可用的机器。
	Young           bool      `orm:"description(服务器是否防沉迷)"`
	Token           string    `orm:"size(512);description(密钥)"`
	Online          int32     `orm:"description(总在线人数)"`
	CreateNum       int32     `orm:"description(总注册人数)"`
	DockerName      string    `orm:"size(512);description(docker镜像名字)"`
	ExcelVer        string    `orm:"size(512);description(excel配置版本名)"`
	ClientVer       string    `orm:"size(512);description(客户端版本)"`
	JavaLogServerId int64     `orm:"description(java日志统计区服id)"`
	JavaLogGameId   int64     `orm:"description(java日志统计游戏id)"`
	Created         time.Time `orm:"auto_now_add;type(datetime)"`
	Updated         time.Time `orm:"auto_now;type(datetime)"`
	InitTime        time.Time `orm:"type(datetime)"`
	SlaveStr        string    `orm:"size(1024);description(合入的服列表)"`
	SlaveList       []int64   `orm:"-"`
	IsSlave         bool      `orm:"description(是否合服后的从服)"`

	Port      int32
	ClientUrl string `orm:"size(128)"`
	CenterUrl string `orm:"size(128)"`
	AdminHost string `orm:"size(128)"`
	SqlHost   string `orm:"size(128)"`
	SqlRoot   string `orm:"size(128)"`
	SqlPass   string `orm:"size(128)"`
	SqlName   string `orm:"size(128)"`
}

// 多字段唯一键
func (n *NodeInfo) TableUnique() [][]string {
	return [][]string{
		{"sid", "platform", "channel"},
	}
}

// 多字段索引
func (n *NodeInfo) TableIndex() [][]string {
	return [][]string{
		{"platform", "channel"},
	}
}

func (n *NodeInfo) GetResponse() *ServerNodeResponse {
	return &ServerNodeResponse{
		Sid:             n.Sid,
		Name:            n.Name,
		State:           n.State,
		Platform:        n.Platform,
		Channel:         n.Channel,
		ClientUrl:       n.ClientUrl,
		Token:           n.Token,
		JavaLogServerId: n.JavaLogServerId,
		JavaLogGameId:   n.JavaLogGameId,
	}
}

func (n *NodeInfo) Copy() *NodeInfo {
	var copyNode NodeInfo
	copyNode = *n
	return &copyNode
}

type ServerNodeResponse struct {
	Sid             int64
	Name            string
	ClientUrl       string
	State           int32
	Platform        string
	Channel         string
	Online          int32
	Create          int32
	NodeNum         int32
	Token           string
	JavaLogServerId int64
	JavaLogGameId   int64
}

type ServerCmd struct {
	Id         int64
	Sid        int64
	Platform   string
	Port       int64
	Channel    string
	CenterHost string
	AdminHost  string
	IsMaster   string
	DockerName string
	CreateTime int64
}
