package models

type WxLoginResponse struct {
	Openid     string `json:"openid"`
	SessionKey string `json:"session_key"`
	Unionid    string `json:"unionid"`
	Errcode    int32  `json:"errcode"`
	Errmsg     string `json:"errmsg"`
}

type VivoDataResponse struct {
	Openid string `json:"openid"`
}

type VivoLoginResponse struct {
	Code int32             `json:"code"`
	Msg  string            `json:"msg"`
	Data *VivoDataResponse `json:"data"`
}

type OppoDataResponse struct {
	UserId string `json:"userId"`
}

type OppoLoginResponse struct {
	ErrCode  int32             `json:"errCode"`
	ErrMsg   string            `json:"errMsg"`
	UserInfo *OppoDataResponse `json:"userInfo"`
}
