package models

import "time"

type ExcelVer struct {
	Id      int64  `orm:"auto;pk"`
	Version string `orm:"size(124);description(版本号)"`
	OssUrl  string `orm:"size(124);description(版本号)"`

	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`
}

// 多字段唯一键
func (n *ExcelVer) TableUnique() [][]string {
	return [][]string{
		{"version"},
	}
}
