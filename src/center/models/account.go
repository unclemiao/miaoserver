package models

import "time"

type Account struct {
	Id           int64     `orm:"auto;pk"`
	Name         string    `orm:"size(64);description(账号名)"`
	Pass         string    `orm:"size(128);description(密码)"`
	Platform     string    `orm:"size(64);description(平台)"`
	Channel      string    `orm:"size(64);description(渠道)"`
	LastServerId int64     `orm:"description(上次登录服务器)"`
	Create       time.Time `orm:"auto_now_add;type(datetime)"`
	Update       time.Time `orm:"auto_now;type(datetime)"`
	Tag          int32     `orm:"description(标识)"`
}

// 多字段唯一键
func (n *Account) TableUnique() [][]string {
	return [][]string{
		{"name", "platform", "channel"},
	}
}

type AccountResponse struct {
	AccountId string
	Due       int64
	Token     string
	ServerId  int64
	OpenId    string
	Server    *ServerNodeResponse
}
