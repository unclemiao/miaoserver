package models

import (
	"time"
)

type StartingNode struct {
	Id        int64  `orm:"auto;pk"`
	Sid       int64  `orm:"description(服务器号)"`
	Name      string `orm:"size(128);description(服务器名)"`
	Port      int32  `orm:"description(服务器端口)"`
	ClientUrl string `orm:"description(客户端连接url)"`
	CenterUrl string `orm:"size(128);description(中心服url)"`
	Token     string `orm:"size(512);description(密钥)"`

	State      int32  `orm:"description(服务器状态)"` // 从 0 开始 [ 关服, 运营推荐, 空闲, 繁忙, 满员, 停服维护 ] 如果等于停服维护则表示。需要开服但是没有可用的机器。
	Platform   string `orm:"size(16);description(平台)"`
	Channel    string `orm:"size(16);description(渠道名字)"`
	Online     int32  `orm:"description(总在线人数)"`
	CreateNum  int32  `orm:"description(总注册人数)"`
	DockerName string `orm:"size(512);description(docker镜像名字)"`

	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`

	AdminHost string `orm:"size(128);description(admin url)"`
	IsMaster  bool   `orm:"description(是否主服)"`
}

func (n *StartingNode) GetResponse() *ServerNodeResponse {
	return &ServerNodeResponse{
		Sid:       n.Sid,
		Name:      n.Name,
		State:     n.State,
		Platform:  n.Platform,
		Channel:   n.Channel,
		ClientUrl: n.ClientUrl,
		Token:     n.Token,
	}
}
