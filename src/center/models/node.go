package models

import (
	"sync"
	"time"
)

const (
	NODE_STATE_TYPE_CLOSE  int32 = iota // 关服
	NODE_STATE_TYPE_HOT                 // 火热推荐
	NODE_STATE_TYPE_FREE                // 空闲
	NODE_STATE_TYPE_BUSY                // 繁忙
	NODE_STATE_TYPE_FULL                // 满员
	NODE_STATE_TYPE_REPAIR              // 维护
)

type NodeInfo struct {
	Id         int64  `orm:"auto;pk"`
	Sid        int64  `orm:"description(服务器号)"`
	Name       string `orm:"size(128);description(服务器名)"`
	Port       int32  `orm:"description(服务器端口)"`
	ClientUrl  string `orm:"description(客户端连接url)"`
	CenterUrl  string `orm:"size(128);description(中心服url)"`
	SqlHost    string `orm:"size(512);description(数据库连接域名)"`
	SqlRoot    string `orm:"size(128);description(数据库用户名)"`
	SqlPass    string `orm:"size(128);description(数据库密码)"`
	SqlName    string `orm:"size(128);description(数据库名)"`
	State      int32  `orm:"description(服务器状态)"` // 从 0 开始 [ 关服, 运营推荐, 空闲, 繁忙, 满员, 停服维护 ] 如果等于停服维护则表示。需要开服但是没有可用的机器。
	Young      bool   `orm:"description(服务器是否防沉迷)"`
	Token      string `orm:"size(512);description(密钥)"`
	Platform   string `orm:"size(16);description(平台)"`
	Channel    string `orm:"size(16);description(渠道名字)"`
	Online     int32  `orm:"description(总在线人数)"`
	CreateNum  int32  `orm:"description(总注册人数)"`
	DockerName string `orm:"size(512);description(docker镜像名字)"`
	ExcelVer   string `orm:"size(512);description(excel配置版本名)"`

	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`

	// ------------------ 动态数据 -----------------------
	NowState  int32        `orm:"-" json:"-"`
	AdminHost string       `orm:"-" json:"-"`
	WaitStart bool         `orm:"-" json:"-"`
	IsMaster  bool         `orm:"-" json:"-"`
	StartNum  int32        `orm:"-" json:"-"`
	Lock      sync.RWMutex `orm:"-" json:"-"`
}

// 多字段唯一键
func (n *NodeInfo) TableUnique() [][]string {
	return [][]string{
		{"sid", "platform", "channel"},
	}
}

// 多字段索引
func (n *NodeInfo) TableIndex() [][]string {
	return [][]string{
		{"platform", "channel"},
	}
}

func (n *NodeInfo) GetResponse() *ServerNodeResponse {
	return &ServerNodeResponse{
		Sid:       n.Sid,
		Name:      n.Name,
		State:     n.State,
		Platform:  n.Platform,
		Channel:   n.Channel,
		ClientUrl: n.ClientUrl,
		Token:     n.Token,
	}
}

func (n *NodeInfo) Copy() *NodeInfo {
	var copyNode NodeInfo
	copyNode = *n
	return &copyNode
}

type ServerNodeResponse struct {
	Sid       int64
	Name      string
	ClientUrl string
	State     int32
	Platform  string
	Channel   string
	Online    int32
	Create    int32
	NodeNum   int32
	Token     string
}

type ServerCmd struct {
	Id         int64
	Sid        int64
	Platform   string
	Port       int64
	Channel    string
	CenterHost string
	AdminHost  string
	IsMaster   string
	DockerName string
	CreateTime int64
}
