package models

import "time"

type Server struct {
	Id      int64     `orm:"pk"`
	Version string    `orm:"size(128)"`
	Token   string    `orm:"size(512)"`
	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`
}

type AppFlag struct {
	Host      string
	Httpport  int64
	Sqlconn   string
	SqlInit   bool
	Token     string
	Master    bool
	MasterUrl string
}
