package models

type GameNotice struct {
	Id       int64 `orm:"auto;pk"`
	Sid      int64
	Platform string `orm:"size(16);description(平台)"`
	Channel  string `orm:"size(16);description(渠道名字)"`
	Title    string `orm:"size(128);description(公告标题)"`
	Msg      string `orm:"size(2048);description(公共内容)"`
	Create   int64
	SendTime int64
	Url      string `orm:"size(1024);description(公告url)"`
}

type NoticeJson struct {
	Title   string
	Msg     string
	Version int64
}
