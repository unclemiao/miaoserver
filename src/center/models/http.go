package models

type HttpResponse struct {
	Result   int32       `json:"result"`
	Msg      string      `json:"msg"`
	ErrMsg   string      `json:"errMsg"`
	Status   int32       `json:"status"`
	DateTime int64       `json:"dateTime"`
	Data     interface{} `json:"models"`
}

func (resp *HttpResponse) SetCode(code int32) {
	resp.Status = code
}

func (resp *HttpResponse) SetErr(errmsg string) {
	resp.ErrMsg = errmsg
}

func (resp *HttpResponse) GetErr() string {
	return resp.ErrMsg
}

type Response struct {
	Code int32
	Err  string
	Data interface{}
}

func (resp *Response) SetCode(code int32) {
	resp.Code = code
}

func (resp *Response) SetErr(errmsg string) {
	resp.Err = errmsg
}

func (resp *Response) GetErr() string {
	return resp.Err
}

type ServerCmdResponse struct {
	Code int32
	Err  string
}

func (resp *ServerCmdResponse) SetCode(code int32) {
	resp.Code = code
}

func (resp *ServerCmdResponse) SetErr(errmsg string) {
	resp.Err = errmsg
}

func (resp *ServerCmdResponse) GetErr() string {
	return resp.Err
}

type NodeTemplateResponse struct {
	Code    int32
	Err     string
	Current int32
	Total   int32
	Data    interface{}
}

func (resp *NodeTemplateResponse) SetCode(code int32) {
	resp.Code = code
}

func (resp *NodeTemplateResponse) SetErr(errmsg string) {
	resp.Err = errmsg
}

func (resp *NodeTemplateResponse) GetErr() string {
	return resp.Err
}

type SelectNodeResponse struct {
	Code int32
	Err  string
	Data *RunningGame
}

func (resp *SelectNodeResponse) SetCode(code int32) {
	resp.Code = code
}

func (resp *SelectNodeResponse) SetErr(errmsg string) {
	resp.Err = errmsg
}

func (resp *SelectNodeResponse) GetErr() string {
	return resp.Err
}

type IosPayResponse struct {
	Result string `json:"result"`
	Errmsg string `json:"errmsg"`
}

func (resp *IosPayResponse) SetCode(code int32) {
	if code == 200 {
		resp.Result = "ok"
	} else {
		resp.Result = "fail"
	}
}

func (resp *IosPayResponse) SetErr(errmsg string) {
	resp.Result = "fail"
	resp.Errmsg = errmsg
}

func (resp *IosPayResponse) GetErr() string {
	return resp.Errmsg
}

type IosStr struct {
	Sid       int64  `json:"sid"`
	RoleId    int64  `json:"roleId"`
	AccountId string `json:"userId"`
	MallId    int64  `json:"itemId"`
	Channel   string `json:"channel"`
	Platform  string `json:"platform"`
}
