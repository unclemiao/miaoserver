package models

import (
	"sync"
)

type AdminNode struct {
	Host            string
	StartingNodeMap map[int64]*RunningGame
	ReadyNodeMap    map[int64]*ServerCmd
	PortNum         int32
	NodeNumMax      int32
	LastPingTime    int64
	Lock            sync.RWMutex
}
