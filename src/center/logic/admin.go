package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/models"
)

type AdminManager struct {
	AdminList []*models.AdminNode
}

func (mgr *ServerManager) getAdminNode(host string) *models.AdminNode {
	for _, ad := range mgr.AdminMgr.AdminList {
		if ad.Host == host {
			return ad
		}
	}
	return nil
}

func (mgr *ServerManager) adminNodeAddNode(admin *models.AdminNode, node *models.RunningGame) {
	delete(admin.ReadyNodeMap, node.Id)
	admin.StartingNodeMap[node.Id] = node
}

func (mgr *ServerManager) UpdateAdmin(host string, nodeMax int32) {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	adminNode := mgr.getAdminNode(host)

	if adminNode == nil {
		adminNode = &models.AdminNode{
			Host:            host,
			StartingNodeMap: make(map[int64]*models.RunningGame),
			ReadyNodeMap:    make(map[int64]*models.ServerCmd),
		}
		mgr.AdminMgr.AdminList = append(mgr.AdminMgr.AdminList, adminNode)
	}
	adminNode.NodeNumMax = nodeMax
	adminNode.LastPingTime = time_helper.NowSec()
	logs.Infof("UpdateAdmin: %+v", adminNode)

}

func (mgr *ServerManager) GetAdminNodeList() []*models.AdminNode {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	dataList := make([]*models.AdminNode, 0, len(mgr.AdminMgr.AdminList))
	for _, n := range mgr.AdminMgr.AdminList {
		dataList = append(dataList, n)
	}
	return dataList
}

func (mgr *ServerManager) findCanStartAdminNode() *models.AdminNode {
	for _, ad := range mgr.AdminMgr.AdminList {
		if ad.NodeNumMax-int32(len(ad.StartingNodeMap)+len(ad.ReadyNodeMap)) > 0 {
			logs.Infof("findCanStartAdminNode: %+v", ad)
			return ad
		}
	}
	return nil
}

func (mgr *ServerManager) TryStartGameServerToAdmin(serverNode *models.NodeInfo, isMaster bool) error {
	logs.Infof("tryStartGameServerToAdmin: %+v", serverNode)

	serverMgr := GetServerManager()

	now := time.Now()
	form := make(url.Values)
	cmd := &models.ServerCmd{
		Id:         serverNode.Sid,
		Sid:        serverNode.Sid,
		Platform:   serverNode.Platform,
		Port:       30000 + serverNode.Sid,
		Channel:    serverNode.Channel,
		CenterHost: serverNode.CenterUrl,
		IsMaster:   fmt.Sprintf("%v", isMaster),
		DockerName: serverNode.DockerName,
		CreateTime: now.Unix(),
	}

	cmdstr, _ := json.Marshal(cmd)
	due := fmt.Sprintf("%d", time_helper.NowSec()+time_helper.Hour*4)
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", cmd.Sid, cmd.Platform, cmd.Channel, due, serverMgr.Server.Token))

	form.Set("data", string(cmdstr))
	form.Set("due", due)
	form.Set("weiyingToken", md5)
	host := serverNode.AdminHost
	logs.Info("Open: ", host+"/server/cmd")
	resp, err := http.PostForm(host+"/server/cmd", form)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	respData := &models.ServerCmdResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		return fmt.Errorf("Unmarshal err, err:%+v ; bs:%s ", err, bs)
	}

	if respData.Err != "" {
		return fmt.Errorf("%s", respData.Err)
	}

	logs.Infof("httpResp %s", string(bs))

	return nil
}

func (mgr *ServerManager) GetServerStarCmd(sid int64, id int64, plt string, ch string, port int32, isMaster bool, adminHost string) (*models.NodeInfo, error) {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	serverCmd, err := gServerMgr.getServerStarCmd(sid, id, plt, ch, isMaster, adminHost, port)
	if err != nil {
		return nil, err
	}
	return serverCmd, nil
}

func (mgr *ServerManager) CheckPing() {
	time.AfterFunc(time.Second*30, mgr.CheckPing)
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	nowSec := time_helper.NowSec()
	newList := make([]*models.AdminNode, 0, len(mgr.AdminMgr.AdminList))
	for _, ad := range mgr.AdminMgr.AdminList {
		if nowSec-ad.LastPingTime <= 3*time_helper.Minute {
			newList = append(newList, ad)
		}
		for _, ready := range ad.ReadyNodeMap {
			if nowSec-ready.CreateTime > time_helper.Minute {
				delete(ad.ReadyNodeMap, ready.Id)
			}
		}
	}
	mgr.AdminMgr.AdminList = newList
	mgr.showStartingNode()
	go gRunningNodeMgr.CheckServerPing()

}

func (mgr *ServerManager) showStartingNode() {
	logs.Info("\n================================== Show Node Info Begin ==================================\n")

	logs.Info(" ================================== print starting node ================================== ")
	gRunningNodeMgr.Range(func(game *models.RunningGame) bool {
		logs.Infof("starting: \n%+v", game)
		return true
	})

	logs.Info(" ================================== print admin info ================================== ")
	for _, admin := range mgr.AdminMgr.AdminList {
		logs.Infof("admin: \n%+v", admin)
		for _, adn := range admin.StartingNodeMap {
			logs.Infof("admin starting: \n%+v", adn)
		}
	}

	logs.Info("\n================================== Show Node Info End ==================================\n")

}
