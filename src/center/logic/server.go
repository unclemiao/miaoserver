package logic

import (
	"flag"
	"fmt"
	"sync"
	"time"

	"github.com/beego/beego/v2/client/orm"
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/filter/cors"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/sdk"
	"goproject/src/center/models"
)

var gServerMgr *ServerManager
var AppFlag *models.AppFlag

type ServerManager struct {
	OrmMgr   *OrmManager
	Server   *models.Server
	AdminMgr *AdminManager

	Lock sync.RWMutex
}

func NewServerManager() *ServerManager {
	gServerMgr = &ServerManager{
		AdminMgr: &AdminManager{},
	}
	return gServerMgr
}

func GetServerManager() *ServerManager {
	return gServerMgr
}

func (mgr *ServerManager) Init() {
	LogInit()
	mgr.initFlag()
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		// 允许访问所有源
		AllowAllOrigins: true,
		// 可选参数"GET", "POST", "PUT", "DELETE", "OPTIONS" (*为所有)
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		// 指的是允许的Header的种类
		AllowHeaders: []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		// 公开的HTTP标头列表
		ExposeHeaders: []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		// 如果设置，则允许共享身份验证凭据，例如cookie
		AllowCredentials: true,
	}))
	mgr.OrmMgr = gOrmMgr

	mgr.OrmMgr.Init(AppFlag.SqlInit)

	sdk.InitHuaWei(
		"https://obs.cn-south-1.myhuaweicloud.com",
		"TIDRIORTMYDE9BPKOBEU",
		"HxBFtQlO4q08UTPaxF92h1FxTzbJh44Vt0T7NY2c",
		"konglong2",
	)

	logs.Info("server init finish")
}

func (mgr *ServerManager) initFlag() {
	AppFlag = new(models.AppFlag)
	flag.Int64Var(&AppFlag.Httpport, "httpport", beego.AppConfig.DefaultInt64("httpport", 8080), "http端口")
	flag.StringVar(&AppFlag.Sqlconn, "sqlconn", beego.AppConfig.DefaultString("sqlconn", ""), "数据库连接")
	flag.BoolVar(&AppFlag.SqlInit, "sqlinit", beego.AppConfig.DefaultBool("sqlInit", false), "是否初始化所有表")
	flag.StringVar(&AppFlag.Host, "host", beego.AppConfig.DefaultString("host", "http://127.0.0.1:8080"), "程序域名")
	flag.StringVar(&AppFlag.Token, "token", beego.AppConfig.DefaultString("token", "*"), "token")
	flag.BoolVar(&AppFlag.Master, "master", beego.AppConfig.DefaultBool("master", true), "master")
	flag.StringVar(&AppFlag.MasterUrl, "masterurl", beego.AppConfig.DefaultString("masterurl", ""), "masterurl")
	flag.Parse()
	logs.Infof("initFlag:%+v", AppFlag)
}

func (mgr *ServerManager) Star() {

	server := &models.Server{Id: 1, Token: AppFlag.Token, Version: "1.0"}
	if AppFlag.Master {
		err := mgr.OrmMgr.Orm.Read(server)
		if err == orm.ErrNoRows {
			_, err = mgr.OrmMgr.Orm.Insert(server)
			assert.Assert(err == nil, err)
		}
		err = gRunningNodeMgr.LoadStartingNode()
		assert.Assert(err == nil, err)
		time.AfterFunc(time.Second*30, func() {
			mgr.CheckPing()
		})
	}

	mgr.Server = server

	mgr.heartbeat()
	beego.Run(fmt.Sprintf(":%d", AppFlag.Httpport))
}

func (mgr *ServerManager) Stop() {

}

func (mgr *ServerManager) heartbeat() {

	time.AfterFunc(time.Minute, mgr.heartbeat)
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	now := time.Now()
	if mgr.Server.Updated.Day() != now.Day() {
		LogInit()
	}
	mgr.Server.Updated = now
	if !AppFlag.Master {
		return
	}
	_, _ = mgr.OrmMgr.Orm.Update(mgr.Server)
}
