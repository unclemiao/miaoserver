package logic

import (
	"time"

	"github.com/beego/beego/v2/client/orm"
	_ "github.com/go-sql-driver/mysql" // import your used driver

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/src/center/models"
)

type OrmManager struct {
	Orm orm.Ormer
}

var gOrmMgr *OrmManager

func init() {
	gOrmMgr = &OrmManager{}
}

func (mgr *OrmManager) Init(isInit bool) {

	sqlConn := AppFlag.Sqlconn

	err := orm.RegisterDriver("mysql", orm.DRMySQL)
	assert.Assert(err == nil, err)
	logs.Debugf("sqlconn %s", sqlConn)
	err = orm.RegisterDataBase("default", "mysql", sqlConn,
		orm.MaxIdleConnections(100),
		orm.MaxOpenConnections(1000),
	)
	assert.Assert(err == nil, err)
	orm.DefaultTimeLoc = time.Local

	mgr.RegisterModel()

	err = orm.RunSyncdb("default", isInit, false)
	assert.Assert(err == nil, err)

	mgr.Orm = orm.NewOrm()

}

func (mgr *OrmManager) RegisterModel() {
	orm.RegisterModel(
		new(models.Server),
		new(models.NodeInfo),
		new(models.Account),
		new(models.RunningGame),
		new(models.ExcelVer),
		new(models.GameNotice),
	)
}
