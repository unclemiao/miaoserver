package logic

import (
	"fmt"

	"goproject/src/center/models"
)

func (mgr *ServerManager) getServerStarCmd(sid int64, id int64, plt string, ch string, isMaster bool, adminHost string, port int32) (*models.NodeInfo, error) {

	nodeInfo, err := gNodeInfoMgr.GetNodeInfo(sid, plt, ch)
	if err != nil {
		return nil, err
	}
	if nodeInfo == nil {
		return nil, fmt.Errorf("server not set, sid:%d, plt:%s, ch:%s", sid, plt, ch)
	}
	return nodeInfo, nil
}

func (mgr *ServerManager) tryStartServerNode(nodeInfo *models.NodeInfo) error {
	return nil

}
