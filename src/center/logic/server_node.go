package logic

import (
	"fmt"
	"time"

	"goproject/engine/logs"
	"goproject/src/center/models"
)

func (mgr *ServerManager) LoadServerNode() {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	var list []*models.ServerNode
	orm := mgr.OrmMgr.Orm
	_, err := orm.QueryTable("server_node").All(&list)
	if err != nil {
		logs.Error(err)
		return
	}
	for _, n := range list {
		if mgr.NodeMap[n.Platform] == nil {
			mgr.NodeMap[n.Platform] = make(map[string]map[int64]*models.ServerNode)
		}
		if mgr.NodeMap[n.Platform][n.Channel] == nil {
			mgr.NodeMap[n.Platform][n.Channel] = make(map[int64]*models.ServerNode)
		}
		mgr.NodeMap[n.Platform][n.Channel][n.Sid] = n
		if n.NodeMap == nil {
			n.NodeMap = make(map[int64]*models.ServerNode)
		}
		mgr.WaitStartMap[n.Id] = n
	}
	time.AfterFunc(time.Second*30, func() {
		mgr.CheckPing()
	})
}

func (mgr *ServerManager) newServerNode(node *models.ServerNode) (*models.ServerNode, error) {

	orm := mgr.OrmMgr.Orm
	_, err := orm.Insert(node)
	if err != nil {
		return nil, err
	}
	if mgr.NodeMap[node.Platform] == nil {
		mgr.NodeMap[node.Platform] = make(map[string]map[int64]*models.ServerNode)
	}
	if mgr.NodeMap[node.Platform][node.Channel] == nil {
		mgr.NodeMap[node.Platform][node.Channel] = make(map[int64]*models.ServerNode)
	}
	mgr.NodeMap[node.Platform][node.Channel][node.Sid] = node
	if node.NodeMap == nil {
		node.NodeMap = make(map[int64]*models.ServerNode)
	}
	logs.Infof("newServerNode: %+v", node)
	return node, nil
}

func (mgr *ServerManager) getServerNode(sid int64, plt string, ch string) (*models.ServerNode, error) {
	if mgr.NodeMap[plt] == nil {
		return nil, fmt.Errorf("platform not found, platform:%s", plt)
	}
	if mgr.NodeMap[plt][ch] == nil {
		return nil, fmt.Errorf("channel not found, channel:%s", ch)
	}
	return mgr.NodeMap[plt][ch][sid], nil
}

func (mgr *ServerManager) updateServerNode(s *models.ServerNode) error {
	_, err := mgr.OrmMgr.Orm.Update(s)
	if err != nil {
		return err
	}
	if mgr.NodeMap[s.Platform] == nil {
		mgr.NodeMap[s.Platform] = make(map[string]map[int64]*models.ServerNode)
	}
	if mgr.NodeMap[s.Platform][s.Channel] == nil {
		mgr.NodeMap[s.Platform][s.Channel] = make(map[int64]*models.ServerNode)
	}
	mgr.NodeMap[s.Platform][s.Channel][s.Sid] = s
	logs.Infof("updateServerNode: %+v", s)
	return err
}

func (mgr *ServerManager) GetServerNode(sid int64, plt string, ch string) (*models.ServerNode, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()

	return mgr.getServerNode(sid, plt, ch)
}

func (mgr *ServerManager) SetServerNode(s *models.ServerNode) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	var open bool

	old, err := mgr.getServerNode(s.Sid, s.Platform, s.Channel)
	if err != nil {
		return err
	}

	if old == nil {
		old, err = mgr.newServerNode(s)
		if err != nil {
			return err
		}
		open = s.State != models.NODE_STATE_TYPE_CLOSE
	} else {
		open = old.State == models.NODE_STATE_TYPE_CLOSE && s.State != models.NODE_STATE_TYPE_CLOSE

		old.Name, old.Port, old.SqlHost, old.SqlRoot, old.SqlPass, old.State, old.Young, old.Token, old.Online, old.CreateNum, old.DockerName, old.ClientUrl =
			s.Name, s.Port, s.SqlHost, s.SqlRoot, s.SqlPass, s.State, s.Young, s.Token, s.Online, s.CreateNum, s.DockerName, s.ClientUrl
		err = mgr.updateServerNode(old)
		if err != nil {
			return err
		}
	}
	logs.Infof("SetServerNode: %+v", s)

	if open {
		mgr.WaitStartMap[s.Id] = s
		mgr.tryStartServerNodes()
	}
	return err
}

func (mgr *ServerManager) PingServerNode(
	sid int64,
	plt string,
	ch string,
	id int64,
	isMaster bool,
	port int32,
	adminHost string,
	online int32,
) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	serverNode, err := mgr.getServerNode(sid, plt, ch)
	if err != nil {
		return err
	}
	pingServer := serverNode.NodeMap[id]
	now := time.Now()
	if pingServer == nil {
		// 第一次 ping
		pingServer = mgr.getServerNodeCopy(serverNode, id, isMaster, adminHost, port)
		pingServer.Created = now

		serverNode.NodeMap[id] = pingServer

		admin := mgr.getAdminNode(adminHost)
		if admin != nil {
			mgr.adminNodeAddNode(admin, pingServer)
		}
	}
	pingServer.Online = online
	pingServer.Updated = now

	logs.Infof("PingServerNode: %+v", pingServer)
	return nil
}

func (mgr *ServerManager) DeleteServerNode(s *models.ServerNode) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	_, err := mgr.OrmMgr.Orm.Delete(s)
	if err != nil {
		return err
	}
	if mgr.NodeMap[s.Platform] == nil {
		return nil
	}
	if mgr.NodeMap[s.Platform][s.Channel] == nil {
		return nil
	}
	delete(mgr.NodeMap[s.Platform][s.Channel], s.Sid)
	logs.Infof("DeleteServerNode: %+v", s)
	return nil
}

func (mgr *ServerManager) SelectServerNode(sid int64, platform string, channel string) (*models.ServerNode, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()

	if mgr.NodeMap[platform] == nil {
		return nil, fmt.Errorf("platform not found, platform:%s", platform)
	}
	if mgr.NodeMap[platform][channel] == nil {
		return nil, fmt.Errorf("channel not found, channel:%s", channel)
	}

	nodeMap := mgr.NodeMap[platform][channel]
	var serverNode *models.ServerNode

	if sid == 0 {
		var minCreate int32 = -1
		var minCreateSid int64
		var minOnline int32 = -1
		var minOnlineSid int64

		for _, n := range nodeMap {
			if n.State == models.NODE_STATE_TYPE_HOT {
				serverNode = mgr.selectSubNodeFromServerNode(n)
				logs.Infof("SelectServerNode: %+v", serverNode)
				break
			} else if n.State != models.NODE_STATE_TYPE_CLOSE &&
				n.State != models.NODE_STATE_TYPE_REPAIR {
				if minCreate == -1 {
					minCreate = n.CreateNum
					minCreateSid = n.Sid
				} else if n.CreateNum < minCreate {
					minCreate = n.CreateNum
					minCreateSid = n.Sid
				}

				if minOnline == -1 {
					minOnline = n.Online
					minOnlineSid = n.Sid
				} else if n.Online < minOnline {
					minOnline = n.Online
					minOnlineSid = n.Sid
				}

			}
		}

		if serverNode == nil {
			if minCreateSid > 0 {
				logs.Infof("SelectServerNode: %+v", nodeMap[minCreateSid])
				serverNode = mgr.selectSubNodeFromServerNode(nodeMap[minCreateSid])

			} else if minOnlineSid > 0 {
				logs.Infof("SelectServerNode: %+v", nodeMap[minOnlineSid])
				serverNode = mgr.selectSubNodeFromServerNode(nodeMap[minOnlineSid])
			}
		}

		if serverNode == nil {
			return nil, fmt.Errorf("no server can in")
		}

		return serverNode, nil
	} else {
		serverNode = mgr.selectSubNodeFromServerNode(nodeMap[sid])
		if serverNode == nil {
			return nil, fmt.Errorf("no server can in")
		}
		logs.Infof("SelectServerNode: %+v", serverNode)
		return serverNode, nil
	}

}

func (mgr *ServerManager) selectSubNodeFromServerNode(serverNode *models.ServerNode) *models.ServerNode {
	var sn *models.ServerNode
	for _, n := range serverNode.NodeMap {
		if sn == nil {
			sn = n
		} else if n.Online < sn.Online {
			sn = n
		}
	}
	return sn
}

func (mgr *ServerManager) SetDockerName(platform string, channel string, dockerName string) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	for plt, chMap := range mgr.NodeMap {
		if platform != "" && platform != plt {
			continue
		}
		for ch, idMap := range chMap {
			if channel != "" && channel != ch {
				continue
			}
			for _, n := range idMap {
				n.DockerName = dockerName
				_, err := mgr.OrmMgr.Orm.Update(n)
				if err != nil {
					return err
				}
			}
		}

	}

	return nil
}

func (mgr *ServerManager) CheckServerPing() {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	now := time.Now()
	for _, chMap := range mgr.NodeMap {
		for _, nodeIdMap := range chMap {
			for _, serverNode := range nodeIdMap {
				for id, node := range serverNode.NodeMap {
					if now.Sub(node.Updated) > time.Minute*3 {
						delete(serverNode.NodeMap, id)

						admin := mgr.getAdminNode(node.AdminHost)
						if admin != nil {
							admin.NodeNum--
							if admin.NodeMap[node.Platform] != nil && admin.NodeMap[node.Platform][node.Channel] != nil {
								delete(admin.NodeMap[node.Platform][node.Channel], id)
							}
						}

					}
				}
			}
		}
	}
}
