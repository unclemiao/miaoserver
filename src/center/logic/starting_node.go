package logic

import (
	"fmt"
	"net/http"
	"time"

	"goproject/engine/logs"
	"goproject/src/center/models"
)

func (mgr *ServerManager) LoadStartingNode() {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	var list []*models.StartingNode
	orm := mgr.OrmMgr.Orm
	_, err := orm.QueryTable("starting_node").All(&list)
	if err != nil {
		logs.Error(err)
		return
	}
	for _, n := range list {
		if !mgr.pingToNode(n) {
			_, err = orm.Delete(n)
			if err != nil {
				logs.Error(err)
			}
			continue
		}
		mgr.addStartingNode(n)

	}
}

func (mgr *ServerManager) getStartingNode(sid int64, id int64, plt string, ch string) *models.StartingNode {
	if mgr.StartingNodeMap[plt] == nil {
		return nil
	}
	if mgr.StartingNodeMap[plt][ch] == nil {
		return nil
	}
	if mgr.StartingNodeMap[plt][ch][sid] == nil {
		return nil
	}
	return mgr.StartingNodeMap[plt][ch][sid][id]
}

func (mgr *ServerManager) addStartingNode(node *models.StartingNode) {
	if mgr.StartingNodeMap[node.Platform] == nil {
		mgr.StartingNodeMap[node.Platform] = make(map[string]map[int64]map[int64]*models.StartingNode)
	}
	if mgr.StartingNodeMap[node.Platform][node.Channel] == nil {
		mgr.StartingNodeMap[node.Platform][node.Channel] = make(map[int64]map[int64]*models.StartingNode)
	}
	if mgr.StartingNodeMap[node.Platform][node.Channel][node.Sid] == nil {
		mgr.StartingNodeMap[node.Platform][node.Channel][node.Sid] = make(map[int64]*models.StartingNode)
	}
	mgr.StartingNodeMap[node.Platform][node.Channel][node.Sid][node.Id] = node
	node.Updated = time.Now()

	nodeInfo := mgr.NodeInfoMap[node.Platform][node.Channel][node.Sid]
	if nodeInfo == nil {
		logs.Warningf("nodeInfo == nil, plt: %s, ch: %s", node.Platform, node.Channel)
		return
	}
	nodeInfo.NowState = nodeInfo.State

}

func (mgr *ServerManager) pingToNode(node *models.StartingNode) bool {
	urlStr := "http://" + node.ClientUrl + "/GmFunc?func=Ping"
	_, err := http.Get(urlStr)
	if err != nil {
		logs.Error(err)
		return false
	}

	return true
}

func (mgr *ServerManager) PingServerNode(
	sid int64,
	plt string,
	ch string,
	id int64,
	isMaster bool,
	port int32,
	adminHost string,
	online int32,
) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()

	startingNode := mgr.getStartingNode(sid, id, plt, ch)
	logs.Debugf("getStartingNode %+v", startingNode)
	now := time.Now()
	if startingNode == nil {

		// 第一次 ping
		serverNode, err := mgr.getNodeInfo(sid, plt, ch)
		if err != nil {
			return err
		}
		startingNode = mgr.newStartingNode(serverNode, id, isMaster, adminHost, port)
		startingNode.Created = now
		startingNode.Online = online
		startingNode.Updated = now

		admin := mgr.getAdminNode(adminHost)
		if admin != nil {
			mgr.adminNodeAddNode(admin, startingNode)
		}

		_, err = mgr.OrmMgr.Orm.Insert(startingNode)
		if err != nil {
			return err
		}
		mgr.addStartingNode(startingNode)

	} else {
		startingNode.Online = online
		startingNode.Updated = now
		_, err := mgr.OrmMgr.Orm.Update(startingNode)
		if err != nil {
			return err
		}
	}

	logs.Infof("PingServerNode: %+v", startingNode)
	return nil
}

func (mgr *ServerManager) SelectStartingNode(sid int64, platform string, channel string) (*models.StartingNode, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()

	if mgr.NodeInfoMap[platform] == nil {
		return nil, fmt.Errorf("platform not found, platform:%s", platform)
	}
	if mgr.NodeInfoMap[platform][channel] == nil {
		return nil, fmt.Errorf("channel not found, channel:%s", channel)
	}

	nodeMap := mgr.NodeInfoMap[platform][channel]
	var serverNode *models.StartingNode

	if sid == 0 {
		var minCreate int32 = -1
		var minCreateSid int64
		var minOnline int32 = -1
		var minOnlineSid int64

		for _, n := range nodeMap {
			if n.NowState == models.NODE_STATE_TYPE_HOT {
				serverNode = mgr.selectSubNodeFromServerNode(n)
				logs.Infof("SelectStartingNode: %+v", serverNode)
				break
			} else if n.NowState != models.NODE_STATE_TYPE_CLOSE &&
				n.NowState != models.NODE_STATE_TYPE_REPAIR {
				if minCreate == -1 {
					minCreate = n.CreateNum
					minCreateSid = n.Sid
				} else if n.CreateNum < minCreate {
					minCreate = n.CreateNum
					minCreateSid = n.Sid
				}

				if minOnline == -1 {
					minOnline = n.Online
					minOnlineSid = n.Sid
				} else if n.Online < minOnline {
					minOnline = n.Online
					minOnlineSid = n.Sid
				}

			}
		}

		if serverNode == nil {
			if minCreateSid > 0 {
				logs.Infof("SelectStartingNode: %+v", nodeMap[minCreateSid])
				serverNode = mgr.selectSubNodeFromServerNode(nodeMap[minCreateSid])

			} else if minOnlineSid > 0 {
				logs.Infof("SelectStartingNode: %+v", nodeMap[minOnlineSid])
				serverNode = mgr.selectSubNodeFromServerNode(nodeMap[minOnlineSid])
			}
		}

		if serverNode == nil {
			return nil, fmt.Errorf("no server can in")
		}

		return serverNode, nil
	} else {
		serverNode = mgr.selectSubNodeFromServerNode(nodeMap[sid])
		if serverNode == nil {
			return mgr.SelectStartingNode(0, platform, channel)
		}
		logs.Infof("SelectStartingNode: %+v", serverNode)
		return serverNode, nil
	}

}

// 找出一个在线人数最少的区服
func (mgr *ServerManager) selectSubNodeFromServerNode(nodeInfo *models.NodeInfo) *models.StartingNode {
	var starting *models.StartingNode
	if mgr.StartingNodeMap[nodeInfo.Platform] == nil {
		return nil
	}
	if mgr.StartingNodeMap[nodeInfo.Platform][nodeInfo.Channel] == nil {
		return nil
	}
	if mgr.StartingNodeMap[nodeInfo.Platform][nodeInfo.Channel][nodeInfo.Sid] == nil {
		return nil
	}
	now := time.Now()
	for _, n := range mgr.StartingNodeMap[nodeInfo.Platform][nodeInfo.Channel][nodeInfo.Sid] {
		if now.Sub(n.Updated) > time.Minute*2 {
			continue
		}
		if starting == nil || starting.Online > n.Online {
			starting = n
		}
	}
	return starting
}

func (mgr *ServerManager) IsRepair(serverNode *models.NodeInfo) bool {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	if mgr.StartingNodeMap[serverNode.Platform] == nil {
		return true
	}
	if mgr.StartingNodeMap[serverNode.Platform][serverNode.Channel] == nil {
		return true
	}
	if len(mgr.StartingNodeMap[serverNode.Platform][serverNode.Channel][serverNode.Sid]) == 0 {
		return true
	}
	return false
}

func (mgr *ServerManager) CheckServerPing() {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	now := time.Now()
	for _, chMap := range mgr.StartingNodeMap {
		for _, sidMap := range chMap {
			for _, idMap := range sidMap {
				for id, n := range idMap {
					if now.Sub(n.Updated) >= time.Minute*2 {
						_, _ = mgr.OrmMgr.Orm.Delete(n)
						delete(idMap, id)
						admin := mgr.getAdminNode(n.AdminHost)
						if admin != nil {
							delete(admin.StartingNodeMap, id)
						}
					}
				}
			}
		}
	}
}
