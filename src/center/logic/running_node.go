package logic

import (
	"encoding/json"
	"fmt"
	"strings"
	"sync"
	"time"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/models"
)

type RunningNodeManager struct {
	RunningMap map[string]map[string]map[int64]map[int64]*models.RunningGame
	Lock       sync.RWMutex
}

var gRunningNodeMgr *RunningNodeManager

func init() {
	gRunningNodeMgr = &RunningNodeManager{
		RunningMap: make(map[string]map[string]map[int64]map[int64]*models.RunningGame),
	}
}

func GetRunningMgr() *RunningNodeManager {
	return gRunningNodeMgr
}

func (mgr *RunningNodeManager) initMap(sid int64, plt string, ch string) {
	if plt != "" && mgr.RunningMap[plt] == nil {
		mgr.RunningMap[plt] = make(map[string]map[int64]map[int64]*models.RunningGame)
	}
	if ch != "" && mgr.RunningMap[plt][ch] == nil {
		mgr.RunningMap[plt][ch] = make(map[int64]map[int64]*models.RunningGame)
	}
	if sid > 0 && mgr.RunningMap[plt][ch][sid] == nil {
		mgr.RunningMap[plt][ch][sid] = make(map[int64]*models.RunningGame)
	}
}

func (mgr *RunningNodeManager) LoadStartingNode() error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	gOrmMgr.Orm.Raw("DELETE FROM `running_game`;").Exec()
	return nil
}

func (mgr *RunningNodeManager) newStartingNode(masterNode *models.NodeInfo, id int64, isMaster bool, adminHost string, port int32) *models.RunningGame {

	startNode := &models.RunningGame{
		Id:              id,
		Sid:             masterNode.Sid,
		Name:            masterNode.Name,
		Port:            port,
		ClientUrl:       masterNode.ClientUrl,
		CenterUrl:       masterNode.CenterUrl,
		State:           masterNode.State,
		Platform:        masterNode.Platform,
		Channel:         masterNode.Channel,
		CreateNum:       masterNode.CreateNum,
		DockerName:      masterNode.DockerName,
		Created:         masterNode.Created,
		Updated:         time.Now(),
		AdminHost:       adminHost,
		IsMaster:        isMaster,
		Token:           masterNode.Token,
		JavaLogServerId: masterNode.JavaLogServerId,
		JavaLogGameId:   masterNode.JavaLogGameId,
		ClientVer:       masterNode.ClientVer,
	}
	return startNode
}

func (mgr *RunningNodeManager) GetRunningNode(sid int64, id int64, plt string, ch string) *models.RunningGame {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	return mgr.getRunningNode(sid, id, plt, ch)
}
func (mgr *RunningNodeManager) getRunningNode(sid int64, id int64, plt string, ch string) *models.RunningGame {
	if mgr.RunningMap[plt] == nil {
		return nil
	}
	if mgr.RunningMap[plt][ch] == nil {
		return nil
	}
	if mgr.RunningMap[plt][ch][sid] == nil {
		return nil
	}
	return mgr.RunningMap[plt][ch][sid][id]
}

func (mgr *RunningNodeManager) addRunningNode(node *models.RunningGame) {
	mgr.initMap(node.Sid, node.Platform, node.Channel)

	mgr.RunningMap[node.Platform][node.Channel][node.Sid][node.Id] = node
	node.Updated = time.Now()

	nodeInfo, err := gNodeInfoMgr.GetNodeInfo(node.Sid, node.Platform, node.Channel)
	if err != nil {
		logs.Error("GetNodeInfo err:", err)
		return
	}
	if nodeInfo == nil {
		logs.Warningf("nodeInfo == nil, plt: %s, ch: %s", node.Platform, node.Channel)
		return
	}
}

func (mgr *RunningNodeManager) PingFromGame(
	sid int64,
	plt string,
	ch string,
	id int64,
	isMaster bool,
	port int32,
	adminHost string,
	online int32,
	slaveListStr string,
) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	running := mgr.getRunningNode(sid, id, plt, ch)
	now := time.Now()
	serverNode, err := gNodeInfoMgr.GetNodeInfo(sid, plt, ch)
	if err != nil {
		return err
	}
	if running == nil {
		// 第一次 ping
		running = mgr.newStartingNode(serverNode, id, isMaster, adminHost, port)
		mgr.addRunningNode(running)
	} else {
		running.State = serverNode.State
	}

	running.Online = online
	running.Updated = now

	_, err = gOrmMgr.Orm.InsertOrUpdate(running)
	if err != nil {
		return err
	}

	logs.Infof("PingFromGame: %+v", running)

	// slave
	var slaveList []int64
	err = json.Unmarshal([]byte(slaveListStr), &slaveList)
	if err != nil {
		return err
	}
	for _, slaveSid := range slaveList {

		slaveRun := mgr.getRunningNode(slaveSid, slaveSid, plt, ch)
		slaveNode, _ := gNodeInfoMgr.GetNodeInfo(slaveSid, plt, ch)

		if slaveRun == nil {
			// 第一次 ping
			slaveRun = &models.RunningGame{}
			*slaveRun = *running
			slaveRun.Sid = slaveSid
			slaveRun.Id = slaveSid
			mgr.addRunningNode(slaveRun)
		} else {
			slaveRun.State = serverNode.State
		}
		if slaveNode != nil {
			slaveRun.JavaLogGameId = slaveNode.JavaLogGameId
			slaveRun.JavaLogServerId = slaveNode.JavaLogServerId
			slaveRun.Name = slaveNode.Name
		}
		slaveRun.Online = online
		slaveRun.Updated = now

		_, err = gOrmMgr.Orm.InsertOrUpdate(slaveRun)
		if err != nil {
			return err
		}
	}
	return nil
}

var selnum int

func (mgr *RunningNodeManager) SelectRunning(sid int64, platform string, channel string, clientVer string) (*models.RunningGame, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	if mgr.RunningMap[platform] == nil {
		return nil, fmt.Errorf("platform not found, platform:%s", platform)
	}
	if mgr.RunningMap[platform][channel] == nil {
		channel = platform
	}
	if mgr.RunningMap[platform][channel] == nil {
		return nil, fmt.Errorf("channel not found, channel:%s", channel)
	}
	logs.Infof("SelectRunning ", sid, platform, channel, clientVer)
	nodeMap := mgr.RunningMap[platform][channel]
	var selected *models.RunningGame
	if sid == 0 {
		var newest *models.RunningGame
		var sels []*models.RunningGame
		for _, ns := range nodeMap {
			for _, n := range ns {
				if newest == nil || n.Created.Unix() >= newest.Created.Unix() {
					newest = n
				}
				if n.State == models.NODE_STATE_TYPE_HOT && n.Online < 5000 {
					sels = append(sels, n)
				}
			}
		}
		if len(sels) > 0 {
			selected = sels[selnum%len(sels)]
			selnum++
			if selnum == len(sels) {
				selnum = 0
			}
		}
		if selected == nil {
			selected = newest
		}
		if selected == nil {
			return nil, fmt.Errorf("服务器正在维护中，关注最新进度请加Q群：657424272")
		}
		if clientVer != "" && mgr.complateVer(clientVer, selected.ClientVer) > 0 {
			return mgr.findTest(platform, channel)
		}
		return selected, nil
	} else {

		for _, n := range nodeMap[sid] {
			if selected == nil || selected.Online > n.Online {
				selected = n
			}
		}
		if selected == nil {
			return nil, fmt.Errorf("服务器正在维护中，关注最新进度请加Q群：657424272")
		}
		if clientVer != "" && mgr.complateVer(clientVer, selected.ClientVer) > 0 {
			return mgr.findTest(platform, channel)
		}
		return selected, nil
	}

}

func (mgr *RunningNodeManager) findTest(platform string, channel string) (*models.RunningGame, error) {
	if mgr.RunningMap[platform] == nil {
		return nil, fmt.Errorf("platform not found, platform:%s", platform)
	}
	if mgr.RunningMap[platform][channel] == nil {
		return nil, fmt.Errorf("channel not found, channel: %s", channel)
	}

	nodeMap := mgr.RunningMap[platform][channel]
	for _, n := range nodeMap[999] {
		return n, nil
	}
	return nil, fmt.Errorf("not server to find")
}

func (mgr *RunningNodeManager) IsRepair(serverNode *models.NodeInfo) bool {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	if mgr.RunningMap[serverNode.Platform] == nil {
		return true
	}
	if mgr.RunningMap[serverNode.Platform][serverNode.Channel] == nil {
		return true
	}
	if len(mgr.RunningMap[serverNode.Platform][serverNode.Channel][serverNode.Sid]) == 0 {
		return true
	}
	return false
}

func (mgr *RunningNodeManager) CheckServerPing() {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	now := time.Now()
	for _, chMap := range mgr.RunningMap {
		for _, sidMap := range chMap {
			for _, idMap := range sidMap {
				for id, n := range idMap {
					if now.Sub(n.Updated) >= time.Minute*2 {
						_, _ = gOrmMgr.Orm.Delete(n)
						delete(idMap, id)
					}
				}
			}
		}
	}
}

func (mgr *RunningNodeManager) Range(f func(*models.RunningGame) bool) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	now := time.Now()
	for _, chMap := range mgr.RunningMap {
		for _, sidMap := range chMap {
			for _, idMap := range sidMap {
				for _, n := range idMap {
					if now.Sub(n.Updated) < time.Minute*2 {
						if !f(n) {
							return
						}
					}
				}
			}
		}
	}
}

func (mgr *RunningNodeManager) complateVer(v1 string, v2 string) int32 {
	s1 := strings.Split(v1, ".")
	s2 := strings.Split(v2, ".")
	a1, b1, c1 := util.AtoInt64(s1[0]), util.AtoInt64(s1[1]), util.AtoInt64(s1[2])
	a2, b2, c2 := util.AtoInt64(s2[0]), util.AtoInt64(s2[1]), util.AtoInt64(s2[2])
	if a1 > a2 {
		return 1
	} else if a1 == a2 {
		if b1 > b2 {
			return 1
		} else if b1 == b2 {
			if c1 > c2 {
				return 1
			} else if c1 == c2 {
				return 0
			} else {
				return -1
			}
		} else {
			return -1
		}
	} else {
		return -1
	}
}
