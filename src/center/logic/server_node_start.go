package logic

import (
	"fmt"
	"strings"

	"goproject/engine/logs"
	"goproject/src/center/models"
)

func (mgr *ServerManager) getServerNodeCopy(masterNode *models.NodeInfo, id int64, isMaster bool, adminHost string, port int32) *models.NodeInfo {
	startNode := masterNode.Copy()
	startNode.Id = id
	ss := strings.Split(adminHost, ":")
	startNode.ClientUrl = fmt.Sprintf("%s:%d", strings.Trim(ss[1], "//"), port)
	startNode.Port = port
	startNode.AdminHost = adminHost
	startNode.IsMaster = isMaster
	return startNode
}

func (mgr *ServerManager) getServerStarCmd(sid int64, id int64, plt string, ch string, isMaster bool, adminHost string, port int32) (*models.NodeInfo, error) {

	masterNode, err := mgr.getNodeInfo(sid, plt, ch)
	if err != nil {
		return nil, err
	}
	if masterNode == nil {
		return nil, fmt.Errorf("server not set, sid:%d, plt:%s, ch:%s", sid, plt, ch)
	}
	startNode := mgr.getServerNodeCopy(masterNode, id, isMaster, adminHost, port)

	logs.Infof("getServerStarCmd: %+v", startNode)

	return startNode, nil
}

func (mgr *ServerManager) tryStartServerNodes() {
	waitList := mgr.WaitStartMap

	logs.Infof("tryStartServerNodes, wait len=%d", len(waitList))

	for key, n := range waitList {
		startAdminNode := mgr.findCanStartAdminNode()
		if startAdminNode == nil {
			logs.Warn("admin not enough")
			break
		}
		err := mgr.tryStartGameServerToAdmin(startAdminNode, n, len(n.NodeMap) == 0)
		if err != nil {
			logs.Error(err)
			continue
		}
		delete(waitList, key)
	}
}
