package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"

	"goproject/engine/logs"
	"goproject/src/center/models"
)

type InvitateManager struct {
}

var gInvitateMgr *InvitateManager

func init() {
	gInvitateMgr = &InvitateManager{}
}

func GetInvitateManager() *InvitateManager {
	return gInvitateMgr
}

// roleId 未被邀请方
func (mgr *InvitateManager) BeInvitate(
	roleId int64, roleName string, roleAvater string,
	invitaterRoleId int64, invitaterSid int64,
	plt string, ch string) error {

	runningNode := gRunningNodeMgr.GetRunningNode(invitaterSid, invitaterSid, plt, ch)

	clientUrl := runningNode.ClientUrl
	var url string
	if strings.Index(clientUrl, "wss") == 0 {
		url = strings.Replace(clientUrl, "wss", "https", 1)
	} else {
		url = strings.Replace(clientUrl, "ws", "http", 1)
	}
	url = strings.TrimRight(url, "ws")
	url = url + fmt.Sprintf("GmFunc?func=Invite&role_id=%d&inviter_roleId=%d&inviter_avater=%s&inviter_name=%s&token=unclemiao",
		invitaterRoleId, roleId, roleAvater, roleName)
	logs.Debugf("send to :%s", url)
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		logs.Error(err)
		return err
	}
	gameResp := &models.HttpResponse{}
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Errorf("ReadAll err:%v", err)
		return err
	}
	err = json.Unmarshal(bs, gameResp)
	if err != nil {
		logs.Errorf("Unmarshal err:%v", err)
		return err
	}
	if gameResp.Status != 0 {
		logs.Errorf("gameResp err:%v", gameResp.ErrMsg)
		return fmt.Errorf("gameResp err:%s", gameResp.ErrMsg)
	}

	return nil
}
