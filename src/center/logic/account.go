package logic

import (
	"fmt"

	"goproject/src/center/models"
)

func (mgr *ServerManager) NewAccount(name string, pass string, plt string, ch string) (*models.Account, error) {
	account := &models.Account{
		Name:     name,
		Pass:     pass,
		Platform: plt,
		Channel:  ch,
	}
	orm := mgr.OrmMgr.Orm
	_, err := orm.Insert(account)
	if err != nil {
		return nil, err
	}
	return account, nil
}

func (mgr *ServerManager) GetAccount(name string, pass string, plt string, ch string) (*models.Account, error) {
	account := &models.Account{
		Name:     name,
		Pass:     pass,
		Platform: plt,
		Channel:  ch,
	}
	orm := mgr.OrmMgr.Orm
	_, _, err := orm.ReadOrCreate(account, "name", "platform", "channel")
	if err != nil {
		return nil, err
	}
	if account.Tag == 1 {
		return nil, fmt.Errorf("账号已禁用，请联系客服")
	}
	return account, nil
}

func (mgr *ServerManager) GetAccountById(id int64) (*models.Account, error) {
	account := &models.Account{
		Id: id,
	}
	orm := mgr.OrmMgr.Orm
	err := orm.Read(account)
	if err != nil {
		return nil, err
	}
	if account.Name == "" {
		return nil, fmt.Errorf("acount not found")
	}
	return account, nil
}

func (mgr *ServerManager) UpdateAccount(account *models.Account) error {

	orm := mgr.OrmMgr.Orm
	_, err := orm.Update(account)
	return err
}
