package logic

import (
	"goproject/src/center/models"
)

func (mgr *ServerManager) SetExcelVersion(fileName string, url string) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	_, err := mgr.OrmMgr.Orm.Insert(&models.ExcelVer{
		Version: fileName,
		OssUrl:  url,
	})
	return err
}

func (mgr *ServerManager) GetExcelVersion(fileName string, url string) (*models.ExcelVer, error) {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	ret := &models.ExcelVer{
		Version: fileName,
	}
	err := mgr.OrmMgr.Orm.Read(&ret, "Version")
	return ret, err
}
