package logic

import (
	"fmt"
	"sync"

	"goproject/src/center/models"
)

type NoticeManager struct {
	Lock sync.RWMutex
}

var gNoticeMgr *NoticeManager

func init() {
	gNoticeMgr = &NoticeManager{}
}

func GetNoticeManager() *NoticeManager {
	return gNoticeMgr
}

func (mgr *NoticeManager) noticeBy(sid int64, plt string, ch string) (*models.GameNotice, error) {
	info := &models.GameNotice{
		Sid:      sid,
		Platform: plt,
		Channel:  ch,
	}

	err := gOrmMgr.Orm.Read(info, "sid", "platform", "channel")
	if err != nil {
		return nil, err
	}
	if info.Id == 0 {
		return nil, fmt.Errorf("node_info not found, sid:%d, plt:%s, ch:%s", sid, plt, ch)
	}

	return info, err
}

func (mgr *NoticeManager) setNotice(info *models.GameNotice) error {
	_, err := gOrmMgr.Orm.InsertOrUpdate(info, "sid", "platform", "channel")
	return err
}

func (mgr *NoticeManager) delNotice(info *models.GameNotice) error {
	_, err := gOrmMgr.Orm.Delete(info, "sid", "platform", "channel")
	return err
}

func (mgr *NoticeManager) GetNotice(sid int64, plt string, ch string) (*models.GameNotice, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	return mgr.noticeBy(sid, plt, ch)
}

func (mgr *NoticeManager) GetNoticeList(sid int64, plt string, ch string) ([]*models.GameNotice, error) {
	mgr.Lock.RLock()
	defer mgr.Lock.RUnlock()
	var list []*models.GameNotice
	orm := gOrmMgr.Orm
	queryTable := orm.QueryTable("node_info")

	if sid != 0 {
		queryTable = queryTable.Filter("sid", sid)
	}
	if plt != "" {
		queryTable = queryTable.Filter("platform", plt)
	}
	if ch != "" {
		queryTable = queryTable.Filter("channel", ch)
	}
	_, err := queryTable.All(&list)
	if err != nil {
		return nil, err
	}
	return list, nil
}

func (mgr *NoticeManager) SetNotice(info *models.GameNotice) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	return mgr.setNotice(info)
}

func (mgr *NoticeManager) DelNotice(info *models.GameNotice) error {
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	return mgr.delNotice(info)
}
