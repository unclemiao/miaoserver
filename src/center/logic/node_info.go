package logic

import (
	"encoding/json"
	"fmt"
	"time"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/models"
)

var gNodeInfoMgr *NodeInfoManager

type NodeInfoManager struct {
}

func init() {
	gNodeInfoMgr = &NodeInfoManager{}
}

func GetNodeInfoMgr() *NodeInfoManager {
	return gNodeInfoMgr
}

func (mgr *NodeInfoManager) nodeInfoBy(sid int64, plt string, ch string) (*models.NodeInfo, error) {

	nodeInfo := &models.NodeInfo{
		Sid:      sid,
		Platform: plt,
		Channel:  ch,
	}
	err := gOrmMgr.Orm.Read(nodeInfo, "sid", "platform", "channel")
	if err != nil {
		return nil, err
	}
	if nodeInfo.Id == 0 {
		return nil, fmt.Errorf("node_info not found, sid:%d, plt:%s, ch:%s", sid, plt, ch)
	}
	if nodeInfo.SlaveStr == "" {
		nodeInfo.SlaveStr = "[]"
	}
	err = json.Unmarshal([]byte(nodeInfo.SlaveStr), &nodeInfo.SlaveList)
	if err != nil {
		return nil, err
	}
	return nodeInfo, err
}

func (mgr *NodeInfoManager) GetNodeInfo(sid int64, plt string, ch string) (*models.NodeInfo, error) {
	return mgr.nodeInfoBy(sid, plt, ch)
}

func (mgr *NodeInfoManager) GetAllNodeInfo(plt string, ch string) ([]*models.NodeInfo, error) {
	var nodeList []*models.NodeInfo
	_, err := gOrmMgr.Orm.QueryTable("node_info").Filter("platform", plt).Filter("channel", ch).All(&nodeList)
	if err != nil {
		return nil, err
	}
	return nodeList, err
}

func (mgr *NodeInfoManager) SetServerNode(s *models.NodeInfo) error {
	logs.Infof("SetServerNode %+v", s)

	if s.Platform != "test" &&
		s.Platform != "kesheng-apk" &&
		s.Platform != "kesheng-ios" &&
		s.Platform != "ov" {

		s.CenterUrl = util.ChoiceStr(s.CenterUrl == "", fmt.Sprintf("https://%s-master-center-%s/konglong2", s.Platform, AppFlag.Host), s.CenterUrl)
		if s.Sid == 999 {
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game-%s/game%03d/ws", s.Platform, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin-%s/admin%03d", s.Platform, AppFlag.Host, s.Sid), s.AdminHost)
		} else if s.Sid > 45 {
			n := (s.Sid - 1) / 45
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game%d-%s/game%03d/ws", s.Platform, n, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin%d-%s/admin%03d", s.Platform, n, AppFlag.Host, s.Sid), s.AdminHost)
		} else {
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game-%s/game%03d/ws", s.Platform, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin-%s/admin%03d", s.Platform, AppFlag.Host, s.Sid), s.AdminHost)
		}
	} else if s.Platform == "ov" {
		s.CenterUrl = util.ChoiceStr(s.CenterUrl == "", fmt.Sprintf("https://%s-master-center-%s/konglong2", s.Platform, AppFlag.Host), s.CenterUrl)
		if s.Sid == 999 {
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game-%s/game%03d/ws", s.Platform, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin-%s/admin%03d", s.Platform, AppFlag.Host, s.Sid), s.AdminHost)
		} else if s.Sid > 45 {
			n := (s.Sid - 1) / 45
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game%d-%s/game%03d/ws", s.Platform, n, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin%d-%s/admin%03d", s.Platform, n, AppFlag.Host, s.Sid), s.AdminHost)
		} else {
			s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game-%s/game%03d/ws", s.Platform, AppFlag.Host, s.Sid), s.ClientUrl)
			s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin-%s/admin%03d", s.Platform, AppFlag.Host, s.Sid), s.AdminHost)
		}
	} else if s.Platform == "kesheng-apk" || s.Platform == "kesheng-ios" {
		s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://%s-game%03d.%s/game%03d/ws", s.Platform, s.Sid, AppFlag.Host, s.Sid), s.ClientUrl)
		s.CenterUrl = util.ChoiceStr(s.CenterUrl == "", fmt.Sprintf("https://%s-master-center.%s/konglong2", s.Platform, AppFlag.Host), s.CenterUrl)
		s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://%s-admin%03d.%s/admin%03d", s.Platform, s.Sid, AppFlag.Host, s.Sid), s.AdminHost)
	} else if s.Platform == "test" {
		s.ClientUrl = util.ChoiceStr(s.ClientUrl == "", fmt.Sprintf("wss://dev-klgo.qcinterfacet.com/game%03d/ws", s.Sid), s.ClientUrl)
		s.CenterUrl = "https://weixin-center-kl2.xdjkj666.com/konglong2"
		s.AdminHost = util.ChoiceStr(s.AdminHost == "", fmt.Sprintf("https://dev-admin-klgo.qcinterfacet.com/admin%03d", s.Sid), s.AdminHost)

		s.SqlRoot = util.ChoiceStr(s.SqlRoot == "", "game", s.SqlRoot)
		s.SqlPass = util.ChoiceStr(s.SqlPass == "", "Mima!23s", s.SqlPass)
	}
	s.SqlName = util.ChoiceStr(s.SqlName == "", fmt.Sprintf("konglong%03d", s.Sid), s.SqlName)
	s.Port = 30000 + int32(s.Sid)
	if s.SqlHost == "" {
		s.SqlHost = "weixin-mysql-kl2.qcinterfacet.com"
	}
	if s.SqlRoot == "" {
		s.SqlRoot = "game"
	}
	if s.SqlPass == "" {
		s.SqlPass = "Mima!23s"
	}
	if s.InitTime.IsZero() {
		s.InitTime = time.Date(1970, 1, 1, 0, 0, 0, 0, time.UTC)
	}
	if s.SlaveStr == "" {
		s.SlaveStr = "[]"
	}

	id, err := gOrmMgr.Orm.InsertOrUpdate(s, "sid", "platform", "channel")
	if err != nil {
		return err
	}
	if s.Id == 0 {
		s.Id = id
	}
	return nil
}

func (mgr *NodeInfoManager) DeleteServerNode(s *models.NodeInfo) error {
	_, err := gOrmMgr.Orm.Delete(s, "sid", "platform", "channel")
	if err != nil {
		return err
	}
	return nil
}
