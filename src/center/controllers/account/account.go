package account

import (
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type LoginController struct {
	beego.Controller
}

func (node *LoginController) Post() {
	var (
		sid, _       = node.GetInt64("sid", 0)
		name         = node.GetString("name")          // 账号名
		jsCode       = node.GetString("js_code")       // wx登录id
		pass         = node.GetString("pass")          // 密码
		plt          = node.GetString("platform")      // 平台
		ch           = node.GetString("channel")       // 渠道
		due          = node.GetString("due")           // token 到期时间 秒
		weiyingToken = node.GetString("weiying_token") // 本平台的token
		clientVer    = node.GetString("clientVer")     // 客户端版本号
		serverMgr    = logic.GetServerManager()
		resp         = &models.Response{Code: 200}
		accountResp  = &models.AccountResponse{}
	)
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	if jsCode != "" {
		beginT := time_helper.NowMill()
		openId, err := node.loginPlatform(jsCode, plt, ch)
		endT := time_helper.NowMill()
		if endT-beginT >= 1000 {
			logs.Warnf("loginPlatform need: %d ms", endT-beginT)
		}
		if controllers.ParamsErr(err != nil, resp, err, 501) {
			return
		}
		name = openId
		pass = openId

	} else {
		md5 := util.Md5(fmt.Sprintf("-_-!!!%s%s%s%s%s%s", name, pass, plt, ch, due, serverMgr.Server.Token))
		if strings.Compare(md5, weiyingToken) != 0 {
			resp.Err = "weiying_token err"
			resp.Code = 501
			logs.Error(resp.Err)
			return
		}
	}

	account, err := serverMgr.GetAccount(name, pass, plt, ch)
	if err != nil {
		resp.Err = err.Error()
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}

	accountResp.AccountId = fmt.Sprintf("weiying_%d", account.Id)
	accountResp.Due = time_helper.NowSec() + time_helper.Hour*4
	accountResp.OpenId = name
	if sid > 0 {
		accountResp.ServerId = sid
	} else {
		accountResp.ServerId = account.LastServerId
	}
	var selectNode *models.RunningGame

	if logic.AppFlag.Master {
		selectNode, err = logic.GetRunningMgr().SelectRunning(accountResp.ServerId, plt, ch, clientVer)
		if err != nil && accountResp.ServerId == 999 {
			selectNode, err = logic.GetRunningMgr().SelectRunning(0, plt, ch, clientVer)
			if controllers.ParamsErr(err != nil, resp, err, 501) ||
				controllers.ParamsErr(selectNode == nil, resp, "服务器正在维护中，关注最新进度请加Q群：657424272", 501) {
				return
			}
		} else {
			if controllers.ParamsErr(err != nil, resp, err, 501) ||
				controllers.ParamsErr(selectNode == nil, resp, "服务器正在维护中，关注最新进度请加Q群：657424272", 501) {
				return
			}
		}

	} else {
		selectNode, err = node.SelectRunning(accountResp.ServerId, plt, ch, clientVer)
		if err != nil && accountResp.ServerId == 999 {
			selectNode, err = logic.GetRunningMgr().SelectRunning(0, plt, ch, clientVer)
			if controllers.ParamsErr(err != nil, resp, err, 501) ||
				controllers.ParamsErr(selectNode == nil, resp, "服务器正在维护中，关注最新进度请加Q群：657424272", 501) {
				return
			}
			return
		} else {
			if controllers.ParamsErr(err != nil, resp, err, 501) ||
				controllers.ParamsErr(selectNode == nil, resp, "服务器正在维护中，关注最新进度请加Q群：657424272", 501) {
				return
			}
		}
	}

	accountResp.ServerId = selectNode.Sid

	accountResp.Server = selectNode.GetResponse()
	// if clientVer != selectNode.ClientVer {
	// 	accountResp.Server.Sid = 998
	// 	accountResp.Server.Platform = "test"
	// 	accountResp.Server.Channel = "weiying"
	// 	accountResp.Server.ClientUrl = "wss://dev-klgo.qcinterfacet.com/game998/ws"
	// }

	accountResp.Token = util.Md5(fmt.Sprintf("@_@!!!%s%d%s%s%s",
		accountResp.AccountId, accountResp.Due, plt, ch, selectNode.Token,
	))

	resp.Data = accountResp

	account.LastServerId = accountResp.ServerId
	err = serverMgr.UpdateAccount(account)
	if err != nil {
		resp.Err = "UpdateAccount err: " + err.Error()
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}

}

func (node *LoginController) Get() {

}

func (node *LoginController) SelectRunning(sid int64, plt string, ch string, clientVer string) (*models.RunningGame, error) {
	host := logic.AppFlag.MasterUrl + "/server/select?"
	form := make(url.Values)
	form.Set("sid", fmt.Sprintf("%d", sid))
	form.Set("platform", plt)
	form.Set("channel", ch)
	form.Set("clientVer", clientVer)
	form.Encode()
	logs.Infof("send: %s", host+form.Encode())
	resp, err := http.Get(host + form.Encode())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	response := &models.SelectNodeResponse{}
	err = json.Unmarshal(bs, &response)
	if err != nil {
		return nil, err
	}
	return response.Data, nil
}

func (node *LoginController) loginPlatform(jsCode string, platform string, channel string) (string, error) {

	switch platform {
	case "weixin":
		return node.loginWx(jsCode)
	case "ov":
		if channel == "vivo" {
			return node.loginViVo(jsCode)
		} else if channel == "oppo" {
			return node.loginOppo(jsCode)
		}

	case "xiaomi":
		return node.loginWx(jsCode)
	case "test":
		switch channel {
		case "weiying":
			return node.loginWx(jsCode)
		case "weixin":
			return node.loginWx(jsCode)
		case "vivo":
			return node.loginViVo(jsCode)
		case "oppo":
			return node.loginOppo(jsCode)
		case "xiaomi":
			return node.loginWx(jsCode)
		}
	}
	return "", fmt.Errorf("platform err %s", platform)
}

func (node *LoginController) loginWx(jsCode string) (string, error) {
	form := make(url.Values)
	form.Set("appid", "wx7af5401255a73f75")
	form.Set("secret", "6089deddd919b4acd29b048308545861")
	form.Set("js_code", jsCode)
	form.Set("grant_type", "authorization_code")

	urlstr := "https://api.weixin.qq.com/sns/jscode2session?" + form.Encode()

	wxResp, err := http.Get(urlstr)
	if err != nil {
		return "", err
	}
	defer wxResp.Body.Close()
	bs, err := ioutil.ReadAll(wxResp.Body)
	if err != nil {
		return "", err
	}

	respData := &models.WxLoginResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		return "", err
	}
	if respData.Errcode != 0 {
		return "", fmt.Errorf("%s", respData.Errmsg)
	}

	return respData.Openid, nil
}

func (node *LoginController) loginViVo(jsCode string) (string, error) {
	nowSec := time_helper.NowSec()
	form := make(url.Values)
	form.Set("pkgName", "com.iuno.hydpd.vivominigame")
	form.Set("signature", "6089deddd919b4acd29b048308545861")
	form.Set("token", jsCode)
	form.Set("nonce", fmt.Sprintf("%d", nowSec))
	form.Set("timestamp", fmt.Sprintf("%d", nowSec*1000))

	tokenForm := make(url.Values)
	tokenForm.Add("appKey", "afe28ddd3aa1a2434034b26a6fe87ebd")
	tokenForm.Add("appSecret", "684d020fb03d1b708475aac6dc747f18")
	tokenForm.Add("nonce", fmt.Sprintf("%d", nowSec))
	tokenForm.Add("pkgName", "com.iuno.hydpd.vivominigame")
	tokenForm.Add("timestamp", fmt.Sprintf("%d", nowSec*1000))
	tokenForm.Add("token", jsCode)
	tokenFormUrl := tokenForm.Encode()

	h := sha256.New()
	h.Write([]byte(tokenFormUrl))
	bs1 := h.Sum(nil)
	str := fmt.Sprintf("%x", bs1)

	form.Set("signature", str)

	urlstr := "https://quickgame.vivo.com.cn/api/quickgame/cp/account/userInfo?" + form.Encode()
	wxResp, err := http.Get(urlstr)
	if err != nil {
		return "", err
	}
	defer wxResp.Body.Close()
	bs, err := ioutil.ReadAll(wxResp.Body)

	if err != nil {
		return "", err
	}

	respData := &models.VivoLoginResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		return "", err
	}
	if respData.Code != 0 {
		return "", fmt.Errorf("%s", respData.Msg)
	}

	return respData.Data.Openid, nil
}

func (node *LoginController) loginOppo(jsCode string) (string, error) {
	nowSec := time_helper.NowSec()
	form := make(url.Values)
	form.Set("pkgName", "com.iuno.hydpd.nearme.gamecenter")
	form.Set("token", jsCode)
	form.Set("timeStamp", fmt.Sprintf("%d", nowSec*1000))

	// 加密签名
	tokenForm := make(url.Values)
	tokenForm.Add("pkgName", "com.iuno.hydpd.nearme.gamecenter")
	tokenForm.Add("appKey", "5twAqB6QZaoSg4SgS08Ww0k4g")
	tokenForm.Add("appSecret", "Ee453872ff13c213735eBD3F12061616")
	tokenForm.Add("token", jsCode)
	tokenForm.Add("timeStamp", fmt.Sprintf("%d", nowSec*1000))
	tokenFormUrl := tokenForm.Encode()
	str := strings.ToTitle(util.Md5(tokenFormUrl))

	form.Set("sign", str)

	form.Set("version", "1.0.0")

	urlstr := "https://play.open.oppomobile.com/instant-game-open/userInfo?" + form.Encode()
	wxResp, err := http.Get(urlstr)
	if err != nil {
		return "", err
	}
	defer wxResp.Body.Close()
	bs, err := ioutil.ReadAll(wxResp.Body)
	if err != nil {
		return "", err
	}
	respData := &models.OppoLoginResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		return "", err
	}
	if respData.ErrCode != 200 {
		return "", fmt.Errorf("%s", respData.ErrMsg)
	}
	return respData.UserInfo.UserId, nil
}
