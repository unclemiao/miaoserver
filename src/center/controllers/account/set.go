package account

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type SetController struct {
	beego.Controller
}

func (node *SetController) Post() {
	var (
		name = node.GetString("name")     // 账号名
		plt  = node.GetString("platform") // 平台
		ch   = node.GetString("channel")  // 渠道
		resp = &models.Response{Code: 200}
	)
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	account := &models.Account{
		Name:     name,
		Platform: plt,
		Channel:  ch,
	}
	orm := logic.GetServerManager().OrmMgr.Orm
	err := orm.Read(account, "name", "platform", "channel")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	account.Tag = 1
	_, err = orm.Update(account, "tag")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
}
