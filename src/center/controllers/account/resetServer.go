package account

import (
	"fmt"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type ResetSidController struct {
	beego.Controller
}

func (node *ResetSidController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	var (
		idstr     = node.GetString("accountId")
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		md5       = util.Md5(fmt.Sprintf("-_-!!!%s%s%s%s%s", idstr, plt, ch, due, serverMgr.Server.Token))
	)

	if strings.Compare(md5, token) != 0 {
		resp.Err = "weiying_token err"
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}

	id := util.AtoInt64(strings.TrimLeft(idstr, "weiying_"))
	account, err := serverMgr.GetAccountById(id)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	account.LastServerId = 0

	err = serverMgr.UpdateAccount(account)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
}
