package excel

import (
	"bytes"
	"io/ioutil"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/sdk"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type UpLoadController struct {
	beego.Controller
}

func (node *UpLoadController) Get() {
}

func (node *UpLoadController) Post() {
	logs.Info("excel Post")

	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		f, h, err = node.GetFile("uploadfilename")
	)

	if controllers.ParamsErr(err != nil, resp, "get file err", 501) {
		return
	}

	defer f.Close()

	allByte, e := ioutil.ReadAll(f)
	if controllers.ParamsErr(e != nil, resp, "ReadAll err", 501) {
		return
	}
	buff := bytes.NewBuffer(allByte)

	_, err2 := sdk.UpLoadToHuaWeiOSS("konglong2", h.Filename, buff)
	if controllers.ParamsErr(err2 != nil, resp, "UpLoadToHuaWeiOSS err", 501) {
		return
	}
	logic.GetServerManager().SetExcelVersion(h.Filename, "https://konglong2.obs.cn-south-1.myhuaweicloud.com/"+h.Filename)

}
