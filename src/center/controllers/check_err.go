package controllers

import (
	"goproject/engine/logs"
)

type RespItf interface {
	SetErr(string)
	SetCode(int32)
	GetErr() string
}

func ParamsErr(cond bool, resp RespItf, err interface{}, code int32) bool {

	if cond {
		errMsg, ok := err.(string)
		if ok {
			resp.SetErr(errMsg)
		} else {
			err2, _ := err.(error)
			resp.SetErr(err2.Error())
		}
		resp.SetCode(code)
		logs.Error(resp.GetErr())
	}
	return cond
}
