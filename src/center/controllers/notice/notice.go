package notice

import (
	"bytes"
	"encoding/json"
	"fmt"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/sdk"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type Controller struct {
	beego.Controller
}

// 设置服务器公告
func (node *Controller) Post() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		s         = &models.GameNotice{}
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		SidList   = node.GetString("sidList", "0")
		serverMgr = logic.GetServerManager()
		noticeMgr = logic.GetNoticeManager()
		data      = node.GetString("data")
	)

	_ = json.Unmarshal([]byte(data), s)

	if controllers.ParamsErr(s.Platform == "", resp, "platform err", 501) ||
		controllers.ParamsErr(s.Channel == "", resp, "channel err", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", s.Sid, s.Platform, s.Channel, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	// 构造公告文件
	noticeJson := models.NoticeJson{
		Title:   s.Title,
		Msg:     s.Title,
		Version: time_helper.NowMill(),
	}

	bs, err := json.Marshal(noticeJson)
	if controllers.ParamsErr(err != nil, resp, "noticeJson Marshal err", 501) {
		return
	}
	txt := append([]byte("<?xml version='1.0' encoding='UTF-8'?>\n"), bs...)
	buff := bytes.NewBuffer(txt)

	strs := strings.Split(SidList, "-")
	if len(strs) > 1 {
		sid0 := util.AtoInt64(strs[0])
		sid9 := util.AtoInt64(strs[1])

		for i := sid0; i <= sid9; i++ {
			fileName := fmt.Sprintf("notice-%d-%d", i, noticeJson.Version)
			_, err2 := sdk.UpLoadToHuaWeiOSS("konglong2-notice", fileName, buff)
			if controllers.ParamsErr(err2 != nil, resp, "UpLoadToHuaWeiOSS err", 501) {
				return
			}
			gameNotice := models.GameNotice{}
			gameNotice = *s
			gameNotice.Sid = i
			gameNotice.Url = "https://konglong2-notice.obs.cn-south-1.myhuaweicloud.com/" + fileName
			err := noticeMgr.SetNotice(&gameNotice)
			if controllers.ParamsErr(err != nil, resp, "SetNotice err", 501) {
				return
			}
		}
	} else {
		if SidList == "0" || SidList == "" {
			fileName := fmt.Sprintf("notice-%d-%d", 0, noticeJson.Version)
			_, err2 := sdk.UpLoadToHuaWeiOSS("konglong2-notice", fileName, buff)
			if controllers.ParamsErr(err2 != nil, resp, "UpLoadToHuaWeiOSS err", 501) {
				return
			}
			gameNotice := models.GameNotice{}
			gameNotice = *s
			gameNotice.Sid = 0
			gameNotice.Url = "https://konglong2-notice.obs.cn-south-1.myhuaweicloud.com/" + fileName
			err := noticeMgr.SetNotice(&gameNotice)
			if controllers.ParamsErr(err != nil, resp, "SetNotice err", 501) {
				return
			}
		} else {
			sid := util.AtoInt64(SidList)
			fileName := fmt.Sprintf("notice-%d-%d", sid, noticeJson.Version)
			_, err2 := sdk.UpLoadToHuaWeiOSS("konglong2-notice", fileName, buff)
			if controllers.ParamsErr(err2 != nil, resp, "UpLoadToHuaWeiOSS err", 501) {
				return
			}
			gameNotice := models.GameNotice{}
			gameNotice = *s
			gameNotice.Sid = 0
			gameNotice.Url = "https://konglong2-notice.obs.cn-south-1.myhuaweicloud.com/" + fileName
			err := noticeMgr.SetNotice(&gameNotice)
			if controllers.ParamsErr(err != nil, resp, "SetNotice err", 501) {
				return
			}
		}
	}

	resp.Data = s
}

func (node *Controller) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		noticeMgr = logic.GetNoticeManager()
		md5       = util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))
	)

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	list, err := noticeMgr.GetNoticeList(sid, plt, ch)

	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	resp.Data = list

}

func (node *Controller) Delete() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		noticeMgr = logic.GetNoticeManager()
	)

	if controllers.ParamsErr(sid == 0, resp, "sid err", 501) ||
		controllers.ParamsErr(plt == "", resp, "platform err", 501) ||
		controllers.ParamsErr(ch == "", resp, "channel err", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	old, _ := noticeMgr.GetNotice(sid, plt, ch)
	if controllers.ParamsErr(old == nil, resp, fmt.Errorf("notice not found, sid：%d, plt:%s, ch:%s", sid, plt, ch), 501) {
		return
	}

	err := noticeMgr.DelNotice(old)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

}
