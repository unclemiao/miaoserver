package info

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
)

type WSSERRController struct {
	beego.Controller
}

// 开服
func (node *WSSERRController) Post() {
	logs.Infof("Open: %s", node.GetString("data"))
}
