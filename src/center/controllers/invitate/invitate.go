package invitate

import (
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type Controller struct {
	beego.Controller
}

func (node *Controller) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		token              = node.GetString("weiyingToken", "*")
		due, _             = node.GetInt64("due", 0)
		roleId, _          = node.GetInt64("role_id", 0)
		roleName           = node.GetString("role_name", "")
		roleAvater         = node.GetString("role_avater", "")
		invitaterRoleId, _ = node.GetInt64("invitater_role_id", 0)
		invitaterSid, _    = node.GetInt64("invitater_sid", 0)
		platform           = node.GetString("platform", "")
		channel            = node.GetString("channel", "")
	)
	logs.Debugf(fmt.Sprintf("-_-!!!%d%d%d%d", roleId, invitaterRoleId, invitaterSid, due))
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%d%d%d", roleId, invitaterRoleId, invitaterSid, due))
	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	err := logic.GetInvitateManager().BeInvitate(roleId, roleName, roleAvater, invitaterRoleId, invitaterSid, platform, channel)
	if err != nil {
		resp.Code = 501
		resp.Err = err.Error()
		return
	}
}
