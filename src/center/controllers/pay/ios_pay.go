package pay

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type IosController struct {
	beego.Controller
}

type IosPost struct {
	OrderNo      string `json:"order_no"`
	Status       int32  `json:"status"`
	TotalFee     int32  `json:"total_fee"`
	Transmission string `json:"transmission"`
}

func (node *IosController) Post() {
	resp := &models.IosPayResponse{Result: "ok"}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	fromPostForm := &IosPost{}
	data := node.Ctx.Input.RequestBody

	err := json.Unmarshal(data, fromPostForm)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	var (
		// 服务器号
		ext      = fromPostForm.Transmission
		status   = fromPostForm.Status
		moneyFen = fromPostForm.TotalFee
		money    = util.MaxInt32(moneyFen/100, 1)
		bill     = fromPostForm.OrderNo
	)
	logs.Infof("node : %+v", fromPostForm)
	logs.Infof("status: %d, moneyFen: %d", status, moneyFen)
	logs.Debugf("transmission %s", ext)

	if status != 1 {
		return
	}
	iosExt := &models.IosStr{}
	err = json.Unmarshal([]byte(ext), iosExt)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	runningGame, err := logic.GetRunningMgr().SelectRunning(iosExt.Sid, iosExt.Platform, iosExt.Channel, "")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

	clientUrl := runningGame.ClientUrl
	var postUrl string
	if strings.Index(clientUrl, "wss") == 0 {
		postUrl = strings.Replace(clientUrl, "wss", "https", 1)
	} else {
		postUrl = strings.Replace(clientUrl, "ws", "http", 1)
	}
	postUrl = strings.TrimRight(postUrl, "ws")
	postUrl = postUrl + "pay_mall"
	postForm := make(url.Values)

	postForm.Set("sid", fmt.Sprintf("%d", iosExt.Sid))
	postForm.Set("account_id", iosExt.AccountId)
	postForm.Set("mall_id", fmt.Sprintf("%d", iosExt.MallId))
	postForm.Set("money", fmt.Sprintf("%d", money))
	postForm.Set("bill", bill)

	due := time_helper.NowSec() + time_helper.Hour
	postForm.Set("due", fmt.Sprintf("%d", due))

	// 	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%d%d%s%d%s", sid, accountId, mallId, money, bill, due, "s1yb8wa298be84e4c3c2a7901e47c350"))
	token := util.Md5(fmt.Sprintf("-_-!!!%d%s%d%d%s%d%s", iosExt.Sid, iosExt.AccountId, iosExt.MallId, money, bill, due, "s1yb8wa298be84e4c3c2a7901e47c350"))
	postForm.Set("token", token)
	gameResp, err := http.PostForm(postUrl, postForm)
	defer gameResp.Body.Close()
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	bs, err := ioutil.ReadAll(gameResp.Body)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	gameRespSt := &models.HttpResponse{}
	err = json.Unmarshal(bs, gameRespSt)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	if gameRespSt.Result != 200 {
		resp.Result = "fail"
		resp.Errmsg = gameRespSt.ErrMsg

	}
}
