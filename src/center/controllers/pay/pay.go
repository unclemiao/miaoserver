package pay

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type Controller struct {
	beego.Controller
}

func (node *Controller) Post() {
	resp := &models.HttpResponse{Status: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	var (
		// 服务器号
		sid, _ = node.GetInt64("sid")
		plt    = node.GetString("platform")
		ch     = node.GetString("channel")
	)

	runningGame, err := logic.GetRunningMgr().SelectRunning(sid, plt, ch, "")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

	clientUrl := runningGame.ClientUrl
	var postUrl string
	if strings.Index(clientUrl, "wss") == 0 {
		postUrl = strings.Replace(clientUrl, "wss", "https", 1)
	} else {
		postUrl = strings.Replace(clientUrl, "ws", "http", 1)
	}
	postUrl = strings.TrimRight(postUrl, "ws")
	postUrl = postUrl + "pay_mall"
	postForm := make(url.Values)

	postForm.Set("sid", node.GetString("sid"))
	postForm.Set("account_id", node.GetString("account_id"))
	postForm.Set("mall_id", node.GetString("mall_id"))
	postForm.Set("money", node.GetString("money"))
	postForm.Set("bill", node.GetString("bill"))
	postForm.Set("due", node.GetString("due"))
	postForm.Set("token", node.GetString("token"))
	gameResp, err := http.PostForm(postUrl, postForm)
	defer gameResp.Body.Close()
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	bs, err := ioutil.ReadAll(gameResp.Body)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	gameRespSt := &models.HttpResponse{}
	err = json.Unmarshal(bs, gameRespSt)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	*resp = *gameRespSt
}
