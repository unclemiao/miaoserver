package item

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	beego "github.com/beego/beego/v2/server/web"

	models2 "goproject/src/admin/models"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type GetItemResp struct {
	Code int32
	Err  string
	Data []*models2.Item
}
type Controller struct {
	beego.Controller
}

func (node *Controller) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	nodeInfo := &models.NodeInfo{
		Sid: 1,
	}

	err := logic.GetServerManager().OrmMgr.Orm.Read(nodeInfo, "sid")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	adminHost := "https://dev-admin-klgo.qcinterfacet.com/admin001"
	rep, err := http.Get(adminHost + "/item")
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	defer rep.Body.Close()

	bs, err := ioutil.ReadAll(rep.Body)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	itemresp := &GetItemResp{}
	err = json.Unmarshal(bs, itemresp)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	resp.Code = itemresp.Code
	resp.Err = itemresp.Err
	resp.Data = itemresp.Data
	return
}
