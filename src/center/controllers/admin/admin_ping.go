package admin

import (
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type AdminPingController struct {
	beego.Controller
}

func (c *AdminPingController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		c.Data["json"] = resp
		_ = c.ServeJSON()
	}()
	var (
		adminHost = c.GetString("host", "")
		due, _    = c.GetInt64("due", 0)
		max, _    = c.GetInt32("nodeMax", 1)
		token     = c.GetString("weiyingToken", "")
		serverMgr = logic.GetServerManager()
	)
	logs.Info("admin ping", adminHost, due, serverMgr.Server.Token)
	md5 := util.Md5(fmt.Sprintf("@_@!!!%s%d%s", adminHost, due, serverMgr.Server.Token))
	if token != md5 {
		resp.Err = "token err"
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}

	serverMgr.UpdateAdmin(adminHost, max)
}

func (c *AdminPingController) Post() {

}
