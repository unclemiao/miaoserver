package node

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/time_helper"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type OnlineNumNodeController struct {
	beego.Controller
}

func (node *OnlineNumNodeController) Get() {
	resp := &models.HttpResponse{Status: 0, DateTime: time_helper.NowMill()}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _     = node.GetInt64("sid", 0)
		plt        = node.GetString("platform", "")
		ch         = node.GetString("channel", "")
		runningMgr = logic.GetRunningMgr()
	)
	run := runningMgr.GetRunningNode(sid, sid, plt, ch)
	if run == nil {
		resp.ErrMsg = "params err"
		return
	}
	resp.Data = run.Online
	resp.Msg = "请求成功"
}

func (node *OnlineNumNodeController) Post() {

}
