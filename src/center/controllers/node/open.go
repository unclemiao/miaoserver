package node

import (
	"fmt"
	"time"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type OpenNodeController struct {
	beego.Controller
}

// 开服
func (node *OpenNodeController) Post() {
	logs.Infof("Open: %s", node.GetString("data"))

	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		infoMgr   = logic.GetNodeInfoMgr()
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
	)

	info, err := infoMgr.GetNodeInfo(sid, plt, ch)

	if controllers.ParamsErr(err != nil, resp, err, 501) ||
		controllers.ParamsErr(info == nil, resp, "sever not set", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}
	if !info.IsSlave {
		err = serverMgr.TryStartGameServerToAdmin(info, true)
		if controllers.ParamsErr(err != nil, resp, err, 501) {
			return
		}
	}
	info.State = 2
	if info.InitTime.IsZero() {
		info.InitTime = time.Now()
		info.Created = info.InitTime
	}
	err = infoMgr.SetServerNode(info)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	resp.Data = "ok"
}

func (node *OpenNodeController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		infoMgr   = logic.GetNodeInfoMgr()
		orm       = serverMgr.OrmMgr.Orm
		md5       = util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))
	)

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	var list []*models.NodeInfo
	queryTable := orm.QueryTable("node_info")

	if sid != 0 {
		queryTable = queryTable.Filter("sid", sid)
	}
	if plt != "" {
		queryTable = queryTable.Filter("platform", plt)
	}
	if ch != "" {
		queryTable = queryTable.Filter("channel", ch)
	}
	_, err := queryTable.All(&list)

	for i, n := range list {
		n, _ = infoMgr.GetNodeInfo(n.Sid, n.Platform, n.Channel)
		list[i] = n
	}

	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

	resp.Data = list

}
