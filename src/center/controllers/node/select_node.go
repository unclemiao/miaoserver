package node

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type SelectNodeController struct {
	beego.Controller
}

func (node *SelectNodeController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _     = node.GetInt64("sid", 0)
		plt        = node.GetString("platform", "")
		ch         = node.GetString("channel", "")
		clientVer  = node.GetString("clientVer", "")
		runningMgr = logic.GetRunningMgr()
	)

	var running, err = runningMgr.SelectRunning(sid, plt, ch, clientVer)

	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	resp.Data = running
}
