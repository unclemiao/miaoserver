package node

import (
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type PingController struct {
	beego.Controller
}

func (node *PingController) Post() {

	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		id, _       = node.GetInt64("id", 0)
		isMaster, _ = node.GetBool("master")
		port, _     = node.GetInt32("port")
		adminHost   = node.GetString("adminHost")
		sid, _      = node.GetInt64("sid", 0)
		plt         = node.GetString("platform", "")
		ch          = node.GetString("channel", "")
		online, _   = node.GetInt32("online", 0)
		slaveList   = node.GetString("slaveList", "[]")
		token       = node.GetString("weiyingToken")
		infoMgr     = logic.GetNodeInfoMgr()
	)
	logs.Infof("server ping id:%d, sid:%d, plt:%s, ch:%s, port:%d, master:%v, adminHost:%s, online:%d", id, sid, plt, ch, port, isMaster, adminHost, online)
	if controllers.ParamsErr(sid == 0, resp, "sid is empty", 501) ||
		controllers.ParamsErr(plt == "", resp, "platform is empty", 501) ||
		controllers.ParamsErr(ch == "", resp, "channel is empty", 501) ||
		controllers.ParamsErr(token == "", resp, "token is empty", 501) {
		return
	}

	serverNode, err := infoMgr.GetNodeInfo(sid, plt, ch)
	if controllers.ParamsErr(err != nil || serverNode == nil, resp, "server not open", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("%d%s%s%d%s", sid, plt, ch, online, serverNode.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	err = logic.GetRunningMgr().PingFromGame(sid, plt, ch, id, isMaster, port, adminHost, online, slaveList)

	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

}

func (node *PingController) Get() {

}
