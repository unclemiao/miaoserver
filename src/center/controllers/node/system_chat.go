package node

import (
	"fmt"
	"net/http"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type SystemChatController struct {
	beego.Controller
}

func (c *SystemChatController) Post() {

}

func (c *SystemChatController) Get() {
	var (
		resp         = &models.Response{Code: 200}
		plt          = c.GetString("platform", "")
		ch           = c.GetString("channel", "")
		kind, _      = c.GetInt32("kind", 0)
		msg          = c.GetString("msg", "")
		beginTime, _ = c.GetInt64("beginTime", 0)
		endTime, _   = c.GetInt64("endTime", 0)
		subTime, _   = c.GetInt64("beginTime", 180)
		nodeMgr      = logic.GetNodeInfoMgr()
	)
	defer func() {
		c.Data["json"] = resp
		_ = c.ServeJSON()
	}()
	ns, err := nodeMgr.GetAllNodeInfo(plt, ch)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	for _, n := range ns {
		go func(nodeInfo *models.NodeInfo) {
			c.sendToNode(nodeInfo, kind, msg, beginTime, endTime, subTime)
		}(n)
	}
}

func (c *SystemChatController) sendToNode(game *models.NodeInfo, kind int32, msg string, beginTime int64, endTime int64, subTime int64) {
	clientUrl := game.ClientUrl
	var url string
	if strings.Index(clientUrl, "wss") == 0 {
		url = strings.Replace(clientUrl, "wss", "https", 1)
	} else {
		url = strings.Replace(clientUrl, "ws", "http", 1)
	}
	url = strings.TrimRight(url, "ws")
	url = url + fmt.Sprintf("GmFunc?func=AddSystemChat&kind=%d&beginTime=%d&endTime=%d&subTime=%d&msg=%s", kind, beginTime, endTime, subTime, msg)
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		logs.Error(err)
	}
}
