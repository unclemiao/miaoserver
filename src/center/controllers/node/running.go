package node

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type RunningController struct {
	beego.Controller
}

func (node *RunningController) Post() {
}

func (node *RunningController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		plt         = node.GetString("platform", "")
		runningList []*models.RunningGame
		orm         = logic.GetServerManager().OrmMgr.Orm
	)
	queryTable := orm.QueryTable("running_game")

	if plt != "" {
		queryTable = queryTable.Filter("platform", plt)
	}
	_, err := queryTable.All(&runningList)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

	resp.Data = runningList

}
