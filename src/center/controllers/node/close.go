package node

import (
	"fmt"
	"net/http"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type CloseNodeController struct {
	beego.Controller
}

// 关服
func (node *CloseNodeController) Post() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		token      = node.GetString("weiyingToken", "*")
		due        = node.GetString("due", "0")
		sid, _     = node.GetInt64("sid", 0)
		serverMgr  = logic.GetServerManager()
		runningMgr = logic.GetRunningMgr()
	)

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s", sid, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}
	logs.Info("Close %d", sid)

	runningMgr.Range(func(game *models.RunningGame) bool {
		if sid == 0 || game.Sid == sid {
			sendToClose(game)
			game.State = 0
		}
		return true
	})

}

func sendToClose(game *models.RunningGame) {
	clientUrl := game.ClientUrl
	var url string
	if strings.Index(clientUrl, "wss") == 0 {
		url = strings.Replace(clientUrl, "wss", "https", 1)
	} else {
		url = strings.Replace(clientUrl, "ws", "http", 1)
	}
	url = strings.TrimRight(url, "ws")
	url = url + "GmFunc?func=StopServer"
	resp, err := http.Get(url)
	defer resp.Body.Close()
	if err != nil {
		logs.Error(err)
	}
}
