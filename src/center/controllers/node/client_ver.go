package node

import (
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type ClientVerController struct {
	beego.Controller
}

func (node *ClientVerController) Get() {

}

// 设置客户端版本号
func (node *ClientVerController) Post() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		clientVer = node.GetString("client_ver", "0")
		serverMgr = logic.GetServerManager()
		nodeMgr   = logic.GetNodeInfoMgr()
	)
	logs.Infof("set clinet ver %d %s %s", sid, plt, ch)
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s%s", sid, plt, ch, due, clientVer, serverMgr.Server.Token))
	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}
	var nodeList []*models.NodeInfo
	if sid != 0 {
		nodeInfo, err := nodeMgr.GetNodeInfo(sid, plt, ch)
		if controllers.ParamsErr(err != nil, resp, err, 501) {
			return
		}
		nodeInfo.ClientVer = clientVer
		err = nodeMgr.SetServerNode(nodeInfo)
		if controllers.ParamsErr(err != nil, resp, err, 501) {
			return
		}
		nodeList = append(nodeList, nodeInfo)
		if nodeInfo.State != 0 {
			running := logic.GetRunningMgr().GetRunningNode(nodeInfo.Sid, nodeInfo.Id, nodeInfo.Platform, nodeInfo.Channel)
			if running != nil && running.State != 0 {
				running.ClientVer = clientVer
			}
		}
	} else {
		ns, err := nodeMgr.GetAllNodeInfo(plt, ch)
		if controllers.ParamsErr(err != nil, resp, err, 501) {
			return
		}
		for _, n := range ns {
			n.ClientVer = clientVer
			err = nodeMgr.SetServerNode(n)
			if controllers.ParamsErr(err != nil, resp, err, 501) {
				return
			}
			nodeList = append(nodeList, n)
			if n.State != 0 {
				running := logic.GetRunningMgr().GetRunningNode(n.Sid, n.Id, n.Platform, n.Channel)
				if running != nil && running.State != 0 {
					running.ClientVer = clientVer
				}
			}
		}
	}
	resp.Data = nodeList

}
