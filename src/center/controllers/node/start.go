package node

import (
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type StartController struct {
	beego.Controller
}

func (c *StartController) Post() {

}

func (c *StartController) Get() {

	var (
		resp        = &models.Response{Code: 200}
		id, _       = c.GetInt64("id", 0)
		sid, _      = c.GetInt64("sid", 0)
		plt         = c.GetString("platform", "")
		ch          = c.GetString("channel", "")
		port, _     = c.GetInt32("port", 0)
		adminHost   = c.GetString("adminHost", "")
		isMaster, _ = c.GetBool("isMaster", false)
		due, _      = c.GetInt64("due", 0)
		token       = c.GetString("weiyingToken", "")
		serverMgr   = logic.GetServerManager()
	)
	defer func() {
		c.Data["json"] = resp
		_ = c.ServeJSON()
	}()
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%d%s", sid, plt, ch, due, serverMgr.Server.Token))
	if md5 != token {
		resp.Err = "token err"
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}

	serverNode, err := serverMgr.GetServerStarCmd(sid, id, plt, ch, port, isMaster, adminHost)
	if err != nil {
		resp.Err = err.Error()
		resp.Code = 501
		logs.Error(resp.Err)
		return
	}
	resp.Data = serverNode
}
