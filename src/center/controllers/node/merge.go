package node

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"os/exec"
	"strings"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type MergeController struct {
	beego.Controller
}

func (node *MergeController) Get() {
	resp := &models.HttpResponse{Status: 0, DateTime: time_helper.NowMill()}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		master, _ = node.GetInt64("master", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		slavestrs = node.GetString("slaves", "")
		nodeMgr   = logic.GetNodeInfoMgr()
	)
	masterNode, err := nodeMgr.GetNodeInfo(master, plt, ch)
	if controllers.ParamsErr(err != nil, resp, err, 501) ||
		controllers.ParamsErr(masterNode.State != 0, resp, "master no close", 501) {
		return
	}

	var slaveIdList []int64
	var slaveNodeList []*models.NodeInfo
	for _, strId := range strings.Split(slavestrs, ",") {
		id := util.AtoInt64(strId)
		logs.Infof("============ GetNodeInfo %d %s %s", id, plt, ch)
		slaveNode, e := nodeMgr.GetNodeInfo(id, plt, ch)
		if controllers.ParamsErr(e != nil, resp, e, 501) ||
			controllers.ParamsErr(slaveNode.State != 0, resp, "master no close", 501) {
			return
		}

		for _, masterSlaveId := range masterNode.SlaveList {
			if controllers.ParamsErr(masterSlaveId == id, resp, "has mergeed", 501) {
				return
			}
		}
		me := node.Merge(masterNode, slaveNode)
		if controllers.ParamsErr(me != nil, resp, me, 501) {
			return
		}
		slaveIdList = append(slaveIdList, id)
		for _, sid := range slaveNode.SlaveList {
			slaveIdList = append(slaveIdList, sid)
		}
		slaveNodeList = append(slaveNodeList, slaveNode)
	}

	masterNode.SlaveList = append(masterNode.SlaveList, slaveIdList...)
	bs, _ := json.Marshal(masterNode.SlaveList)
	masterNode.SlaveStr = string(bs)
	masterNode.IsSlave = false
	nodeMgr.SetServerNode(masterNode)
	for _, n := range slaveNodeList {
		n.IsSlave = true
		n.State = 0
		nodeMgr.SetServerNode(n)
	}
}

func (node *MergeController) Post() {

}

func (node *MergeController) GetMysqlIpFromAdmin(server *models.NodeInfo) string {
	host := server.AdminHost + "/mysqlIp"
	resp, err := http.Get(host)
	if err != nil {
		logs.Error(err)
		return ""
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		return ""
	}
	httpResp := models.Response{}
	err = json.Unmarshal(bs, &httpResp)
	if err != nil {
		logs.Error(err)
		return ""
	}
	return httpResp.Data.(string)
}

func (node *MergeController) Merge(master *models.NodeInfo, slave *models.NodeInfo) error {

	pathStr, _ := os.Getwd()
	pathStr = pathStr + "/script/"
	fileName := "merge.sh"
	pathStr = pathStr + fileName

	masterIp := node.GetMysqlIpFromAdmin(master)
	slaveIp := node.GetMysqlIpFromAdmin(slave)

	if masterIp == "" || slaveIp == "" {
		return fmt.Errorf("get ip err")
	}
	cmd := exec.Command(
		pathStr,
		fmt.Sprintf("%d", master.Sid),
		masterIp,
		master.SqlName,
		fmt.Sprintf("%d", slave.Sid),
		slaveIp,
		slave.SqlName,
	)
	logs.Infof("Command %s %s %s %s %s %s %s",
		"./script/merge.sh",
		fmt.Sprintf("%d", master.Sid),
		masterIp,
		master.SqlName,
		fmt.Sprintf("%d", slave.Sid),
		slaveIp,
		slave.SqlName,
	)
	var out bytes.Buffer
	cmd.Stdout = &out
	err := cmd.Run()
	if err != nil {
		logs.Error(err)
		return err
	}
	str := out.String()
	if str != "end merge" && str != "end merge\n" {
		return fmt.Errorf("merge err:%s", str)
	}
	logs.Infof("merge: %s", str)
	return nil
}
