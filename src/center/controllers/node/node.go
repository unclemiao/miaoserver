package node

import (
	"encoding/json"
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/center/controllers"
	"goproject/src/center/logic"
	"goproject/src/center/models"
)

type SetNodeController struct {
	beego.Controller
}

// 设置服务器节点
func (node *SetNodeController) Post() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		s         = &models.NodeInfo{}
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		infoMgr   = logic.GetNodeInfoMgr()
		data      = node.GetString("data")
	)

	_ = json.Unmarshal([]byte(data), s)

	if controllers.ParamsErr(s.Sid == 0, resp, "sid err", 501) ||
		controllers.ParamsErr(s.Platform == "", resp, "platform err", 501) ||
		controllers.ParamsErr(s.Channel == "", resp, "channel err", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", s.Sid, s.Platform, s.Channel, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	oldNode, _ := infoMgr.GetNodeInfo(s.Sid, s.Platform, s.Channel)
	if oldNode == nil {
		oldNode = s
	} else {
		oldNode.Name, oldNode.State, oldNode.Young, oldNode.Token, oldNode.ClientUrl, oldNode.CenterUrl, oldNode.AdminHost, oldNode.SqlHost, oldNode.Desc =
			s.Name, s.State, s.Young, s.Token, s.ClientUrl, s.CenterUrl, s.AdminHost, s.SqlHost, s.Desc
	}
	err := infoMgr.SetServerNode(oldNode)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

	if s.State == 0 {
		running := logic.GetRunningMgr().GetRunningNode(s.Sid, s.Sid, s.Platform, s.Channel)
		if running != nil && running.State != 0 {
			sendToClose(running)
		}
	}

	resp.Data = s
}

func (node *SetNodeController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		orm       = serverMgr.OrmMgr.Orm
		md5       = util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))
	)

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) {
		return
	}

	var list []*models.NodeInfo
	queryTable := orm.QueryTable("node_info")

	if sid != 0 {
		queryTable = queryTable.Filter("sid", sid)
	}
	if plt != "" {
		queryTable = queryTable.Filter("platform", plt)
	}
	if ch != "" {
		queryTable = queryTable.Filter("channel", ch)
	}
	_, err := queryTable.All(&list)

	var retList []*models.NodeInfo
	for _, n := range list {
		logs.Debugf("Node :%+v", n)
		if n.IsSlave {
			continue
		}
		retList = append(retList, n)
	}

	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}
	resp.Data = retList

}

func (node *SetNodeController) Delete() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	var (
		sid, _    = node.GetInt64("sid", 0)
		plt       = node.GetString("platform", "")
		ch        = node.GetString("channel", "")
		token     = node.GetString("weiyingToken", "*")
		due       = node.GetString("due", "0")
		serverMgr = logic.GetServerManager()
		infoMgr   = logic.GetNodeInfoMgr()
	)

	if controllers.ParamsErr(plt == "", resp, "platform err", 501) ||
		controllers.ParamsErr(ch == "", resp, "channel err", 501) {
		return
	}

	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", sid, plt, ch, due, serverMgr.Server.Token))

	if controllers.ParamsErr(md5 != token, resp, "token err", 501) ||
		controllers.ParamsErr(ch == "", resp, "channel err", 501) {
		return
	}

	old, _ := infoMgr.GetNodeInfo(sid, plt, ch)
	if controllers.ParamsErr(old == nil, resp, fmt.Errorf("server not found, sid：%d, plt:%s, ch:%s", sid, plt, ch), 501) {
		return
	}

	err := infoMgr.DeleteServerNode(old)
	if controllers.ParamsErr(err != nil, resp, err, 501) {
		return
	}

}
