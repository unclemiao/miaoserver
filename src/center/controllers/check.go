package controllers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/models"
)

type PingController struct {
	beego.Controller
}

func (c *PingController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		c.Data["json"] = resp
		_ = c.ServeJSON()
	}()
}
