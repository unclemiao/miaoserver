package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/admin"
)

func init() {
	ns := beego.NewNamespace("/konglong2/admin",
		beego.NSRouter("/ping",
			&admin.PingController{},
		),
	)
	beego.AddNamespace(ns)
}
