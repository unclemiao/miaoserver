package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/excel"
)

func init() {
	ns := beego.NewNamespace("/konglong2/excel",
		beego.NSRouter("/upload",
			&excel.UpLoadController{},
		),
	)
	beego.AddNamespace(ns)
}
