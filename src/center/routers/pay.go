package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/pay"
)

func init() {
	beego.Router("/konglong2/pay_mall",
		&pay.Controller{},
	)
	beego.Router("/konglong2/ios/pay_mall",
		&pay.IosController{},
	)

}
