package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/account"
)

func init() {
	ns := beego.NewNamespace("/konglong2/account",
		beego.NSRouter("/login",
			&account.LoginController{},
		),
		beego.NSRouter("/resetSid",
			&account.ResetSidController{},
		),
		beego.NSRouter("/set",
			&account.SetController{},
		),
	)
	beego.AddNamespace(ns)
}
