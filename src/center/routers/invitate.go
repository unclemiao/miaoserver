package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/invitate"
)

func init() {
	beego.Router("/konglong2/invitate",
		&invitate.Controller{},
	)

}
