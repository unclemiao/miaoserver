package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers"
)

func init() {

	beego.Router("/konglong2/ping",
		&controllers.PingController{},
	)
}
