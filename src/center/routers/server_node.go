package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/center/controllers/item"
	"goproject/src/center/controllers/node"
	"goproject/src/center/controllers/notice"
)

func init() {
	ns := beego.NewNamespace("/konglong2/server",
		beego.NSRouter("/node",
			&node.SetNodeController{},
		),
		beego.NSRouter("/start",
			&node.StartController{},
		),
		beego.NSRouter("/ping",
			&node.PingController{},
		),
		beego.NSRouter("/open",
			&node.OpenNodeController{},
		),
		beego.NSRouter("/notice",
			&notice.Controller{},
		),
		beego.NSRouter("/select",
			&node.SelectNodeController{},
		),
		beego.NSRouter("/item",
			&item.Controller{},
		),
		beego.NSRouter("/OnlineNum",
			&node.OnlineNumNodeController{},
		),
		beego.NSRouter("/close",
			&node.CloseNodeController{},
		),
		beego.NSRouter("/merge",
			&node.MergeController{},
		),
		beego.NSRouter("/systemChat",
			&node.SystemChatController{},
		),
		beego.NSRouter("/clientVer",
			&node.ClientVerController{},
		),
		beego.NSRouter("/running",
			&node.RunningController{},
		),
	)
	beego.AddNamespace(ns)
}
