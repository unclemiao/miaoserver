#!/bin/sh

masterId="${1}"
masterIp="${2}"
masterSqlName="${3}"

slaveId="${4}"
slaveIp="${5}"
slaveSqlName="${6}"

sqlRoot=root
sqlPass=123456

mysqldump -u${sqlRoot} -h${masterIp} -P3306 -p${sqlPass} ${masterSqlName} > ./${masterSqlName}_bk.sql
mysqldump -u${sqlRoot} -h${slaveIp} -P3306 -p${sqlPass} ${slaveSqlName} > ./${slaveSqlName}_bk.sql

mysql -u${sqlRoot} -h${masterIp} -P3306 -p${sqlPass} -e "
delete from ${masterSqlName}.duel where role_id > 0;
delete from ${masterSqlName}.duel_record where id > 0;
delete from ${masterSqlName}.pet_king_record where id > 0;
delete from ${masterSqlName}.mail where name != '';
"

mysql -u${sqlRoot} -h${slaveIp} -P3306 -p${sqlPass} -e "
delete from ${slaveSqlName}.duel where role_id > 0;
delete from ${slaveSqlName}.duel_record where id > 0;
delete from ${slaveSqlName}.pet_king_record where id > 0;
delete from ${slaveSqlName}.quota where role_id = 0;
delete from ${slaveSqlName}.feast_done where role_id = 0;
delete from ${slaveSqlName}.mail where name != '';
"

mysqldump -t -u${sqlRoot} -h${slaveIp} -P3306 -p${sqlPass} ${slaveSqlName} > ./${slaveSqlName}.sql

mysql -u${sqlRoot} -h${masterIp} -P3306 -p${sqlPass} ${masterSqlName} < ${slaveSqlName}.sql
rm ${slaveSqlName}.sql

mysql -u${sqlRoot} -h${masterIp} -P3306 -p${sqlPass} -e "
	update ${masterSqlName}.server_node SET is_merge=true where sid > 0;
	update ${masterSqlName}.server_node SET is_merge=false where sid = ${masterId};
"

echo "end merge"