#!/bin/sh

masterId="${1}"
masterIp="${2}"
masterSqlName="${3}"

slaveId="${4}"
slaveIp="${5}"
slaveSqlName="${6}"

mysqldump -ugame -h${masterIp} -P3306 -pMima!23s ${masterSqlName} > ./${masterSqlName}_bk.sql
mysqldump -ugame -h${slaveIp} -P3306 -pMima!23s ${slaveSqlName} > ./${slaveSqlName}_bk.sql

mysql -ugame -h${masterIp} -P3306 -pMima!23s -e "
delete from ${masterSqlName}.duel where role_id > 0;
delete from ${masterSqlName}.duel_record where id > 0;
delete from ${masterSqlName}.pet_king_record where id > 0;
delete from ${masterSqlName}.mail where name != '';
"

mysql -ugame -h${slaveIp} -P3306 -pMima!23s -e "
delete from ${slaveSqlName}.duel where role_id > 0;
delete from ${slaveSqlName}.duel_record where id > 0;
delete from ${slaveSqlName}.pet_king_record where id > 0;
delete from ${slaveSqlName}.quota where role_id = 0;
delete from ${slaveSqlName}.feast_done where role_id = 0;
delete from ${slaveSqlName}.mail where name != '';
"

mysqldump -t -ugame -h${slaveIp} -P3306 -pMima!23s ${slaveSqlName} > ./${slaveSqlName}.sql

mysql -ugame -h${masterIp} -P3306 -pMima!23s ${masterSqlName} < ${slaveSqlName}.sql
rm ${slaveSqlName}.sql

mysql -ugame -h${masterIp} -P3306 -pMima!23s -e "
	update ${masterSqlName}.server_node SET is_merge=true where sid > 0;
	update ${masterSqlName}.server_node SET is_merge=false where sid = ${masterId};
"

echo "end merge"