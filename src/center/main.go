package main

import (
	"goproject/src/center/logic"
	_ "goproject/src/center/routers"
)

func main() {
	server := logic.NewServerManager()
	server.Init()
	server.Star()
}
