package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"

	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/models"
	"goproject/src/konglong/data"
)

type Send struct {
	AccessToken string      `json:"access_token"`
	Touser      string      `json:"touser"`
	TemplateId  string      `json:"template_id"`
	Data        *DataStruct `json:"data"`
}

type DataStruct struct {
	Thing1 *DataStructSub1 `json:"thing1"`
	Thing2 *DataStructSub1 `json:"thing2"`
	Thing5 *DataStructSub1 `json:"thing5"`
}

type DataStructSub1 struct {
	Value interface{} `json:"value"`
}

func main() {

}

func GetIp() string {
	host := "http://192.168.10.178:8081/admin001" + "/mysqlIp"
	resp, err := http.Get(host)
	if err != nil {
		logs.Error(err)
		return ""
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		return ""
	}
	httpResp := models.Response{}
	err = json.Unmarshal(bs, &httpResp)
	if err != nil {
		logs.Error(err)
		return ""
	}
	return httpResp.Data.(string)
}
func GetToken() string {

	form := make(url.Values)
	form.Add("grant_type", "client_credential")
	form.Add("appid", "wx7af5401255a73f75")
	form.Add("secret", "6089deddd919b4acd29b048308545861")

	getUrl := "https://api.weixin.qq.com/cgi-bin/token?" + form.Encode()
	logs.Infof("------- :%s", getUrl)
	resp, err := http.Get(getUrl)
	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			GetToken()
		})
		return ""
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			GetToken()
		})
		return ""
	}

	respData := &data.WXAccessToken{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			GetToken()
		})
		return ""
	}
	if respData.Errcode != 0 {
		logs.Error(respData.Errmsg)
		time.AfterFunc(time.Minute, func() {
			GetToken()
		})
		return ""
	}
	return respData.AccessToken
}

var Ranks []*A

type A struct {
	OldRank int32
	Rank    int32
	Done    int32
}

func InitTest() {
	for i := int32(0); i < 10; i++ {
		Ranks = append(Ranks, &A{
			OldRank: i,
			Rank:    i + 1,
			Done:    100 - 10*i,
		})
	}
}
func TestUp(me *A, ranks []*A, up bool) {
	if up {
		for i := me.Rank - 2; i >= 0; i-- {
			other := ranks[i]
			if me.Done > other.Done {
				ranks[i+1] = ranks[i]
				ranks[i] = me
				other.Rank = other.Rank + 1
				me.Rank = me.Rank - 1
			}
		}
	} else {
		for i := me.Rank; i < util.MinInt32(int32(len(ranks)), 1000); i++ {
			other := ranks[i]
			if me.Done <= other.Done {
				ranks[i-1] = ranks[i]
				ranks[i] = me
				other.Rank = other.Rank - 1
				me.Rank = me.Rank + 1
			}
		}
	}
}

func TTTT(jsCode string) (string, error) {
	nowSec := time_helper.NowSec()
	form := make(url.Values)
	form.Set("pkgName", "com.iuno.hydpd.nearme.gamecenter")
	form.Set("token", jsCode)
	form.Set("timeStamp", fmt.Sprintf("%d", nowSec*1000))

	// 加密签名
	tokenForm := make(url.Values)
	tokenForm.Add("pkgName", "com.iuno.hydpd.nearme.gamecenter")
	tokenForm.Add("appKey", "5twAqB6QZaoSg4SgS08Ww0k4g")
	tokenForm.Add("appSecret", "Ee453872ff13c213735eBD3F12061616")
	tokenForm.Add("token", jsCode)
	tokenForm.Add("timeStamp", fmt.Sprintf("%d", nowSec*1000))
	tokenFormUrl := tokenForm.Encode()
	str := strings.ToTitle(util.Md5(tokenFormUrl))

	form.Set("sign", str)

	form.Set("version", "1.0.0")

	urlstr := "https://play.open.oppomobile.com/instant-game-open/userInfo?" + form.Encode()
	logs.Debugf("get to .........: %s", urlstr)
	wxResp, err := http.Get(urlstr)
	if err != nil {
		return "", err
	}
	defer wxResp.Body.Close()
	bs, err := ioutil.ReadAll(wxResp.Body)
	if err != nil {
		return "", err
	}
	respData := &models.OppoLoginResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		return "", err
	}
	if respData.ErrCode != 200 {
		return "", fmt.Errorf("%s", respData.ErrMsg)
	}
	return respData.UserInfo.UserId, nil

}
func hmacSha256(data string, secret string) string {
	h := hmac.New(sha256.New, []byte(secret))
	h.Write([]byte(data))
	return hex.EncodeToString(h.Sum(nil))
}
