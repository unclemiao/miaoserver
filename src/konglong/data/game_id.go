package data

type IdData struct {
	IdType     int32 `db:"id_type"`
	Sid        int32 `db:"sid"`
	GenerateId int64 `db:"generate_id"`
}
