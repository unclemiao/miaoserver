package data

import (
	"time"

	"goproject/src/konglong/protocol"
)

type Duel struct {
	RoleId   int64     `db:"role_id"`
	Name     string    `db:"name"`
	IsRobot  bool      `db:"is_robot"`
	Avatar   string    `db:"avatar"`
	Rank     int32     `db:"rank"`
	DuelTime time.Time `db:"duel_time"`

	FightInfoJson     string  `db:"fight_info_json"`
	FightInfoRankList []int32 `db:"-"`
}

type DuelRecord struct {
	Id            int64     `db:"id"`
	WinnerId      int64     `db:"winner_id"`
	WinnerName    string    `db:"winner_name"`
	WinnerAvatar  string    `db:"winner_avatar"`
	WinnerIsRobot bool      `db:"winner_is_robot"`
	LoserId       int64     `db:"loser_id"`
	LoserName     string    `db:"loser_name"`
	LoserAvatar   string    `db:"loser_avatar"`
	LoserIsRobot  bool      `db:"loser_is_robot"`
	Rank          int32     `db:"rank"`
	IsWin         bool      `db:"is_win"`
	DuelTime      time.Time `db:"duel_time"`
}

func (record *DuelRecord) GetForClient() *protocol.DuelFightRecord {
	return &protocol.DuelFightRecord{
		Winner: &protocol.DuelRankInfo{
			RoleId:  record.WinnerId,
			Name:    record.WinnerName,
			Avatar:  record.WinnerAvatar,
			IsRobot: record.WinnerIsRobot,
		},
		Loser: &protocol.DuelRankInfo{
			RoleId:  record.LoserId,
			Name:    record.LoserName,
			Avatar:  record.LoserAvatar,
			IsRobot: record.LoserIsRobot,
		},
		Rankup: record.Rank,
		IsWin:  record.IsWin,
	}
}

func (info *Duel) GetForClient() *protocol.DuelInfo {
	return &protocol.DuelInfo{
		RoleId:   info.RoleId,
		Name:     info.Name,
		IsRobot:  info.IsRobot,
		Avatar:   info.Avatar,
		Rank:     info.Rank,
		StubInfo: nil,
	}
}

func (info *Duel) GetRankInfoForClient() *protocol.DuelRankInfo {
	return &protocol.DuelRankInfo{
		RoleId:  info.RoleId,
		Name:    info.Name,
		IsRobot: info.IsRobot,
		Avatar:  info.Avatar,
		Rank:    info.Rank,
	}
}

type DuelCfg struct {
	CfgId     int32     `json:"index"`
	RankList  []int32   `json:"rank"`
	RankAward *AwardCfg `json:"reward"`
	WinAward  *AwardCfg `json:"winReward"`
	LoseAward *AwardCfg `json:"failReward"`
}
