package data

import "goproject/src/konglong/protocol"

type Rank struct {
	RoleId              int64  `db:"role_id"`
	Name                string `db:"name"`
	Avatar              string `db:"avatar"`
	Rank                int32  `db:"-"`
	YesterDayRank       int32  `db:"yesterday_rank"`
	YesterDayBeforeRank int32  `db:"yesterday_before_rank"`
	GamePass            int32  `db:"game_pass"`
	GamePassTime        int64  `db:"game_pass_time"`
	Awarded             bool   `db:"awarded"`
}

func (info *Rank) GetForClient() *protocol.RankInfo {
	return &protocol.RankInfo{
		RoleId:   info.RoleId,
		Name:     info.Name,
		Avatar:   info.Avatar,
		Rank:     info.Rank,
		RankType: protocol.RankType_Rank_Game_Pass,
		Score:    info.GamePass,
	}
}

func (info *Rank) GetSelfForClient() *protocol.SelfRankInfo {
	return &protocol.SelfRankInfo{
		RoleId:   info.RoleId,
		Name:     info.Name,
		Avatar:   info.Avatar,
		Rank:     info.Rank,
		RankType: protocol.RankType_Rank_Game_Pass,
		Score:    info.GamePass,
		Awarded:  info.Awarded,
		UpRank:   info.YesterDayBeforeRank - info.YesterDayRank,
	}
}

type RankAwardCfg struct {
	CfgId       int32   `json:"index"`
	Rank        []int32 `json:"rank"`
	Diamonds    int64   `json:"coin"`
	AddDiamonds int64   `json:"addcoin"`
}
