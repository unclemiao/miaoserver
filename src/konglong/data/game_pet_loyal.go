package data

type PetLoyalCfg struct {
	CfgId       int32 `json:"index"`
	Weight      int64 `json:"weight"`
	TotalWeight int64
	Loyal       int32
}

func (cfg *PetLoyalCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *PetLoyalCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *PetLoyalCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *PetLoyalCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *PetLoyalCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}
