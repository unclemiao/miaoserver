package data

import "goproject/src/konglong/protocol"

type FoodBook struct {
	RoleId      int64 `db:"role_id"`
	Level       int32 `db:"level"`
	Exp         int64 `db:"exp"`
	HighEndTime int64 `db:"high_end_time"`
}

func (book *FoodBook) GetForClient() *protocol.FoodBook {
	return &protocol.FoodBook{
		Level:       book.Level,
		Exp:         book.Exp,
		HighEndTime: book.HighEndTime,
	}
}

type FoodBookCfg struct {
	Level         int32     `json:"index"`
	DropId        int32     `json:"drop_id"`
	Stuff         *AwardCfg `json:"stuff"`
	PassLevel     int32     `json:"pass_level"`
	Exp           int64     `json:"exp"`
	LevelUpStuff  *AwardCfg `json:"level_up_stuff"`
	AvgExp        uint64
	BoxDropId     int32     `json:"box_drop_id"`
	FosterExp     uint64    `json:"foster_exp"`
	HighBuyLv     int32     `json:"high_buy_lv"`
	HighBuyStuff  *AwardCfg `json:"high_buy_stuff"`
	HighBuyAddExp int64     `json:"high_buy_add_exp"`
	HighBuyDropId int32     `json:"high_buy_drop_id"`
}
