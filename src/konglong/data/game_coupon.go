package data

type Coupon struct {
	RoleId int64 `db:"role_id"`
	CfgId  int32 `db:"cfg_id"`
}

type RoleCoupon struct {
	CouponMap map[int32]struct{}
}
