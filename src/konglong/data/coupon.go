package data

type CouponCfg struct {
	CfgId      int32     `json:"index"`
	CouponCode string    `json:"coupon_code"`
	Reward     *AwardCfg `json:"reward"`
}
