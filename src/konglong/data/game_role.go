package data

import (
	"goproject/src/konglong/protocol"
)

type RoleChangeType int32

type Role struct {
	RoleId      int64  `db:"role_id"`      // 玩家id
	Name        string `db:"name"`         // 玩家名字
	Avatar      string `db:"avatar"`       // 玩家头像
	Channel     string `db:"channel"`      // 玩家渠道
	AccountId   string `db:"account_id"`   // 账号id
	AccountName string `db:"account_name"` // 账号
	Sid         int32  `db:"sid"`          // 服务器id
	Tag         int32  `db:"tag"`          // 禁号标识

	Level                  int32  `db:"level"`                      // 当前等级
	Exp                    uint64 `db:"exp"`                        // 当前经验
	GamePass               int32  `db:"game_pass"`                  // 当前关卡数
	LastGamePassExtAward   int32  `db:"last_game_pass_ext_award"`   // 最后一次领取额外奖励的关卡数
	GamePassExtAwardAmount int32  `db:"game_pass_ext_award_amount"` // 当前领取额外奖励的次数
	Newbie                 int32  `db:"newbie"`                     // 新手引导步骤
	SevenSign              int32  `db:"seven_sign"`                 // 七日签到天数
	NextSevenSignTime      int32  `db:"next_seven_sign_time"`       // 下次可签到时间
	SystemAwarded          int64  `db:"system_awarded"`             // 系统开启奖励，位运算 0b0100101, 1 表示已签到，0表示未签到，从低位到高位 1-7
	HelpIndexUnlockInfo    int32  `db:"help_index_unlock_info"`     // 助阵解锁信息，低位运算 0b00001101
	Horse                  int64  `db:"horse"`                      // 坐骑 id
	ModelCfgId             int32  `db:"model_cfg_id"`
	ModelName              string `db:"model_name"`
	PrivateProto           bool   `db:"private_proto"`
	Collect                int32  `db:"collect"` // 是否收藏 0 = 未收藏， 1=已收藏未领取奖励 2=已收藏并领取奖励

	// 充值相关
	Payn   int32 `db:"payn"`   // 充值次数
	Payall int32 `db:"payall"` // 充值总计
	Vip    int32 `db:"vip"`    // vip 等级
	Vipp   int32 `db:"vipp"`   // vip 经验

	LotteryItemRate int32 `db:"lottery_item_rate"` // 转盘倍数

	Audio    bool   `db:"audio"`    // 声音开关
	Shake    bool   `db:"shake"`    // 震动开关
	Language string `db:"language"` // 语言

	InviteId     int64 `db:"invite_id"`
	InviteSid    int64 `db:"invite_sid"`
	InviteNotify bool  `db:"invite_notify"`

	Attr *Attr `db:"-"`
}

func (role *Role) GetForClient() *protocol.Role {
	return &protocol.Role{
		Id:                role.RoleId,
		Name:              role.Name,
		Avatar:            role.Avatar,
		Level:             role.Level,
		Exp:               int64(role.Exp),
		GamePass:          role.GamePass,
		Newbie:            role.Newbie,
		Audio:             role.Audio,
		Shake:             role.Shake,
		Language:          role.Language,
		SignedDay:         role.SevenSign,
		NextSevenSignTime: role.NextSevenSignTime,
		HorseId:           role.Horse,
		SystemAwarded:     role.SystemAwarded,
		Atk:               role.Attr.Atk,
		AtkTime:           role.Attr.AtkTime,
		Crit:              role.Attr.Crit,
		Def:               role.Attr.Def,
		Dis:               role.Attr.Dis,
		Dodge:             role.Attr.Dodge,
		HpMax:             role.Attr.HpMax,
		LotteryRate:       role.LotteryItemRate,
		ModelCfgId:        role.ModelCfgId,
		ModelName:         role.ModelName,
		PrivateProto:      role.PrivateProto,
		Collect:           role.Collect,
	}
}

type RoleLevelCfg struct {
	Level       int32  `json:"level"`
	Exp         uint64 `json:"exp"`
	Atk         int32  `json:"atk"`     // 攻击力
	AtkTime     int32  `json:"atktime"` // 攻击时间间隔
	Crit        int32  `json:"crit"`    // 暴击 （千分比）
	Def         int32  `json:"def"`     // 防御
	Dis         int32  `json:"dis"`     // 攻击距离
	Dodge       int32  `json:"dodge"`   // 闪避 （千分比）
	HpMax       int64  `json:"hp"`      // 血量上限
	TalentPoint int32  `json:"skill_point"`
}

type PiecesChoice struct {
	Num    int64
	IdList []int64
}

// 七日签到配置
type SevenSignCfg struct {
	Day          int32     `json:"index"`
	Reward       *AwardCfg `json:"reward"`
	ChoiceReward *AwardCfg `json:"choice_reward"`
	PetDropId    int32     `json:"pet_drop_id"`
	NeedAd       bool      `json:"ad"`
}
