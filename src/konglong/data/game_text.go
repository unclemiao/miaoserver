package data

type GameTextCfg struct {
	CfgId int32  `json:"index"`
	Text  string `json:"text"`
}
