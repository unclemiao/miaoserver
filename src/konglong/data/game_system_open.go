package data

type SystemAwardCfg struct {
	CfgId      int32     `json:"index"`
	UnLockType int32     `json:"unlockType"`
	UnLockCond int32     `json:"unlockLevel"`
	Award      *AwardCfg `json:"reward"`
}
