package data

import "goproject/src/konglong/protocol"

type QuotaCfg struct {
	LotteryItem         int32 // 转盘使用次数
	Paral               int32 // 副本使用次数
	Duel                int32 // 困兽场所使用次数
	DailyFreeLotteryPet int32 // 每日免费孵化次数
}

// 所有系统的使用次数
type Quota struct {
	RoleId              int64 `db:"role_id"`
	LotteryItem         int32 `db:"lottery_item"`           // 转盘使用次数
	LotteryItemAdd      int32 `db:"lottery_item_add"`       // 转盘增加次数的次数
	LotteryItemGemAdd   int32 `db:"lottery_item_gem_add"`   // 转盘宝石增加次数的次数
	Paral               int32 `db:"paral"`                  // 副本使用次数
	ParalAdSdk          int32 `db:"paral_ad_sdk"`           // 副本看视频使用次数
	ParalGem            int32 `db:"paral_gem"`              // 副本宝石使用次数
	Duel                int32 `db:"duel"`                   // 困兽场所使用次数
	DuelAdSdk           int32 `db:"duel_ad_sdk"`            // 困兽场所看广告次数
	DuelGem             int32 `db:"duel_gem"`               // 困兽场所宝石免广告次数
	GamePassExtAward    int32 `db:"game_pass_ext_award"`    // 关卡额外奖励使用次数
	GameDropDiamond     int32 `db:"game_drop_diamond"`      // 游戏掉落钻石宝箱使用次数
	GameDropPet         int32 `db:"game_drop_pet"`          // 游戏掉落孵化蛋使用次数
	FreeFastPass        int32 `db:"free_fast_pass"`         // 免费加速使用次数
	AdFastPass          int32 `db:"ad_fast_pass"`           // 看视频加速使用次数
	GemFastPass         int32 `db:"gem_fast_pass"`          // 宝石加速使用次数
	DailyFreeLotteryPet int32 `db:"daily_free_lottery_pet"` // 每日免费孵化使用次数
	FreeRefreshDuel     int32 `db:"free_refresh_duel"`      // 每日免费刷新竞技场使用次数
	LotteryEquip        int32 `db:"lottery_equip"`          // 装备抽卡次数
	VentureFreeFast     int32 `db:"venture_free_fast"`      // 免费探险扫荡使用次数
	VentureAddFast      int32 `db:"venture_add_fast"`       // 增加免费探险扫荡使用次数的次数
	VentureGemAddFast   int32 `db:"venture_gem_add_fast"`   // 宝石增加免费探险扫荡使用次数的次数

	PetKingFreeRefresh    int32 `db:"pet_king_free_refresh"` // 恐龙争霸赛免费刷新次数
	PetKingFreeFight      int32 `db:"pet_king_free_fight"`   // 恐龙争霸赛免费战斗次数
	PetKingAdFight        int32 `db:"pet_king_ad_fight"`     // 每日恐龙争霸赛看广告战斗次数
	DropBox               int32 `db:"drop_box"`              // 宝箱掉落次数
	TryVariation          int32 `db:"try_variation"`         // 世界尝试变异使用次数
	DailyAdSdk            int32 `db:"daily_ad_sdk"`
	DailyCanLookAdSdk     int32 `db:"daily_can_look_ad_sdk"`
	DailyCanLookAdSdkAdd  int32 `db:"daily_can_look_ad_sdk_add"`
	SuperEvolutionFree    int32 `db:"super_evolution_free"`
	SuperEvolutionAdSdk   int32 `db:"super_evolution_ad_sdk"`
	SuperEvolutionItemBuy int32 `db:"super_evolution_item_buy"`
}

type CdCfg struct {
	LotteryItem int32 // 转盘增加次数cd
}

type Cd struct {
	RoleId      int64 `db:"role_id"`
	LotteryItem int32 `db:"lottery_item_quota_cd"` // 转盘增加次数cd
	AdFastPass  int32 `db:"ad_fast_pass"`          // 加速关卡看广告cd
}

func (info *Cd) GetForClient() []*protocol.Cd {
	return []*protocol.Cd{
		{
			CdType:  protocol.CdType_CD_Lottery_Item_Quota_Cd,
			CdValue: info.LotteryItem,
		},
		{
			CdType:  protocol.CdType_CD_Ad_Fast_Pass_Cd,
			CdValue: info.AdFastPass,
		},
	}
}

type QuotaFunc struct {
	QuotaNumBy func(*Context, int64, int32) int32
	QuotaMaxBy func(*Context, int64) int32
	TakeQuota  func(*Context, int64, int32, int32)
	GiveQuota  func(*Context, int64, int32, int32)
}

type CdFunc struct {
	CdUnitBy func(*Context, int64) int32
	AddCd    func(*Context, int64, int64)
	CleanCd  func(*Context, int64, int64)
}

type QuotaMaxCfg struct {
	CfgId int32              `json:"index"`
	Kind  protocol.QuotaType `json:"kind"`
	Max   int32              `json:"max"`
}
