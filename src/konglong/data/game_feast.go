package data

import "goproject/src/konglong/protocol"

type FeastCfg struct {
	CfgId int32              `json:"index"`
	Kind  protocol.FeastKind `json:"kind"`
	Ons   map[int64]int64
	Offs  map[int64]int64
	On    int64 // 开启时间
	Off   bool  // 是否提前结束
	Week  int32
	On0   string `json:"on0"`
	On9   string `json:"on9"`
	Off0  string `json:"off0"`
	Off9  string `json:"off9"`

	// ------------ 龙王争霸 ------------
	RankRange []int32   `json:"rank_range"`
	Reward    *AwardCfg `json:"reward"`

	// ------------ 限时活动 ------------
	Done      int32   `json:"amount"`
	ChoicePet []int32 `json:"choice_reward"`
	PetLevel  int32   `json:"pet_level"`

	// ------------ 月卡活动 ------------
	BuyReward               *AwardCfg `json:"buy_reward"`
	DailyReward             *AwardCfg `json:"daily_reward"`
	DailyMailDesc           []int32   `json:"dail_mail_desc"`
	ReadyOvertimeMailDesc   []int32   `json:"ready_overtime_mail_desc"`
	AlreadyOvertimeMailDesc []int32   `json:"already_overtime_mail_desc"`
}

type FeastsCfg struct {
	Kind   protocol.FeastKind
	Ons    map[int64]int64
	Offs   map[int64]int64
	On     int64 // 开启时间
	Off    bool  // 是否提前结束
	Week   int32
	On0    string
	On9    string
	Off0   string
	Off9   string
	CfgMap map[int32]*FeastCfg
}

type FeastRepeatPayCfg struct {
	CfgId        int32              `json:"index"`
	Kind         protocol.FeastKind `json:"kind"`
	Pay          int32              `json:"pay"`
	Award        *AwardCfg          `json:"reward"`
	ChoiceReward []int32            `json:"choice_reward"`
	PetLevel     int32              `json:"pet_level"`
}
