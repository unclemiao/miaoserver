package data

import "goproject/src/konglong/protocol"

type StubDb struct {
	Id        int64             `db:"id"`
	RoleId    int64             `db:"role_id"`
	StubIndex int32             `db:"stub_index"`
	Kind      protocol.StubKind `db:"kind"`
	PetId     int64             `db:"pet_id"`
	HelpId1   int64             `db:"help_id_1"`
	HelpId2   int64             `db:"help_id_2"`
}

type HelpUnit struct {
	Index int32
	Lock  bool
	PetId int64
}

func (stubUnit *HelpUnit) GetForClient() *protocol.HelpIndexInfo {
	info := &protocol.HelpIndexInfo{
		Index: stubUnit.Index,
		Lock:  stubUnit.Lock,
	}

	return info
}

type StubUnit struct {
	Id         int64
	Index      int32
	PetId      int64
	HelperList []*HelpUnit
}

type StubInfo struct {
	RoleId    int64
	Kind      protocol.StubKind
	UnitList  []*StubUnit
	PetInStub map[int64]int32
	PetInHelp map[int64]int32
}

type RoleStubGroud struct {
	StubInfoMap map[protocol.StubKind]*StubInfo
}
