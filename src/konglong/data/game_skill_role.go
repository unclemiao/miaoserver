package data

type SkillRole struct {
	Id         int64 `db:"id"`
	RoleId     int64 `db:"role_id"`
	CfgId      int32 `db:"cfg_id"`
	Lv         int32 `db:"lv"`
	TotalPoint int32 `db:"total_point"`
}

type RoleSkills struct {
	SkillMap    map[int64]*SkillRole
	SkillCfgMap map[int32]*SkillRole
}

type SkillRoleCfg struct {
	CfgId             int32     `json:"id"`
	Layer             int32     `json:"index"`
	Level             int32     `json:"level"`
	MaxLv             int32     `json:"maxLevel"`
	NeedPreSkillPoint int32     `json:"needPoint"`
	Stuff             *AwardCfg `json:"cost"`
	SkillCfgId        int32     `json:"skill"`
	Point             int32
}
