package data

import "goproject/engine/util"

type PetDropCfg struct {
	CfgId       int32 `json:"index"`
	DropId      int32 `json:"drop_id"`
	PetId       int32 `json:"pet_id"`
	Weight      int64 `json:"weight"`
	Variation   int32 `json:"variation"`
	TotalWeight int64
}

func (cfg *PetDropCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *PetDropCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *PetDropCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *PetDropCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *PetDropCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type PetDropGroupCfg struct {
	DropId         int32 `json:"drop_id"`
	DropCfgMap     map[int32]*PetDropCfg
	DropWeightList []util.WeightItr
}
