package data

import (
	"goproject/src/konglong/protocol"
)

type EventNet struct {
	EventBase
	Msg *protocol.Envelope
}

func (m *EventNet) IsNetEvent() bool {
	return true
}

func (m *EventNet) GetEnvMsg() *protocol.Envelope {
	return m.Msg
}

func (m *EventNet) GetMsgId() MsgIdItf {
	return m.Msg.MsgType
}
