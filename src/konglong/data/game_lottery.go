package data

import (
	"goproject/src/konglong/protocol"
)

// ************************************ LotteryItem ***********************************
type LotteryItemCfg struct {
	CfgId       int32   `json:"index"`
	Name        string  `json:"name"`
	ItemCfgId   []int64 `json:"itemid"`
	ItemDropId  int32   `json:"drop_id"`
	Num         int64   `json:"num"`
	Weight      int64   `json:"rate"`
	TotalWeight int64   `json:"-"`
	ArraryIndex int32   `json:"-"`
}

func (cfg *LotteryItemCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *LotteryItemCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *LotteryItemCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *LotteryItemCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *LotteryItemCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

// ************************************ LotteryPet ************************************
type LotteryPetCfg struct {
	CfgId       int32  `json:"index"`
	Kind        int32  `json:"kind"`
	Name        string `json:"name"`
	PetCfgId    int32  `json:"pet_cfg_id"`
	Weight      int64  `json:"weight"`
	TotalWeight int64
}

func (cfg *LotteryPetCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *LotteryPetCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *LotteryPetCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *LotteryPetCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *LotteryPetCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type LotteryPetScoreCfg struct {
	CfgId  int32     `json:"index"`
	Score  int32     `json:"score"`
	Reward [][]int32 `json:"reward"`
}

type LotteryPetAward struct {
	CfgId int32
	Score int32
	Award int32
}

type LotteryPet struct {
	RoleId                  int64 `db:"role_id"`
	IsFirst                 bool  `db:"is_first"`
	IsSecond                bool  `db:"is_second"`
	SuperEvolutionBuyTime   int64 `db:"super_evolution_buy_time"`
	SuperEvolutionBuyAmount int32 `db:"super_evolution_buy_amount"`

	Score               int32              `db:"score"`
	AwardStr            string             `db:"award_str"`
	DailyLotteryPetLuck int32              `db:"daily_lottery_pet_luck"`
	AwardInfo           []*LotteryPetAward `db:"-"`
}

func (info *LotteryPet) GetForClient() *protocol.LotteryPet {
	client := &protocol.LotteryPet{
		Score:                 info.Score,
		DailyLotteryPetLuck:   info.DailyLotteryPetLuck,
		SuperEvolutionBuyTime: info.SuperEvolutionBuyTime,
	}
	for _, award := range info.AwardInfo {
		client.AwardBoxList = append(client.AwardBoxList, &protocol.LotteryPetAward{
			CfgId: award.CfgId,
			Score: award.Score,
			Award: award.Award,
		})
	}
	return client
}

type LotteryEquipCfg struct {
	CfgId       int32  `json:"index"`
	Name        string `json:"name"`
	EquipCfgId  int64  `json:"equip_id"`
	Weight      int64  `json:"weight"`
	TotalWeight int64
	ArraryIndex int32
}

func (cfg *LotteryEquipCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *LotteryEquipCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *LotteryEquipCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *LotteryEquipCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *LotteryEquipCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}
