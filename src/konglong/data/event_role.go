package data

type EventOfflineRole struct {
	EventBase
	RoleId  int64
	SignOut bool
}

type EventOnlineRole struct {
	EventBase
	RoleId int64
	SignIn bool
	IsNew  bool
}

type EventRoleNewDay struct {
	EventBase
	RoleId    int64
	IsOnline  bool
	IsDaily   bool
	IsNewWeek bool
	NowSec    int64
	LastSec   int64 // 上一次 timeFeast.last
}

type EventRoleLoop struct {
	EventBase
	RoleId   int64
	NewDay   bool
	Online   bool
	Now      int64
	Interval int64
}

type EventRoleLevel struct {
	EventBase
	Role     *Role
	OldLevel int32
	NewLevel int32
}
