package data

import "github.com/jmoiron/sqlx"

type SqlDsn struct {
	Driver       string `db:"db_driver"`
	Host         string `db:"db_host"`
	Port         int32  `db:"db_port"`
	Username     string `db:"db_user"`
	Password     string `db:"db_password"`
	DatabaseName string `db:"db_database"`
}

type CleanCallback func(int64)

type GameSql struct {
	Dsn           *SqlDsn
	Db            *sqlx.DB
	Tx            *sqlx.Tx
	IsBegin       bool
	MarkCleanIds  map[int64]struct{} // key=role_id
	CleanCallback CleanCallback      // sql rollback 事件回调
}
