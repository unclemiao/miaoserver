package data

import "goproject/src/konglong/protocol"

type DailyQuestCfg struct {
	CfgId  int32     `json:"index"`
	Name   string    `json:"task"`
	Count  int32     `json:"count"`
	Award  *AwardCfg `json:"reward"`
	IsRate int32     `json:"limit"`
	AddExp int32     `json:"addExp"`
}

type DailyQuest struct {
	Id      int64 `db:"id"`
	RoleId  int64 `db:"role_id"`
	CfgId   int32 `db:"cfg_id"`
	Count   int32 `db:"count"`
	Awarded bool  `db:"awarded"`
	IsRate  bool  `db:"is_rate"`
}

func (info *DailyQuest) GetForClient() *protocol.DailyQuest {
	return &protocol.DailyQuest{
		CfgId:   info.CfgId,
		Count:   info.Count,
		Awarded: info.Awarded,
		IsRate:  info.IsRate,
	}
}

type RoleDailyQuest struct {
	QuestMap   map[int32]*DailyQuest
	QuestAward *QuestAward
}

func (info *RoleDailyQuest) GetForClient() []*protocol.DailyQuest {
	var clientList []*protocol.DailyQuest
	for _, quest := range info.QuestMap {
		clientList = append(clientList, quest.GetForClient())
	}
	return clientList
}

type QuestAwardCfg struct {
	Index int32     `json:"index"`
	Score int32     `json:"score"`
	Award *AwardCfg `json:"reward"`
	Limit int32     `json:"limit"`
}

type QuestAward struct {
	RoleId     int64  `db:"role_id"`
	Score      int32  `db:"score"`
	AwardedStr string `db:"awarded_str"`
	Awarded    []int32
}

func (info *QuestAward) GetForClient() *protocol.DailyQuestAward {
	if info == nil {
		return &protocol.DailyQuestAward{
			Score: 0,
		}
	}
	return &protocol.DailyQuestAward{
		Score:   info.Score,
		Awarded: info.Awarded,
	}
}
