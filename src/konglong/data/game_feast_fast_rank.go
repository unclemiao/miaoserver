package data

import "goproject/src/konglong/protocol"

type FeastFastRankCfg struct {
	CfgId     int32              `json:"index"`
	Kind      protocol.FeastKind `json:"kind"`
	RankRange []int32            `json:"rank_range"`
	Reward    *AwardCfg          `json:"reward"`
}

type FeastFastRank struct {
	FeastDoneList []*FeastDone
}

func (f *FeastFastRank) GetForClient() []*protocol.FeastDone {
	var proto []*protocol.FeastDone
	for _, fd := range f.FeastDoneList {
		proto = append(proto, &protocol.FeastDone{
			Id:      fd.Id,
			CfgId:   fd.CfgId,
			Done:    fd.Done,
			Awarded: fd.Awarded,
			Kind:    fd.Kind,
		})
	}
	return proto
}
