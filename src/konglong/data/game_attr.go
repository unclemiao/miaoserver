package data

import "goproject/src/konglong/protocol"

type Attr struct {
	Atk     int32 `json:"atk"`     // 攻击力
	AtkTime int32 `json:"atktime"` // 攻击时间间隔
	Crit    int32 `json:"crit"`    // 暴击 （千分比）
	Def     int32 `json:"def"`     // 防御
	Dis     int32 `json:"dis"`     // 攻击距离
	Dodge   int32 `json:"dodge"`   // 闪避 （千分比）
	Hp      int64 `json:"hp"`      // 当前血量
	HpMax   int64 `json:"hp_max"`  // 血量上限
}

type AttrCfg struct {
	ElemList []*protocol.AttrElem `json:"elem_list"`
}
