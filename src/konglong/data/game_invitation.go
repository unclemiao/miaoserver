package data

import "goproject/src/konglong/protocol"

type InviteCfg struct {
	CfgId        int32   `json:"index"`
	Invite       int32   `json:"invitation"`
	RandDropId   int32   `json:"rand_drop_id"`
	ChoiceReward []int32 `json:"choice_reward"`
	IsVariation  int32   `json:"is_variation"`
}

type Invite struct {
	Id           int64  `db:"id"`
	RoleId       int64  `db:"role_id"`
	InviteRoleId int64  `db:"invite_role_id"`
	InviteAvater string `db:"invite_avater"`
	InviteName   string `db:"invite_name"`
	AwardNum     int32  `db:"awarded_amount"`
	UpdateTime   int64  `db:"time"`
}

type RoleInvites struct {
	InviteMap      map[int64]*Invite
	InviteList     []*protocol.Invite
	AwardNumInvite *Invite
}
