package data

import "goproject/src/konglong/protocol"

type Venture struct {
	RoleId        int64   `db:"role_id"`
	Chapter       int32   `db:"chapter"`         // 当前正在进行中的章节
	Passed        int32   `db:"passed"`          // 该章节的已通关数
	BuffInfo      string  `db:"buff_info"`       // 已获得buff数
	UngetBuffInfo string  `db:"unget_buff_info"` // 未领取buff数
	BuffList      []int32 `db:"-"`               // 已获取的buff配置数组
	UngetBuffList []int32 `db:"-"`               // 未领取的buff配置数组
}

func (info *Venture) GetForClient() *protocol.Venture {
	return &protocol.Venture{
		Chapter:            info.Chapter,
		Passed:             info.Passed,
		BuffCfgIdList:      info.BuffList,
		UngetBuffCfgIdList: info.UngetBuffList,
	}
}

type AdventurePassCfg struct {
	CfgId       int32     `json:"index"`
	Pass        int32     `json:"pass"`
	Name        string    `json:"name"`
	Chapter     int32     `json:"chapter"`
	BossList    []int32   `json:"boss_list"`
	Reward      *AwardCfg `json:"reward"`
	BuffDropId1 int32     `json:"buff_drop_id1"`
	BuffDropId2 int32     `json:"buff_drop_id2"`
	BuffDropId3 int32     `json:"buff_drop_id3"`
}

type AdventureChapterCfg struct {
	Chapter    int32     `json:"index"`
	Name       string    `json:"name"`
	Reward     *AwardCfg `json:"reward"`
	PassAmount int32
	IsLast     bool

	PassCfgList []*AdventurePassCfg
}
