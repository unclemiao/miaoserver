package data

type SkillCfg struct {
	CfgId               int32 `json:"index"`
	StudySkill          int32 `json:"study_skill"`
	VariationStudySkill int32 `json:"bystudy_skill"`
}
