package data

type ParalCfg struct {
	CfgId        int32     `json:"index"`
	MonsterList  []int32   `json:"monster_list"`
	Award        *AwardCfg `json:"reward"`
	NeedPetLevel []int32   `json:"level"`
	RandAward    *AwardCfg `json:"rand_reward"`
}
