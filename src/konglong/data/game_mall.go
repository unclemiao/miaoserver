package data

type MallCfg struct {
	CfgId    int32     `json:"index"`
	SellKind int32     `json:"sell_kind"`
	SellItem *AwardCfg `json:"sell_item"`
	Diamonds int64     `json:"diamonds"`
	Gem      int64     `json:"pay"`
	Rmb      int64     `json:"rmb"`
	Order    int32     `json:"order"`
	Desc     string    `json:"desc"`
	Icon     string    `json:"icon"`
	MoreGem  int32     `json:"more_gem"`
	Limit    int32     `json:"limit"`
}

type RoleMall struct {
	Id     int64 `db:"id"`
	RoleId int64 `db:"role_id"`
	CfgId  int32 `db:"cfg_id"`
	Amount int32 `db:"amount"`
}

type RoleMalls struct {
	MallItemMap map[int32]*RoleMall
}
