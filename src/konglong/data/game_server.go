package data

import (
	"time"

	"goproject/engine/netWork/webSocket"
)

type ServerCmd struct {
	Id        int64  `db:"id"`
	Sid       int64  `db:"sid"`
	Name      string `db:"name"`
	Port      int32  `db:"port"`
	ClientUrl string `db:"client_url"`

	SqlHost   string `db:"sql_host"`
	SqlRoot   string `db:"sql_root"`
	SqlPass   string `db:"sql_pass"`
	SqlName   string `db:"sql_name"`
	State     int32  `db:"state"`
	Young     bool   `db:"young"`
	Token     string `db:"token"`
	Platform  string `db:"platform"`
	Channel   string `db:"channel"`
	Online    int32  `db:"online"`
	CreateNum int32  `db:"create_num"`

	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`

	JavaLogServerId int64 `orm:"description(java日志统计区服id)"`
	JavaLogGameId   int64 `orm:"description(java日志统计游戏id)"`
}
type ServerNode struct {
	Sid        int32  `db:"sid"`
	IsMerge    bool   `db:"is_merge"`
	BeginTime  int64  `db:"begin_time"`
	LastMinute int64  `db:"last_minute"`
	DataVer    int32  `db:"data_ver"`
	Host       string `db:"host"`
	SqlVersion int64  `db:"sql_version"`
}

type Server struct {
	Node *ServerNode

	// 网络底层接口
	Net *webSocket.NetServer

	// 节点数据
	NodeList     []*ServerNode
	NodeIdList   []int32
	NodeRolesMap map[int32]int32 // 所有节点的玩家数

	// 动态数据----------
	IsStarted   bool
	Stop        chan struct{}
	ReceiveChan chan *Context
	StopChan    chan struct{}
}
