package data

import "goproject/src/konglong/protocol"

type PetFoster struct {
	RoleId    int64 `db:"role_id"`
	PetId     int64 `db:"pet_id"`
	Grid      int32 `db:"grid"`
	BeginTime int64 `db:"begin_time"`
}

func (info *PetFoster) GetForClient() *protocol.PetFoster {
	return &protocol.PetFoster{
		PetId:     info.PetId,
		Grid:      info.Grid,
		BeginTime: info.BeginTime,
	}
}

type AllPetFoster struct {
	PetFosterList [4]*PetFoster
	FosterPetMap  map[int64]int32 // { petId = grid }
}

func (info *AllPetFoster) GetForClient() []*protocol.PetFoster {
	var list []*protocol.PetFoster
	for _, f := range info.PetFosterList {
		if f != nil {
			list = append(list, f.GetForClient())
		}
	}
	return list
}
