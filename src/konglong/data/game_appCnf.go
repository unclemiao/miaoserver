package data

type AppCnf struct {
	Server *ServerCfg
	Admin  *AdminCfg
}

type ServerCfg struct {
	Id       int64
	Sid      int32
	Name     string
	Platform string
	Channel  string
	SignKey  string
	Host     string
	Young    bool
	Port     int32
	IsMaster bool

	Sql *SqlCfg
}

type SqlCfg struct {
	Driver string
	Host   string
	Root   string
	Pass   string
	Name   string
}

type AdminCfg struct {
	Host string
	Sql  *SqlCfg
}
