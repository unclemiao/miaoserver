package data

type PetSuperEvolutionCfg struct {
	CfgId          int32     `json:"index"`
	Progress       int32     `json:"progress"`
	VariationStuff [][]int32 `json:"variation_stuff"`
	AttrStuff      [][]int32 `json:"attr_stuff"`
}

type PetSuperEvolution struct {
	Id       int64 `db:"id"`
	RoleId   int64 `db:"role_id"`
	CfgId    int32 `db:"cfg_id"`
	Progress int32 `db:"progress"`
}

type RolePetSuperEvolution struct {
	PetSuperEvolutionMap map[int32]*PetSuperEvolution
	FinishNum            int32
}

type PetSuperEvolutionUnlockAttrCfg struct {
	CfgId    int32 `json:"index"`
	Progress int32 `json:"progress"`
	Hp       int64 `json:"hp"`
	Atk      int32 `json:"atk"`
	Def      int32 `json:"def"`
	HpRate   int64 `json:"hp_rate"`
	AtkRate  int32 `json:"atk_rate"`
	DefRate  int32 `json:"def_rate"`
}

type PetSuperEvolutionBuyCfg struct {
	CfgId  int32 `json:"index"`
	Rate   int32 `json:"rate"`
	Price  int64 `json:"price"`
	AtMost int32 `json:"floors"`
}
