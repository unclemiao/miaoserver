package data

import "goproject/src/konglong/protocol"

type LimitTimeFeastCfg struct {
	CfgId        int32              `json:"index"`
	Name         string             `json:"name"`
	Kind         protocol.FeastKind `json:"kind"`
	Amount       int32              `json:"amount"`
	ChoiceReward []int32            `json:"choice_reward"`
	PetLevel     int32              `json:"pet_level"`
}
