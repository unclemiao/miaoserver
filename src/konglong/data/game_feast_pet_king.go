package data

import (
	"sync"
	"time"

	"goproject/src/konglong/protocol"
)

type FeastPetKingRank struct {
	FeastDoneList    []*FeastDone
	RoleFeastDoneMap map[int64]*FeastDone
	RoleLvList       []*Role
	Lock             sync.RWMutex
	On               bool
	FeastDone        *FeastDone
}

func (f *FeastPetKingRank) GetForClient() []*protocol.FeastDone {
	var proto []*protocol.FeastDone
	for _, fd := range f.FeastDoneList {
		proto = append(proto, &protocol.FeastDone{
			Id:      fd.Id,
			CfgId:   fd.CfgId,
			Done:    fd.Done,
			Awarded: fd.Awarded,
			Kind:    fd.Kind,
		})
	}
	return proto
}

type PetKingFightRecord struct {
	Id          int64     `db:"id"`
	AtkerId     int64     `db:"atker_id"`
	AtkerName   string    `db:"atker_name"`
	AtkerAvatar string    `db:"atker_avatar"`
	AtkerScore  int32     `db:"atker_score"`
	DeferId     int64     `db:"defer_id"`
	DeferName   string    `db:"defer_name"`
	DeferAvatar string    `db:"defer_avatar"`
	DeferScore  int32     `db:"defer_score"`
	CreateTime  time.Time `db:"create_time"`
}
