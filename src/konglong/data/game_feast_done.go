package data

import "goproject/src/konglong/protocol"

type FeastDone struct {
	Id         int64              `db:"id"`
	RoleId     int64              `db:"role_id"`
	CfgId      int32              `db:"cfg_id"`
	Kind       protocol.FeastKind `db:"kind"`
	Done       int32              `db:"done"`
	Awarded    bool               `db:"awarded"`
	UpdateTime int64              `db:"update_time"`
	Rank       int32

	FightWinAmount      int32   `db:"fight_win_amount"`
	FightLoseAmount     int32   `db:"fight_lose_amount"`
	FightInfoJson       string  `db:"fight_info_json"`
	FightInfoRoleIdList []int64 `db:"-"`

	Name   string `db:"-"`
	Avatar string `db:"-"`

	// 循环充值奖励
	AwardNum int32 `db:"award_num"`
}

type FeastLimitTime struct {
	FeastDoneList []*FeastDone
}

func (f *FeastLimitTime) GetForClient(tf *TimeFeast) []*protocol.FeastDone {
	var proto []*protocol.FeastDone
	for _, fd := range f.FeastDoneList {
		if fd.Kind == protocol.FeastKind_Newbie_Gift_Online {
			proto = append(proto, &protocol.FeastDone{
				Id:      fd.Id,
				CfgId:   fd.CfgId,
				Done:    int32(tf.Sec) - fd.Done,
				Awarded: fd.Awarded,
				Kind:    fd.Kind,
			})
		} else {
			proto = append(proto, &protocol.FeastDone{
				Id:      fd.Id,
				CfgId:   fd.CfgId,
				Done:    fd.Done,
				Awarded: fd.Awarded,
				Kind:    fd.Kind,
			})
		}

	}
	return proto
}

type FeastPays struct {
	Feast *FeastDone
}

func (f *FeastPays) GetForClient() []*protocol.FeastDone {
	var proto []*protocol.FeastDone
	if f.Feast != nil {
		proto = append(proto, &protocol.FeastDone{
			Id:          f.Feast.Id,
			CfgId:       f.Feast.CfgId,
			Done:        f.Feast.Done,
			Awarded:     f.Feast.Awarded,
			Kind:        f.Feast.Kind,
			AwardAmount: f.Feast.AwardNum,
		})
	}

	return proto
}

type FeastMonthCards struct {
	Feast *FeastDone
}

func (f *FeastMonthCards) GetForClient() []*protocol.FeastDone {
	var proto []*protocol.FeastDone
	if f.Feast != nil {
		proto = append(proto, &protocol.FeastDone{
			Id:          f.Feast.Id,
			CfgId:       f.Feast.CfgId,
			Done:        f.Feast.Done,
			Awarded:     f.Feast.Awarded,
			Kind:        f.Feast.Kind,
			AwardAmount: f.Feast.AwardNum,
		})
	}

	return proto
}
