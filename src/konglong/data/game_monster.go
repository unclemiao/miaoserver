package data

type CMon struct {
	Index  int32   `json:"index"`
	IsBoss int32   `json:"isboss"`
	Skill  []int32 `json:"skill1"`

	Atk     int32 `json:"atk"`
	AtkTime int32 `json:"atktime"`
	Crit    int32 `json:"crit"`
	Def     int32 `json:"def"`
	Dis     int32 `json:"dis"`
	Dodge   int32 `json:"dodge"`
	Hp      int32 `json:"hp"`
}
