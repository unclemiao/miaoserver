package data

type Subscribe struct {
	Id          int64  `db:"id"`
	RoleId      int64  `db:"role_id"`
	AccountName string `db:"account_name"`
	Kind        string `db:"kind"`
	State       int32  `db:"state"`
}

type RoleSubscribe struct {
	SubscribeMap map[string]*Subscribe
}

type WXAccessToken struct {
	AccessToken string `json:"access_token"`
	ExpiresIn   int64  `json:"expires_in"`
	Errcode     int32  `json:"errcode"`
	Errmsg      string `json:"errmsg"`
}

type WXSubscribeSend struct {
	AccessToken string               `json:"access_token"`
	Touser      string               `json:"touser"`
	TemplateId  string               `json:"template_id"`
	Data        *WXSubscribeSendData `json:"data"`
}

type WXSubscribeSendData struct {
	Number2 *WXSubscribeSendDataSub `json:"number2"`
	Number3 *WXSubscribeSendDataSub `json:"number3"`
	Thing1  *WXSubscribeSendDataSub `json:"thing1"`
	Thing2  *WXSubscribeSendDataSub `json:"thing2"`
	Thing5  *WXSubscribeSendDataSub `json:"thing5"`
	Thing6  *WXSubscribeSendDataSub `json:"thing6"`
	Thing7  *WXSubscribeSendDataSub `json:"thing7"`
}

type WXSubscribeSendDataSub struct {
	Value interface{} `json:"value"`
}
