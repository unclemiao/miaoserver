package data

import (
	"goproject/src/konglong/protocol"
)

type EventItf interface {
	IsNetEvent() bool
	GetEnvMsg() *protocol.Envelope
	GetMsgId() MsgIdItf
	SetMsgId(MsgIdItf)
}

type MsgIdItf interface {
	String() string
	Int32() int32
}

type EventBase struct {
	MsgId MsgIdItf // 事件枚举
}

func (m *EventBase) IsNetEvent() bool {
	return false
}

func (m *EventBase) SetMsgId(msgId MsgIdItf) {
	m.MsgId = msgId
}

func (m *EventBase) GetMsgId() MsgIdItf {
	return m.MsgId
}

func (m *EventBase) GetEnvMsg() *protocol.Envelope {
	return nil
}

type EventFunc struct {
	MsgId int32
	Order int32
	Func  func(msg *Context, args interface{})
}

type NetFunc struct {
	MsgId int32
	Order int32
	Func  func(msg *Context, args *protocol.Envelope)
}
