package data

type EventTimer struct {
	EventBase
	Func func(ctx *Context)
	Ctx  *Context
}
