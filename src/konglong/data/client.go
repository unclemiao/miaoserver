package data

import (
	"sync"

	"github.com/gorilla/websocket"
)

type Conn struct {
	WsConn      *websocket.Conn
	AccountName string
	PlayerId    string
	Lock        sync.RWMutex
	BeginTime   int64
	EndTime     int64
}
