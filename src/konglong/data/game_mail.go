package data

import (
	"time"

	"goproject/engine/util"
	"goproject/src/konglong/protocol"
)

// 全服公共邮件
type Mail struct {
	Name     string    `db:"name"`  // 名字
	Title    string    `db:"title"` // 标题
	Time     time.Time `db:"time"`  // 发件时间
	TimeOut  time.Time `db:"time_out"`
	Body     string    `db:"body"` // 内容
	EachStr  string    `db:"each"` // 是否给全服发 [0] 表示全服邮件， [] 表示个人邮件， [1,2,3]表示发给某个服的玩家
	Each     []int32   `db:"-"`
	AwardStr string    `db:"award"`     // 邮件内容
	GiftCode string    `db:"gift_code"` // 礼包码
	Award    *AwardCfg `db:"-"`
}

func (m *Mail) GetForClient() *protocol.Mail {
	return &protocol.Mail{
		Name:      m.Name,
		Title:     m.Title,
		Time:      m.Time.Unix(),
		Body:      m.Body,
		Reward:    m.Award.GetForClient(),
		AwardTime: 0,
		GiftCode:  m.GiftCode,
		IsRead:    false,
		TimeOut:   util.ChoiceInt64(m.TimeOut.IsZero(), 0, m.TimeOut.Unix()),
	}
}

// 个人邮件
type Mailee struct {
	Name      string    `db:"name"`
	RoleId    int64     `db:"role_id"`
	Title     string    `db:"title"`
	AwardStr  string    `db:"award"`
	Award     *AwardCfg `db:"-"`
	GiftCode  string    `db:"gift_code"`
	Body      string    `db:"body"`
	IsDel     bool      `db:"is_del"`
	IsRead    bool      `db:"is_read"`
	Time      time.Time `db:"time"`
	TimeOut   time.Time `db:"time_out"`
	AwardTime time.Time `db:"award_time"`
}

func (m *Mailee) GetForClient() *protocol.Mail {
	return &protocol.Mail{
		Name:      m.Name,
		Title:     m.Title,
		Time:      util.ChoiceInt64(m.Time.IsZero(), 0, m.Time.Unix()),
		Body:      m.Body,
		Reward:    m.Award.GetForClient(),
		AwardTime: util.ChoiceInt64(m.AwardTime.IsZero(), 0, m.AwardTime.Unix()),
		GiftCode:  m.GiftCode,
		IsRead:    m.IsRead,
		TimeOut:   util.ChoiceInt64(m.TimeOut.IsZero(), 0, m.TimeOut.Unix()),
	}
}

type AllMail struct {
	MailList []*Mail
	MailMap  map[string]*Mail
}
