package data

import "goproject/engine/util"

type SkillDropCfg struct {
	CfgId       int32 `json:"index"`
	DropId      int32 `json:"drop_id"`
	SkillId     int32 `json:"skill_id"`
	Weight      int64 `json:"weight"`
	TotalWeight int64
}

func (cfg *SkillDropCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *SkillDropCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *SkillDropCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *SkillDropCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *SkillDropCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type SkillDropGroupCfg struct {
	DropId         int32 `json:"drop_id"`
	DropCfgMap     map[int32]*SkillDropCfg
	DropWeightList []util.WeightItr
}
