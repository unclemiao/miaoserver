package data

import "goproject/engine/util"

type ItemDropCfg struct {
	CfgId       int32 `json:"index"`
	DropId      int32 `json:"drop_id"`
	ItemId      int64 `json:"item_id"`
	Weight      int64 `json:"weight"`
	TotalWeight int64
}

func (cfg *ItemDropCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *ItemDropCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *ItemDropCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *ItemDropCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *ItemDropCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type ItemDropGroupCfg struct {
	DropId         int32 `json:"drop_id"`
	DropCfgMap     map[int32]*ItemDropCfg
	DropWeightList []util.WeightItr
}
