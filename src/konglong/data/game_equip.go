package data

// 装备品阶
type EquipGrade struct {
	CfgId       int32 `json:"equip_id"`
	Grade       int32 `json:"grade"`
	Weight      int64 `json:"weight"`
	SkillDropId int32 `json:"skill_drop_id"`

	HpRate  int32 `json:"hp_rate"`
	AtkRate int32 `json:"atk_rate"`
	DefRate int32 `json:"def_rate"`

	HpAddRate  []int32 `json:"hp_add_rate"`
	AtkAddRate []int32 `json:"atk_add_rate"`
	DefAddRate []int32 `json:"def_add_rate"`

	HpRange      []int64 `json:"hp_range"`
	AtkRange     []int32 `json:"atk_range"`
	DefRange     []int32 `json:"def_range"`
	CritRange    []int32 `json:"crit_range"`
	DodgeRange   []int32 `json:"dodge_range"`
	AtkTimeRange []int32 `json:"atktime_range"`
	RandNum      int32   `json:"rand_num"`
	AttrNum      int32

	TotalWeight int64
}

func (cfg *EquipGrade) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *EquipGrade) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *EquipGrade) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *EquipGrade) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *EquipGrade) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type EquipStarCfg struct {
	CfgId      int64  `json:"index"`
	Star       int32  `json:"star"`
	Name       string `json:"name"`
	SkillLevel int32  `json:"skill_level"`
	HpRate     int32  `json:"hp"`
	AtkRate    int32  `json:"atk"`
	DefRate    int32  `json:"def"`
	SelfNum    int32  `json:"self_num"`
	SGradeNum  int32  `json:"s_grade_num"`
	AGradeNum  int32  `json:"a_grade_num"`
	BGradeNum  int32  `json:"b_grade_num"`
	CGradeNum  int32  `json:"c_grade_num"`
}

type CheckEquipStar struct {
	SelfNum   int32
	SGradeNum int32
	AGradeNum int32
	BGradeNum int32
	CGradeNum int32
}

// 回收
type EquipResolveCfg struct {
	Star   int32     `json:"index"`
	Reward *AwardCfg `json:"resolve_reward"`
}
