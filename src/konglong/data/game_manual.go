package data

import "goproject/src/konglong/protocol"

type Manual struct {
	Id                         int64               `db:"id"`
	RoleId                     int64               `db:"role_id"`
	Kind                       protocol.ManualKind `db:"kind"`
	PetCfgId                   int32               `db:"pet_cfg_id"`
	EvolutionLv                int32               `db:"evolution_lv"`
	StarMax                    int32               `db:"star_max"`
	HasNormal                  bool                `db:"has_normal"`
	HasVariation               bool                `db:"has_variation"`
	VariationEvolutionLv       int32               `db:"variation_evolution_lv"`
	VariationStarMax           int32               `db:"variation_star_max"`
	HasVariationSuperEvolution bool                `db:"has_variation_super_evolution"`
	HasSuperEvolution          bool                `db:"has_super_evolution"`

	// 道具图鉴
	ItemCfgId int32 `db:"item_cfg_id"`

	Awarded bool `db:"awarded"`
}

func (info *Manual) GetForClient() *protocol.Manual {
	return &protocol.Manual{
		PetCfgId:                   info.PetCfgId,
		EvolutionLevel:             info.EvolutionLv,
		HasVariation:               info.HasVariation,
		StarMax:                    info.StarMax,
		VariationEvolutionLevel:    info.VariationEvolutionLv,
		HasNormal:                  info.HasNormal,
		VariationStarMax:           info.VariationStarMax,
		Kind:                       info.Kind,
		ItemCfgId:                  info.ItemCfgId,
		Awarded:                    info.Awarded,
		HasVariationSuperEvolution: info.HasVariationSuperEvolution,
		HasSuperEvolution:          info.HasSuperEvolution,
	}
}

type RoleManual struct {
	PetManualMap  map[int32]*Manual
	FoodManualMap map[protocol.ItemSubKind]*Manual
}

func (info *RoleManual) GetForClient() []*protocol.Manual {
	var list []*protocol.Manual
	for _, manual := range info.PetManualMap {
		list = append(list, manual.GetForClient())
	}
	for _, manual := range info.FoodManualMap {
		list = append(list, manual.GetForClient())
	}
	return list
}

type FoodManualCfg struct {
	FoodSubKind protocol.ItemSubKind `json:"food_sub_kind"`
	Reward      *AwardCfg            `json:"reward"`
}
