package data

import "goproject/src/konglong/protocol"

type PetBreed struct {
	RoleId       int64 `db:"role_id"`
	Grid         int32 `db:"grid"`
	PetId1       int64 `db:"pet1"`
	PetId2       int64 `db:"pet2"`
	BreedEndTime int64 `db:"breed_end_time"`
	BreedFastNum int32 `db:"breed_fast_num"`
}

func (info *PetBreed) GetForClient() *protocol.PetBreed {
	return &protocol.PetBreed{
		Grid:         info.Grid,
		Pet1:         info.PetId1,
		Pet2:         info.PetId2,
		BreedEndTime: info.BreedEndTime,
		BreedFastNum: info.BreedFastNum,
	}
}

type RolePetBreeds struct {
	PetBreedList []*PetBreed
	PetGridMap   map[int64]int32
}

func (info *RolePetBreeds) GetForClient() []*protocol.PetBreed {
	protocolList := make([]*protocol.PetBreed, 0, len(info.PetBreedList))
	for _, breed := range info.PetBreedList {
		if breed != nil {
			protocolList = append(protocolList, breed.GetForClient())
		}
	}
	return protocolList
}

type BreedWeight struct {
	CfgId       int32 `json:"cfg_id"`
	Breed       int32 `json:"breed"`
	Weight      int64 `json:"weight"`
	TotalWeight int64
}

func (cfg *BreedWeight) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *BreedWeight) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *BreedWeight) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *BreedWeight) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *BreedWeight) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}
