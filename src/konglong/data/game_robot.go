package data

type RobotCfg struct {
	CfgId       int32   `json:"index"`
	Name        string  `json:"name"`
	FighterList []int32 `json:"fighter_list"`
}
