package data

import (
	"goproject/src/konglong/protocol"
)

type AllItem struct {
	RoleId        int64
	ItemMap       map[int64]*Item
	ItemMapForKit map[protocol.KitType][]*Item
	OwnerItems    map[int64]map[int64]*Item // 穿在恐龙身上的装备 { ownerId={ itemId=item } }
	CurrencyMap   map[int64]*Item           // 货币  { cfgId = *Item }
	DustbinMap    map[int64]*Item           // 货币  { cfgId = *Item }
}

type Item struct {
	Id      int64            `db:"id"`       // id
	RoleId  int64            `db:"role_id"`  // 角色id
	OwnerId int64            `db:"owner_id"` // 拥有者id
	Cid     int64            `db:"cid"`      // 配置id
	N       int64            `db:"n"`        // 数量
	Grid    int32            `db:"grid"`     // 格子号
	Kit     protocol.KitType `db:"kit"`      // 栏位
	Due     int64            `db:"due"`      // 超时时间

	// 装备相关
	HpMaxBase   int64  `db:"hp_max_base"`  // 增加血量上限
	AtkBase     int32  `db:"atk_base"`     // 增加攻击
	DefBase     int32  `db:"def_base"`     // 增加防御
	Grade       int32  `db:"grade"`        // 品阶
	Star        int32  `db:"star"`         // 星级
	SkillCfgId  int32  `db:"skill_cfg_id"` // 技能
	OpenCfgId   int64  `db:"open_cfg_id"`
	RandAttr    *Attr  `db:"-"`             // 随机属性
	RandAttrStr string `db:"rand_attr_str"` // 随机属性json
}

type ItemA struct {
	Id      int64  `json:"id"`
	RoleId  int64  `json:"role_id"`
	OwnerId int64  `json:"owner_id"`
	Cid     int64  `json:"cid"`  // 配置id
	N       uint64 `json:"n"`    // 数量
	Grid    int32  `json:"grid"` // 格子号
	Kit     int32  `json:"kit"`  // 栏位
	Due     int64  `json:"due"`  // 超时时间

	// 装备相关
	HpMaxBase   int64  `json:"hp_max_base"`   // 增加血量上限
	AtkBase     int32  `json:"atk_base"`      // 增加攻击
	DefBase     int32  `json:"def_base"`      // 增加防御
	Grade       int32  `json:"grade"`         // 品阶
	Star        int32  `json:"star"`          // 星级
	SkillCfgId  int32  `json:"skill_cfg_id"`  // 技能
	RandAttr    *Attr  `json:"-"`             // 随机属性
	RandAttrStr string `json:"rand_attr_str"` // 随机属性json
}

type ItemCfg struct {
	CfgId      int64                `json:"index"`
	Name       string               `json:"name"`
	Grade      int32                `json:"grade"`
	Kind       protocol.ItemKind    `json:"kind"`
	SubKind    protocol.ItemSubKind `json:"sub_kind"`
	Level      int32                `json:"level"`
	Price      []int64              `json:"price"`
	Award      *AwardCfg            `json:"gift"`
	Exp        uint64               `json:"exp"`
	AddRoleExp int64                `json:"add_role_exp"`
	PassLevel  int32                `json:"passLevel"`
	N9         int64                `json:"amount_max"`
	Due        int64                `json:"due"`
	PetDropId  int32                `json:"pet_drop_id"`
	Args1      []int32              `json:"args1"`

	HpMaxBase int64 `json:"hp"`  // 增加血量上限
	AtkBase   int32 `json:"atk"` // 增加攻击
	DefBase   int32 `json:"def"` // 增加防御
}

type ItemAmount struct {
	CfgId    int64
	N        int64
	Rate     int32
	AutoLoad bool // 是否自动穿戴
}

type MakeCfg struct {
	CfgId          int64     `json:"index"`
	Name           string    `json:"name"`
	Stuff          *AwardCfg `json:"stuff"`
	AddFoodBookExp int64     `json:"add_food_book_exp"`
}

type AwardCfg struct {
	AwardList   []*ItemAmount
	Why         int32
	TotalPetNum int64
}

func (cfg *AwardCfg) GetForClient() []*protocol.ItemAmount {
	if cfg == nil {
		return []*protocol.ItemAmount{}
	}
	list := make([]*protocol.ItemAmount, 0, len(cfg.AwardList))
	for _, award := range cfg.AwardList {
		list = append(list, &protocol.ItemAmount{
			CfgId: award.CfgId,
			Num:   award.N,
		})
	}
	return list
}

type FormulaCfg struct {
	CfgId       int64  `json:"index"`
	ItemCfgId   int64  `json:"item_cfg_id"`
	StuffCfgId1 int64  `json:"stuff_cfg_id_1"`
	StuffCfgId2 int64  `json:"stuff_cfg_id_2"`
	Name        string `json:"name"`
}

// 保留食物，免费升级概率配置
type MakeProbCfg struct {
	CfgId    int64          `json:"index"`
	Kind     int32          `json:"type"`
	FoodLv   int32          `json:"unlock"`
	Rate     int32          `json:"rate"`
	LevelMax map[int32]bool `json:"nLevel"`
}

// 动态存储合成结果
type MakeItemDone struct {
	Grid          int32 // 目标格子号
	ItemCfgId     int64 // 需要合成的道具id
	ItemId1       int64 // 源道具id1
	ItemId2       int64 // 源道具id2
	State         int32 // 合成结果 1=免费升级	2=保留食物
	FreeUpItemId  int64 // 需要免费升级的道具id
	KeepItemCfgId int64 // 需要保留的道具配置id
}
