package data

type HttpResult struct {
	Code   int32
	Result interface{}
	RoleId int64
}

type CenterResp struct {
	Code int32
	Err  string
	Data interface{}
}
