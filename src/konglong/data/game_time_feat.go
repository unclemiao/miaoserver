package data

import (
	"encoding/json"
	"time"

	"goproject/engine/assert"
	"goproject/src/konglong/protocol"
)

type TimeFeastAward struct {
	DaySecAwarded map[int64]int32 // 每日在线奖励信息
	SecAwarded    map[int64]int32 // 总在线奖励
	ViewHangAward *AwardCfg       // 查看的在线挂机奖励
}

type TimeFeast struct {
	RoleId             int64           `db:"role_id"`
	NewRole            time.Time       `db:"new_role"`              // 创建角色时间
	Ip                 string          `db:"ip"`                    // 最后登录ip
	LastSec            int64           `db:"last_sec"`              // 上次更新时刻 秒
	Young              int64           `db:"young"`                 // 防沉迷在线时间  秒
	DaySec             int64           `db:"day_sec"`               // 今日在线时间 秒
	WeekSec            int64           `db:"week_sec"`              // 本周在线时间 秒
	LastWeekSec        int64           `db:"last_week_sec"`         // 上周在线时间 秒
	WeekDay            int32           `db:"week_day"`              // 本周在线天数
	Sec                int64           `db:"sec"`                   // 总在线时间 秒
	Signin             int32           `db:"signin"`                // 累计登录天数
	LastViewHangSec    int64           `db:"last_view_hang_sec"`    // 上次查看在线奖励时间 秒
	LastHangAwardedSec int64           `db:"last_hang_awarded_sec"` // 上次领取挂机奖励时间 秒
	AwardStr           string          `db:"award_str"`             // 在线奖励信息
	AwardInfo          *TimeFeastAward `db:"-"`
	NextDropBoxTime    int64           `db:"next_drop_box_time"` // 下次宝箱掉落时间
}

func (tf *TimeFeast) ChangeAward() {
	bs, err := json.Marshal(tf.AwardInfo)
	assert.Assert(err == nil, err)
	tf.AwardStr = string(bs)
}

func (tf *TimeFeast) GetForClient() *protocol.TimeFeast {
	return &protocol.TimeFeast{DaySec: int32(tf.DaySec), Signin: tf.Signin}
}
