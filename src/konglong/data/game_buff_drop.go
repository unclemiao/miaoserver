package data

import "goproject/engine/util"

type BuffDropCfg struct {
	ArraryIndex int32
	CfgId       int32 `json:"index"`
	DropId      int32 `json:"drop_id"`
	BuffId      int32 `json:"buff_id"`
	Weight      int64 `json:"weight"`
	TotalWeight int64
}

func (cfg *BuffDropCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *BuffDropCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *BuffDropCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *BuffDropCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *BuffDropCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}

type BuffDropGroupCfg struct {
	DropId         int32 `json:"drop_id"`
	DropCfgMap     map[int32]*BuffDropCfg
	DropWeightList []util.WeightItr
}
