package data

import "goproject/src/konglong/protocol"

type GamePassCfg struct {
	CfgId              int32     `json:"index"`
	Name               string    `json:"passName"`
	MonsterList        []int32   `json:"monster_list"`
	BossList           []int32   `json:"boss_list"`
	MonAward           *AwardCfg `json:"mon_reward"`
	FastMinute         int64     `json:"addspnum"`
	BossAward          *AwardCfg `json:"boss_reward"`
	CurrencyReward     *AwardCfg `json:"currency_reward"`
	CurrencyRewardArgs int32     `json:"currency_reward_args"`
	ItemReward         *AwardCfg `json:"item_reward"`
	RoleLevel          int32     `json:"role_level"`
}

type GamePassExtAwardCfg struct {
	CfgId             int32   `json:"index"`
	ItemCfgId         int64   `json:"rewardid"`
	AmountRange       []int32 `json:"count"`
	Award             *AwardCfg
	NormalWeight      int64 `json:"normalrate"`
	NormalTotalWeight int64
	FirstWeight       int64 `json:"firstrate"`
	FirstTotalWeight  int64
}

func (cfg *GamePassExtAwardCfg) GetCfgId() int32 {
	return cfg.CfgId
}

type NewbieBossCfg struct {
	Index       int32     `json:"index"`
	Name        string    `json:"name"`
	MonsterList []int32   `json:"monster_list"`
	Award       *AwardCfg `json:"reward"`
	ExtAward    *AwardCfg `json:"extReward"`
}

type NewbieBossPass struct {
	Id         int64 `db:"id"`
	RoleId     int64 `db:"role_id"`
	CfgIndex   int32 `db:"cfg_index"`
	Awarded    bool  `db:"awarded"`
	ExtAwarded bool  `db:"ext_awarded"`
	Passed     bool  `db:"passed"`
}

func (info *NewbieBossPass) GetForClient() *protocol.NewbieBossPass {
	return &protocol.NewbieBossPass{
		Index:      info.CfgIndex,
		Awarded:    info.Awarded,
		ExtAwarded: info.ExtAwarded,
		Passed:     info.Passed,
	}
}

type NewbieBoss struct {
	RoleId   int64
	PassList []*NewbieBossPass
}

func (info *NewbieBoss) GetForClient() []*protocol.NewbieBossPass {
	var clientList []*protocol.NewbieBossPass
	for _, pass := range info.PassList {
		clientList = append(clientList, pass.GetForClient())
	}
	return clientList
}

type GameDropCfg struct {
	CfgId     int32 `json:"index"`
	Weight    int64 `json:"weather"`
	KillMon   int32 `json:"killmonsterTime"`
	PassNum   int32 `json:"passNum"`
	FoodLv    int32 `json:"foodLevel"`
	ItemCfgId int64 `json:"item_cfg_id"`
	Limit     int32 `json:"limit"`

	TotalWeight int64
}

func (cfg *GameDropCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *GameDropCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *GameDropCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *GameDropCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *GameDropCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}
