package data

type SystemChat struct {
	Id        int64  `db:"id"`
	Kind      int32  `db:"kind"`
	Msg       string `db:"msg"`
	BeginTime int64  `db:"begin_time"`
	EndTime   int64  `db:"end_time"`
	SubTime   int64  `db:"sub_time"`
	NextTime  int64  `db:"next_time"`
}
