package data

type ServerExtData struct {
	Id       int64  `json:"id"`
	TypeCode string `json:"typeCode"`
	TypeName string `json:"typeName"`
	Key      string `json:"datakey"`
	Value    string `json:"dataVal"`
}

type ServerExtDataAdSdk struct {
	Id       int64  `json:"id"`
	Company  string `json:"company"`
	GameName string `json:"game_name"`
	AppId    string `json:"app_id"`
	Icon     string `json:"icon"`
	State    int32  `json:"state"`
}

type AdSdkApp struct {
	Id      int64  `json:"id"`
	AppName string `json:"appName"`
	AppId   string `json:"appId"`
	Icon    string `json:"icon"`
	Path    string `json:"path"`
}

type AdSdkSlots struct {
	Id       int64       `json:"id"`
	SlotNum  int32       `json:"slotNum"`
	Position string      `json:"position"`
	Apps     []*AdSdkApp `json:"apps"`
	AppMap   map[string]*AdSdkApp
}

type AdSdkSlotsResp struct {
	Result   int32         `json:"result"`
	Msg      string        `json:"msg"`
	ErrMsg   string        `json:"errMsg"`
	Status   int32         `json:"status"`
	Data     []*AdSdkSlots `json:"data"`
	DateTime int64         `json:"dateTime"`
}

type AdSdkAwardInfo struct {
	Id     int64  `db:"id"`
	RoleId int64  `db:"role_id"`
	AppId  string `db:"appid"`
	State  int32  `db:"state"` // 0 = 未玩游戏， 1=可领取， 2=已领取
}

type RoleAdSdkAwardInfo struct {
	DataMap map[int64]*AdSdkAwardInfo
}
