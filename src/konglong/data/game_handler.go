package data

import (
	"github.com/kataras/golog"
	"goproject/engine/netWork/webSocket"
)

type HandlerManager struct {
	logs *golog.Logger
	net  *webSocket.NetServer
}
