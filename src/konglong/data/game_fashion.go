package data

type Fashion struct {
	Id     int64 `db:"id"`
	RoleId int64 `db:"role_id"`
	CfgId  int32 `db:"cfg_id"`
	State  int32 `db:"state"` // 1=激活 2=穿戴
}

type FashionCond struct {
	Kind int32
	Cond int32
}

type FashionCfg struct {
	CfgId         int32          `json:"index"`
	Kind          int32          `json:"kind"`
	ActivateCond  []*FashionCond `json:"activate_cond"`
	ActivateStuff *AwardCfg      `json:"activate_stuff"`
	HpMax         int64          `json:"hp"`
	Atk           int32          `json:"atk"`
	Def           int32          `json:"def"`
	HpMaxRate     int32          `json:"hp_rate"`
	AtkRate       int32          `json:"atk_rate"`
	DefRate       int32          `json:"def_rate"`
}

type RoleFashion struct {
	FashionMap map[int32]*Fashion // { cfgId = *Fashion }
	LoadingMap map[int32]int32    // { kind = cfgId }
}
