package data

type EventOnPetAdd struct {
	EventBase
	RoleId    int64
	Pet       *Pet
	IsNewRole bool
}
