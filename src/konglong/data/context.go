package data

import (
	"time"

	"goproject/engine/sql"
)

type Context struct {
	MsgId    MsgIdItf // 事件枚举
	EventMsg EventItf // 消息参数结构

	Session    *Session // 链接
	Sql        *sql.Sql
	Sid        int32
	AccountId  string // 账号id
	RoleId     int64  // 角色id
	Stop       bool   // 是否停止后续事件
	BeginTime  int64  // 事件开始时间(毫秒)
	NowMill    int64  // 事件发生时间(毫秒)
	NowSec     int64  // 事件发生时间(秒)
	NowTime    time.Time
	EndTime    int64                    // 事件结束时间(毫秒)
	FinishFunc func(errMsg interface{}) // 事件结束后回调

	// 动态数据
	IsFirstPet        bool
	IsSecondPet       bool
	TryS              bool
	TrySuperEvolution bool
	PetLevel          int32
	SendAtOnce        bool
	OldLastSec        int64
	ItemMustGive      bool
	IsTenLottery      bool
	IsUseGoldAddItem  bool
	ArgsInt           int64
	ArgsStr           string
}
