package data

type GameFlag struct {
	Id         int64
	Sid        int64
	Platform   string
	Channel    string
	CenterHost string
	AdminHost  string
	Port       int64
	IsMaster   bool
	LogPath    string
}
