package data

type FettersCfg struct {
	CfgId     int32 `json:"index"`
	GroupId   int32 `json:"group"`
	PreCfgId  int32
	NextCfgId int32
	MainPetId int32 `json:"main_pet_id"`
	SubPetId  int32 `json:"sub_pet_id"`
	Level     int32 `json:"level"`

	MainPetStar      int32     `json:"main_pet_star"`
	MainPetVariation int32     `json:"main_pet_variation"`
	SubPetStar       int32     `json:"sub_pet_star"`
	SubPetVariation  int32     `json:"sub_pet_variation"`
	Reward           *AwardCfg `json:"reward"`

	Hp  int64 `json:"hp"`
	Atk int32 `json:"atk"`
	Def int32 `json:"def"`

	HpRate  int64 `json:"hp_rate"`
	AtkRate int32 `json:"atk_rate"`
	DefRate int32 `json:"def_rate"`
}

type Fetters struct {
	Id      int64 `db:"id"`
	RoleId  int64 `db:"role_id"`
	CfgId   int32 `db:"cfg_id"`
	Awarded bool  `db:"awarded"`
}

type RoleFetters struct {
	FettersMap map[int64]*Fetters

	// { cfgId=*Fetters }
	FettersCfgIdMap map[int32]*Fetters
}
