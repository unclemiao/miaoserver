package data

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"
	"goproject/src/konglong/protocol"
)

type MsgNode struct {
	Msg      *protocol.Envelope
	NextNode *MsgNode
}

type MsgBuff struct {
	Head *MsgNode
	Last *MsgNode
}
type HttpSessionBase struct {
	W       http.ResponseWriter
	R       *http.Request
	TrackId string
}

type ProtoType int32

const (
	PROTO_TYPE_HTTP ProtoType = 1
	PROTO_TYPE_NET  ProtoType = 2
)

type Session struct {
	HttpSessionBase
	Conn       *websocket.Conn
	ProtoType  ProtoType
	Id         int64
	Ip         string
	AccountId  string
	RoleId     int64
	From       string
	Young      bool
	Sid        int32
	SignInTime int64

	LastAuthAd int64

	// 消息缓冲
	MsgBuff *MsgBuff
	// 消息发送管道
	WriteChan chan *protocol.Envelope
	StopChan  chan chan struct{}
	IsStop    bool

	Lock sync.RWMutex
}
