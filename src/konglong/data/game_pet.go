package data

import "goproject/src/konglong/protocol"

type Pet struct {
	Id             int64  `db:"id"`
	RoleId         int64  `db:"role_id"`
	CfgId          int32  `db:"cfg_id"`
	Level          int32  `db:"level"`
	Exp            uint64 `db:"exp"`
	Evolution      int32  `db:"evolution"` // 进化次数
	SuperEvolution bool   `db:"super_evolution"`
	Star           int32  `db:"star"`
	Variation      bool   `db:"variation"` // 变异
	TryVariation   bool   `db:"try_variation"`
	AtkAdd         int32  `db:"atk_add"`
	DefAdd         int32  `db:"def_add"`
	HpMaxAdd       int64  `db:"hp_max_add"`
	Name           string `db:"name"`

	Sex                    int32   `db:"sex"`
	Breed                  int32   `db:"breed"`
	SkillCfgId             int32   `db:"skill_cfg_id"`
	StudySkillCfgIdListStr string  `db:"study_skill_cfg_id_list_str"`
	StudySkillCfgIdList    []int32 `db:"-"`

	// 阵型数据
	StubIndex int32 `db:"stub_index"`
	HelpIndex int32 `db:"help_index"`

	// 忠诚度
	Loyal int32 `db:"loyal"`

	Attr *Attr `db:"-"`
}

func (pet *Pet) GetForClient() *protocol.Pet {
	return &protocol.Pet{
		Id:             pet.Id,
		CfgId:          pet.CfgId,
		Level:          pet.Level,
		Star:           pet.Star,
		IsVariation:    pet.Variation,
		Sex:            pet.Sex,
		Exp:            int64(pet.Exp),
		Breed:          pet.Breed,
		StubIndex:      pet.StubIndex,
		HelpIndex:      pet.HelpIndex,
		Atk:            pet.Attr.Atk,
		AtkTime:        pet.Attr.AtkTime,
		Crit:           pet.Attr.Crit,
		Def:            pet.Attr.Def,
		Dis:            pet.Attr.Dis,
		Dodge:          pet.Attr.Dodge,
		HpMax:          pet.Attr.HpMax,
		Evolution:      pet.Evolution,
		AtkAdd:         pet.AtkAdd,
		DefAdd:         pet.DefAdd,
		HpMaxAdd:       pet.HpMaxAdd,
		Loyal:          pet.Loyal,
		SkillPetCfgId:  pet.SkillCfgId,
		Name:           pet.Name,
		SuperEvolution: pet.SuperEvolution,
		StudySkillList: pet.StudySkillCfgIdList,
	}
}

type PetCfg struct {
	CfgId                       int32     `json:"index"`
	Name                        string    `json:"name"`
	SuperEvolutionName          string    `json:"super_evolution_name"`
	VariationSuperEvolutionName string    `json:"variation_super_evolution_name"`
	Grade                       int32     `json:"xiyou"`
	EvolutionLvList             []int32   `json:"jinhuaLevel"` // 进化等级
	VariationRate               int32     `json:"bianyiRate"`  // 变异概率 千分比
	Vegetarian                  int32     `json:"eat"`         // 1 = 肉食， 2=素食
	CanStarLevel                int32     `json:"upstarLevel"`
	SelfPiecesStuff             *AwardCfg `json:"self_pieces_stuff"`
	PiecesStuff                 *AwardCfg `json:"pieces_stuff"`
	Skills                      []int32   `json:"skills"`
	SkillCfgId                  int32
	StudySlotRate               [][]int32 `json:"study_slot_rate"`
	SkillStudyStar              []int32   `json:"skill_study_star"`

	// 喜好
	LikeTypeList []int32 `json:"like_list"`
	HateTypeList []int32 `json:"hate_list"`

	Atk     int32 `json:"atk"`     // 攻击力
	AtkTime int32 `json:"atktime"` // 攻击时间间隔
	Crit    int32 `json:"crit"`    // 暴击 （千分比）
	Def     int32 `json:"def"`     // 防御
	Dis     int32 `json:"dis"`     // 攻击距离
	Dodge   int32 `json:"dodge"`   // 闪避 （千分比）
	HpMax   int64 `json:"hp"`      // 血量上限

	// 等级成长加成
	AtkAdd   int32 `json:"atkAdd"`   // 攻击力
	CritAdd  int32 `json:"critAdd"`  // 暴击 （千分比）
	DefAdd   int32 `json:"defAdd"`   // 防御
	DodgeAdd int32 `json:"dodgeAdd"` // 闪避 （千分比）
	HpMaxAdd int64 `json:"hpAdd"`    // 血量上限

	// 变异加成
	AtkAddRate   []int32 `json:"atkadd_by"`
	DefAddRate   []int32 `json:"defadd_by"`
	HpMaxAddRate []int64 `json:"hpadd_by"`

	// 繁殖继承
	BreedAtkAddRate   []int32 `json:"breed_atkadd_by"`
	BreedDefAddRate   []int32 `json:"breed_defadd_by"`
	BreedHpMaxAddRate []int64 `json:"breed_hpadd_by"`

	// 乘骑相关
	HorseStar int32 `json:"horse_star"`
	CanHorse  int32 `json:"can_horse"`

	// 超进化相关
	SuperEvolutionAtk      int32     `json:"super_evolution_atk"`
	SuperEvolutionAtkTime  int32     `json:"super_evolution_atk_time"`
	SuperEvolutionCrit     int32     `json:"super_evolution_crit"`
	SuperEvolutionDef      int32     `json:"super_evolution_def"`
	SuperEvolutionDis      int32     `json:"super_evolution_dis"`
	SuperEvolutionDodge    int32     `json:"super_evolution_dodge"`
	SuperEvolutionHp       int64     `json:"super_evolution_hp"`
	SuperEvolutionAtkRate  int32     `json:"super_evolution_atk_rate"`
	SuperEvolutionDefRate  int32     `json:"super_evolution_def_rate"`
	SuperEvolutionHpRate   int64     `json:"super_evolution_hp_rate"`
	SuperEvolutionStuff    *AwardCfg `json:"super_evolution_stuff"`
	SuperEvolutionNeedStar int32     `json:"super_evolution_need_star"`
}

func (cfg *PetCfg) IsVegetarian() bool {
	return cfg.Vegetarian == 2
}

type PetResolveCfg struct {
	Star   int32     `json:"starNum"`
	Reward *AwardCfg `json:"reward"`
}

type PetLevelCfg struct {
	Level int32  `json:"level"`
	Exp   uint64 `json:"exp"`
}

type PetStarCfg struct {
	PetCfgId      int32 `json:"index"`
	Star          int32 `json:"star"`
	Hp            int64 `json:"hp"`
	Atk           int32 `json:"atk"`
	Def           int32 `json:"def"`
	SelfNum       int32 `json:"self_num"`
	SGradeNum     int32 `json:"s_grade_num"`
	AGradeNum     int32 `json:"a_grade_num"`
	BGradeNum     int32 `json:"b_grade_num"`
	CGradeNum     int32 `json:"c_grade_num"`
	HorseAttrRate int32 `json:"horse_attr_rate"`
}

type CheckPetStar struct {
	SelfNum   int32
	SGradeNum int32
	AGradeNum int32
	BGradeNum int32
	CGradeNum int32
}

type AllPet struct {
	RoleId int64
	PetMap map[int64]*Pet
	MaxLv  int32
}

type TryVariationCfg struct {
	CfgId       int32 `json:"index"`
	Kind        int32 `json:"kind"`
	Add         int64 `json:"add"`
	Weight      int64 `json:"weight"`
	TotalWeight int64 `json:"total_weight"`
}

func (cfg *TryVariationCfg) GetCfgId() int32 {
	return cfg.CfgId
}

func (cfg *TryVariationCfg) GetWeight() int64 {
	return cfg.Weight
}

func (cfg *TryVariationCfg) GetTotalWeight() int64 {
	return cfg.TotalWeight
}

func (cfg *TryVariationCfg) SetWeight(w int64) {
	cfg.Weight = w
}

func (cfg *TryVariationCfg) SetTotalWeight(w int64) {
	cfg.TotalWeight = w
}
