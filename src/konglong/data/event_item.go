package data

import "goproject/src/konglong/protocol"

type EventItemAdd struct {
	EventBase
	RoleId    int64
	ItemCfgId int64
	Num       int64
	Action    protocol.ItemActionType
}

type EventItemMake struct {
	EventBase
	RoleId int64
	Item   *Item
}
