package data

import (
	"time"
)

type EventInitServer struct {
	EventBase
	NowSec int64
	Now    time.Time
}

type EventMinutely struct {
	EventBase
	NowSec int64
	Now    time.Time
}

type EventHourly struct {
	EventBase
	Hour int64
	Now  time.Time
}

type EventDaily struct {
	EventBase
	Week      time.Weekday
	IsNewWeek bool
	Now       time.Time
	NowSec    int64
}

type EventSqlRollBack struct {
	EventBase
	RoleId  int64
	SignOut bool
}
