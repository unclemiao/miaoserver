package base

import (
	"sort"

	"github.com/kataras/golog"

	"goproject/engine/assert"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

const (
	EVENT_MSG_BUFF_SIZE = 65535
	NET_MSG_BUFF_SIZE   = 65535
)

type EventManager struct {
	Nets   [][]*data.NetFunc
	Events [][]*data.EventFunc
	Logs   *golog.Logger

	NetOverFunc func(args *data.Context, msg *protocol.Envelope)
}

func (mgr *EventManager) Init(logger *golog.Logger) {
	mgr.Logs = logger
	mgr.Events = make([][]*data.EventFunc, EVENT_MSG_BUFF_SIZE)
	mgr.Nets = make([][]*data.NetFunc, NET_MSG_BUFF_SIZE)
}

func (mgr *EventManager) When(eventType interface{}, f interface{}, orderList ...int32) {
	var order int32 = 1
	if len(orderList) > 0 {
		order = orderList[0]
	}
	switch t := eventType.(type) {
	case data.EventType:
		_, ok := f.(func(msg *data.Context, args interface{}))
		assert.Assert(ok, "func not a func(msg *models.Context, args interface{})")
		mgr.whenEvent(int32(t), order, f)
	case protocol.EnvelopeType:
		_, ok := f.(func(msg *data.Context, args *protocol.Envelope))
		assert.Assert(ok, "func not a func(msg *models.Context, args *protocol.Envelope)")
		mgr.whenNet(int32(t), order, f)
	}
}

func (mgr *EventManager) WhenNetOver(f func(args *data.Context, msg *protocol.Envelope)) {
	mgr.NetOverFunc = f
}

func (mgr *EventManager) whenNet(msgId int32, order int32, funList ...interface{}) {
	for _, f := range funList {
		msg := &data.NetFunc{MsgId: msgId, Func: f.(func(msg *data.Context, args *protocol.Envelope))}

		msg.Order = order

		assert.Assert(msgId > 0 && msgId < int32(len(mgr.Nets)), "msgId[%d] err", msgId)
		mgr.Nets[msgId] = append(mgr.Nets[msgId], msg)
	}

	sort.Slice(mgr.Nets[msgId], func(i, j int) bool {
		return mgr.Nets[msgId][i].Order < mgr.Nets[msgId][j].Order
	})
}

func (mgr *EventManager) whenEvent(msgId int32, order int32, funList ...interface{}) {
	for _, f := range funList {
		msg := &data.EventFunc{MsgId: msgId, Func: f.(func(msg *data.Context, args interface{}))}
		msg.Order = order
		assert.Assert(msgId > 0 && msgId < int32(len(mgr.Events)), "msgId[%d] err", msgId)
		mgr.Events[msgId] = append(mgr.Events[msgId], msg)
	}
	sort.Slice(mgr.Events[msgId], func(i, j int) bool {
		return mgr.Events[msgId][i].Order < mgr.Events[msgId][j].Order
	})
}

func (mgr *EventManager) Call(ctx *data.Context, msgId data.MsgIdItf, msg data.EventItf) {
	msg.SetMsgId(msgId)
	if ctx == nil {
		ctx = &data.Context{
			MsgId:    msgId,
			EventMsg: msg,
		}
	}
	switch t := msgId.(type) {
	case data.EventType:
		mgr.callEvent(ctx, t, msg)
	case protocol.EnvelopeType:
		mgr.callNet(ctx, t, msg.GetEnvMsg())
	}

}

func (mgr *EventManager) callNet(args *data.Context, msgId protocol.EnvelopeType, msg *protocol.Envelope) {
	assert.Assert(msgId > 0 && int32(msgId) < int32(len(mgr.Nets)), "msgId[%s] err", msgId)
	for _, event := range mgr.Nets[msgId] {
		event.Func(args, msg)
		if args.Stop {
			break
		}
	}
	if mgr.NetOverFunc != nil {
		mgr.NetOverFunc(args, msg)
	}
}

func (mgr *EventManager) callEvent(args *data.Context, msgId data.EventType, msg interface{}) {
	assert.Assert(msgId > 0 && int32(msgId) < int32(len(mgr.Events)), "msgId[%s] err", msgId)
	for _, event := range mgr.Events[msgId] {
		event.Func(args, msg)
		if args.Stop {
			break
		}
	}
}
