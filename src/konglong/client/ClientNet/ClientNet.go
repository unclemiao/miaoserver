package ClientNet

import (
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"

	"goproject/src/konglong/client/config"
	"goproject/src/konglong/data"

	"github.com/gorilla/websocket"
)

var OnConn func(conn *data.Conn)
var OnData func(conn *data.Conn, datas []byte)
var OnClose func(conn *data.Conn)

func RandString(len int) string {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		b := r.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return string(bytes)
}

func Star(
	onConn func(conn *data.Conn),
	onData func(conn *data.Conn, datas []byte),
	onClose func(conn *data.Conn),
) {
	OnConn = onConn
	OnData = onData
	OnClose = onClose
	log.SetFlags(log.Lshortfile | log.Ldate)

	cfg := config.GetConfig()
	host := fmt.Sprintf("%s?token=", "wss://weixin-game-kl2.qcinterfacet.com/game001/ws")
	wait := sync.WaitGroup{}
	wait.Add(1)
	for i := 0; i < cfg.ClientNum; i++ {
		in := i
		time.AfterFunc(time.Millisecond*20*time.Duration(i), func() {
			go func(n int) {
				Dail(fmt.Sprintf("%s%s", host, ""), fmt.Sprintf("%d", n)) // ready服
			}(in)
		})

	}

	wait.Wait()
}

func Dail(urlstr string, accountName string) {
	var dialer *websocket.Dialer

	conn, _, err := dialer.Dial(urlstr, nil)

	if err != nil {
		log.Printf("Dial[%s] err:%v\n", urlstr, err)
		return
	}
	ws := &data.Conn{
		WsConn:      conn,
		AccountName: accountName,
	}
	defer func() {
		if OnClose != nil {
			OnClose(ws)
		}
	}()
	if OnConn != nil {
		OnConn(ws)
	}
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read err:", err)
			return
		}
		if OnData != nil {
			OnData(ws, message)
		}
	}

}
