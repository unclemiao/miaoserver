package Logic

import (
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

func WinBossPass(session *data.Session) {
	Send(session.Conn, &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_WinPassBoss,
		Payload: &protocol.Envelope_C2SWinPassBoss{C2SWinPassBoss: &protocol.C2S_WinPassBossMsg{}},
	})
}
