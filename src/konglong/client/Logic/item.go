package Logic

import (
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

func init() {
}

func LoadItem() {
	gEventMgr.When(protocol.EnvelopeType_S2C_NotifyItemList, func(msg *data.Context) {

	})

	gEventMgr.When(protocol.EnvelopeType_S2C_NotifyUpdateItemList, func(msg *data.Context) {

	})
}

func BuyItem(session *data.Session) {
	Send(session.Conn, &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_BuyFood,
		Payload: &protocol.Envelope_C2SBuyFood{C2SBuyFood: &protocol.C2S_BuyFoodMsg{}},
	})
}

func MakeItem(session *data.Session) {
	Send(session.Conn, &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_MakeItem,
		Payload: &protocol.Envelope_C2SMakeItem{C2SMakeItem: &protocol.C2S_MakeItemMsg{
			ItemCfgId: 102,
		}},
	})
}
