package Logic

import (
	"log"
	"runtime/debug"
	"time"

	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"

	"github.com/gorilla/websocket"
)

type NetMsg struct {
	MsgId   int32  `json:"msg_id"`
	PayLoad string `json:"pay_load"`
}

func OnConn(conn *data.Conn) {

	SigInPlayer(conn, conn.AccountName)

}

func Ping(conn *data.Conn) {
	Send(conn.WsConn, &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_Ping,
		Payload: &protocol.Envelope_C2SPing{C2SPing: &protocol.C2S_PingMsg{}},
	})
	time.AfterFunc(time.Second*10, func() {
		Ping(conn)
	})
}

func OnData(conn *data.Conn, datas []byte) {
	conn.Lock.Lock()
	defer conn.Lock.Unlock()
	defer func() {
		err := recover()
		if err != nil {
			log.Println(err)
			log.Println(string(debug.Stack()))
		}
	}()
	netMsg := &protocol.Envelope{}
	err := netMsg.Unmarshal(datas)
	if err != nil {
		log.Println(err)
		return
	}

	gEventMgr.Call(&data.Context{
		MsgId: netMsg.GetMsgType(),
		EventMsg: &data.EventNet{
			EventBase: data.EventBase{MsgId: netMsg.GetMsgType()},
			Msg:       netMsg,
		},
		AccountId: conn.AccountName,
		Session: &data.Session{
			Conn:      conn.WsConn,
			AccountId: conn.AccountName,
		},
	}, netMsg.GetMsgType(), &data.EventNet{
		EventBase: data.EventBase{MsgId: netMsg.GetMsgType()},
		Msg:       netMsg,
	})
}

func OnClose(conn *data.Conn) {
	conn.WsConn.Close()
}

func Send(conn *websocket.Conn, message *protocol.Envelope) {

	payload, err := message.Marshal()
	if err == nil {
		err = conn.WriteMessage(websocket.BinaryMessage, payload)
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}
}
