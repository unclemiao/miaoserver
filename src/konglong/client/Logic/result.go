package Logic

import (
	"log"
	"sync"
	"time"

	"goproject/src/konglong/protocol"
)

var ResultMap map[string]*protocol.Role
var Lock sync.RWMutex

func init() {
	ResultMap = make(map[string]*protocol.Role)
}
func ResultBeginSend(account string) {
	Lock.Lock()
	defer Lock.Unlock()
	ResultMap[account] = &protocol.Role{}
}

func ResultEndSend(account string, role *protocol.Role) {
	Lock.Lock()
	defer Lock.Unlock()
	ResultMap[account] = role
}

func PrintlnResult() {
	time.AfterFunc(time.Second*60, PrintlnResult)
	Lock.RLock()
	defer Lock.RUnlock()
	var total, success, fail int32
	for _, role := range ResultMap {
		total++
		if role.Id > 0 {
			success++

		} else {

			fail++
		}
	}
	log.Printf("total:%d, success:%d, fail:%d", total, success, fail)
}
