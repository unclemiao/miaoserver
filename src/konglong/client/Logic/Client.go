package Logic

import (
	"goproject/src/konglong/base"
	"goproject/src/konglong/client/ClientNet"
)

var gClient *Client
var gEventMgr *base.EventManager

type Client struct {
	ServerHost string
}

func ClientBegin() {
	gClient = &Client{}
	gEventMgr = &base.EventManager{}

	gEventMgr.Init(nil)
	LoadSignIn()
	Begin()

	ClientNet.Star(OnConn, OnData, OnClose)

}

func Begin() {
	PrintlnResult()
}
