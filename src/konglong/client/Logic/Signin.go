package Logic

import (
	"log"
	"sync"
	"time"

	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var gRole *protocol.Role

var TimeMap map[string]int64
var TimeMapLock sync.RWMutex

func LoadSignIn() {
	TimeMap = make(map[string]int64)
	gEventMgr.When(protocol.EnvelopeType_S2C_Pong, func(msg *data.Context) {

	})

	gEventMgr.When(protocol.EnvelopeType_S2C_SigninPlayerDone, func(msg *data.Context) {
		r := msg.EventMsg.GetEnvMsg().GetS2CSigninPlayerDone().Role
		ResultEndSend(msg.AccountId, r)
		TimeMapLock.RLock()
		beginTime := TimeMap[msg.AccountId]
		TimeMapLock.RUnlock()
		end := time_helper.NowMill()
		sub := end - beginTime
		if sub > 10 {
			log.Printf(" %s SUB: %d ms\n", msg.AccountId, sub)
		}
		DoAi(msg.Session)
	})
}

func SigInPlayer(conn *data.Conn, account string) {
	ResultBeginSend(account)
	TimeMapLock.Lock()
	TimeMap[account] = time_helper.NowMill()
	TimeMapLock.Unlock()
	Send(conn.WsConn, &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_SigninPlayer,
		Payload: &protocol.Envelope_C2SSigninPlayer{C2SSigninPlayer: &protocol.C2S_SigninPlayerMsg{
			AccountId: account,
			Snode:     999,
			From:      "client",
			Young:     false,
			Due:       0,
			Token:     "",
		}},
	})
}

func DoAi(session *data.Session) {
	time.AfterFunc(time.Duration(util.Random(1, 10))*time.Second, func() {
		DoAi(session)
	})

	switch util.Random32(3, 3) {
	case 1:
		BuyItem(session)
	case 2:
		MakeItem(session)
	case 3:
		WinBossPass(session)
	}
}
