package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"
	"time"

	"github.com/gorilla/websocket"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/center/models"
)

func main() {
	logs.InitLog("./logs")

	//	Logic.ClientBegin()
	TestAccountAndSignIn()
	wait := sync.WaitGroup{}
	wait.Add(1)
	wait.Wait()
}

var accountTime int64
var accountNum int64
var accountSussce int64

var dailTime int64
var dailNum int64
var dailSussce int64

var first bool

func TestAccountAndSignIn() {
	time.AfterFunc(time.Second*1, TestAccountAndSignIn)

	defer func() {
		err2 := recover()
		if err2 != nil {
			logs.Error(err2)
		}
	}()
	accountResp := account("unclemiao22322")
	dialer := &websocket.Dialer{
		Proxy:            http.ProxyFromEnvironment,
		HandshakeTimeout: 10 * time.Second,
	}
	beginT := time_helper.NowMill()

	if !first {
		logs.Infof("url:%s", accountResp.Server.ClientUrl)
		first = true
	}
	conn, _, err := dialer.Dial(accountResp.Server.ClientUrl, nil)
	endT := time_helper.NowMill()
	sub := endT - beginT
	dailTime += sub
	dailNum++
	logs.Infof("Dial need: %d ms", sub)
	assert.Assert(err == nil, err)
	time.AfterFunc(time.Second*5, func() {
		conn.Close()
	})
	dailSussce++
	logs.Infof("server id:%d", accountResp.Server.Sid)
	logs.Infof("Account Avg:%d ms, Num:%d", accountTime/accountNum, accountNum)
	logs.Infof("dial Avg:%d ms, Num:%d", dailTime/dailNum, dailNum)

	logs.Infof("account fail:%d, dial fail:%d", accountNum-accountSussce, dailNum-dailSussce)

}

type AccountResponse struct {
	Code int32
	Err  string
	Data *models.AccountResponse
}

func account(account string) *models.AccountResponse {
	beginT := time_helper.NowMill()
	host := "https://ov-center-kl2.qcinterfacet.com/konglong2/account/login"
	form := url.Values{}
	form.Set("sid", "24")
	form.Set("name", account)
	form.Set("pass", account)
	form.Set("platform", "ov")
	form.Set("channel", "ov")
	due := fmt.Sprintf("%d", time_helper.NowSec()+time_helper.Hour*4)
	form.Set("due", due)
	md5 := util.Md5(fmt.Sprintf("-_-!!!%s%s%s%s%s%s", account, account, "ov", "ov", due, "b7561aa298be84e4c3c2a7901e47c350"))
	form.Set("weiying_token", md5)
	resp, err := http.PostForm(host, form)
	endT := time_helper.NowMill()
	sub := endT - beginT
	accountTime += sub
	accountNum++
	assert.Assert(err == nil, err)
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	assert.Assert(err == nil, err)

	response := &AccountResponse{}
	err = json.Unmarshal(bs, response)
	assert.Assert(err == nil, err)

	accountSussce++

	logs.Infof("Account need: %d ms", sub)
	return response.Data
}
