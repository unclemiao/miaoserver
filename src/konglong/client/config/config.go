package config

import (
	"fmt"
	"log"
	"strconv"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/util"
)

var testConfig *TestConfig

func GetConfig() *TestConfig {
	if testConfig == nil {
		LoadConfig()
	}
	return testConfig
}

type TestConfig struct {
	AccountHost string
	GameHost    string
	ClientNum   int
}

func LoadConfig() error {
	beCnf := beego.AppConfig

	accountUrl, _ := beCnf.String("account_url")
	serverUrl, _ := beCnf.String("server_url")
	num, _ := strconv.Atoi(beCnf.DefaultString("client_num", "0"))
	testConfig = &TestConfig{
		AccountHost: accountUrl,
		GameHost:    serverUrl,
		ClientNum:   util.LimitIntMin(num, 1),
	}

	if testConfig.AccountHost == "" ||
		testConfig.GameHost == "" ||
		testConfig.ClientNum < 1 {
		return fmt.Errorf("测试数据配置错误, %+v", testConfig)
	}

	log.Printf("测试环境配置数据: %+v\n", testConfig)
	return nil
}
