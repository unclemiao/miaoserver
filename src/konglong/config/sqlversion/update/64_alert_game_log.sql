ALTER TABLE `game_log` ADD `sub_action` int(11) UNSIGNED COMMENT '子原因' AFTER `action`;
ALTER TABLE `game_log` ADD `args_int` bigint(20) UNSIGNED  COMMENT '参数' AFTER `sub_action`;
ALTER TABLE `game_log` ADD `args_str` varchar(64)  COMMENT '参数2' AFTER `args_int`;