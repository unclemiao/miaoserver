ALTER TABLE `quota` ADD `venture_free_fast` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '免费探险扫荡使用次数' AFTER `lottery_equip`;
ALTER TABLE `quota` ADD `venture_add_fast` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '增加免费探险扫荡使用次数的次数' AFTER `venture_free_fast`;
