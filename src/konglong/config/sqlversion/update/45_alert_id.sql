ALTER TABLE `id_manager` ADD `sid` int(11) UNSIGNED NOT NULL COMMENT '服务器号' AFTER `id_type`;
ALTER TABLE `id_manager` DROP PRIMARY KEY;
ALTER TABLE `id_manager` ADD PRIMARY KEY (`id_type`, `sid`);
