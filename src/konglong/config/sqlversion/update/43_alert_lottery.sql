ALTER TABLE `lottery_pet` ADD `super_evolution_buy_time` bigint(20) UNSIGNED NOT NULL COMMENT '进化石购买到期时间' AFTER `award_str`;
ALTER TABLE `quota` ADD `super_evolution_item_buy` int(11) UNSIGNED NOT NULL COMMENT '当日超进化石头购买次数' AFTER `super_evolution_ad_sdk`;
ALTER TABLE `pet` ADD `study_skill_cfg_id_list_str` varchar(256) NOT NULL default '[]' COMMENT '学习的技能配置id' AFTER `skill_cfg_id`;
ALTER TABLE `lottery_pet` ADD `super_evolution_buy_amount` int(11) UNSIGNED NOT NULL COMMENT '当日超进化石头购买出现次数' AFTER `super_evolution_buy_time`;
