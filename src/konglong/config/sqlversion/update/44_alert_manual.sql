ALTER TABLE `manual` ADD `has_super_evolution` tinyint(1) UNSIGNED NOT NULL COMMENT '普通是否超进化' AFTER `variation_star_max`;
ALTER TABLE `manual` ADD `has_variation_super_evolution` tinyint(1) UNSIGNED NOT NULL COMMENT '变异是否超进化' AFTER `has_super_evolution`;
