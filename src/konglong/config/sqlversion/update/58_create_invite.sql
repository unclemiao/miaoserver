CREATE TABLE `invite` (
  `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,   `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,   `invite_role_id`  bigint(20) UNSIGNED NOT NULL COMMENT '邀请的玩家id'
,   `invite_avater`   varchar(512) NOT NULL COMMENT '邀请的玩家的头像'
,   `invite_name`  varchar(1024) NOT NULL COMMENT '邀请的玩家的名字'
,   PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


