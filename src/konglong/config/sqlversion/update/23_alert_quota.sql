ALTER TABLE `quota` ADD `pet_king_free_refresh` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '恐龙争霸赛免费刷新次数' AFTER `venture_add_fast`;
ALTER TABLE `quota` ADD `pet_king_free_fight` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '恐龙争霸赛免费战斗次数' AFTER `pet_king_free_refresh`;
ALTER TABLE `quota` ADD `pet_king_ad_fight` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '每日恐龙争霸赛看广告战斗次数' AFTER `pet_king_free_fight`;

