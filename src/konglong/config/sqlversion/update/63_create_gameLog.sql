CREATE TABLE `game_log` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `time` timestamp NOT NULL COMMENT '日志时刻'
,    `account_id`  varchar(128)  COMMENT '账号id'
,    `role_id` bigint(20) UNSIGNED  COMMENT '角色id'
,    `role_name` varchar(128)  COMMENT '角色名'
,    `op`  varchar(128)  NOT NULL COMMENT '操作/事件名'

,    `channel` varchar(64) COMMENT '渠道名'
,    `item_cfg_id` int(11) COMMENT '道具配置id'
,    `n`  bigint(20) COMMENT '数量'
,    `bn`  bigint(20) COMMENT '剩余数量'

,    `mall_cfg_id`  int(11) COMMENT '商城道具配置id'
,    `bill`  varchar(64) COMMENT '订单号'
,    `gem`  int(11)  COMMENT '宝石'
,    `bgem`  int(11) COMMENT '剩余宝石'
,    `money`  int(11) COMMENT '现实货币'

,    `action`  int(11) COMMENT '原因'


,    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
