CREATE TABLE `feast_done` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `cfg_id` int(11) UNSIGNED NOT NULL COMMENT '配置id'
,    `done`  int(11) UNSIGNED NOT NULL COMMENT '完成计数'
,    `awarded` tinyint(1) NOT NULL COMMENT '是否领取奖励'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;