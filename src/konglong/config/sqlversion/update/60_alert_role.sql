ALTER TABLE `role` ADD `invite_id`  bigint(20) UNSIGNED NOT NULL COMMENT '邀请者id' AFTER `language`;
ALTER TABLE `role` ADD `invite_sid`  bigint(20) UNSIGNED NOT NULL COMMENT '邀请者服务器id' AFTER `invite_id`;
ALTER TABLE `role` ADD  `invite_notify` tinyint(1) UNSIGNED NOT NULL COMMENT '是否已经通知邀请者' AFTER `invite_sid`;
