CREATE TABLE `ad_sdk_award` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id'
,    `appid` varchar(1024) NOT NULL COMMENT '广告appid'
,    `state` int(11) UNSIGNED NOT NULL COMMENT '状态  0=可玩， 1=可领取， 2=已领取'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
