-- 商城记录
create table role_mall (
 `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,   `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id'
,	`cfg_id`  int(11) UNSIGNED NOT NULL COMMENT '配置id'
,	`amount`  int(11) UNSIGNED NOT NULL COMMENT '购买次数'
,	primary key (`id`)
,   KEY `INDEX_ROLEID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
