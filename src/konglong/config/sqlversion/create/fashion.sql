CREATE TABLE `fashion` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `role_id` bigint(20)UNSIGNED NOT NULL COMMENT '角色id',
    `cfg_id`  int(11) UNSIGNED NOT NULL COMMENT '配置id',
    `state`  int(11) UNSIGNED NOT NULL COMMENT '状态',
    PRIMARY KEY (`id`),
    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

