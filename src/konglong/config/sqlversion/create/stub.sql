CREATE TABLE `stub` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `kind` int(11) UNSIGNED NOT NULL COMMENT '阵位类型， 1=正常；2=恐龙争霸赛'
,    `stub_index` int(11) UNSIGNED NOT NULL COMMENT '阵位'
,    `pet_id`  bigint(20) UNSIGNED NOT NULL COMMENT '恐龙id'
,    `help_id_1`  bigint(20) NOT NULL COMMENT '助阵位1的恐龙id, < 0 表示未解锁'
,    `help_id_2` bigint(20) NOT NULL COMMENT '助阵位1的恐龙id, < 0 表示未解锁'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

