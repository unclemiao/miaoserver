CREATE TABLE `system_chat` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `kind` int(11) UNSIGNED NOT NULL COMMENT '公告类型'
,    `msg` varchar(2048) NOT NULL COMMENT '公告内容'
,    `begin_time`  bigint(20) UNSIGNED NOT NULL COMMENT '开始时间'
,    `end_time`  bigint(20) NOT NULL COMMENT '结束时间'
,    `sub_time` bigint(20) NOT NULL COMMENT '间隔'
,    `next_time` bigint(20) NOT NULL COMMENT '下次发送时间'
,    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

