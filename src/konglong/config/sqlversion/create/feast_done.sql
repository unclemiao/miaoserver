CREATE TABLE `feast_done` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `cfg_id` int(11) UNSIGNED NOT NULL COMMENT '配置id'
,    `kind` int(11) UNSIGNED NOT NULL COMMENT '活动类型'
,    `done`  int(11) UNSIGNED NOT NULL COMMENT '完成计数'
,    `awarded` tinyint(1) NOT NULL COMMENT '是否领取奖励'
,    `award_num` int(11) NOT NULL COMMENT '领取奖励次数'
,    `fight_info_json`  varchar(128) NOT NULL COMMENT '赛选出要战斗的排名列表'
,    `fight_win_amount`  int(11) NOT NULL COMMENT '胜利次数'
,    `fight_lose_amount`  int(11) NOT NULL COMMENT '失败次数'
,    `update_time` bigint(20) UNSIGNED NOT NULL COMMENT '最后竞技时间'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID_AND_KIND` (`role_id`, `kind`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

