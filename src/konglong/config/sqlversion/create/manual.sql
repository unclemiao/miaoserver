CREATE TABLE `manual` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `kind` int(11) UNSIGNED NOT NULL COMMENT '类型',
    `pet_cfg_id` int(11) UNSIGNED NOT NULL COMMENT '恐龙配置id',
    `evolution_lv` int(11) NOT NULL COMMENT '进化等级',
    `star_max` int(11) UNSIGNED NOT NULL COMMENT '最高星级',
    `has_normal` tinyint(1) UNSIGNED NOT NULL COMMENT '是否有变异',
    `has_variation` tinyint(1) UNSIGNED NOT NULL COMMENT '是否有变异',
    `variation_evolution_lv` int(11) NOT NULL COMMENT '变异进化等级',
    `variation_star_max` int(11) UNSIGNED NOT NULL COMMENT '变异最高星级',
    `has_super_evolution` tinyint(1) UNSIGNED NOT NULL COMMENT '普通是否超进化',
    `has_variation_super_evolution` tinyint(1) UNSIGNED NOT NULL COMMENT '变异是否超进化',
    `item_cfg_id` int(11) UNSIGNED NOT NULL COMMENT '食物配置id',
    `awarded` tinyint(1) UNSIGNED NOT NULL COMMENT '奖励是否领取',
    PRIMARY KEY (`id`),
    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



