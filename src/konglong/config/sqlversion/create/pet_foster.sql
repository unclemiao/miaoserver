CREATE TABLE `pet_foster` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,   `pet_id` bigint(20) UNSIGNED NOT NULL COMMENT '恐龙id'
,   `grid`  int(22) UNSIGNED NOT NULL COMMENT '所在位置'
,   `begin_time`  bigint(20) UNSIGNED NOT NULL COMMENT '开始时间'
,   PRIMARY KEY (`role_id`, `grid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
