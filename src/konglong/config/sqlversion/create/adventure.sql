CREATE TABLE `adventure` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id'
,    `chapter` int(11) UNSIGNED NOT NULL COMMENT '当前正在进行中的章节'
,    `passed`  int(11) UNSIGNED NOT NULL COMMENT '该章节的已通关数'
,    `buff_info` json  NOT NULL COMMENT '已经领取的buff'
,    `unget_buff_info` json  NOT NULL COMMENT '未领取的buff'
,    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
