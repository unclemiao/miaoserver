CREATE TABLE `lottery_pet` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `score` int(11) UNSIGNED NOT NULL COMMENT '积分',
    `is_first` tinyint(1) UNSIGNED NOT NULL COMMENT '是否第1次',
    `is_second` tinyint(1) UNSIGNED NOT NULL COMMENT '是否第2次',
    `daily_lottery_pet_luck` int(11) UNSIGNED NOT NULL COMMENT '每日孵化幸运值',
    `award_str` varchar(1024) NOT NULL COMMENT '奖励信息',
    `super_evolution_buy_time`  bigint(20) UNSIGNED NOT NULL COMMENT '进化石购买到期时间',
    `super_evolution_buy_amount` int(11) UNSIGNED NOT NULL COMMENT '当日超进化石头购买出现次数',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


