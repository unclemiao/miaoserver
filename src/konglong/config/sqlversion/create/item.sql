CREATE TABLE `item` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `owner_id` bigint(20) UNSIGNED NOT NULL COMMENT '装配者id'
,    `cid` bigint(20) UNSIGNED NOT NULL COMMENT '配置id'
,    `n`  bigint(20) UNSIGNED NOT NULL COMMENT '数量'
,    `grid` int(11) UNSIGNED NOT NULL COMMENT '所在格子号'
,    `kit`  int(11) UNSIGNED NOT NULL COMMENT '所在位置，0=垃圾箱，1=背包，2=装备栏'
,    `hp_max_base`  int(11) UNSIGNED NOT NULL COMMENT '血量上限基础值'
,    `atk_base`  int(11) UNSIGNED NOT NULL COMMENT '攻击基础值'
,    `def_base`  int(11) UNSIGNED NOT NULL COMMENT '防御基础值'
,    `grade`  int(11) UNSIGNED NOT NULL COMMENT '品阶'
,    `star`  int(11) UNSIGNED NOT NULL COMMENT '星级'
,    `skill_cfg_id`  int(11) UNSIGNED NOT NULL COMMENT '技能'
,    `open_cfg_id`  bigint(20) UNSIGNED NOT NULL COMMENT '限时食物宝箱开启食物配置id'
,    `rand_attr_str`  JSON NOT NULL COMMENT '随机属性'
,    `due`  bigint(20) UNSIGNED NOT NULL COMMENT '超时时间'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
,    KEY `INDEX_ROLE_ID_AND_KIT` (`role_id`, `kit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


