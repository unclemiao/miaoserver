package logic

import (
	"path/filepath"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/engine/excel"
	"goproject/engine/logs"
	"goproject/src/konglong/base"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"

	"time"
)

func InitConfig(path string) {
	path, _ = filepath.Abs(path)
	// 加载服务器启动配置
	InitFlag()
	logFile()
	base.InitServerCnf(filepath.Join(path, "config"))
	cnf := base.GetAppCfg()
	err := cnf.Unmarshal(&gAppCnf)

	if gGameFlag.CenterHost != "" {
		gAppCnf.Admin.Host = gGameFlag.CenterHost
	}
	if gGameFlag.Sid > 0 {
		gAppCnf.Server.Sid = int32(gGameFlag.Sid)
	}
	if gGameFlag.Platform != "" {
		gAppCnf.Server.Platform = gGameFlag.Platform
	}
	if gGameFlag.Channel != "" {
		gAppCnf.Server.Channel = gGameFlag.Channel
	}
	if gGameFlag.Port != 0 {
		gAppCnf.Server.Port = int32(gGameFlag.Port)
	}
	if gGameFlag.Id != 0 {
		gAppCnf.Server.Id = gGameFlag.Id
	}
	gAppCnf.Server.IsMaster = gGameFlag.IsMaster

	logs.Infof("InitFlag %+v", gGameFlag)
	logs.Infof("InitConfig: %+v", gAppCnf)
	assert.Assert(err == nil, err)
	// 加载 excel
	excel.XlsConfigInit(filepath.Join(path, "common/excel"))
	// util.XlsConfigInitZip(path + "common/excel")

}

func LoadConfig() {
	gCache = cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4)
	// 加载游戏配置
	config.LoadConfig(logs.GetLogger())
	AuthLoadConfig()
	// 初始化模块
	TimerLoadConfig()
	gIdMgr.LoadConfig()
	gRoleMgr.Init()
	gFoodBookMgr.Init() // before item
	gItemMgr.Init()
	gMonMgr.Init()
	gMailMgr.Init()
	gKingMgr.Init()
	gGameMgr.Init()
	gAwardMgr.Init()
	gRankMgr.Init()
	gAttrMgr.Init()
	gPetMgr.Init()
	gPetBreedMgr.Init()
	gGamePassMgr.Init()
	gHangMgr.Init()
	gStubMgr.Init()
	gLotteryPetMgr.Init()
	gLotteryItemMgr.Init()
	gLotteryEquipMgr.Init()
	gDailyQuestMgr.Init()
	gManualMgr.Init()
	gParal.Init()
	gDuelMgr.Init()
	gFightMgr.Init()
	gPetFosterMgr.Init()
	gVentureMgr.Init()
	gShareMgr.Init()
	gFeastMgr.Init() // after share
	gChatMgr.Init()
	gCouponManager.Init()
	gViewMgr.Init()
	gRoleTalentMgr.Init()
	gFashionMgr.Init()
	gRoleAdSdkAwardMgr.Init()
	gPetSuperEvolutionMgr.Init()
	gServerExtDataMgr.Init()
	gMallMgr.Init()
	gSystemChatMgr.Init()

	gTimeFeastMgr.Init()
	gQuotaCdMgr.Init()
	gGmManager.Init()
	gPayMgr.Init()
	gInviteMgr.Init()
	gGameLogMgr.Init()

	assert.Assert(checkConfig(), "配置检测错误")

}

func AfterConfig() {
	gEventMgr.Call(&data.Context{
		Sid: gServer.Node.Sid,
	}, data.EVENT_MSGID_AFTER_CONFIG, &data.EventBase{})
	gServerExtDataMgr.LoadServerDataMgr()
}

func checkConfig() bool {
	if !config.CheckConfig() {
		return false
	}
	return true
}
