package logic

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/sql"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type DuelManager struct {
	Rank        []*data.Duel
	RankToProto []*protocol.DuelRankInfo
	RoleDuelMap map[int64]int32
}

func init() {
	gDuelMgr = &DuelManager{
		RoleDuelMap: make(map[int64]int32),
	}
}

func (mgr *DuelManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, args interface{}) {
		mgr.LoadDataFromDB(ctx)
	})

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventMsg := msg.(*data.EventSqlRollBack)
		if !eventMsg.SignOut {
			mgr.LoadDataFromDB(ctx)
		}
	})

	gEventMgr.When(data.EVENT_MSGID_DAILY, func(ctx *data.Context, args interface{}) {
		go func() {
			defer func() {
				err := recover()
				if err != nil {
					logs.Error(err)
				}
			}()
			rankSql, err := sql.LoadSql(&sql.Dsn{
				Driver:       "mysql",
				Host:         gAdminMgr.ServerCmd.SqlHost,
				Username:     gAdminMgr.ServerCmd.SqlRoot,
				Password:     gAdminMgr.ServerCmd.SqlPass,
				DatabaseName: gAdminMgr.ServerCmd.SqlName,
			})
			assert.Assert(err == nil, err)
			err = rankSql.Ping()
			assert.Assert(err == nil, err)
			defer rankSql.Close()
			rankSql.MustExcel(gConst.SQL_DELETE_DUEL_RECORD, time.Now().AddDate(0, 0, -7))
		}()
		mgr.AwardTopRank(ctx)
	})

	gEventMgr.When(data.EVENT_MSGID_MINUTELY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventMinutely)
		if args.NowSec%(gConst.RANK_SORT_SUB_TIME_MINUTIL*time_helper.Minute) == 0 {
			mgr.LoadDataFromDB(ctx)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelInfo, func(ctx *data.Context, args *protocol.Envelope) {
		mgr.NotifyDuelInfo(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelRankList, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SGetDuelRankList()
		)
		mgr.GetRankList(ctx, ctx.RoleId, rep.RankBegin, rep.RankEnd)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_RefreshDuel, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SRefreshDuel()
		)
		myDuel := mgr.MyDuelBy(ctx, ctx.RoleId)
		if myDuel != nil {
			if !rep.AdSdk {
				gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Free_Refresh_Duel)
			}
			mgr.RefreshFighList(ctx, myDuel)
			mgr.NotifyDuelInfo(ctx, ctx.RoleId)
		}
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, args interface{}) {
		myDuel := mgr.MyDuelBy(ctx, ctx.RoleId)
		if myDuel != nil {
			mgr.RefreshFighList(ctx, myDuel)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_WinDuel, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SWinDuel()
		)
		mgr.Win(ctx, ctx.RoleId, rep.ToRank)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_LoseDuel, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SLoseDuel()
		)
		mgr.Lose(ctx, ctx.RoleId, rep.ToRank)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelFightRecordList, func(ctx *data.Context, args *protocol.Envelope) {
		mgr.GetDuelRecord(ctx, ctx.RoleId)
	})

}

func (mgr *DuelManager) InitRobot(ctx *data.Context) {
	gSql.Begin()
	now := time.Now()
	for rank, cfg := range config.GetRobotCfgList() {
		gSql.MustExcel(gConst.SQL_INSERT_DUEL, rank+1, cfg.CfgId, cfg.Name, true, "", "[]", now)
	}
}

func (mgr *DuelManager) LoadDataFromDB(ctx *data.Context) {
	var rankList []*data.Duel
	gSql.MustSelect(&rankList, gConst.SQL_SELECT_ALL_DUEL)
	mgr.LoadData(ctx, rankList)
}

func (mgr *DuelManager) LoadData(ctx *data.Context, rankList []*data.Duel) {
	if len(rankList) == 0 {
		mgr.InitRobot(ctx)
		mgr.LoadDataFromDB(ctx)
		return
	}
	mgr.Rank = make([]*data.Duel, len(rankList)+1)
	mgr.RankToProto = make([]*protocol.DuelRankInfo, len(rankList))
	for _, d := range rankList {
		if d.Rank > gConst.DUEL_RANK_MAX {
			continue
		}
		mgr.Rank[d.Rank] = d
		mgr.RoleDuelMap[d.RoleId] = d.Rank
		mgr.RankToProto[d.Rank-1] = d.GetRankInfoForClient()
	}
}

func (mgr *DuelManager) getMyDuelCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_MY_DUEL, roleId)
}

func (mgr *DuelManager) MyDuelBy(ctx *data.Context, roleId int64) *data.Duel {
	cacheKey := mgr.getMyDuelCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.Duel
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_DUEL, roleId)
		if len(dataList) == 0 {
			return nil, nil
		}
		myDuel := dataList[0]
		jsonErr := json.Unmarshal([]byte(myDuel.FightInfoJson), &myDuel.FightInfoRankList)
		if jsonErr != nil {
			return nil, jsonErr
		}

		return myDuel, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	if rv == nil {
		return nil
	}

	return rv.(*data.Duel)
}

func (mgr *DuelManager) SetDuelRank(ctx *data.Context, role *data.Role) *data.Duel {
	myDuel := mgr.MyDuelBy(ctx, role.RoleId)
	if myDuel != nil {
		return myDuel
	}
	lastRank := int32(len(mgr.Rank) - 1)
	rankInfo := &data.Duel{
		RoleId:        role.RoleId,
		Name:          role.Name,
		Avatar:        role.Avatar,
		Rank:          lastRank + 1,
		DuelTime:      time.Now(),
		FightInfoJson: "[]",
	}
	if rankInfo.Rank <= gConst.DUEL_RANK_MAX {
		mgr.Rank = append(mgr.Rank, rankInfo)
		mgr.RoleDuelMap[role.RoleId] = rankInfo.Rank
	} else {
		rankInfo.Rank = gConst.DUEL_RANK_MAX + 1
	}
	gCache.Set(mgr.getMyDuelCacheKey(role.RoleId), rankInfo, gConst.SERVER_DATA_CACHE_TIME_OUT)
	gSql.Begin().Clean(role.RoleId).MustExcel(gConst.SQL_INSERT_DUEL,
		rankInfo.Rank, rankInfo.RoleId, rankInfo.Name, false, rankInfo.Avatar, "[]", rankInfo.DuelTime)
	return rankInfo
}

func (mgr *DuelManager) Win(ctx *data.Context, roleId int64, toRank int32) {
	// A FIGHT B
	A_Duel := mgr.MyDuelBy(ctx, roleId)
	assert.Assert(A_Duel != nil, "duel not open, roleId:%d", roleId)
	var rankAdd int32
	find := false
	for _, r := range A_Duel.FightInfoRankList {
		if r == toRank {
			find = true
			break
		}
	}
	assert.Assert(find, "toRank not find, toRank:%d", toRank)

	B_Duel := mgr.Rank[toRank]

	// 扣除次数
	gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Duel)

	gSql.Begin()
	if toRank < A_Duel.Rank {
		// 战胜排名比自己高的人
		rankAdd = A_Duel.Rank - B_Duel.Rank
		B_Duel.Rank = A_Duel.Rank
		A_Duel.Rank = toRank

		gSql.Clean(B_Duel.RoleId).MustExcel(gConst.SQL_UPDATE_DULE_RANK, B_Duel.Rank, B_Duel.RoleId)
		mgr.RoleDuelMap[B_Duel.RoleId] = B_Duel.Rank

		gSql.Clean(A_Duel.RoleId).MustExcel(gConst.SQL_UPDATE_DULE_RANK, A_Duel.Rank, A_Duel.RoleId)
		mgr.RoleDuelMap[A_Duel.RoleId] = A_Duel.Rank

		if A_Duel.Rank < int32(len(mgr.Rank)) {
			mgr.Rank[A_Duel.Rank] = A_Duel
		}

		if B_Duel.Rank < int32(len(mgr.Rank)) {
			mgr.Rank[B_Duel.Rank] = B_Duel
		}
		gShareMgr.OnDuelLost(B_Duel.RoleId, A_Duel.Rank, B_Duel.Rank)

	}

	cfg := config.DuelCfgBy(toRank)
	gItemMgr.GiveAward(ctx, roleId, cfg.WinAward, 1.0, protocol.ItemActionType_Action_Kunshou)
	mgr.RefreshFighList(ctx, A_Duel)
	mgr.NotifyDuelInfo(ctx, roleId)
	// 日志
	mgr.AddDuelRecord(ctx, roleId, A_Duel, B_Duel, rankAdd, true)
}

func (mgr *DuelManager) Lose(ctx *data.Context, roleId int64, toRank int32) {
	A_Duel := mgr.MyDuelBy(ctx, roleId)
	B_Duel := mgr.Rank[toRank]
	// 扣除次数
	gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Duel)

	cfg := config.DuelCfgBy(toRank)
	gItemMgr.GiveAward(ctx, roleId, cfg.LoseAward, 1.0, protocol.ItemActionType_Action_Kunshou)

	// 日志
	mgr.AddDuelRecord(ctx, roleId, A_Duel, B_Duel, 0, false)
}

func (mgr *DuelManager) AddDuelRecord(ctx *data.Context, roleId int64, winner *data.Duel, loser *data.Duel, rankAdd int32, isWin bool) {

	gSql.Begin().MustExcel(gConst.SQL_INSERT_DUEL_RECORD,
		gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_DUEL_RECORD),
		winner.RoleId, winner.Name, winner.Avatar, winner.IsRobot,
		loser.RoleId, loser.Name, loser.Avatar, loser.IsRobot,
		rankAdd, isWin, time.Now(),
	)
}

func (mgr *DuelManager) GetDuelRecord(ctx *data.Context, roleId int64) {
	var winnerList, loserList []*data.DuelRecord
	gSql.MustSelect(&winnerList, gConst.SQL_SELECT_DUEL_WINNER_RECORD, roleId)
	gSql.MustSelect(&loserList, gConst.SQL_SELECT_DUEL_LOSER_RECORD, roleId)

	var allList []*protocol.DuelFightRecord
	for _, win := range winnerList {
		allList = append(allList, win.GetForClient())
	}
	for _, lose := range loserList {
		allList = append(allList, lose.GetForClient())
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetDuelFightRecordListDoneMsg(ctx.EventMsg.GetEnvMsg(), allList))
}

func (mgr *DuelManager) RefreshFighList(ctx *data.Context, duel *data.Duel) {
	duel.FightInfoRankList = make([]int32, 0, gConst.DUEL_GET_RANK_INFO_AMOUNT)
	myRank := util.MinInt(int(duel.Rank), gConst.DUEL_RANK_MAX)
	afRank := util.MaxInt(myRank-1, gConst.DUEL_RAN_RANK_INFO_AMOUNT)
	afRank = util.MinInt(afRank, gConst.DUEL_RANK_MAX)

	rs := rand.Perm(util.MinInt(gConst.DUEL_RANK_MAX, gConst.DUEL_RAN_RANK_INFO_AMOUNT))

	var randIndex int
	for i := 1; i <= gConst.DUEL_GET_RANK_INFO_AMOUNT; {
		randRank := afRank - rs[randIndex]
		if randRank != myRank && randRank < len(mgr.Rank) && mgr.Rank[randRank] != nil {
			duel.FightInfoRankList = append(duel.FightInfoRankList, int32(randRank))
			i++
		}
		randIndex++
		if randIndex >= len(rs) {
			break
		}

	}

	bs, err := json.Marshal(duel.FightInfoRankList)
	assert.Assert(err == nil, err)
	duel.FightInfoJson = string(bs)
	gSql.Begin().Clean(duel.RoleId).MustExcel(gConst.SQL_UPDATE_DULE_FIGHT_INFO, duel.FightInfoJson, duel.RoleId)

}

func (mgr *DuelManager) GetRankList(ctx *data.Context, roleId int64, begin int32, end int32) {
	begin = util.MaxInt32(0, util.MinInt32(begin-1, int32(len(mgr.RankToProto)-1)))
	end = util.MaxInt32(0, util.MinInt32(end, int32(len(mgr.RankToProto))))
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetDuelRankListDoneMsg(ctx.EventMsg.GetEnvMsg(), mgr.RankToProto[begin:end]))
}

func (mgr *DuelManager) AwardTopRank(ctx *data.Context) {
	now := time.Now()
	body := "亲爱的冒险者：\n       您在困兽场中排名%d，我们特为您献上了%d符文！请及时查收哦！"
	mailee := &data.Mailee{
		Name:     fmt.Sprintf("duel_%d", ctx.NowSec),
		Title:    "困兽场奖励",
		AwardStr: "",
		Award:    nil,
		GiftCode: "",
		Body:     "",
		IsDel:    false,
		IsRead:   false,
		Time:     now,
		TimeOut:  now.AddDate(0, 0, 7),
	}
	for _, duelInfo := range mgr.Rank {
		if duelInfo == nil || duelInfo.RoleId <= 1000 {
			continue
		}
		rankCfg := config.DuelCfgBy(duelInfo.Rank)
		if rankCfg.RankAward == nil || len(rankCfg.RankAward.AwardList) == 0 {
			continue
		}
		mailee.RoleId = duelInfo.RoleId
		mailee.AwardStr = config.AwardToStr(rankCfg.RankAward)
		mailee.Award = rankCfg.RankAward
		mailee.Body = fmt.Sprintf(body, duelInfo.Rank, rankCfg.RankAward.AwardList[0].N)
		gMailMgr.SendMailee(ctx, mailee)
	}
}

func (mgr *DuelManager) NotifyDuelInfo(ctx *data.Context, roleId int64) []*protocol.DuelInfo {

	myDuel := mgr.MyDuelBy(ctx, roleId)
	if myDuel == nil {
		myDuel = gDuelMgr.SetDuelRank(ctx, gRoleMgr.RoleBy(ctx, roleId))
	}
	assert.Assert(myDuel != nil, "duel not open, roleId:%d", roleId)
	myDuelClient := myDuel.GetForClient()
	myDuelClient.StubInfo = gStubMgr.GetForClient(ctx, gStubMgr.StubInfoBy(ctx, roleId, gConst.STUB_TYPE_NORMAL))

	if len(myDuel.FightInfoRankList) == 0 {
		mgr.RefreshFighList(ctx, myDuel)
	}

	var fightList []*protocol.DuelInfo

	for _, rank := range myDuel.FightInfoRankList {
		if rank > 0 && rank < int32(len(mgr.Rank)) {
			rankDuel := mgr.Rank[rank]
			client := rankDuel.GetForClient()
			if !rankDuel.IsRobot {
				clientRole := gRoleMgr.RoleBy(ctx, client.RoleId)
				gAttrMgr.RoleAttrBy(ctx, clientRole, false)
				client.Name, client.Avatar = clientRole.Name, clientRole.Avatar
				client.StubInfo, _ = gStubMgr.ViewOtherStub(ctx, client.RoleId, gConst.STUB_TYPE_NORMAL)
				client.FightRole = gViewMgr.ViewFightRole(ctx, clientRole)
			}
			fightList = append(fightList, client)
		}
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyDuelInfoMsg(myDuelClient, fightList))

	return fightList
}
