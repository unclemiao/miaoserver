package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type StubManager struct {
}

func init() {
	gStubMgr = &StubManager{}
}

func (mgr *StubManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getRoleCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		mgr.afterConfig()
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetStub, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg = msg.GetC2SGetStub()
			roleId = ctx.RoleId
			kind   = repMsg.Kind
		)
		mgr.NotifyStub(ctx, roleId, kind)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_StubPet, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg    = msg.GetC2SStubPet()
			roleId    = ctx.RoleId
			petId     = repMsg.PetId
			stubIndex = repMsg.StubIndex
		)
		mgr.StubPet(ctx, roleId, petId, repMsg.Kind, stubIndex, true)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_StubHelpPet, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg    = msg.GetC2SStubHelpPet()
			roleId    = ctx.RoleId
			petId     = repMsg.PetId
			stubIndex = repMsg.StubIndex
			helpIndex = repMsg.HelpIndex
		)
		mgr.StubHelpPet(ctx, roleId, petId, repMsg.Kind, stubIndex, helpIndex, true)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_UnlockStubHelp, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg    = msg.GetC2SUnlockStubHelp()
			roleId    = ctx.RoleId
			stubIndex = repMsg.StubIndex
			helpIndex = repMsg.HelpIndex
		)

		mgr.UnlockHelp(ctx, roleId, repMsg.Kind, stubIndex, helpIndex)
	})

}

func (mgr *StubManager) afterConfig() {
	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		if args.IsNew {
			roleStub := &data.RoleStubGroud{
				StubInfoMap: make(map[protocol.StubKind]*data.StubInfo),
			}
			roleStub.StubInfoMap[gConst.STUB_TYPE_NORMAL] = mgr.newStubInfo(ctx, args.RoleId, gConst.STUB_TYPE_NORMAL, true)
			roleStub.StubInfoMap[gConst.STUB_TYPE_PET_KING] = mgr.newStubInfo(ctx, args.RoleId, gConst.STUB_TYPE_PET_KING, true)
			gCache.Set(mgr.getRoleCacheKey(args.RoleId), roleStub, gConst.SERVER_DATA_CACHE_TIME_OUT)
		}
	})

}

func (mgr *StubManager) getRoleCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_STUB, roleId)
}

func (mgr *StubManager) RoleStubBy(ctx *data.Context, roleId int64) *data.RoleStubGroud {
	// rollback 后处理
	cacheKey := mgr.getRoleCacheKey(roleId)

	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleStub := &data.RoleStubGroud{
			StubInfoMap: make(map[protocol.StubKind]*data.StubInfo),
		}
		var dataList []*data.StubDb
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_STUB, roleId)
		for _, db := range dataList {

			if roleStub.StubInfoMap[db.Kind] == nil {
				roleStub.StubInfoMap[db.Kind] = mgr.newStubInfo(ctx, roleId, db.Kind, false)
			}
			stubInfo := roleStub.StubInfoMap[db.Kind]
			stubInfo.Kind = db.Kind

			unit := stubInfo.UnitList[db.StubIndex-1]
			unit.Id = db.Id
			if db.PetId > 0 {
				unit.PetId = db.PetId
				stubInfo.PetInStub[db.PetId] = db.StubIndex
			}

			unit.HelperList[0].Lock = db.HelpId1 < 0
			unit.HelperList[0].PetId = db.HelpId1
			if db.HelpId1 > 0 {
				stubInfo.PetInHelp[db.HelpId1] = 1
			}

			unit.HelperList[1].Lock = db.HelpId2 < 0
			unit.HelperList[1].PetId = db.HelpId2
			if db.HelpId2 > 0 {
				stubInfo.PetInHelp[db.HelpId2] = 2
			}

		}
		return roleStub, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	stub := rv.(*data.RoleStubGroud)
	assert.Assert(stub != nil, "stub not found")

	return stub
}

func (mgr *StubManager) newStubInfo(ctx *data.Context, roleId int64, kind protocol.StubKind, createDb bool) *data.StubInfo {

	ul := make([]*data.StubUnit, gConst.STUB_NUM_MAX)
	gSql.Begin().Clean(roleId)
	for index, u := range ul {

		u = &data.StubUnit{
			Id:         0,
			HelperList: make([]*data.HelpUnit, gConst.HELP_NUM_MAX),
		}
		if createDb {
			u.Id = gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_STUB)
		}

		u.Index = int32(index) + 1
		u.HelperList[0] = &data.HelpUnit{
			Index: 1,
			Lock:  !(index == 0) && kind != protocol.StubKind_StubPetKing,
			PetId: util.ChoiceInt64(index == 0 || kind == protocol.StubKind_StubPetKing, 0, -1),
		}
		u.HelperList[1] = &data.HelpUnit{
			Index: 2,
			Lock:  kind != protocol.StubKind_StubPetKing,
			PetId: util.ChoiceInt64(kind == protocol.StubKind_StubPetKing, 0, -1),
		}
		ul[index] = u
		if createDb {
			gSql.MustExcel(gConst.SQL_INSERT_STUB, u.Id, roleId, kind, u.Index, 0, u.HelperList[0].PetId, u.HelperList[1].PetId)
		}
	}

	stubInfo := &data.StubInfo{
		RoleId:    roleId,
		Kind:      kind,
		UnitList:  ul,
		PetInStub: make(map[int64]int32),
		PetInHelp: make(map[int64]int32),
	}

	return stubInfo
}

func (mgr *StubManager) StubInfoBy(ctx *data.Context, roleId int64, kind protocol.StubKind) *data.StubInfo {
	return mgr.RoleStubBy(ctx, roleId).StubInfoMap[kind]
}

func (mgr *StubManager) InStub(ctx *data.Context, roleId int64, petId int64) (bool, protocol.StubKind) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubNormal)
	if stubInfo.PetInStub[petId] > 0 {
		return true, protocol.StubKind_StubNormal
	}
	if stubInfo.PetInHelp[petId] > 0 {
		return true, protocol.StubKind_StubNormal
	}

	stubInfo = mgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubPetKing)
	if stubInfo.PetInStub[petId] > 0 {
		return true, protocol.StubKind_StubPetKing
	}
	if stubInfo.PetInHelp[petId] > 0 {
		return true, protocol.StubKind_StubPetKing
	}
	role := gRoleMgr.RoleBy(ctx, roleId)
	if role.Horse == petId {
		return true, protocol.StubKind_StubNormal
	}
	return false, protocol.StubKind_StubKind_Unknown
}

func (mgr *StubManager) InStubNotHelp(ctx *data.Context, roleId int64, petId int64) (bool, protocol.StubKind) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubNormal)
	if stubInfo.PetInStub[petId] > 0 && stubInfo.PetInHelp[petId] == 0 {
		return true, protocol.StubKind_StubNormal
	}

	stubInfo = mgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubPetKing)
	if stubInfo.PetInStub[petId] > 0 && stubInfo.PetInHelp[petId] == 0 {
		return true, protocol.StubKind_StubPetKing
	}
	return false, protocol.StubKind_StubKind_Unknown
}

func (mgr *StubManager) LevelStub(ctx *data.Context, roleId int64, petId int64, kind protocol.StubKind) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	for _, unit := range stubInfo.UnitList {
		if unit == nil {
			continue
		}
		if unit.PetId == petId {
			mgr.StubPet(ctx, roleId, 0, kind, unit.Index, true)
			return
		}
		for _, hunit := range unit.HelperList {
			if hunit == nil {
				continue
			}
			if hunit.PetId == petId {
				mgr.StubHelpPet(ctx, roleId, 0, kind, unit.Index, hunit.Index, true)
				return
			}
		}
	}

}

// 下发阵型数据
func (mgr *StubManager) GetForClient(ctx *data.Context, stubInfo *data.StubInfo) *protocol.StubInfo {
	roleId := stubInfo.RoleId
	stubPbInfo := &protocol.StubInfo{
		Kind: stubInfo.Kind,
	}
	for _, unit := range stubInfo.UnitList {
		if unit == nil {
			continue
		}
		pbUnit := &protocol.StubIndexInfo{
			Index: unit.Index,
		}
		if unit.PetId > 0 {
			pet := gPetMgr.PetBy(ctx, roleId, unit.PetId)
			gAttrMgr.PetAttrBy(ctx, pet, false)
			pbUnit.Unit = pet.GetForClient()
		}
		for _, hUnit := range unit.HelperList {
			if hUnit == nil {
				continue
			}
			hUnitPb := &protocol.HelpIndexInfo{
				Index: hUnit.Index,
				Lock:  hUnit.Lock,
			}
			if hUnit.PetId > 0 {
				pet := gPetMgr.PetBy(ctx, roleId, hUnit.PetId)
				gAttrMgr.PetAttrBy(ctx, pet, false)
				hUnitPb.Unit = pet.GetForClient()
			}
			pbUnit.HelpIndexInfo = append(pbUnit.HelpIndexInfo, hUnitPb)
		}

		stubPbInfo.StubIndexInfoList = append(stubPbInfo.StubIndexInfoList, pbUnit)
	}
	return stubPbInfo
}

// 下发阵型数据
func (mgr *StubManager) NotifyStub(ctx *data.Context, roleId int64, kind protocol.StubKind) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	stubInfoPb := mgr.GetForClient(ctx, stubInfo)
	mgr.StubPbAttrBy(ctx, stubInfoPb)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyStubMsg(stubInfoPb))
}

// pet == nil 表示下阵
func (mgr *StubManager) StubPet(ctx *data.Context, roleId int64, petId int64, kind protocol.StubKind, stubIndex int32, remote bool) *data.StubInfo {
	role := gRoleMgr.RoleBy(ctx, roleId)
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	gSql.Begin().Clean(roleId)
	if petId == 0 {
		// 下阵
		unit := stubInfo.UnitList[stubIndex-1]
		atPetId := unit.PetId
		assert.Assert(atPetId > 0, "stub[%d] not pet", stubIndex-1)
		unit.PetId = 0
		delete(stubInfo.PetInStub, atPetId)
		gAttrMgr.PetAttrBy(ctx, gPetMgr.PetBy(ctx, roleId, atPetId), true)
		gSql.MustExcel(gConst.SQL_UPDATE_STUB, unit.PetId, unit.HelperList[0].PetId, unit.HelperList[1].PetId, unit.Id)
	} else {
		assert.Assert(stubInfo.PetInHelp[petId] == 0, "pet is in help")
		assert.Assert(role.Horse != petId, "pet in horse")
		oldPetId := stubInfo.UnitList[stubIndex-1].PetId // 目标助阵位
		if oldPetId > 0 {
			// 原有的下阵
			if stubInfo.PetInStub[petId] > 0 {
				// 阵位中交换
				toUnit := stubInfo.UnitList[stubIndex-1]
				fromUnit := stubInfo.UnitList[stubInfo.PetInStub[petId]-1]

				toUnit.PetId = petId
				fromUnit.PetId = oldPetId

				stubInfo.PetInStub[oldPetId] = stubInfo.PetInStub[petId]
				stubInfo.PetInStub[petId] = stubIndex
				gSql.MustExcel(gConst.SQL_UPDATE_STUB, toUnit.PetId, toUnit.HelperList[0].PetId, toUnit.HelperList[1].PetId, toUnit.Id)

				gSql.MustExcel(gConst.SQL_UPDATE_STUB, fromUnit.PetId, fromUnit.HelperList[0].PetId, fromUnit.HelperList[1].PetId, fromUnit.Id)
			} else {
				toUnit := stubInfo.UnitList[stubIndex-1]
				toUnit.PetId = petId
				gSql.MustExcel(gConst.SQL_UPDATE_STUB, toUnit.PetId, toUnit.HelperList[0].PetId, toUnit.HelperList[1].PetId, toUnit.Id)
				delete(stubInfo.PetInStub, oldPetId)
				stubInfo.PetInStub[petId] = stubIndex
				gAttrMgr.PetAttrBy(ctx, gPetMgr.PetBy(ctx, roleId, oldPetId), true)
			}
		} else {
			// 目标位置没人
			if stubInfo.PetInStub[petId] > 0 {
				fromUnit := stubInfo.UnitList[stubInfo.PetInStub[petId]-1]
				fromUnit.PetId = 0
				gSql.MustExcel(gConst.SQL_UPDATE_STUB, fromUnit.PetId, fromUnit.HelperList[0].PetId, fromUnit.HelperList[1].PetId, fromUnit.Id)
			}
			toUnit := stubInfo.UnitList[stubIndex-1]
			toUnit.PetId = petId
			gSql.MustExcel(gConst.SQL_UPDATE_STUB, toUnit.PetId, toUnit.HelperList[0].PetId, toUnit.HelperList[1].PetId, toUnit.Id)

			stubInfo.PetInStub[petId] = stubIndex

		}

	}

	if remote {
		mgr.NotifyStub(ctx, roleId, kind)
	}
	return stubInfo
}

// petId == 0 表示下阵
func (mgr *StubManager) StubHelpPet(ctx *data.Context, roleId int64, petId int64, kind protocol.StubKind, stubIndex int32, helpIndex int32, remote bool) *data.StubInfo {
	role := gRoleMgr.RoleBy(ctx, roleId)
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	unitInfo := stubInfo.UnitList[stubIndex-1]
	gSql.Begin().Clean(roleId)
	if petId == 0 {
		// 下助阵
		targetId := unitInfo.HelperList[helpIndex-1].PetId
		assert.Assert(targetId > 0, "this stub not helper, stubIndex:%d, helpIndex:%d", stubIndex, helpIndex)
		unitInfo.HelperList[helpIndex-1].PetId = 0
		delete(stubInfo.PetInStub, targetId)
		delete(stubInfo.PetInHelp, targetId)
		gAttrMgr.PetAttrBy(ctx, gPetMgr.PetBy(ctx, roleId, targetId), true)
		gSql.MustExcel(gConst.SQL_UPDATE_STUB, unitInfo.PetId, unitInfo.HelperList[0].PetId, unitInfo.HelperList[1].PetId, unitInfo.Id)
	} else {
		assert.Assert(role.Horse != petId, "pet in horse")
		targetHelperId := unitInfo.HelperList[helpIndex-1].PetId
		if targetHelperId > 0 {
			// 目标位置有人
			if stubInfo.PetInHelp[petId] > 0 {
				// 我从其他助阵位过来的
				// 双方都在援助中[在同一阵位或者不在同一阵位]
				if stubInfo.PetInStub[petId] == stubInfo.PetInStub[targetHelperId] {
					// 在同一阵位
					unitInfo.HelperList[helpIndex-1].PetId = petId
					unitInfo.HelperList[stubInfo.PetInHelp[petId]-1].PetId = targetHelperId

					stubInfo.PetInHelp[targetHelperId] = stubInfo.PetInHelp[petId]
					stubInfo.PetInHelp[petId] = helpIndex
					gSql.MustExcel(gConst.SQL_UPDATE_STUB, unitInfo.PetId, unitInfo.HelperList[0].PetId, unitInfo.HelperList[1].PetId, unitInfo.Id)
				} else {
					// 不在同一阵位
					unitInfo.HelperList[helpIndex-1].PetId = petId

					gSql.MustExcel(gConst.SQL_UPDATE_STUB, unitInfo.PetId, unitInfo.HelperList[0].PetId, unitInfo.HelperList[1].PetId, unitInfo.Id)

					helperUnit := stubInfo.UnitList[stubInfo.PetInStub[petId]-1]
					helperUnit.HelperList[stubInfo.PetInHelp[petId]-1].PetId = targetHelperId
					gSql.MustExcel(gConst.SQL_UPDATE_STUB, helperUnit.PetId, helperUnit.HelperList[0].PetId, helperUnit.HelperList[1].PetId, helperUnit.Id)

					stubInfo.PetInHelp[targetHelperId] = stubInfo.PetInHelp[petId]
					stubInfo.PetInStub[targetHelperId] = stubInfo.PetInStub[petId]

					stubInfo.PetInHelp[petId] = helpIndex
					stubInfo.PetInStub[petId] = stubIndex
				}

			} else {
				// 我从下方空闲出上助阵的
				assert.Assert(stubInfo.PetInStub[petId] == 0, "pet in stub, can not help")

				unitInfo.HelperList[helpIndex-1].PetId = petId
				gSql.MustExcel(gConst.SQL_UPDATE_STUB, unitInfo.PetId, unitInfo.HelperList[0].PetId, unitInfo.HelperList[1].PetId, unitInfo.Id)

				delete(stubInfo.PetInStub, targetHelperId)
				delete(stubInfo.PetInHelp, targetHelperId)

				stubInfo.PetInHelp[petId] = helpIndex
				stubInfo.PetInStub[petId] = stubIndex

				gAttrMgr.PetAttrBy(ctx, gPetMgr.PetBy(ctx, roleId, targetHelperId), true)
			}
		} else {

			if stubInfo.PetInHelp[petId] > 0 {
				// 我从其他助阵位过来的
				fromUnit := stubInfo.UnitList[stubInfo.PetInStub[petId]-1]
				fromUnit.HelperList[stubInfo.PetInHelp[petId]-1].PetId = 0

				gSql.MustExcel(gConst.SQL_UPDATE_STUB, fromUnit.PetId, fromUnit.HelperList[0].PetId, fromUnit.HelperList[1].PetId, fromUnit.Id)
			}
			unitInfo.HelperList[helpIndex-1].PetId = petId
			gSql.MustExcel(gConst.SQL_UPDATE_STUB, unitInfo.PetId, unitInfo.HelperList[0].PetId, unitInfo.HelperList[1].PetId, unitInfo.Id)

			stubInfo.PetInHelp[petId] = helpIndex
			stubInfo.PetInStub[petId] = stubIndex
		}
	}

	if remote {
		mgr.NotifyStub(ctx, roleId, kind)
	}
	return stubInfo
}

func (mgr *StubManager) UnlockHelp(ctx *data.Context, roleId int64, kind protocol.StubKind, stubIndex int32, helpIndex int32) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	unit := stubInfo.UnitList[stubIndex-1]
	unit.HelperList[helpIndex-1].Lock = false
	unit.HelperList[helpIndex-1].PetId = util.MaxInt64(0, unit.HelperList[helpIndex-1].PetId)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_STUB,
		0,
		unit.HelperList[0].PetId,
		unit.HelperList[1].PetId,
		unit.Id,
	)
	gRoleMgr.Send(ctx, roleId, protocol.MakeUnlockStubHelpDoneMsg(ctx.EventMsg.GetEnvMsg(), stubIndex, helpIndex, kind))
}

func (mgr *StubManager) ViewOtherStub(ctx *data.Context, roleId int64, kind protocol.StubKind) (*protocol.StubInfo, bool) {
	stubInfo := mgr.StubInfoBy(ctx, roleId, kind)
	stubInfoPb := &protocol.StubInfo{
		Kind: kind,
	}
	empty := true
	for _, unit := range stubInfo.UnitList {
		if unit == nil {
			continue
		}
		pbUnit := &protocol.StubIndexInfo{
			Index: unit.Index,
		}
		if unit.PetId > 0 {
			viewPet := gPetMgr.ViewPetBy(ctx, roleId, unit.PetId)
			if viewPet != nil {
				pbUnit.Unit = viewPet.GetForClient()
				empty = false
			}
		}
		for _, hUnit := range unit.HelperList {
			if hUnit == nil {
				continue
			}
			pbHelpUnit := &protocol.HelpIndexInfo{
				Index: hUnit.Index,
				Lock:  hUnit.Lock,
			}
			if hUnit.PetId > 0 {
				viewPet := gPetMgr.ViewPetBy(ctx, roleId, hUnit.PetId)
				if viewPet != nil {
					pbHelpUnit.Unit = viewPet.GetForClient()
					empty = false
				}
			}
			pbUnit.HelpIndexInfo = append(pbUnit.HelpIndexInfo, pbHelpUnit)
		}
		stubInfoPb.StubIndexInfoList = append(stubInfoPb.StubIndexInfoList, pbUnit)
	}

	mgr.StubPbAttrBy(ctx, stubInfoPb)
	return stubInfoPb, empty
}

func (mgr *StubManager) StubPbAttrBy(ctx *data.Context, stub *protocol.StubInfo) {
	for _, u := range stub.StubIndexInfoList {
		if u.Unit == nil {
			continue
		}
		for _, help := range u.HelpIndexInfo {
			helpPet := help.Unit
			if helpPet == nil {
				continue
			}
			u.Unit.Atk += util.MaxInt32(1, int32(float64(helpPet.Atk)*0.05))
			u.Unit.Def += util.MaxInt32(1, int32(float64(helpPet.Def)*0.05))
			u.Unit.HpMax += util.MaxInt64(1, int64(float64(helpPet.HpMax)*0.05))
		}
	}
}
