package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type FoodBookManager struct {
}

func init() {
	gFoodBookMgr = &FoodBookManager{}
}

func (mgr *FoodBookManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})
	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		if args.IsNew {
			mgr.NewBook(ctx, args.RoleId, ctx.Session.Sid)
		}
		TimeAfter(nil, 0, func(ctx2 *data.Context) {
			mgr.NotifyBook(ctx2, args.RoleId)
		}, ctx.Session)
	}, 0)

	gEventMgr.When(protocol.EnvelopeType_C2S_BuyFood, mgr.HandlerBuy)
	gEventMgr.When(protocol.EnvelopeType_C2S_FoodBookLevelUp, mgr.HandlerLevelUp)

	gEventMgr.When(protocol.EnvelopeType_C2S_FoodBookHighBuy, mgr.HandlerBeginHighBuy)

}

func (mgr *FoodBookManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FOOD_BOOK, roleId)
}

func (mgr *FoodBookManager) FoodBookBy(ctx *data.Context, roleId int64) *data.FoodBook {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.FoodBook
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_FOOD_BOOK, roleId)
		if len(dataList) > 0 {
			return dataList[0], nil
		}
		return nil, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.FoodBook)
}

func (mgr *FoodBookManager) NewBook(ctx *data.Context, roleId int64, sid int32) {
	book := &data.FoodBook{
		RoleId: roleId,
		Level:  1,
	}
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_FOOD_BOOK, book.RoleId, book.Level, book.Exp)
	cacheKey := mgr.getCacheKey(roleId)
	gCache.Set(cacheKey, book, gConst.SERVER_DATA_CACHE_TIME_OUT)
}

func (mgr *FoodBookManager) AddExp(ctx *data.Context, roleId int64, exp int64) {
	gSql.Begin().Clean(roleId)
	book := mgr.FoodBookBy(ctx, roleId)
	book.Exp += exp
	bookCfg := config.FoodBookCfgBy(book.Level)
	for {
		if bookCfg == nil || bookCfg.Exp == 0 || book.Exp < bookCfg.Exp {
			break
		}
		book.Level++
		book.Exp -= bookCfg.Exp
	}

	gSql.MustExcel(gConst.SQL_UPDATE_FOOD_BOOK, book.Level, book.Exp, book.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFoodBookMsg(book))
}

func (mgr *FoodBookManager) LevelUp(ctx *data.Context, roleId int64) {

}

func (mgr *FoodBookManager) BeginHighBuy(ctx *data.Context, roleId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	assert.Assert(role.Newbie >= int32(protocol.GuideType_buy0), "invalid newbie")
	gSql.Begin().Clean(roleId)
	book := mgr.FoodBookBy(ctx, roleId)
	assert.Assert(book.HighEndTime <= ctx.NowSec, "high buy beginning")
	book.HighEndTime = ctx.NowSec + time_helper.Minute*5
	gSql.MustExcel(gConst.SQL_UPDATE_HIGH_BUY, book.HighEndTime, book.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFoodBookMsg(book))
}

func (mgr *FoodBookManager) NowFoodCfg(ctx *data.Context, roleId int64) *data.FoodBookCfg {

	foodBook := mgr.FoodBookBy(ctx, roleId)
	var lv int32 = 1
	if foodBook.Level >= 4 {
		lv = foodBook.Level - 3
	}
	cfg := config.FoodBookCfgBy(lv)
	return cfg
}

func (mgr *FoodBookManager) NotifyBook(ctx *data.Context, roleId int64) {
	book := mgr.FoodBookBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFoodBookMsg(book))
}

// ******************************************* controllers *******************************************
func (mgr *FoodBookManager) HandlerBuy(ctx *data.Context, args *protocol.Envelope) {
	roleId := ctx.RoleId
	role := gRoleMgr.RoleBy(ctx, roleId)

	book := mgr.FoodBookBy(ctx, roleId)
	buyFoodCfg := config.FoodBookCfgBy(book.Level)

	if book.HighEndTime <= ctx.NowSec {
		dropCfg := config.ItemDropRandom(buyFoodCfg.DropId)
		gItemMgr.TakeAward(ctx, roleId, buyFoodCfg.Stuff, 1, protocol.ItemActionType_Action_Buy)

		if role.Newbie == int32(protocol.GuideType_buy0) {
			gItemMgr.GiveItem(ctx, roleId, 500001, 1, protocol.ItemActionType_Action_Buy)
		} else {
			gItemMgr.GiveItem(ctx, roleId, dropCfg.ItemId, 1, protocol.ItemActionType_Action_Buy)
		}
	} else {
		dropCfg := config.ItemDropRandom(buyFoodCfg.HighBuyDropId)
		gItemMgr.TakeAward(ctx, roleId, buyFoodCfg.HighBuyStuff, 1, protocol.ItemActionType_Action_Buy)
		gItemMgr.GiveItem(ctx, roleId, dropCfg.ItemId, 1, protocol.ItemActionType_Action_Buy)
		mgr.AddExp(ctx, roleId, buyFoodCfg.HighBuyAddExp)
	}

}

func (mgr *FoodBookManager) HandlerLevelUp(ctx *data.Context, args *protocol.Envelope) {
	mgr.LevelUp(ctx, ctx.RoleId)
}

func (mgr *FoodBookManager) HandlerBeginHighBuy(ctx *data.Context, args *protocol.Envelope) {
	assert.Assert(len(args.AdSdkToken) > 0, "invalid adsdk")
	mgr.BeginHighBuy(ctx, ctx.RoleId)
}
