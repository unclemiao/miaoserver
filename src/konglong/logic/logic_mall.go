package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type MallManager struct {
	cache *cache.ShardedCache
}

var gMallMgr *MallManager

func init() {
	gMallMgr = &MallManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4),
	}
}

func (mgr *MallManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		TimeAfter(ctx, 0, func(ctx *data.Context) {
			gRoleMgr.SendAtOnce(ctx, args.RoleId, protocol.MakeNotifyMallCfgMsg(config.GetAllMallCfgPbList()))
			gRoleMgr.SendAtOnce(ctx, args.RoleId, protocol.MakeNotifyRoleMallMsg(mgr.RoleMallsGetForClient(ctx, args.RoleId)))
		}, ctx.Session)
	})
}

func (mgr *MallManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ROLE_MALL, roleId)
}

func (mgr *MallManager) RoleMallsBy(ctx *data.Context, roleId int64) *data.RoleMalls {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleMall := &data.RoleMalls{
			MallItemMap: make(map[int32]*data.RoleMall),
		}
		var dataList []*data.RoleMall

		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ROLE_MALL, roleId)
		for _, info := range dataList {
			roleMall.MallItemMap[info.CfgId] = info
		}
		return roleMall, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.RoleMalls)
}

func (mgr *MallManager) RoleMallBy(ctx *data.Context, roleId int64, cfgId int32) *data.RoleMall {
	roleMalls := mgr.RoleMallsBy(ctx, roleId)
	return roleMalls.MallItemMap[cfgId]
}

func (mgr *MallManager) RoleMallsGetForClient(ctx *data.Context, roleId int64) []*protocol.RoleMall {
	roleMalls := mgr.RoleMallsBy(ctx, roleId)
	var pbList []*protocol.RoleMall
	for _, m := range roleMalls.MallItemMap {
		pbList = append(pbList, &protocol.RoleMall{
			CfgId:  m.CfgId,
			Amount: m.Amount,
		})
	}
	return pbList
}

func (mgr *MallManager) Buy(ctx *data.Context, roleId int64, mallId int32, rmb int64, bill string) {
	mallItemCfg := config.GetMallCfg(mallId)
	assert.Assert(mallItemCfg != nil, "mallId err, %d", mallId)
	role := gRoleMgr.RoleBy(ctx, roleId)

	mall := mgr.RoleMallBy(ctx, roleId, mallId)
	gSql.Begin().Clean(roleId)
	if mall == nil {
		mall = &data.RoleMall{
			Id:     gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_ROLE_MALL),
			RoleId: roleId,
			CfgId:  mallId,
			Amount: 1,
		}
		allMall := mgr.RoleMallsBy(ctx, roleId)
		allMall.MallItemMap[mallId] = mall
		gSql.MustExcel(gConst.SQL_INSTER_ROLE_MALL, mall.Id, mall.RoleId, mall.CfgId, mall.Amount)
	} else {
		assert.Assert(mallItemCfg.Limit == 0 || mall.Amount < mallItemCfg.Limit, "!限购已满")
		mall.Amount++
		gSql.MustExcel(gConst.SQL_UPDATE_ROLE_MALL, mall.Amount, mall.Id)
	}

	if rmb > 0 {
		assert.Assert(gAdminMgr.ServerCmd.Platform == "test" || rmb == mallItemCfg.Rmb, "rmb err")
		gRoleMgr.AddPay(ctx, roleId, int32(rmb))
	} else if mallItemCfg.Diamonds > 0 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Diamonds), mallItemCfg.Diamonds, protocol.ItemActionType_Action_Mall_Buy)
	} else if mallItemCfg.Gem > 0 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Gem), mallItemCfg.Gem, protocol.ItemActionType_Action_Mall_Buy)
	}
	gGameLogMgr.OnMallBuy(ctx, role, mallItemCfg.CfgId, 1, 0, 0, bill, rmb)

	gItemMgr.GiveAward(ctx, roleId, mallItemCfg.SellItem, 1, protocol.ItemActionType_Action_Mall_Buy)

	if mallItemCfg.SellItem != nil && len(mallItemCfg.SellItem.AwardList) > 0 {
		var giveGem int32
		for _, cfg := range mallItemCfg.SellItem.AwardList {
			if cfg.CfgId == int64(protocol.CurrencyType_Gem) {
				giveGem += int32(cfg.N * int64(cfg.Rate) / 1000)
			}
		}
		gFeastMgr.onMallBuy(ctx, roleId, giveGem, int32(rmb))
	}
	if mallItemCfg.SellKind == 4 {
		gFeastMgr.onBuyMonthCard(ctx, roleId)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleMallMsg([]*protocol.RoleMall{&protocol.RoleMall{
		CfgId:  mall.CfgId,
		Amount: mall.Amount,
	}}))
}
