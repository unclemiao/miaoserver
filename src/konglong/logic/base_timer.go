package logic

import (
	"time"

	"goproject/src/konglong/data"
)

func TimeAfter(
	ctx *data.Context,
	t time.Duration,
	f func(ctx *data.Context),
	args ...interface{},
) *time.Timer {
	var session *data.Session
	if len(args) > 0 {
		session = args[0].(*data.Session)
	}
	if t == 0 {
		go func() {
			gServer.ReceiveChan <- &data.Context{
				Session: session,
				MsgId:   data.EVENT_MSGID_TIMER,
				EventMsg: &data.EventTimer{
					EventBase: data.EventBase{MsgId: data.EVENT_MSGID_TIMER},
					Func:      f,
					Ctx:       ctx,
				},
			}
		}()
		return nil

	} else {
		return time.AfterFunc(t, func() {
			gServer.ReceiveChan <- &data.Context{
				Session: session,
				MsgId:   data.EVENT_MSGID_TIMER,
				EventMsg: &data.EventTimer{
					EventBase: data.EventBase{MsgId: data.EVENT_MSGID_TIMER},
					Func:      f,
					Ctx:       ctx,
				},
			}
		})
	}
}

func TimeAfterFunc(
	ctx *data.Context,
	t time.Duration,
	f func(ctx *data.Context),
	finish func(errMsg interface{}),
	args ...interface{},
) *time.Timer {
	var session *data.Session
	if len(args) > 0 {
		session = args[0].(*data.Session)
	}
	eventArgs := &data.Context{
		Session:    session,
		MsgId:      data.EVENT_MSGID_TIMER,
		FinishFunc: finish,
		EventMsg: &data.EventTimer{
			EventBase: data.EventBase{MsgId: data.EVENT_MSGID_TIMER},
			Func:      f,
			Ctx:       ctx,
		},
	}
	if t == 0 {
		go func() { gServer.ReceiveChan <- eventArgs }()
		return nil

	} else {
		return time.AfterFunc(t, func() {
			gServer.ReceiveChan <- eventArgs
		})
	}
}

func TimerLoadConfig() {
	gEventMgr.When(
		data.EVENT_MSGID_TIMER,
		func(ctx *data.Context, args interface{}) {
			if args != nil {
				f, _ := args.(*data.EventTimer)
				if f.Func != nil {
					if f.Ctx != nil {
						f.Func(f.Ctx)
					} else {
						f.Func(ctx)
					}
				}
			}
		},
	)
}
