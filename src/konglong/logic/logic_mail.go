package logic

import (
	"encoding/json"
	"time"

	"goproject/engine/assert"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type MailManager struct {
	ServerMails *data.AllMail
}

func init() {
	gMailMgr = &MailManager{
		ServerMails: &data.AllMail{
			MailMap: make(map[string]*data.Mail),
		},
	}

}

func (mgr *MailManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		gSql.Begin()
		var mailList []*data.Mail
		now := time.Now()
		gSql.MustSelect(&mailList, gConst.SQL_SELECT_MAIL, now)
		for _, m := range mailList {
			if m.EachStr != "" {
				err := json.Unmarshal([]byte(m.EachStr), &m.Each)
				assert.Assert(err == nil, err)
			}
			mgr.ServerMails.MailMap[m.Name] = m
		}
		mgr.ServerMails.MailList = mailList
		gSql.MustExcel(gConst.SQL_UPDATE_MAILEE_DEL_ALL, now)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetMailList, func(ctx *data.Context, msg *protocol.Envelope) {
		_, pbs := mgr.GetMailee(ctx, ctx.RoleId)
		gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyMailListMsg(pbs))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ReadMail, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SReadMail()
		mgr.ReadMailee(ctx, ctx.RoleId, repMsg.GetName())
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardMail, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SAwardMail()
		if repMsg.GetName() != "" {
			mgr.AwardMailee(ctx, ctx.RoleId, repMsg.GetName())
		} else {
			mgr.AwardAllMailee(ctx, ctx.RoleId)
		}

	})

	gEventMgr.When(protocol.EnvelopeType_C2S_DelMail, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SDelMail()
		if repMsg.GetName() != "" {
			mgr.DelMailee(ctx, ctx.RoleId, repMsg.GetName())
		} else {
			mgr.DelAllMailee(ctx, ctx.RoleId)
		}

	})
}

// 获取全服邮件
func (mgr *MailManager) GetMail(ctx *data.Context, name string) *data.Mail {
	return mgr.ServerMails.MailMap[name]
}

// 获取玩家邮件
func (mgr *MailManager) GetMailee(ctx *data.Context, roleId int64) ([]*data.Mailee, []*protocol.Mail) {
	var (
		now          = time.Now()
		role         = gRoleMgr.RoleBy(ctx, roleId)
		tf           = gTimeFeastMgr.TimeFeastBy(ctx, roleId)
		maileeDbList []*data.Mailee                  // 个人邮件列表
		maileeMap    = make(map[string]*data.Mailee) // 个人邮件map
	)

	gSql.MustSelect(&maileeDbList, gConst.SQL_SELECT_MAILEE, roleId, now)
	for _, m := range maileeDbList {
		maileeMap[m.Name] = m
	}
	for _, m := range mgr.ServerMails.MailList {
		if len(m.Each) == 0 {
			// 个人邮件
			continue
		}
		// 找到符合该服务器的邮件
		is_continue := true
		for _, sid := range m.Each {
			if sid == 0 || sid == role.Sid {
				is_continue = false
				break
			}
		}
		if is_continue {
			continue
		}
		if maileeMap[m.Name] == nil && now.Before(m.TimeOut) {
			if !m.Time.Before(tf.NewRole) {
				mailee := &data.Mailee{
					Name:      m.Name,
					RoleId:    roleId,
					Title:     m.Title,
					AwardStr:  m.AwardStr,
					Body:      m.Body,
					IsDel:     false,
					IsRead:    false,
					Time:      m.Time,
					TimeOut:   m.TimeOut,
					AwardTime: time.Time{},
				}
				maileeMap[m.Name] = mailee
				gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_MAILEE, mailee.RoleId, mailee.Name, mailee.Title,
					mailee.AwardStr, mailee.GiftCode, mailee.Time, mailee.Body, mailee.IsDel, mailee.IsRead, mailee.TimeOut, mailee.AwardTime)
			}
		}
	}

	resultList := make([]*data.Mailee, 0, len(maileeMap))
	resultPbList := make([]*protocol.Mail, 0, len(maileeMap))

	for _, m := range maileeMap {
		if m.IsDel {
			continue
		}
		m.Award = config.LoadAwardCfgStr(m.AwardStr)
		resultList = append(resultList, m)
		resultPbList = append(resultPbList, m.GetForClient())
	}

	return resultList, resultPbList
}

// 发送全服邮件
func (mgr *MailManager) SendMail(ctx *data.Context, mail *data.Mail) {

	assert.Assert(mgr.ServerMails.MailMap[mail.Name] == nil, "!邮件名[%s]重复", mail.Name)

	mail.Award = config.LoadAwardCfgStr(mail.AwardStr)

	gSql.Begin().MustExcel(gConst.SQL_INSERT_MAIL, mail.Name, mail.Title, mail.AwardStr, mail.GiftCode, mail.EachStr,
		mail.Time, mail.TimeOut, mail.Body)

	mgr.ServerMails.MailList = append(mgr.ServerMails.MailList, mail)
	mgr.ServerMails.MailMap[mail.Name] = mail
	gRoleMgr.Broadcast(protocol.MakeNotifyMailListMsg([]*protocol.Mail{mail.GetForClient()}))
}

// 发送个人邮件
func (mgr *MailManager) SendMailee(ctx *data.Context, mailee *data.Mailee) {
	if mailee.Award == nil {
		mailee.Award = config.LoadAwardCfgStr(mailee.AwardStr)
	}
	gSql.Begin().Clean(mailee.RoleId).MustExcel(gConst.SQL_INSERT_MAILEE,
		mailee.RoleId, mailee.Name, mailee.Title, mailee.AwardStr, mailee.GiftCode,
		mailee.Time, mailee.Body, mailee.IsDel, mailee.IsRead,
		mailee.TimeOut, mailee.AwardTime)

	gRoleMgr.SendAtOnce(ctx, mailee.RoleId, protocol.MakeNotifyMailListMsg([]*protocol.Mail{mailee.GetForClient()}))

}

// 阅读邮件
func (mgr *MailManager) ReadMailee(ctx *data.Context, roleId int64, mailName string) {
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_MAILEE_READ, true, roleId, mailName)
	gRoleMgr.Send(ctx, roleId, protocol.MakeReadMailDoneMsg(mailName))
}

// 领取奖励
func (mgr *MailManager) AwardMailee(ctx *data.Context, roleId int64, mailName string) {
	var mailee *data.Mailee
	var mailList []*data.Mailee
	isNew := false
	gSql.Begin().Clean(roleId).MustSelect(&mailList, gConst.SQL_SELECT_MAILEE_FROM_ROLE, roleId, mailName)
	if len(mailList) > 0 {
		mailee = mailList[0]
	} else {
		for _, m := range mgr.ServerMails.MailList {
			if len(m.Each) == 0 {
				// 个人邮件
				continue
			}
			// 找到符合该服务器的邮件
			is_continue := true
			for _, sid := range m.Each {
				if sid == 0 || sid == ctx.Sid {
					is_continue = false
					break
				}
			}
			if is_continue {
				continue
			}
			if m.Name == mailName {
				mailee = &data.Mailee{
					Name:      m.Name,
					RoleId:    roleId,
					Title:     m.Title,
					AwardStr:  m.AwardStr,
					Body:      m.Body,
					IsDel:     false,
					IsRead:    false,
					Time:      m.Time,
					TimeOut:   m.TimeOut,
					AwardTime: time.Time{},
				}
				isNew = true
			}
		}
	}

	assert.Assert(mailee != nil, "mail not found")
	assert.Assert(!mailee.IsDel && mailee.TimeOut.After(ctx.NowTime))
	assert.Assert(mailee.AwardTime.IsZero(), "mail awarded")
	awardCfg := config.LoadAwardCfgStr(mailee.AwardStr)
	ctx.ItemMustGive = true
	gItemMgr.GiveAward(ctx, roleId, awardCfg, 1, protocol.ItemActionType_Action_Mail)
	ctx.ItemMustGive = false
	mailee.AwardTime = ctx.NowTime
	mailee.IsRead = true
	if isNew {
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_MAILEE, mailee.RoleId, mailee.Name, mailee.Title,
			mailee.AwardStr, mailee.GiftCode, mailee.Time, mailee.Body, mailee.IsDel, mailee.IsRead, mailee.TimeOut, mailee.AwardTime)
	} else {
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_MAILEE_AWARD, mailee.AwardTime, mailee.IsRead, roleId, mailName)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeAwardMailDoneMsg(mailName))
}

// 删除邮件
func (mgr *MailManager) DelMailee(ctx *data.Context, roleId int64, mailName string) {
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_MAILEE_DEL, true, roleId, mailName)
	gRoleMgr.Send(ctx, roleId, protocol.MakeDelMailDoneMsg(mailName, nil))
}

// 一键领取
func (mgr *MailManager) AwardAllMailee(ctx *data.Context, roleId int64) {
	ms, _ := mgr.GetMailee(ctx, roleId)
	award := &data.AwardCfg{}
	now := ctx.NowTime
	gSql.Begin().Clean(roleId)
	for _, m := range ms {
		if m.AwardTime.IsZero() {
			config.AwardCfgAdd(award, m.Award, 1)
			m.IsRead = true
			gSql.MustExcel(gConst.SQL_UPDATE_MAILEE_AWARD, now, m.IsRead, roleId, m.Name)
		}
	}
	ctx.ItemMustGive = true
	gItemMgr.GiveAward(ctx, roleId, award, 1, protocol.ItemActionType_Action_Mail)
	ctx.ItemMustGive = false
	gRoleMgr.Send(ctx, roleId, protocol.MakeAwardMailDoneMsg(""))
}

// 一键删除
func (mgr *MailManager) DelAllMailee(ctx *data.Context, roleId int64) {
	allMailee, _ := mgr.GetMailee(ctx, roleId)
	var delNameList []string
	for _, m := range allMailee {
		if !m.IsRead || (m.AwardStr != "" && m.AwardTime.IsZero()) {
			continue
		}
		delNameList = append(delNameList, m.Name)
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_MAILEE_DEL, true, roleId, m.Name)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeDelMailDoneMsg("", delNameList))
}
