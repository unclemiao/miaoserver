package logic

import (
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"time"
)

type IdManager struct {
	IdMap           map[int32]map[int32]*data.IdData
	CacheCleanTimer time.Ticker
}

func init() {
	gIdMgr = &IdManager{
		IdMap: make(map[int32]map[int32]*data.IdData),
	}
}

func (mgr *IdManager) LoadConfig() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, args interface{}) {
		mgr.IdMap = make(map[int32]map[int32]*data.IdData)
	})
}

func (mgr *IdManager) Stop() {
	mgr.CacheCleanTimer.Stop()
}

func (mgr *IdManager) idBy(sid int32, idType int32) *data.IdData {

	serverIdData := mgr.IdMap[sid]
	if serverIdData == nil {
		serverIdData = make(map[int32]*data.IdData)
	}
	idData := serverIdData[idType]
	if idData == nil {
		var idDataList []*data.IdData

		gSql.MustSelect(&idDataList, gConst.SQL_ID_SELECT, idType, sid)

		if len(idDataList) > 0 {
			idData = idDataList[0]
		}
	}
	serverIdData[idType] = idData
	mgr.IdMap[sid] = serverIdData

	return idData
}

func (mgr *IdManager) GenerateId(ctx *data.Context, sid int32, idType int32) int64 {
	idData := mgr.idBy(sid, idType)

	if idData == nil {
		// 初始化数据
		gSql.Begin().MustExcel(gConst.SQL_ID_INSERT, idType, sid, 1)
		idData = &data.IdData{
			IdType:     idType,
			GenerateId: 1,
		}
	} else {
		// 更新
		gSql.Begin().MustExcel(gConst.SQL_ID_UPDATE, idData.GenerateId+1, idType, sid)
		idData.GenerateId += 1
	}

	id := idData.GenerateId*1000 + int64(sid)

	return id
}
