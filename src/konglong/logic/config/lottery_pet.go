package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var lotteryPetCfgWeightList []util.WeightItr
var lotteryPetDoubleSCfgWeightList []util.WeightItr

var lotteryDailyPetCfgWeightList []util.WeightItr
var lotteryDailyPetACfgWeightList []util.WeightItr
var lotteryDailyPetDoubleSCfgWeightList []util.WeightItr
var lotteryDailyPetDoubleASCfgWeightList []util.WeightItr
var lotteryTenPetSCfgWeightList []util.WeightItr

var lotteryBoxCfgWeightList []util.WeightItr

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadLotteryPetCfg,
		CheckFunc: CheckLotteryPetCfg,
		Order:     10,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadLotteryPetScoreCfg,
		CheckFunc: CheckLotteryPetScoreCfg,
	})
}

func LoadLotteryPetCfg() {
	logs.Infof("======================= LoadLotteryPetCfg =======================")
	var datas []*data.LotteryPetCfg
	err := excel.XlsConfigLoad("XL_LOTTERYPET", &datas, nil)

	assert.Assert(err == nil, err)

	lotteryPetCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryPetDoubleSCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryDailyPetCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryDailyPetACfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryDailyPetDoubleSCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryDailyPetDoubleASCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryBoxCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotteryTenPetSCfgWeightList = make([]util.WeightItr, 0, len(datas))

	// 构建双倍S列表和A列表

	for _, cfg := range datas {
		tmp := &data.LotteryPetCfg{}
		*tmp = *cfg
		switch cfg.Kind {
		case 11:
			lotteryPetCfgWeightList = append(lotteryPetCfgWeightList, tmp)
		case 12:
			lotteryPetDoubleSCfgWeightList = append(lotteryPetDoubleSCfgWeightList, tmp)
		case 21:
			lotteryDailyPetCfgWeightList = append(lotteryDailyPetCfgWeightList, tmp)
		case 22:
			lotteryDailyPetDoubleSCfgWeightList = append(lotteryDailyPetDoubleSCfgWeightList, tmp)
		case 23:
			lotteryDailyPetACfgWeightList = append(lotteryDailyPetACfgWeightList, tmp)
		case 24:
			lotteryDailyPetDoubleASCfgWeightList = append(lotteryDailyPetDoubleASCfgWeightList, tmp)
		case 25:
			lotteryBoxCfgWeightList = append(lotteryBoxCfgWeightList, tmp)
		case 26:
			lotteryTenPetSCfgWeightList = append(lotteryTenPetSCfgWeightList, tmp)
		}
	}

	var totalWeight int64 = 0
	for _, cfg := range lotteryPetCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryPetCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建S权重
	totalWeight = 0
	for _, cfg := range lotteryPetDoubleSCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryPetDoubleSCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建AS权重
	totalWeight = 0
	for _, cfg := range lotteryDailyPetCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryDailyPetCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建AS权重
	totalWeight = 0
	for _, cfg := range lotteryDailyPetDoubleSCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryDailyPetDoubleSCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建AS权重
	totalWeight = 0
	for _, cfg := range lotteryDailyPetACfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryDailyPetACfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建AS权重
	totalWeight = 0
	for _, cfg := range lotteryDailyPetDoubleASCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryDailyPetDoubleASCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	// 重新构建开宝箱权重
	totalWeight = 0
	for _, cfg := range lotteryBoxCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryBoxCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

	totalWeight = 0
	for _, cfg := range lotteryTenPetSCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotteryTenPetSCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}

}

func CheckLotteryPetCfg() bool {
	logs.Infof("======================= CheckLotteryPetCfg =======================")
	return true
}

func GetLotteryPetCfgWeightList() []util.WeightItr {
	return lotteryPetCfgWeightList
}

func GetLotteryPetDoubleSCfgWeightList() []util.WeightItr {
	return lotteryPetDoubleSCfgWeightList
}

func GetLotteryDailyPetCfgWeightList() []util.WeightItr {
	return lotteryDailyPetCfgWeightList
}

func GetLotteryDailyPetACfgWeightList() []util.WeightItr {
	return lotteryDailyPetACfgWeightList
}

func GetLotteryDailyPetDoubleSCfgWeightList() []util.WeightItr {
	return lotteryDailyPetDoubleSCfgWeightList
}

func GetLotteryDailyPetDoubleASCfgWeightList() []util.WeightItr {
	return lotteryDailyPetDoubleASCfgWeightList
}

func GetLotterylotteryBoxCfgWeightList() []util.WeightItr {
	return lotteryBoxCfgWeightList
}

func GetLotteryTenPetSCfgWeightList() []util.WeightItr {
	return lotteryTenPetSCfgWeightList
}

// ******************************************* lotteryPetScore *******************************************
var lotteryPetScoreCfgMap map[int32]*data.LotteryPetScoreCfg

func LoadLotteryPetScoreCfg() {
	logs.Infof("======================= LoadLotteryPetScoreCfg =======================")
	var datas []*data.LotteryPetScoreCfg
	err := excel.XlsConfigLoad("XL_LOTTERYPETSCORE", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.LotteryPetScoreCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Reward)
			assert.Assert(err2 == nil, err2)
		}

	})

	assert.Assert(err == nil, err)

	lotteryPetScoreCfgMap = make(map[int32]*data.LotteryPetScoreCfg)
	for _, cfg := range datas {
		lotteryPetScoreCfgMap[cfg.CfgId] = cfg
	}

}

func CheckLotteryPetScoreCfg() bool {
	logs.Infof("======================= CheckLotteryPetScoreCfg =======================")
	return true
}

func GetLotteryPetScoreCfg(cfgId int32) *data.LotteryPetScoreCfg {
	cfg, find := lotteryPetScoreCfgMap[cfgId]
	assert.Assert(find, "LotteryPetScoreCfg not found, cfgId:%d", cfgId)
	return cfg
}

func GetLotteryPetScoreCfgMap() map[int32]*data.LotteryPetScoreCfg {
	return lotteryPetScoreCfgMap
}
