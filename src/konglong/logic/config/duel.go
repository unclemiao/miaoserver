package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var duelCfgList []*data.DuelCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadDuelCfg,
		CheckFunc: CheckDuelCfg,
	})
}

func LoadDuelCfg() {
	logs.Infof("======================= LoadDuelCfg =======================")
	var datas []*data.DuelCfg
	err := excel.XlsConfigLoad("XL_DUEL", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "RankList" && state.Content != "" {
			cfg := state.Element.(*data.DuelCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.RankList)
			assert.Assert(err2 == nil, err2)

		} else if state.Field == "RankAward" && state.Content != "" {
			cfg := state.Element.(*data.DuelCfg)
			cfg.RankAward = LoadAwardCfgStr(state.Content)

		} else if state.Field == "WinAward" && state.Content != "" {
			cfg := state.Element.(*data.DuelCfg)
			cfg.WinAward = LoadAwardCfgStr(state.Content)

		} else if state.Field == "LoseAward" && state.Content != "" {
			cfg := state.Element.(*data.DuelCfg)
			cfg.LoseAward = LoadAwardCfgStr(state.Content)
		}

	})

	assert.Assert(err == nil, err)
	duelCfgList = datas
}

func CheckDuelCfg() bool {
	logs.Infof("======================= CheckDuelCfg =======================")
	return true
}

func DuelCfgListBy() []*data.DuelCfg {
	return duelCfgList
}

func DuelCfgBy(rank int32) *data.DuelCfg {
	for _, cfg := range duelCfgList {
		if rank >= cfg.RankList[0] && rank <= cfg.RankList[1] {
			return cfg
		}
	}
	return duelCfgList[len(duelCfgList)-1]
}
