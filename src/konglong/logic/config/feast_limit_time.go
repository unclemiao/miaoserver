package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var limitTimeFeastCfgMap map[int32]*data.LimitTimeFeastCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadLimitTimeFeastCfg,
		CheckFunc: CheckLimitTimeFeastCfg,
	})
}

func LoadLimitTimeFeastCfg() {
	logs.Infof("======================= LoadLimitTimeFeastCfg =======================")
	var dataList []*data.LimitTimeFeastCfg

	err := excel.XlsConfigLoad("XL_LIMITTIMEFEAST", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ChoiceReward" && state.Content != "" {
			cfg := state.Element.(*data.LimitTimeFeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.ChoiceReward)
			assert.Assert(err2 == nil, err2)
		}

	})
	assert.Assert(err == nil, err)

	limitTimeFeastCfgMap = make(map[int32]*data.LimitTimeFeastCfg)
	for _, cfg := range dataList {
		limitTimeFeastCfgMap[cfg.CfgId] = cfg
	}
}

func CheckLimitTimeFeastCfg() bool {
	logs.Infof("======================= CheckLimitTimeFeastCfg =======================")
	return true
}

func GetLimitTimeFeastCfg(cfgId int32) *data.LimitTimeFeastCfg {
	return limitTimeFeastCfgMap[cfgId]
}
