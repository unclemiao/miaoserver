package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var mallCfgMap map[int32]*data.MallCfg
var mallCfgPbList []*protocol.MallCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadMallCfg,
		CheckFunc: CheckMallCfg,
	})
}
func LoadMallCfg() {
	logs.Infof("======================= LoadMallCfg =======================")
	var dataList []*data.MallCfg

	err := excel.XlsConfigLoad("XL_MALL", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "SellItem" && state.Content != "" {
			cfg := state.Element.(*data.MallCfg)
			cfg.SellItem = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	mallCfgMap = make(map[int32]*data.MallCfg)
	mallCfgPbList = make([]*protocol.MallCfg, 0, len(mallCfgMap))
	for _, cfg := range dataList {
		mallCfgMap[cfg.CfgId] = cfg
		mallCfgPbList = append(mallCfgPbList, &protocol.MallCfg{
			CfgId:        cfg.CfgId,
			SellKind:     cfg.SellKind,
			SellItemList: cfg.SellItem.GetForClient(),
			Diamonds:     int32(cfg.Diamonds),
			Gem:          int32(cfg.Gem),
			Rmb:          int32(cfg.Rmb),
			Order:        cfg.Order,
			Desc:         cfg.Desc,
			MoreGem:      cfg.MoreGem,
			Icon:         cfg.Icon,
			Limit:        cfg.Limit,
		})
		if cfg.SellItem != nil && cfg.MoreGem > 0 {
			find := false
			for _, awardCfg := range cfg.SellItem.AwardList {
				if awardCfg.CfgId == int64(protocol.CurrencyType_Gem) {
					awardCfg.N += int64(cfg.MoreGem)
					find = true
				}
			}
			if !find {
				cfg.SellItem.AwardList = append(cfg.SellItem.AwardList, &data.ItemAmount{
					CfgId: int64(protocol.CurrencyType_Gem),
					N:     int64(cfg.MoreGem),
					Rate:  1,
				})
			}
		}

	}
}

func CheckMallCfg() bool {
	logs.Infof("======================= CheckMallCfg =======================")
	return true
}

func GetMallCfg(cfgId int32) *data.MallCfg {
	return mallCfgMap[cfgId]
}

func GetAllMallCfgPbList() []*protocol.MallCfg {
	return mallCfgPbList
}
