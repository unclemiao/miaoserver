package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var feastRepeatPayCfgMap map[int32]*data.FeastRepeatPayCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadFeastRepeatPayCfg,
		CheckFunc: CheckFeastRepeatPayCfg,
	})
}

func LoadFeastRepeatPayCfg() {
	logs.Infof("======================= LoadFeastRepeatPayCfg =======================")
	var dataList []*data.FeastRepeatPayCfg

	err := excel.XlsConfigLoad("XL_FEASTREPEATPAY", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ChoiceReward" && state.Content != "" {
			cfg := state.Element.(*data.FeastRepeatPayCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.ChoiceReward)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.FeastRepeatPayCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	feastRepeatPayCfgMap = make(map[int32]*data.FeastRepeatPayCfg)
	for _, cfg := range dataList {
		feastRepeatPayCfgMap[cfg.CfgId] = cfg
	}
}

func CheckFeastRepeatPayCfg() bool {
	logs.Infof("======================= CheckFeastRepeatPayCfg =======================")
	return true
}

func GetFeastRepeatPayCfg(cfgId int32) *data.FeastRepeatPayCfg {
	return feastRepeatPayCfgMap[cfgId]
}
