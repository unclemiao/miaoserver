package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetLoyalCfg,
		CheckFunc: CheckPetLoyalCfg,
	})
}

var loyalCfgMap map[int32][]util.WeightItr

func LoadPetLoyalCfg() {
	logs.Infof("======================= LoadPetLoyalCfg =======================")
	var dataList []*data.PetLoyalCfg
	err := excel.XlsConfigLoad("XL_PETLOYAL", &dataList, nil)
	assert.Assert(err == nil, err)

	loyalCfgMap = make(map[int32][]util.WeightItr)

	for _, cfg := range dataList {
		loyalCfgMap[cfg.CfgId] = append(loyalCfgMap[cfg.CfgId], cfg)
	}

	for _, dropCfgList := range loyalCfgMap {
		var totalWeight int64
		for _, cfg := range dropCfgList {
			totalWeight += cfg.GetWeight()
			cfg.SetWeight(totalWeight)
		}
		for _, cfg := range dropCfgList {
			cfg.SetTotalWeight(totalWeight)
		}
	}

}

func CheckPetLoyalCfg() bool {
	logs.Infof("======================= CheckItemDropCfg =======================")
	return true
}

func PetLoyalRandom(groupId int32) *data.PetLoyalCfg {
	groupCfgList := loyalCfgMap[groupId]
	index := util.RandomWeight(groupCfgList)
	return groupCfgList[index].(*data.PetLoyalCfg)
}
