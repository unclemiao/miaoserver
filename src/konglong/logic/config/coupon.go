package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadCouponCfg,
		CheckFunc: CheckCouponCfg,
	})
}

var couponCfgMap map[int32]*data.CouponCfg
var couponCfgMapByCode map[string]*data.CouponCfg

func LoadCouponCfg() {
	logs.Infof("======================= LoadCouponCfg =======================")
	var dataList []*data.CouponCfg
	err := excel.XlsConfigLoad("XL_COUPON", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.CouponCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	couponCfgMap = make(map[int32]*data.CouponCfg)
	couponCfgMapByCode = make(map[string]*data.CouponCfg)

	for _, cfg := range dataList {
		couponCfgMap[cfg.CfgId] = cfg
		couponCfgMapByCode[cfg.CouponCode] = cfg
	}

}

func CheckCouponCfg() bool {
	logs.Infof("======================= CheckCouponCfg =======================")
	return true
}

func GetCouponCfg(cfgId int32) *data.CouponCfg {
	return couponCfgMap[cfgId]
}

func GetCouponCfgByCode(code string) *data.CouponCfg {
	return couponCfgMapByCode[code]
}
