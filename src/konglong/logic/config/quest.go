package config

import (
	"math/rand"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var dailyQuestCfg map[int32]*data.DailyQuestCfg
var rateCfgList []*data.DailyQuestCfg

var questAwardCfg []*data.QuestAwardCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadDailyQuestCfg,
		CheckFunc: CheckDailyQuestCfg,
	})

}

func LoadDailyQuestCfg() {
	logs.Infof("======================= LoadDailyQuestCfg =======================")
	var dataList []*data.DailyQuestCfg
	err := excel.XlsConfigLoad("XL_DAILYQUEST", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.DailyQuestCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)
	dailyQuestCfg = make(map[int32]*data.DailyQuestCfg)
	rateCfgList = rateCfgList[:0]

	for _, cfg := range dataList {
		dailyQuestCfg[cfg.CfgId] = cfg
		if cfg.IsRate == 1 {
			rateCfgList = append(rateCfgList, cfg)
		}
	}

	var awardList []*data.QuestAwardCfg
	err = excel.XlsConfigLoad("XL_DAILYQUESTAWARD", &awardList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.QuestAwardCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)
	questAwardCfg = make([]*data.QuestAwardCfg, len(awardList)+1)
	for _, cfg := range awardList {
		questAwardCfg[cfg.Index] = cfg
	}
}

func CheckDailyQuestCfg() bool {
	logs.Infof("======================= CheckDailyQuestCfg =======================")
	return true
}

func DailyQuestCfgBy(cfgId int32) *data.DailyQuestCfg {
	cfg, find := dailyQuestCfg[cfgId]
	assert.Assert(find, "daily quest cfg not found, cfgId:%d", cfgId)
	return cfg
}

func DailyQuestAwardCfgBy(index int32) *data.QuestAwardCfg {
	cfg := questAwardCfg[index]
	return cfg
}

func DailyQuestCfgMapBy() map[int32]*data.DailyQuestCfg {
	return dailyQuestCfg
}

func RandRateDailyQuestMap() map[int32]*data.DailyQuestCfg {
	results := make(map[int32]*data.DailyQuestCfg)
	ids := rand.Perm(len(rateCfgList))
	num := util.MinInt32(int32(len(ids)), util.Random32(3, 5))
	for i := int32(0); i < num; i++ {
		cfg := rateCfgList[ids[i]]
		results[cfg.CfgId] = cfg
	}
	return results
}
