package config

import (
	"encoding/json"
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var lotteryItemCfgList []*data.LotteryItemCfg
var lotteryItemCfgWeightList []util.WeightItr

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadLotteryItemCfg,
		CheckFunc: CheckLotteryItemCfg,
	})
}

func LoadLotteryItemCfg() {
	logs.Infof("======================= LoadLotteryItemCfg =======================")
	var datas []*data.LotteryItemCfg
	err := excel.XlsConfigLoad("XL_LOTTERYITEM", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ItemCfgId" && state.Content != "" {
			cfg := state.Element.(*data.LotteryItemCfg)
			var ids []int64
			err2 := json.Unmarshal([]byte(state.Content), &ids)
			assert.Assert(err2 == nil, err2)
			cfg.ItemCfgId = ids
		}
	})

	assert.Assert(err == nil, err)

	lotteryItemCfgList = make([]*data.LotteryItemCfg, 0, len(datas))
	lotteryItemCfgWeightList = make([]util.WeightItr, 0, len(datas))
	var totalWeight int64
	for _, cfg := range datas {
		totalWeight += cfg.Weight
		cfg.Weight = totalWeight
		lotteryItemCfgList = append(lotteryItemCfgList, cfg)
	}
	for _, cfg := range datas {
		cfg.TotalWeight = totalWeight
	}
	sort.Slice(lotteryItemCfgList, func(i, j int) bool {
		return lotteryItemCfgList[i].Weight < lotteryItemCfgList[j].Weight
	})
	for index, cfg := range lotteryItemCfgList {
		lotteryItemCfgWeightList = append(lotteryItemCfgWeightList, cfg)
		cfg.ArraryIndex = int32(index)

	}

}

func CheckLotteryItemCfg() bool {
	logs.Infof("======================= CheckLotteryItemCfg =======================")
	return true
}

func LotteryItemCfgBy(index int32) *data.LotteryItemCfg {
	assert.Assert(index > 0 && index < int32(len(lotteryItemCfgList)), "lottery cfg not found, index:%d", index)
	return lotteryItemCfgList[index]
}

func GetLotteryItemCfgList() ([]*data.LotteryItemCfg, int64) {
	return lotteryItemCfgList, lotteryItemCfgList[0].TotalWeight
}

func GetLotteryItemCfgWeightList() []util.WeightItr {
	return lotteryItemCfgWeightList
}
