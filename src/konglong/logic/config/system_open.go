package config

import (
	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var systemOpenCfgList []*data.SystemAwardCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSystemOpenCfg,
		CheckFunc: CheckSystemOpenCfg,
	})
}

func LoadSystemOpenCfg() {
	logs.Infof("======================= LoadSystemOpenCfg =======================")
	var dataList []*data.SystemAwardCfg
	err := util.XlsConfigLoad("XL_SYSTEMOPEN", &dataList, func(state *util.XlsConfigReaderState) {
		if state.Field == "Award" {
			cfg := state.Element.(*data.SystemAwardCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	systemOpenCfgList = dataList

}

func CheckSystemOpenCfg() bool {
	logs.Infof("======================= CheckSystemOpenCfg =======================")
	return true
}

func SystemOpenCfgBy(index int32) *data.SystemAwardCfg {
	assert.Assert(index >= 0 && index < int32(len(systemOpenCfgList)), "system open cfg not found, index:%d", index)
	return systemOpenCfgList[index]
}
