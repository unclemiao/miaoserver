package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var fastRankFeastCfgList []*data.FeastFastRankCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadFeastFastRankCfg,
		CheckFunc: CheckFeastFastRankCfg,
	})
}

func LoadFeastFastRankCfg() {
	logs.Infof("======================= LoadFeastFastRankCfg =======================")
	var dataList []*data.FeastFastRankCfg

	err := excel.XlsConfigLoad("XL_FASTRANKFEAST", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "RankRange" && state.Content != "" {
			cfg := state.Element.(*data.FeastFastRankCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.RankRange)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.FeastFastRankCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	fastRankFeastCfgList = dataList

}

func CheckFeastFastRankCfg() bool {
	logs.Infof("======================= CheckFeastFastRankCfg =======================")
	return true
}

func GetFeastFastRankCfg(rank int32, kind protocol.FeastKind) *data.FeastFastRankCfg {
	for _, cfg := range fastRankFeastCfgList {
		if cfg.Kind == kind && rank >= cfg.RankRange[0] && rank <= cfg.RankRange[1] {
			return cfg
		}
	}
	return nil
}
