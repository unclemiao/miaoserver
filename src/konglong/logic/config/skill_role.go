package config

import (
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var skillRoleCfgMap map[int32][]*data.SkillRoleCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSkillRoleCfg,
		CheckFunc: CheckSkillRoleCfg,
	})
}

func LoadSkillRoleCfg() {
	logs.Infof("======================= LoadSkillRoleCfg =======================")
	var dataList []*data.SkillRoleCfg

	err := excel.XlsConfigLoad("XL_SKILLROLE", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Stuff" && state.Content != "" {
			cfg := state.Element.(*data.SkillRoleCfg)
			cfg.Stuff = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)
	skillRoleCfgMap = make(map[int32][]*data.SkillRoleCfg)
	for _, cfg := range dataList {
		skillRoleCfgMap[cfg.CfgId] = append(skillRoleCfgMap[cfg.CfgId], cfg)
		for _, amount := range cfg.Stuff.AwardList {
			if amount.CfgId == int64(protocol.CurrencyType_TalentPoint) {
				cfg.Point = int32(amount.N)
			}
		}
	}
	for _, list := range skillRoleCfgMap {
		sort.Slice(list, func(i, j int) bool {
			return list[i].Level < list[j].Level
		})
	}
}

func CheckSkillRoleCfg() bool {
	logs.Info("======================= CheckSevenSignCfg =======================")
	return true
}

func GetSkillRoleCfg(cfgId int32, level int32) *data.SkillRoleCfg {
	if level <= 0 || level > int32(len(skillRoleCfgMap[cfgId])) {
		assert.Assert(false, "cfg not found, level:%d", level)
	}
	return skillRoleCfgMap[cfgId][level-1]
}
