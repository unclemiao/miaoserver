package config

import (
	"encoding/json"
	"sort"

	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var robotMap map[int32]*data.RobotCfg
var robotList []*data.RobotCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadRobotCfg,
		CheckFunc: CheckRobotCfg,
	})
}

func LoadRobotCfg() {
	logs.Infof("======================= LoadRobotCfg =======================")
	var dataList []*data.RobotCfg
	err := util.XlsConfigLoad("XL_ROBOT", &dataList, func(state *util.XlsConfigReaderState) {
		if state.Field == "FighterList" {
			cfg := state.Element.(*data.RobotCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.FighterList)
			assert.Assert(err2 == nil, err2)
		}
	})
	assert.Assert(err == nil, err)

	robotMap = make(map[int32]*data.RobotCfg)
	robotList = make([]*data.RobotCfg, 0, len(robotMap))
	for _, cfg := range dataList {
		robotMap[cfg.CfgId] = cfg
		robotList = append(robotList, cfg)
	}
	sort.Slice(robotList, func(i, j int) bool {
		return robotList[i].CfgId < robotList[j].CfgId
	})
}

func CheckRobotCfg() bool {
	logs.Infof("======================= CheckRobotCfg =======================")
	return true
}

func GetRobotCfg(id int32) *data.RobotCfg {
	cfg, find := robotMap[id]
	assert.Assert(find, "robot cfg[%d] not found", id)
	return cfg
}

func GetRobotCfgList() []*data.RobotCfg {
	return robotList
}
