package config

import (
	"encoding/json"
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     100, // after mon
		LoadFunc:  LoadAdventureCfg,
		CheckFunc: CheckAdventureCfg,
	})
}

var adventureChapterCfgMap []*data.AdventureChapterCfg

func LoadAdventureCfg() {
	logs.Infof("======================= LoadAdventureCfg =======================")
	var chapterList []*data.AdventureChapterCfg
	err := excel.XlsConfigLoad("XL_ADVENTURECHAPTER", &chapterList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.AdventureChapterCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	var passList []*data.AdventurePassCfg
	err = excel.XlsConfigLoad("XL_ADVENTUREPASS", &passList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "BossList" && state.Content != "" {
			cfg := state.Element.(*data.AdventurePassCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BossList)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.AdventurePassCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	adventureChapterCfgMap = make([]*data.AdventureChapterCfg, len(chapterList)+1)

	for _, cfg := range chapterList {
		adventureChapterCfgMap[cfg.Chapter] = cfg
	}

	for _, cfg := range passList {
		chapterCfg := adventureChapterCfgMap[cfg.Chapter]
		chapterCfg.PassCfgList = append(chapterCfg.PassCfgList, cfg)
		chapterCfg.PassAmount++
	}

	var max int32
	for _, cfg := range adventureChapterCfgMap {
		if cfg == nil {
			continue
		}
		sort.Slice(cfg.PassCfgList, func(i, j int) bool {
			return cfg.PassCfgList[i].Pass < cfg.PassCfgList[j].Pass
		})
		max = util.MaxInt32(max, cfg.Chapter)
	}

	for _, cfg := range adventureChapterCfgMap {
		if cfg == nil {
			continue
		}
		cfg.IsLast = cfg.Chapter == max
	}
}

func CheckAdventureCfg() bool {
	logs.Infof("======================= CheckAdventureCfg =======================")
	return true
}

func GetAdventurePassCfg(chapter int32, pass int32) *data.AdventurePassCfg {
	return adventureChapterCfgMap[chapter].PassCfgList[pass-1]
}

func GetAdventureChapterCfg(chapter int32) *data.AdventureChapterCfg {
	return adventureChapterCfgMap[chapter]
}
