package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

// ************************************************** 食物 ************************************************
var foodLvMap [][]*data.ItemCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     101, // after item, fourmal
		LoadFunc:  LoadFoodCfg,
		CheckFunc: CheckFoodCfg,
	})

}

func LoadFoodCfg() {
	logs.Infof("======================= LoadFoodCfg =======================")
	var dataList []*data.ItemCfg

	err := excel.XlsConfigLoad("XL_FOODITEM", &dataList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	var maxLv int32
	for _, cfg := range dataList {
		assert.Assert(itemCfgMap[cfg.CfgId] == nil, "!食物id重复,id:%d", cfg.CfgId)
		itemCfgMap[cfg.CfgId] = cfg
		maxLv = util.MaxInt32(maxLv, cfg.Level)
	}
	foodLvMap = make([][]*data.ItemCfg, maxLv+1)
	for _, cfg := range dataList {
		if isFoodStuff(cfg.CfgId) {
			continue
		}
		foodLvMap[cfg.Level] = append(foodLvMap[cfg.Level], cfg)
	}

}

func CheckFoodCfg() bool {
	logs.Infof("======================= CheckFoodCfg =======================")
	return true
}

func GetFoodLvListCfg(lv int32) []*data.ItemCfg {
	return foodLvMap[lv]
}
