package config

import (
	"encoding/json"

	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var paralCfgMap map[int32]*data.ParalCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadParalCfg,
		CheckFunc: CheckParalCfg,
	})
}

func LoadParalCfg() {
	logs.Infof("======================= LoadParalCfg =======================")
	var dataList []*data.ParalCfg
	err := util.XlsConfigLoad("XL_PARAL", &dataList, func(state *util.XlsConfigReaderState) {
		if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.ParalCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		} else if state.Field == "MonsterList" && state.Content != "" {
			cfg := state.Element.(*data.ParalCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.MonsterList)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "NeedPetLevel" && state.Content != "" {
			cfg := state.Element.(*data.ParalCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.NeedPetLevel)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "RandAward" && state.Content != "" {
			cfg := state.Element.(*data.ParalCfg)
			cfg.RandAward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	paralCfgMap = make(map[int32]*data.ParalCfg)
	for _, cfg := range dataList {
		paralCfgMap[cfg.CfgId] = cfg
	}
}

func CheckParalCfg() bool {
	logs.Infof("======================= CheckParalCfg =======================")
	return true
}

func GetParalCfg(id int32) *data.ParalCfg {
	cfg, find := paralCfgMap[id]
	assert.Assert(find, "paral[%d] not found", id)
	return cfg
}
