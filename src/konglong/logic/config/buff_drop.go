package config

import (
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadBuffDropCfg,
		CheckFunc: CheckBuffDropCfg,
	})
}

var buffDropGroupCfgMap map[int32]*data.BuffDropGroupCfg

func LoadBuffDropCfg() {
	logs.Infof("======================= LoadBuffDropCfg =======================")
	var dataList []*data.BuffDropCfg
	err := excel.XlsConfigLoad("XL_BUFFDROP", &dataList, nil)
	assert.Assert(err == nil, err)

	buffDropGroupCfgMap = make(map[int32]*data.BuffDropGroupCfg)

	for _, cfg := range dataList {
		if buffDropGroupCfgMap[cfg.DropId] == nil {
			buffDropGroupCfgMap[cfg.DropId] = &data.BuffDropGroupCfg{
				DropId:         cfg.DropId,
				DropCfgMap:     make(map[int32]*data.BuffDropCfg),
				DropWeightList: nil,
			}
		}
		groupCfg := buffDropGroupCfgMap[cfg.DropId]
		groupCfg.DropCfgMap[cfg.GetCfgId()] = cfg
	}

	for _, group := range buffDropGroupCfgMap {
		var totalWeight int64
		var dropCfgList []*data.BuffDropCfg
		for _, dropCfg := range group.DropCfgMap {
			dropCfg.Weight += totalWeight
			totalWeight = dropCfg.Weight
			dropCfgList = append(dropCfgList, dropCfg)
		}
		sort.Slice(dropCfgList, func(i, j int) bool {
			return dropCfgList[i].Weight < dropCfgList[j].Weight
		})
		for index, cfg := range dropCfgList {
			cfg.TotalWeight = totalWeight
			cfg.ArraryIndex = int32(index)
			group.DropWeightList = append(group.DropWeightList, cfg)
		}
	}

}

func CheckBuffDropCfg() bool {
	logs.Infof("======================= CheckBuffDropCfg =======================")
	return true
}

func GetBuffListCfg(groupId int32) []util.WeightItr {
	groupCfg := buffDropGroupCfgMap[groupId]
	return groupCfg.DropWeightList
}
