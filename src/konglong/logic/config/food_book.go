package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var foodBookCfgList []*data.FoodBookCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     200, // after item, after drop
		LoadFunc:  LoadFoodBookCfg,
		CheckFunc: CheckFoodBookCfg,
	})
}
func LoadFoodBookCfg() {
	logs.Infof("======================= LoadFoodBookCfg =======================")
	var dataList []*data.FoodBookCfg

	err := excel.XlsConfigLoad("XL_FOODBOOK", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Stuff" && state.Content != "" {
			cfg := state.Element.(*data.FoodBookCfg)
			cfg.Stuff = LoadAwardCfgStr(state.Content)
		} else if state.Field == "LevelUpStuff" && state.Content != "" {
			cfg := state.Element.(*data.FoodBookCfg)
			cfg.LevelUpStuff = LoadAwardCfgStr(state.Content)
		} else if state.Field == "HighBuyStuff" && state.Content != "" {
			cfg := state.Element.(*data.FoodBookCfg)
			cfg.HighBuyStuff = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	foodBookCfgList = make([]*data.FoodBookCfg, len(dataList)+1)
	for _, cfg := range dataList {
		foodBookCfgList[cfg.Level] = cfg
		dropCfg := itemDropGroupCfgMap[cfg.DropId]
		var total, num uint64
		for _, itDropCfg := range dropCfg.DropCfgMap {
			total += GetItemCfg(itDropCfg.ItemId).Exp
			num++
		}
		cfg.AvgExp = total / num
	}
}

func CheckFoodBookCfg() bool {
	return true
}

func FoodBookCfgBy(level int32) *data.FoodBookCfg {
	return foodBookCfgList[level]
}
