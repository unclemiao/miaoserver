package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var rankAwardCfgList []*data.RankAwardCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadRankAwardCfg,
		CheckFunc: CheckRankAwardCfg,
	})
}

func LoadRankAwardCfg() {
	logs.Infof("======================= LoadRankAwardCfg =======================")
	var dataList []*data.RankAwardCfg
	err := excel.XlsConfigLoad("XL_RANKAWARD", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Rank" && state.Content != "" {
			cfg := state.Element.(*data.RankAwardCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Rank)
			assert.Assert(err2 == nil, err2)
		}

	})
	assert.Assert(err == nil, err)
	rankAwardCfgList = dataList

}

func CheckRankAwardCfg() bool {
	logs.Infof("======================= CheckRankAwardCfg =======================")
	return true
}

func GetRankAwardCfgList() []*data.RankAwardCfg {
	return rankAwardCfgList
}

func RankAwardCfgBy(rank int32) *data.RankAwardCfg {
	for _, cfg := range rankAwardCfgList {
		if rank >= cfg.Rank[0] && rank <= cfg.Rank[1] {
			return cfg
		}
	}
	assert.Assert(false, "rank award cfg not found, rank%d", rank)
	return nil
}
