package config

import (
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadItemDropCfg,
		CheckFunc: CheckItemDropCfg,
	})
}

var itemDropGroupCfgMap map[int32]*data.ItemDropGroupCfg

func LoadItemDropCfg() {
	logs.Infof("======================= LoadItemDropCfg =======================")
	var dataList []*data.ItemDropCfg
	err := excel.XlsConfigLoad("XL_ITEMDROP", &dataList, nil)
	assert.Assert(err == nil, err)

	itemDropGroupCfgMap = make(map[int32]*data.ItemDropGroupCfg)

	for _, cfg := range dataList {
		if itemDropGroupCfgMap[cfg.DropId] == nil {
			itemDropGroupCfgMap[cfg.DropId] = &data.ItemDropGroupCfg{
				DropId:         cfg.DropId,
				DropCfgMap:     make(map[int32]*data.ItemDropCfg),
				DropWeightList: nil,
			}
		}
		groupCfg := itemDropGroupCfgMap[cfg.DropId]
		groupCfg.DropCfgMap[cfg.GetCfgId()] = cfg
	}

	for _, group := range itemDropGroupCfgMap {
		var totalWeight int64
		var dropCfgList []*data.ItemDropCfg
		for _, dropCfg := range group.DropCfgMap {
			dropCfg.Weight += totalWeight
			totalWeight = dropCfg.Weight
			dropCfgList = append(dropCfgList, dropCfg)
		}
		sort.Slice(dropCfgList, func(i, j int) bool {
			return dropCfgList[i].Weight < dropCfgList[j].Weight
		})
		for _, cfg := range dropCfgList {
			cfg.TotalWeight = totalWeight
			group.DropWeightList = append(group.DropWeightList, cfg)
		}
	}

}

func CheckItemDropCfg() bool {
	logs.Infof("======================= CheckItemDropCfg =======================")
	return true
}

func ItemDropRandom(groupId int32) *data.ItemDropCfg {
	groupCfg := itemDropGroupCfgMap[groupId]
	index := util.RandomWeight(groupCfg.DropWeightList)
	return groupCfg.DropWeightList[index].(*data.ItemDropCfg)
}

func GetItemDropGroupCfg(groupId int32) *data.ItemDropGroupCfg {
	groupCfg := itemDropGroupCfgMap[groupId]
	return groupCfg
}
