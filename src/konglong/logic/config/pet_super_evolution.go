package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetSuperEvolutionCfg,
		CheckFunc: CheckPetSuperEvolutionCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSuperEvolutionBuyCfg,
		CheckFunc: CheckSuperEvolutionBuyCfg,
	})
}

var superEvolutionCfgMap map[int32]*data.PetSuperEvolutionCfg
var superEvolutionUnlockAttrCfgMap map[int32]map[int32]*data.PetSuperEvolutionUnlockAttrCfg

func LoadPetSuperEvolutionCfg() {
	logs.Infof("======================= LoadPetSuperEvolutionCfg =======================")
	var dataList []*data.PetSuperEvolutionCfg
	err := excel.XlsConfigLoad("XL_PETSUPEREVOLUTION", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "VariationStuff" && state.Content != "" {
			cfg := state.Element.(*data.PetSuperEvolutionCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.VariationStuff)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "AttrStuff" && state.Content != "" {
			cfg := state.Element.(*data.PetSuperEvolutionCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.AttrStuff)
			assert.Assert(err2 == nil, err2)
		}

	})
	assert.Assert(err == nil, err)

	superEvolutionCfgMap = make(map[int32]*data.PetSuperEvolutionCfg)

	for _, cfg := range dataList {
		superEvolutionCfgMap[cfg.CfgId] = cfg
	}

	logs.Infof("======================= LoadPetSuperEvolutionUnlockCfg =======================")
	var attrList []*data.PetSuperEvolutionUnlockAttrCfg
	err = excel.XlsConfigLoad("XL_PETSUPEREVOLUTIONUNLOCKATTR", &attrList, nil)
	assert.Assert(err == nil, err)

	superEvolutionUnlockAttrCfgMap = make(map[int32]map[int32]*data.PetSuperEvolutionUnlockAttrCfg)

	for _, cfg := range attrList {
		if superEvolutionUnlockAttrCfgMap[cfg.CfgId] == nil {
			superEvolutionUnlockAttrCfgMap[cfg.CfgId] = make(map[int32]*data.PetSuperEvolutionUnlockAttrCfg)
		}
		superEvolutionUnlockAttrCfgMap[cfg.CfgId][cfg.Progress] = cfg
	}

}

func CheckPetSuperEvolutionCfg() bool {
	logs.Infof("======================= CheckPetSuperEvolutionCfg =======================")
	return true
}

func GetPetSuperEvolutionCfg(cfgId int32) *data.PetSuperEvolutionCfg {
	return superEvolutionCfgMap[cfgId]
}

func GetAllPetSuperEvolutionCfg() map[int32]*data.PetSuperEvolutionCfg {
	return superEvolutionCfgMap
}

func GetPetSuperEvolutionUnlockFinishNum() int32 {
	return int32(len(superEvolutionCfgMap))
}

func GetPetSuperEvolutionUnlockAttrCfg(cfgId int32, progress int32) *data.PetSuperEvolutionUnlockAttrCfg {
	return superEvolutionUnlockAttrCfgMap[cfgId][progress]
}

// ****************************************** 进化石购买概率配置 *************************************************
var superEvolutionBuyCfgMap []*data.PetSuperEvolutionBuyCfg

func LoadSuperEvolutionBuyCfg() {
	logs.Infof("======================= LoadPetSuperEvolutionCfg =======================")
	var dataList []*data.PetSuperEvolutionBuyCfg
	err := excel.XlsConfigLoad("XL_PETSUPEREVOLUTIONBUY", &dataList, nil)
	assert.Assert(err == nil, err)

	superEvolutionBuyCfgMap = make([]*data.PetSuperEvolutionBuyCfg, len(dataList)+1)
	for _, cfg := range dataList {
		superEvolutionBuyCfgMap[cfg.CfgId] = cfg
	}

}

func CheckSuperEvolutionBuyCfg() bool {
	logs.Infof("======================= CheckSuperEvolutionBuyCfg =======================")
	return true
}

func GetSuperEvolutionBuyCfg(quota int32) *data.PetSuperEvolutionBuyCfg {
	if quota >= int32(len(superEvolutionBuyCfgMap)) {
		quota = int32(len(superEvolutionBuyCfgMap) - 1)
	}
	assert.Assert(quota > 0 && quota < int32(len(superEvolutionBuyCfgMap)), "quota err")
	return superEvolutionBuyCfgMap[quota]
}
