package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var fettersCfgMap map[int32]*data.FettersCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadFettersCfg,
		CheckFunc: CheckFettersCfg,
	})
}

func LoadFettersCfg() {
	logs.Infof("======================= LoadFettersCfg =======================")
	var datas []*data.FettersCfg
	err := excel.XlsConfigLoad("XL_FETTERS", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.FettersCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}

	})

	assert.Assert(err == nil, err)

	fettersCfgMap = make(map[int32]*data.FettersCfg)
	for _, info := range datas {
		fettersCfgMap[info.CfgId] = info
		for _, info2 := range datas {
			if info2.MainPetId == info.MainPetId &&
				info2.MainPetVariation == info.MainPetVariation &&
				info2.SubPetId == info.SubPetId &&
				info2.SubPetVariation == info.SubPetVariation &&
				info2.Level == info.Level+1 {

				info.NextCfgId = info2.CfgId
				info2.PreCfgId = info.CfgId
				break
			}
		}
	}

}

func CheckFettersCfg() bool {
	logs.Infof("======================= CheckFettersCfg =======================")
	return true
}

func GetFettersCfg(cfgId int32) *data.FettersCfg {
	return fettersCfgMap[cfgId]
}
