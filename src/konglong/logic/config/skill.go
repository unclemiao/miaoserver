package config

import (
	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var skillCfgMap map[int32]*data.SkillCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSkillCfg,
		CheckFunc: CheckSkillCfg,
	})

}

func LoadSkillCfg() {
	logs.Infof("======================= LoadSkillCfg =======================")
	var dataList []*data.SkillCfg
	err := util.XlsConfigLoad("XL_SKILL", &dataList, nil)
	assert.Assert(err == nil, err)

	skillCfgMap = make(map[int32]*data.SkillCfg)
	for _, cfg := range dataList {
		skillCfgMap[cfg.CfgId] = cfg
	}
}

func CheckSkillCfg() bool {
	return true
}

func GetSkillCfg(cfgId int32) *data.SkillCfg {
	return skillCfgMap[cfgId]
}
