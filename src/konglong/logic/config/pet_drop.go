package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetDropCfg,
		CheckFunc: CheckPetDropCfg,
	})
}

var petDropGroupCfgMap map[int32]*data.PetDropGroupCfg

func LoadPetDropCfg() {
	logs.Infof("======================= LoadPetDropCfg =======================")
	var dataList []*data.PetDropCfg
	err := excel.XlsConfigLoad("XL_PETDROP", &dataList, nil)
	assert.Assert(err == nil, err)

	petDropGroupCfgMap = make(map[int32]*data.PetDropGroupCfg)

	for _, cfg := range dataList {
		if petDropGroupCfgMap[cfg.DropId] == nil {
			petDropGroupCfgMap[cfg.DropId] = &data.PetDropGroupCfg{
				DropId:         cfg.DropId,
				DropCfgMap:     make(map[int32]*data.PetDropCfg),
				DropWeightList: nil,
			}
		}
		groupCfg := petDropGroupCfgMap[cfg.DropId]
		groupCfg.DropCfgMap[cfg.GetCfgId()] = cfg
	}

	for _, group := range petDropGroupCfgMap {
		var totalWeight int64
		for _, dropCfg := range group.DropCfgMap {
			dropCfg.Weight += totalWeight
			totalWeight = dropCfg.Weight
			group.DropWeightList = append(group.DropWeightList, dropCfg)
		}
		for _, cfg := range group.DropWeightList {
			cfg.SetTotalWeight(totalWeight)
		}
	}

}

func CheckPetDropCfg() bool {
	logs.Infof("======================= CheckPetDropCfg =======================")
	return true
}

func GetRandomPetCfg(groupId int32) *data.PetDropCfg {
	groupCfg := petDropGroupCfgMap[groupId]
	index := util.RandomWeight(groupCfg.DropWeightList)
	return groupCfg.DropWeightList[index].(*data.PetDropCfg)
}
