package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var foodManualCfgMap map[protocol.ItemSubKind]*data.FoodManualCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadFoodManualCfg,
		CheckFunc: CheckFoodManualCfg,
	})
}
func LoadFoodManualCfg() {
	logs.Infof("======================= LoadFoodManualCfg =======================")
	var dataList []*data.FoodManualCfg

	err := excel.XlsConfigLoad("XL_MANUALFOOD", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.FoodManualCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	foodManualCfgMap = make(map[protocol.ItemSubKind]*data.FoodManualCfg)
	for _, cfg := range dataList {
		foodManualCfgMap[cfg.FoodSubKind] = cfg
	}
}

func CheckFoodManualCfg() bool {
	logs.Infof("======================= CheckFoodManualCfg =======================")
	return true
}

func GetFoodManualCfg(subKind protocol.ItemSubKind) *data.FoodManualCfg {
	return foodManualCfgMap[subKind]
}
