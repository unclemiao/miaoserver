package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/src/konglong/data"
)

func AttrToStr(attr *data.Attr) string {
	bs, err := json.Marshal(attr)
	assert.Assert(err == nil, err)
	return string(bs)
}

func StrToAttr(str string) *data.Attr {
	attr := &data.Attr{}
	err := json.Unmarshal([]byte(str), attr)
	assert.Assert(err == nil, err)
	return attr
}
