package config

import (
	"encoding/json"
	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var monCfgMap map[int32]*data.CMon

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadCMon,
		CheckFunc: CheckCMon,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadCBoss,
		CheckFunc: CheckCBoss,
	})
}

func LoadCMon() {
	logs.Infof("======================= LoadCMon =======================")
	var cmons []*data.CMon
	monCfgMap = make(map[int32]*data.CMon)
	err := util.XlsConfigLoad("XL_MONSTER", &cmons, func(state *util.XlsConfigReaderState) {
		if state.Field == "Skill" {
			cfg := state.Element.(*data.CMon)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Skill)
			assert.Assert(err2 == nil, err2)
		}
	})
	assert.Assert(err == nil, err)

	for _, cfg := range cmons {
		monCfgMap[cfg.Index] = cfg
	}
}

func CheckCMon() bool {
	logs.Infof("======================= CheckCMon =======================")
	return true
}

func CMonBy(cid int32) *data.CMon {
	cfg, find := monCfgMap[cid]
	assert.Assert(find, "item cfg not found, cid:%d", cid)
	return cfg
}

func CMonByNotErr(cid int32) *data.CMon {
	return monCfgMap[cid]
}

func CMonMapBy() map[int32]*data.CMon {
	return monCfgMap
}

// ************************************* boss ***************************************

func LoadCBoss() {
	logs.Infof("======================= LoadCBoss =======================")
	var cboss []*data.CMon
	err := util.XlsConfigLoad("XL_BOSS", &cboss, func(state *util.XlsConfigReaderState) {
		if state.Field == "Skill" {
			cfg := state.Element.(*data.CMon)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Skill)
			assert.Assert(err2 == nil, err2)
		}
	})
	assert.Assert(err == nil, err)

	for _, cfg := range cboss {
		monCfgMap[cfg.Index] = cfg
	}
}

func CheckCBoss() bool {
	logs.Infof("======================= CheckCBoss =======================")
	return true
}
