package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var invitationCfgList []*data.InviteCfg
var inviteMaxNum int32

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     200,
		LoadFunc:  LoadInvitationCfg,
		CheckFunc: CheckInvitationCfg,
	})
}
func LoadInvitationCfg() {
	logs.Infof("======================= LoadInvitationCfg =======================")
	var dataList []*data.InviteCfg

	err := excel.XlsConfigLoad("XL_INVITATIONFRIEND", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ChoiceReward" && state.Content != "" {
			cfg := state.Element.(*data.InviteCfg)
			jsonerr := json.Unmarshal([]byte(state.Content), &cfg.ChoiceReward)
			assert.Assert(jsonerr == nil, jsonerr)
		}

	})
	assert.Assert(err == nil, err)

	invitationCfgList = dataList
	for _, cfg := range dataList {
		inviteMaxNum = util.MaxInt32(inviteMaxNum, cfg.Invite)
	}

}

func CheckInvitationCfg() bool {
	return true
}

func GetInvitationCfg(cfgId int32) *data.InviteCfg {
	for _, cfg := range invitationCfgList {
		if cfg.CfgId == cfgId {
			return cfg
		}
	}
	return nil
}

func GetInvitationCfgByAmount(amount int32) *data.InviteCfg {
	for _, cfg := range invitationCfgList {
		if cfg.Invite == amount {
			return cfg
		}
	}
	return nil
}

func GetInvitationMaxNum() int32 {
	return inviteMaxNum
}

func GetNextInvitationCfg(cfgId int32) *data.InviteCfg {
	if cfgId == 0 {
		return invitationCfgList[0]
	}
	for index, cfg := range invitationCfgList {
		if cfg.CfgId == cfgId {
			if index == len(invitationCfgList)-1 {
				return nil
			}
			return invitationCfgList[index+1]
		}
	}
	return nil
}
