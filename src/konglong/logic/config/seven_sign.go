package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var sevenSignCfgList []*data.SevenSignCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSevenSignCfgCfg,
		CheckFunc: CheckSevenSignCfg,
	})
}

func LoadSevenSignCfgCfg() {
	logs.Infof("======================= LoadSevenSignCfgCfg =======================")
	var dataList []*data.SevenSignCfg

	err := excel.XlsConfigLoad("XL_SEVENSIGN", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.SevenSignCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		} else if state.Field == "ChoiceReward" && state.Content != "" {
			cfg := state.Element.(*data.SevenSignCfg)
			cfg.ChoiceReward = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	sevenSignCfgList = make([]*data.SevenSignCfg, len(dataList)+1)
	for _, cfg := range dataList {
		sevenSignCfgList[cfg.Day] = cfg
	}

}

func CheckSevenSignCfg() bool {
	logs.Info("======================= CheckSevenSignCfg =======================")
	return true
}

func GetSevenSignCfg(day int32) *data.SevenSignCfg {
	assert.Assert(day > 0 && day < int32(len(sevenSignCfgList)), "invalid day, day:%d", day)
	return sevenSignCfgList[day]
}
