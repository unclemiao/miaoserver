package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var petCfgMap map[int32]*data.PetCfg
var BreedSGradeSelect *util.WeightSelecter
var BreedSelect *util.WeightSelecter

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetCfg,
		CheckFunc: CheckPetCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetLevelCfg,
		CheckFunc: CheckPetLevelCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetResolveCfg,
		CheckFunc: CheckPetResolveCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetStarCfg,
		CheckFunc: CheckPetStarCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadTryVariationCfg,
		CheckFunc: CheckTryVariationCfg,
	})

}

func LoadPetCfg() {
	logs.Infof("======================= LoadPetCfg =======================")
	var dataList []*data.PetCfg
	petCfgMap = make(map[int32]*data.PetCfg)
	err := excel.XlsConfigLoad("XL_PET", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "EvolutionLvList" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.EvolutionLvList)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "SelfPiecesStuff" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			cfg.SelfPiecesStuff = LoadAwardCfgStr(state.Content)
		}
		if state.Field == "PiecesStuff" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			cfg.PiecesStuff = LoadAwardCfgStr(state.Content)
		}
		if state.Field == "AtkAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.AtkAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "DefAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.DefAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "HpMaxAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.HpMaxAddRate)
			assert.Assert(err2 == nil, err2)
		}

		if state.Field == "BreedAtkAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BreedAtkAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "BreedDefAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BreedDefAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "BreedHpMaxAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BreedHpMaxAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "BreedHpMaxAddRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BreedHpMaxAddRate)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "LikeTypeList" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.LikeTypeList)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "HateTypeList" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.HateTypeList)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "Skills" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Skills)
			assert.Assert(err2 == nil, err2)
			if len(cfg.Skills) > 0 {
				cfg.SkillCfgId = cfg.Skills[0]
			}
		}
		if state.Field == "SkillStudyStar" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.SkillStudyStar)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "StudySlotRate" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.StudySlotRate)
			assert.Assert(err2 == nil, err2)
		}

	})
	assert.Assert(err == nil, err)
	BreedSGradeSelect = util.NewWeightSelecter()
	for _, cfg := range dataList {
		petCfgMap[cfg.CfgId] = cfg
	}

	BreedSGradeSelect.Add(&data.BreedWeight{
		CfgId:  1,
		Breed:  0,
		Weight: 45,
	})
	BreedSGradeSelect.Add(&data.BreedWeight{
		CfgId:  2,
		Breed:  1,
		Weight: 49,
	})
	BreedSGradeSelect.Add(&data.BreedWeight{
		CfgId:  3,
		Breed:  2,
		Weight: 5,
	})
	BreedSGradeSelect.Add(&data.BreedWeight{
		CfgId:  4,
		Breed:  3,
		Weight: 1,
	})

	BreedSelect = util.NewWeightSelecter()

	BreedSelect.Add(&data.BreedWeight{
		CfgId:  1,
		Breed:  0,
		Weight: 36,
	})
	BreedSelect.Add(&data.BreedWeight{
		CfgId:  2,
		Breed:  1,
		Weight: 55,
	})
	BreedSelect.Add(&data.BreedWeight{
		CfgId:  3,
		Breed:  2,
		Weight: 7,
	})
	BreedSelect.Add(&data.BreedWeight{
		CfgId:  4,
		Breed:  3,
		Weight: 2,
	})

	// 超进化
	var superList []*data.PetCfg
	err = excel.XlsConfigLoad("XL_PETSUPEREVOLUTIONATTR", &superList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "SuperEvolutionStuff" && state.Content != "" {
			cfg := state.Element.(*data.PetCfg)
			cfg.SuperEvolutionStuff = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)
	for _, p := range superList {
		petCfg := petCfgMap[p.CfgId]
		petCfg.SuperEvolutionAtk = p.SuperEvolutionAtk
		petCfg.SuperEvolutionAtkTime = p.SuperEvolutionAtkTime
		petCfg.SuperEvolutionCrit = p.SuperEvolutionCrit
		petCfg.SuperEvolutionDef = p.SuperEvolutionDef
		petCfg.SuperEvolutionDis = p.SuperEvolutionDis
		petCfg.SuperEvolutionDodge = p.SuperEvolutionDodge
		petCfg.SuperEvolutionHp = p.SuperEvolutionHp
		petCfg.SuperEvolutionAtkRate = p.SuperEvolutionAtkRate
		petCfg.SuperEvolutionDefRate = p.SuperEvolutionDefRate
		petCfg.SuperEvolutionHpRate = p.SuperEvolutionHpRate
		petCfg.SuperEvolutionStuff = p.SuperEvolutionStuff
		petCfg.SuperEvolutionNeedStar = p.SuperEvolutionNeedStar
		petCfg.SuperEvolutionName = p.SuperEvolutionName
		petCfg.VariationSuperEvolutionName = p.VariationSuperEvolutionName
	}

}

func CheckPetCfg() bool {
	logs.Infof("======================= CheckPetCfg =======================")
	return true
}

func GetPetCfg(cfgId int32) *data.PetCfg {
	cfg, find := petCfgMap[cfgId]
	assert.Assert(find, "pet cfg not found, cid: %d", cfgId)
	return cfg
}

// ********************************* pet level ***********************************
var petLevelCfgList []*data.PetLevelCfg

func LoadPetLevelCfg() {
	logs.Infof("======================= LoadPetLevelCfg =======================")
	var dataList []*data.PetLevelCfg

	err := excel.XlsConfigLoad("XL_PET_LEVEL", &dataList, nil)
	assert.Assert(err == nil, err)

	petLevelCfgList = make([]*data.PetLevelCfg, len(dataList)+1)
	for _, cfg := range dataList {
		petLevelCfgList[cfg.Level] = cfg
	}
}

func CheckPetLevelCfg() bool {
	logs.Infof("======================= CheckPetLevelCfg =======================")

	return true
}

func PetLevelCfgBy(level int32) *data.PetLevelCfg {
	assert.Assert(level > 0 && level < int32(len(petLevelCfgList)))
	cfg := petLevelCfgList[level]
	assert.Assert(cfg != nil, "pet level cfg not found, level:%d", level)
	return cfg
}

func PetLevelCfgByCanNil(level int32) *data.PetLevelCfg {
	if level <= 0 || level >= int32(len(petLevelCfgList)) {
		return nil
	}
	return petLevelCfgList[level]
}

// ********************************* pet resolve ***********************************
var petResolveCfgList []*data.PetResolveCfg

func LoadPetResolveCfg() {
	logs.Infof("======================= LoadPetResolveCfg =======================")
	var dataList []*data.PetResolveCfg

	err := excel.XlsConfigLoad("XL_RESOLVEPET", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.PetResolveCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	petResolveCfgList = make([]*data.PetResolveCfg, len(dataList))
	for _, cfg := range dataList {
		petResolveCfgList[cfg.Star] = cfg
	}
}

func CheckPetResolveCfg() bool {
	logs.Infof("======================= CheckPetResolveCfg =======================")

	return true
}

func PetResolveCfgBy(star int32) *data.PetResolveCfg {
	assert.Assert(star >= 0 && star < int32(len(petResolveCfgList)), "invalid star, star: ", star)
	cfg := petResolveCfgList[star]
	assert.Assert(cfg != nil, "PetResolveCfg not found, star:%d ", star)
	return cfg
}

// ********************************* pet star ***********************************
var petStarCfgMap map[int32][]*data.PetStarCfg

func LoadPetStarCfg() {
	logs.Infof("======================= LoadPetStarCfg =======================")
	var dataList []*data.PetStarCfg

	err := excel.XlsConfigLoad("XL_PETSTAR", &dataList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	petStarCfgMap = make(map[int32][]*data.PetStarCfg)
	for _, cfg := range dataList {
		if len(petStarCfgMap[cfg.PetCfgId]) == 0 {
			petStarCfgMap[cfg.PetCfgId] = make([]*data.PetStarCfg, 1)
			petStarCfgMap[cfg.PetCfgId][0] = &data.PetStarCfg{
				PetCfgId:  cfg.PetCfgId,
				Star:      0,
				Hp:        0,
				Atk:       0,
				Def:       0,
				SelfNum:   0,
				SGradeNum: 0,
				AGradeNum: 0,
				BGradeNum: 0,
				CGradeNum: 0,
			}
		}
		petStarCfgMap[cfg.PetCfgId] = append(petStarCfgMap[cfg.PetCfgId], cfg)
	}
	for _, list := range petStarCfgMap {
		for i := 1; i < len(list); i++ {
			for j := i + 1; j < len(list); j++ {
				tmp := list[i]
				if list[i].Star > list[j].Star {
					list[i] = list[j]
					list[j] = tmp
				}
			}
		}
	}
}

func CheckPetStarCfg() bool {
	logs.Infof("======================= CheckPetStarCfg =======================")

	return true
}

func PetStarCfgBy(petId int32, star int32) *data.PetStarCfg {
	return petStarCfgMap[petId][star]
}

// ============================ like and hate ============================
func IsLikeFood(petCfgId int32, foodCfgId int64) bool {
	itCfg := GetItemCfg(foodCfgId)
	petCfg := GetPetCfg(petCfgId)
	for _, typeId := range petCfg.LikeTypeList {
		if int32(itCfg.SubKind) == typeId {
			return true
		}
	}
	return false
}

func IsHateFood(petCfgId int32, foodCfgId int64) bool {
	itCfg := GetItemCfg(foodCfgId)
	petCfg := GetPetCfg(petCfgId)
	for _, typeId := range petCfg.HateTypeList {
		if int32(itCfg.SubKind) == typeId {
			return true
		}
	}
	return false
}

// ============================ try varation ============================
var tryVariationCfgMap map[int32]*util.WeightSelecter

func LoadTryVariationCfg() {
	logs.Infof("======================= LoadTryVariationCfg =======================")
	var dataList []*data.TryVariationCfg

	err := excel.XlsConfigLoad("XL_TRYVARIATION", &dataList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	tryVariationCfgMap = make(map[int32]*util.WeightSelecter)
	for _, cfg := range dataList {
		sel := tryVariationCfgMap[cfg.Kind]
		if sel == nil {
			sel = util.NewWeightSelecter()
		}
		sel.Add(cfg)
		tryVariationCfgMap[cfg.Kind] = sel
	}
}

func CheckTryVariationCfg() bool {
	return true
}

func GetTryVariationCfgSelect(kind int32) *util.WeightSelecter {
	return tryVariationCfgMap[kind]
}
