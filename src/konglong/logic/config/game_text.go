package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var textMap map[int32]*data.GameTextCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadGameTextCfg,
		CheckFunc: CheckFoodBookCfg,
	})
}
func LoadGameTextCfg() {
	logs.Infof("======================= LoadGameTextCfg =======================")
	var dataList []*data.GameTextCfg

	err := excel.XlsConfigLoad("XL_GAMETEXT", &dataList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	textMap = make(map[int32]*data.GameTextCfg)
	for _, cfg := range dataList {
		textMap[cfg.CfgId] = cfg
	}
}

func CheckGameTextCfg() bool {
	logs.Infof("======================= CheckGameTextCfg =======================")
	return true
}

func GetGameTextCfg(cfgId int32) *data.GameTextCfg {
	return textMap[cfgId]
}
