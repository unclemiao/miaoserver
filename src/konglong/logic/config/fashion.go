package config

import (
	"regexp"
	"strings"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var fashionCfgMap map[int32]*data.FashionCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadFashionCfg,
		CheckFunc: CheckFashionCfg,
	})
}

func LoadFashCondCfgStr(str string) []*data.FashionCond {
	str = strings.ReplaceAll(str, " ", "")
	match, _ := regexp.Compile(`(-?\d+),(-?\d+),(-?\d+)`)
	bs := match.FindAll([]byte(str), -1)
	if bs == nil {
		match, _ := regexp.Compile(`(-?\d+),(-?\d+)`)
		bs = match.FindAll([]byte(str), -1)
	}
	var list []*data.FashionCond
	for _, s := range bs {
		ss := strings.Split(string(s), ",")
		assert.Assert(len(ss) >= 2, "invalid stuff")
		list = append(list, &data.FashionCond{
			Kind: util.AtoInt32(ss[0]),
			Cond: util.AtoInt32(ss[1]),
		})
	}
	return list
}

func LoadFashionCfg() {
	logs.Infof("======================= LoadFashionCfg =======================")
	var datas []*data.FashionCfg
	err := excel.XlsConfigLoad("XL_FASHION", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ActivateCond" && state.Content != "" {
			cfg := state.Element.(*data.FashionCfg)
			cfg.ActivateCond = LoadFashCondCfgStr(state.Content)
		} else if state.Field == "ActivateStuff" && state.Content != "" {
			cfg := state.Element.(*data.FashionCfg)
			cfg.ActivateStuff = LoadAwardCfgStr(state.Content)
		}
	})

	assert.Assert(err == nil, err)
	fashionCfgMap = make(map[int32]*data.FashionCfg)
	for _, d := range datas {
		fashionCfgMap[d.CfgId] = d
	}
}

func CheckFashionCfg() bool {
	logs.Infof("======================= CheckFashionCfg =======================")
	return true
}

func GetFashionCfg(cfgId int32) *data.FashionCfg {
	return fashionCfgMap[cfgId]
}
