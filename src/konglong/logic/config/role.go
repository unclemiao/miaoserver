package config

import (
	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/konglong/data"
)

var roleLevelCfgList []*data.RoleLevelCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadRoleLevelCfg,
		CheckFunc: CheckRoleLevelCfg,
	})
}

func LoadRoleLevelCfg() {
	logs.Infof("======================= LoadRoleLevelCfg =======================")
	var dataList []*data.RoleLevelCfg
	err := util.XlsConfigLoad("XL_ROLE_LEVEL", &dataList, nil)
	assert.Assert(err == nil, err)

	roleLevelCfgList = make([]*data.RoleLevelCfg, len(dataList)+1)
	for _, cfg := range dataList {
		roleLevelCfgList[cfg.Level] = cfg
	}
}

func CheckRoleLevelCfg() bool {
	logs.Infof("======================= CheckRoleLevelCfg =======================")
	return true
}

func GetRoleLevelCfg(level int32) *data.RoleLevelCfg {
	assert.Assert(level > 0 && level < int32(len(roleLevelCfgList)), "level err, level:%d", level)
	return roleLevelCfgList[level]
}
