package config

import (
	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/time_helper"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var quotaCfg *data.QuotaCfg
var quotaMaxCfgMap map[protocol.QuotaType]*data.QuotaMaxCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadQuotaCfg,
		CheckFunc: CheckQuotaCfg,
	})
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadCdCfg,
		CheckFunc: CheckCdCfg,
	})
}

func LoadQuotaCfg() {
	quotaCfg = &data.QuotaCfg{
		LotteryItem:         5,
		Paral:               3,
		Duel:                5,
		DailyFreeLotteryPet: 1,
	}

	logs.Infof("======================= LoadQuotaCfg =======================")
	var dataList []*data.QuotaMaxCfg
	err := excel.XlsConfigLoad("XL_QUOTA", &dataList, nil)
	assert.Assert(err == nil, err)

	quotaMaxCfgMap = make(map[protocol.QuotaType]*data.QuotaMaxCfg)
	for _, d := range dataList {
		quotaMaxCfgMap[d.Kind] = d
	}
}

func CheckQuotaCfg() bool {
	return true
}

func GetQuotaCfg() *data.QuotaCfg {
	return quotaCfg
}

func GetQuotaCfgAmount(quotaType protocol.QuotaType) int32 {
	switch quotaType {
	case protocol.QuotaType_Quota_Lottery_Item:
		return quotaCfg.LotteryItem
	case protocol.QuotaType_Quota_Paral:
		return quotaCfg.Paral
	case protocol.QuotaType_Quota_Duel:
		return quotaCfg.Duel
	default:
		return 0
	}
}

var cdCfg *data.CdCfg

func LoadCdCfg() {
	cdCfg = &data.CdCfg{
		LotteryItem: int32(time_helper.Hour),
	}

}

func CheckCdCfg() bool {
	return true
}

func GetCdCfgValue(cdType protocol.CdType) int32 {
	switch cdType {
	case protocol.CdType_CD_Lottery_Item_Quota_Cd:
		return cdCfg.LotteryItem
	default:
		return 0
	}
}

func GetQuotaMaxCfg(kind protocol.QuotaType) *data.QuotaMaxCfg {
	return quotaMaxCfgMap[kind]
}
