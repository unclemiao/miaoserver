package config

import (
	"sort"

	"github.com/kataras/golog"
)

var logs *golog.Logger

type ExcelCfg struct {
	LoadFunc  func()
	CheckFunc func() bool
	Order     int32
}

var ExcelCfgList []*ExcelCfg

func LoadConfig(logger *golog.Logger) bool {
	logs = logger
	sort.SliceStable(ExcelCfgList, func(i, j int) bool {
		return ExcelCfgList[i].Order < ExcelCfgList[j].Order
	})
	for _, cfg := range ExcelCfgList {
		cfg.LoadFunc()
	}
	return true
}

func CheckConfig() bool {
	for _, cfg := range ExcelCfgList {
		if !cfg.CheckFunc() {
			return false
		}
	}
	return true
}
