package config

import (
	"encoding/json"
	"math/rand"
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

// ************************************************** 装备相关 ************************************************
func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     100, // after item
		LoadFunc:  LoadEquipCfg,
		CheckFunc: CheckEquipCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     100, // after item
		LoadFunc:  LoadEquipGradeCfg,
		CheckFunc: CheckEquipGradeCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     100, // after item
		LoadFunc:  LoadEquipStarCfg,
		CheckFunc: CheckEquipStarCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     100, // after item
		LoadFunc:  LoadEquipResolveCfg,
		CheckFunc: CheckEquipResolveCfg,
	})
}

func LoadEquipCfg() {
	logs.Infof("======================= LoadEquipCfg =======================")
	var dataList []*data.ItemCfg

	err := excel.XlsConfigLoad("XL_EQUIP", &dataList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	for _, cfg := range dataList {
		assert.Assert(itemCfgMap[cfg.CfgId] == nil, "!装备表id重复,id:%d", cfg.CfgId)
		cfg.N9 = 1
		itemCfgMap[cfg.CfgId] = cfg
	}

}

func CheckEquipCfg() bool {
	logs.Infof("======================= CheckEquipCfg =======================")
	return true
}

// ************************************************** 装备品阶相关 ************************************************
var equipGradeCfgMap map[int32][]*data.EquipGrade
var equipGradeCfgItfMap map[int32][]util.WeightItr
var equipGradeMap map[int32]map[int32]bool

func LoadEquipGradeCfg() {
	logs.Infof("======================= LoadEquipGradeCfg =======================")
	var dataList []*data.EquipGrade

	err := excel.XlsConfigLoad("XL_EQUIPGRADE", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "HpAddRate" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.HpAddRate)
			assert.Assert(err == nil, err)
		}
		if state.Field == "AtkAddRate" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.AtkAddRate)
			assert.Assert(err == nil, err)
		}
		if state.Field == "DefAddRate" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.DefAddRate)
			assert.Assert(err == nil, err)
		}

		if state.Field == "HpRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.HpRange)
			assert.Assert(err == nil, err)
			if len(cfg.HpRange) > 0 {
				cfg.AttrNum++
			}
		}
		if state.Field == "AtkRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.AtkRange)
			assert.Assert(err == nil, err)
			if len(cfg.AtkRange) > 0 {
				cfg.AttrNum++
			}
		}
		if state.Field == "DefRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.DefRange)
			assert.Assert(err == nil, err)
			if len(cfg.DefRange) > 0 {
				cfg.AttrNum++
			}
		}
		if state.Field == "CritRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.CritRange)
			assert.Assert(err == nil, err)
			if len(cfg.CritRange) > 0 {
				cfg.AttrNum++
			}
		}
		if state.Field == "DodgeRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.DodgeRange)
			assert.Assert(err == nil, err)
			if len(cfg.DodgeRange) > 0 {
				cfg.AttrNum++
			}
		}
		if state.Field == "AtkTimeRange" && state.Content != "" {
			cfg := state.Element.(*data.EquipGrade)
			err := json.Unmarshal([]byte(state.Content), &cfg.AtkTimeRange)
			assert.Assert(err == nil, err)
			if len(cfg.AtkTimeRange) > 0 {
				cfg.AttrNum++
			}
		}
	})
	assert.Assert(err == nil, err)

	equipGradeCfgMap = make(map[int32][]*data.EquipGrade)
	equipGradeCfgItfMap = make(map[int32][]util.WeightItr)
	equipGradeMap = make(map[int32]map[int32]bool)

	for _, cfg := range dataList {
		cfg.AttrNum = 6
		equipGradeCfgMap[cfg.CfgId] = append(equipGradeCfgMap[cfg.CfgId], cfg)
		equipGradeCfgItfMap[cfg.CfgId] = append(equipGradeCfgItfMap[cfg.CfgId], cfg)
		if equipGradeMap[cfg.CfgId] == nil {
			equipGradeMap[cfg.CfgId] = make(map[int32]bool)
		}
		equipGradeMap[cfg.CfgId][cfg.Grade] = true
	}

	// build weight
	var totalWeight int64
	for _, cfgList := range equipGradeCfgMap {

		sort.Slice(cfgList, func(i, j int) bool {
			return cfgList[i].Grade < cfgList[j].Grade
		})
		for _, cfg := range cfgList {
			cfg.Weight += totalWeight
			totalWeight = cfg.Weight
		}
		for _, cfg := range cfgList {
			cfg.TotalWeight = totalWeight
		}
	}

}

func CheckEquipGradeCfg() bool {
	logs.Infof("======================= CheckEquipGradeCfg =======================")
	return true
}

func GetEquipGradeCfg(cfgId int32) *data.EquipGrade {
	return equipGradeCfgMap[cfgId][0]
}

func EquipHasGrade(cfgId int32, grade int32) bool {
	return equipGradeMap[cfgId][grade]
}

func RandomAttr(cfgId int32, grade int32) *data.Attr {
	cfg := equipGradeCfgMap[cfgId][0]
	attr := &data.Attr{}
	if cfg.AttrNum == 0 {
		return attr
	}
	perm := rand.Perm(int(cfg.AttrNum))

	for i := int32(0); i < cfg.RandNum; i++ {
		switch perm[i] {
		case 0:
			if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			} else if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			} else if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			} else if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			} else if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			} else if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			}
		case 1:
			if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			} else if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			} else if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			} else if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			} else if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			} else if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			}
		case 2:
			if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			} else if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			} else if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			} else if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			} else if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			} else if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			}
		case 3:
			if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			} else if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			} else if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			} else if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			} else if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			} else if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			}
		case 4:
			if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			} else if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			} else if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			} else if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			} else if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			} else if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			}
		case 5:
			if len(cfg.AtkTimeRange) > 0 && attr.AtkTime == 0 {
				attr.AtkTime = cfg.AtkTimeRange[rand.Intn(len(cfg.AtkTimeRange))]
			} else if len(cfg.HpRange) > 0 && attr.HpMax == 0 {
				attr.HpMax = cfg.HpRange[rand.Intn(len(cfg.HpRange))]
			} else if len(cfg.AtkRange) > 0 && attr.Atk == 0 {
				attr.Atk = cfg.AtkRange[rand.Intn(len(cfg.AtkRange))]
			} else if len(cfg.DefRange) > 0 && attr.Def == 0 {
				attr.Def = cfg.DefRange[rand.Intn(len(cfg.DefRange))]
			} else if len(cfg.CritRange) > 0 && attr.Crit == 0 {
				attr.Crit = cfg.CritRange[rand.Intn(len(cfg.CritRange))]
			} else if len(cfg.DodgeRange) > 0 && attr.Dodge == 0 {
				attr.Dodge = cfg.DodgeRange[rand.Intn(len(cfg.DodgeRange))]
			}
		default:

		}
	}
	return attr
}

// *************************** 升星相关 **************************************
var equipStarCfgMap map[int64][]*data.EquipStarCfg

func LoadEquipStarCfg() {
	logs.Infof("======================= LoadEquipStarCfg =======================")
	var dataList []*data.EquipStarCfg

	err := excel.XlsConfigLoad("XL_EQUIPSTAR", &dataList, nil)
	assert.Assert(err == nil, err)

	equipStarCfgMap = make(map[int64][]*data.EquipStarCfg)
	for _, cfg := range dataList {
		equipStarCfgMap[cfg.CfgId] = append(equipStarCfgMap[cfg.CfgId], cfg)
	}

	for _, list := range equipStarCfgMap {
		sort.Slice(list, func(i, j int) bool {
			return list[i].Star < list[j].Star
		})
	}

}

func CheckEquipStarCfg() bool {
	logs.Infof("======================= CheckEquipStarCfg =======================")
	return true
}

func GetEquipStarCfg(cid int64, star int32) *data.EquipStarCfg {
	assert.Assert(star > 0 && star <= int32(len(equipStarCfgMap[cid])), "star err, star:%d", star)
	return equipStarCfgMap[cid][star-1]
}

// ************************************** 装备回收 **************************************
var equipResolveCfgMap []*data.EquipResolveCfg

func LoadEquipResolveCfg() {
	logs.Infof("======================= LoadEquipResolveCfg =======================")
	var dataList []*data.EquipResolveCfg

	err := excel.XlsConfigLoad("XL_EQUIPRESOLVE", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.EquipResolveCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}
	})
	assert.Assert(err == nil, err)

	equipResolveCfgMap = make([]*data.EquipResolveCfg, len(dataList)+1)
	for _, cfg := range dataList {
		equipResolveCfgMap[cfg.Star] = cfg
	}
}

func CheckEquipResolveCfg() bool {
	return true
}

func GetEquipResolveCfg(star int32) *data.EquipResolveCfg {
	assert.Assert(star >= 0 && star < int32(len(equipResolveCfgMap)), "star err, star:%d", star)
	return equipResolveCfgMap[star]
}
