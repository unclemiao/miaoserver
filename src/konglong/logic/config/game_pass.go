package config

import (
	"encoding/json"
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

var gamePassCfgList []*data.GamePassCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadGamePassCfg,
		CheckFunc: CheckGamePassCfg,
	})
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadNewbieBossCfg,
		CheckFunc: CheckNewbieBossCfg,
	})
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadGamePassExtAwardCfg,
		CheckFunc: CheckGamePassExtAwardCfg,
	})
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadGameDropCfg,
		CheckFunc: CheckGameDropCfg,
	})

}

func LoadGamePassCfg() {
	logs.Infof("======================= LoadGamePassCfg =======================")
	var datas []*data.GamePassCfg
	err := excel.XlsConfigLoad("XL_GAME_PASS", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "MonsterList" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.MonsterList)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "BossList" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.BossList)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "MonAward" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			cfg.MonAward = LoadAwardCfgStr(state.Content)
		} else if state.Field == "BossAward" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			cfg.BossAward = LoadAwardCfgStr(state.Content)
		} else if state.Field == "CurrencyReward" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			cfg.CurrencyReward = LoadAwardCfgStr(state.Content)
		} else if state.Field == "ItemReward" && state.Content != "" {
			cfg := state.Element.(*data.GamePassCfg)
			cfg.ItemReward = LoadAwardCfgStr(state.Content)
		}

	})

	assert.Assert(err == nil, err)

	gamePassCfgList = make([]*data.GamePassCfg, len(datas))

	for _, cfg := range datas {
		gamePassCfgList[cfg.CfgId] = cfg
	}

}

func CheckGamePassCfg() bool {
	logs.Infof("======================= CheckGamePassCfg =======================")
	return true
}

func GamePassCfgBy(index int32) *data.GamePassCfg {
	assert.Assert(index > 0 && index < int32(len(gamePassCfgList)), "game pass not found, pass: %d", index)
	return gamePassCfgList[index]
}

func GamePassCfgListBy() []*data.GamePassCfg {
	return gamePassCfgList
}

var nGamePassExtAwardCfgList []*data.GamePassExtAwardCfg
var fGamePassExtAwardCfgList []*data.GamePassExtAwardCfg
var firstGamePassExtAwardCfg *data.AwardCfg

func LoadGamePassExtAwardCfg() {
	logs.Infof("======================= LoadGamePassExtAwardCfg =======================")
	var datas []*data.GamePassExtAwardCfg
	err := excel.XlsConfigLoad("XL_PASSEXTAWARD", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "AmountRange" && state.Content != "" {
			cfg := state.Element.(*data.GamePassExtAwardCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.AmountRange)
			assert.Assert(err2 == nil, err2)
		}
	})

	assert.Assert(err == nil, err)

	nGamePassExtAwardCfgList = make([]*data.GamePassExtAwardCfg, 0, len(datas))
	fGamePassExtAwardCfgList = make([]*data.GamePassExtAwardCfg, 0, len(datas))

	var nTotalWeight, fTotalWeight int64
	for _, cfg := range datas {

		cfg.NormalWeight += nTotalWeight
		nTotalWeight = cfg.NormalWeight

		cfg.FirstWeight += fTotalWeight
		fTotalWeight = cfg.FirstWeight
		cfg.Award = &data.AwardCfg{
			AwardList: []*data.ItemAmount{
				{
					CfgId: cfg.ItemCfgId,
					N:     1,
				},
			},
		}
	}

	for _, cfg := range datas {
		cfg.NormalTotalWeight = nTotalWeight
		cfg.FirstTotalWeight = fTotalWeight
		nGamePassExtAwardCfgList = append(nGamePassExtAwardCfgList, cfg)
		fGamePassExtAwardCfgList = append(fGamePassExtAwardCfgList, cfg)
	}

	sort.Slice(nGamePassExtAwardCfgList, func(i, j int) bool {
		return nGamePassExtAwardCfgList[i].NormalWeight < nGamePassExtAwardCfgList[j].NormalWeight
	})

	sort.Slice(fGamePassExtAwardCfgList, func(i, j int) bool {
		return fGamePassExtAwardCfgList[i].NormalWeight < fGamePassExtAwardCfgList[j].NormalWeight
	})

	firstGamePassExtAwardCfg = &data.AwardCfg{
		AwardList: []*data.ItemAmount{
			{
				CfgId: int64(protocol.CurrencyType_Diamonds),
				N:     160,
			},
		},
		Why: 0,
	}
}

func CheckGamePassExtAwardCfg() bool {
	logs.Infof("======================= CheckGamePassExtAwardCfg =======================")
	return true
}

func NoramlGamePassExtAwardCfgListBy() ([]*data.GamePassExtAwardCfg, int64) {
	return nGamePassExtAwardCfgList, nGamePassExtAwardCfgList[0].NormalTotalWeight
}

func FirstGamePassExtAwardCfgListBy() ([]*data.GamePassExtAwardCfg, int64) {
	return fGamePassExtAwardCfgList, fGamePassExtAwardCfgList[0].FirstTotalWeight
}

func GetFirstAwardCfg() *data.AwardCfg {
	return firstGamePassExtAwardCfg
}

// ************************************** 新手boss关卡 **************************************
var newbieBossCfgList []*data.NewbieBossCfg

func LoadNewbieBossCfg() {
	logs.Infof("======================= LoadNewbieBossCfg =======================")
	var datas []*data.NewbieBossCfg
	err := excel.XlsConfigLoad("XL_NEWBIEBOSS", &datas, func(state *excel.XlsConfigReaderState) {
		if state.Field == "MonsterList" && state.Content != "" {
			cfg := state.Element.(*data.NewbieBossCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.MonsterList)
			assert.Assert(err2 == nil, err2)
		}
		if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.NewbieBossCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		}
		if state.Field == "ExtAward" && state.Content != "" {
			cfg := state.Element.(*data.NewbieBossCfg)
			cfg.ExtAward = LoadAwardCfgStr(state.Content)
		}

	})

	assert.Assert(err == nil, err)

	newbieBossCfgList = make([]*data.NewbieBossCfg, len(datas)+1)

	for _, cfg := range datas {
		newbieBossCfgList[cfg.Index] = cfg
	}
}

func CheckNewbieBossCfg() bool {
	logs.Infof("======================= CheckNewbieBossCfg =======================")
	return true
}

func GetNewbieBossCfg(index int32) *data.NewbieBossCfg {
	assert.Assert(index > 0 && index < int32(len(newbieBossCfgList)), "newbieBoss[%d] cfg not found", index)
	return newbieBossCfgList[index]
}

// *************************************** 游戏掉落奖励 ***************************************
var gameDropCfgList []*data.GameDropCfg
var gameDropCfgWeightList []util.WeightItr

func LoadGameDropCfg() {
	logs.Infof("======================= LoadGameDropCfg =======================")
	var datas []*data.GameDropCfg
	err := excel.XlsConfigLoad("XL_GAMEDROP", &datas, nil)
	assert.Assert(err == nil, err)

	gameDropCfgList = datas
	gameDropCfgWeightList = nil

	var total int64
	for _, cfg := range datas {
		gameDropCfgWeightList = append(gameDropCfgWeightList, cfg)
		cfg.Weight += total
		total = cfg.Weight
	}
	for _, cfg := range datas {
		cfg.TotalWeight = total
	}
}

func CheckGameDropCfg() bool {
	logs.Infof("======================= CheckGameDropCfg =======================")
	return true
}

func GetGameDropCfg(cfgId int32) *data.GameDropCfg {
	for _, cfg := range gameDropCfgList {
		if cfg.CfgId == cfgId {
			return cfg
		}
	}
	assert.Assert(false, "drop cfg not found, cfg:%d", cfgId)
	return nil
}

func GetGameDropCfgList() ([]*data.GameDropCfg, int64) {
	return gameDropCfgList, gameDropCfgList[0].TotalWeight
}

func GameDropCfgRandomWeight() *data.GameDropCfg {
	index := util.RandomWeight(gameDropCfgWeightList)
	return gameDropCfgList[index]
}
