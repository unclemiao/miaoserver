package config

import (
	"encoding/json"
	"fmt"
	"regexp"
	"strings"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var itemCfgMap map[int64]*data.ItemCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadItemCfg,
		CheckFunc: CheckItemCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadItemMakeCfg,
		CheckFunc: CheckItemMakeCfg,
	})

	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadItemMakeProbCfg,
		CheckFunc: CheckItemMakeProbCfg,
	})

}

func LoadItemCfg() {
	logs.Infof("======================= LoadItemCfg =======================")
	var citems []*data.ItemCfg
	itemCfgMap = make(map[int64]*data.ItemCfg)
	err := excel.XlsConfigLoad("XL_PROPS", &citems, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Price" && state.Content != "" {
			cfg := state.Element.(*data.ItemCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Price)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "Award" && state.Content != "" {
			cfg := state.Element.(*data.ItemCfg)
			cfg.Award = LoadAwardCfgStr(state.Content)
		} else if state.Field == "Args1" && state.Content != "" {
			cfg := state.Element.(*data.ItemCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.Args1)
			assert.Assert(err2 == nil, err2)
		}

	})
	assert.Assert(err == nil, err)

	for _, cfg := range citems {
		itemCfgMap[cfg.CfgId] = cfg
	}

}

func CheckItemCfg() bool {
	logs.Infof("======================= CheckItemCfg =======================")
	return true
}

func GetItemCfg(cid int64) *data.ItemCfg {
	cfg, find := itemCfgMap[cid]
	assert.Assert(find, "item cfg not found, cid:%d", cid)
	return cfg
}

func ItemCfgByNotErr(cid int64) *data.ItemCfg {
	return itemCfgMap[cid]
}

func LoadAwardCfgStr(str string) *data.AwardCfg {
	str = strings.ReplaceAll(str, " ", "")
	awardCfg := &data.AwardCfg{}
	match, _ := regexp.Compile(`(-?\d+),(-?\d+),(-?\d+)`)
	bs := match.FindAll([]byte(str), -1)
	if bs == nil {
		match, _ := regexp.Compile(`(-?\d+),(-?\d+)`)
		bs = match.FindAll([]byte(str), -1)
	}
	for _, s := range bs {
		ss := strings.Split(string(s), ",")
		assert.Assert(len(ss) >= 2, "invalid stuff")
		amount := &data.ItemAmount{
			CfgId: util.AtoInt64(ss[0]),
			N:     util.AtoInt64(ss[1]),
			Rate:  1000,
		}
		if amount.N <= 0 {
			awardCfg.TotalPetNum += amount.N * (-1)
		}
		if len(ss) == 3 {
			amount.Rate = util.AtoInt32(ss[2])
		}
		awardCfg.AwardList = append(awardCfg.AwardList, amount)
	}
	return awardCfg
}

func AwardCfgAdd(to *data.AwardCfg, from *data.AwardCfg, rate float64) {
	for _, fcfg := range from.AwardList {
		find := false
		for _, tcfg := range to.AwardList {
			if tcfg.CfgId == fcfg.CfgId {
				find = true
				tcfg.N += util.MaxInt64(1, int64(float64(fcfg.N)*rate))
				break
			}
		}
		if !find {
			to.AwardList = append(to.AwardList, &data.ItemAmount{
				CfgId: fcfg.CfgId,
				N:     util.MaxInt64(1, int64(float64(fcfg.N)*rate)),
			})
		}
	}
}

func RandAwardCfg(from *data.AwardCfg) *data.AwardCfg {
	var (
		randAward = &data.AwardCfg{
			Why: from.Why,
		}
	)
	for _, amount := range from.AwardList {
		if util.RandomMatched(amount.Rate * 10) {
			randAward.AwardList = append(randAward.AwardList, amount)
		}
	}
	return randAward
}

func AwardToStr(from *data.AwardCfg) string {
	str := ""
	for _, a := range from.AwardList {
		if str == "" {
			str += fmt.Sprintf("[%d,%d]", a.CfgId, a.N)
		} else {
			str += fmt.Sprintf(",[%d,%d]", a.CfgId, a.N)
		}
	}
	if str == "" {
		return ""
	}
	return fmt.Sprintf("[%s]", str)
}

// ***************************** 合成 ***********************************
var itemMakeCfgMap map[int64]*data.MakeCfg         // cfgMap[cfgId]=*MakeCfg
var itemMakeProbCfgMap map[int32]*data.MakeProbCfg // cfgMap[kind]=*MakeProbCfg

func LoadItemMakeCfg() {
	logs.Infof("======================= LoadItemMakeCfg =======================")
	var dataList []*data.MakeCfg
	itemMakeCfgMap = make(map[int64]*data.MakeCfg)
	err := excel.XlsConfigLoad("XL_MAKE", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "Stuff" && state.Content != "" {
			cfg := state.Element.(*data.MakeCfg)
			cfg.Stuff = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	for _, cfg := range dataList {
		itemMakeCfgMap[cfg.CfgId] = cfg
	}

	var formulaList []*data.FormulaCfg
	err = excel.XlsConfigLoad("XL_FORMULA", &formulaList, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)

	for _, cfg := range formulaList {
		itemMakeCfgMap[cfg.ItemCfgId] = &data.MakeCfg{
			CfgId: cfg.ItemCfgId,
			Name:  cfg.Name,
			Stuff: &data.AwardCfg{
				AwardList: []*data.ItemAmount{
					{CfgId: cfg.StuffCfgId1, N: 1},
					{CfgId: cfg.StuffCfgId2, N: 1},
				},
			},
		}
	}

}

func CheckItemMakeCfg() bool {
	logs.Infof("======================= CheckItemMakeCfg =======================")
	return true
}

func ItemMakeCfgBy(cid int64) *data.MakeCfg {
	cfg, find := itemMakeCfgMap[cid]
	assert.Assert(find, "make cfg not found, cid:%d", cid)
	return cfg
}

func isFoodStuff(cid int64) bool {
	for _, cfg := range itemMakeCfgMap {
		for _, aw := range cfg.Stuff.AwardList {
			if aw.CfgId == cid {
				return true
			}
		}
	}
	return false
}

func LoadItemMakeProbCfg() {
	logs.Infof("======================= LoadItemMakeProbCfg =======================")
	var dataList []*data.MakeProbCfg

	err := excel.XlsConfigLoad("XL_MAKEPROB", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "LevelMax" && state.Content != "" {
			cfg := state.Element.(*data.MakeProbCfg)
			var lvs []int32
			err := json.Unmarshal([]byte(state.Content), &lvs)
			assert.Assert(err == nil, err)
			for _, lv := range lvs {
				if cfg.LevelMax == nil {
					cfg.LevelMax = make(map[int32]bool)
				}
				cfg.LevelMax[lv] = true
			}
		}

	})
	assert.Assert(err == nil, err)

	itemMakeProbCfgMap = make(map[int32]*data.MakeProbCfg)

	for _, cfg := range dataList {
		itemMakeProbCfgMap[cfg.Kind] = cfg
	}

}

func CheckItemMakeProbCfg() bool {
	logs.Infof("======================= CheckItemMakeProbCfg =======================")
	return true
}

func ItemMakeProbCfgBy(kind int32) *data.MakeProbCfg {
	cfg, find := itemMakeProbCfgMap[kind]
	assert.Assert(find, "make prob cfg not found, kind:%d", kind)
	return cfg
}

func ItemMakeProbCfgMapBy() map[int32]*data.MakeProbCfg {
	return itemMakeProbCfgMap
}
