package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var monthCardFeastCfgMap map[int32]*data.FeastCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadMonthCardFeastCfg,
		CheckFunc: CheckMonthCardFeastCfg,
	})
}

func LoadMonthCardFeastCfg() {
	logs.Infof("======================= LoadMonthCardFeastCfg =======================")
	var dataList []*data.FeastCfg

	err := excel.XlsConfigLoad("XL_FEASTMONTHCARD", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "DailyMailDesc" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.DailyMailDesc)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "ReadyOvertimeMailDesc" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.ReadyOvertimeMailDesc)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "AlreadyOvertimeMailDesc" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.AlreadyOvertimeMailDesc)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "BuyReward" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			cfg.BuyReward = LoadAwardCfgStr(state.Content)
		} else if state.Field == "DailyReward" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			cfg.DailyReward = LoadAwardCfgStr(state.Content)
		}

	})
	assert.Assert(err == nil, err)

	monthCardFeastCfgMap = make(map[int32]*data.FeastCfg)
	for _, cfg := range dataList {
		monthCardFeastCfgMap[cfg.CfgId] = cfg
	}
}

func CheckMonthCardFeastCfg() bool {
	logs.Infof("======================= CheckMonthCardFeastCfg =======================")
	return true
}

func GetMonthCardFeastCfg(cfgId int32) *data.FeastCfg {
	return monthCardFeastCfgMap[cfgId]
}
