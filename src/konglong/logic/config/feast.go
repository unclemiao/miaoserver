package config

import (
	"encoding/json"
	"fmt"
	"strings"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

func init() {
	// ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
	// 	LoadFunc:  LoadFeastCfg,
	// 	CheckFunc: CheckFeastCfg,
	// })
}

func loadFeast(f *data.FeastCfg) *data.FeastCfg {

	if len(f.On0) > 0 {
		f.Ons = make(map[int64]int64)
		f.Offs = make(map[int64]int64)
		on0s := strings.Split(f.On0, ",")
		on9s := strings.Split(f.On9, ",")
		var b, e int64

		for i := 0; i < len(on0s); i++ {
			x, x9 := on0s[i], ""
			if len(on9s)-1 >= i {
				x9 = on9s[i]
			}
			if strings.Count(x, "#") > 0 {
				w, t := strings.Split(x, "#")[0], strings.Split(x, "#")[1]
				wi := util.AtoInt64(w)
				assert.Assert(wi >= 0 && wi <= 6, fmt.Sprintf("活动cid(%d)开启时间[%s]格式配置错误\n", f.CfgId, x))

				b = time_helper.DateErr("time", t) + (3+wi)*time_helper.Day

				w9, t9 := strings.Split(x9, "#")[0], strings.Split(x9, "#")[1]
				wi9 := util.AtoInt64(w9)
				assert.Assert(wi9 >= 0 && wi9 <= 6, fmt.Sprintf("活动cid(%d)开启时间[%s]格式配置错误\n", f.CfgId, x))
				e = time_helper.DateErr("time", t9) + (3+wi9)*time_helper.Day + time_helper.Minute

				f.Week = int32(wi)

			} else {
				t, _ := time_helper.DateNoErr("time", x)
				if t > 0 {
					b = t
					e = time_helper.DateErr("time", x9) + time_helper.Minute
				} else {
					b = time_helper.DateErr("", x)
					if x9 == "" {
						e = time_helper.DateErr("", "2021-12-31 23:59") + time_helper.Minute
					} else {
						e = time_helper.DateErr("", x9) + time_helper.Minute
					}
				}
			}
			f.Ons[b] = e
		}
	}
	return f
}

func InsideTime(ts map[int64]int64, now int64, off bool) (bool, int64) {
	var offi int64
	if off {
		offi = 1
	} else {
		offi = 0
	}
	for b, e := range ts {
		if b < time_helper.Day {
			if e < b {
				e += time_helper.Day
			}
			if (now-b-offi)%time_helper.Day+offi < (e-b-1)%time_helper.Day+1 {
				return true, now - (now-b)%time_helper.Day
			}
		} else if b < 10*time_helper.Day {
			if e < b {
				e += 7 * time_helper.Day
			}
			if (now-b-offi)%(7*time_helper.Day)+offi < (e-b-1)%(7*time_helper.Day)+1 {
				return true, now - (now-b)%(7*time_helper.Day)
			}
		} else {
			if now >= b+offi && now < e {
				return true, b
			}
		}
	}
	return false, 0
}

var feastCfgMap map[protocol.FeastKind]*data.FeastsCfg
var openFeasts map[protocol.FeastKind]*data.FeastsCfg

func init() {
	feastCfgMap = make(map[protocol.FeastKind]*data.FeastsCfg)
	openFeasts = make(map[protocol.FeastKind]*data.FeastsCfg)
}

func LoadFeastCfg() {
	logs.Infof("======================= LoadFeastCfg =======================")
	var dataList []*data.FeastCfg
	dataCfgMap := make(map[protocol.FeastKind]*data.FeastsCfg)

	err := excel.XlsConfigLoad("XL_LIMITTIMEFEAST", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "ChoicePet" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.ChoicePet)
			assert.Assert(err2 == nil, err2)
		}

	})

	assert.Assert(err == nil, err)

	for _, cfg := range dataList {
		fs := dataCfgMap[cfg.Kind]
		if fs == nil {
			fs = &data.FeastsCfg{
				Kind:   cfg.Kind,
				On0:    cfg.On0,
				On9:    cfg.On9,
				Off0:   cfg.Off0,
				Off9:   cfg.Off9,
				CfgMap: make(map[int32]*data.FeastCfg),
			}
		}
		fs.CfgMap[cfg.CfgId] = cfg
		dataCfgMap[cfg.Kind] = fs
	}

	// for kind, cf := range openFeasts {
	//
	// }

}

func CheckFeastCfg() bool {
	return true
}
