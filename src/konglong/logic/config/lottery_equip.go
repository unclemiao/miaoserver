package config

import (
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

var lotteryEquipCfgList []*data.LotteryEquipCfg
var lotteryEquipCfgWeightList []util.WeightItr
var lotterySGradeEquipCfgWeightList []util.WeightItr

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		Order:     101, // after item and EquipGrade
		LoadFunc:  LoadLotteryEquipCfg,
		CheckFunc: CheckLotteryEquipCfg,
	})
}

func LoadLotteryEquipCfg() {
	logs.Infof("======================= LoadLotteryEquipCfg =======================")
	var datas []*data.LotteryEquipCfg
	err := excel.XlsConfigLoad("XL_LOTTERYEQUIP", &datas, nil)

	assert.Assert(err == nil, err)

	lotteryEquipCfgList = make([]*data.LotteryEquipCfg, 0, len(datas))
	lotteryEquipCfgWeightList = make([]util.WeightItr, 0, len(datas))
	lotterySGradeEquipCfgWeightList = make([]util.WeightItr, 0, len(datas))
	var totalWeight int64

	for _, cfg := range datas {
		if EquipHasGrade(int32(cfg.EquipCfgId), 4) {
			lotterySGradeEquipCfgWeightList = append(lotterySGradeEquipCfgWeightList, &data.LotteryEquipCfg{
				CfgId:       cfg.CfgId,
				Name:        cfg.Name,
				EquipCfgId:  cfg.EquipCfgId,
				Weight:      cfg.Weight,
				TotalWeight: cfg.TotalWeight,
				ArraryIndex: cfg.ArraryIndex,
			})
		}
		totalWeight += cfg.Weight
		cfg.Weight = totalWeight
		lotteryEquipCfgList = append(lotteryEquipCfgList, cfg)
	}
	for _, cfg := range datas {
		cfg.TotalWeight = totalWeight
	}
	sort.Slice(lotteryEquipCfgList, func(i, j int) bool {
		return lotteryEquipCfgList[i].Weight < lotteryEquipCfgList[j].Weight
	})
	for index, cfg := range lotteryEquipCfgList {
		lotteryEquipCfgWeightList = append(lotteryEquipCfgWeightList, cfg)
		cfg.ArraryIndex = int32(index)

	}

	// 构建可能出现S级装备的权重
	totalWeight = 0
	for _, cfg := range lotterySGradeEquipCfgWeightList {
		totalWeight += cfg.GetWeight()
		cfg.SetWeight(totalWeight)
	}
	for _, cfg := range lotterySGradeEquipCfgWeightList {
		cfg.SetTotalWeight(totalWeight)
	}
	sort.Slice(lotterySGradeEquipCfgWeightList, func(i, j int) bool {
		return lotterySGradeEquipCfgWeightList[i].GetWeight() < lotterySGradeEquipCfgWeightList[j].GetWeight()
	})
	for index, cfg := range lotterySGradeEquipCfgWeightList {
		weightCfg := cfg.(*data.LotteryEquipCfg)
		weightCfg.ArraryIndex = int32(index)
	}

}

func CheckLotteryEquipCfg() bool {
	logs.Infof("======================= CheckLotteryEquipCfg =======================")
	for _, cfg := range lotteryEquipCfgList {
		assert.Assert(itemCfgMap[cfg.EquipCfgId] != nil, "装备抽卡配置中装备id配置不存在于装备表中,Excle:%s, index:%d, equipId:%d",
			"lotteryEquip.xlsx", cfg.CfgId, cfg.EquipCfgId)
	}
	return true
}

func LotteryEquipCfgBy(index int32) *data.LotteryEquipCfg {
	assert.Assert(index > 0 && index < int32(len(lotteryItemCfgList)), "lottery cfg not found, index:%d", index)
	return lotteryEquipCfgList[index]
}

func GetLotteryEquipCfgList() ([]*data.LotteryEquipCfg, int64) {
	return lotteryEquipCfgList, lotteryItemCfgList[0].TotalWeight
}

func GetLotteryEquipCfgWeightList() []util.WeightItr {
	return lotteryEquipCfgWeightList
}

func GetLotterySGradeEquipCfgWeightList() []util.WeightItr {
	return lotterySGradeEquipCfgWeightList
}

func LotteryNormalEquip() int64 {
	index := util.RandomWeight(lotteryEquipCfgWeightList)
	return lotteryEquipCfgWeightList[index].(*data.LotteryEquipCfg).EquipCfgId
}

func LotterySGradeEquip() int64 {
	index := util.RandomWeight(lotterySGradeEquipCfgWeightList)
	return lotterySGradeEquipCfgWeightList[index].(*data.LotteryEquipCfg).EquipCfgId
}
