package config

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/src/konglong/data"
)

var petKingFeastCfgMap map[int32]*data.FeastCfg
var petKingFeastCfg *data.FeastCfg

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadPetKingFeastCfg,
		CheckFunc: CheckPetKingFeastCfg,
	})
}

func LoadPetKingFeastCfg() {
	logs.Infof("======================= LoadPetKingFeastCfg =======================")
	var dataList []*data.FeastCfg

	err := excel.XlsConfigLoad("XL_FEASTPETKING", &dataList, func(state *excel.XlsConfigReaderState) {
		if state.Field == "RankRange" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			err2 := json.Unmarshal([]byte(state.Content), &cfg.RankRange)
			assert.Assert(err2 == nil, err2)
		} else if state.Field == "Reward" && state.Content != "" {
			cfg := state.Element.(*data.FeastCfg)
			cfg.Reward = LoadAwardCfgStr(state.Content)
		}

	})

	assert.Assert(err == nil, err)

	for _, cfg := range dataList {
		// 解析时间
		cfg = loadFeast(cfg)
	}

	petKingFeastCfg = dataList[0]

	petKingFeastCfgMap = make(map[int32]*data.FeastCfg)
	for _, cfg := range dataList {
		petKingFeastCfgMap[cfg.CfgId] = cfg
	}
}

func CheckPetKingFeastCfg() bool {
	logs.Infof("======================= CheckLimitTimeFeastCfg =======================")
	return true
}

func GetPetKingFeastCfgBy(cfgId int32) *data.FeastCfg {
	return petKingFeastCfgMap[cfgId]
}

func GetPetKingFeastCfgByRank(rank int32) *data.FeastCfg {
	for _, cfg := range petKingFeastCfgMap {
		if rank >= cfg.RankRange[0] && rank <= cfg.RankRange[1] {
			return cfg
		}
	}
	return nil
}

func GetPetKingFeastCfg() *data.FeastCfg {
	return petKingFeastCfg
}
