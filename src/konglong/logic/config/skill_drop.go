package config

import (
	"sort"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

func init() {
	ExcelCfgList = append(ExcelCfgList, &ExcelCfg{
		LoadFunc:  LoadSkillDropCfg,
		CheckFunc: CheckSkillDropCfg,
	})
}

var skillDropGroupCfgMap map[int32]*data.SkillDropGroupCfg

func LoadSkillDropCfg() {
	logs.Infof("======================= LoadSkillDropCfg =======================")
	var dataList []*data.SkillDropCfg
	err := excel.XlsConfigLoad("XL_SKILLDROP", &dataList, nil)
	assert.Assert(err == nil, err)

	skillDropGroupCfgMap = make(map[int32]*data.SkillDropGroupCfg)

	for _, cfg := range dataList {
		if skillDropGroupCfgMap[cfg.DropId] == nil {
			skillDropGroupCfgMap[cfg.DropId] = &data.SkillDropGroupCfg{
				DropId:         cfg.DropId,
				DropCfgMap:     make(map[int32]*data.SkillDropCfg),
				DropWeightList: nil,
			}
		}
		groupCfg := skillDropGroupCfgMap[cfg.DropId]
		groupCfg.DropCfgMap[cfg.GetCfgId()] = cfg
	}

	for _, group := range skillDropGroupCfgMap {
		var totalWeight int64
		var dropCfgList []*data.SkillDropCfg
		for _, dropCfg := range group.DropCfgMap {
			dropCfg.Weight += totalWeight
			totalWeight = dropCfg.Weight
			dropCfgList = append(dropCfgList, dropCfg)
		}
		sort.Slice(dropCfgList, func(i, j int) bool {
			return dropCfgList[i].Weight < dropCfgList[j].Weight
		})
		for _, cfg := range dropCfgList {
			cfg.TotalWeight = totalWeight
			group.DropWeightList = append(group.DropWeightList, cfg)
		}
	}

}

func CheckSkillDropCfg() bool {
	logs.Infof("======================= CheckSkillDropCfg =======================")
	return true
}

func GetRandomSkillCfg(groupId int32) *data.SkillDropCfg {
	groupCfg := skillDropGroupCfgMap[groupId]
	index := util.RandomWeight(groupCfg.DropWeightList)
	return groupCfg.DropWeightList[index].(*data.SkillDropCfg)
}
