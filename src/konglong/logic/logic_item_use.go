package logic

import (
	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

// 使用道具
func (mgr *ItemManager) UseItem(ctx *data.Context, roleId int64, itemId int64, choice int32, sdkAd bool) {
	item := mgr.ItemBy(ctx, roleId, itemId)
	if item.Kit == protocol.KitType_Kit_Dustbin {
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyItemUpdate(nil, nil, []int64{itemId}, protocol.ItemActionType_Action_GiftBox))
		return
	}
	cfg := config.GetItemCfg(item.Cid)
	ctx.ArgsInt = item.Cid
	mgr.TakeItem(ctx, roleId, item, 0, 1, protocol.ItemActionType_Action_GiftBox)
	if sdkAd {
		switch cfg.Kind {
		case protocol.ItemKind_Item_Award:
			switch cfg.SubKind {
			case protocol.ItemSubKind_Sub_Food:
				cfgId := item.OpenCfgId
				if cfgId == 0 {
					stub := gStubMgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubNormal)
					var foodLikeTypeList []int32
					var minLayorPet *data.Pet
					for _, info := range stub.UnitList {
						if info.PetId > 0 {
							pet := gPetMgr.PetBy(ctx, roleId, info.PetId)
							if minLayorPet == nil {
								minLayorPet = pet
							} else if pet.Loyal < minLayorPet.Loyal {
								minLayorPet = pet
							}
						}
					}
					if minLayorPet != nil {
						foodLikeTypeList = append(foodLikeTypeList, config.GetPetCfg(minLayorPet.CfgId).LikeTypeList...)
					}

					dropGroup := config.GetItemDropGroupCfg(config.FoodBookCfgBy(gFoodBookMgr.FoodBookBy(ctx, roleId).Level).BoxDropId)
					var foodLikeIdList []int64
					for _, dropCfg := range dropGroup.DropCfgMap {
						itCfg := config.GetItemCfg(dropCfg.ItemId)
						if len(foodLikeTypeList) > 0 {
							for _, likeType := range foodLikeTypeList {
								if likeType == int32(itCfg.SubKind) {
									foodLikeIdList = append(foodLikeIdList, dropCfg.ItemId)
								}
							}
						} else {
							foodLikeIdList = append(foodLikeIdList, dropCfg.ItemId)
						}

					}
					cfgId = foodLikeIdList[util.Random(0, len(foodLikeIdList)-1)]
				}

				mgr.GiveItem(ctx, roleId, cfgId, 1, protocol.ItemActionType_Action_GiftBox)

			case protocol.ItemSubKind_Sub_Diamonds:
				mgr.GiveAward(ctx, roleId, cfg.Award, 1.0, protocol.ItemActionType_Action_GiftBox)
			case protocol.ItemSubKind_Sub_Pet:
				gLotteryPetMgr.LotteryFreeCfg(ctx, roleId, config.GetLotterylotteryBoxCfgWeightList(), protocol.PetActionType_Pet_Action_GiftBox, true)
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Game_Drop_Pet)

			}
		default:
			assert.Assert(false, "!该道具不能使用")
		}
	} else {
		switch cfg.Kind {
		case protocol.ItemKind_Item_Award:
			switch cfg.SubKind {
			case protocol.ItemSubKind_Sub_Rand_Pet:
				if cfg.PetDropId == 0 {
					gLotteryPetMgr.LotteryFree(ctx, roleId, protocol.PetActionType_Pet_Action_Open_Box, true)
				} else {
					dropCfg := config.GetRandomPetCfg(cfg.PetDropId)
					gPetMgr.GivePetFromCfgId(ctx, roleId, dropCfg.PetId,
						dropCfg.Variation == 1, dropCfg.Variation == 2, false, protocol.PetActionType_Pet_Action_Open_Box, true)
				}
			case protocol.ItemSubKind_Sub_Choice:
				itemAmount := cfg.Award.AwardList[choice-1]
				awardCfg := &data.AwardCfg{
					AwardList: []*data.ItemAmount{itemAmount},
				}
				mgr.GiveAward(ctx, roleId, awardCfg, 1.0, protocol.ItemActionType(protocol.PetActionType_Pet_Action_Open_Box))
			}
		case protocol.ItemKind_Item_Use:
			switch cfg.SubKind {
			case protocol.ItemSubKind_Sub_Add_Gold:
				ctx.IsUseGoldAddItem = true
				gHangMgr.FastHangAward(ctx, roleId)
				ctx.IsUseGoldAddItem = false
			}
		}
	}

}

func (mgr *ItemManager) openGift(ctx *data.Context, roleId int64, cfgId int64, num int64, why protocol.ItemActionType) bool {
	itemCfg := config.GetItemCfg(cfgId)
	if itemCfg.SubKind == protocol.ItemSubKind_Sub_The_Pet {
		logs.Infof("len %d", len(gPetMgr.AllPetBy(ctx, roleId).PetMap))
		if len(gPetMgr.AllPetBy(ctx, roleId).PetMap)+int(itemCfg.Award.TotalPetNum) > gConst.PET_AMOUNT_MAX {
			return false
		}
		allPet := gPetMgr.AllPetBy(ctx, roleId)
		for _, amountCfg := range itemCfg.Award.AwardList {
			for i := int64(0); i < num; i++ {
				pet := gPetMgr.NewPet(ctx, roleId, int32(amountCfg.CfgId), false, false, false, protocol.PetActionType_Pet_Action_Open_Box)
				pet.Breed = itemCfg.Args1[5]
				pet.AtkAdd = itemCfg.Args1[3]
				pet.DefAdd = itemCfg.Args1[4]

				pet.Star = itemCfg.Args1[1]
				pet.HpMaxAdd = int64(itemCfg.Args1[2])

				if itemCfg.Args1[0] > 0 {
					pet.Level = util.MaxInt32(pet.Level, itemCfg.Args1[0])
				} else {
					gPetMgr.LevelUp(ctx, pet.RoleId, pet, util.MaxInt32(pet.Level, allPet.MaxLv), false)
				}

				gAttrMgr.PetAttrBy(ctx, pet, false)
				gPetMgr.GivePet(ctx, roleId, pet, protocol.PetActionType_Pet_Action_Open_Box)
			}

		}
	}
	return true
}
