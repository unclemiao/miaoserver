package logic

import (
	"encoding/json"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type HangManager struct {
}

func init() {
	gHangMgr = &HangManager{}
}

func (mgr *HangManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewHangAward, mgr.HandlerViewHangAward)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetHangAward, mgr.HandlerGetHangAward)

	gEventMgr.When(protocol.EnvelopeType_C2S_FastGamePass, mgr.HandlerFastHang)
}

func (mgr *HangManager) calcAward(ctx *data.Context, roleId int64, doSql bool, rate float64) (*data.AwardCfg, int64) {
	var (
		role   = gRoleMgr.RoleBy(ctx, ctx.RoleId)
		tf     = gTimeFeastMgr.TimeFeastBy(ctx, role.RoleId)
		nowSec = ctx.NowSec
	)

	return mgr.CalcAwardByTime(ctx, roleId, nowSec-tf.LastHangAwardedSec, nowSec-tf.LastViewHangSec, doSql, rate, false)

}

func (mgr *HangManager) CalcAwardByTime(ctx *data.Context, roleId int64, awardSubSec int64, viewSubSec int64, doSql bool, rate float64, isFast bool) (*data.AwardCfg, int64) {
	var (
		role         = gRoleMgr.RoleBy(ctx, roleId)
		pass         = role.GamePass
		passCfg      = config.GamePassCfgBy(pass)
		tf           = gTimeFeastMgr.TimeFeastBy(ctx, role.RoleId)
		awardCfg     = &data.AwardCfg{}
		itemAwardCfg = &data.AwardCfg{}
	)
	awardSubSec = util.MinInt64(awardSubSec, gConst.HOUR*8)

	subItemSec := util.MinInt64(viewSubSec, gConst.HOUR*8)
	assert.Assert(subItemSec >= gConst.HANG_CURRENCY_SUB_SEC, "invalid time")

	// 算出货币
	subCurrencyQuota := awardSubSec / gConst.HANG_CURRENCY_SUB_SEC
	if isFast {
		config.AwardCfgAdd(awardCfg, passCfg.CurrencyReward, float64(subCurrencyQuota)*rate)
	} else {
		config.AwardCfgAdd(awardCfg, passCfg.CurrencyReward, float64(subCurrencyQuota)*float64(passCfg.CurrencyRewardArgs)/1000.0*rate)

	}

	// 计算道具
	if passCfg.ItemReward != nil && len(passCfg.ItemReward.AwardList) > 0 {
		subItemQuota := subItemSec / gConst.HANG_ITEM_SUB_SEC
		if subItemQuota == 0 {
			return awardCfg, awardSubSec
		}

		for _, amount := range passCfg.ItemReward.AwardList {
			totalRate := int64(amount.Rate) * subItemQuota

			mustCount := util.ChoiceInt64(util.RandomMatched(int32(totalRate%1000)*10), totalRate/1000+1, totalRate/1000)
			if mustCount == 0 {
				continue
			}

			itemAwardCfg.AwardList = append(itemAwardCfg.AwardList, &data.ItemAmount{
				CfgId: amount.CfgId,
				N:     mustCount * amount.N,
			})

		}

		config.AwardCfgAdd(tf.AwardInfo.ViewHangAward, itemAwardCfg, 1)
		bs, err := json.Marshal(tf.AwardInfo)
		assert.Assert(err == nil, err)
		tf.AwardStr = string(bs)
		tf.LastViewHangSec = tf.LastViewHangSec + (subItemQuota * gConst.HANG_ITEM_SUB_SEC)
		if doSql {
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_TIME_FEAST_HANG, tf.LastViewHangSec, tf.LastHangAwardedSec, tf.AwardStr, tf.RoleId)
		}
		config.AwardCfgAdd(awardCfg, itemAwardCfg, 1)
	}

	return awardCfg, awardSubSec

}

func (mgr *HangManager) hangeAward(ctx *data.Context, roleId int64, awardCfg *data.AwardCfg, clean bool) {
	var (
		tf          = gTimeFeastMgr.TimeFeastBy(ctx, roleId)
		giveToolNum = 0
		currency    = &data.AwardCfg{}
		emptyNum    = 1
	)

	for _, amount := range awardCfg.AwardList {
		cfg := config.GetItemCfg(amount.CfgId)
		if cfg.Kind == protocol.ItemKind_Item_Normal &&
			(cfg.SubKind == protocol.ItemSubKind_Sub_Tool || cfg.SubKind == protocol.ItemSubKind_Sub_Base_Stuff) {
			giveToolNum++
		} else {
			currency.AwardList = append(currency.AwardList, amount)
		}
	}
	if giveToolNum > 0 {
		emptyNum = len(gItemMgr.EmptyGridBy(ctx, roleId, protocol.KitType_Kit_Tool)) + len(gItemMgr.EmptyGridBy(ctx, roleId, protocol.KitType_Kit_Storage_Tool))
	}

	if giveToolNum > emptyNum {
		gItemMgr.GiveAward(ctx, roleId, currency, 1,
			protocol.ItemActionType(util.ChoiceInt32(ctx.IsUseGoldAddItem, int32(protocol.ItemActionType_Action_Gold_Item_Use), int32(protocol.ItemActionType_Action_HangAward))))
	} else {
		gItemMgr.GiveAward(ctx, roleId, awardCfg, 1,
			protocol.ItemActionType(util.ChoiceInt32(ctx.IsUseGoldAddItem, int32(protocol.ItemActionType_Action_Gold_Item_Use), int32(protocol.ItemActionType_Action_HangAward))))
		tf.AwardInfo.ViewHangAward = &data.AwardCfg{}
		tf.ChangeAward()
	}
	if clean {
		tf.LastHangAwardedSec = ctx.NowSec
	}

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_TIME_FEAST_HANG, tf.LastViewHangSec, tf.LastHangAwardedSec, tf.AwardStr, tf.RoleId)
}

func (mgr *HangManager) GetHangAward(ctx *data.Context, roleId int64, isAdSdk bool) *data.AwardCfg {
	var (
		awardCfg, _ = mgr.calcAward(ctx, roleId, false, float64(util.ChoiceInt32(isAdSdk, 3, 1)))
	)

	mgr.hangeAward(ctx, roleId, awardCfg, true)

	return awardCfg
}

func (mgr *HangManager) FastHangAward(ctx *data.Context, roleId int64) *data.AwardCfg {
	var (
		role        = gRoleMgr.RoleBy(ctx, roleId)
		pass        = role.GamePass
		passCfg     = config.GamePassCfgBy(pass)
		awardCfg, _ = mgr.CalcAwardByTime(ctx, roleId, passCfg.FastMinute*gConst.MINUTE, passCfg.FastMinute*gConst.MINUTE, false, 1, true)
	)

	mgr.hangeAward(ctx, roleId, awardCfg, false)

	return awardCfg
}

// ============================ Handler =====================================
func (mgr *HangManager) HandlerViewHangAward(ctx *data.Context, msg *protocol.Envelope) {
	var (
		_      = msg.GetC2SViewHangeAward()
		roleId = ctx.RoleId
	)
	awardCfg, hangTime := mgr.calcAward(ctx, roleId, true, 1)
	gRoleMgr.Send(ctx, roleId, protocol.MakeViewHangAwardDoneMsg(msg, awardCfg.GetForClient(), hangTime))
}

func (mgr *HangManager) HandlerGetHangAward(ctx *data.Context, msg *protocol.Envelope) {
	var (
		_      = msg.GetC2SGetHangeAward()
		roleId = ctx.RoleId
	)
	awardCfg := mgr.GetHangAward(ctx, roleId, len(msg.AdSdkToken) > 0)

	gRoleMgr.Send(ctx, roleId, protocol.MakeGetHangAwardDoneMsg(msg, awardCfg.GetForClient()))
}

func (mgr *HangManager) HandlerFastHang(ctx *data.Context, msg *protocol.Envelope) {
	var (
		roleId = ctx.RoleId
		role   = gRoleMgr.RoleBy(ctx, roleId)
	)

	if role.Newbie != int32(protocol.GuideType_jiasu) {
		freeLeft := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Free_Fast_Pass)
		if freeLeft > 0 {
			gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Free_Fast_Pass)
		} else {
			assert.Assert(len(msg.AdSdkToken) > 0, "can not fast")
			gQuotaCdMgr.BeginCd(ctx, roleId, ctx.NowSec, protocol.CdType_CD_Ad_Fast_Pass_Cd)
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Ad_Fast_Pass)
			if quota <= 0 {
				assert.Assert(msg.AdSdkSkip, "not AdSdkSkip")
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Gem_Fast_Pass)
			} else {
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Ad_Fast_Pass)
			}

		}
	}
	mgr.FastHangAward(ctx, roleId)
	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_FAST_PASS, 1)
}
