package logic

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type RoleManager struct {
	roleOns  map[int64]*data.Session       // 已登录的角色集合
	roleOffs map[int64]int64               // 临时断线的集合
	changes  map[int64]data.RoleChangeType // 标记角色有改变的脏标记
}

func init() {
	gRoleMgr = &RoleManager{
		roleOns:  make(map[int64]*data.Session),
		roleOffs: make(map[int64]int64),
		changes:  make(map[int64]data.RoleChangeType),
	}

}

func (mgr *RoleManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_Ping, func(ctx *data.Context, msg *protocol.Envelope) {
		gSessionMgr.Send(ctx, ctx.Session, protocol.MakePongMsg(msg))
	})

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		mgr.sessTimeout(ctx)
	})

	gEventMgr.When(data.EVENT_MSGID_DAILY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventDaily)
		for roleId, _ := range mgr.roleOns {
			gEventMgr.Call(ctx, data.EVENT_MSGID_ROLE_NEWDAY, &data.EventRoleNewDay{
				RoleId:   roleId,
				IsOnline: true,
				IsDaily:  true,
				NowSec:   args.Now.Unix(),
			})
		}
		for roleId, _ := range mgr.roleOffs {
			gEventMgr.Call(ctx, data.EVENT_MSGID_ROLE_NEWDAY, &data.EventRoleNewDay{
				RoleId:   roleId,
				IsOnline: false,
				IsDaily:  true,
				NowSec:   args.Now.Unix(),
			})
		}
	})

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getRoleCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_SigninPlayer, mgr.SignIn)

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, mgr.OnOfflineRole, 0)

	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleNewbie, mgr.HandlerSetNewbie)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetSystemOpenAward, mgr.HandlerGetSystemOpenAward)

	gEventMgr.When(protocol.EnvelopeType_C2S_SevenSign, mgr.HandlerSevenSign)

	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleSetting, mgr.HandlerSetRoleSetting)

	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleExtInfo, mgr.HandlerSetRoleExtInfo)

	gEventMgr.When(protocol.EnvelopeType_C2S_RoleHorse, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SRoleHorse()
		)
		mgr.Horse(ctx, ctx.RoleId, rep.HorseId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_RoleFeed, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SRoleFeed()
		)
		mgr.Feed(ctx, ctx.RoleId, rep.FoodIdList)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ChangeRoleModel, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SChangeRoleModel()
		)
		mgr.ChangeMode(ctx, ctx.RoleId, rep.GetModelCfgId(), rep.GetName())
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ResetRole, mgr.HandlerResetRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_AgreePrivate, mgr.HandlerAgreePrivate)
}

func (mgr *RoleManager) getRoleCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ROLE, roleId)
}

func (mgr *RoleManager) RoleByRoleIdCanNull(ctx *data.Context, roleId int64) (*data.Role, error) {
	var role *data.Role
	cacheKey := mgr.getRoleCacheKey(roleId)
	var touch bool
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var roleList []*data.Role
		gSql.MustSelect(&roleList, gConst.SQL_SELECT_ROLE, roleId)
		if len(roleList) > 0 {
			role = roleList[0]
			touch = true
		} else {
			return nil, fmt.Errorf("role[%d] not found", roleId)
		}
		return role, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	if err != nil {
		return nil, err
	}

	role = rv.(*data.Role)
	if touch {
		gAttrMgr.RoleAttrBy(ctx, role, false)
	}
	return role, nil
}

func (mgr *RoleManager) RoleBy(ctx *data.Context, roleId int64) *data.Role {
	role, err := mgr.RoleByRoleIdCanNull(ctx, roleId)
	assert.Assert(err == nil, err)
	assert.Assert(role != nil, "role[%d] not found", roleId)
	return role
}

func (mgr *RoleManager) RoleSessionBy(ctx *data.Context, roleId int64) *data.Session {
	return mgr.roleOns[roleId]
}

func (mgr *RoleManager) RoleByRoleIdAndAccountId(ctx *data.Context, roleId int64, accountId string) *data.Role {
	role, err := mgr.RoleByRoleIdCanNull(ctx, roleId)
	assert.Assert(err == nil, err)
	assert.Assert(role != nil, "role[%d] not found", roleId)
	assert.Assert(role.AccountId == accountId, "role[%d] not found", roleId)
	return role
}

func (mgr *RoleManager) RoleByAccountId(ctx *data.Context, accountId string, sid int32) (*data.Role, bool) {
	var roleList []*data.Role
	gSql.MustSelect(&roleList, gConst.SQL_SELECT_ROLE_BY_ACCOUNT, accountId, sid)
	if len(roleList) > 0 {
		for _, r := range roleList {
			// todo 验证tag
			if r.Tag&gConst.ROLE_TAG_DEL == 0 {
				return r, false
			}
		}
		return nil, false
	} else {
		return nil, true
	}
}

func (mgr *RoleManager) SignIn(ctx *data.Context, msg *protocol.Envelope) {
	reqMsg := msg.GetC2SSigninPlayer()
	session := ctx.Session
	var (
		accountId     = reqMsg.GetAccountId()
		accountName   = reqMsg.GetOpenId()
		sid           = reqMsg.GetSnode()
		from          = reqMsg.GetFrom()
		young         = reqMsg.GetYoung()
		due           = reqMsg.GetDue()
		token         = reqMsg.GetToken()
		inviteId      = reqMsg.GetInvitaterId()
		inviteSid     = int64(reqMsg.GetInvitaterSid())
		serverCfg     = gAppCnf.Server
		_, isThisNode = gServer.NodeRolesMap[sid]
	)

	// 检测各个参数
	assert.Assert(sid > 0 && sid < 10000, "invalid snode")
	assert.Assert(isThisNode, "!非本服玩家")
	assert.Assert(len(from) <= 64, "!渠道名太长")
	assert.Assert(len(accountId) > 0, "!帐号不能为空")

	// 检测 token
	if gAdminMgr.ServerCmd.Token != "*" {
		assert.Assert(time_helper.NowSec() <= due, "token time out")
		md5 := util.Md5(fmt.Sprintf("%s%s%d%s", accountId, from, due, gAdminMgr.ServerCmd.Token))
		assert.Assert(md5 == token, "invalid token")
	} else {
		young = serverCfg.Young
	}

	role, _ := mgr.RoleByAccountId(ctx, accountId, sid)

	var isNew int32 = 1
	if role == nil {
		role = mgr.newRole(ctx, accountId, accountName, sid, "", from, inviteId, inviteSid)
		isNew = 0
	}
	assert.Assert(role.Tag&gConst.ROLE_TAG_LOCK == 0, "角色%s已被锁定, 请联系客服", role.Name)
	gAttrMgr.RoleAttrBy(ctx, role, false)
	session.AccountId, session.From, session.Young, session.Sid = accountId, from, young, sid
	ctx.RoleId = role.RoleId
	ctx.Sid = sid
	mgr.SignInRole(ctx, isNew)
	f := gTimeFeastMgr.TimeFeastBy(ctx, role.RoleId)
	stubInfo := gStubMgr.StubInfoBy(ctx, role.RoleId, protocol.StubKind_StubNormal)
	stubInfoPb := gStubMgr.GetForClient(ctx, stubInfo)
	gStubMgr.StubPbAttrBy(ctx, stubInfoPb)
	repMsg := protocol.MakeSignInDoneMsg(
		msg,
		role.GetForClient(),
		gItemMgr.GetAllItemForClient(ctx, role.RoleId),
		gPetMgr.GetAllPetForClient(ctx, role.RoleId),
		f.LastViewHangSec,
		f.GetForClient(),
		stubInfoPb,
	)

	mgr.Send(ctx, role.RoleId, repMsg)

	ctx.Stop = true
}

func (mgr *RoleManager) checkName(ctx *data.Context, name string) {

}

func (mgr *RoleManager) newRole(
	ctx *data.Context,
	accountId string,
	accountName string,
	sid int32,
	name string,
	channel string,
	inviteId int64,
	inviteSid int64,
) *data.Role {
	role := &data.Role{
		RoleId:      gIdMgr.GenerateId(ctx, sid, gConst.ID_TYPE_ROLE),
		Name:        name,
		Channel:     channel,
		AccountId:   accountId,
		AccountName: accountName,
		Sid:         sid,
		Level:       1,

		GamePass:            1,
		LotteryItemRate:     1,
		Audio:               true,
		Shake:               true,
		HelpIndexUnlockInfo: 0b00001000,
		InviteId:            inviteId,
		InviteSid:           inviteSid,
	}

	gSql.Begin().Clean(role.RoleId)
	gSql.MustExcel(gConst.SQL_INSERT_ROLE,
		role.RoleId,
		role.AccountId,
		role.AccountName,
		role.Channel,
		role.Sid,
		role.Tag,
		role.Name,
		role.Avatar,
		role.GamePass,
		role.LotteryItemRate,
		role.HelpIndexUnlockInfo,
		role.Horse,
		role.ModelCfgId,
		role.ModelName,
		role.PrivateProto,
		role.Audio,
		role.Shake,
		role.Language,
		role.InviteId,
		role.InviteSid,
	)
	return role
}

func (mgr *RoleManager) SignInRole(ctx *data.Context, isNew int32) {

	var (
		roleId  = ctx.RoleId
		session = ctx.Session
		signIn  = true
	)

	role := mgr.RoleBy(ctx, roleId)

	oldSess := mgr.roleOns[roleId]
	if oldSess != nil {
		assert.Assert(oldSess != ctx.Session, "duplicated signIn")
		gSessionMgr.SendAtOnce(oldSess, protocol.MakeKickRoleMsg())
		gEventMgr.Call(ctx, data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: false})
	}

	if mgr.roleOffs[roleId] > 0 {
		// 临时短暂断线
		delete(mgr.roleOffs, roleId)
		signIn = false
	}

	mgr.roleOns[roleId] = session
	gSessionMgr.SessionSetRoleId(session, roleId)
	gServer.NodeRolesMap[role.Sid] += 1

	gEventMgr.Call(ctx, data.EVENT_MSGID_ONLINE_ROLE, &data.EventOnlineRole{RoleId: roleId, SignIn: signIn, IsNew: isNew == 0})

}

func (mgr *RoleManager) AddExp(ctx *data.Context, roleId int64, exp uint64) {
	role := mgr.RoleBy(ctx, roleId)
	totalExp := role.Exp + exp
	oldLevel := role.Level
	level := role.Level
	levelCfg := config.GetRoleLevelCfg(level)

	for levelCfg != nil {
		if totalExp < levelCfg.Exp || levelCfg.Exp == 0 {
			break
		}
		level = levelCfg.Level + 1
		totalExp -= levelCfg.Exp
		levelCfg = config.GetRoleLevelCfg(level)
		gEventMgr.Call(ctx, data.EVENT_MSGID_ROLE_LEVEL, &data.EventRoleLevel{
			Role:     role,
			OldLevel: level - 1,
			NewLevel: level,
		})
	}

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_LEVEL, level, totalExp, roleId)

	role.Level, role.Exp = level, totalExp
	if oldLevel != role.Level {
		gAttrMgr.RoleAttrBy(ctx, role, false)
	}

	// send
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

func (mgr *RoleManager) AddPay(ctx *data.Context, roleId int64, pay int32) {
	role := mgr.RoleBy(ctx, roleId)
	gSql.Begin().Clean(roleId)

	role.Payn++
	role.Payall += pay
	gSql.MustExcel(gConst.SQL_UPDATE_ROLE_PAY, role.Payn, role.Payall, role.RoleId)
}

func (mgr *RoleManager) Horse(ctx *data.Context, roleId int64, horseId int64) {
	role := mgr.RoleBy(ctx, roleId)
	oldHorse := role.Horse

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_HORSE, horseId, roleId)

	if horseId > 0 {
		// 验证 horseId 合法性
		pet := gPetMgr.PetBy(ctx, roleId, horseId)
		petCfg := config.GetPetCfg(pet.CfgId)
		assert.Assert(pet.Star >= petCfg.HorseStar, "star not enough, star:%d", pet.Star)
		assert.Assert(petCfg.CanHorse > 0, "this pet can not horse, petCfgId:%d", petCfg.CfgId)
		instub, _ := gStubMgr.InStub(ctx, roleId, horseId)
		assert.Assert(!instub, "pet can not horse, pet in stub")
		role.Horse = horseId
		gAttrMgr.PetAttrBy(ctx, pet, true)
	} else {
		role.Horse = horseId
		pet := gPetMgr.PetBy(ctx, roleId, oldHorse)
		gAttrMgr.PetAttrBy(ctx, pet, true)
	}

	gAttrMgr.RoleAttrBy(ctx, role, true)
}

func (mgr *RoleManager) Feed(ctx *data.Context, roleId int64, foodIdList []int64) {
	var totalExp int64
	for _, itId := range foodIdList {
		it := gItemMgr.ItemBy(ctx, roleId, itId)
		itCfg := config.GetItemCfg(it.Cid)
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, 1, protocol.ItemActionType_Action_Role_Feed)
		totalExp += itCfg.AddRoleExp
	}
	mgr.AddExp(ctx, roleId, uint64(totalExp))

}

func (mgr *RoleManager) ChangeMode(ctx *data.Context, roleId int64, modelCfgId int32, name string) {
	role := mgr.RoleBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	role.ModelCfgId, role.ModelName = modelCfgId, name
	gSql.MustExcel(gConst.SQL_UPDATE_ROLE_MODEL, role.ModelCfgId, role.ModelName, role.RoleId)
	// send
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

func (mgr *RoleManager) OnOfflineRole(ctx *data.Context, msg interface{}) {
	var (
		envMsg = msg.(*data.EventOfflineRole)
		roleId = envMsg.RoleId
	)

	if envMsg.SignOut {
		if mgr.roleOns[roleId] != nil {
			gEventMgr.Call(ctx, data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: false})
		}
		gEventMgr.Call(ctx, data.EVENT_MSGID_SQL_ROLLBACK, &data.EventSqlRollBack{RoleId: roleId})

		delete(mgr.roleOffs, roleId)

	} else {
		session := mgr.roleOns[roleId]
		assert.Assert(session != nil, "role not online")
		delete(mgr.roleOns, roleId)
		gServer.NodeRolesMap[session.Sid]--
		gSessionMgr.Stop(session)
		mgr.roleOffs[roleId] = time_helper.NowSec() + gConst.ROLE_OFFLINE_TIME_OUT
	}
}

func (mgr *RoleManager) sessTimeout(ctx *data.Context) {
	TimeAfter(nil, time.Duration(time.Now().Unix())+time.Second+time.Duration(15), func(ctx2 *data.Context) {
		mgr.sessTimeout(ctx2)
	})
	nowSec := time_helper.NowSec()
	for roleId, timeOut := range mgr.roleOffs {
		if nowSec > timeOut {
			gEventMgr.Call(ctx, data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: true})
		}
	}
}

func (mgr *RoleManager) Send(ctx *data.Context, roleId int64, msg *protocol.Envelope) {
	session := mgr.roleOns[roleId]
	if session != nil && !session.IsStop {
		if ctx.SendAtOnce {
			gSessionMgr.SendAtOnce(session, msg)
		} else {
			gSessionMgr.Send(ctx, session, msg)
		}
	}
}

func (mgr *RoleManager) SendAtOnce(ctx *data.Context, roleId int64, msg *protocol.Envelope) {
	session := mgr.roleOns[roleId]
	if session != nil && !session.IsStop {
		gSessionMgr.SendAtOnce(session, msg)
	}
}

func (mgr *RoleManager) IsOnline(roleId int64) bool {
	return mgr.roleOns[roleId] != nil
}

func (mgr *RoleManager) Broadcast(msg *protocol.Envelope) {
	for _, s := range mgr.roleOns {
		gSessionMgr.SendAtOnce(s, msg)
	}
}

// ************************************ controllers ******************************
func (mgr *RoleManager) HandlerSetNewbie(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SSetRoleNewbie()
	)
	r := mgr.RoleBy(ctx, ctx.RoleId)
	r.Newbie = repMsg.Step
	gSql.Begin().Clean(r.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_NEWBOE, r.Newbie, r.RoleId)
	mgr.Send(ctx, r.RoleId, protocol.MakeNotifyRoleUpdateMsg(r.GetForClient()))
}

func (mgr *RoleManager) HandlerSevenSign(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SSevenSign()
		roleId = ctx.RoleId
		day    = repMsg.Day
		choice = repMsg.Choice
	)

	role := mgr.RoleBy(ctx, roleId)

	assert.Assert(role.SevenSign+1 == day, "already sign, day:%d", day)
	assert.Assert(time_helper.NowSec() >= int64(role.NextSevenSignTime), "already sign today, day:%d", day)
	role.SevenSign = day
	role.NextSevenSignTime = int32(time_helper.Eod(time.Now()).Unix())
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_SEVEN_SIGN, role.SevenSign, role.NextSevenSignTime, roleId)

	cfg := config.GetSevenSignCfg(day)
	if cfg.Reward != nil && len(cfg.Reward.AwardList) > 0 {
		gItemMgr.GiveAward(ctx, roleId, cfg.Reward, 1, protocol.ItemActionType_Action_SevenSign)
	} else if cfg.ChoiceReward != nil && len(cfg.ChoiceReward.AwardList) > 0 {
		award := &data.AwardCfg{}
		award.AwardList = append(award.AwardList, cfg.ChoiceReward.AwardList[choice])
		gItemMgr.GiveAward(ctx, roleId, award, 1, protocol.ItemActionType_Action_SevenSign)
	} else if cfg.PetDropId > 0 {
		dropCfg := config.GetRandomPetCfg(cfg.PetDropId)
		gPetMgr.GivePetFromCfgId(ctx, roleId, dropCfg.PetId, dropCfg.Variation == 1, dropCfg.Variation == 2, false, protocol.PetActionType_Pet_Action_SevenSign, true)
	}

	mgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

func (mgr *RoleManager) HandlerSetRoleSetting(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SSetRoleSetting()
		audio  = repMsg.Audio
		shake  = repMsg.Shake
		role   = mgr.RoleBy(ctx, ctx.RoleId)
	)
	role.Audio, role.Shake = audio, shake
	gSql.Begin().Clean(ctx.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_SETTING, role.Audio, role.Shake, role.RoleId)

	mgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

func (mgr *RoleManager) HandlerGetSystemOpenAward(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SGetSystemOpenAward()
		index  = repMsg.Index - 1 // 从 1 开始
		roleId = ctx.RoleId
	)
	r := gRoleMgr.RoleBy(ctx, roleId)
	cfg := config.SystemOpenCfgBy(index)
	if cfg.UnLockType == 1 {
		assert.Assert(r.Level >= cfg.UnLockCond, "system not open, roleId%d, index:%d", roleId, index)
	} else {
		assert.Assert(r.GamePass >= cfg.UnLockCond, "system not open, roleId%d, index:%d", roleId, index)
	}
	tag := 1 << index

	assert.Assert(r.SystemAwarded&int64(tag) == 0, "already awarded, index:%d", index)
	r.SystemAwarded = r.SystemAwarded ^ int64(tag)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_SYSTEM_OPEN_AWARD, r.SystemAwarded, r.RoleId)

	gItemMgr.GiveAward(ctx, roleId, cfg.Award, 1, protocol.ItemActionType_Action_SystemOpen)

	mgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyRoleUpdateMsg(r.GetForClient()))
}

func (mgr *RoleManager) HandlerSetRoleExtInfo(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SSetRoleExtInfo()
	)
	role := mgr.RoleBy(ctx, ctx.RoleId)
	role.Name = repMsg.Name
	role.Avatar = repMsg.Avatar
	role.Language = repMsg.Language
	_, err := gSql.Begin().Clean(ctx.RoleId).Excel(gConst.SQL_UPDATE_ROLE_INFO, role.Name, role.Avatar, role.Language, role.RoleId)
	assert.Assert(err == nil, "!名字重复")
	mgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

func (mgr *RoleManager) HandlerResetRole(ctx *data.Context, msg *protocol.Envelope) {

	role := mgr.RoleBy(ctx, ctx.RoleId)
	role.Tag = role.Tag | gConst.ROLE_TAG_DEL
	gSql.Begin().Clean(ctx.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_TAG, role.Tag, role.RoleId)

	centerHost := gGameFlag.CenterHost

	plt, ch := gAdminMgr.ServerCmd.Platform, gAdminMgr.ServerCmd.Channel
	due := fmt.Sprintf("%d", ctx.NowSec+time_helper.Hour)
	form := make(url.Values)
	form.Add("accountId", role.AccountId)
	form.Add("platform", plt)
	form.Add("channel", ch)
	form.Add("due", due)

	md5 := util.Md5(fmt.Sprintf("-_-!!!%s%s%s%s%s", role.AccountId, plt, ch, due, "b7561aa298be84e4c3c2a7901e47c350"))
	form.Add("weiyingToken", md5)

	formUrl := centerHost + "/account/resetSid?" + form.Encode()
	logs.Infof("send resetSid : %s", formUrl)
	resp, err := http.Get(formUrl)
	if err != nil {
		logs.Error(err)
		return
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		return
	}
	logs.Infof("send  resetSid response: %s", string(bs))

	mgr.SendAtOnce(ctx, ctx.RoleId, protocol.MakeResetRoleDoneMsg(ctx.EventMsg.GetEnvMsg()))
	gEventMgr.Call(ctx, data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: ctx.RoleId, SignOut: false})
}

func (mgr *RoleManager) HandlerAgreePrivate(ctx *data.Context, msg *protocol.Envelope) {

	role := mgr.RoleBy(ctx, ctx.RoleId)
	gSql.Begin().Clean(ctx.RoleId)
	role.PrivateProto = true
	gSql.MustExcel(gConst.SQL_UPDATE_PRIVATE_PROTO, role.PrivateProto, role.RoleId)

}
