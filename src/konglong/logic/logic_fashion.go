package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type FashionManager struct {
	cache *cache.ShardedCache
}

var gFashionMgr *FashionManager

func init() {
	gFashionMgr = &FashionManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4),
	}
}

func (mgr *FashionManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllFashion, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Notify(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ActivateFashion, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SActivateFashion()
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		ctx.ArgsInt = int64(rep.GetCfgId())
		mgr.Activate(ctx, ctx.RoleId, rep.GetCfgId())
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_LoadFashion, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SLoadFashion()
		mgr.Load(ctx, ctx.RoleId, rep.GetCfgId(), rep.GetKind())
	})
}

func (mgr *FashionManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FASHION, roleId)
}

func (mgr *FashionManager) RoleFashionBy(ctx *data.Context, roleId int64) *data.RoleFashion {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleFashion := &data.RoleFashion{
			FashionMap: make(map[int32]*data.Fashion), // { cfgId = *Fashion }
			LoadingMap: make(map[int32]int32),         // { kind = cfgId }
		}
		var dataList []*data.Fashion

		gSql.MustSelect(&dataList, gConst.SQL_SELECT_FASHION, roleId)
		for _, info := range dataList {
			roleFashion.FashionMap[info.CfgId] = info
			cfg := config.GetFashionCfg(info.CfgId)
			if info.State == 2 {
				roleFashion.LoadingMap[cfg.Kind] = cfg.CfgId
			}
		}
		return roleFashion, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.RoleFashion)
}

func (mgr *FashionManager) FashionBy(ctx *data.Context, roleId int64, cfgId int32) *data.Fashion {
	roleFashion := mgr.RoleFashionBy(ctx, roleId)
	return roleFashion.FashionMap[cfgId]
}

func (mgr *FashionManager) GetForClient(ctx *data.Context, fashion *data.Fashion) *protocol.Fashion {
	return &protocol.Fashion{
		CfgId: fashion.CfgId,
		State: fashion.State,
	}
}

func (mgr *FashionManager) newActivate(ctx *data.Context, roleId int64, cfgId int32) *data.Fashion {
	fashion := &data.Fashion{
		Id:     gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FASHION),
		RoleId: roleId,
		CfgId:  cfgId,
		State:  1,
	}
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_FASHION, fashion.Id, fashion.RoleId, fashion.CfgId, fashion.State)
	roleFashion := mgr.RoleFashionBy(ctx, roleId)
	roleFashion.FashionMap[cfgId] = fashion
	return fashion
}

func (mgr *FashionManager) Notify(ctx *data.Context, roleId int64) {
	var pbList []*protocol.Fashion
	roleFashion := mgr.RoleFashionBy(ctx, roleId)
	for _, f := range roleFashion.FashionMap {
		pbList = append(pbList, mgr.GetForClient(ctx, f))
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFashionListMsg(pbList))
}

func (mgr *FashionManager) Activate(ctx *data.Context, roleId int64, cfgId int32) {
	cfg := config.GetFashionCfg(cfgId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	for _, cond := range cfg.ActivateCond {
		if cond.Kind == 1 {
			assert.Assert(role.Level >= cond.Cond, "level not enough, need:%d", cond.Cond)
		} else if cond.Kind == 2 {
			assert.Assert(role.GamePass >= cond.Cond, "game pass not enough, need:%d", cond.Cond)
		} else if cond.Kind == 3 {
			tf := gTimeFeastMgr.TimeFeastBy(ctx, roleId)
			assert.Assert(tf.Signin >= cond.Cond, "Signin not enough, need:%d", cond.Cond)
		} else if cond.Kind == 4 {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Ad_Sdk)
			assert.Assert(quota >= cond.Cond, "daily ad sdk not enough, need:%d", cond.Cond)
		}
	}
	if cfg.ActivateStuff != nil {
		gItemMgr.TakeAward(ctx, roleId, cfg.ActivateStuff, 1, protocol.ItemActionType_Action_Fashion_Activate)
	}
	fashion := mgr.newActivate(ctx, roleId, cfgId)
	gAttrMgr.RoleAttrBy(ctx, gRoleMgr.RoleBy(ctx, roleId), true)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFashionListMsg([]*protocol.Fashion{mgr.GetForClient(ctx, fashion)}))
}

func (mgr *FashionManager) Load(ctx *data.Context, roleId int64, cfgId int32, kind int32) {
	roleFashion := mgr.RoleFashionBy(ctx, roleId)
	var pbList []*protocol.Fashion
	if cfgId > 0 {
		fashion := mgr.FashionBy(ctx, roleId, cfgId)
		assert.Assert(fashion != nil, "fashion not found, roleId%d, cfgId:%d", roleId, cfgId)
		fashionCfg := config.GetFashionCfg(cfgId)
		oldLoadedCfgId := roleFashion.LoadingMap[fashionCfg.Kind]
		gSql.Begin().Clean(roleId)
		if oldLoadedCfgId > 0 {
			oldLoadedFashion := mgr.FashionBy(ctx, roleId, oldLoadedCfgId)
			oldLoadedFashion.State = 1
			pbList = append(pbList, mgr.GetForClient(ctx, oldLoadedFashion))
			gSql.MustExcel(gConst.SQL_UPDATE_FASHION, oldLoadedFashion.State, oldLoadedFashion.Id)
		}
		roleFashion.LoadingMap[fashionCfg.Kind] = fashion.CfgId
		fashion.State = 2
		pbList = append(pbList, mgr.GetForClient(ctx, fashion))
		gSql.MustExcel(gConst.SQL_UPDATE_FASHION, fashion.State, fashion.Id)
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFashionListMsg(pbList))

	} else {

		oldLoadedCfgId := roleFashion.LoadingMap[kind]
		assert.Assert(oldLoadedCfgId > 0, "this kind has no fashion, kind %d", kind)
		roleFashion.LoadingMap[kind] = 0
		oldFashion := mgr.FashionBy(ctx, roleId, oldLoadedCfgId)
		oldFashion.State = 1
		gSql.MustExcel(gConst.SQL_UPDATE_FASHION, oldFashion.State, oldFashion.Id)
		pbList = append(pbList, mgr.GetForClient(ctx, oldFashion))
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFashionListMsg(pbList))
	}

	gAttrMgr.RoleAttrBy(ctx, gRoleMgr.RoleBy(ctx, roleId), true)
}
