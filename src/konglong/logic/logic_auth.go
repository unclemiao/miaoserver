package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func authPlayer(args *data.Context, _ *protocol.Envelope) {
	session := args.Session
	assert.Assert(session.AccountId != "", "player not signin")
	args.AccountId = session.AccountId
	args.Sid = session.Sid
}

func authRole(args *data.Context, msg *protocol.Envelope) {
	session := args.Session
	assert.Assert(session.AccountId != "", "player not signin")
	assert.Assert(session.RoleId > 0, "role offline")
	args.AccountId = session.AccountId
	args.RoleId = session.RoleId
	args.Sid = session.Sid

}

func authEnd(args *data.Context, msg *protocol.Envelope) {
	session := args.Session

	if len(msg.AdSdkToken) > 0 {
		gDailyQuestMgr.Done(args, session.RoleId, gConst.QUEST_TYPE_LOOK_AD, 1)
		gQuotaCdMgr.GiveQuota(args, args.RoleId, 1, protocol.QuotaType_Quota_Daily_Ad_Sdk)

		if msg.AdSdkSkip {
			gItemMgr.TakeItem(args, session.RoleId, nil, int64(protocol.CurrencyType_Gem), 10, protocol.ItemActionType_Action_Ad_Sdk_Skip)
		} else {
			gQuotaCdMgr.TakeQuota(args, args.RoleId, 1, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk)
		}

	}

}

func authAdSdk(session *data.Session, msg *protocol.Envelope, bs []byte) bool {
	nowSec := time_helper.NowSec()
	if !msg.AdSdkSkip && nowSec-session.LastAuthAd <= 6 && gAdminMgr.ServerCmd.Platform != "test" {
		return false
	}
	token := msg.AdSdkToken
	serverToken := gAdminMgr.ServerCmd.Token
	session.LastAuthAd = nowSec
	md5 := util.Md5(fmt.Sprintf("%d%d%s", msg.MsgType, msg.SeqId, serverToken))
	return md5 == token
}

func AuthLoadConfig() {
	gEventMgr.WhenNetOver(authEnd)
	gEventMgr.When(protocol.EnvelopeType_C2S_Share, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_Collect, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_CollectAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetSubscribe, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SetSubscribe, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleNewbie, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetSystemOpenAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ResetRole, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleSetting, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SevenSign, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleSetting, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewHangAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetHangAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SetRoleExtInfo, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_RoleHorse, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_RoleFeed, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ChangeRoleModel, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AgreePrivate, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherRole, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherStub, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetRoleTalent, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_RoleTalentLevelUp, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ResetRoleTalent, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemUp, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemKeep, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemShare, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_UseItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_MoveItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_BuyFood, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FoodBookLevelUp, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FoodBookHighBuy, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_TakeToolTmpAllItem, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetLotteryItemInfo, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_SetLotteryItemTenRate, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LoadEquip, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryEquip, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EquipStar, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EquipInherit, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EquipResolve, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllFashion, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ActivateFashion, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LoadFashion, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FeedPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetStar, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetStarBatch, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetMake, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetResolve, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EvolutionPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetLevelUpMax, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherPlayerPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetDead, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetAddLoyal, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ChangePetName, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_TryVariation, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetBreed, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_BeginBreed, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FastBreed, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EndBreed, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_CancelBreed, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetFoster, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_BeginFoster, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_EndFoster, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetLotteryPetAwardBox, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_DailyLotteryPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetChangeStar, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetSuperEvolution, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AddSuperEvolutionProgress, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetSuperEvolution, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetSuperEvolutionItemBuy, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetStudySkill, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_WinPassMonster, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_WinPassBoss, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LosePassMonster, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LosePassBoss, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FastGamePass, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_WinParal, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LoseParal, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetGamePassExtAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_WinNewbieBoss, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AwardNewbieBoss, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AwardExtNewbieBoss, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_StubPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_StubHelpPet, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_UnlockStubHelp, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetStub, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardDailyQuest, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AwardDailyQuestAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetManual, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetFoodManualAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FettersActive, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FettersAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelInfo, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_RefreshDuel, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_WinDuel, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LoseDuel, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelRankList, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetDuelFightRecordList, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetVenture, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_VentureWinPass, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_VentureFastChapter, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_VentureChoiceBuff, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_VentureRefreshBuff, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastDone, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_FeastAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastPetKingInfo, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastPetKingRankList, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_RefreshPetKing, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_WinPetKing, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_LosePetKing, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetKingFightRecordList, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_PetKingAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_AddQuota, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetRanking, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetRankingAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetMailList, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_ReadMail, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_AwardMail, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_DelMail, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_OpenChat, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_Chat, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_CloseChat, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetCoupon, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_LookAdSdkAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardAdSdkAward, authRole)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetInviteList, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_GetInviteAward, authRole)
	gEventMgr.When(protocol.EnvelopeType_C2S_BeInvited, authRole)
}
