package logic

import (
	"flag"

	"goproject/src/konglong/data"
)

var gGameFlag *data.GameFlag

func InitFlag() {
	gGameFlag = new(data.GameFlag)
	flag.Int64Var(&gGameFlag.Id, "id", 0, "区服id")
	flag.Int64Var(&gGameFlag.Sid, "sid", 0, "服务器号")
	flag.StringVar(&gGameFlag.Platform, "platform", "", "平台名")
	flag.StringVar(&gGameFlag.Channel, "channel", "", "渠道名")
	flag.StringVar(&gGameFlag.CenterHost, "centerHost", "", "中心服url")
	flag.StringVar(&gGameFlag.AdminHost, "adminHost", "", "监控服url")
	flag.BoolVar(&gGameFlag.IsMaster, "isMaster", true, "是否主区")
	flag.Int64Var(&gGameFlag.Port, "port", 0, "端口")
	flag.StringVar(&gGameFlag.LogPath, "logPath", "./logs/", "日志地址")
	flag.Parse()

}
