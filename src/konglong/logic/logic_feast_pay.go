package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func (mgr *FeastManager) payInit() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getPayCacheKey(eventArgs.RoleId))
	})
}

func (mgr *FeastManager) onFeastPay(ctx *data.Context) {

}

func (mgr *FeastManager) offFeastPay(ctx *data.Context) {

}

func (mgr *FeastManager) getPayCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FEAST_PAY_REPEAT, roleId)
}

func (mgr *FeastManager) RoleFeastPayBy(ctx *data.Context, roleId int64) *data.FeastPays {
	cacheKey := mgr.getPayCacheKey(roleId)

	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		d := &data.FeastPays{}
		var list1 []*data.FeastDone
		gSql.MustSelect(&list1, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Pay_Repeat)
		if len(list1) > 0 {
			d.Feast = list1[0]
		}
		return d, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	return rv.(*data.FeastPays)
}

func (mgr *FeastManager) feastPayAward(ctx *data.Context, roleId int64, fd *data.FeastDone, index int32) {
	ctx.ItemMustGive = true
	fcfg := config.GetFeastRepeatPayCfg(fd.CfgId)
	assert.Assert(fd.Done/fcfg.Pay > fd.AwardNum, "awarded")
	if len(fcfg.ChoiceReward) > 0 {
		petCfgId := fcfg.ChoiceReward[index-1]
		gPetMgr.GivePetFromCfgId(ctx, roleId, petCfgId, false, true, false, protocol.PetActionType_Pet_Action_Repeat_Pay, true)
	}
	if fcfg.Award != nil {
		gItemMgr.GiveAward(ctx, roleId, fcfg.Award, 1, protocol.ItemActionType_Action_Repeat_Pay)
	}
	gSql.Begin().Clean(roleId)
	fd.Awarded = true
	fd.AwardNum++
	gSql.MustExcel(gConst.SQL_UPDATE_FEAST_AWARD, fd.Awarded, fd.AwardNum, fd.Id)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFeastDoneMsg([]*protocol.FeastDone{
		{
			Id:          fd.Id,
			CfgId:       fd.CfgId,
			Kind:        fd.Kind,
			Done:        fd.Done,
			Awarded:     fd.Awarded,
			AwardAmount: fd.AwardNum,
		},
	}))
	ctx.ItemMustGive = false
}

func (mgr *FeastManager) onMallBuy(ctx *data.Context, roleId int64, pay int32, money int32) {
	fs := mgr.RoleFeastPayBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	if fs.Feast == nil {
		fs.Feast = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_REPEAT_PAY,
			Done:          pay,
			Awarded:       false,
			FightInfoJson: "[]",
			Kind:          protocol.FeastKind_Pay_Repeat,
		}
		fd := fs.Feast
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			fd.Id, fd.RoleId, fd.CfgId, fd.Kind, fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
	} else {
		fs.Feast.Done += pay
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fs.Feast.Done, fs.Feast.Awarded, fs.Feast.Id)
	}
	var pbList []*protocol.FeastDone
	pbList = append(pbList, fs.GetForClient()...)
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetFeastDoneMsg(nil, pbList))
}
