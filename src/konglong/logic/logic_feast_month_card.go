package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func (mgr *FeastManager) monthCardInit() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getMonthCardCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventRoleNewDay)
		f := mgr.RoleFeastMonthCardBy(ctx, args.RoleId)
		if f.Feast == nil {
			return
		}
		cfg := config.GetMonthCardFeastCfg(f.Feast.CfgId)
		if f.Feast.Done == f.Feast.AwardNum {
			return
		}

		now := time.Now()
		mailee := &data.Mailee{
			RoleId:   args.RoleId,
			Title:    config.GetGameTextCfg(cfg.DailyMailDesc[0]).Text,
			AwardStr: config.AwardToStr(cfg.DailyReward),
			Award:    cfg.DailyReward,
			Body:     config.GetGameTextCfg(cfg.DailyMailDesc[1]).Text,
			Time:     now,
			TimeOut:  now.Add(time.Hour * 24 * 7),
		}
		lastDay := time_helper.NowDay(args.LastSec)
		nowDay := time_helper.NowDay(args.NowSec)
		subDay := util.ChoiceInt64(f.Feast.Awarded, nowDay-lastDay-1, nowDay-lastDay)
		f.Feast.Awarded = false
		gived := false
		for i := int64(0); i < subDay; i++ {
			if f.Feast.Awarded || f.Feast.AwardNum >= f.Feast.Done {
				break
			}
			mailee.Name = fmt.Sprintf("月卡每日奖励_%d_%d", args.RoleId, args.NowSec-i)
			gMailMgr.SendMailee(ctx, mailee)
			f.Feast.AwardNum++
			gived = true
		}
		if f.Feast.Done-f.Feast.AwardNum == 0 {
			f.Feast.Awarded = true
			if gived {
				mailee.Name = fmt.Sprintf("月卡到期_%d_%d", args.RoleId, args.NowSec)
				mailee.Title = config.GetGameTextCfg(cfg.AlreadyOvertimeMailDesc[0]).Text
				mailee.Body = config.GetGameTextCfg(cfg.AlreadyOvertimeMailDesc[1]).Text
				mailee.AwardStr = ""
				mailee.Award = nil
				gMailMgr.SendMailee(ctx, mailee)
			}
		} else if f.Feast.Done-f.Feast.AwardNum == 3 {
			mailee.Name = fmt.Sprintf("月卡快到期_%d_%d", args.RoleId, args.NowSec)
			mailee.Title = config.GetGameTextCfg(cfg.ReadyOvertimeMailDesc[0]).Text
			mailee.Body = config.GetGameTextCfg(cfg.ReadyOvertimeMailDesc[1]).Text
			mailee.AwardStr = ""
			mailee.Award = nil
			gMailMgr.SendMailee(ctx, mailee)
		}
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_AWARD, f.Feast.Awarded, f.Feast.AwardNum, f.Feast.Id)
		TimeAfter(ctx, 0, func(ctx2 *data.Context) {
			gRoleMgr.Send(ctx2, args.RoleId, protocol.MakeNotifyFeastDoneMsg([]*protocol.FeastDone{
				{
					Id:          f.Feast.Id,
					CfgId:       f.Feast.CfgId,
					Kind:        f.Feast.Kind,
					Done:        f.Feast.Done,
					Awarded:     f.Feast.Awarded,
					AwardAmount: f.Feast.AwardNum,
				},
			}))
		}, ctx.Session)
	}, 100)
}

func (mgr *FeastManager) onMonthCardFeast(ctx *data.Context) {

}

func (mgr *FeastManager) offMonthCardFeast(ctx *data.Context) {

}

func (mgr *FeastManager) getMonthCardCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FEAST_MONTH_CARD, roleId)
}

func (mgr *FeastManager) onBuyMonthCard(ctx *data.Context, roleId int64) {
	fs := mgr.RoleFeastMonthCardBy(ctx, roleId)
	if fs.Feast == nil {
		fs.Feast = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_MONTH_CARD,
			Done:          30,
			Awarded:       false,
			FightInfoJson: "[]",
			Kind:          protocol.FeastKind_Month_Card,
		}
		fd := fs.Feast
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			fd.Id, fd.RoleId, fd.CfgId, fd.Kind, fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
	} else {
		fs.Feast.Done += 30
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fs.Feast.Done, fs.Feast.Awarded, fs.Feast.Id)
	}
	cfg := config.GetMonthCardFeastCfg(fs.Feast.CfgId)
	gItemMgr.GiveAward(ctx, roleId, cfg.BuyReward, 1, protocol.ItemActionType_Action_Month_Card)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFeastDoneMsg([]*protocol.FeastDone{
		{
			Id:          fs.Feast.Id,
			CfgId:       fs.Feast.CfgId,
			Kind:        fs.Feast.Kind,
			Done:        fs.Feast.Done,
			Awarded:     fs.Feast.Awarded,
			AwardAmount: fs.Feast.AwardNum,
		},
	}))
}

func (mgr *FeastManager) RoleFeastMonthCardBy(ctx *data.Context, roleId int64) *data.FeastMonthCards {
	cacheKey := mgr.getMonthCardCacheKey(roleId)

	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		d := &data.FeastMonthCards{}
		var list1 []*data.FeastDone
		gSql.MustSelect(&list1, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Month_Card)
		if len(list1) > 0 {
			d.Feast = list1[0]
		}
		return d, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	return rv.(*data.FeastMonthCards)
}

func (mgr *FeastManager) feastMonthCardAward(ctx *data.Context, roleId int64, fd *data.FeastDone, index int32) {
	assert.Assert(fd.Done-fd.AwardNum >= 0 && !fd.Awarded, "awarded")
	cfg := config.GetMonthCardFeastCfg(fd.CfgId)
	gItemMgr.GiveAward(ctx, roleId, cfg.DailyReward, 1, protocol.ItemActionType_Action_Month_Card)
	fd.AwardNum++
	fd.Awarded = true
	gSql.MustExcel(gConst.SQL_UPDATE_FEAST_AWARD, fd.Awarded, fd.AwardNum, fd.Id)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFeastDoneMsg([]*protocol.FeastDone{
		{
			Id:          fd.Id,
			CfgId:       fd.CfgId,
			Kind:        fd.Kind,
			Done:        fd.Done,
			Awarded:     fd.Awarded,
			AwardAmount: fd.AwardNum,
		},
	}))
}
