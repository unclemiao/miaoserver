package logic

import (
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/protocol"
)

type ParalManager struct {
}

func init() {
	gParal = &ParalManager{}
}

func (mgr *ParalManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_WinParal, mgr.HandlerWinParal)
	gEventMgr.When(protocol.EnvelopeType_C2S_LoseParal, mgr.HandlerLoseParal)

}

func (mgr *ParalManager) HandlerWinParal(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg   = msg.GetC2SWinParal()
		paralCfg = config.GetParalCfg(repMsg.ParalCfgId)
		roleId   = ctx.RoleId
	)

	gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Paral)

	// gItemMgr.GiveAward(ctx, roleId, paralCfg.Award, 1, protocol.ItemActionType_Action_Fuben)

	randAward := &data.AwardCfg{
		Why: paralCfg.RandAward.Why,
	}
	var maxAmount *data.ItemAmount
	for _, awardCfg := range paralCfg.RandAward.AwardList {
		if maxAmount == nil || awardCfg.Rate > maxAmount.Rate {
			maxAmount = awardCfg
		}
		if util.RandomMatched(awardCfg.Rate * 10) {
			randAward.AwardList = append(randAward.AwardList, awardCfg)
			if awardCfg.N < 0 {
				randAward.TotalPetNum++
			}
		}
	}
	if len(randAward.AwardList) == 0 && maxAmount != nil {
		randAward.AwardList = append(randAward.AwardList, maxAmount)
		if maxAmount.N < 0 {
			randAward.TotalPetNum++
		}
	}
	config.AwardCfgAdd(randAward, paralCfg.Award, 1)
	gItemMgr.GiveAward(ctx, roleId, randAward, 1, protocol.ItemActionType_Action_Fuben)
}

func (mgr *ParalManager) HandlerLoseParal(args *data.Context, msg *protocol.Envelope) {

}
