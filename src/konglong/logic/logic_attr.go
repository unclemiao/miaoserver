package logic

import (
	"math"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/protocol"
)

type AttrManager struct {
}

func init() {
	gAttrMgr = &AttrManager{}
}

func (mgr *AttrManager) Init() {

}

func (mgr *AttrManager) totalAttr(attrCfgList []*data.AttrCfg) *data.AttrCfg {
	totalAttr := &data.AttrCfg{
		ElemList: make([]*protocol.AttrElem, protocol.AttrType_Attr_AmountMax+1),
	}
	for _, attrCfg := range attrCfgList {
		for _, attr := range attrCfg.ElemList {
			if totalAttr.ElemList[attr.AttrType] == nil {
				totalAttr.ElemList[attr.AttrType] = &protocol.AttrElem{
					AttrType: attr.AttrType,
				}
			}
			totalAttr.ElemList[attr.AttrType].Value += attr.Value
		}
	}
	return totalAttr
}

func (mgr *AttrManager) addAttr(toAttr *data.AttrCfg, from *data.AttrCfg) {
	for _, attr := range from.ElemList {
		if toAttr.ElemList[attr.AttrType] == nil {
			toAttr.ElemList[attr.AttrType] = &protocol.AttrElem{
				AttrType: attr.AttrType,
			}
		}
		toAttr.ElemList[attr.AttrType].Value += attr.Value
	}
}

func (mgr *AttrManager) applyAttr(attr *data.Attr, from *data.AttrCfg) {
	if from.ElemList[protocol.AttrType_Attr_Atk] != nil {
		attr.Atk += int32(from.ElemList[protocol.AttrType_Attr_Atk].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_AtkTime] != nil {
		attr.AtkTime += int32(from.ElemList[protocol.AttrType_Attr_AtkTime].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_Crit] != nil {
		attr.Crit += int32(from.ElemList[protocol.AttrType_Attr_Crit].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_Def] != nil {
		attr.Def += int32(from.ElemList[protocol.AttrType_Attr_Def].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_Dis] != nil {
		attr.Dis += int32(from.ElemList[protocol.AttrType_Attr_Dis].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_Dodge] != nil {
		attr.Dodge += int32(from.ElemList[protocol.AttrType_Attr_Dodge].Value)
	}

	if from.ElemList[protocol.AttrType_Attr_HpMax] != nil {
		attr.HpMax += from.ElemList[protocol.AttrType_Attr_HpMax].Value
	}

	attr.Hp = attr.HpMax
}

func addAttr(ctx *data.Context, attr *data.Attr, from *data.Attr) {
	attr.HpMax += from.HpMax
	attr.Atk += from.Atk
	attr.Def += from.Def
	attr.Crit += from.Crit
	attr.Dodge += from.Dodge
	attr.Dis += from.Dis
	attr.AtkTime = util.MaxInt32(attr.AtkTime-from.AtkTime, 1500)
}

// 宠物重算属性
func (mgr *AttrManager) PetAttrBy(ctx *data.Context, pet *data.Pet, remote bool) {
	if pet.Attr == nil {
		pet.Attr = &data.Attr{}
	}
	cfg := config.GetPetCfg(pet.CfgId)
	starCfg := config.PetStarCfgBy(pet.CfgId, pet.Star)
	var hpRate, atkRate, defRate float64
	var hpAdd int64
	var atkAdd, defAdd int32

	// 进化次数
	var evolutionLv int32
	if pet.Evolution < 0 {
		evolutionLv = (-pet.Evolution) - 1
	} else {
		evolutionLv = pet.Evolution
	}

	// 攻击
	atkBase := float64(cfg.Atk) + float64(pet.AtkAdd)*float64(pet.Level)*0.1 + 50
	atkRate = float64(starCfg.Atk)/1000.0 + float64(evolutionLv)*0.05

	// 防御
	defBase := float64(cfg.Def) + float64(pet.DefAdd)*float64(pet.Level)*0.08 + 30
	defRate = float64(starCfg.Def)/1000.0 + float64(evolutionLv)*0.05

	// 血量上限
	hp9Base := float64(cfg.HpMax) + float64(pet.HpMaxAdd)*float64(pet.Level)*0.62 + 200
	hpRate = float64(starCfg.Hp)/1000.0 + float64(evolutionLv)*0.05

	pet.Attr.Crit = cfg.Crit
	pet.Attr.Dodge = cfg.Dodge
	pet.Attr.Dis = cfg.Dis
	pet.Attr.AtkTime = cfg.AtkTime

	// 羁绊加成
	_, fetterCfgList := gManualMgr.FettersByPetCfgId(ctx, pet.RoleId, pet.CfgId, true)
	for _, fetterCfg := range fetterCfgList {
		if ok, _ := gStubMgr.InStub(ctx, pet.RoleId, pet.Id); ok {
			atkAdd += fetterCfg.Atk
			defAdd += fetterCfg.Def
			hpAdd += fetterCfg.Hp

			hpRate += float64(fetterCfg.HpRate) / 1000.0
			atkRate += float64(fetterCfg.AtkRate) / 1000.0
			defRate += float64(fetterCfg.DefRate) / 1000.0
		}

	}

	// 超进化解锁加成
	for _, super := range gPetSuperEvolutionMgr.RolePetSuperEvolutionBy(ctx, pet.RoleId).PetSuperEvolutionMap {
		if ok, _ := gStubMgr.InStubNotHelp(ctx, pet.RoleId, pet.Id); ok {
			superCfg := config.GetPetSuperEvolutionUnlockAttrCfg(super.CfgId, super.Progress)
			atkAdd += superCfg.Atk
			defAdd += superCfg.Def
			hpAdd += superCfg.Hp

			hpRate += float64(superCfg.HpRate) / 1000.0
			atkRate += float64(superCfg.AtkRate) / 1000.0
			defRate += float64(superCfg.DefRate) / 1000.0
		}
	}

	// 超进化加成
	if pet.SuperEvolution {
		atkAdd += cfg.SuperEvolutionAtk
		defAdd += cfg.SuperEvolutionDef
		hpAdd += cfg.SuperEvolutionHp

		hpRate += float64(cfg.SuperEvolutionHpRate) / 1000.0
		atkRate += float64(cfg.SuperEvolutionAtkRate) / 1000.0
		defRate += float64(cfg.SuperEvolutionDefRate) / 1000.0

		pet.Attr.Crit += cfg.SuperEvolutionCrit
		pet.Attr.Dodge += cfg.SuperEvolutionDodge
		pet.Attr.Dis += cfg.SuperEvolutionDis
		pet.Attr.AtkTime = util.MaxInt32(pet.Attr.AtkTime-cfg.SuperEvolutionAtkTime, 1500)
	}

	// 属性最终值
	pet.Attr.Atk = util.MaxInt32(1, int32(atkBase+atkBase*atkRate)+atkAdd)
	pet.Attr.Def = util.MaxInt32(1, int32(defBase+defBase*defRate)+defAdd)
	pet.Attr.HpMax = util.MaxInt64(1, int64(hp9Base+hp9Base*hpRate)+hpAdd)

	if remote {
		gRoleMgr.Send(ctx, pet.RoleId, protocol.MakeNotifyPetUpdateMsg(nil, []*protocol.Pet{pet.GetForClient()}, nil, protocol.PetActionType_Pet_Action_Unknow))
	}
}

// 角色重算属性
func (mgr *AttrManager) RoleAttrBy(ctx *data.Context, role *data.Role, remote bool) {
	if role.Attr == nil {
		role.Attr = &data.Attr{}
	}

	// base
	var (
		atkBase, atkAdd     int32
		atkTimeBase         int32
		critBase, critAdd   int32
		defBase, defAdd     int32
		disBase, disAdd     int32
		dodgeBase, dodgeAdd int32
		hpMaxBase, hpMaxAdd int64
		atkRate             float64
		defRate             float64
		hpMaxRate           float64
		critRate            float64
		dodgeRate           float64
	)
	// 计算等级
	lvCfg := config.GetRoleLevelCfg(role.Level)
	atkBase += lvCfg.Atk
	atkTimeBase += lvCfg.AtkTime
	critBase += lvCfg.Crit
	defBase += lvCfg.Def
	disBase += lvCfg.Dis
	dodgeBase += lvCfg.Dodge
	hpMaxBase += lvCfg.HpMax

	// 计算装备
	equipList := gItemMgr.ItemListByKit(ctx, role.RoleId, protocol.KitType_Kit_At_Role)
	for _, equip := range equipList {
		if equip == nil {
			continue
		}

		if equip.Star > 0 {
			starCfg := config.GetEquipStarCfg(equip.Cid, equip.Star)
			gradeCfg := config.GetEquipGradeCfg(int32(equip.Cid))

			hpMaxAdd += equip.HpMaxBase + int64(float64(equip.HpMaxBase)*(float64(starCfg.HpRate)/1000.0)*(float64(gradeCfg.HpRate)/1000.0))
			atkAdd += equip.AtkBase + int32(float64(equip.AtkBase)*(float64(starCfg.AtkRate)/1000.0)*(float64(gradeCfg.AtkRate)/1000.0))
			defAdd += equip.DefBase + int32(float64(equip.DefBase)*(float64(starCfg.DefRate)/1000.0)*(float64(gradeCfg.DefRate)/1000.0))
		} else {
			hpMaxAdd += equip.HpMaxBase
			atkAdd += equip.AtkBase
			defAdd += equip.DefBase
		}

		// 计算随机属性
		if equip.RandAttr != nil {
			hpMaxAdd += equip.RandAttr.HpMax
			atkAdd += equip.RandAttr.Atk
			defAdd += equip.RandAttr.Def
			critAdd += equip.RandAttr.Crit
			dodgeAdd += equip.RandAttr.Dodge
			disAdd += equip.RandAttr.Dis
			atkTimeBase = util.MaxInt32(atkTimeBase-equip.RandAttr.AtkTime, 1500)
		}
	}

	// 计算乘骑
	if role.Horse > 0 {
		horse := gPetMgr.PetBy(ctx, role.RoleId, role.Horse)
		horseStarCfg := config.PetStarCfgBy(horse.CfgId, horse.Star)

		if horse.Attr == nil {
			logs.Warn("pet attr == nil")
			mgr.PetAttrBy(ctx, horse, true)
		}

		hpMaxAdd += int64(float64(horse.Attr.HpMax) * (float64(horseStarCfg.HorseAttrRate) / 1000.0))
		atkAdd += int32(float64(horse.Attr.Atk) * (float64(horseStarCfg.HorseAttrRate) / 1000.0))
		defAdd += int32(float64(horse.Attr.Def) * (float64(horseStarCfg.HorseAttrRate) / 1000.0))
	}

	// 时装
	roleFashion := gFashionMgr.RoleFashionBy(ctx, role.RoleId)
	for _, f := range roleFashion.FashionMap {
		if f != nil {
			cfg := config.GetFashionCfg(f.CfgId)
			hpMaxAdd += cfg.HpMax
			atkAdd += cfg.Atk
			defAdd += cfg.Def
			atkRate += float64(cfg.AtkRate) / 1000.0
			defRate += float64(cfg.DefRate) / 1000.0
			hpMaxRate += float64(cfg.HpMaxRate) / 1000.0
		}
	}
	role.Attr.HpMax = hpMaxBase + int64(math.Ceil(float64(hpMaxBase)*hpMaxRate)) + hpMaxAdd
	role.Attr.Atk = atkBase + int32(math.Ceil(float64(atkBase)*atkRate)) + atkAdd
	role.Attr.Def = defBase + int32(math.Ceil(float64(defBase)*defRate)) + defAdd

	role.Attr.AtkTime = atkTimeBase
	role.Attr.Crit = critBase + int32(math.Ceil(float64(critBase)*critRate)) + critAdd
	role.Attr.Dodge = dodgeBase + int32(math.Ceil(float64(dodgeBase)*dodgeRate)) + dodgeAdd
	role.Attr.Dis = disBase + disAdd

	if remote {
		gRoleMgr.Send(ctx, role.RoleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
	}
}
