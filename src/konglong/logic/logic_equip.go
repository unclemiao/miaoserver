package logic

import (
	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func (mgr *ItemManager) InitEquipMgr() {
	gEventMgr.When(protocol.EnvelopeType_C2S_LoadEquip, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			req = msg.GetC2SLoadEquip()
		)
		if req.KitType == protocol.KitType_Kit_At_Role {
			mgr.LoadEquip(ctx, ctx.RoleId, req.FromEquipId, true)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_EquipStar, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SEquipStar()
		)
		mgr.Star(ctx, ctx.RoleId, rep.ToEquipId, rep.SelfEquipIdList, rep.StuffEquipIdList)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_EquipInherit, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SEquipInherit()
		)
		mgr.Inherit(ctx, ctx.RoleId, rep.FromEquipId, rep.ToEquipId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_EquipResolve, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SEquipResolve()
		)
		mgr.Resolve(ctx, ctx.RoleId, rep.GetEquipIdList(), float64(util.ChoiceInt32(len(msg.AdSdkToken) > 0, 5, 1)))
	})

}

func (mgr *ItemManager) initEquip(ctx *data.Context, roleId int64, item *data.Item) *data.Item {
	itemCfg := config.GetItemCfg(item.Cid)
	gradeCfg := config.GetEquipGradeCfg(int32(item.Cid))

	item.Grade = gradeCfg.Grade
	item.HpMaxBase = util.CalcBonusRateUnit64(itemCfg.HpMaxBase, util.Random32(gradeCfg.HpAddRate[0], gradeCfg.HpAddRate[1]), 1000)
	item.AtkBase = util.CalcBonusRateUnit(itemCfg.AtkBase, util.Random32(gradeCfg.AtkAddRate[0], gradeCfg.AtkAddRate[1]), 1000)
	item.DefBase = util.CalcBonusRateUnit(itemCfg.DefBase, util.Random32(gradeCfg.DefAddRate[0], gradeCfg.DefAddRate[1]), 1000)
	if gradeCfg.SkillDropId > 0 {
		item.SkillCfgId = config.GetRandomSkillCfg(gradeCfg.SkillDropId).SkillId
	}
	item.RandAttr = config.RandomAttr(int32(item.Cid), gradeCfg.Grade)
	item.RandAttrStr = config.AttrToStr(item.RandAttr)
	return item
}

func (mgr *ItemManager) LoadEquip(ctx *data.Context, roleId int64, equipId int64, remote bool) {
	equip := mgr.ItemBy(ctx, roleId, equipId)
	assert.Assert(equip.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
	equipCfg := config.GetItemCfg(equip.Cid)
	equipBagList := mgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Role_Equip)
	roleEquipList := mgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_At_Role)
	var updateList []*protocol.Item

	gSql.Begin().Clean(roleId)

	oldEquip := roleEquipList[equipCfg.SubKind]
	if oldEquip != nil {
		// 原部位有装备则替换
		equipBagList[equip.Grid-1] = oldEquip

		oldEquip.Grid = equip.Grid
		oldEquip.OwnerId = roleId
		oldEquip.Kit = protocol.KitType_Kit_Role_Equip

		gSql.MustExcel(gConst.SQL_UPDATE_ITEM_FOR_EQUIP, oldEquip.Kit, oldEquip.Grid, oldEquip.OwnerId, oldEquip.Id)
		updateList = append(updateList, mgr.GetForClient(oldEquip))

	} else {
		equipBagList[equip.Grid-1] = nil
	}

	roleEquipList[equipCfg.SubKind] = equip
	equip.Grid = int32(equipCfg.SubKind)
	equip.OwnerId = roleId
	equip.Kit = protocol.KitType_Kit_At_Role
	updateList = append(updateList, mgr.GetForClient(equip))

	gSql.MustExcel(gConst.SQL_UPDATE_ITEM_FOR_EQUIP, equip.Kit, equip.Grid, equip.OwnerId, equip.Id)

	gAttrMgr.RoleAttrBy(ctx, gRoleMgr.RoleBy(ctx, roleId), remote)

	if remote {
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyItemUpdate(nil, updateList, nil, protocol.ItemActionType_Action_Load))
	}
}

// 升星
func (mgr *ItemManager) Star(ctx *data.Context, roleId int64, toItemId int64, SelfIdList []int64, StuffIdList []int64) {
	item := mgr.ItemBy(ctx, roleId, toItemId)
	assert.Assert(item.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
	itemCfg := config.GetItemCfg(item.Cid)
	starCfg := config.GetEquipStarCfg(itemCfg.CfgId, item.Star+1)
	role := gRoleMgr.RoleBy(ctx, roleId)
	checkArgs := &data.CheckEquipStar{}

	for _, id := range SelfIdList {
		assert.Assert(id != toItemId, "can not stuff self")
		delItem := mgr.ItemBy(ctx, roleId, id)
		assert.Assert(delItem.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
		assert.Assert(delItem.Cid == item.Cid, "not the same equip, toCid:%d, stuffCid:%d", item.Cid, delItem.Cid)
		assert.Assert(delItem.Grade == item.Grade, "not the same grade")
		mgr.TakeItem(ctx, roleId, delItem, delItem.Cid, delItem.N, protocol.ItemActionType_Action_Star_Equip)
	}
	assert.Assert(starCfg.SelfNum == int32(len(SelfIdList)))

	for _, id := range StuffIdList {
		assert.Assert(id != toItemId, "can not stuff self")
		delItem := mgr.ItemBy(ctx, roleId, id)
		assert.Assert(delItem.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
		mgr.TakeItem(ctx, roleId, delItem, delItem.Cid, delItem.N, protocol.ItemActionType_Action_Star_Equip)
		if delItem.Grade == 1 {
			checkArgs.CGradeNum++
		} else if delItem.Grade == 2 {
			checkArgs.BGradeNum++
		} else if delItem.Grade == 3 {
			checkArgs.AGradeNum++
		} else if delItem.Grade == 4 {
			checkArgs.SGradeNum++
		}
	}

	assert.Assert(checkArgs.CGradeNum == starCfg.CGradeNum &&
		checkArgs.BGradeNum == starCfg.BGradeNum &&
		checkArgs.AGradeNum == starCfg.AGradeNum &&
		checkArgs.SGradeNum == starCfg.SGradeNum,
		"invalid from pet stuff num not enough",
	)

	gSql.Begin().Clean(roleId)
	gSql.MustExcel(gConst.SQL_UPDATE_ITEM_STAR, item.Star+1, item.Id)
	item.Star += 1
	if item.Kit == protocol.KitType_Kit_At_Role {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyItemUpdate(nil, []*protocol.Item{mgr.GetForClient(item)}, nil, protocol.ItemActionType_Action_Star_Equip))
}

// 装备继承
func (mgr *ItemManager) Inherit(ctx *data.Context, roleId int64, fromId int64, toId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	fromIt := mgr.ItemBy(ctx, roleId, fromId)
	toIt := mgr.ItemBy(ctx, roleId, toId)
	gSql.Begin().Clean(roleId)
	assert.Assert(fromIt.Kit != protocol.KitType_Kit_Dustbin && toIt.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
	assert.Assert(fromIt.Grade == toIt.Grade, "not the same grade")

	fromStar := fromIt.Star
	fromIt.Star = toIt.Star
	toIt.Star = fromStar

	if fromIt.Kit == protocol.KitType_Kit_At_Role ||
		toIt.Kit == protocol.KitType_Kit_At_Role {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}
	gSql.MustExcel(gConst.SQL_UPDATE_ITEM_STAR, fromIt.Star, fromIt.Id)
	gSql.MustExcel(gConst.SQL_UPDATE_ITEM_STAR, toIt.Star, toIt.Id)

	updatList := []*protocol.Item{mgr.GetForClient(fromIt), mgr.GetForClient(toIt)}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyItemUpdate(nil, updatList, nil, protocol.ItemActionType_Action_Equip_Inherit))
	gRoleMgr.Send(ctx, roleId, protocol.MakeEquipInheritDoneMsg(ctx.EventMsg.GetEnvMsg(), fromId, toId))

}

// 回收
func (mgr *ItemManager) Resolve(ctx *data.Context, roleId int64, fromIdList []int64, rate float64) {
	gSql.Begin().Clean(roleId)
	award := &data.AwardCfg{}
	for _, id := range fromIdList {
		it := mgr.ItemBy(ctx, roleId, id)
		assert.Assert(it.Kit != protocol.KitType_Kit_Dustbin, "item in dustbin")
		resolveCfg := config.GetEquipResolveCfg(it.Star)
		assert.Assert(resolveCfg != nil, "item can not resolve, id:%d", id)
		config.AwardCfgAdd(award, resolveCfg.Reward, rate)
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Equip_Resolve)
	}
	gItemMgr.GiveAward(ctx, roleId, award, 1, protocol.ItemActionType_Action_Equip_Resolve)
}
