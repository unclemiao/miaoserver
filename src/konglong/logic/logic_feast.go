package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type FeastManager struct {
	cache    *cache.ShardedCache
	rankInfo *data.FeastPetKingRank

	LimitTimeFeastOpen bool
}

func init() {
	gFeastMgr = &FeastManager{
		cache:              cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Minute, 2^4),
		LimitTimeFeastOpen: false,
	}
}

func (mgr *FeastManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getLimitTimeCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastDone, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.GetFeastDoneList(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_FeastAward, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SFeastAward()
		)
		mgr.FeastAward(ctx, ctx.RoleId, rep.Id, rep.ChoiceIndex)
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		TimeAfter(nil, 0, func(ctx2 *data.Context) {
			mgr.NotifyFeast(ctx2, args.RoleId)
		}, ctx.Session)
	}, 0)

	mgr.fastRankInit()

	mgr.petKingInit()

	mgr.payInit()

	mgr.monthCardInit()

}

func (mgr *FeastManager) OnLetteryPet(ctx *data.Context, roleId int64, petList []*data.Pet) {
	if !mgr.LimitTimeFeastOpen {
		return
	}
	fd := mgr.LimitTimeFeastDoneBy(ctx, roleId, gConst.FEAST_CFG_ID_LOTTERY_PET)
	fcfg := config.GetLimitTimeFeastCfg(gConst.FEAST_CFG_ID_LOTTERY_PET)
	isNew := false
	needUpdate := false
	needRemote := false
	ltf := mgr.LimitTimeFeastBy(ctx, roleId)

	if fd == nil {
		fd = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_LOTTERY_PET,
			Done:          0,
			Awarded:       false,
			FightInfoJson: "[]",
		}

		ltf.FeastDoneList = append(ltf.FeastDoneList, fd)
		isNew = true
	}

	if fd.Done < fcfg.Amount {
		fd.Done = util.MinInt32(fcfg.Amount, fd.Done+int32(len(petList)))
		needUpdate = true
	}
	if isNew {
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			fd.Id, fd.RoleId, fd.CfgId, fd.Kind, fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
		needRemote = true
	}
	if needUpdate {
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fd.Done, fd.Awarded, fd.Id)
		needRemote = true
	}
	if needRemote {
		pbList := make([]*protocol.FeastDone, 0, len(ltf.FeastDoneList))
		pbList = append(pbList, ltf.GetForClient(gTimeFeastMgr.TimeFeastBy(ctx, roleId))...)
		gRoleMgr.Send(ctx, roleId, protocol.MakeGetFeastDoneMsg(ctx.EventMsg.GetEnvMsg(), pbList))
	}

}

func (mgr *FeastManager) GetFeastDoneList(ctx *data.Context, roleId int64) {
	tf := gTimeFeastMgr.TimeFeastBy(ctx, roleId)
	ltf := mgr.getLimitTimeFeast(ctx, roleId)
	frf := mgr.FastRankFeastBy(ctx, roleId)
	rprf := mgr.RoleFeastPayBy(ctx, roleId)
	mcrf := mgr.RoleFeastMonthCardBy(ctx, roleId)
	pbList := make([]*protocol.FeastDone, 0, len(ltf.FeastDoneList)+len(frf.FeastDoneList)+1)
	pbList = append(pbList, ltf.GetForClient(tf)...)
	pbList = append(pbList, frf.GetForClient()...)
	pbList = append(pbList, rprf.GetForClient()...)
	pbList = append(pbList, mcrf.GetForClient()...)
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetFeastDoneMsg(ctx.EventMsg.GetEnvMsg(), pbList))
}

func (mgr *FeastManager) NotifyFeast(ctx *data.Context, roleId int64) {
	var fs []*protocol.Feast
	server := gServer.Node
	// if mgr.LimitTimeFeastOpen {
	// 	fs = append(fs, &protocol.Feast{
	// 		Kind:      protocol.FeastKind_Limit_Time_Lottery_Pet,
	// 		BeginTime: server.BeginTime,
	// 		EndTime:   0,
	// 	})
	// 	fs = append(fs, &protocol.Feast{
	// 		Kind:      protocol.FeastKind_Limit_Time_Online,
	// 		BeginTime: server.BeginTime,
	// 		EndTime:   0,
	// 	})
	// } else {
	fs = append(fs, &protocol.Feast{
		Kind:      protocol.FeastKind_Newbie_Gift_Free_Pet,
		BeginTime: server.BeginTime,
		EndTime:   0,
	})
	// 	fs = append(fs, &protocol.Feast{
	// 		Kind:      protocol.FeastKind_Newbie_Gift_Online,
	// 		BeginTime: server.BeginTime,
	// 		EndTime:   0,
	// 	})
	// }

	beginTime := time.Unix(server.BeginTime, 0)
	midnight := time.Date(beginTime.Year(), beginTime.Month(), beginTime.Day(), 0, 0, 0, 0, time.Local)
	fs = append(fs, &protocol.Feast{
		Kind:      protocol.FeastKind_Fast_Rank_Three,
		BeginTime: server.BeginTime,
		EndTime:   midnight.Unix() + time_helper.Day*3 - time_helper.Hour*2,
	})

	fs = append(fs, &protocol.Feast{
		Kind:      protocol.FeastKind_Fast_Rank_Seven,
		BeginTime: server.BeginTime,
		EndTime:   midnight.Unix() + time_helper.Day*7 - time_helper.Hour*2,
	})

	fs = append(fs, &protocol.Feast{
		Kind:      protocol.FeastKind_Fast_Rank_Fifteen,
		BeginTime: server.BeginTime,
		EndTime:   midnight.Unix() + time_helper.Day*15,
	})

	// 累充活动
	fs = append(fs, &protocol.Feast{
		Kind:      protocol.FeastKind_Pay_Repeat,
		BeginTime: server.BeginTime,
		EndTime:   0,
	})

	// 月卡活动
	// fs = append(fs, &protocol.Feast{
	// 	Kind:      protocol.FeastKind_Month_Card,
	// 	BeginTime: server.BeginTime,
	// 	EndTime:   0,
	// })

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFeastMsg(fs))
}

func (mgr *FeastManager) FeastDoneBy(ctx *data.Context, roleId int64, id int64) *data.FeastDone {
	ltf := mgr.getLimitTimeFeast(ctx, roleId)
	for _, fd := range ltf.FeastDoneList {
		if fd.Id == id {
			return fd
		}
	}
	frf := mgr.FastRankFeastBy(ctx, roleId)
	for _, fd := range frf.FeastDoneList {
		if fd.Id == id {
			return fd
		}
	}
	rpf := mgr.RoleFeastPayBy(ctx, roleId)
	if rpf != nil && rpf.Feast != nil && rpf.Feast.Id == id {
		return rpf.Feast
	}
	mcf := mgr.RoleFeastMonthCardBy(ctx, roleId)
	if mcf != nil && mcf.Feast != nil && mcf.Feast.Id == id {
		return mcf.Feast
	}

	return nil
}

func (mgr *FeastManager) FeastAward(ctx *data.Context, roleId int64, id int64, index int32) {
	fd := mgr.FeastDoneBy(ctx, roleId, id)
	switch fd.Kind {
	case protocol.FeastKind_Limit_Time_Lottery_Pet,
		protocol.FeastKind_Limit_Time_Online,
		protocol.FeastKind_Newbie_Gift_Free_Pet,
		protocol.FeastKind_Newbie_Gift_Online:
		mgr.limitTimeFeastAward(ctx, roleId, fd, index)
	case protocol.FeastKind_Fast_Rank_Three,
		protocol.FeastKind_Fast_Rank_Seven,
		protocol.FeastKind_Fast_Rank_Fifteen:
		mgr.fastRankFeastAward(ctx, roleId, fd, index)
	case protocol.FeastKind_Pay_Repeat:
		mgr.feastPayAward(ctx, roleId, fd, index)
	case protocol.FeastKind_Month_Card:
		mgr.feastMonthCardAward(ctx, roleId, fd, index)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeFeastAwardDoneMsg(ctx.EventMsg.GetEnvMsg(), id))
}

// ************************** 限时 活动 **************************

func (mgr *FeastManager) getLimitTimeCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FEAST_LIMIT_TIME, roleId)
}

func (mgr *FeastManager) LimitTimeFeastBy(ctx *data.Context, roleId int64) *data.FeastLimitTime {
	cacheKey := mgr.getLimitTimeCacheKey(roleId)

	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		d := &data.FeastLimitTime{}
		var list1, list2, list3, list4 []*data.FeastDone
		gSql.MustSelect(&list1, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Limit_Time_Lottery_Pet)
		gSql.MustSelect(&list2, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Limit_Time_Online)
		gSql.MustSelect(&list3, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Newbie_Gift_Free_Pet)
		gSql.MustSelect(&list4, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Newbie_Gift_Online)
		if len(list1) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, list1[0])
		}
		if len(list2) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, list2[0])
		}
		if len(list3) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, list3[0])
		}
		if len(list4) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, list4[0])
		}
		return d, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	return rv.(*data.FeastLimitTime)
}

func (mgr *FeastManager) LimitTimeFeastDoneBy(ctx *data.Context, roleId int64, cfgId int32) *data.FeastDone {
	ltf := mgr.LimitTimeFeastBy(ctx, roleId)
	for _, fd := range ltf.FeastDoneList {
		if fd.CfgId == cfgId {
			return fd
		}
	}
	return nil
}

func (mgr *FeastManager) getLimitTimeFeast(ctx *data.Context, roleId int64) *data.FeastLimitTime {
	ltf := mgr.LimitTimeFeastBy(ctx, roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)

	petfd := mgr.LimitTimeFeastDoneBy(ctx, roleId, gConst.FEAST_CFG_ID_LOTTERY_PET)
	if petfd == nil && mgr.LimitTimeFeastOpen {
		petfd = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_LOTTERY_PET,
			Kind:          protocol.FeastKind_Limit_Time_Lottery_Pet,
			Done:          0,
			Awarded:       false,
			FightInfoJson: "[]",
		}
		gSql.Begin().Clean(roleId)
		ltf.FeastDoneList = append(ltf.FeastDoneList, petfd)
		gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			petfd.Id, petfd.RoleId, petfd.CfgId, petfd.Kind, petfd.Done, petfd.Awarded, petfd.FightInfoJson, petfd.UpdateTime)
	}

	freePetfd := mgr.LimitTimeFeastDoneBy(ctx, roleId, gConst.FEAST_CFG_ID_NEWBIE_GIFT_PET)
	if freePetfd == nil && !mgr.LimitTimeFeastOpen {
		freePetfd = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_NEWBIE_GIFT_PET,
			Kind:          protocol.FeastKind_Newbie_Gift_Free_Pet,
			Done:          0,
			Awarded:       false,
			FightInfoJson: "[]",
		}
		gSql.Begin().Clean(roleId)
		ltf.FeastDoneList = append(ltf.FeastDoneList, freePetfd)
		gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			freePetfd.Id, freePetfd.RoleId, freePetfd.CfgId, freePetfd.Kind, freePetfd.Done,
			freePetfd.Awarded, freePetfd.FightInfoJson, freePetfd.UpdateTime)
	}

	onlinefd := mgr.LimitTimeFeastDoneBy(ctx, roleId, gConst.FEAST_CFG_ID_ROLE_ONLINE)
	if onlinefd == nil && mgr.LimitTimeFeastOpen {

		onlinefd = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			RoleId:        roleId,
			CfgId:         gConst.FEAST_CFG_ID_ROLE_ONLINE,
			Awarded:       false,
			Kind:          protocol.FeastKind_Limit_Time_Online,
			Done:          util.MinInt32(config.GetLimitTimeFeastCfg(gConst.FEAST_CFG_ID_ROLE_ONLINE).Amount, int32(gTimeFeastMgr.TimeFeastBy(ctx, roleId).Sec)),
			FightInfoJson: "[]",
		}

		gSql.Begin().Clean(roleId)
		ltf.FeastDoneList = append(ltf.FeastDoneList, onlinefd)
		gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE,
			onlinefd.Id, onlinefd.RoleId, onlinefd.CfgId, onlinefd.Kind, onlinefd.Done, onlinefd.Awarded, onlinefd.FightInfoJson, onlinefd.UpdateTime)
	} else if mgr.LimitTimeFeastOpen {
		gSql.Begin().Clean(roleId)
		newDone := util.MinInt32(config.GetLimitTimeFeastCfg(gConst.FEAST_CFG_ID_ROLE_ONLINE).Amount, int32(gTimeFeastMgr.TimeFeastBy(ctx, roleId).Sec))
		if newDone != onlinefd.Done {
			onlinefd.Done = newDone
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_FEAST_DONE, onlinefd.Done, onlinefd.Awarded, onlinefd.Id)
		}
	}

	onlineGiftfd := mgr.LimitTimeFeastDoneBy(ctx, roleId, gConst.FEAST_CFG_ID_NEWBIE_GIFT_ONLINE)
	if role.GamePass >= 6 && !mgr.LimitTimeFeastOpen {
		if onlineGiftfd == nil {

			onlineGiftfd = &data.FeastDone{
				Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
				RoleId:        roleId,
				CfgId:         gConst.FEAST_CFG_ID_NEWBIE_GIFT_ONLINE,
				Awarded:       false,
				Kind:          protocol.FeastKind_Newbie_Gift_Online,
				Done:          int32(gTimeFeastMgr.TimeFeastBy(ctx, roleId).Sec),
				FightInfoJson: "[]",
			}

			gSql.Begin().Clean(roleId)
			ltf.FeastDoneList = append(ltf.FeastDoneList, onlineGiftfd)
			gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE,
				onlineGiftfd.Id, onlineGiftfd.RoleId, onlineGiftfd.CfgId, onlineGiftfd.Kind, onlineGiftfd.Done,
				onlineGiftfd.Awarded, onlineGiftfd.FightInfoJson, onlineGiftfd.UpdateTime)
		} else {
			gSql.Begin().Clean(roleId)
			newDone := int32(gTimeFeastMgr.TimeFeastBy(ctx, roleId).Sec)
			if newDone < onlineGiftfd.Done {
				onlineGiftfd.Done = newDone
				gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_FEAST_DONE, onlineGiftfd.Done, onlineGiftfd.Awarded, onlineGiftfd.Id)
			}
		}
	}

	return ltf
}

func (mgr *FeastManager) limitTimeFeastAward(ctx *data.Context, roleId int64, fd *data.FeastDone, index int32) {
	fcfg := config.GetLimitTimeFeastCfg(fd.CfgId)
	if fcfg.Kind == protocol.FeastKind_Newbie_Gift_Online {
		done := int32(gTimeFeastMgr.TimeFeastBy(ctx, roleId).Sec) - fd.Done
		assert.Assert(done >= fcfg.Amount && !fd.Awarded, "can not award")
	} else {
		ctx.ArgsInt = int64(fcfg.Kind)
		assert.Assert(fd.Done >= fcfg.Amount && !fd.Awarded, "can not award")
	}

	petCfgId := fcfg.ChoiceReward[index-1]
	ctx.PetLevel = fcfg.PetLevel
	gPetMgr.GivePetFromCfgId(ctx, roleId, petCfgId, false, true, false, protocol.PetActionType_Pet_Action_Limit_Time_Feast, true)

	gSql.Begin().Clean(roleId)
	fd.Awarded = true
	gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fd.Done, fd.Awarded, fd.Id)
}

// ************************** 竞速活动 活动 **************************
func (mgr *FeastManager) fastRankInit() {
	gEventMgr.When(data.EVENT_MSGID_HOURLY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventHourly)
		if args.Hour == 22 {
			server := gServer.Node
			beginTime := time.Unix(server.BeginTime, 0)

			midnight := time.Date(beginTime.Year(), beginTime.Month(), beginTime.Day(), 0, 0, 0, 0, time.Local)

			subDay := args.Now.Sub(midnight) + time.Duration(2*time_helper.Hour)*time.Second
			if subDay == time.Duration(3*time_helper.Day)*time.Second ||
				subDay == time.Duration(7*time_helper.Day)*time.Second {
				gRankMgr.RangeGamePassRank(ctx, 1, 3000, func(ctx *data.Context, ranker *data.Rank) bool {
					var cfg *data.FeastFastRankCfg
					var kind protocol.FeastKind
					if subDay == time.Duration(3*time_helper.Day)*time.Second {
						kind = protocol.FeastKind_Fast_Rank_Three
					} else if subDay == time.Duration(7*time_helper.Day)*time.Second {
						kind = protocol.FeastKind_Fast_Rank_Seven
					}
					cfg = config.GetFeastFastRankCfg(ranker.Rank, kind)

					fd := &data.FeastDone{
						Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
						RoleId:        ranker.RoleId,
						CfgId:         cfg.CfgId,
						Awarded:       false,
						Kind:          kind,
						Done:          ranker.Rank,
						FightInfoJson: "[]",
					}

					gSql.Begin().Clean(ranker.RoleId)
					gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE,
						fd.Id, fd.RoleId, fd.CfgId, fd.Kind, fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
					mgr.cache.Delete(mgr.getFastRankCacheKey(ranker.RoleId))
					if gRoleMgr.IsOnline(ranker.RoleId) {
						gRoleMgr.Send(ctx, ranker.RoleId, protocol.MakeNotifyFeastDoneMsg([]*protocol.FeastDone{
							{
								Id:      fd.Id,
								CfgId:   fd.CfgId,
								Kind:    fd.Kind,
								Done:    fd.Done,
								Awarded: fd.Awarded,
							},
						}))
					}
					return true
				})
			}
		}

	})
}

func (mgr *FeastManager) getFastRankCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FEAST_FAST_RANK, roleId)
}

func (mgr *FeastManager) FastRankFeastBy(ctx *data.Context, roleId int64) *data.FeastFastRank {
	cacheKey := mgr.getFastRankCacheKey(roleId)

	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		d := &data.FeastFastRank{}
		var threeList, sevenList, fifteen []*data.FeastDone
		gSql.MustSelect(&threeList, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Fast_Rank_Three)
		gSql.MustSelect(&sevenList, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Fast_Rank_Seven)
		gSql.MustSelect(&fifteen, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Fast_Rank_Fifteen)
		if len(threeList) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, threeList[0])
		}
		if len(sevenList) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, sevenList[0])
		}
		if len(fifteen) > 0 {
			d.FeastDoneList = append(d.FeastDoneList, fifteen[0])
		}
		return d, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	return rv.(*data.FeastFastRank)
}

func (mgr *FeastManager) FastRankFeastByKind(ctx *data.Context, roleId int64, kind protocol.FeastKind) *data.FeastDone {
	all := mgr.FastRankFeastBy(ctx, roleId)
	for _, f := range all.FeastDoneList {
		if f.Kind == kind {
			return f
		}
	}
	return nil
}

func (mgr *FeastManager) fastRankFeastAward(ctx *data.Context, roleId int64, fd *data.FeastDone, index int32) {
	fcfg := config.GetFeastFastRankCfg(fd.Done, fd.Kind)
	assert.Assert(!fd.Awarded, "can not award")

	gItemMgr.GiveAward(ctx, roleId, fcfg.Reward, 1, protocol.ItemActionType_Action_Feast_Fast_Rank)

	gSql.Begin().Clean(roleId)
	fd.Awarded = true
	gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fd.Done, fd.Awarded, fd.Id)
}

// ******************* 累计充值活动
