package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type PetBreedManager struct {
}

func init() {
	gPetBreedMgr = &PetBreedManager{}
}

func (mgr *PetBreedManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventRoleNewDay)
		gSql.Begin().Clean(args.RoleId)
		gSql.MustExcel(gConst.SQL_UPDATE_BREED_DAILY, args.RoleId)
		gCache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetBreed, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.NotifyPetBreed(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_BeginBreed, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SBeginBreed()
		mgr.BeginBreed(ctx, ctx.RoleId, rep.Grid, rep.Pet1, rep.Pet2)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_FastBreed, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SFastBreed()
		mgr.FastBreed(ctx, ctx.RoleId, rep.Grid)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_EndBreed, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SEndBreed()
		mgr.EndBreed(ctx, ctx.RoleId, rep.Grid)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_CancelBreed, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SCancelBreed()
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.CancelBreed(ctx, ctx.RoleId, rep.Grid)
	})

}

func (mgr *PetBreedManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_PET_BREED, roleId)
}

func (mgr *PetBreedManager) AllPetBreedBy(ctx *data.Context, roleId int64) *data.RolePetBreeds {
	cacheKey := mgr.getCacheKey(roleId)

	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {

		var breedList []*data.PetBreed
		gSql.MustSelect(&breedList, gConst.SQL_SELECT_BREED, roleId)

		roleBreed := &data.RolePetBreeds{
			PetBreedList: make([]*data.PetBreed, 3),
			PetGridMap:   make(map[int64]int32),
		}
		for _, breed := range breedList {
			roleBreed.PetBreedList[breed.Grid-1] = breed
			if breed.PetId1 > 0 {
				roleBreed.PetGridMap[breed.PetId1] = breed.Grid
			}
			if breed.PetId2 > 0 {
				roleBreed.PetGridMap[breed.PetId2] = breed.Grid
			}
		}

		return roleBreed, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)
	return rv.(*data.RolePetBreeds)
}

func (mgr *PetBreedManager) PetBreedBy(ctx *data.Context, roleId int64, grid int32) *data.PetBreed {
	rolePetBreed := mgr.AllPetBreedBy(ctx, roleId)
	return rolePetBreed.PetBreedList[grid-1]
}

func (mgr *PetBreedManager) NotifyPetBreed(ctx *data.Context, roleId int64) {
	allPetBreed := mgr.AllPetBreedBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetBreedMsg(allPetBreed.GetForClient()))
}

// 开始繁殖
func (mgr *PetBreedManager) BeginBreed(ctx *data.Context, roleId int64, grid int32, petId1 int64, petId2 int64) {
	allBreed := mgr.AllPetBreedBy(ctx, roleId)
	breed := mgr.PetBreedBy(ctx, roleId, grid)
	assert.Assert(breed == nil || breed.BreedEndTime == 0, "now begining, grid: %d", grid)
	// 检测 petId
	pet1 := gPetMgr.PetBy(ctx, roleId, petId1)
	pet1Cfg := config.GetPetCfg(pet1.CfgId)
	pet2 := gPetMgr.PetBy(ctx, roleId, petId2)
	pet2Cfg := config.GetPetCfg(pet2.CfgId)
	assert.Assert(pet1Cfg.Vegetarian == pet2Cfg.Vegetarian, "vegetarian not the same")
	assert.Assert(pet1.Sex != pet2.Sex, "can not the same sex")
	assert.Assert(pet1.Breed*pet2.Breed != 0, "pet has no breed, pet1.breed = %d and pet2.breed = %d", pet1.Breed, pet2.Breed)
	assert.Assert(allBreed.PetGridMap[petId1] == 0, "pet had in breed, petId:%d, breed grid:%d", petId1, allBreed.PetGridMap[petId1])
	assert.Assert(allBreed.PetGridMap[petId2] == 0, "pet had in breed, petId:%d, breed grid:%d", petId2, allBreed.PetGridMap[petId2])
	gSql.Begin().Clean(roleId)
	if breed == nil {
		breed = &data.PetBreed{
			RoleId:       roleId,
			Grid:         grid,
			PetId1:       petId1,
			PetId2:       petId2,
			BreedEndTime: ctx.NowSec + gConst.BREED_TIME_OUT,
		}
		allBreed.PetBreedList[grid-1] = breed
		gSql.MustExcel(gConst.SQL_INSERT_BREED,
			breed.RoleId, breed.Grid, breed.PetId1, breed.PetId2, breed.BreedEndTime, breed.BreedFastNum)
	} else {
		breed.PetId1, breed.PetId2, breed.BreedEndTime = petId1, petId2, ctx.NowSec+gConst.BREED_TIME_OUT
		gSql.MustExcel(gConst.SQL_UPDATE_BREED, breed.PetId1, breed.PetId2, breed.BreedEndTime,
			breed.BreedFastNum, breed.RoleId, breed.Grid)
	}

	allBreed.PetGridMap[petId1] = grid
	allBreed.PetGridMap[petId2] = grid
	if grid == 2 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Gem), 10, protocol.ItemActionType_Action_Pet_Breed)
	} else if grid == 3 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Gem), 50, protocol.ItemActionType_Action_Pet_Breed)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetBreedUpdateMsg([]*protocol.PetBreed{breed.GetForClient()}))

}

// 加速繁殖
func (mgr *PetBreedManager) FastBreed(ctx *data.Context, roleId int64, grid int32) {
	breed := mgr.PetBreedBy(ctx, roleId, grid)
	gSql.Begin().Clean(roleId)
	assert.Assert(breed != nil && breed.BreedEndTime > 0 && breed.BreedEndTime >= ctx.NowSec, "not begining, grid: %d", grid)
	assert.Assert(breed.BreedFastNum == 0, "alway fasted")
	breed.BreedFastNum++
	breed.BreedEndTime -= gConst.BREED_TIME_OUT / 2
	if grid > 1 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Gem), 10, protocol.ItemActionType_Action_Pet_Breed)
	}
	gSql.MustExcel(gConst.SQL_UPDATE_FAST_BREED, breed.BreedFastNum, breed.BreedEndTime, roleId, grid)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetBreedUpdateMsg([]*protocol.PetBreed{breed.GetForClient()}))
}

// 取消繁殖
func (mgr *PetBreedManager) CancelBreed(ctx *data.Context, roleId int64, grid int32) {
	allBreed := mgr.AllPetBreedBy(ctx, roleId)
	breed := mgr.PetBreedBy(ctx, roleId, grid)
	assert.Assert(breed != nil && breed.BreedEndTime > 0 && breed.BreedEndTime >= ctx.NowSec, "not begining, grid: %d", grid)
	gSql.Begin().Clean(roleId)
	delete(allBreed.PetGridMap, breed.PetId1)
	delete(allBreed.PetGridMap, breed.PetId2)

	breed.PetId1, breed.PetId2, breed.BreedEndTime, breed.BreedFastNum = 0, 0, 0, 0
	gSql.MustExcel(gConst.SQL_UPDATE_BREED, breed.PetId1, breed.PetId2, breed.BreedEndTime,
		breed.BreedFastNum, breed.RoleId, breed.Grid)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetBreedUpdateMsg([]*protocol.PetBreed{breed.GetForClient()}))
}

// 结束繁殖
func (mgr *PetBreedManager) EndBreed(ctx *data.Context, roleId int64, grid int32) {
	allBreed := mgr.AllPetBreedBy(ctx, roleId)
	breed := mgr.PetBreedBy(ctx, roleId, grid)
	pet1 := gPetMgr.PetBy(ctx, roleId, breed.PetId1)
	pet2 := gPetMgr.PetBy(ctx, roleId, breed.PetId2)
	gSql.Begin().Clean(roleId)
	assert.Assert(breed != nil && breed.BreedEndTime > 0 && breed.BreedEndTime < ctx.NowSec+5, "not ending, grid: %d", grid)

	delete(allBreed.PetGridMap, breed.PetId1)
	delete(allBreed.PetGridMap, breed.PetId2)

	breed.PetId1, breed.PetId2, breed.BreedEndTime = 0, 0, 0
	gSql.MustExcel(gConst.SQL_UPDATE_BREED, breed.PetId1, breed.PetId2, breed.BreedEndTime,
		breed.BreedFastNum, breed.RoleId, breed.Grid)

	pet1.Breed--
	pet2.Breed--
	breed0 := util.MinInt32(pet1.Breed, pet2.Breed)
	gSql.MustExcel(gConst.SQL_UPDATE_PET_BREED, pet1.Breed, pet1.Id)
	gSql.MustExcel(gConst.SQL_UPDATE_PET_BREED, pet2.Breed, pet2.Id)

	pet := gPetMgr.NewPet(ctx, roleId, util.ChoiceInt32(util.Random(0, 1) == 0, pet1.CfgId, pet2.CfgId),
		false, false, false, protocol.PetActionType_Pet_Action_Breed)
	pet.Breed = breed0
	pet.AtkAdd = util.Random32(util.MaxInt32(0, util.MinInt32(pet1.AtkAdd, pet2.AtkAdd)-3), util.MaxInt32(0, util.MaxInt32(pet1.AtkAdd, pet2.AtkAdd)+8))
	pet.DefAdd = util.Random32(util.MaxInt32(0, util.MinInt32(pet1.DefAdd, pet2.DefAdd)-3), util.MaxInt32(0, util.MaxInt32(pet1.DefAdd, pet2.DefAdd)+8))
	pet.HpMaxAdd = util.Random64(util.MaxInt64(0, util.MinInt64(pet1.HpMaxAdd, pet2.HpMaxAdd)-3), util.MaxInt64(0, util.MaxInt64(pet1.HpMaxAdd, pet2.HpMaxAdd)+8))
	pet.SkillCfgId = util.ChoiceInt32(util.Random(0, 1) == 0, pet1.CfgId, pet2.CfgId)
	gAttrMgr.PetAttrBy(ctx, pet, false)

	gPetMgr.GivePet(ctx, roleId, pet, protocol.PetActionType_Pet_Action_Breed)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, []*protocol.Pet{pet1.GetForClient(), pet2.GetForClient()}, nil, protocol.PetActionType_Pet_Action_Breed))

	gRoleMgr.Send(ctx, roleId, protocol.MakeEndBreedDoneMsg(ctx.EventMsg.GetEnvMsg(), grid, pet.GetForClient()))
}
