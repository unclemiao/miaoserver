package logic

import (
	"goproject/engine/assert"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

var gViewMgr *ViewManager

type ViewManager struct {
}

func init() {
	gViewMgr = &ViewManager{}
}

func (mgr *ViewManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherRole, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SViewOtherRole()
		)
		viewRole := mgr.ViewRole(ctx, rep.RoleId)
		gRoleMgr.Send(ctx, ctx.RoleId, &protocol.Envelope{
			SeqId:   ctx.EventMsg.GetEnvMsg().SeqId,
			MsgType: protocol.EnvelopeType_S2C_ViewOtherRoleDone,
			Payload: &protocol.Envelope_S2CViewOtherRoleDone{S2CViewOtherRoleDone: &protocol.S2C_ViewOtherRoleDoneMsg{
				ViewRole: viewRole,
			}},
		})
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherItem, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SViewOtherItem()
		)
		mgr.ViewItem(ctx, ctx.RoleId, rep.ItemId)
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherPet, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SViewOtherPet()
		)
		mgr.ViewPet(ctx, ctx.RoleId, rep.PetId)
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherStub, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SViewOtherStub()
		)
		mgr.ViewStub(ctx, ctx.RoleId, rep.RoleId, rep.StubKind)
	})
}

func (mgr *ViewManager) ViewRole(ctx *data.Context, roleId int64) *protocol.ViewRole {

	otherRole := gRoleMgr.RoleBy(ctx, roleId)
	gAttrMgr.RoleAttrBy(ctx, otherRole, false)

	viewRole := &protocol.ViewRole{
		Id:         otherRole.RoleId,
		Name:       otherRole.Name,
		Avatar:     otherRole.Avatar,
		Level:      otherRole.Level,
		Atk:        otherRole.Attr.Atk,
		AtkTime:    otherRole.Attr.AtkTime,
		Crit:       otherRole.Attr.Crit,
		Def:        otherRole.Attr.Def,
		Dis:        otherRole.Attr.Dis,
		Dodge:      otherRole.Attr.Dodge,
		HpMax:      otherRole.Attr.HpMax,
		ModelCfgId: otherRole.ModelCfgId,
		ModelName:  otherRole.ModelName,
	}

	if otherRole.Horse > 0 {
		horse := gPetMgr.PetBy(ctx, otherRole.RoleId, otherRole.Horse)
		viewRole.HorseId = otherRole.Horse
		viewRole.HorseCfgId = horse.CfgId
		viewRole.HorseStar = horse.Star
		viewRole.HorseSkillPetCfgId = horse.SkillCfgId
	}

	equipList := gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_At_Role)
	for _, e := range equipList {
		if e != nil {
			viewItem := &protocol.ViewItem{
				Id:         e.Id,
				CfgId:      e.Cid,
				Num:        e.N,
				SkillCfgId: e.SkillCfgId,
				Grade:      e.Grade,
				Star:       e.Star,
				HpMax:      e.HpMaxBase,
				Atk:        e.AtkBase,
				Def:        e.DefBase,
			}
			if e.RandAttr != nil {
				viewItem.RandHpMax = e.RandAttr.HpMax
				viewItem.RandAtk = e.RandAttr.Atk
				viewItem.RandAtkTime = e.RandAttr.AtkTime
				viewItem.RandCrit = e.RandAttr.Crit
				viewItem.RandDef = e.RandAttr.Def
				viewItem.RandDodge = e.RandAttr.Dodge
			}
			viewRole.EquipList = append(viewRole.EquipList, viewItem)
		}
	}

	var pbList []*protocol.Fashion
	roleFashion := gFashionMgr.RoleFashionBy(ctx, otherRole.RoleId)
	for _, f := range roleFashion.FashionMap {
		if f != nil && f.State == 2 {
			pbList = append(pbList, gFashionMgr.GetForClient(ctx, f))
		}
	}
	viewRole.FashionList = pbList

	return viewRole
}

func (mgr *ViewManager) ViewFightRole(ctx *data.Context, otherRole *data.Role) *protocol.FightRole {

	gAttrMgr.RoleAttrBy(ctx, otherRole, false)

	viewRole := &protocol.FightRole{
		Id:         otherRole.RoleId,
		Name:       otherRole.Name,
		Avatar:     otherRole.Avatar,
		Level:      otherRole.Level,
		Atk:        otherRole.Attr.Atk,
		AtkTime:    otherRole.Attr.AtkTime,
		Crit:       otherRole.Attr.Crit,
		Def:        otherRole.Attr.Def,
		Dis:        otherRole.Attr.Dis,
		Dodge:      otherRole.Attr.Dodge,
		HpMax:      otherRole.Attr.HpMax,
		ModelName:  otherRole.ModelName,
		ModelCfgId: otherRole.ModelCfgId,
	}

	if otherRole.Horse > 0 {
		horse := gPetMgr.PetBy(ctx, otherRole.RoleId, otherRole.Horse)
		viewRole.HorseId = otherRole.Horse
		viewRole.HorseCfgId = horse.CfgId
		viewRole.HorseStar = horse.Star
		viewRole.HorseSkillPetCfgId = horse.SkillCfgId
		viewRole.HorseVariation = horse.Variation
		viewRole.HorseSuperEvolution = horse.SuperEvolution
	}

	equipList := gItemMgr.ItemListByKit(ctx, otherRole.RoleId, protocol.KitType_Kit_At_Role)
	for _, e := range equipList {
		if e != nil {
			viewRole.EquipList = append(viewRole.EquipList, gItemMgr.GetForClient(e))
		}
	}

	viewRole.RoleTalentList = gRoleTalentMgr.GetForClient(ctx, gRoleTalentMgr.AllRoleTalentBy(ctx, otherRole.RoleId))

	var pbList []*protocol.Fashion
	roleFashion := gFashionMgr.RoleFashionBy(ctx, otherRole.RoleId)
	for _, f := range roleFashion.FashionMap {
		if f != nil && f.State == 2 {
			pbList = append(pbList, gFashionMgr.GetForClient(ctx, f))
		}
	}
	viewRole.FashionList = pbList
	return viewRole
}

func (mgr *ViewManager) ViewItem(ctx *data.Context, myRoleId int64, itemId int64) *protocol.ViewItem {
	var itemList []*data.Item
	gSql.MustSelect(&itemList, gConst.SQL_SELECT_ITEM, itemId)
	assert.Assert(len(itemList) > 0, "!装备丢在丛林里了")
	item := itemList[0]
	viewItem := &protocol.ViewItem{
		Id:         item.Id,
		CfgId:      item.Cid,
		Num:        item.N,
		SkillCfgId: item.SkillCfgId,
		Grade:      item.Grade,
		Star:       item.Star,
		HpMax:      item.HpMaxBase,
		Atk:        item.AtkBase,
		Def:        item.DefBase,
	}
	if item.RandAttr != nil {
		viewItem.RandHpMax = item.RandAttr.HpMax
		viewItem.RandAtk = item.RandAttr.Atk
		viewItem.RandAtkTime = item.RandAttr.AtkTime
		viewItem.RandCrit = item.RandAttr.Crit
		viewItem.RandDef = item.RandAttr.Def
		viewItem.RandDodge = item.RandAttr.Dodge
	}

	gRoleMgr.Send(ctx, myRoleId, &protocol.Envelope{
		SeqId:   ctx.EventMsg.GetEnvMsg().SeqId,
		MsgType: protocol.EnvelopeType_S2C_ViewOtherItemDone,
		Payload: &protocol.Envelope_S2CViewOtherItemDone{S2CViewOtherItemDone: &protocol.S2C_ViewOtherItemDoneMsg{
			ViewItem: viewItem,
		}},
	})
	return viewItem
}

func (mgr *ViewManager) ViewPet(ctx *data.Context, myRoleId int64, petId int64) *protocol.ViewPet {
	var petList []*data.Pet
	gSql.MustSelect(&petList, gConst.SQL_SELECT_PET, petId)
	assert.Assert(len(petList) > 0, "!宠物出去玩了，晚点再来看吧")
	pet := petList[0]
	gAttrMgr.PetAttrBy(ctx, pet, false)
	viewPet := &protocol.ViewPet{
		Id:             pet.Id,
		CfgId:          pet.CfgId,
		Level:          pet.Level,
		Star:           pet.Star,
		IsVariation:    pet.Variation,
		Sex:            pet.Sex,
		Breed:          pet.Breed,
		Atk:            pet.Attr.Atk,
		AtkTime:        pet.Attr.AtkTime,
		Crit:           pet.Attr.Crit,
		Def:            pet.Attr.Def,
		Dis:            pet.Attr.Dis,
		Dodge:          pet.Attr.Dodge,
		HpMax:          pet.Attr.HpMax,
		AtkAdd:         pet.AtkAdd,
		DefAdd:         pet.DefAdd,
		HpMaxAdd:       pet.HpMaxAdd,
		SkillPetCfgId:  pet.SkillCfgId,
		SuperEvolution: pet.SuperEvolution,
	}
	gRoleMgr.Send(ctx, myRoleId, &protocol.Envelope{
		SeqId:   ctx.EventMsg.GetEnvMsg().SeqId,
		MsgType: protocol.EnvelopeType_S2C_ViewOtherPetDone,
		Payload: &protocol.Envelope_S2CViewOtherPetDone{S2CViewOtherPetDone: &protocol.S2C_ViewOtherPetDoneMsg{
			ViewPet: viewPet,
		}},
	})
	return viewPet
}

func (mgr *ViewManager) ViewStub(ctx *data.Context, myRoleId int64, roleId int64, kind protocol.StubKind) {

	stubInfoPb, _ := gStubMgr.ViewOtherStub(ctx, roleId, kind)

	fightRole := gViewMgr.ViewFightRole(ctx, gRoleMgr.RoleBy(ctx, roleId))

	gRoleMgr.Send(ctx, myRoleId, &protocol.Envelope{
		SeqId:   ctx.EventMsg.GetEnvMsg().SeqId,
		MsgType: protocol.EnvelopeType_S2C_ViewOtherStubDone,
		Payload: &protocol.Envelope_S2CViewOtherStubDone{S2CViewOtherStubDone: &protocol.S2C_ViewOtherStubDoneMsg{
			StubInfo:  stubInfoPb,
			FightRole: fightRole,
		}},
	})

}
