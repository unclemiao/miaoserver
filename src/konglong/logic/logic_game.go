package logic

import (
	"goproject/src/konglong/protocol"
	"net/http"
)

type GameManager struct {
}

func init() {
	gGameMgr = &GameManager{}
}

func (mgr *GameManager) Init() {
	HandleFunc("/game-api/base/game/material", func(w http.ResponseWriter, r *http.Request) {
		SendToHttp(w, &protocol.S2C_GetGameShareMaterialDoneMsg{
			ShareImage: "https://static.qcinterfacet.com/filems//file/2020/8/1600777769135.jpg",
			ShareTitle: "送你一只变异的恐龙，快来领养吧！",
		})
	})
}
