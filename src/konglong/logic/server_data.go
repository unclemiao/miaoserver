package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

type ServerExtDataManager struct {
	trackId    string
	dataMap    map[string]*data.ServerExtData
	adSdkSlots map[string]*data.AdSdkSlots // { pos = AdSdkSlots }
}

var gServerExtDataMgr *ServerExtDataManager

func init() {
	gServerExtDataMgr = &ServerExtDataManager{
		dataMap:    make(map[string]*data.ServerExtData),
		adSdkSlots: make(map[string]*data.AdSdkSlots),
	}
}

func (mgr *ServerExtDataManager) Init() {

}

func (mgr *ServerExtDataManager) GetSlotsProto() []*protocol.AdSdkSlotsCfg {
	var list []*protocol.AdSdkSlotsCfg
	for _, slot := range mgr.adSdkSlots {
		pb := &protocol.AdSdkSlotsCfg{
			Id:       slot.Id,
			SlotNum:  slot.SlotNum,
			Position: slot.Position,
		}
		for _, app := range slot.Apps {
			pb.Apps = append(pb.Apps, &protocol.AdSdkAppCfg{
				Id:      app.Id,
				AppName: app.AppName,
				AppId:   app.AppId,
				Icon:    app.Icon,
				Path:    app.Path,
			})
		}
		list = append(list, pb)
	}
	return list
}

func (mgr *ServerExtDataManager) GetApp(appId string) *data.AdSdkApp {
	slot := mgr.adSdkSlots["12"]
	return slot.AppMap[appId]
}

func (mgr *ServerExtDataManager) LoadServerDataMgr() {
	logs.Infof("LoadServerDataMgr......................")
	var host string
	var adHost string
	if gAdminMgr.ServerCmd.Platform == "test" {
		host = "https://hygae.qcinterfacet.com"
	} else {
		host = "https://hyga-pro.qcinterfacet.com"
	}

	if gAdminMgr.ServerCmd.Platform == "test" {
		adHost = "https://hygae.qcinterfacet.com"
	} else {
		adHost = "https://hysj.qcinterfacet.com"
	}

	mgr.loginAuth(host + "/game-manage/cas/sys/auth")
	mgr.getAdminAdSdkInfo(adHost + "/game-open/slot/slots")
}

type authResp struct {
	Result   int32  `json:"result"`
	Msg      string `json:"msg"`
	ErrMsg   string `json:"errMsg"`
	Status   int32  `json:"status"`
	Data     string `json:"data"`
	DateTime int64  `json:"dateTime"`
}

func (mgr *ServerExtDataManager) loginAuth(host string) string {
	account, pass := "neibu", util.Md5("neibuadmin@123")
	urlForm := make(url.Values)
	urlForm.Set("account", account)
	urlForm.Set("pas", pass)

	resp, err := http.PostForm(host, urlForm)
	assert.Assert(err == nil, err)
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	assert.Assert(err == nil, err)
	hostResp := &authResp{}
	err = json.Unmarshal(bs, hostResp)
	assert.Assert(err == nil, err)
	mgr.trackId = hostResp.Data
	return hostResp.Data
}

func (mgr *ServerExtDataManager) getAdminAdSdkInfo(host string) {
	urlForm := make(url.Values)
	urlForm.Set("trackId", mgr.trackId)
	urlForm.Set("gameId", fmt.Sprintf("%d", gAdminMgr.ServerCmd.JavaLogGameId))
	urlForm.Set("gameAreaId", fmt.Sprintf("%d", gAdminMgr.ServerCmd.JavaLogServerId))

	resp, err := http.PostForm(host, urlForm)
	assert.Assert(err == nil, err)
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	assert.Assert(err == nil, err)
	logs.Debugf("%s", string(bs))
	hostResp := &data.AdSdkSlotsResp{}
	err = json.Unmarshal(bs, hostResp)
	assert.Assert(err == nil, err)
	for _, slot := range hostResp.Data {
		if slot.AppMap == nil {
			slot.AppMap = make(map[string]*data.AdSdkApp)
		}
		mgr.adSdkSlots[slot.Position] = slot
		for _, app := range slot.Apps {
			slot.AppMap[app.AppId] = app
		}
	}
}
