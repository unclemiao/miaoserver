package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/engine/logs"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type ManualManager struct {
	cache *cache.ShardedCache
}

func init() {
	gManualMgr = &ManualManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4),
	}
}

func (mgr *ManualManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetManual, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Notify(ctx, ctx.RoleId, nil)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFoodManualAward, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SGetFoodManualAward()
		)
		mgr.FoodManualAward(ctx, ctx.RoleId, rep.CfgId)
	})

	gEventMgr.When(data.EVENT_MSGID_ON_ITEM_MAKE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventItemMake)
		mgr.OnItemMake(ctx, args.RoleId, args.Item)
	})

	mgr.InitFetters()

}

func (mgr *ManualManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_MANUAL, roleId)
}

func (mgr *ManualManager) RoleManualBy(ctx *data.Context, roleId int64) *data.RoleManual {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleManual := &data.RoleManual{
			PetManualMap:  make(map[int32]*data.Manual),
			FoodManualMap: make(map[protocol.ItemSubKind]*data.Manual),
		}
		var dataList []*data.Manual

		gSql.MustSelect(&dataList, gConst.SQL_SELECT_MANUAL, roleId)
		for _, info := range dataList {
			if info.Kind == protocol.ManualKind_Manual_Pet {
				roleManual.PetManualMap[info.PetCfgId] = info
			} else if info.Kind == protocol.ManualKind_Manual_Food {
				cfg := config.GetItemCfg(int64(info.ItemCfgId))
				roleManual.FoodManualMap[cfg.SubKind] = info
			}
		}
		return roleManual, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.RoleManual)
}

func (mgr *ManualManager) PetManualBy(ctx *data.Context, roleId int64, petCfgId int32) *data.Manual {
	roleManual := mgr.RoleManualBy(ctx, roleId)
	return roleManual.PetManualMap[petCfgId]
}

func (mgr *ManualManager) OnPetUpdate(ctx *data.Context, roleId int64, pet *data.Pet) {
	infoMap := mgr.RoleManualBy(ctx, roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	manual := infoMap.PetManualMap[pet.CfgId]
	isNew := manual == nil
	update := false
	if isNew {
		manual = &data.Manual{
			Id:       gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_MANUAL),
			Kind:     protocol.ManualKind_Manual_Pet,
			RoleId:   roleId,
			PetCfgId: pet.CfgId,
		}
		if pet.Variation {
			manual.HasVariation = true
			manual.VariationEvolutionLv = pet.Evolution
			manual.VariationStarMax = pet.Star
		} else {
			manual.HasNormal = true
			manual.EvolutionLv = pet.Evolution
			manual.StarMax = pet.Star
		}
		if pet.SuperEvolution {
			if pet.Variation {
				manual.HasVariationSuperEvolution = true
			} else {
				manual.HasSuperEvolution = true
			}
		}
		infoMap.PetManualMap[pet.CfgId] = manual
	} else {
		if pet.Variation {
			if !manual.HasVariation {
				manual.HasVariation = true
				update = true
			}
			if manual.VariationEvolutionLv != pet.Evolution {
				manual.VariationEvolutionLv = pet.Evolution
				update = true
			}
			if manual.VariationStarMax < pet.Star {
				manual.VariationStarMax = pet.Star
				update = true
			}
			if pet.SuperEvolution {
				manual.HasVariationSuperEvolution = true
				update = true
			}
		} else {
			if !manual.HasNormal {
				manual.HasNormal = true
				update = true
			}

			if manual.EvolutionLv < pet.Evolution {
				manual.EvolutionLv = pet.Evolution
				update = true
			}
			if manual.StarMax < pet.Star {
				manual.StarMax = pet.Star
				update = true
			}
			if pet.SuperEvolution {
				manual.HasSuperEvolution = true
				update = true
			}
		}

	}
	logs.Infof("manual %+v", manual)
	if isNew {
		gSql.MustExcel(gConst.SQL_INSERT_MANUAL,
			manual.Id,
			manual.RoleId,
			manual.Kind,
			manual.Awarded,
			manual.ItemCfgId,
			manual.PetCfgId,
			manual.EvolutionLv,
			manual.StarMax,
			manual.HasNormal,
			manual.HasVariation,
			manual.VariationEvolutionLv,
			manual.VariationStarMax,
			manual.HasSuperEvolution,
			manual.HasVariationSuperEvolution,
		)
	} else if update {
		gSql.MustExcel(gConst.SQL_UPDATE_MANUAL,
			manual.EvolutionLv,
			manual.StarMax,
			manual.HasNormal,
			manual.HasVariation,
			manual.VariationEvolutionLv,
			manual.VariationStarMax,
			manual.HasSuperEvolution,
			manual.HasVariationSuperEvolution,
			manual.Id,
		)
	}

	if isNew || update {
		mgr.NotifyUpdate(ctx, roleId, []*protocol.Manual{manual.GetForClient()})
	}
}

// ************************************ 食物图鉴 ************************************

func (mgr *ManualManager) FoodManualBy(ctx *data.Context, roleId int64, foodCfgId int32) *data.Manual {
	roleManual := mgr.RoleManualBy(ctx, roleId)
	foodCfg := config.GetItemCfg(int64(foodCfgId))
	return roleManual.FoodManualMap[foodCfg.SubKind]
}

func (mgr *ManualManager) OnItemMake(ctx *data.Context, roleId int64, item *data.Item) {
	cfg := config.GetItemCfg(item.Cid)
	if cfg.Kind == protocol.ItemKind_Item_Food {
		roleManual := mgr.RoleManualBy(ctx, roleId)
		manual := roleManual.FoodManualMap[cfg.SubKind]
		if manual == nil {
			manual = &data.Manual{
				Id:        gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_MANUAL),
				Kind:      protocol.ManualKind_Manual_Food,
				RoleId:    roleId,
				ItemCfgId: int32(item.Cid),
			}
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_MANUAL,
				manual.Id,
				manual.RoleId,
				manual.Kind,
				manual.Awarded,
				manual.ItemCfgId,
				manual.PetCfgId,
				manual.EvolutionLv,
				manual.StarMax,
				manual.HasNormal,
				manual.HasVariation,
				manual.VariationEvolutionLv,
				manual.VariationStarMax,
				manual.HasSuperEvolution,
				manual.HasVariationSuperEvolution,
			)
			roleManual.FoodManualMap[cfg.SubKind] = manual
			mgr.NotifyUpdate(ctx, roleId, []*protocol.Manual{manual.GetForClient()})
		}
	}
}

func (mgr *ManualManager) FoodManualAward(ctx *data.Context, roleId int64, cfgId int32) {
	cfg := config.GetItemCfg(int64(cfgId))
	manual := mgr.FoodManualBy(ctx, roleId, cfgId)
	assert.Assert(manual != nil, "manual not found, kind:%d", cfg.SubKind)
	manualCfg := config.GetFoodManualCfg(cfg.SubKind)
	gItemMgr.GiveAward(ctx, roleId, manualCfg.Reward, 1, protocol.ItemActionType_Action_Food_Manual)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_MANUAL_AWARD, true, manual.Id)
	manual.Awarded = true
	mgr.NotifyUpdate(ctx, roleId, []*protocol.Manual{manual.GetForClient()})
}

func (mgr *ManualManager) Notify(ctx *data.Context, roleId int64, info *data.RoleManual) {
	if info == nil {
		info = mgr.RoleManualBy(ctx, roleId)
	}
	roleFetters := mgr.RoleFettersBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyManualMsg(info.GetForClient(), mgr.GetRoleFettersForClient(roleFetters)))
}

func (mgr *ManualManager) NotifyUpdate(ctx *data.Context, roleId int64, manualList []*protocol.Manual) {
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyManualUpdateMsg(manualList))
}
