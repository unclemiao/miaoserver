package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func (mgr *ManualManager) InitFetters() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_FettersActive, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SFettersActive()
		)
		mgr.FettersActive(ctx, ctx.RoleId, rep.GetCfgId())
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_FettersAward, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SFettersAward()
		)
		mgr.FettersAward(ctx, ctx.RoleId, rep.CfgId)
	})
}

func (mgr *ManualManager) getFettersCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FETTERS, roleId)
}

func (mgr *ManualManager) RoleFettersBy(ctx *data.Context, roleId int64) *data.RoleFetters {
	cacheKey := mgr.getFettersCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleFetters := &data.RoleFetters{
			FettersMap:      make(map[int64]*data.Fetters),
			FettersCfgIdMap: make(map[int32]*data.Fetters),
		}
		var dataList []*data.Fetters

		gSql.MustSelect(&dataList, gConst.SQL_SELECT_FETTERS, roleId)
		for _, info := range dataList {
			roleFetters.FettersMap[info.Id] = info
			roleFetters.FettersCfgIdMap[info.CfgId] = info
		}
		return roleFetters, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.RoleFetters)
}

func (mgr *ManualManager) FettersBy(ctx *data.Context, roleId int64, Id int64) *data.Fetters {
	roleFetters := mgr.RoleFettersBy(ctx, roleId)
	return roleFetters.FettersMap[Id]
}

func (mgr *ManualManager) FettersByCfgId(ctx *data.Context, roleId int64, cfgId int32) *data.Fetters {
	roleFetters := mgr.RoleFettersBy(ctx, roleId)
	return roleFetters.FettersCfgIdMap[cfgId]
}

func (mgr *ManualManager) FettersByPetCfgId(ctx *data.Context, roleId int64, petCfgId int32, variation bool) ([]*data.Fetters, []*data.FettersCfg) {
	var fs []*data.Fetters
	var fcfgs []*data.FettersCfg
	roleFetters := mgr.RoleFettersBy(ctx, roleId)
	for _, f := range roleFetters.FettersMap {
		cfg := config.GetFettersCfg(f.CfgId)
		if cfg.MainPetId == petCfgId {
			if variation {
				fs = append(fs, f)
				fcfgs = append(fcfgs, cfg)
			} else if cfg.MainPetVariation == 0 {
				fs = append(fs, f)
				fcfgs = append(fcfgs, cfg)
			}

		}
	}
	return fs, fcfgs
}

func (mgr *ManualManager) GetFettersForClient(fetters *data.Fetters) *protocol.Fetters {
	cfg := config.GetFettersCfg(fetters.CfgId)
	pbf := &protocol.Fetters{
		Id:        fetters.Id,
		CfgId:     fetters.CfgId,
		Awarded:   fetters.Awarded,
		GroupId:   cfg.GroupId,
		NextCfgId: cfg.NextCfgId,
	}
	return pbf
}

func (mgr *ManualManager) GetRoleFettersForClient(roleFetters *data.RoleFetters) []*protocol.Fetters {
	flist := make([]*protocol.Fetters, 0, len(roleFetters.FettersMap))
	for _, f := range roleFetters.FettersMap {
		flist = append(flist, mgr.GetFettersForClient(f))
	}
	return flist
}

func (mgr *ManualManager) newFetters(ctx *data.Context, roleId int64, cfgId int32) *data.Fetters {
	f := &data.Fetters{
		Id:      gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FETTERS),
		RoleId:  roleId,
		CfgId:   cfgId,
		Awarded: false,
	}
	roleFetters := mgr.RoleFettersBy(ctx, roleId)
	roleFetters.FettersMap[f.Id] = f
	roleFetters.FettersCfgIdMap[cfgId] = f
	return f
}

func (mgr *ManualManager) FettersActive(ctx *data.Context, roleId int64, cfgId int32) {
	fettersCfg := config.GetFettersCfg(cfgId)
	mainManual := mgr.PetManualBy(ctx, roleId, fettersCfg.MainPetId)
	subManual := mgr.PetManualBy(ctx, roleId, fettersCfg.SubPetId)
	assert.Assert(mainManual != nil, "mainManual is null")
	assert.Assert(subManual != nil, "subManual is null")

	assert.Assert(fettersCfg.MainPetVariation == 0 || (mainManual.HasVariation == (fettersCfg.MainPetVariation == 1)), "mainPet not same variation")
	if fettersCfg.MainPetVariation == 1 {
		assert.Assert(mainManual.HasVariation, "mainPet not same variation")
		assert.Assert(mainManual.VariationStarMax >= fettersCfg.MainPetStar, "mainPet not same variation")
	} else {
		assert.Assert(mainManual.StarMax >= fettersCfg.MainPetStar, "mainPet not same variation")
	}

	if fettersCfg.SubPetVariation == 1 {
		assert.Assert(subManual.HasVariation, "subPet not same variation")
		assert.Assert(subManual.VariationStarMax >= fettersCfg.SubPetStar, "subPet not same variation")
	} else {
		assert.Assert(subManual.StarMax >= fettersCfg.SubPetStar, "subPet not same variation")
	}

	var fetters *data.Fetters
	var isNew bool
	gSql.Begin().Clean(roleId)

	fetters = mgr.FettersByCfgId(ctx, roleId, cfgId)
	assert.Assert(fetters == nil, " duplicate active, cfgId:%d", cfgId)

	if fettersCfg.PreCfgId == 0 {
		// 激活
		isNew = true
		fetters = mgr.newFetters(ctx, roleId, cfgId)
	} else {
		fetters = mgr.FettersByCfgId(ctx, roleId, fettersCfg.PreCfgId)
		roleFetters := mgr.RoleFettersBy(ctx, roleId)
		delete(roleFetters.FettersCfgIdMap, fetters.CfgId)
		fetters.CfgId = cfgId
		roleFetters.FettersCfgIdMap[cfgId] = fetters

	}

	fetters.Awarded = false
	if isNew {
		gSql.MustExcel(gConst.SQL_INSTER_FETTERS, fetters.Id, fetters.RoleId, fetters.CfgId, fetters.Awarded)
	} else {
		gSql.MustExcel(gConst.SQL_UPDATE_FETTERS, fetters.CfgId, fetters.Awarded, fetters.Id)
	}

	// 刷主恐龙属性
	gStubMgr.NotifyStub(ctx, roleId, gConst.STUB_TYPE_NORMAL)

	gRoleMgr.Send(ctx, roleId, protocol.MakeFettersActiveDoneMsg(ctx.EventMsg.GetEnvMsg(), mgr.GetFettersForClient(fetters)))
}

func (mgr *ManualManager) FettersAward(ctx *data.Context, roleId int64, cfgId int32) {

	fetters := mgr.FettersByCfgId(ctx, roleId, cfgId)

	assert.Assert(fetters != nil, "fetters not found, cfgId:%d", cfgId)
	assert.Assert(!fetters.Awarded, "fetters awarded, cfgId:%d", cfgId)

	fetters.Awarded = true
	fettersCfg := config.GetFettersCfg(cfgId)
	gItemMgr.GiveAward(ctx, roleId, fettersCfg.Reward, 1, protocol.ItemActionType_Action_Fetters)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_FETTERS, fetters.CfgId, fetters.Awarded, fetters.Id)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFettersMsg([]*protocol.Fetters{mgr.GetFettersForClient(fetters)}))
}
