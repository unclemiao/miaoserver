package logic

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"sort"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

// ************************** 龙王争霸赛 **************************
var petKingFeastRank *data.FeastPetKingRank

func (mgr *FeastManager) petKingInit() {

	gEventMgr.When(data.EVENT_MSGID_DAILY, func(ctx *data.Context, args interface{}) {
		go func() {
			defer func() {
				err := recover()
				if err != nil {
					logs.Error(err)
				}
			}()
			gSql.Do("USE " + gSql.Dsn.DatabaseName)
			gSql.Do(gConst.SQL_DELETE_PET_KING_RECORD, time.Now().AddDate(0, 0, -7))
		}()
	})

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, args interface{}) {
		if petKingFeastRank == nil {
			petKingFeastRank = &data.FeastPetKingRank{
				RoleFeastDoneMap: make(map[int64]*data.FeastDone),
			}
			topFd := make([]*data.FeastDone, 0, 2)
			gSql.MustSelect(&topFd, gConst.SQL_SELECT_FEAST_DONE_PET_KING_RANK, protocol.FeastKind_Pet_King)
			if len(topFd) > 0 {
				f := topFd[0]
				err := json.Unmarshal([]byte(f.FightInfoJson), &f.FightInfoRoleIdList)
				assert.Assert(err == nil, err)
				petKingFeastRank.FeastDone = f
			} else {
				fd := &data.FeastDone{
					Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
					Kind:          protocol.FeastKind_Pet_King,
					Done:          int32(time_helper.NowSec()),
					Rank:          gConst.PET_KING_RANK_MAX + 1,
					FightInfoJson: "[]",
				}
				gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE, fd.Id, fd.RoleId, fd.CfgId, fd.Kind,
					fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
				petKingFeastRank.FeastDone = fd
			}

			fd := make([]*data.FeastDone, 0, 2048)
			gSql.MustSelect(&fd, gConst.SQL_SELECT_ALL_FEAST_DONE_PET_KING_RANK, protocol.FeastKind_Pet_King)
			if len(fd) > 0 {

				sort.SliceStable(fd, func(i, j int) bool {
					if fd[i].Done > fd[j].Done {
						return true
					}
					if fd[i].Done == fd[j].Done {
						if fd[i].UpdateTime < fd[j].UpdateTime {
							return true
						}
					}
					return false
				})
				for index, f := range fd {
					if f.RoleId == 0 {
						continue
					}
					if index%10 == 0 {
						tmp := f
						TimeAfter(ctx, time.Second*time.Duration(index/10), func(ctx *data.Context) {
							r := gRoleMgr.RoleBy(ctx, tmp.RoleId)
							tmp.Name = r.Name
							tmp.Avatar = r.Avatar
						})
					}

					if len(petKingFeastRank.FeastDoneList) < gConst.PET_KING_RANK_MAX {
						petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, f)
						f.Rank = int32(index) + 1
					} else {
						f.Rank = gConst.PET_KING_RANK_MAX + 1
					}
					petKingFeastRank.RoleFeastDoneMap[f.RoleId] = f

					err := json.Unmarshal([]byte(f.FightInfoJson), &f.FightInfoRoleIdList)
					assert.Assert(err == nil, err)
				}
			} else {
				nowSec := time_helper.NowSec()
				gRankMgr.RangeGamePassRank(ctx, 1, 1000, func(ctx *data.Context, rank *data.Rank) bool {
					f := &data.FeastDone{
						Id:            gIdMgr.GenerateId(ctx, gServer.Node.Sid, gConst.ID_TYPE_FEAST_DONE),
						RoleId:        rank.RoleId,
						Kind:          protocol.FeastKind_Pet_King,
						Done:          gConst.PET_KING_RANK_INIT_SCORE,
						Awarded:       false,
						UpdateTime:    nowSec,
						Rank:          rank.Rank,
						FightInfoJson: "[]",
						Name:          rank.Name,
						Avatar:        rank.Avatar,
					}
					petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, f)
					petKingFeastRank.RoleFeastDoneMap[f.RoleId] = f
					gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE, f.Id, f.RoleId, f.CfgId, f.Kind,
						f.Done, f.Awarded, f.FightInfoJson, f.UpdateTime)
					return true
				})
			}
		}
	})

	gEventMgr.When(data.EVENT_MSGID_MINUTELY, func(ctx *data.Context, args interface{}) {
		minuteArgs := args.(*data.EventMinutely)
		if petKingFeastRank == nil || !petKingFeastRank.On {
			cfg := config.GetPetKingFeastCfg()
			isOn, _ := config.InsideTime(cfg.Ons, minuteArgs.Now.Local().Unix(), false)
			isOff, _ := config.InsideTime(cfg.Offs, minuteArgs.Now.Local().Unix(), false)
			if isOn && !isOff {
				mgr.onPetKing(ctx)
			}
		} else if petKingFeastRank.On {
			cfg := config.GetPetKingFeastCfg()
			isOff, _ := config.InsideTime(cfg.Offs, minuteArgs.Now.Local().Unix(), false)
			isOn, _ := config.InsideTime(cfg.Ons, minuteArgs.Now.Local().Unix(), true)

			if minuteArgs.NowSec%10 == 0 {
				mgr.refreshPetKing(ctx, false, false)
			}

			if isOff || !isOn {
				petKingFeastRank.On = false
				mgr.offPetKing(ctx)
			}
		}

	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastPetKingInfo, func(ctx *data.Context, args *protocol.Envelope) {
		mgr.NotifyPetKingRankInfo(ctx, ctx.RoleId, true)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetFeastPetKingRankList, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SGetFeastPetKingRankList()
		)
		mgr.GetRankList(ctx, ctx.RoleId, rep.RankBegin, rep.RankEnd)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_RefreshPetKing, func(ctx *data.Context, args *protocol.Envelope) {
		myInfo := mgr.RolePetKingFeastBy(ctx, ctx.RoleId)
		if myInfo != nil {
			if !petKingFeastRank.On {
				return
			}
			if len(args.AdSdkToken) == 0 {
				gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Pet_King_Free_Refresh)
			}
			mgr.refreshPetKingFighList(ctx, myInfo)
			mgr.NotifyPetKingRankInfo(ctx, ctx.RoleId, false)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_WinPetKing, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SWinPetKing()
		)
		if !petKingFeastRank.On {
			return
		}
		if len(args.AdSdkToken) > 0 {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Pet_King_Ad_Fight)
		} else {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Pet_King_Free_Fight)
		}
		mgr.Win(ctx, ctx.RoleId, rep.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_LosePetKing, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			rep = args.GetC2SLosePetKing()
		)
		if !petKingFeastRank.On {
			return
		}
		if len(args.AdSdkToken) > 0 {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Pet_King_Ad_Fight)
		} else {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Pet_King_Free_Fight)
		}
		mgr.Lose(ctx, ctx.RoleId, rep.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetKingFightRecordList, func(ctx *data.Context, args *protocol.Envelope) {
		mgr.GetPetKingFightRecord(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetKingAward, func(ctx *data.Context, args *protocol.Envelope) {
		mgr.PetKingAward(ctx, ctx.RoleId)
	})
}

func (mgr *FeastManager) refreshPetKing(ctx *data.Context, isNewFeast bool, setRole bool) {
	if petKingFeastRank == nil {
		return
	}
	fd := petKingFeastRank.FeastDoneList
	sort.SliceStable(fd, func(i, j int) bool {
		if fd[i].Done > fd[j].Done {
			return true
		}
		if fd[i].Done == fd[j].Done {
			return fd[i].UpdateTime < fd[j].UpdateTime
		}
		return false
	})
	petKingFeastRank.FeastDoneList = make([]*data.FeastDone, 0, 3000)
	if petKingFeastRank.RoleFeastDoneMap == nil {
		petKingFeastRank.RoleFeastDoneMap = make(map[int64]*data.FeastDone)
	}
	for index, f := range fd {
		if f.RoleId == 0 {
			continue
		}

		f.Done = util.ChoiceInt32(isNewFeast, 1000, f.Done)
		if isNewFeast {
			f.UpdateTime = ctx.NowSec + int64(index)
			f.Awarded = false
		}
		if len(petKingFeastRank.FeastDoneList) < gConst.PET_KING_RANK_MAX {
			petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, f)
			f.Rank = int32(index) + 1
		} else {
			f.Rank = gConst.PET_KING_RANK_MAX + 1
		}
		if setRole {
			if index%10 == 0 {
				tmp := f
				TimeAfter(ctx, time.Second*time.Duration(index/10), func(ctx *data.Context) {
					r := gRoleMgr.RoleBy(ctx, tmp.RoleId)
					tmp.Name = r.Name
					tmp.Avatar = r.Avatar
				})
			}
		}
		petKingFeastRank.RoleFeastDoneMap[f.RoleId] = f
		err := json.Unmarshal([]byte(f.FightInfoJson), &f.FightInfoRoleIdList)
		assert.Assert(err == nil, err)
	}
	if isNewFeast {
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE_ALL_FIGHT_INFO, gConst.PET_KING_RANK_INIT_SCORE, false, protocol.FeastKind_Pet_King)
	}
}

func (mgr *FeastManager) RefreshPetKing(ctx *data.Context, isNewFeast bool) {
	if petKingFeastRank == nil {
		return
	}
	mgr.refreshPetKing(ctx, isNewFeast, false)
}

func (mgr *FeastManager) onPetKing(ctx *data.Context) {
	nowSec := time_helper.NowSec()
	gSql.Begin()
	var isNewFeast bool
	topFd := make([]*data.FeastDone, 0, 2)
	gSql.MustSelect(&topFd, gConst.SQL_SELECT_FEAST_DONE_PET_KING_RANK, protocol.FeastKind_Pet_King)
	if len(topFd) > 0 {
		f := topFd[0]
		err := json.Unmarshal([]byte(f.FightInfoJson), &f.FightInfoRoleIdList)
		assert.Assert(err == nil, err)
		petKingFeastRank.FeastDone = f
	} else {
		fd := &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			Kind:          protocol.FeastKind_Pet_King,
			Done:          int32(nowSec),
			Rank:          gConst.PET_KING_RANK_MAX + 1,
			FightInfoJson: "[]",
		}
		gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE, fd.Id, fd.RoleId, fd.CfgId, fd.Kind,
			fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
		petKingFeastRank.FeastDone = fd
	}

	// 计算是否同届
	lastDay := time_helper.NowDay(int64(petKingFeastRank.FeastDone.Done))
	nowDay := time_helper.NowDay(nowSec)
	if nowDay-lastDay >= 5 {
		isNewFeast = true
		petKingFeastRank.FeastDone.Done = int32(nowSec)
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, petKingFeastRank.FeastDone.Done, petKingFeastRank.FeastDone.Awarded, petKingFeastRank.FeastDone.Id)
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, petKingFeastRank.FeastDone.Done, petKingFeastRank.FeastDone.Awarded, petKingFeastRank.FeastDone.Id)
	}

	fd := make([]*data.FeastDone, 0, 2048)
	gSql.MustSelect(&fd, gConst.SQL_SELECT_ALL_FEAST_DONE_PET_KING_RANK, protocol.FeastKind_Pet_King)

	petKingFeastRank.FeastDoneList = fd
	if len(fd) > 0 {
		mgr.refreshPetKing(ctx, isNewFeast, true)
	} else {
		petKingFeastRank.FeastDoneList = petKingFeastRank.FeastDoneList[:0]
		petKingFeastRank.RoleFeastDoneMap = make(map[int64]*data.FeastDone)
		gRankMgr.RangeGamePassRank(ctx, 1, 1000, func(ctx *data.Context, rank *data.Rank) bool {
			f := &data.FeastDone{
				Id:            gIdMgr.GenerateId(ctx, gServer.Node.Sid, gConst.ID_TYPE_FEAST_DONE),
				RoleId:        rank.RoleId,
				Kind:          protocol.FeastKind_Pet_King,
				Done:          gConst.PET_KING_RANK_INIT_SCORE,
				Awarded:       false,
				UpdateTime:    nowSec,
				Rank:          rank.Rank,
				FightInfoJson: "[]",
				Name:          rank.Name,
				Avatar:        rank.Avatar,
			}
			petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, f)
			petKingFeastRank.RoleFeastDoneMap[f.RoleId] = f
			gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE, f.Id, f.RoleId, f.CfgId, f.Kind,
				f.Done, f.Awarded, f.FightInfoJson, f.UpdateTime)
			return true
		})
	}

	var roleList []*data.Role
	gSql.MustSelect(&roleList, gConst.SQL_SELECT_ALL_PET_KING_ROLE)
	sort.SliceStable(roleList, func(i, j int) bool {
		return roleList[i].Level > roleList[j].Level
	})
	petKingFeastRank.RoleLvList = roleList
	petKingFeastRank.On = true

	gShareMgr.OnPetKingOn()
}

func (mgr *FeastManager) offPetKing(ctx *data.Context) {
	logs.Info("offPetKing")
	fd := petKingFeastRank.FeastDone
	var top3 []int64
	for _, f := range petKingFeastRank.FeastDoneList {
		top3 = append(top3, f.RoleId)
		if len(top3) == 3 {
			break
		}
	}

	var isNew bool
	if fd == nil {
		fd = &data.FeastDone{
			Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
			Kind:          protocol.FeastKind_Pet_King,
			Done:          0,
			Rank:          gConst.PET_KING_RANK_MAX + 1,
			FightInfoJson: "[]",
		}
		isNew = true
	}
	petKingFeastRank.FeastDone = fd

	fd.FightInfoRoleIdList = top3
	bs, err := json.Marshal(fd.FightInfoRoleIdList)
	assert.Assert(err == nil, err)
	fd.FightInfoJson = string(bs)

	if isNew {
		gSql.MustExcel(gConst.SQL_INSERT_FEAST_DONE, fd.Id, fd.RoleId, fd.CfgId, fd.Kind,
			fd.Done, fd.Awarded, fd.FightInfoJson, fd.UpdateTime)
	} else {
		gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE_FIGHT_INFO, fd.FightInfoJson, fd.Id)
	}
	for _, f := range petKingFeastRank.FeastDoneList {
		f.Awarded = false
	}
	gSql.MustExcel(gConst.SQL_UPDATE_ALL_PET_KING_AWARD, protocol.FeastKind_Pet_King)
	mgr.refreshPetKing(ctx, false, false)

}

func (mgr *FeastManager) getPetKingCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_FEAST_PET_KING, roleId)
}

func (mgr *FeastManager) RolePetKingFeastBy(ctx *data.Context, roleId int64) *data.FeastDone {

	fd := petKingFeastRank.RoleFeastDoneMap[roleId]
	if fd == nil {
		rv, err := mgr.cache.GetOrStore(mgr.getPetKingCacheKey(roleId), func() (interface{}, error) {
			var dataList []*data.FeastDone
			gSql.MustSelect(&dataList, gConst.SQL_SELECT_ALL_FEAST_DONE, roleId, protocol.FeastKind_Pet_King)
			if len(dataList) > 0 {
				return dataList[0], nil
			}
			return nil, nil
		}, gConst.SERVER_DATA_CACHE_TIME_OUT)
		assert.Assert(err == nil, err)
		if rv != nil {
			fd = rv.(*data.FeastDone)
			fd.Rank = gConst.PET_KING_RANK_MAX + 1
			petKingFeastRank.RoleFeastDoneMap[roleId] = fd
		}
	}

	return fd
}

func (mgr *FeastManager) GetRankList(ctx *data.Context, roleId int64, begin int32, end int32) {
	begin = util.MaxInt32(0, util.MinInt32(begin-1, int32(len(petKingFeastRank.FeastDoneList)-1)))
	end = util.MaxInt32(0, util.MinInt32(end, int32(len(petKingFeastRank.FeastDoneList))))
	pbList := make([]*protocol.FeastPetKingRankInfo, 0, end-begin+1)
	for i := begin; i < end; i++ {
		fd := petKingFeastRank.FeastDoneList[i]
		pbList = append(pbList, &protocol.FeastPetKingRankInfo{
			RoleId:  fd.RoleId,
			Name:    fd.Name,
			Avatar:  fd.Avatar,
			Rank:    fd.Rank,
			Score:   fd.Done,
			Awarded: fd.Awarded,
		})
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetFeastPetKingRankListDoneMsg(ctx.EventMsg.GetEnvMsg(), pbList))
}

func (mgr *FeastManager) newRoleRankInfo(ctx *data.Context, roleId int64) *data.FeastDone {
	role := gRoleMgr.RoleBy(ctx, roleId)
	myRankInfo := &data.FeastDone{
		Id:            gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_FEAST_DONE),
		RoleId:        roleId,
		Kind:          protocol.FeastKind_Pet_King,
		Done:          gConst.PET_KING_RANK_INIT_SCORE,
		Awarded:       false,
		UpdateTime:    ctx.NowSec,
		FightInfoJson: "[]",
		Name:          role.Name,
		Avatar:        role.Avatar,
	}

	petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, myRankInfo)
	myRankInfo.Rank = util.ChoiceInt32(
		len(petKingFeastRank.FeastDoneList) > gConst.PET_KING_RANK_MAX,
		gConst.PET_KING_RANK_MAX+1,
		int32(len(petKingFeastRank.FeastDoneList)),
	)
	petKingFeastRank.RoleFeastDoneMap[roleId] = myRankInfo
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_FEAST_DONE,
		myRankInfo.Id, myRankInfo.RoleId, myRankInfo.CfgId, myRankInfo.Kind, myRankInfo.Done, myRankInfo.Awarded, myRankInfo.FightInfoJson, myRankInfo.UpdateTime)
	mgr.cache.Set(mgr.getPetKingCacheKey(roleId), myRankInfo, gConst.SERVER_DATA_CACHE_TIME_OUT)
	return myRankInfo
}

func (mgr *FeastManager) RankInfoPbByRank(ctx *data.Context, rankInfo *data.FeastDone) *protocol.FeastPetKingRankInfo {
	return &protocol.FeastPetKingRankInfo{
		RoleId:  rankInfo.RoleId,
		Name:    rankInfo.Name,
		Avatar:  rankInfo.Avatar,
		Rank:    rankInfo.Rank,
		Score:   rankInfo.Done,
		Awarded: rankInfo.Awarded,
	}
}

func (mgr *FeastManager) NotifyPetKingRankInfo(ctx *data.Context, roleId int64, feast bool) *protocol.FeastPetKingRankInfo {

	var myRankInfoPb *protocol.FeastPetKingRankInfo
	myRankInfo := mgr.RolePetKingFeastBy(ctx, roleId)

	var fightList []*protocol.FeastPetKingRankInfo

	if petKingFeastRank.On {
		if myRankInfo == nil {
			myRankInfo = mgr.newRoleRankInfo(ctx, roleId)
		}
		myRankInfoPb = mgr.RankInfoPbByRank(ctx, myRankInfo)
		if len(myRankInfo.FightInfoRoleIdList) == 0 {
			mgr.refreshPetKingFighList(ctx, myRankInfo)
		}
		for _, rid := range myRankInfo.FightInfoRoleIdList {
			otherClient := mgr.getFeastPetKingRankInfo(ctx, rid)

			fightList = append(fightList, otherClient)
		}
	} else {

		if myRankInfo == nil {
			role := gRoleMgr.RoleBy(ctx, roleId)
			myRankInfoPb = &protocol.FeastPetKingRankInfo{
				RoleId:  roleId,
				Name:    role.Name,
				Avatar:  role.Avatar,
				Rank:    gConst.PET_KING_RANK_MAX + 1,
				Score:   0,
				Awarded: true,
			}
		} else {
			myRankInfoPb = mgr.RankInfoPbByRank(ctx, myRankInfo)
		}

	}

	var feastInfo *protocol.FeastPetKing
	if feast {
		feastInfo = &protocol.FeastPetKing{
			IsFightTime: petKingFeastRank.On,
		}
		feastInfo.TopList = mgr.getPetKingTop(ctx, roleId)

	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFeastPetKingInfoMsg(myRankInfoPb, fightList, feastInfo))

	return myRankInfoPb

}

func (mgr *FeastManager) getPetKingTop(ctx *data.Context, roleId int64) []*protocol.FeastPetKingRankInfo {

	pbList := make([]*protocol.FeastPetKingRankInfo, 0, 3)
	for i := 0; i < 3; i++ {
		if len(petKingFeastRank.FeastDoneList) > i {
			pbList = append(pbList, mgr.getFeastPetKingRankInfo(ctx, petKingFeastRank.FeastDoneList[i].RoleId))
		}
	}
	return pbList
}

func (mgr *FeastManager) getFeastPetKingRankInfo(ctx *data.Context, roleId int64) *protocol.FeastPetKingRankInfo {

	otherClient := &protocol.FeastPetKingRankInfo{}
	empty := true

	r := gRoleMgr.RoleBy(ctx, roleId)
	otherFd := petKingFeastRank.RoleFeastDoneMap[r.RoleId]

	otherClient.RoleId = r.RoleId
	otherClient.Avatar = r.Avatar
	otherClient.Name = r.Name
	if otherFd == nil {
		otherClient.Rank = gConst.PET_KING_RANK_MAX + 1
		otherClient.Score = gConst.PET_KING_RANK_INIT_SCORE
	} else {
		otherClient.Score = otherFd.Done
	}
	otherClient.StubInfo, empty = gStubMgr.ViewOtherStub(ctx, otherClient.RoleId, protocol.StubKind_StubPetKing)
	if empty {
		otherClient.StubInfo, _ = gStubMgr.ViewOtherStub(ctx, otherClient.RoleId, protocol.StubKind_StubNormal)
	}
	otherClient.FightRole = gViewMgr.ViewFightRole(ctx, r)

	return otherClient
}

func (mgr *FeastManager) refreshPetKingFighList(ctx *data.Context, rankInfo *data.FeastDone) {
	rankInfo.FightInfoRoleIdList = make([]int64, 0, gConst.PET_KING_GET_RANK_INFO_AMOUNT)
	myRankIndex := rankInfo.Rank - 1
	var b, e int32
	findMap := make(map[int64]bool)

	b = util.ChoiceInt32(myRankIndex < 15, 0, myRankIndex-15)
	e = util.ChoiceInt32(myRankIndex+15 >= gConst.PET_KING_RANK_MAX-1, gConst.PET_KING_RANK_MAX-1, myRankIndex+15)
	e = util.MinInt32(e, int32(len(petKingFeastRank.FeastDoneList)-1))

	list := petKingFeastRank.FeastDoneList[b : e+1]
	if len(list) > 3 {
		perms := rand.Perm(len(list))
		permsIndex := -1
		for i := 0; i < gConst.PET_KING_GET_RANK_INFO_AMOUNT; i++ {
			if permsIndex == -1 {
				permsIndex++
			}
			listIndex := perms[permsIndex]
			if list[listIndex].RoleId == rankInfo.RoleId {
				permsIndex++
			}
			listIndex = perms[permsIndex]
			if findMap[list[listIndex].RoleId] {
				permsIndex++
			}
			if permsIndex >= len(perms) {
				break
			}
			listIndex = perms[permsIndex]
			permsIndex++
			findMap[list[listIndex].RoleId] = true
			rankInfo.FightInfoRoleIdList = append(rankInfo.FightInfoRoleIdList, list[listIndex].RoleId)
		}
	}

	// 检查找到的人是否够3个
	if len(rankInfo.FightInfoRoleIdList) < gConst.PET_KING_GET_RANK_INFO_AMOUNT {
		// 相差的人数
		sub := gConst.PET_KING_GET_RANK_INFO_AMOUNT - len(rankInfo.FightInfoRoleIdList)
		rid := rankInfo.RoleId
		for i := 0; i < sub; i++ {
			var count int32
			for rid == rankInfo.RoleId || findMap[rid] {
				r := util.Random(0, len(petKingFeastRank.RoleLvList)-1)
				rid = petKingFeastRank.RoleLvList[r].RoleId

				count++
				if count == 10 {
					break
				}
			}
			findMap[rid] = true
			rankInfo.FightInfoRoleIdList = append(rankInfo.FightInfoRoleIdList, rid)
		}
	}
	bs, err := json.Marshal(rankInfo.FightInfoRoleIdList)
	assert.Assert(err == nil, err)
	rankInfo.FightInfoJson = string(bs)
	gSql.Begin().Clean(rankInfo.RoleId).MustExcel(gConst.SQL_UPDATE_FEAST_DONE_FIGHT_INFO, rankInfo.FightInfoJson, rankInfo.Id)
}

func (mgr *FeastManager) RefreshPetKingFighList(ctx *data.Context, roleId int64) {
	myRankInfo := mgr.RolePetKingFeastBy(ctx, roleId)
	assert.Assert(myRankInfo != nil, "myRankInfo == nil")
	mgr.refreshPetKingFighList(ctx, myRankInfo)
	mgr.NotifyPetKingRankInfo(ctx, roleId, false)
}

func (mgr *FeastManager) Win(ctx *data.Context, roleId int64, fighterId int64) {
	// A FIGHT B
	A_Fighter := mgr.RolePetKingFeastBy(ctx, roleId)
	assert.Assert(A_Fighter != nil, "pet king not open, roleId:%d", roleId)
	oldRank := A_Fighter.Rank

	gSql.Begin().Clean(roleId)
	gSql.Begin().Clean(fighterId)

	A_Score := util.ChoiceInt32(A_Fighter.FightWinAmount == 1, 5, util.ChoiceInt32(A_Fighter.FightWinAmount == 0, 10, 2))
	A_Score = 10

	A_Fighter.Done += A_Score
	A_Fighter.FightWinAmount++
	A_Fighter.UpdateTime = ctx.NowSec

	gSql.MustExcel(gConst.SQL_UPDATE_PET_KING_SCORE, A_Fighter.Done, A_Fighter.FightWinAmount, A_Fighter.FightLoseAmount, A_Fighter.UpdateTime, A_Fighter.Id)

	B_Fighter := mgr.RolePetKingFeastBy(ctx, fighterId)

	var B_Score, B_Done int32
	B_Done = gConst.PET_KING_RANK_INIT_SCORE
	if B_Fighter != nil {
		B_Score = util.ChoiceInt32(B_Fighter.FightLoseAmount == 1, 4, util.ChoiceInt32(B_Fighter.FightLoseAmount == 0, 8, 1))
		B_Score = 5
		B_Fighter.Done -= B_Score
		B_Done = B_Fighter.Done
		B_Fighter.Done = util.MaxInt32(B_Fighter.Done, 0)
		B_Fighter.FightLoseAmount++
		B_Fighter.UpdateTime = ctx.NowSec
		gSql.MustExcel(gConst.SQL_UPDATE_PET_KING_SCORE, B_Fighter.Done, B_Fighter.FightWinAmount, B_Fighter.FightLoseAmount, A_Fighter.UpdateTime, B_Fighter.Id)

		if B_Fighter.Rank <= gConst.PET_KING_RANK_MAX {
			lastInfo := petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1]
			B_Rank := B_Fighter.Rank
			if B_Fighter.Done <= lastInfo.Done {
				petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1] = B_Fighter
				B_Fighter.Rank = int32(len(petKingFeastRank.FeastDoneList))

				petKingFeastRank.FeastDoneList[B_Rank-1] = lastInfo
			}
		}

	}
	mgr.AddFightRecord(ctx, roleId, A_Fighter.RoleId, A_Score, fighterId, -B_Score)

	// 入榜
	if A_Fighter.Rank > gConst.PET_KING_RANK_MAX {
		if len(petKingFeastRank.FeastDoneList) < gConst.PET_KING_RANK_MAX {
			petKingFeastRank.FeastDoneList = append(petKingFeastRank.FeastDoneList, A_Fighter)
		} else {
			lastInfo := petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1]
			if A_Fighter.Done > lastInfo.Done {
				A_Fighter.Rank = lastInfo.Rank
				lastInfo.Rank = gConst.PET_KING_RANK_MAX + 1
				petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1] = A_Fighter
			}
		}

	}
	mgr.SortPetKingRank(ctx)

	mgr.refreshPetKingFighList(ctx, A_Fighter)
	mgr.NotifyPetKingRankInfo(ctx, roleId, false)
	A_Role := gRoleMgr.RoleBy(ctx, A_Fighter.RoleId)
	B_Role := gRoleMgr.RoleBy(ctx, fighterId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeWinPetKingDoneMsg(ctx.EventMsg.GetEnvMsg(),
		&protocol.FeastPetKingRankInfo{
			RoleId: A_Fighter.RoleId,
			Name:   A_Role.Name,
			Avatar: A_Role.Avatar,
			Rank:   A_Fighter.Rank,
			Score:  A_Fighter.Done,
		},
		A_Score,
		&protocol.FeastPetKingRankInfo{
			RoleId: B_Role.RoleId,
			Name:   B_Role.Name,
			Avatar: B_Role.Avatar,
			Rank:   0,
			Score:  B_Done,
		},
		-B_Score,
		oldRank-A_Fighter.Rank,
	))
}

func (mgr *FeastManager) Lose(ctx *data.Context, roleId int64, fighterId int64) {
	// A FIGHT B
	A_Fighter := mgr.RolePetKingFeastBy(ctx, roleId)
	assert.Assert(A_Fighter != nil, "pet king not open, roleId:%d", roleId)
	oldRank := A_Fighter.Rank

	gSql.Begin().Clean(roleId)

	A_Score := util.ChoiceInt32(A_Fighter.FightWinAmount == 1, 4, util.ChoiceInt32(A_Fighter.FightWinAmount == 0, 8, 1))
	A_Score = 8

	A_Fighter.Done -= A_Score
	A_Fighter.Done = util.MaxInt32(A_Fighter.Done, 0)
	A_Fighter.FightLoseAmount++
	A_Fighter.UpdateTime = ctx.NowSec

	gSql.MustExcel(gConst.SQL_UPDATE_PET_KING_SCORE, A_Fighter.Done, A_Fighter.FightWinAmount, A_Fighter.FightLoseAmount, A_Fighter.UpdateTime, A_Fighter.Id)

	mgr.AddFightRecord(ctx, roleId, A_Fighter.RoleId, -A_Score, fighterId, 0)

	// A 出榜
	if A_Fighter.Rank <= gConst.PET_KING_RANK_MAX {
		lastInfo := petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1]
		A_Rank := A_Fighter.Rank
		if A_Fighter.Done <= lastInfo.Done {
			petKingFeastRank.FeastDoneList[len(petKingFeastRank.FeastDoneList)-1] = A_Fighter
			A_Fighter.Rank = int32(len(petKingFeastRank.FeastDoneList))

			petKingFeastRank.FeastDoneList[A_Rank-1] = lastInfo
		}
	}
	var B_Score int32
	B_Fighter := mgr.RolePetKingFeastBy(ctx, fighterId)
	if B_Fighter != nil {
		B_Score = B_Fighter.Done
	} else {
		B_Score = gConst.PET_KING_RANK_INIT_SCORE
	}

	mgr.SortPetKingRank(ctx)
	mgr.NotifyPetKingRankInfo(ctx, roleId, false)

	A_Role := gRoleMgr.RoleBy(ctx, A_Fighter.RoleId)
	B_Role := gRoleMgr.RoleBy(ctx, fighterId)

	gRoleMgr.Send(ctx, roleId, protocol.MakeLosePetKingDoneMsg(ctx.EventMsg.GetEnvMsg(),
		&protocol.FeastPetKingRankInfo{
			RoleId: A_Fighter.RoleId,
			Name:   A_Role.Name,
			Avatar: A_Role.Avatar,
			Rank:   A_Fighter.Rank,
			Score:  A_Fighter.Done,
		},
		-A_Score,
		&protocol.FeastPetKingRankInfo{
			RoleId: B_Role.RoleId,
			Name:   B_Role.Name,
			Avatar: B_Role.Avatar,
			Rank:   0,
			Score:  B_Score,
		},
		0,
		oldRank-A_Fighter.Rank,
	))

}

func (mgr *FeastManager) SortPetKingRank(ctx *data.Context) {
	sort.SliceStable(petKingFeastRank.FeastDoneList, func(i, j int) bool {
		if petKingFeastRank.FeastDoneList[i].Done > petKingFeastRank.FeastDoneList[j].Done {
			return true
		}
		if petKingFeastRank.FeastDoneList[i].Done == petKingFeastRank.FeastDoneList[j].Done {
			if petKingFeastRank.FeastDoneList[i].UpdateTime < petKingFeastRank.FeastDoneList[j].UpdateTime {
				return true
			}
		}
		return false
	})
	for index, fd := range petKingFeastRank.FeastDoneList {
		fd.Rank = util.MinInt32(int32(index+1), gConst.PET_KING_RANK_MAX+1)
	}
}

func (mgr *FeastManager) PetKingAward(ctx *data.Context, roleId int64) {
	assert.Assert(!petKingFeastRank.On, "!龙王争霸赛已开启")
	fd := mgr.RolePetKingFeastBy(ctx, roleId)
	assert.Assert(!fd.Awarded, "!奖励已领取")
	gSql.Begin().Clean(roleId)
	cfg := config.GetPetKingFeastCfgByRank(fd.Rank)
	gItemMgr.GiveAward(ctx, roleId, cfg.Reward, 1, protocol.ItemActionType_Action_Pet_King)
	fd.Awarded = true
	gSql.MustExcel(gConst.SQL_UPDATE_FEAST_DONE, fd.Done, fd.Awarded, fd.Id)
	gRoleMgr.Send(ctx, roleId, protocol.MakePetKingAwardDoneMsg(ctx.EventMsg.GetEnvMsg()))
}

func (mgr *FeastManager) AddFightRecord(
	ctx *data.Context, roleId int64,
	atkerId int64, atkScore int32,
	definerId int64, defScore int32,
) {
	atkerRole := gRoleMgr.RoleBy(ctx, atkerId)
	deferRole := gRoleMgr.RoleBy(ctx, definerId)
	gSql.Begin().MustExcel(gConst.SQL_INSERT_PET_KING_RECORD,
		gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_PET_KING_RECORD),
		atkerRole.RoleId, atkerRole.Name, atkerRole.Avatar, atkScore,
		deferRole.RoleId, deferRole.Name, deferRole.Avatar, defScore,
		time.Now(),
	)
}

func (mgr *FeastManager) GetPetKingFightRecord(ctx *data.Context, roleId int64) {
	var atkerList, deferList []*data.PetKingFightRecord
	gSql.MustSelect(&atkerList, gConst.SQL_SELECT_PET_KING_ATKER_RECORD, roleId)
	gSql.MustSelect(&deferList, gConst.SQL_SELECT_PET_KING_DEFER_RECORD, roleId)

	var allList []*protocol.PetKingFightRecord
	for _, atker := range atkerList {
		record := &protocol.PetKingFightRecord{
			Score: atker.AtkerScore,
		}
		record.Foe = &protocol.FeastPetKingRankInfo{
			RoleId: atker.DeferId,
			Name:   atker.DeferName,
			Avatar: atker.DeferAvatar,
			Score:  gConst.PET_KING_RANK_INIT_SCORE,
		}
		foeFd := mgr.RolePetKingFeastBy(ctx, atker.DeferId)
		if foeFd != nil {
			record.Foe.Score = foeFd.Done
		}
		allList = append(allList, record)
	}
	for _, definer := range deferList {
		record := &protocol.PetKingFightRecord{
			Score: definer.DeferScore,
		}
		record.Foe = &protocol.FeastPetKingRankInfo{
			RoleId: definer.AtkerId,
			Name:   definer.AtkerName,
			Avatar: definer.AtkerAvatar,
			Score:  gConst.PET_KING_RANK_INIT_SCORE,
		}
		foeFd := mgr.RolePetKingFeastBy(ctx, definer.AtkerId)
		if foeFd != nil {
			record.Foe.Score = foeFd.Done
		}
		allList = append(allList, record)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeGetPetKingFightRecordListDoneMsg(ctx.EventMsg.GetEnvMsg(), allList))
}
