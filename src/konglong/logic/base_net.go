package logic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/netWork"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

func InitNet(port int32) {
	// 网络初始化
	netServer := netWork.NewWebSocketServer("/ws", "0.0.0.0", port)

	netServer.SetOnConnectFunc(onConnect)
	netServer.SetOnDataFunc(onData)
	netServer.SetOnCloseFunc(onClose)
	netServer.SetIdleTimeOut(time.Second * 200)
	netServer.SetReadTimeOut(time.Second * 200)
	netServer.SetWriteTimeOut(time.Second * 200)
	netServer.SetLog(logs.GetLogger())
	gServer.Net = netServer

	HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
	})
}

func HandleFunc(pattern string, handle func(w http.ResponseWriter, r *http.Request)) {
	pattern = fmt.Sprintf("/game%03d%s", gAppCnf.Server.Sid, pattern)
	err := gServer.Net.HandleFunc(pattern, func(hw http.ResponseWriter, hr *http.Request) {
		defer hr.Body.Close()
		logs.Infof("Handle %+v", hr.RequestURI)
		ch := make(chan int32)
		hw.Header().Set("access-control-allow-origin", "*")
		hw.Header().Set("Access-Control-Allow-Credentials", "true")
		hr.ParseForm()
		roleId := util.AtoInt64(hr.Form.Get("role_id"))
		var session *data.Session
		if roleId > 0 {
			session = gRoleMgr.RoleSessionBy(nil, roleId)
		} else {
			session = gSessionMgr.NewHttpSession(hw, hr)
		}
		TimeAfterFunc(nil, 0,
			func(ctx *data.Context) {
				handle(hw, hr)
			},
			func(errMsg interface{}) {
				if errMsg != nil {
					SendErrToHttp(hw, errMsg)
				} else {

				}
				ch <- 1
			},
			session,
		)
		<-ch

	})

	assert.Assert(err == nil, err)
}

func MakeHttpResponse(msg interface{}) *protocol.HttpResponse {

	resp := &protocol.HttpResponse{
		Result:   0,
		Msg:      "请求成功!",
		ErrMsg:   "",
		Status:   0,
		DateTime: time_helper.NowMill(),
		Data:     msg,
	}
	return resp
}

func MakeHttpErrResponse(msg interface{}) *protocol.HttpResponse {

	resp := &protocol.HttpResponse{
		Result:   0,
		Msg:      "",
		ErrMsg:   fmt.Sprint(msg),
		Status:   501,
		DateTime: time_helper.NowMill(),
	}
	return resp
}

func SendToHttp(w http.ResponseWriter, msg interface{}) {
	respMsg := MakeHttpResponse(msg)
	bs, err := json.Marshal(respMsg)
	assert.Assert(err == nil, err)
	_, err = w.Write(bs)
	assert.Assert(err == nil, err)
	if len(bs) <= 1024 {
		logs.Infof("SendToHttp %s", string(bs))
	}

}

func SendErrToHttp(w http.ResponseWriter, errMsg interface{}) {
	respMsg := MakeHttpErrResponse(errMsg)
	bs, err := json.Marshal(respMsg)
	assert.Assert(err == nil, err)
	_, err = w.Write(bs)
	assert.Assert(err == nil, err)
	if len(bs) <= 1024 {
		logs.Infof("SendErrToHttp %s", string(bs))
	}

}
