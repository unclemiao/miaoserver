package logic

import (
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"time"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type LotteryItemManager struct {
}

func init() {
	gLotteryItemMgr = &LotteryItemManager{}
}

func (mgr *LotteryItemManager) Init() {

	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryItem, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Lottery(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetLotteryItemInfo, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.GetLotteryInfo(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_SetLotteryItemTenRate, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SSetLotteryItemTenRate()
		rep.AdSdk = len(msg.AdSdkToken) > 0
		mgr.SetTenRate(ctx, ctx.RoleId, rep.AdSdk)
	})

}

func (mgr *LotteryItemManager) GetLotteryInfo(ctx *data.Context, roleId int64) {
	nowSec := time_helper.NowSec()
	roleQuota := gQuotaCdMgr.QuotaBy(ctx, roleId)
	roleCd := gQuotaCdMgr.CdBy(ctx, roleId)
	if nowSec >= int64(roleCd.LotteryItem) {
		add := int32(((nowSec - int64(roleCd.LotteryItem)) / gConst.LOTTERY_ITEM_QUOTA_CD) + 1)
		add = util.MinInt32(add, roleQuota.LotteryItem)
		if add > 0 {
			gQuotaCdMgr.CleanCd(ctx, roleId, nowSec, protocol.CdType_CD_Lottery_Item_Quota_Cd)
			gQuotaCdMgr.GiveQuota(ctx, roleId, add, protocol.QuotaType_Quota_Lottery_Item)
		}
	}
}

func (mgr *LotteryItemManager) Lottery(ctx *data.Context, roleId int64) {
	r := gRoleMgr.RoleBy(ctx, roleId)
	cfgList, _ := config.GetLotteryItemCfgList()
	rate := r.LotteryItemRate
	var randIndex int32
	if r.Newbie == int32(protocol.GuideType_zhuanpan) {
		randIndex = 5
	} else if rate == 10 {
		randIndex = 5
	} else {
		randIndex = util.RandomWeight(config.GetLotteryItemCfgWeightList())
		rate = util.MaxInt32(1, rate)
	}
	cfg := cfgList[randIndex]
	r.LotteryItemRate = 1

	roleQuota := gQuotaCdMgr.QuotaBy(ctx, roleId)
	if roleQuota.LotteryItem == 0 {
		gQuotaCdMgr.BeginCd(ctx, roleId, ctx.NowSec, protocol.CdType_CD_Lottery_Item_Quota_Cd)
	}

	result := &data.AwardCfg{}
	if len(cfg.ItemCfgId) == 0 {
		if cfg.ItemDropId > 0 {
			dropCfg := config.ItemDropRandom(cfg.ItemDropId)
			result.AwardList = append(result.AwardList, &data.ItemAmount{
				CfgId: dropCfg.ItemId,
				N:     cfg.Num * int64(rate),
			})
			gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Lottery_Item)
			gItemMgr.GiveAward(ctx, roleId, result, 1, protocol.ItemActionType_Action_Zhuanpan)
		} else {
			r.LotteryItemRate = -10
		}
	} else {
		gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Lottery_Item)
		if cfg.ItemCfgId[0] == gConst.ITEM_CFG_ID_GOLD {
			foodBookLv := gFoodBookMgr.FoodBookBy(ctx, roleId).Level
			foodBookCfg := config.FoodBookCfgBy(foodBookLv)
			for _, st := range foodBookCfg.Stuff.AwardList {
				result.AwardList = append(result.AwardList, &data.ItemAmount{
					CfgId: st.CfgId,
					N:     st.N * int64(5.0*float64(rate)),
				})
			}
			gItemMgr.GiveAward(ctx, roleId, result, 1, protocol.ItemActionType_Action_Zhuanpan)

		} else {
			result.AwardList = append(result.AwardList, &data.ItemAmount{
				CfgId: cfg.ItemCfgId[0],
				N:     cfg.Num * int64(rate),
			})

			gItemMgr.GiveAward(ctx, roleId, result, 1, protocol.ItemActionType_Action_Zhuanpan)

		}

	}
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_LOTTERY_ITEM_RATE, r.LotteryItemRate, r.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeLotteryItemDoneMsg(cfg.CfgId, rate, result.GetForClient()))
}

func (mgr *LotteryItemManager) SetTenRate(ctx *data.Context, roleId int64, adSdk bool) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	if role.LotteryItemRate != -10 {
		return
	}
	role.LotteryItemRate = util.ChoiceInt32(adSdk, 10, 1)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_LOTTERY_ITEM_RATE, role.LotteryItemRate, role.RoleId)
}

// *************************************** 孵化相关 ***************************************
type LotteryPetManager struct {
}

func init() {
	gLotteryPetMgr = &LotteryPetManager{}
}

func (mgr *LotteryPetManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		if args.IsNew {
			mgr.NewLottery(ctx, args.RoleId)
		}
		TimeAfter(ctx, time.Second, func(ctx2 *data.Context) {
			mgr.Notify(ctx2, args.RoleId, nil)
		}, ctx.Session)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryPet, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SLotteryPet()
		)
		mgr.Lottery(ctx, ctx.RoleId, rep.Count, len(msg.AdSdkToken) > 0)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetLotteryPetAwardBox, mgr.HandlerAwardScore)

	gEventMgr.When(protocol.EnvelopeType_C2S_DailyLotteryPet, func(ctx *data.Context, msg *protocol.Envelope) {
		args := msg.GetC2SDailyLotteryPet()
		token := args.GetDoubleAdSdkToken()
		if len(token) > 0 {
			serverToken := gAdminMgr.ServerCmd.Token
			md5 := util.Md5(fmt.Sprintf("C2S_daily_lottery_pet%s", serverToken))
			assert.Assert(md5 == token, "invalid adsdk")
		}
		mgr.DailyLottery(ctx, ctx.RoleId, len(token) > 0)
	})

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(args.RoleId))
	})

}

func (mgr *LotteryPetManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_LOTTERY_PET, roleId)
}

func (mgr *LotteryPetManager) LotteryPetBy(ctx *data.Context, roleId int64) *data.LotteryPet {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.LotteryPet
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_LOTTERY_PET, roleId)
		if len(dataList) > 0 {
			d := dataList[0]
			if d.AwardStr != "" {
				err := json.Unmarshal([]byte(d.AwardStr), &d.AwardInfo)
				if err != nil {
					return nil, err
				}
			} else {

			}
			return d, nil
		}
		return nil, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.LotteryPet)
}

func (mgr *LotteryPetManager) NewLottery(ctx *data.Context, roleId int64) *data.LotteryPet {
	info := &data.LotteryPet{
		RoleId:    roleId,
		Score:     0,
		AwardStr:  "{}",
		AwardInfo: nil,
		IsFirst:   true,
		IsSecond:  true,
	}
	for _, scoreCfg := range config.GetLotteryPetScoreCfgMap() {
		info.AwardInfo = append(info.AwardInfo, &data.LotteryPetAward{
			CfgId: scoreCfg.CfgId,
			Score: scoreCfg.Score,
			Award: 0,
		})
	}
	sort.Slice(info.AwardInfo, func(i, j int) bool {
		return info.AwardInfo[i].Score < info.AwardInfo[j].Score
	})

	bs, err := json.Marshal(info.AwardInfo)
	assert.Assert(err == nil, err)
	info.AwardStr = string(bs)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_LOTTERY_PET, info.RoleId, info.Score, info.AwardStr, info.IsFirst, info.IsSecond)

	gCache.Set(mgr.getCacheKey(roleId), info, gConst.SERVER_DATA_CACHE_TIME_OUT)
	return info
}

func (mgr *LotteryPetManager) Notify(ctx *data.Context, roleId int64, info *data.LotteryPet) {
	if info == nil {
		info = mgr.LotteryPetBy(ctx, roleId)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyLotteryPetMsg(info.GetForClient()))
}

func (mgr *LotteryPetManager) NotifyUpdate(ctx *data.Context, roleId int64, info *data.LotteryPet) {
	if info == nil {
		info = mgr.LotteryPetBy(ctx, roleId)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyLotteryPetUpdateMsg(info.GetForClient()))
}

func (mgr *LotteryPetManager) OnChangeScore(ctx *data.Context, lottery *data.LotteryPet, allAwarded bool) {
	gSql.Begin().Clean(lottery.RoleId)
	if !allAwarded {
		for _, score := range lottery.AwardInfo {
			if score.Award == 0 && lottery.Score >= score.Score {
				score.Award = 1
			} else if score.Award == 1 && lottery.Score < score.Score {
				score.Award = 0
			}
		}
	} else {
		for _, score := range lottery.AwardInfo {
			score.Award = 0
			if lottery.Score >= score.Score {
				score.Award = 1
			}
		}
	}

	bs, err := json.Marshal(lottery.AwardInfo)
	assert.Assert(err == nil, err)
	lottery.AwardStr = string(bs)
	gSql.MustExcel(gConst.SQL_UPDATE_LOTTERY_PET,
		lottery.Score, lottery.AwardStr,
		lottery.SuperEvolutionBuyTime,
		lottery.SuperEvolutionBuyAmount,
		lottery.RoleId,
	)
}

func (mgr *LotteryPetManager) Lottery(ctx *data.Context, roleId int64, count int32, adSdk bool) {
	assert.Assert(count <= 10, "invalid count")
	gItemMgr.TakeItem(ctx, roleId, nil, util.ChoiceInt64(count == 10, gConst.LOTTERY_PET_COST_ITEM_CFG_ID_TEN, gConst.LOTTERY_PET_COST_ITEM_CFG_ID),
		util.ChoiceInt64(count == 10, gConst.LOTTERY_PET_COST_ITEM_AMOUNT_TEN, gConst.LOTTERY_PET_COST_ITEM_AMOUNT),
		protocol.ItemActionType_Action_Chouka)
	lottery := mgr.LotteryPetBy(ctx, roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	ctx.IsTenLottery = count == 10
	var PetPbList []*protocol.Pet
	var petCfgList []util.WeightItr

	if adSdk {
		petCfgList = config.GetLotteryPetDoubleSCfgWeightList()
	} else if count > 1 {
		petCfgList = config.GetLotteryPetDoubleSCfgWeightList()
	} else {
		petCfgList = config.GetLotteryPetCfgWeightList()
	}
	var hasS bool

	for i := int32(0); i < count; i++ {
		var lotteryPet *data.Pet
		if i < 9 {
			lotteryPet = mgr.LotteryFreeCfg(ctx, roleId, petCfgList, protocol.PetActionType_Pet_Action_Lottery, false)
			if config.GetPetCfg(lotteryPet.CfgId).Grade == 4 {
				hasS = true
			}
		} else if !hasS {
			lotteryPet = mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryTenPetSCfgWeightList(), protocol.PetActionType_Pet_Action_Lottery, false)
		} else {
			lotteryPet = mgr.LotteryFreeCfg(ctx, roleId, petCfgList, protocol.PetActionType_Pet_Action_Lottery, false)

		}
		PetPbList = append(
			PetPbList,
			lotteryPet.GetForClient(),
		)
	}

	// 判定是否购买超进化
	if ctx.NowSec >= lottery.SuperEvolutionBuyTime {
		open := role.GamePass >= 25
		if open {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Super_Evolution_Item_Buy)

			cfg := config.GetSuperEvolutionBuyCfg(quota + 1)
			if util.RandomMatched(cfg.Rate*10) || lottery.SuperEvolutionBuyAmount == cfg.AtMost {
				lottery.SuperEvolutionBuyTime = ctx.NowSec + time_helper.Day
				lottery.SuperEvolutionBuyAmount = 0
			} else {
				lottery.SuperEvolutionBuyAmount++
			}
		}

	}
	lottery.Score += count
	mgr.OnChangeScore(ctx, lottery, false)
	mgr.NotifyUpdate(ctx, roleId, lottery)
	gRoleMgr.Send(ctx, roleId, protocol.MakeLotteryPetDoneMsg(ctx.EventMsg.GetEnvMsg(), count, PetPbList))
	ctx.IsTenLottery = false
}

func (mgr *LotteryPetManager) LotteryFree(ctx *data.Context, roleId int64, why protocol.PetActionType, remote bool) *data.Pet {
	return mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryPetCfgWeightList(), why, remote)
}

func (mgr *LotteryPetManager) LotteryFreeCfg(ctx *data.Context, roleId int64, petCfgList []util.WeightItr, why protocol.PetActionType, remote bool) *data.Pet {
	lotteryPet := mgr.LotteryPetBy(ctx, roleId)
	gSql.Begin().Clean(roleId)

	petCfgId := util.ChoiceInt32(lotteryPet.IsFirst, 22,
		petCfgList[util.RandomWeight(petCfgList)].(*data.LotteryPetCfg).PetCfgId)

	petCfgId = util.ChoiceInt32(!lotteryPet.IsFirst && lotteryPet.IsSecond, 15, petCfgId)

	role := gRoleMgr.RoleBy(ctx, roleId)
	ctx.IsFirstPet = lotteryPet.IsFirst
	ctx.IsSecondPet = lotteryPet.IsSecond
	pet := gPetMgr.GivePetFromCfgId(ctx, roleId, petCfgId, false, lotteryPet.IsFirst || lotteryPet.IsSecond, false, why, remote)
	if ctx.IsTenLottery {
		allPet := gPetMgr.AllPetBy(ctx, ctx.RoleId)
		gPetMgr.LevelUp(ctx, pet.RoleId, pet, util.MaxInt32(pet.Level, int32(math.Ceil(float64(allPet.MaxLv)*0.80))), false)
	}

	petCfg := config.GetPetCfg(petCfgId)

	if lotteryPet.IsFirst {
		lotteryPet.IsFirst = false
		gSql.MustExcel(gConst.SQL_UPDATE_LOTTERY_PET_FIRST, lotteryPet.IsFirst, roleId)
	} else if lotteryPet.IsSecond {
		lotteryPet.IsSecond = false
		gSql.MustExcel(gConst.SQL_UPDATE_LOTTERY_PET_SECOND, lotteryPet.IsSecond, roleId)
	}

	gFeastMgr.OnLetteryPet(ctx, roleId, []*data.Pet{pet})
	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_LOTTERY_PET, 1)

	if pet.Variation {
		gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
			Msg: fmt.Sprintf(config.GetGameTextCfg(
				gConst.TEXT_CFG_ID_VARIATION_PET).Text,
				role.Name,
				role.RoleId,
				petCfg.Name,
				pet.Id,
			),
			MsgType: protocol.MessageType_Msg_System,
		}, 0)
	} else if petCfg.Grade == 4 {
		gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
			Msg: fmt.Sprintf(config.GetGameTextCfg(
				gConst.TEXT_CFG_ID_S_GRADE_PET).Text,
				role.Name,
				role.RoleId,
				petCfg.Name,
				pet.Id,
			),
			MsgType: protocol.MessageType_Msg_System,
		}, 0)

	}

	return pet
}

func (mgr *LotteryPetManager) AwardScore(ctx *data.Context, roleId int64, cfgId int32, petIndex int32) {
	lottery := mgr.LotteryPetBy(ctx, roleId)
	cfg := config.GetLotteryPetScoreCfg(cfgId)
	assert.Assert(cfg.Score <= lottery.Score, "score not enough, score:%d", lottery.Score)
	ok := false
	allAwarded := true
	for _, scoreInfo := range lottery.AwardInfo {
		if scoreInfo.CfgId == cfgId && scoreInfo.Award == 1 {
			ok = true
			scoreInfo.Award = 2
			givePet := cfg.Reward[util.ChoiceInt32(petIndex == 0, util.Random32(0, int32(len(cfg.Reward)-1)), petIndex-1)]
			for i := int32(0); i < givePet[1]; i++ {
				gPetMgr.GivePetFromCfgId(
					ctx, roleId, givePet[0],
					false, false, false,
					protocol.PetActionType_Pet_Action_Lottery_Score, true,
				)
			}

		}
		if scoreInfo.Award != 2 {
			allAwarded = false
		}
	}
	assert.Assert(ok, "score info not found, cfgId:%d", cfgId)
	if allAwarded {
		lottery.Score -= 30
	}
	mgr.OnChangeScore(ctx, lottery, allAwarded)

	mgr.NotifyUpdate(ctx, roleId, lottery)
}

func (mgr *LotteryPetManager) DailyLottery(ctx *data.Context, roleId int64, doubleS bool) {
	lottery := mgr.LotteryPetBy(ctx, roleId)
	var pet *data.Pet
	if lottery.DailyLotteryPetLuck >= 100 {
		if doubleS {
			pet = mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryDailyPetDoubleASCfgWeightList(), protocol.PetActionType_Pet_Action_Daily_Lottey, true)
		} else {
			pet = mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryDailyPetACfgWeightList(), protocol.PetActionType_Pet_Action_Daily_Lottey, true)
		}
	} else {
		if doubleS {
			pet = mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryDailyPetDoubleSCfgWeightList(), protocol.PetActionType_Pet_Action_Daily_Lottey, true)
		} else {
			pet = mgr.LotteryFreeCfg(ctx, roleId, config.GetLotteryDailyPetCfgWeightList(), protocol.PetActionType_Pet_Action_Daily_Lottey, true)

		}
	}
	petCfg := config.GetPetCfg(pet.CfgId)
	if petCfg.Grade >= 3 {
		gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Daily_Free_Lottery_Pet)
		if lottery.DailyLotteryPetLuck >= 100 {
			lottery.DailyLotteryPetLuck = 0
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_LOTTERY_DAILY_LUCK, lottery.DailyLotteryPetLuck, roleId)
			mgr.NotifyUpdate(ctx, roleId, lottery)

		}
	} else if lottery.DailyLotteryPetLuck < 100 {
		lottery.DailyLotteryPetLuck = util.MinInt32(100, lottery.DailyLotteryPetLuck+34)
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_LOTTERY_DAILY_LUCK, lottery.DailyLotteryPetLuck, roleId)
		mgr.NotifyUpdate(ctx, roleId, lottery)
	}

}

// --------------------------------------- controllers -----------------------------------------
func (mgr *LotteryPetManager) HandlerAwardScore(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg = msg.GetC2SGetLotteryPetAwardBox()
	)

	mgr.AwardScore(ctx, ctx.RoleId, repMsg.CfgId, repMsg.ChoicePetIndex)
}

// *************************************** 装备相关 ***************************************
type LotteryEquipManager struct {
}

func init() {
	gLotteryEquipMgr = &LotteryEquipManager{}
}

func (mgr *LotteryEquipManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_LotteryEquip, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SLotteryEquip()
		)
		mgr.LotteryEquip(ctx, ctx.RoleId, rep.Count)
	})
}

func (mgr *LotteryEquipManager) LotteryEquip(ctx *data.Context, roleId int64, count int32) {

	quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	var giveList []*protocol.Item
	role := gRoleMgr.RoleBy(ctx, roleId)

	for i := int32(0); i < count; i++ {
		if quota.LotteryEquip < 9 {
			// 普通抽
			equipCfgId := config.LotteryNormalEquip()
			if role.Newbie == int32(protocol.GuideType_equipChouka) {
				equipCfgId = 20002
			}
			equipCfg := config.GetItemCfg(equipCfgId)
			equips, ok := gItemMgr.giveItem(ctx, roleId, equipCfgId, 1, protocol.KitType_Kit_Role_Equip, protocol.ItemActionType_Action_Lottery_Equip)
			assert.Assert(ok, "背包已满")
			var equip *data.Item
			for e, _ := range equips {
				equip = e
				break
			}
			giveList = append(giveList, gItemMgr.GetForClient(equip))

			if equip.Grade != 4 {
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Lottery_Equip)
			} else if quota.LotteryEquip > 0 {
				gQuotaCdMgr.GiveQuota(ctx, roleId, quota.LotteryEquip, protocol.QuotaType_Quota_Lottery_Equip)
			}

			if equip.Grade == 4 {
				gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
					Msg: fmt.Sprintf(config.GetGameTextCfg(
						gConst.TEXT_CFG_ID_S_GRADE_EQUIP).Text,
						role.Name,
						role.RoleId,
						equipCfg.Name,
						equip.Id,
					),
					MsgType: protocol.MessageType_Msg_System,
				}, 0)
			}

		} else {
			equipCfgId := config.LotterySGradeEquip()
			if role.Newbie == int32(protocol.GuideType_equipChouka) {
				equipCfgId = 20002
			}
			equips, ok := gItemMgr.giveItem(ctx, roleId, equipCfgId, 1, protocol.KitType_Kit_Role_Equip, protocol.ItemActionType_Action_Lottery_Equip)
			assert.Assert(ok, "背包已满")
			var equip *data.Item
			for e, _ := range equips {
				equip = e
				break
			}
			giveList = append(giveList, gItemMgr.GetForClient(equip))
			gQuotaCdMgr.GiveQuota(ctx, roleId, quota.LotteryEquip, protocol.QuotaType_Quota_Lottery_Equip)
		}
	}
	if count == 1 {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Scallop), gConst.LOTTERY_EQUIP_COST_ONE, protocol.ItemActionType_Action_Lottery_Equip)
	} else {
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Scallop), gConst.LOTTERY_EQUIP_COST_TEN, protocol.ItemActionType_Action_Lottery_Equip)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeLotteryEquipDoneMsg(ctx.EventMsg.GetEnvMsg(), count, giveList))
}
