package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"goproject/engine/logs"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type InviteManager struct {
	cache *cache.ShardedCache
}

var gInviteMgr *InviteManager

func init() {
	gInviteMgr = &InviteManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4),
	}
}

func (mgr *InviteManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_INVITE, roleId)
}

func (mgr *InviteManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventMsg := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(eventMsg.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetInviteList, func(ctx *data.Context, msg *protocol.Envelope) {
		roleId := ctx.RoleId
		roleInvites := mgr.RoleInvitesBy(ctx, roleId)
		var awardCfg int32
		if roleInvites.AwardNumInvite != nil {
			awardCfg = roleInvites.AwardNumInvite.AwardNum
		}
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyInviteListMsg(roleInvites.InviteList, awardCfg))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetInviteAward, func(ctx *data.Context, msg *protocol.Envelope) {
		args := msg.GetC2SGetInviteAward()
		mgr.Award(ctx, ctx.RoleId, args.Index)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_BeInvited, func(ctx *data.Context, msg *protocol.Envelope) {
		args := msg.GetC2SBeInvited()
		mgr.BeInvited(ctx, ctx.RoleId, args.InviterRoleId, args.InviterSid)
	})
}

func (mgr *InviteManager) RoleInvitesBy(ctx *data.Context, roleId int64) *data.RoleInvites {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		info := &data.RoleInvites{
			InviteMap: make(map[int64]*data.Invite),
		}
		var dataList []*data.Invite
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_INVITE, roleId)
		for _, d := range dataList {
			if d.InviteRoleId == 0 {
				info.AwardNumInvite = d
			} else {
				info.InviteMap[d.RoleId] = d
				info.InviteList = append(info.InviteList, &protocol.Invite{
					Avater: d.InviteAvater,
					Name:   d.InviteName,
					Time:   d.UpdateTime,
				})
			}

		}
		return info, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	return rv.(*data.RoleInvites)
}

func (mgr *InviteManager) Award(ctx *data.Context, roleId int64, index int32) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	roleInvites := mgr.RoleInvitesBy(ctx, roleId)
	var nextCfg *data.InviteCfg
	if roleInvites.AwardNumInvite == nil {
		nextCfg = config.GetNextInvitationCfg(0)
	} else {
		nextCfg = config.GetNextInvitationCfg(roleInvites.AwardNumInvite.AwardNum)
	}
	assert.Assert(nextCfg != nil, "nextCfg not found, cfgId:%d", nextCfg.CfgId)
	assert.Assert(int32(len(roleInvites.InviteList)) >= nextCfg.Invite, "invite not enough")
	gSql.Begin().Clean(roleId)
	if nextCfg.RandDropId > 0 {
		dropCfg := config.GetRandomPetCfg(nextCfg.RandDropId)
		gPetMgr.GivePetFromCfgId(ctx, roleId, dropCfg.PetId,
			dropCfg.Variation == 1, dropCfg.Variation == 2, false, protocol.PetActionType_Pet_Action_Invite, true)
	}
	if len(nextCfg.ChoiceReward) > 0 && index > 0 {
		gPetMgr.GivePetFromCfgId(ctx, roleId, nextCfg.ChoiceReward[index-1],
			nextCfg.IsVariation == 1, nextCfg.IsVariation == 2, false, protocol.PetActionType_Pet_Action_Invite, true)
	}
	if roleInvites.AwardNumInvite == nil {
		awardNumInvite := &data.Invite{
			Id:       gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_ROLE_INVITE),
			RoleId:   roleId,
			AwardNum: nextCfg.CfgId,
		}
		roleInvites.AwardNumInvite = awardNumInvite
		gSql.MustExcel(gConst.SQL_INSERT_INVITE, awardNumInvite.Id, awardNumInvite.RoleId,
			awardNumInvite.InviteRoleId, awardNumInvite.InviteAvater, awardNumInvite.InviteName, awardNumInvite.AwardNum, awardNumInvite.UpdateTime)
	} else {
		roleInvites.AwardNumInvite.AwardNum = nextCfg.CfgId
		gSql.MustExcel(gConst.SQL_UPDATE_INVITE_AWARD, roleInvites.AwardNumInvite.AwardNum, roleInvites.AwardNumInvite.Id)
	}

	var awardCfg int32
	if roleInvites.AwardNumInvite != nil {
		awardCfg = roleInvites.AwardNumInvite.AwardNum
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyInviteListMsg(nil, awardCfg))
}

func (mgr *InviteManager) InvitesCallBack(ctx *data.Context, roleId int64, inviterRoleId int64, inviterAvter string, inviterName string) {
	roleInvites := mgr.RoleInvitesBy(ctx, roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	if roleInvites.InviteMap[inviterRoleId] != nil {
		logs.Error("roleInvites.InviteMap[inviterRoleId] != nil")
		return
	}
	if int32(len(roleInvites.InviteList)) == config.GetInvitationMaxNum() {
		logs.Error("num max")
		return
	}
	invite := &data.Invite{
		Id:           gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_ROLE_INVITE),
		RoleId:       roleId,
		InviteRoleId: inviterRoleId,
		InviteAvater: inviterAvter,
		InviteName:   inviterName,
		UpdateTime:   time_helper.NowSec(),
	}
	pb := &protocol.Invite{
		Avater: invite.InviteAvater,
		Name:   invite.InviteName,
		Time:   invite.UpdateTime,
	}
	roleInvites.InviteMap[inviterRoleId] = invite
	roleInvites.InviteList = append(roleInvites.InviteList, pb)
	gSql.MustExcel(gConst.SQL_INSERT_INVITE, invite.Id, invite.RoleId, invite.InviteRoleId, invite.InviteAvater, invite.InviteName, invite.AwardNum, invite.UpdateTime)

	var awardCfg int32
	if roleInvites.AwardNumInvite != nil {
		awardCfg = roleInvites.AwardNumInvite.AwardNum
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyInviteListMsg([]*protocol.Invite{pb}, awardCfg))
}

func (mgr *InviteManager) BeInvited(ctx *data.Context, roleId int64, fromRoleId int64, fromSid int32) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	if !role.InviteNotify && role.InviteId > 0 && role.InviteId == fromRoleId {
		mgr.SendToInviterCenter(ctx, role, fromRoleId, fromSid)
	}
}

func (mgr *InviteManager) SendToInviterServer(ctx *data.Context, role *data.Role, fromRoleId int64, fromSid int32) {
	var nodeUrl string
	if gAdminMgr.ServerCmd.Platform == "test" {
		nodeUrl = fmt.Sprintf("https://dev-klgo.qcinterfacet.com/game998/GmFunc?func=Invite&role_id=%d&inviter_roleId=%d&inviter_avater=%s&inviter_name=%s",
			fromRoleId, role.RoleId, role.Avatar, role.Name)
	} else {
		if fromSid < 45 {
			nodeUrl = fmt.Sprintf("https://weixin-game-kl2.qcinterfacet.com/game%03d/GmFunc?func=Invite&role_id=%d&inviter_roleId=%d&inviter_avater=%s&inviter_name=%s",
				fromSid, fromRoleId, role.RoleId, role.Avatar, role.Name)
		} else {
			gameIndex := (fromSid - 1) / 45
			nodeUrl = fmt.Sprintf("https://weixin-game%d-kl2.qcinterfacet.com/game%03d/GmFunc?func=Invite&role_id=%d&inviter_roleId=%d&inviter_avater=%s&inviter_name=%s",
				gameIndex, fromSid, fromRoleId, role.RoleId, role.Avatar, role.Name)
		}
	}
	logs.Info(nodeUrl)
	resp, err := http.Get(nodeUrl)
	defer resp.Body.Close()
	if err != nil {
		logs.Warnf("SendToInviterServer err:%s", err)
	}

}

func (mgr *InviteManager) SendToInviterCenter(ctx *data.Context, role *data.Role, fromRoleId int64, fromSid int32) {

	centerUrl := fmt.Sprintf("%s/invitate?", gGameFlag.CenterHost)
	getForm := make(url.Values)
	getForm.Add("role_id", fmt.Sprintf("%d", role.RoleId))
	getForm.Add("role_name", role.Name)
	getForm.Add("role_avater", role.Avatar)

	getForm.Add("invitater_role_id", fmt.Sprintf("%d", fromRoleId))
	getForm.Add("invitater_sid", fmt.Sprintf("%d", fromSid))

	getForm.Add("platform", gAdminMgr.ServerCmd.Platform)
	getForm.Add("channel", gAdminMgr.ServerCmd.Channel)

	due := time_helper.NowSec() + 6*time_helper.Hour
	logs.Debugf(fmt.Sprintf("-_-!!!%d%d%d%d", role.RoleId, fromRoleId, fromSid, due))
	token := util.Md5(fmt.Sprintf("-_-!!!%d%d%d%d", role.RoleId, fromRoleId, fromSid, due))
	getForm.Add("due", fmt.Sprintf("%d", due))
	getForm.Add("weiyingToken", token)
	logs.Info(centerUrl)
	getUrl := centerUrl + getForm.Encode()
	resp, err := http.Get(getUrl)
	defer resp.Body.Close()
	if err != nil {
		logs.Warnf("SendToInviterServer err:%s", err)
		return
	}
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Warnf("ReadAll err:%s", err)
		return
	}
	centerResp := &data.CenterResp{}
	err = json.Unmarshal(bs, centerResp)
	if err != nil {
		logs.Warnf("ReadAll err:%s", err)
		return
	}
	logs.Debugf("SendToInviterCenter resp %s", string(bs))
	if centerResp.Code == 200 {
		TimeAfter(ctx, 0, func(ctx2 *data.Context) {
			roleId := role.RoleId
			tRole := gRoleMgr.RoleBy(ctx, roleId)
			tRole.InviteNotify = true
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ROLE_INVITE_NOTIFY, true, roleId)
		})
	} else {
		logs.Warnf("SendToInviterCenter err:%s", centerResp.Err)
		return
	}

}
