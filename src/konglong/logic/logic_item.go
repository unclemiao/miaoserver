package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type ItemManager struct {
	TimeOutTimerMap map[int64]map[int64]*time.Timer

	// 存储所有玩家的合成结果
	MakeItemDoneMap map[int64]*data.MakeItemDone
}

func init() {
	gItemMgr = &ItemManager{
		TimeOutTimerMap: make(map[int64]map[int64]*time.Timer),
		MakeItemDoneMap: make(map[int64]*data.MakeItemDone),
	}
}

func (mgr *ItemManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllItem, func(ctx *data.Context, msg *protocol.Envelope) {
		envMsg := msg.GetC2SGetAllItem()
		mgr.NotifyItemList(ctx, ctx.RoleId, envMsg.KitType)
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)

		if args.IsNew {
			mgr.GiveAward(ctx, args.RoleId, &data.AwardCfg{AwardList: []*data.ItemAmount{
				{
					CfgId: gConst.ITEM_CFG_ID_DIAMONDS,
					N:     280,
				},
				{
					CfgId: gConst.ITEM_CFG_ID_GOLD,
					N:     500,
				},
				{
					CfgId: int64(protocol.CurrencyType_Scallop),
					N:     280,
				},
			}}, 1, protocol.ItemActionType_Action_NewRole)
			gs, _ := mgr.giveItem(ctx, args.RoleId, 10000, 1, protocol.KitType_Kit_Role_Equip, protocol.ItemActionType_Action_NewRole)
			var equip *data.Item
			for it, _ := range gs {
				equip = it
				break
			}
			mgr.LoadEquip(ctx, args.RoleId, equip.Id, false)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItem, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SMakeItem()
		mgr.MakeItem(ctx, ctx.RoleId, repMsg.ItemCfgId, repMsg.Grid, repMsg.FoodId1, repMsg.FoodId2)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_MoveItem, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SMoveItem()
		mgr.MoveItem(ctx, ctx.RoleId, repMsg.FromGrid, repMsg.ToGrid, repMsg.FromKitType, repMsg.ToKitType)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_UseItem, func(ctx *data.Context, msg *protocol.Envelope) {
		repMsg := msg.GetC2SUseItem()
		mgr.UseItem(ctx, ctx.RoleId, repMsg.ItemId, repMsg.ChoiceIndex, len(msg.AdSdkToken) > 0)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_TakeToolTmpAllItem, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.TakeAllToolTempItem(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemUp, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.FreeUp(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemKeep, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.KeepItem(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_MakeItemShare, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.ShareItem(ctx, ctx.RoleId)
	})

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOfflineRole)
		if args.SignOut {
			gCache.Delete(mgr.getCacheKey(args.RoleId))
			for _, t := range mgr.TimeOutTimerMap[args.RoleId] {
				if t != nil {
					t.Stop()
				}
			}
			delete(mgr.TimeOutTimerMap, args.RoleId)
		} else {

		}

	})

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		gSql.MustExcel("update `item` set kit=? where due > 0 and due <= ?", protocol.KitType_Kit_Dustbin, time_helper.NowSec())
	})

	mgr.InitEquipMgr()
}

func (mgr *ItemManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ALL_ITEM, roleId)
}

func (mgr *ItemManager) AllItemBy(ctx *data.Context, roleId int64) *data.AllItem {

	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		allItem := &data.AllItem{
			RoleId:        roleId,
			ItemMap:       make(map[int64]*data.Item),
			ItemMapForKit: make(map[protocol.KitType][]*data.Item),
			OwnerItems:    make(map[int64]map[int64]*data.Item),
			CurrencyMap:   make(map[int64]*data.Item),
			DustbinMap:    make(map[int64]*data.Item),
		}
		var itemList []*data.Item
		gSql.MustSelect(&itemList, gConst.SQL_SELECT_ALL_ITEM, roleId)
		mgr.initAllItem(ctx, allItem, itemList)
		return allItem, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	return rv.(*data.AllItem)
}

func (mgr *ItemManager) initAllItem(ctx *data.Context, allItem *data.AllItem, itemList []*data.Item) {
	assert.Assert(allItem != nil, "invalid allItem")
	nowSec := time_helper.NowSec()
	for _, it := range itemList {
		if it.RandAttrStr != "" {
			it.RandAttr = config.StrToAttr(it.RandAttrStr)
		}
		if it.Due > 0 && it.Due <= nowSec {
			it.Kit = protocol.KitType_Kit_Dustbin
		}

		if it.Kit == protocol.KitType_Kit_Dustbin {
			allItem.DustbinMap[it.Id] = it
		} else {
			if allItem.ItemMapForKit[it.Kit] == nil {
				allItem.ItemMapForKit[it.Kit] = make([]*data.Item, mgr.KitNumMaxBy(ctx, it.Kit)+1)
			}

			if it.Grid < int32(len(allItem.ItemMapForKit[it.Kit])) {
				allItem.ItemMapForKit[it.Kit][it.Grid] = it
			}
			allItem.ItemMap[it.Id] = it
		}

		if allItem.OwnerItems[it.OwnerId] == nil {
			allItem.OwnerItems[it.OwnerId] = make(map[int64]*data.Item)
		}

		if it.Kit == protocol.KitType_Kit_Currency {
			allItem.CurrencyMap[it.Cid] = it
		}
		allItem.OwnerItems[it.OwnerId][it.Id] = it

		if it.Due > 0 {
			t := TimeAfter(nil, time.Duration(it.Due-nowSec+time_helper.Minute*5)*time.Second, func(ctx2 *data.Context) {
				mgr.TakeItem(ctx2, it.RoleId, it, 0, it.N, protocol.ItemActionType_Action_TimeOut)
				delete(mgr.TimeOutTimerMap[it.RoleId], it.Id)
			}, gRoleMgr.RoleSessionBy(ctx, it.RoleId))
			if mgr.TimeOutTimerMap[it.RoleId] == nil {
				mgr.TimeOutTimerMap[it.RoleId] = make(map[int64]*time.Timer)
			}
			mgr.TimeOutTimerMap[it.RoleId][it.Id] = t
		}
	}

	for kitType := protocol.KitType_Kit_Unknown + 1; kitType < protocol.KitType_KitTypeMax; kitType++ {
		if len(allItem.ItemMapForKit[kitType]) == 0 {
			allItem.ItemMapForKit[kitType] = make([]*data.Item, mgr.KitNumMaxBy(ctx, kitType)+1)
		}
	}
}

func (mgr *ItemManager) ItemBy(ctx *data.Context, roleId int64, itemId int64) *data.Item {
	allItem := mgr.AllItemBy(ctx, roleId)
	it := allItem.ItemMap[itemId]
	assert.Assert(it != nil, "item not found, itemId: %d", itemId)
	assert.Assert(it.Kit != protocol.KitType_Kit_Dustbin, "item deleted")
	return it
}

// 在背包栏里找
func (mgr *ItemManager) ItemByGrid(ctx *data.Context, roleId int64, kit protocol.KitType, grid int32) *data.Item {

	allItem := mgr.AllItemBy(ctx, roleId)
	kitMap := allItem.ItemMapForKit[kit]
	assert.Assert(kitMap != nil, "item not found: grid: ", grid)
	assert.Assert(grid > 0 && grid < int32(len(kitMap)), "item not found: grid: ", grid)
	it := kitMap[grid]
	assert.Assert(it != nil, "item not found: grid: ", grid)
	return kitMap[grid]
}

func (mgr *ItemManager) ItemByOwner(ctx *data.Context, roleId int64, ownerId int64, itemId int64, kit protocol.KitType) *data.Item {
	it := mgr.ItemBy(ctx, roleId, itemId)
	assert.Assert(it.OwnerId == ownerId && kit == it.Kit, "item not found, itemId: %d, ownerId: %d, kit: %s", itemId, ownerId, kit)
	return it
}

// 从1开始
func (mgr *ItemManager) ItemListByKit(ctx *data.Context, roleId int64, kit protocol.KitType) []*data.Item {
	allItem := mgr.AllItemBy(ctx, roleId)
	if kit != protocol.KitType_Kit_Unknown {
		if allItem.ItemMapForKit[kit] == nil {
			allItem.ItemMapForKit[kit] = make([]*data.Item, mgr.KitNumMaxBy(ctx, kit)+1)
		}
		return allItem.ItemMapForKit[kit]
	} else {
		var itemList []*data.Item
		for _, it := range allItem.ItemMap {
			itemList = append(itemList, it)
		}
		return itemList
	}
}

func (mgr *ItemManager) ItemNum(ctx *data.Context, roleId int64, cfgId int64) int64 {
	kit := mgr.KitByCfgId(ctx, cfgId)
	kitBag := mgr.ItemListByKit(ctx, roleId, kit)
	var total int64
	for _, it := range kitBag {
		if it != nil && it.Cid == cfgId {
			total += it.N
		}
	}
	return total
}

func (mgr *ItemManager) ItemNumByOwner(ctx *data.Context, roleId int64, ownerId int64) int32 {
	allItem := mgr.AllItemBy(ctx, roleId)
	return int32(len(allItem.OwnerItems[ownerId]))
}

func (mgr *ItemManager) KitNumMaxBy(ctx *data.Context, kit protocol.KitType) int32 {
	switch kit {
	case protocol.KitType_Kit_Food:
		return gConst.FOOD_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Pieces:
		return gConst.PIECES_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Currency:
		return gConst.CURRENCY_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Tool:
		return gConst.TOOL_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Storage_Tool:
		return gConst.STORAGE_TOOL_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Storage_Item:
		return gConst.STORAGE_ITEM_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Storage_Food:
		return gConst.STORAGE_FOOD_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_Role_Equip:
		return gConst.EQUIP_KIT_AMOUNT_MAX
	case protocol.KitType_Kit_At_Role:
		return 2
	default:
		return 0
	}
}

func (mgr *ItemManager) KitByCfgId(ctx *data.Context, cfgId int64) protocol.KitType {
	cfg := config.GetItemCfg(cfgId)
	switch cfg.Kind {
	case protocol.ItemKind_Item_Food:
		return protocol.KitType_Kit_Food
	case protocol.ItemKind_Item_Omnipotent_Pieces:
		return protocol.KitType_Kit_Pieces
	case protocol.ItemKind_Item_Pieces:
		return protocol.KitType_Kit_Pieces
	case protocol.ItemKind_Item_Currency:
		return protocol.KitType_Kit_Currency
	case protocol.ItemKind_Item_Award:
		switch cfg.SubKind {
		case protocol.ItemSubKind_Sub_Rand_Pet:
			return protocol.KitType_Kit_Storage_Item
		case protocol.ItemSubKind_Sub_Choice:
			return protocol.KitType_Kit_Storage_Item
		}
		return protocol.KitType_Kit_Food
	case protocol.ItemKind_Item_Role_Equip:
		return protocol.KitType_Kit_Role_Equip
	case protocol.ItemKind_Item_Normal:
		switch cfg.SubKind {
		case protocol.ItemSubKind_Sub_Base_Stuff,
			protocol.ItemSubKind_Sub_Tool:
			return protocol.KitType_Kit_Tool
		default:
			return protocol.KitType_Kit_Storage_Item
		}
	default:
		return protocol.KitType_Kit_Storage_Item
	}
	return protocol.KitType_Kit_Storage_Item
}

// 空格子数量
func (mgr *ItemManager) EmptyGridBy(ctx *data.Context, roleId int64, kit protocol.KitType) map[int32]bool {
	gridMap := make(map[int32]bool)
	itemList := mgr.ItemListByKit(ctx, roleId, kit)
	for i := 1; i < len(itemList); i++ {
		if itemList[i] == nil {
			gridMap[int32(i)] = true
		}
	}
	return gridMap
}

func (mgr *ItemManager) newItem(ctx *data.Context, allItem *data.AllItem, roleId int64, sid int32, cid int64, n int64, grid int32, kit protocol.KitType) (*data.Item, bool) {
	var it *data.Item
	var isNew bool
	if len(allItem.DustbinMap) > 0 {
		it = &data.Item{}
		for _, dust := range allItem.DustbinMap {
			it.Id, it.RoleId = dust.Id, dust.RoleId
			delete(allItem.DustbinMap, it.Id)
			break
		}
	} else {
		isNew = true
		it = &data.Item{
			Id:     gIdMgr.GenerateId(ctx, sid, gConst.ID_TYPE_ITEM),
			RoleId: roleId,
		}
	}
	it.OwnerId, it.N, it.Cid, it.Grid, it.Kit, it.RandAttrStr, it.Due =
		roleId, n, cid, grid, kit, "{}", 0
	itemCfg := config.GetItemCfg(cid)
	if itemCfg.Due > 0 {
		it.Due = time_helper.NowSec() + itemCfg.Due
		t := TimeAfter(ctx, time.Duration(itemCfg.Due+5*time_helper.Minute)*time.Second, func(ctx2 *data.Context) {
			mgr.TakeItem(ctx2, roleId, it, 0, it.N, protocol.ItemActionType_Action_TimeOut)
			delete(mgr.TimeOutTimerMap[roleId], it.Id)
		}, gRoleMgr.RoleSessionBy(ctx, it.RoleId))
		if mgr.TimeOutTimerMap[it.RoleId] == nil {
			mgr.TimeOutTimerMap[it.RoleId] = make(map[int64]*time.Timer)
		}
		mgr.TimeOutTimerMap[it.RoleId][it.Id] = t
	}
	if itemCfg.Kind == protocol.ItemKind_Item_Award && itemCfg.SubKind == protocol.ItemSubKind_Sub_Food {
		stub := gStubMgr.StubInfoBy(ctx, roleId, protocol.StubKind_StubNormal)
		var foodLikeTypeList []int32
		var minLayorPet *data.Pet
		for _, info := range stub.UnitList {
			if info.PetId > 0 {
				pet := gPetMgr.PetBy(ctx, roleId, info.PetId)
				if minLayorPet == nil {
					minLayorPet = pet
				} else if pet.Loyal < minLayorPet.Loyal {
					minLayorPet = pet
				}
			}
		}
		if minLayorPet != nil {
			foodLikeTypeList = append(foodLikeTypeList, config.GetPetCfg(minLayorPet.CfgId).LikeTypeList...)
		}

		dropGroup := config.GetItemDropGroupCfg(config.FoodBookCfgBy(gFoodBookMgr.FoodBookBy(ctx, roleId).Level).BoxDropId)
		var foodLikeIdList []int64
		for _, dropCfg := range dropGroup.DropCfgMap {
			itCfg := config.GetItemCfg(dropCfg.ItemId)
			if len(foodLikeTypeList) > 0 {
				for _, likeType := range foodLikeTypeList {
					if likeType == int32(itCfg.SubKind) {
						foodLikeIdList = append(foodLikeIdList, dropCfg.ItemId)
					}
				}
			} else {
				foodLikeIdList = append(foodLikeIdList, dropCfg.ItemId)
			}

		}
		cfgId := foodLikeIdList[util.Random(0, len(foodLikeIdList)-1)]
		it.OpenCfgId = cfgId
	}

	if itemCfg.Kind == protocol.ItemKind_Item_Role_Equip {
		mgr.initEquip(ctx, roleId, it)
	}

	return it, isNew
}

func (mgr *ItemManager) addItemToCache(ctx *data.Context, roleId int64, allItems *data.AllItem, item *data.Item) {
	allItems.ItemMap[item.Id] = item
	kitNum := mgr.KitNumMaxBy(ctx, item.Kit)
	if allItems.ItemMapForKit[item.Kit] == nil {
		allItems.ItemMapForKit[item.Kit] = make([]*data.Item, kitNum+1)
	} else {
		if int32(len(allItems.ItemMapForKit[item.Kit])) < kitNum+1 {
			newList := make([]*data.Item, kitNum+1)
			copy(newList, allItems.ItemMapForKit[item.Kit])
			allItems.ItemMapForKit[item.Kit] = newList
		}
	}

	if item.Grid > 0 && item.Grid <= kitNum {
		allItems.ItemMapForKit[item.Kit][item.Grid] = item
	}

	if item.OwnerId != item.RoleId {
		if allItems.OwnerItems[item.OwnerId] == nil {
			allItems.OwnerItems[item.OwnerId] = make(map[int64]*data.Item)
		}
		allItems.OwnerItems[item.OwnerId][item.Id] = item
	}

	if item.Kit == protocol.KitType_Kit_Currency {
		allItems.CurrencyMap[item.Cid] = item
	}
	gCache.Touch(mgr.getCacheKey(roleId), gConst.SERVER_DATA_CACHE_TIME_OUT)

}

func (mgr *ItemManager) delItemFromCache(ctx *data.Context, roleId int64, allItems *data.AllItem, item *data.Item) {
	if item.Kit == protocol.KitType_Kit_Currency {
		return
	}

	if allItems.ItemMapForKit[item.Kit] != nil {
		if item.Grid > 0 && item.Grid < int32(len(allItems.ItemMapForKit[item.Kit])) {
			allItems.ItemMapForKit[item.Kit][item.Grid] = nil
		}
	}

	allItems.DustbinMap[item.Id] = item

	if item.OwnerId != item.RoleId {
		if allItems.OwnerItems[item.OwnerId] == nil {
			allItems.OwnerItems[item.OwnerId] = make(map[int64]*data.Item)
		}
		allItems.OwnerItems[item.OwnerId][item.Id] = item
	}
	item.Kit = protocol.KitType_Kit_Dustbin
	delete(allItems.ItemMap, item.Id)
}

// findGrids 在背包里找格子
// free只找空格子 onlyOne 只找一个格子 -> { [grid]=+n空格子 -n要叠放的格子 }
func (mgr *ItemManager) findGrids(
	ctx *data.Context,
	roleId int64,
	cid int64,
	n int64,
	kit protocol.KitType,
	free bool,
	onlyOne bool,
	must bool,
) (map[int32]int64, bool) {
	var n9 int64
	if cid == 0 {
		// 找一个空格子
		assert.Assert(n == 1 && free && onlyOne)
		n9 = 1
	} else {
		n9 = config.GetItemCfg(cid).N9
	}
	assert.Assert(n > 0 && (!onlyOne || n <= n9), "invalid amount, amount: %d", n)

	var (
		allItems = mgr.AllItemBy(ctx, roleId)
		gridc    int64 // 所有非空格子该物品可叠加数量
		vacc     int64 // 所有空格子该物品可叠加数量
		grids    = make(map[int32]int64)
		vacs     = make(map[int32]int64)
		kitn     = mgr.KitNumMaxBy(ctx, kit)
		kitBag   = allItems.ItemMapForKit[kit]
	)

	if kit == protocol.KitType_Kit_Currency {
		return mgr.findCurrencyGrids(ctx, roleId, cid, n)
	}

	assert.Assert(kitn > 0, "invalid kit: %s", kit)

	for k := int32(1); k <= kitn; k++ {
		it := kitBag[k]
		if it != nil && !free {
			// 格子里有东西并且不是只找空格子
			if it.Cid == cid && it.N < n9 {
				left := n9 - it.N
				if gridc+left > n {
					// 该格子剩余位置放得下所有剩余数量
					grids[k] = -n - gridc
					gridc = n
					break

				} else {
					// 该格子剩余位置放不下所有剩余数量
					grids[k] = -left
					gridc = gridc + left
				}
			}

		} else if it == nil {
			// 格子里没东西
			if free && vacc+n9 >= n {
				vacs[k] = n - vacc
				vacc = n
				break

			}
			vacs[k] = n9
			vacc += n9
		}
	}

	if gridc+vacc < n {
		if must {
			if kit != protocol.KitType_Kit_Tool {
				logs.Warn("!背包已满")
			}
			return nil, false
		}
	}

	if gridc >= n {
		return grids, true
	}

	for k := int32(1); k <= kitn; k++ {
		if vacs[k] != 0 {
			if gridc+n9 >= n {
				grids[k] = n - gridc
				gridc = n
				break

			} else {
				grids[k] = n9
				gridc = gridc + n9
			}
		}
	}

	if must {
		assert.Assert(len(grids) > 0, "!背包已满")
	}
	return grids, true
}

func (mgr *ItemManager) findCurrencyGrids(
	ctx *data.Context,
	roleId int64,
	cid int64,
	n int64,
) (map[int32]int64, bool) {
	var (
		n9       = config.GetItemCfg(cid).N9
		allItems = mgr.AllItemBy(ctx, roleId)
		grids    = make(map[int32]int64)
		kitn     = mgr.KitNumMaxBy(ctx, protocol.KitType_Kit_Currency)
		kitBag   = allItems.ItemMapForKit[protocol.KitType_Kit_Currency]
	)

	currency := allItems.CurrencyMap[cid]
	if currency == nil {
		// 无货币
		for grid := int32(1); grid < kitn; grid++ {
			it := kitBag[grid]
			if it == nil {
				grids[grid] = n
				break
			}
		}
	} else {
		// 有货币
		grids[currency.Grid] = -util.MinInt64(n, n9-currency.N)
	}
	return grids, true
}

func (mgr *ItemManager) giveItemToEmptyGrid(
	ctx *data.Context,
	roleId int64,
	cfgId int64,
	num int64,
	grid int32,
	kit protocol.KitType,
	why protocol.ItemActionType,
) *data.Item {
	role := gRoleMgr.RoleBy(ctx, roleId)
	allItem := mgr.AllItemBy(ctx, roleId)
	if allItem.ItemMapForKit[kit] != nil {
		assert.Assert(grid > 0 && grid < int32(len(allItem.ItemMapForKit[kit])), "invalid grid %d", grid)
		assert.Assert(allItem.ItemMapForKit[kit][grid] == nil, "invalid grid %d", grid)
	}
	gSql.Begin().Clean(roleId)
	it, isNew := mgr.newItem(ctx, allItem, roleId, role.Sid, cfgId, num, grid, kit)
	if !isNew {
		gSql.MustExcel(gConst.SQL_UPDATE_ITEM, it.OwnerId, it.Cid, it.N, it.Grid, it.Kit,
			it.SkillCfgId, it.OpenCfgId, it.HpMaxBase, it.AtkBase, it.DefBase, it.Grade, it.Star, it.RandAttrStr, it.Due, it.Id)
	} else {
		gSql.MustExcel(gConst.SQL_INSERT_ITEM, it.Id, it.RoleId, it.OwnerId, it.Cid, it.N, it.Grid, it.Kit,
			it.SkillCfgId, it.OpenCfgId, it.HpMaxBase, it.AtkBase, it.DefBase, it.Grade, it.Star, it.RandAttrStr, it.Due)
	}
	mgr.addItemToCache(ctx, roleId, allItem, it)
	gGameLogMgr.OnItemGive(ctx, role, it, num, why)

	gEventMgr.Call(ctx, data.EVENT_MSGID_ON_ITEM_ADD, &data.EventItemAdd{
		RoleId:    roleId,
		ItemCfgId: cfgId,
		Num:       num,
		Action:    why,
	})

	addList := []*protocol.Item{mgr.GetForClient(it)}
	gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemUpdate(addList, nil, nil, why))
	return it
}

// 给道具 no remote
// 返回值：给了什么道具多少个 { it=num, num>0新增， num<0叠加 }
func (mgr *ItemManager) giveItem(ctx *data.Context, roleId int64, cfgId int64, n int64, kit protocol.KitType, why protocol.ItemActionType) (map[*data.Item]int64, bool) {
	var (
		gives   = make(map[*data.Item]int64)
		role    = gRoleMgr.RoleBy(ctx, roleId)
		itemCfg = config.GetItemCfg(cfgId)
	)

	// 如果是经验
	if itemCfg.CfgId == gConst.ITEM_CFG_ID_EXP {
		gRoleMgr.AddExp(ctx, roleId, uint64(n))
		return nil, true
	}

	// 检测 cfgId 合法性
	grids, canAdd := mgr.findGrids(ctx, roleId, cfgId, n, kit, false, false, true)
	allItem := mgr.AllItemBy(ctx, roleId)

	if !canAdd {
		return nil, false
	}
	gSql.Begin().Clean(roleId)
	for grid, num := range grids {
		if num < 0 {
			// 叠加
			it := mgr.ItemByGrid(ctx, roleId, kit, grid)
			gSql.MustExcel(gConst.SQL_UPDATE_ITEM_AMOUNT, it.N-num, it.Id)
			it.N -= num
			gives[it] = num
			gGameLogMgr.OnItemGive(ctx, role, it, -num, why)
		} else if num > 0 {
			// 空格子里创建新物品
			it, isNew := mgr.newItem(ctx, allItem, roleId, role.Sid, cfgId, num, grid, kit)
			if !isNew {
				gSql.MustExcel(gConst.SQL_UPDATE_ITEM, it.OwnerId, it.Cid, it.N, it.Grid, it.Kit,
					it.SkillCfgId, it.OpenCfgId, it.HpMaxBase, it.AtkBase, it.DefBase, it.Grade, it.Star, it.RandAttrStr, it.Due, it.Id)
			} else {
				gSql.MustExcel(gConst.SQL_INSERT_ITEM, it.Id, it.RoleId, it.OwnerId, it.Cid, it.N, it.Grid, it.Kit,
					it.SkillCfgId, it.OpenCfgId, it.HpMaxBase, it.AtkBase, it.DefBase, it.Grade, it.Star, it.RandAttrStr, it.Due)
			}
			mgr.addItemToCache(ctx, roleId, allItem, it)
			gives[it] = num
			gGameLogMgr.OnItemGive(ctx, role, it, num, why)
		}
	}
	gEventMgr.Call(ctx, data.EVENT_MSGID_ON_ITEM_ADD, &data.EventItemAdd{
		RoleId:    roleId,
		ItemCfgId: cfgId,
		Num:       n,
		Action:    why,
	})
	return gives, true
}

// 扣除道具
// 扣除整个 item 或者 配置 cid 指定数量
// 返回值：扣除了什么道具多少个
func (mgr *ItemManager) takeItem(ctx *data.Context, roleId int64, item *data.Item, cid int64, n int64, kit protocol.KitType, why protocol.ItemActionType) map[*data.Item]int64 {

	var (
		takes   = make(map[*data.Item]int64)
		allItem = mgr.AllItemBy(ctx, roleId)
		itemCfg *data.ItemCfg
		role    = gRoleMgr.RoleBy(ctx, roleId)
	)

	gSql.Begin().Clean(roleId)

	if item != nil {
		if mgr.TimeOutTimerMap[roleId] != nil {
			t, ok := mgr.TimeOutTimerMap[roleId][item.Id]
			if ok && t != nil {
				t.Stop()
				delete(mgr.TimeOutTimerMap[roleId], item.Id)
			}
		}
		itemCfg = config.GetItemCfg(item.Cid)
		// 扣除这个item
		assert.Assert(n <= item.N, "!%s数量不足", itemCfg.Name)
		if n < item.N {
			// 扣除部分数量
			gSql.MustExcel(gConst.SQL_UPDATE_ITEM_AMOUNT, item.N-n, item.Id)
		} else {
			// 扣除全部数量
			if item.Kit == protocol.KitType_Kit_Currency {
				gSql.MustExcel(gConst.SQL_UPDATE_ITEM_AMOUNT, 0, item.Id)
			} else {
				gSql.MustExcel(gConst.SQL_UPDATE_ITEM_KIT, protocol.KitType_Kit_Dustbin, item.Id)
				mgr.delItemFromCache(ctx, roleId, allItem, item)
			}
		}

		item.N -= n
		gGameLogMgr.OnItemCost(ctx, role, item, n, why)
		takes[item] = n

		return takes
	}

	// 按配置 id 扣除
	itemCfg = config.GetItemCfg(cid)

	if n <= 0 {
		return takes
	}

	var (
		count    int64 = 0
		takeList       = make([]*data.Item, 0) // 需要扣除的道具列表
		bagMap         = allItem.ItemMapForKit[kit]
		nowSec         = time_helper.NowSec()
	)

	if kit == protocol.KitType_Kit_Currency {
		currency := allItem.CurrencyMap[cid]
		if currency != nil {
			count = currency.N
			takeList = append(takeList, currency)
		}
	} else {
		for _, it := range bagMap {
			if it == nil {
				continue
			}
			if it.Cid == cid && (0 == it.Due || it.Due > nowSec) {
				takeList = append(takeList, it)
				count += it.N
			}
		}
	}

	assert.Assert(count >= n, "!%s数量不足", itemCfg.Name)
	count = n

	for _, it := range takeList {
		if count <= 0 {
			return takes
		}
		if it.N <= count {
			if it.Kit == protocol.KitType_Kit_Currency {
				gSql.MustExcel(gConst.SQL_UPDATE_ITEM_AMOUNT, 0, it.Id)
			} else {
				gSql.MustExcel(gConst.SQL_UPDATE_ITEM_KIT, protocol.KitType_Kit_Dustbin, it.Id)
				mgr.delItemFromCache(ctx, roleId, allItem, it)
			}
			takes[it] = it.N
			count -= it.N
			it.N = 0
			gGameLogMgr.OnItemCost(ctx, role, it, takes[it], why)

		} else {
			gSql.MustExcel(gConst.SQL_UPDATE_ITEM_AMOUNT, it.N-count, it.Id)
			takes[it] = count
			it.N -= count
			count = 0
			gGameLogMgr.OnItemCost(ctx, role, it, takes[it], why)

		}

	}
	return takes
}

func (mgr *ItemManager) GetForClient(item *data.Item) *protocol.Item {

	it := &protocol.Item{
		Id:         item.Id,
		OwnerId:    item.OwnerId,
		CfgId:      item.Cid,
		Num:        item.N,
		Kit:        item.Kit,
		Grid:       item.Grid,
		Star:       item.Star,
		UpdateTime: time_helper.NowMill(),
		Due:        item.Due,

		Grade:      item.Grade,
		SkillCfgId: item.SkillCfgId,
		OpenCfgId:  []int64{item.OpenCfgId},
		HpMaxBase:  item.HpMaxBase,
		AtkBase:    item.AtkBase,
		DefBase:    item.DefBase,
		HpMax:      item.HpMaxBase,
		Atk:        item.AtkBase,
		Def:        item.DefBase,
	}

	cfg := config.GetItemCfg(item.Cid)
	var hpRate, atkRate, defRate int32
	if item.Star > 0 {
		starCfg := config.GetEquipStarCfg(item.Cid, item.Star)
		hpRate += starCfg.HpRate
		atkRate += starCfg.AtkRate
		defRate += starCfg.DefRate
	}

	if cfg.Kind == protocol.ItemKind_Item_Role_Equip {
		gradeCfg := config.GetEquipGradeCfg(int32(item.Cid))
		if gradeCfg != nil {
			hpRate = hpRate * gradeCfg.HpRate
			atkRate = atkRate * gradeCfg.AtkRate
			defRate = defRate * gradeCfg.DefRate
		}

	}

	it.HpMax = util.CalcBonusRateUnit64(it.HpMaxBase, hpRate, 1000000)

	it.Atk = util.CalcBonusRateUnit(it.AtkBase, atkRate, 1000000)
	it.Def = util.CalcBonusRateUnit(it.DefBase, defRate, 1000000)

	if item.RandAttr != nil {
		it.RandHpMax = util.MaxInt64(item.RandAttr.Hp, item.RandAttr.HpMax)
		if item.RandAttr.Atk > 0 {
			it.RandAtk = item.RandAttr.Atk
		}
		if item.RandAttr.Def > 0 {
			it.RandDef = item.RandAttr.Def
		}
		if item.RandAttr.Crit > 0 {
			it.RandCrit = item.RandAttr.Crit
		}
		if item.RandAttr.Dodge > 0 {
			it.RandDodge = item.RandAttr.Dodge
		}
		if item.RandAttr.AtkTime > 0 {
			it.RandAtkTime = item.RandAttr.AtkTime
		}
	}
	return it
}

func (mgr *ItemManager) GetAllItemForClient(ctx *data.Context, roleId int64) []*protocol.Item {
	var clientList []*protocol.Item
	allItem := mgr.AllItemBy(ctx, roleId)
	for _, it := range allItem.ItemMap {
		if it.Kit != protocol.KitType_Kit_Dustbin {
			clientList = append(clientList, mgr.GetForClient(it))
		}
	}
	return clientList
}

func (mgr *ItemManager) GiveItem(ctx *data.Context, roleId int64, cfgId int64, n int64, why protocol.ItemActionType) map[*data.Item]int64 {
	mailAward := &data.AwardCfg{}
	var addList []*protocol.Item
	var updateList []*protocol.Item
	kit := mgr.KitByCfgId(ctx, cfgId)
	gives, ok := mgr.giveItem(ctx, roleId, cfgId, n, kit, why)
	if !ok {
		if kit == protocol.KitType_Kit_Tool {
			gives, ok = mgr.giveItem(ctx, roleId, cfgId, n, protocol.KitType_Kit_Storage_Tool, why)
			if !ok {
				// mail
				mailAward.AwardList = append(mailAward.AwardList, &data.ItemAmount{
					CfgId: cfgId,
					N:     n,
				})
			} else {
				for it, n := range gives {
					if n > 0 {
						addList = append(addList, mgr.GetForClient(it))
					} else {
						updateList = append(updateList, mgr.GetForClient(it))
					}
				}
			}
		} else {
			// mail
			mailAward.AwardList = append(mailAward.AwardList, &data.ItemAmount{
				CfgId: cfgId,
				N:     n,
			})
		}
	} else {
		for it, n := range gives {
			if n > 0 {
				addList = append(addList, mgr.GetForClient(it))
			} else {
				updateList = append(updateList, mgr.GetForClient(it))
			}
		}
	}
	gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemUpdate(addList, updateList, nil, why))
	return gives
}

// 扣除道具
// 扣除整个 item 或者 配置 cid 指定数量
func (mgr *ItemManager) TakeItem(ctx *data.Context, roleId int64, item *data.Item, cid int64, n int64, why protocol.ItemActionType) {

	var delIdList []int64
	var updateList []*protocol.Item
	var itemCfg *data.ItemCfg
	if item != nil {
		itemCfg = config.GetItemCfg(item.Cid)
	} else {
		itemCfg = config.GetItemCfg(cid)
	}

	takes := mgr.takeItem(ctx, roleId, item, cid, n, mgr.KitByCfgId(ctx, itemCfg.CfgId), why)

	for it, _ := range takes {
		if it.N == 0 {
			delIdList = append(delIdList, it.Id)
		} else {
			updateList = append(updateList, mgr.GetForClient(it))
		}
	}

	gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemUpdate(nil, updateList, delIdList, why))
}

// 批量给予道具
func (mgr *ItemManager) GiveAward(ctx *data.Context, roleId int64, awardCfg *data.AwardCfg, rate float64, why protocol.ItemActionType) bool {
	if awardCfg == nil {
		return false
	}
	mailAward := &data.AwardCfg{}
	var addList []*protocol.Item
	var updateList []*protocol.Item

	for _, amountCfg := range awardCfg.AwardList {
		cfgId := amountCfg.CfgId
		if amountCfg.N > 0 {
			num := util.MaxInt64(1, int64(float64(amountCfg.N)*rate))
			itemCfg := config.GetItemCfg(cfgId)
			if itemCfg.Kind == protocol.ItemKind_Item_Auto_Award {
				if !mgr.openGift(ctx, roleId, cfgId, num, why) {
					// mail
					mailAward.AwardList = append(mailAward.AwardList, &data.ItemAmount{
						CfgId: cfgId,
						N:     num,
					})
				}
			} else {
				kit := mgr.KitByCfgId(ctx, cfgId)
				gives, ok := mgr.giveItem(ctx, roleId, cfgId, num, kit, why)
				if !ok {
					if kit == protocol.KitType_Kit_Tool {
						gives, ok = mgr.giveItem(ctx, roleId, cfgId, num, protocol.KitType_Kit_Storage_Tool, why)
						if !ok {
							// mail
							mailAward.AwardList = append(mailAward.AwardList, &data.ItemAmount{
								CfgId: cfgId,
								N:     num,
							})
						} else {
							for it, n := range gives {
								if n > 0 {
									addList = append(addList, mgr.GetForClient(it))
								} else {
									updateList = append(updateList, mgr.GetForClient(it))
								}
							}
						}
					} else {
						// mail
						mailAward.AwardList = append(mailAward.AwardList, &data.ItemAmount{
							CfgId: cfgId,
							N:     num,
						})
					}
				} else {
					for it, n := range gives {
						if n > 0 {
							addList = append(addList, mgr.GetForClient(it))
						} else {
							updateList = append(updateList, mgr.GetForClient(it))
						}
					}
				}
			}

		} else {
			gPetMgr.GivePetFromCfgId(ctx, roleId, int32(cfgId), amountCfg.N == -2, true, false, protocol.PetActionType(why), true)
		}

	}
	if len(addList)+len(updateList) > 0 {
		gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemUpdate(addList, updateList, nil, why))
	}

	if len(mailAward.AwardList) > 0 {
		if ctx.ItemMustGive {
			assert.Assert(false, "!背包已满")
		}
		now := time.Now()
		awardstr := config.AwardToStr(mailAward)
		gMailMgr.SendMailee(ctx, &data.Mailee{
			Name:     fmt.Sprintf("GiveAward%d", now.Nanosecond()),
			RoleId:   roleId,
			Title:    "背包已满",
			AwardStr: awardstr,
			Award:    mailAward,
			Body:     "亲爱的玩家，您的背包已满，您获得的物品现在已通过邮件发送给您，请清理背包后查收",
			Time:     now,
			TimeOut:  now.Add(time.Hour * 24 * 7),
		})
		return true
	}
	return false
}

// 批量扣除道具
func (mgr *ItemManager) TakeAward(ctx *data.Context, roleId int64, awardCfg *data.AwardCfg, rate float64, why protocol.ItemActionType) {
	var updateList []*protocol.Item
	var delIdList []int64

	for _, amountCfg := range awardCfg.AwardList {
		cfgId := amountCfg.CfgId
		num := util.MaxInt64(1, int64(float64(amountCfg.N)*rate))
		takes := mgr.takeItem(ctx, roleId, nil, cfgId, num, mgr.KitByCfgId(ctx, cfgId), why)

		for it, _ := range takes {
			if it.N == 0 {
				delIdList = append(delIdList, it.Id)
			} else {
				updateList = append(updateList, mgr.GetForClient(it))
			}
		}

	}

	gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemUpdate(nil, updateList, delIdList, why))

}

// 合成道具
func (mgr *ItemManager) MakeItem(ctx *data.Context, roleId int64, toItemCfgId int64, grid int32, foodId1 int64, foodId2 int64) {
	toCfg := config.GetItemCfg(toItemCfgId)
	makeCfg := config.ItemMakeCfgBy(toItemCfgId)
	food1 := mgr.ItemBy(ctx, roleId, foodId1)
	food2 := mgr.ItemBy(ctx, roleId, foodId2)
	for _, stuff := range makeCfg.Stuff.AwardList {
		assert.Assert(stuff.CfgId == food1.Cid || stuff.CfgId == food2.Cid, "invalid food")
	}

	mgr.TakeItem(ctx, roleId, food1, 0, food1.N, protocol.ItemActionType_Action_Make)
	mgr.TakeItem(ctx, roleId, food2, 0, food2.N, protocol.ItemActionType_Action_Make)

	giveIt := mgr.giveItemToEmptyGrid(ctx, roleId, toItemCfgId, 1, grid, mgr.KitByCfgId(ctx, toItemCfgId), protocol.ItemActionType_Action_Make)

	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_MAKE_ITEM, 1)

	itemDone := &data.MakeItemDone{
		Grid:      grid,
		ItemCfgId: toItemCfgId,
		ItemId1:   foodId1,
		ItemId2:   foodId2,
	}

	// 合成结果概率
	if toCfg.Kind == protocol.ItemKind_Item_Food {
		mgr.MakeItemDoneMap[roleId] = itemDone

		gFoodBookMgr.AddExp(ctx, roleId, makeCfg.AddFoodBookExp)
	}

	gEventMgr.Call(ctx, data.EVENT_MSGID_ON_ITEM_MAKE, &data.EventItemMake{
		RoleId: roleId,
		Item:   giveIt,
	})
	gRoleMgr.Send(ctx, roleId, protocol.MakeMakeItemDoneMsg(ctx.EventMsg.GetEnvMsg(), itemDone.State,
		util.ChoiceInt64(itemDone.KeepItemCfgId > 0, itemDone.KeepItemCfgId, giveIt.Cid)))

}

// 免费升级
func (mgr *ItemManager) FreeUp(ctx *data.Context, roleId int64) {

}

// 保留食物
func (mgr *ItemManager) KeepItem(ctx *data.Context, roleId int64) {
	makeDone := mgr.MakeItemDoneMap[roleId]
	assert.Assert(makeDone != nil && makeDone.State == 2, "invalid makeDone")

	mgr.GiveItem(ctx, roleId, makeDone.KeepItemCfgId, 1, protocol.ItemActionType_Action_ItemKeep)
	delete(mgr.MakeItemDoneMap, roleId)
}

// 分享炫耀
func (mgr *ItemManager) ShareItem(ctx *data.Context, roleId int64) {
	makeDone := mgr.MakeItemDoneMap[roleId]
	assert.Assert(makeDone != nil && makeDone.State == 0, "invalid makeDone")
	mgr.GiveItem(ctx, roleId, makeDone.ItemCfgId, 1, protocol.ItemActionType_Action_Share)
	delete(mgr.MakeItemDoneMap, roleId)
	gDailyQuestMgr.Done(ctx, ctx.RoleId, gConst.QUEST_TYPE_SHARE, 1)
}

// 移动道具
func (mgr *ItemManager) MoveItem(ctx *data.Context, roleId int64, fromGrid int32, toGrid int32, fromKit protocol.KitType, toKit protocol.KitType) {
	fromItemList := mgr.ItemListByKit(ctx, roleId, fromKit)
	assert.Assert(fromGrid > 0 && fromGrid < int32(len(fromItemList)), "invalid fromGrid or toGrid, from:%d", fromGrid)

	toItemList := mgr.ItemListByKit(ctx, roleId, toKit)

	if toGrid == 0 {
		// 自动找个空格子
		for i := 1; i < len(toItemList); i++ {
			if toItemList[i] == nil {
				toGrid = int32(i)
				break
			}
		}
	}
	gSql.Begin().Clean(roleId)

	assert.Assert(toGrid > 0 && toGrid < int32(len(toItemList)), "invalid fromGrid or toGrid, toGrid:%d", toGrid)

	fromItem, toItem := fromItemList[fromGrid], toItemList[toGrid]
	assert.Assert(fromItem != nil, "fromGrid has not item, fromGrid:%d", fromGrid)

	var changeList []*protocol.Item

	fromItemList[fromGrid] = toItem
	toItemList[toGrid] = fromItem
	fromItem.Grid = toGrid
	fromItem.Kit = toKit
	changeList = append(changeList, mgr.GetForClient(fromItem))

	gSql.MustExcel(gConst.SQL_UPDATE_ITEM_KIT_AND_GRID, fromItem.Kit, fromItem.Grid, fromItem.Id)

	if toItem != nil {
		toItem.Grid = fromGrid
		toItem.Kit = fromKit
		changeList = append(changeList, mgr.GetForClient(toItem))
		gSql.MustExcel(gConst.SQL_UPDATE_ITEM_KIT_AND_GRID, toItem.Kit, toItem.Grid, toItem.Id)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeMoveItemDoneMsg(ctx.EventMsg.GetEnvMsg(), fromGrid, toGrid, changeList))
}

// 把指定 item 放入到指定 kit 中的空格子中
// 返回是否成功放入
func (mgr *ItemManager) moveItemToKit(ctx *data.Context, roleId int64, fromItem *data.Item, kitType protocol.KitType) bool {
	toKitItemList := mgr.ItemListByKit(ctx, roleId, kitType)
	gSql.Begin().Clean(roleId)
	for i := 1; i < len(toKitItemList); i++ {
		if toKitItemList[i] == nil {
			fromItem.Kit = kitType
			fromItem.Grid = int32(i)
			gSql.MustExcel(gConst.SQL_UPDATE_ITEM_KIT_AND_GRID, fromItem.Kit, fromItem.Grid, fromItem.Id)
			toKitItemList[i] = fromItem
			return true
		}
	}
	return false
}

// 一键获取临时背包道具
func (mgr *ItemManager) TakeAllToolTempItem(ctx *data.Context, roleId int64) {

}

func (mgr *ItemManager) NotifyItemList(ctx *data.Context, roleId int64, kit protocol.KitType) {
	var clientItemList []*protocol.Item
	for _, it := range mgr.ItemListByKit(ctx, roleId, kit) {
		clientItemList = append(clientItemList, mgr.GetForClient(it))
	}
	gSessionMgr.Send(ctx, gRoleMgr.RoleSessionBy(ctx, roleId), protocol.MakeNotifyItemList(kit, clientItemList))

}
