package logic

import (
	"fmt"
	"io/ioutil"
	"sort"
	"strings"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/sql"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

// 初始化sql
func InitSql(winPath string, cnf *data.AppCnf) {
	var err error
	gSql, err = sql.LoadSql(&sql.Dsn{
		Driver:   "mysql",
		Host:     gAdminMgr.ServerCmd.SqlHost,
		Username: gAdminMgr.ServerCmd.SqlRoot,
		Password: gAdminMgr.ServerCmd.SqlPass,
	})
	dbName := gAdminMgr.ServerCmd.SqlName
	gSql.Dsn.DatabaseName = dbName
	_, err = gSql.Excel(fmt.Sprintf("USE %s", dbName))
	if err != nil {
		initDataBase(dbName)
	}
	gSql.SetRollBackEventFunc(func(id int64, args ...interface{}) {
		gEventMgr.Call(nil, data.EVENT_MSGID_SQL_ROLLBACK, &data.EventSqlRollBack{RoleId: id})
	})
	InitScript("./")

}

// 加载游戏
func InitScript(winPath string) {
	var nodes []*data.ServerNode
	err := gSql.Select(&nodes, "select * from server_node")
	if err != nil {
		initSqlTable(winPath)
	}
}

func initDataBase(dbName string) {
	gSql.MustExcel(fmt.Sprintf("CREATE DATABASE IF NOT EXISTS %s  DEFAULT CHARACTER SET utf8mb4;", dbName))
	gSql.MustExcel(fmt.Sprintf("USE %s", dbName))
	err := gSql.Ping()
	assert.Assert(err == nil, err)
}

// 初始化数据库
func initSqlTable(winPath string) {
	// 获取数据库脚本路径
	doCreateSqlTableFromDir(winPath + "/config/sqlversion/create")
}

// 更新数据库
func UpdateSqlTable(winPath string, nowVersion int64) int64 {
	// 获取数据库脚本路径
	return doUpdateSqlTableFromDir(winPath+"/config/sqlversion/update", nowVersion)
}

func GetUpdateMaxVersion(dirPath string) int64 {
	path := dirPath + "config/sqlversion/update"
	fileList, err := ioutil.ReadDir(path)
	assert.Assert(err == nil, err, path)

	var maxVersion int64
	for _, file := range fileList {
		if strings.Index(file.Name(), ".sql") == -1 {
			continue
		}
		Version := util.AtoInt64(strings.Split(file.Name(), "_")[0])
		maxVersion = util.MaxInt64(maxVersion, Version)
	}

	return maxVersion
}

func doUpdateSqlTableFromDir(dirPath string, nowVersion int64) int64 {
	// 获取文件夹下所有文件
	fileList, err := ioutil.ReadDir(dirPath)
	assert.Assert(err == nil, err, dirPath)

	// 执行所有.sql文件脚本
	type SqlFile struct {
		Version  int64
		FileName string
		SqlStr   string
	}

	var sqlFiles []*SqlFile
	for _, file := range fileList {
		if strings.Index(file.Name(), ".sql") == -1 {
			continue
		}
		filePath := dirPath + "/" + file.Name()
		sqlBytes, _ := ioutil.ReadFile(filePath)

		sqlFiles = append(sqlFiles, &SqlFile{
			Version:  util.AtoInt64(strings.Split(file.Name(), "_")[0]),
			SqlStr:   string(sqlBytes),
			FileName: file.Name(),
		})
	}
	sort.Slice(sqlFiles, func(i, j int) bool {
		return sqlFiles[i].Version < sqlFiles[j].Version
	})

	gSql.Begin()
	var v int64
	for _, file := range sqlFiles {
		if file.Version > nowVersion {
			logs.Infof("Excel [%s] begin", file.FileName)
			gSql.MustExcel(file.SqlStr)
			gSql.MustExcel("update `server_node` set `sql_version` = ? where `is_merge` = ?", file.Version, false)
			v = file.Version
		}
	}

	return v
}

func doCreateSqlTableFromDir(dirPath string) {
	// 获取文件夹下所有文件
	fileList, err := ioutil.ReadDir(dirPath)
	assert.Assert(err == nil, err, dirPath)

	// 执行所有.sql文件脚本
	gSql.Begin()
	for _, file := range fileList {
		filePath := dirPath + "/" + file.Name()
		sqlBytes, _ := ioutil.ReadFile(filePath)
		logs.Infof("Excel [%s] begin", filePath)

		gSql.MustExcel(string(sqlBytes))
	}
}
