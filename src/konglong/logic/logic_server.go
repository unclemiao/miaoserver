package logic

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/base"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"

	"github.com/gorilla/websocket"
)

func logFile() {
	InitLog(gGameFlag.LogPath)
}

// 事件开始前调用函数
func beforeCall(ctx *data.Context) {
	now := time.Now()
	ctx.BeginTime = time_helper.UnixMill(now)
	ctx.NowMill = ctx.BeginTime
	ctx.NowSec = ctx.NowMill / 1e3
	ctx.NowTime = now
	ctx.Sql = gSql
	gSql.Begin()
}

// 防攻击
func antiDos() {

}

// 消息结束后
func afterCall(ctx *data.Context) {
	ctx.EndTime = time_helper.NowMill()
	subMs := ctx.EndTime - ctx.BeginTime
	if subMs > 30 {
		logs.Warnf("event[%s] call over than %d ms", ctx.MsgId.String(), subMs)
	}
}

// 消息产生错误是回调
func errCall(ctx *data.Context, err interface{}) {
	defer func() {
		err2 := recover()
		if err2 != nil {
			logs.Error(err2)
		}
	}()
	sql := gSql
	if ctx.Sql != nil {
		sql = ctx.Sql
	}
	if err != nil {
		sql.Rollback()
		errstr := fmt.Sprintf("%s", err)
		if ctx.Session != nil {
			gSessionMgr.Clean(ctx.Session)
			if ctx.EventMsg.IsNetEvent() {
				env := ctx.EventMsg.GetEnvMsg()
				env.Error = &protocol.ErrorCode{}
				env.Error.Code = 501
				env.Error.Msg = util.ChoiceStr(errstr[0] == '!', errstr[1:], errstr)
				gSessionMgr.SendAtOnce(ctx.Session, env)
			}
		}
		if errstr[0] == '!' {
			logs.Info(errstr)
		} else {
			logs.Infof("ErrMsg:[%s] roleId[%d] msg:[%+v]", ctx.MsgId.String(), ctx.RoleId, ctx.EventMsg.GetEnvMsg())
			logs.Error(errstr)
		}

	} else {
		sql.Commit()
		if ctx.Session != nil {
			gSessionMgr.Flush(ctx.Session)
		}
	}
	if ctx.FinishFunc != nil {
		ctx.FinishFunc(err)
	}
}

func beginNet() {
	go func() {
		err := gServer.Net.Run(fmt.Sprintf("/game%03d", gServer.Node.Sid))
		assert.Assert(err == nil, err)
	}()
}

func heartbeat(ctx *data.Context) {

	now := time.Now()
	elapsed := (60-now.Second())*1e9 - now.Nanosecond()
	TimeAfter(nil, time.Duration(elapsed)*time.Nanosecond, heartbeat)

	nowMinute := time_helper.UnixMin(now)
	gSql.Begin()
	for gServer.Node.LastMinute < nowMinute {
		gServer.Node.LastMinute++
		if gAppCnf.Server.IsMaster {
			gSql.MustExcel("update server_node set last_minute = ? where sid = ?", gServer.Node.LastMinute, gServer.Node.Sid)
		}
		minutely(ctx, gServer.Node.LastMinute*gConst.MINUTE, time.Unix(gServer.Node.LastMinute*gConst.MINUTE, 0))
	}

	go sendToCenter()
}

func InitServer(winPath string, port int32) {
	// 初始化
	gServer = &data.Server{
		IsStarted:    false,
		Stop:         nil,
		ReceiveChan:  make(chan *data.Context, gConst.SERVER_RECEIVE_SIZE),
		StopChan:     make(chan struct{}),
		NodeRolesMap: make(map[int32]int32),
	}

	gEventMgr = new(base.EventManager)
	gEventMgr.Init(logs.GetLogger())

	// 加载sql文件
	InitSql(winPath, gAppCnf)
	// 初始化网络
	InitNet(port)

	logs.Infof("server init finish")
}

func LoadServer() {
	var (
		nodeList []*data.ServerNode
		master   *data.ServerNode
		nowSec   = time_helper.NowSec()
	)
	gSql.Begin()
	gSql.MustSelect(&nodeList, "select * from server_node")

	// 初始化node
	if len(nodeList) == 0 {
		assert.Assert(gAppCnf.Server.IsMaster, "master not open")
		// 第一次开服
		master = &data.ServerNode{
			Sid:        gAppCnf.Server.Sid,
			IsMerge:    false,
			BeginTime:  nowSec,
			LastMinute: 0,
			SqlVersion: GetUpdateMaxVersion("./"),
		}
		nodeList = append(nodeList, master)
		gSql.MustExcel("insert into server_node (sid, is_merge, begin_time, last_minute, sql_version) values (?, ?, ?, ?, ?)",
			master.Sid, master.IsMerge, master.BeginTime, master.LastMinute, master.SqlVersion)
		gEventMgr.Call(nil, data.EVENT_MSGID_INIT_SERVER, &data.EventInitServer{
			NowSec: nowSec,
			Now:    time.Now(),
		})
		logs.Info("insert into server_node")
	} else if len(nodeList) == 1 {
		master = nodeList[0]
		// 检测服务器号是否不一致
		assert.Assert(master.Sid == gAppCnf.Server.Sid, "invalid server_node: %d", master.Sid)

	} else {
		for _, n := range nodeList {
			if !n.IsMerge {
				master = n
				assert.Assert(master.Sid == gAppCnf.Server.Sid, "invalid server_node: %d", master.Sid)
				break
			}
		}
	}
	gServer.Node = master
	gServer.NodeList = nodeList
	for _, node := range nodeList {
		gServer.NodeIdList = append(gServer.NodeIdList, node.Sid)
		gServer.NodeRolesMap[node.Sid] = 0
	}

	// 更新 node
	assert.Assert(master != nil, "invalid master")

	master.LastMinute = util.MaxInt64(
		master.LastMinute,
		time_helper.Eod(time.Now().AddDate(0, 0, -1)).Unix()/gConst.MINUTE,
	)

	ver := UpdateSqlTable("./", master.SqlVersion)
	if ver > master.SqlVersion {
		master.SqlVersion = ver
		gSql.MustExcel("update `server_node` SET `sql_version` = ? where `sid` = ?", ver, master.Sid)
	}

	TimeAfter(nil, 1, heartbeat)

}

func run() {

	for {
		select {
		case bs := <-gServer.ReceiveChan:
			OnEvent(bs)
		}

	}

}

func OnEvent(ctx *data.Context) {
	if ctx.EventMsg.IsNetEvent() && ctx.Session != nil && gIsLog {
		logs.Debugf("OnEvent RoleId:%d, MsgId:%s, Msg:%+v",
			ctx.Session.RoleId, ctx.EventMsg.GetMsgId().String(), ctx.EventMsg.GetEnvMsg())
	}
	defer func() {
		err := recover()
		errCall(ctx, err)
	}()
	beforeCall(ctx)
	gEventMgr.Call(ctx, ctx.EventMsg.GetMsgId(), ctx.EventMsg)
	afterCall(ctx)
}

func Stop() {
	err := gLogFile.Close()
	if err != nil {
		logs.Error(err)
	}
}

func onConnect(conn *websocket.Conn) {
	gSessionMgr.NewSession(conn)
}

func onData(conn *websocket.Conn, raw []byte) {
	session := gSessionMgr.SessionBy(conn)
	msg := &protocol.Envelope{}
	err := msg.Unmarshal(raw)
	if err != nil {
		logs.Error("onData msg Unmarshal err: ", err.Error())
		return
	}

	if msg.MsgType == protocol.EnvelopeType_C2S_Ping {
		gSessionMgr.SendAtOnce(session, &protocol.Envelope{
			SeqId:   msg.SeqId,
			MsgType: protocol.EnvelopeType_S2C_Pong,
			Payload: &protocol.Envelope_S2CPong{S2CPong: &protocol.S2C_PongMsg{}},
		})
	} else {
		if len(msg.AdSdkToken) > 0 {
			if !authAdSdk(session, msg, raw) {
				logs.Error("ad sdk token err")
				return
			}
		}
		if msg.MsgType != protocol.EnvelopeType_C2S_SigninPlayer && len(gServer.ReceiveChan) >= 1000 {
			logs.Warn("ReceiveChan len too manay")
			return
		}
		gServer.ReceiveChan <- &data.Context{
			Session: session,
			MsgId:   msg.MsgType,
			EventMsg: &data.EventNet{
				EventBase: data.EventBase{MsgId: msg.MsgType},
				Msg:       msg,
			},
		}
	}

}

func onClose(conn *websocket.Conn) {
	session := gSessionMgr.SessionBy(conn)
	if session != nil {
		roleId := session.RoleId
		session.RoleId, session.AccountId = 0, ""

		if !session.IsStop {
			gServer.ReceiveChan <- &data.Context{
				Session: session,
				MsgId:   data.EVENT_MSGID_OFFLINE_ROLE,
				EventMsg: &data.EventOfflineRole{
					EventBase: data.EventBase{MsgId: data.EVENT_MSGID_OFFLINE_ROLE},
					RoleId:    roleId,
					SignOut:   false,
				},
			}
		}
		gSessionMgr.DelSession(conn)
	}
}

func loadConfig() {
	LoadConfig()
}

func afterConfig() {
	AfterConfig()
}

func StarServer() {
	defer func() {
		err := recover()
		if err != nil {
			logs.Error(err)
			if gSql != nil {
				gSql.Rollback()
			}
		} else {
			if gSql != nil {
				gSql.Commit()
			}
		}
		gSql.Close()
	}()

	InitConfig("./")
	gIsLog = true
	// 开始admin
	gAdminMgr.StarServer()
	gAdminMgr.ServerCmd.Port = util.ChoiceInt32(gAppCnf.Server.Port > 0, gAppCnf.Server.Port, gAdminMgr.ServerCmd.Port)
	// 初始化服务器
	InitServer("./", gAdminMgr.ServerCmd.Port)

	gIsLog = gAdminMgr.ServerCmd.Platform == "test"

	loadConfig()

	// 开始运行服务器
	LoadServer()

	afterConfig()

	// 网络初始化
	beginNet()

	go run()

	<-gServer.StopChan
}

func minutely(ctx *data.Context, nowSec int64, now time.Time) {
	ctx.Sid = gServer.Node.Sid
	nh, nm := now.Hour(), now.Minute()
	if nh+nm == 0 {
		InitLog(gGameFlag.LogPath)
	}
	logs.Info("========================== server minutely ==========================")
	gEventMgr.Call(ctx, data.EVENT_MSGID_MINUTELY, &data.EventMinutely{NowSec: nowSec, Now: now})
	if nm == 0 {
		hourly(ctx, nowSec, int64(nh), now)
	}
	if nh+nm == 0 {
		w := now.Weekday()
		daily(ctx, nowSec, now, w)
	}

}

func hourly(ctx *data.Context, nowSec int64, hour int64, now time.Time) {
	logs.Info("========================== server hourly   ==========================")
	gEventMgr.Call(ctx, data.EVENT_MSGID_HOURLY, &data.EventHourly{Hour: hour, Now: now})
}

func daily(ctx *data.Context, nowSec int64, now time.Time, week time.Weekday) {
	logs.Info("========================== server daily    ==========================")
	gEventMgr.Call(ctx, data.EVENT_MSGID_DAILY, &data.EventDaily{Week: week, IsNewWeek: week == time.Monday, Now: now, NowSec: nowSec})
}

func sendToCenter() {
	defer func() {
		err := recover()
		if err != nil {
			logs.Error("sendToCenter err:", err)
		}
	}()
	centerHost := gGameFlag.CenterHost
	adminHost := gGameFlag.AdminHost

	sid, plt, ch, online, token := gAdminMgr.ServerCmd.Sid, gAdminMgr.ServerCmd.Platform, gAdminMgr.ServerCmd.Channel,
		gSessionMgr.SessionNumber, gAdminMgr.ServerCmd.Token

	postForm := make(url.Values)
	postForm.Add("id", fmt.Sprintf("%d", gAppCnf.Server.Id))
	postForm.Add("sid", fmt.Sprintf("%d", sid))
	postForm.Add("platform", plt)
	postForm.Add("channel", ch)
	postForm.Add("adminHost", adminHost)
	postForm.Add("port", fmt.Sprintf("%d", gAdminMgr.ServerCmd.Port))
	postForm.Add("master", fmt.Sprintf("%v", gAppCnf.Server.IsMaster))
	postForm.Add("online", fmt.Sprintf("%d", online))

	// slave
	var slave string
	for _, n := range gServer.NodeList {
		if n.IsMerge {
			if slave == "" {
				slave = fmt.Sprintf("%d", n.Sid)
			} else {
				slave += fmt.Sprintf(",%d", n.Sid)
			}
		}
	}
	postForm.Add("slaveList", "["+slave+"]")

	md5 := util.Md5(fmt.Sprintf("%d%s%s%d%s", sid, plt, ch, online, token))
	postForm.Add("weiyingToken", md5)

	logs.Infof("sendToCenter: %s, %+v", centerHost+"/server/ping", postForm)

	resp, err := http.PostForm(centerHost+"/server/ping", postForm)
	if err != nil {
		logs.Error(err)
		return
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		return
	}
	logs.Infof("sendToCenter response: %s", string(bs))
}
