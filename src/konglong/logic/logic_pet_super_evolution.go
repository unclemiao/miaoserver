package logic

import (
	"encoding/json"
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type PetSuperEvolutionManager struct {
	cache *cache.ShardedCache
}

var gPetSuperEvolutionMgr *PetSuperEvolutionManager

func init() {
	gPetSuperEvolutionMgr = &PetSuperEvolutionManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Minute, 2^4),
	}
}

func (mgr *PetSuperEvolutionManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetSuperEvolution, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.GetPetSuperEvolution(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AddSuperEvolutionProgress, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SAddSuperEvolutionProgress()
		)
		adSdk := len(msg.AdSdkToken) > 0
		if adSdk {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk)
		} else {
			gQuotaCdMgr.TakeQuota(ctx, ctx.RoleId, 1, protocol.QuotaType_Quota_Super_Evolution_Free)
		}
		mgr.AddProgress(ctx, ctx.RoleId, rep.CfgId, rep.PetIdList)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetSuperEvolution, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SPetSuperEvolution()
		)
		mgr.SuperEvolution(ctx, ctx.RoleId, rep.PetId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetSuperEvolutionItemBuy, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.SuperEvolutionBuy(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetStudySkill, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SPetStudySkill()
		)
		mgr.StudySkill(ctx, ctx.RoleId, rep.PetId, rep.Slot, rep.EatPetId, len(msg.AdSdkToken) > 0)
	})

}

func (mgr *PetSuperEvolutionManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_PET_SUPER_EVOLUTION, roleId)
}

func (mgr *PetSuperEvolutionManager) RolePetSuperEvolutionBy(ctx *data.Context, roleId int64) *data.RolePetSuperEvolution {
	cacheKey := mgr.getCacheKey(roleId)

	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {

		roleData := &data.RolePetSuperEvolution{
			PetSuperEvolutionMap: make(map[int32]*data.PetSuperEvolution),
		}

		var dataList []*data.PetSuperEvolution
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_SUPER_EVOLUTION, roleId)
		for _, d := range dataList {
			roleData.PetSuperEvolutionMap[d.CfgId] = d
			cfg := config.GetPetSuperEvolutionCfg(d.CfgId)
			if d.Progress >= cfg.Progress {
				roleData.FinishNum++
			}
		}

		return roleData, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	return rv.(*data.RolePetSuperEvolution)
}

func (mgr *PetSuperEvolutionManager) PetSuperEvolutionBy(ctx *data.Context, roleId int64, cfgId int32) *data.PetSuperEvolution {
	roleData := mgr.RolePetSuperEvolutionBy(ctx, roleId)
	return roleData.PetSuperEvolutionMap[cfgId]
}

func (mgr *PetSuperEvolutionManager) GetPetSuperEvolution(ctx *data.Context, roleId int64) {
	var list []*protocol.PetSuperEvolutionLockInfo
	// for _, d := range mgr.RolePetSuperEvolutionBy(ctx, roleId).PetSuperEvolutionMap {
	// 	list = append(list, &protocol.PetSuperEvolutionLockInfo{
	// 		CfgId:    d.CfgId,
	// 		Progress: d.Progress,
	// 	})
	// }
	for _, cfg := range config.GetAllPetSuperEvolutionCfg() {
		list = append(list, &protocol.PetSuperEvolutionLockInfo{
			CfgId:    cfg.CfgId,
			Progress: cfg.Progress,
		})
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifySuperEvolutionMsg(list))
}

func (mgr *PetSuperEvolutionManager) newProgress(ctx *data.Context, roleId int64, cfgId int32, add int32) *data.PetSuperEvolution {
	roleData := mgr.RolePetSuperEvolutionBy(ctx, roleId)
	gSql.Begin().Clean(roleId)

	petSuperEvolution := &data.PetSuperEvolution{
		Id:       gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_SUPER_EVOLUTION),
		RoleId:   roleId,
		CfgId:    cfgId,
		Progress: add,
	}
	roleData.PetSuperEvolutionMap[petSuperEvolution.CfgId] = petSuperEvolution

	gSql.MustExcel(gConst.SQL_INSERT_SUPER_EVOLUTION, petSuperEvolution.Id, petSuperEvolution.RoleId, petSuperEvolution.CfgId, petSuperEvolution.Progress)

	return petSuperEvolution
}

func (mgr *PetSuperEvolutionManager) AddProgress(ctx *data.Context, roleId int64, cfgId int32, petIdList []int64) {
	var petSuperEvolution *data.PetSuperEvolution
	role := gRoleMgr.RoleBy(ctx, roleId)
	cfg := config.GetPetSuperEvolutionCfg(cfgId)
	assert.Assert(cfg != nil, "invalid cfgId:%d", cfgId)
	gSql.Begin().Clean(roleId)
	roleSuperEvolution := mgr.RolePetSuperEvolutionBy(ctx, roleId)

	var add int32
	var petList []*data.Pet
	for _, petId := range petIdList {
		pet := gPetMgr.PetBy(ctx, roleId, petId)
		petList = append(petList, pet)
		petCfg := config.GetPetCfg(pet.CfgId)
		for _, kinds := range cfg.VariationStuff {
			if petCfg.Grade == kinds[0] {
				add += kinds[1]
				break
			}
		}
		for _, kinds := range cfg.AttrStuff {
			switch kinds[0] {
			case 1:
				if pet.AtkAdd >= kinds[1] {
					add += kinds[2]
				}
			case 2:
				if pet.DefAdd >= kinds[1] {
					add += kinds[2]
				}
			case 3:
				if pet.HpMaxAdd >= int64(kinds[1]) {
					add += kinds[2]
				}
			}
		}
	}
	assert.Assert(add > 0, "invalid pet")
	gPetMgr.TakePet(ctx, roleId, petList, petIdList, protocol.PetActionType_Pet_Action_Super_Evolution)

	petSuperEvolution = mgr.PetSuperEvolutionBy(ctx, roleId, cfgId)
	if petSuperEvolution == nil {
		add = util.MinInt32(add, cfg.Progress)
		petSuperEvolution = mgr.newProgress(ctx, roleId, cfgId, add)
	} else {
		petSuperEvolution.Progress = util.MinInt32(petSuperEvolution.Progress+add, cfg.Progress)
		gSql.MustExcel(gConst.SQL_UPDATE_SUPER_EVOLUTION, petSuperEvolution.Progress, petSuperEvolution.Id)
	}

	// 属性变更
	gStubMgr.NotifyStub(ctx, roleId, gConst.STUB_TYPE_NORMAL)
	gStubMgr.NotifyStub(ctx, roleId, gConst.STUB_TYPE_PET_KING)

	// 尝试解锁全部
	if petSuperEvolution.Progress >= cfg.Progress {
		roleSuperEvolution.FinishNum++
		if roleSuperEvolution.FinishNum == config.GetPetSuperEvolutionUnlockFinishNum() {
			gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
				Msg: fmt.Sprintf(config.GetGameTextCfg(
					gConst.TEXT_CFG_ID_PET_SUPER_EVOLUTION_UNLOCK).Text,
					role.Name,
					role.RoleId,
				),
				MsgType: protocol.MessageType_Msg_System,
			}, 0)
		}
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifySuperEvolutionMsg([]*protocol.PetSuperEvolutionLockInfo{
		{
			CfgId:    cfgId,
			Progress: petSuperEvolution.Progress,
		},
	}))
}

func (mgr *PetSuperEvolutionManager) SuperEvolution(ctx *data.Context, roleId int64, petId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	if !ctx.TrySuperEvolution {
		// assert.Assert(roleSuperEvolution.FinishNum == config.GetPetSuperEvolutionUnlockFinishNum(), "not finish")
	}

	pet := gPetMgr.PetBy(ctx, roleId, petId)
	petCfg := config.GetPetCfg(pet.CfgId)
	assert.Assert(len(petCfg.SuperEvolutionStuff.AwardList) > 0, "pet can not super evolution")
	assert.Assert(pet.Star >= petCfg.SuperEvolutionNeedStar, "pet star not enough")

	gItemMgr.TakeAward(ctx, roleId, petCfg.SuperEvolutionStuff, 1, protocol.ItemActionType_Action_Pet_Super_Evolution)

	gSql.Begin().Clean(roleId)
	pet.SuperEvolution = true
	gSql.MustExcel(gConst.SQL_UPDATE_PET_SUPER_EVOLUTION, pet.SuperEvolution, pet.Id)
	gAttrMgr.PetAttrBy(ctx, pet, false)
	if role.Horse == petId {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}
	gPetMgr.OnPetUpdate(ctx, roleId, pet)

	// 系统公告
	gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
		Msg: fmt.Sprintf(config.GetGameTextCfg(
			gConst.TEXT_CFG_ID_PET_SUPER_EVOLUTION).Text,
			role.Name,
			role.RoleId,
			util.ChoiceStr(pet.Variation, petCfg.VariationSuperEvolutionName, petCfg.SuperEvolutionName),
			pet.Id,
		),
		MsgType: protocol.MessageType_Msg_System,
	}, 0)

	petList := []*protocol.Pet{pet.GetForClient()}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, nil, protocol.PetActionType_Pet_Action_Super_Evolution))

}

func (mgr *PetSuperEvolutionManager) SuperEvolutionBuy(ctx *data.Context, roleId int64) {
	quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Super_Evolution_Item_Buy)
	if quota > 0 {
		cfg := config.GetSuperEvolutionBuyCfg(quota + 1)
		gItemMgr.TakeItem(ctx, roleId, nil, int64(protocol.CurrencyType_Diamonds), cfg.Price, protocol.ItemActionType_Action_Pet_Super_Evolution_Stone_Buy)
	}
	gItemMgr.GiveItem(ctx, roleId, int64(protocol.CurrencyType_EvolutionStone), 1, protocol.ItemActionType_Action_Pet_Super_Evolution_Stone_Buy)

	gQuotaCdMgr.GiveQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Super_Evolution_Item_Buy)
	lottery := gLotteryPetMgr.LotteryPetBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	lottery.SuperEvolutionBuyTime = 0
	gSql.MustExcel(gConst.SQL_UPDATE_LOTTERY_PET, lottery.Score, lottery.AwardStr, lottery.SuperEvolutionBuyTime, lottery.SuperEvolutionBuyAmount, lottery.RoleId)
	gLotteryPetMgr.NotifyUpdate(ctx, roleId, lottery)
}

func (mgr *PetSuperEvolutionManager) StudySkill(ctx *data.Context, roleId int64, petId int64, slot int32, eated int64, isRate bool) {
	slot--
	pet := gPetMgr.PetBy(ctx, roleId, petId)
	assert.Assert(pet.SuperEvolution, "pet not SuperEvolution")
	petCfg := config.GetPetCfg(pet.CfgId)
	eatedPet := gPetMgr.PetBy(ctx, roleId, eated)
	eatedPetCfg := config.GetPetCfg(eatedPet.CfgId)
	var rate int32
	var studySkill int32
	var studyNew bool

	if slot >= int32(len(pet.StudySkillCfgIdList)) {
		// 学新
		slot = util.MaxInt32(0, int32(len(pet.StudySkillCfgIdList)))
		studyNew = true
	}
	if slot >= 1 {
		assert.Assert(pet.Star >= petCfg.SkillStudyStar[slot], "star not enough")
	}

	findRate := false
	for _, rs := range petCfg.StudySlotRate {
		if rs[0] == slot+1 && eatedPetCfg.Grade == rs[1] {
			rate = util.ChoiceInt32(isRate, rs[3]*10, rs[2]*10)
			findRate = true
			break
		}
	}
	assert.Assert(findRate, "rate cfg not found")
	gSql.Begin().Clean(roleId)
	if util.RandomMatched(rate) {

		studySkill = util.ChoiceInt32(eatedPet.Variation, config.GetSkillCfg(eatedPet.SkillCfgId).VariationStudySkill, config.GetSkillCfg(eatedPet.SkillCfgId).StudySkill)

		if studyNew {
			pet.StudySkillCfgIdList = append(pet.StudySkillCfgIdList, studySkill)
		} else {
			pet.StudySkillCfgIdList[slot] = studySkill
		}

		bs, err := json.Marshal(pet.StudySkillCfgIdList)
		assert.Assert(err == nil, err)
		pet.StudySkillCfgIdListStr = string(bs)
		gSql.MustExcel(gConst.SQL_UPDATE_PET_STUDY_SKILL, pet.StudySkillCfgIdListStr, pet.Id)
	}

	gPetMgr.TakePet(ctx, roleId, []*data.Pet{eatedPet}, []int64{eated}, protocol.PetActionType_Pet_Action_Study_Skill)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, []*protocol.Pet{pet.GetForClient()}, nil, protocol.PetActionType_Pet_Action_Study_Skill))
}
