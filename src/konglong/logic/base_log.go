package logic

import (
	"goproject/engine/logs"
)

func InitLog(logPath string) {
	logs.InitLog(logPath)

	logs.Infof("InitLog finish")
}
