package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type QuotaCdManager struct {
	AddAdSdkTimer map[int64]*time.Timer
}

type QuotaRuleManager struct {
	QuotaFuncMap map[protocol.QuotaType]*data.QuotaFunc
	CdFuncMap    map[protocol.CdType]*data.CdFunc
}

var quotaRuleMgr *QuotaRuleManager

func init() {
	quotaRuleMgr = &QuotaRuleManager{
		QuotaFuncMap: make(map[protocol.QuotaType]*data.QuotaFunc),
		CdFuncMap:    make(map[protocol.CdType]*data.CdFunc),
	}
	gQuotaCdMgr = &QuotaCdManager{
		AddAdSdkTimer: make(map[int64]*time.Timer),
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Lottery_Item] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).LotteryItem
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Lottery_Item).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.LotteryItem
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.LotteryItem += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_ITEM, quota.LotteryItem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Item,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.LotteryItem >= n, "!次数已满")
			quota.LotteryItem -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_ITEM, quota.LotteryItem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Item,
					QuotaValue: max - quota.LotteryItem,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Lottery_Item_Add] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).LotteryItemAdd
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Lottery_Item_Add).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.LotteryItemAdd
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.LotteryItemAdd += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_ITEM_ADD, quota.LotteryItemAdd, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Item_Add,
					QuotaValue: left - n,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Lottery_Item_Gem_Add] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).LotteryItemGemAdd
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Lottery_Item_Gem_Add).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.LotteryItemGemAdd
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.LotteryItemGemAdd += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_ITEM_GEM_ADD, quota.LotteryItemGemAdd, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Item_Gem_Add,
					QuotaValue: left - n,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Paral] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).Paral
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Paral).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.Paral
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.Paral += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL, quota.Paral, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.Paral >= n, "!次数已满")
			quota.Paral -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL, quota.Paral, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral,
					QuotaValue: max - quota.Paral,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Paral_Ad_Sdk] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).ParalAdSdk
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Paral_Ad_Sdk).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.ParalAdSdk
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.ParalAdSdk += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL_AD_SDK, quota.ParalAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral_Ad_Sdk,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.ParalAdSdk >= n, "!次数已满")
			quota.ParalAdSdk -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL_AD_SDK, quota.ParalAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral_Ad_Sdk,
					QuotaValue: max - quota.ParalAdSdk,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Paral_Gem] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).ParalGem
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Paral_Gem).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.ParalGem
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.ParalGem += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL_GEM, quota.ParalGem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral_Gem,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.ParalGem >= n, "!次数已满")
			quota.ParalGem -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PARAL_GEM, quota.ParalGem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Paral_Gem,
					QuotaValue: max - quota.ParalGem,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Duel] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).Duel
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Duel).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.Duel
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.Duel += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL, quota.Duel, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.Duel >= n, "!次数已满")
			quota.Duel -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL, quota.Duel, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel,
					QuotaValue: max - quota.Duel,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Duel_Ad_Sdk] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).DuelAdSdk
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 20
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Duel_Ad_Sdk).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.DuelAdSdk
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.DuelAdSdk += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL_AD_SDK, quota.DuelAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel_Ad_Sdk,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.DuelAdSdk >= n, "!次数已满")
			quota.DuelAdSdk -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL_AD_SDK, quota.DuelAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel_Ad_Sdk,
					QuotaValue: max - quota.DuelAdSdk,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Duel_Gem] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).DuelGem
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 20
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Duel_Gem).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.DuelGem
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.DuelGem += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL_GEM, quota.DuelGem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel_Gem,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.DuelGem >= n, "!次数已满")
			quota.DuelGem -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DUEL_GEM, quota.DuelGem, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Duel_Gem,
					QuotaValue: max - quota.DuelGem,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Game_Pass_Ext_Award_Amount] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return gQuotaCdMgr.QuotaBy(ctx, roleId).GamePassExtAward
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return int32(len(config.GamePassCfgListBy()))
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			logs.Warn("should not call takeGamePassExtAwardQuota")
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			quota.GamePassExtAward += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_GAME_PASS_EXT_AWARD, quota.GamePassExtAward, roleId)
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Game_Drop_Diamond] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).GameDropDiamond
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetGameDropCfg(2).Limit
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.GameDropDiamond
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.GameDropDiamond += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_GAME_DROP_DIAMOND, quota.GameDropDiamond, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Game_Drop_Diamond,
					QuotaValue: left - n,
				},
			}, nil))

		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Game_Drop_Pet] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).GameDropPet
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return config.GetGameDropCfg(3).Limit
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.GameDropPet
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.GameDropPet += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_GAME_DROP_PET, quota.GameDropPet, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Game_Drop_Pet,
					QuotaValue: left - n,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Free_Fast_Pass] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).FreeFastPass
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 1
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Free_Fast_Pass).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.FreeFastPass
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.FreeFastPass += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_FREE_FAST_PASS, quota.FreeFastPass, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Fast_Pass,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.FreeFastPass >= n, "!次数已满")
			quota.FreeFastPass -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_FREE_FAST_PASS, quota.FreeFastPass, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Fast_Pass,
					QuotaValue: max - quota.FreeFastPass,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Ad_Fast_Pass] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).AdFastPass
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 20
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Ad_Fast_Pass).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.AdFastPass
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.AdFastPass += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_AD_FAST_PASS, quota.AdFastPass, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Ad_Fast_Pass,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.AdFastPass >= n, "!次数已满")
			quota.AdFastPass -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_AD_FAST_PASS, quota.AdFastPass, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Ad_Fast_Pass,
					QuotaValue: max - quota.AdFastPass,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Gem_Fast_Pass] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).GemFastPass
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 20  gConst.QUOTA_AD_FAST_PASS_MAX
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Gem_Fast_Pass).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.GemFastPass
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.GemFastPass += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_GEM_FAST_PASS, quota.GemFastPass, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Gem_Fast_Pass,
					QuotaValue: left - n,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Daily_Free_Lottery_Pet] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).DailyFreeLotteryPet
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 1
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Daily_Free_Lottery_Pet).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.DailyFreeLotteryPet
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.DailyFreeLotteryPet += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_FREE_LOTTERY_PET, quota.DailyFreeLotteryPet, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Daily_Free_Lottery_Pet,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.DailyFreeLotteryPet >= n, "!次数已满")
			quota.DailyFreeLotteryPet -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_FREE_LOTTERY_PET, quota.DailyFreeLotteryPet, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Daily_Free_Lottery_Pet,
					QuotaValue: max - quota.DailyFreeLotteryPet,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Free_Refresh_Duel] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).FreeRefreshDuel
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 5
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Free_Refresh_Duel).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.FreeRefreshDuel
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.FreeRefreshDuel += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_FREE_REFRESH_DUEL, quota.FreeRefreshDuel, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Refresh_Duel,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.FreeRefreshDuel >= n, "!次数已满")
			quota.FreeRefreshDuel -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_FREE_REFRESH_DUEL, quota.FreeRefreshDuel, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Refresh_Duel,
					QuotaValue: max - quota.FreeRefreshDuel,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Lottery_Equip] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).LotteryEquip
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 10
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Lottery_Equip).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.LotteryEquip
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.LotteryEquip += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_EQUIP, quota.LotteryEquip, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Equip,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.LotteryEquip >= n, "!次数已满")
			quota.LotteryEquip -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_LOTTERY_EQUIP, quota.LotteryEquip, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Lottery_Equip,
					QuotaValue: max - quota.LotteryEquip,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Free_Fast_Venture] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).VentureFreeFast
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 2
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Free_Fast_Venture).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.VentureFreeFast
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.VentureFreeFast += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_VENTURE_FREE_FAST, quota.VentureFreeFast, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Fast_Venture,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.VentureFreeFast >= n, "!次数已满")
			quota.VentureFreeFast -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_VENTURE_FREE_FAST, quota.VentureFreeFast, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Free_Fast_Venture,
					QuotaValue: max - quota.VentureFreeFast,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Fast_Venture_Add] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).VentureAddFast
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 3
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Fast_Venture_Add).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.VentureAddFast
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.VentureAddFast += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_VENTURE_ADD_FAST, quota.VentureAddFast, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Fast_Venture_Add,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.VentureAddFast >= n, "!次数已满")
			quota.VentureAddFast -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_VENTURE_ADD_FAST, quota.VentureAddFast, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Fast_Venture_Add,
					QuotaValue: max - quota.VentureAddFast,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Fast_Venture_Gem_Add] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).VentureGemAddFast
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// return 3 gConst.QUOTA_VENTURE_ADD_FAST
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Fast_Venture_Gem_Add).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.VentureGemAddFast
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.VentureGemAddFast += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_VENTURE_GEM_ADD_FAST, quota.VentureGemAddFast, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Fast_Venture_Gem_Add,
					QuotaValue: left - n,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Pet_King_Free_Refresh] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).PetKingFreeRefresh
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 5
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Pet_King_Free_Refresh).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.PetKingFreeRefresh
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.PetKingFreeRefresh += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_FREE_REFRESH, quota.PetKingFreeRefresh, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Refresh,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.PetKingFreeRefresh >= n, "!次数已满")
			quota.PetKingFreeRefresh -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_FREE_REFRESH, quota.PetKingFreeRefresh, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Refresh,
					QuotaValue: max - quota.PetKingFreeRefresh,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Pet_King_Free_Fight] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).PetKingFreeFight
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 5
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Pet_King_Free_Fight).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.PetKingFreeFight
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.PetKingFreeFight += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_FREE_FIGHT, quota.PetKingFreeFight, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Fight,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.PetKingFreeFight >= n, "!次数已满")
			quota.PetKingFreeFight -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_FREE_FIGHT, quota.PetKingFreeFight, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Fight,
					QuotaValue: max - quota.PetKingFreeFight,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Pet_King_Ad_Fight] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).PetKingAdFight
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 5
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Pet_King_Ad_Fight).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.PetKingAdFight
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.PetKingAdFight += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_AD_FIGHT, quota.PetKingAdFight, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Ad_Fight,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			assert.Assert(quota.PetKingAdFight >= n, "!次数已满")
			quota.PetKingAdFight -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_PET_KING_AD_FIGHT, quota.PetKingAdFight, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Pet_King_Ad_Fight,
					QuotaValue: max - quota.PetKingAdFight,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Drop_Box] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return gQuotaCdMgr.QuotaBy(ctx, roleId).DropBox
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return 0
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			quota.DropBox += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DROP_BOX, quota.DropBox, roleId)
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Daily_Ad_Sdk] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return gQuotaCdMgr.QuotaBy(ctx, roleId).DailyAdSdk
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			return 0
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			quota.DailyAdSdk += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_AD_SDK, quota.DailyAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Daily_Ad_Sdk,
					QuotaValue: quota.DailyAdSdk,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).DailyCanLookAdSdk
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 30
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {

			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			logs.Debugf("TakeQuota QuotaType_Quota_Daily_Can_Look_Ad_Sdk %d-%d", quota.DailyCanLookAdSdk, n)
			left := max - quota.DailyCanLookAdSdk
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			if left <= 15 {
				// 没有次数了
				_, ok := gQuotaCdMgr.AddAdSdkTimer[roleId]
				if !ok {
					t := TimeAfter(ctx, time.Minute*2, func(ctx2 *data.Context) {
						quotaRuleMgr.recoveryDailyCanLookAdSdk(ctx2, roleId)
					}, ctx.Session)
					gQuotaCdMgr.AddAdSdkTimer[roleId] = t

				}
			}
			quota.DailyCanLookAdSdk += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK, quota.DailyCanLookAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {

			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			n = util.ChoiceInt32(quota.DailyCanLookAdSdk >= n, n, quota.DailyCanLookAdSdk)
			logs.Debugf("GiveQuota QuotaType_Quota_Daily_Can_Look_Ad_Sdk %d-%d", quota.DailyCanLookAdSdk, n)

			quota.DailyCanLookAdSdk -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK, quota.DailyCanLookAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk,
					QuotaValue: max - quota.DailyCanLookAdSdk,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).DailyCanLookAdSdkAdd
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 35
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {

			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			logs.Debugf("TakeQuota QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add %d-%d", quota.DailyCanLookAdSdkAdd, n)

			left := max - quota.DailyCanLookAdSdkAdd
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")

			quota.DailyCanLookAdSdkAdd += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK_ADD, quota.DailyCanLookAdSdkAdd, roleId)
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			n = util.ChoiceInt32(quota.DailyCanLookAdSdkAdd >= n, n, quota.DailyCanLookAdSdkAdd)
			logs.Debugf("GiveQuota QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add %d-%d", quota.DailyCanLookAdSdkAdd, n)

			quota.DailyCanLookAdSdkAdd -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK_ADD, quota.DailyCanLookAdSdkAdd, roleId)
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Super_Evolution_Free] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).SuperEvolutionFree
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 4
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Super_Evolution_Free).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.SuperEvolutionFree
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.SuperEvolutionFree += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_SUPER_EVOLUTION_FREE, quota.SuperEvolutionFree, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Free,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			gSql.Begin().Clean(roleId)
			quota.SuperEvolutionFree -= n
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_SUPER_EVOLUTION_FREE, quota.SuperEvolutionFree, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Free,
					QuotaValue: max - quota.SuperEvolutionFree,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return max - gQuotaCdMgr.QuotaBy(ctx, roleId).SuperEvolutionAdSdk
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 2
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := max - quota.SuperEvolutionAdSdk
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.SuperEvolutionAdSdk += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_SUPER_EVOLUTION_AD_SDK, quota.SuperEvolutionAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk,
					QuotaValue: left - n,
				},
			}, nil))
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			gSql.Begin().Clean(roleId)
			quota.SuperEvolutionAdSdk -= n
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_SUPER_EVOLUTION_AD_SDK, quota.SuperEvolutionAdSdk, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk,
					QuotaValue: max - quota.SuperEvolutionAdSdk,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Super_Evolution_Item_Buy] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return gQuotaCdMgr.QuotaBy(ctx, roleId).SuperEvolutionItemBuy
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 2
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Super_Evolution_Item_Buy).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {

		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			gSql.Begin().Clean(roleId)
			quota.SuperEvolutionItemBuy += n
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_SUPER_EVOLUTION_ITEM_BUY, quota.SuperEvolutionItemBuy, roleId)
			gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg([]*protocol.Quota{
				{
					QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Item_Buy,
					QuotaValue: quota.SuperEvolutionItemBuy,
				},
			}, nil))
		},
	}

	quotaRuleMgr.QuotaFuncMap[protocol.QuotaType_Quota_Try_Variation] = &data.QuotaFunc{
		QuotaNumBy: func(ctx *data.Context, roleId int64, max int32) int32 {
			return gQuotaCdMgr.QuotaBy(ctx, roleId).TryVariation
		},
		QuotaMaxBy: func(ctx *data.Context, roleId int64) int32 {
			// 501
			return config.GetQuotaMaxCfg(protocol.QuotaType_Quota_Try_Variation).Max
		},
		TakeQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			left := quota.TryVariation
			assert.Assert(n > 0, "invalid n")
			assert.Assert(left >= n, "!次数不足")
			quota.TryVariation -= n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_TRY_VARIATION, quota.TryVariation, roleId)
		},
		GiveQuota: func(ctx *data.Context, roleId int64, n int32, max int32) {
			quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
			assert.Assert(n > 0, "invalid n")
			quota.TryVariation += n
			gSql.Begin().Clean(roleId)
			gSql.MustExcel(gConst.SQL_UPDATE_QUOTA_TRY_VARIATION, quota.TryVariation, roleId)
		},
	}

}

func (mgr *QuotaRuleManager) recoveryDailyCanLookAdSdk(ctx *data.Context, roleId int64) {

	oldT := gQuotaCdMgr.AddAdSdkTimer[roleId]
	if oldT != nil {
		oldT.Stop()
	}
	delete(gQuotaCdMgr.AddAdSdkTimer, roleId)
	canAdd := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add)
	logs.Debugf("canAdd...... %d", canAdd)
	if canAdd >= 0 {
		amount := gQuotaCdMgr.QuotaBy(ctx, roleId).DailyCanLookAdSdk
		logs.Debugf("amount...... %d", amount)
		if amount > 0 {
			giveQupta := util.MinInt32(amount, util.MinInt32(canAdd, util.Random32(5, 8)))
			gQuotaCdMgr.TakeQuota(ctx, roleId, giveQupta, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add)
			gQuotaCdMgr.GiveQuota(ctx, roleId, giveQupta, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk)
			t := TimeAfter(ctx, time.Minute*2, func(ctx2 *data.Context) {
				mgr.recoveryDailyCanLookAdSdk(ctx2, roleId)
			}, ctx.Session)
			gQuotaCdMgr.AddAdSdkTimer[roleId] = t
		}
	}

}

func (mgr *QuotaCdManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		quota := gQuotaCdMgr.QuotaBy(ctx, 0)
		if quota == nil {
			gSql.Begin().MustExcel(gConst.SQL_INSTER_QUOTA, 0)
			gCache.Delete(mgr.getQuotaCacheKey(0))
		}
	})

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getQuotaCacheKey(eventArgs.RoleId))
		gCache.Delete(mgr.getCdCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventOnlineRole)
		roleId := eventArgs.RoleId
		if eventArgs.IsNew {
			mgr.newQuotaCd(ctx, roleId)
		}
		left := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk)
		if left <= 15 {
			subSec := ctx.NowSec - ctx.OldLastSec
			if subSec < gConst.HOUR {
				t := TimeAfterFunc(ctx, time.Duration(gConst.HOUR-subSec)*time.Second, func(ctx2 *data.Context) {
					quotaRuleMgr.recoveryDailyCanLookAdSdk(ctx2, roleId)
				}, nil)
				gQuotaCdMgr.AddAdSdkTimer[roleId] = t
			} else {
				addAmount := subSec / gConst.HOUR
				var add int32
				for i := int64(0); i < addAmount; i++ {
					add += util.Random32(5, 8)
				}
				canAdd := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add)
				if canAdd >= 0 {
					quota := gQuotaCdMgr.QuotaBy(ctx, roleId)
					giveQupta := util.MinInt32(quota.DailyCanLookAdSdk, util.MinInt32(canAdd, add))
					gQuotaCdMgr.TakeQuota(ctx, roleId, giveQupta, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk_Add)
					gQuotaCdMgr.GiveQuota(ctx, roleId, giveQupta, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk)

				}

			}
		}
		TimeAfter(ctx, time.Second, func(ctx2 *data.Context) {
			mgr.Notify(ctx2, eventArgs.RoleId)
		}, ctx.Session)
	}, 100)

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventRoleNewDay)
		mgr.ResetQuota(ctx, eventArgs.RoleId)
	})

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventOfflineRole)
		if eventArgs.SignOut {
			t := mgr.AddAdSdkTimer[eventArgs.RoleId]
			if t != nil {
				t.Stop()
				delete(mgr.AddAdSdkTimer, eventArgs.RoleId)
			}
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AddQuota, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg    = msg.GetC2SAddQuota()
			roleId    = ctx.RoleId
			quotaType = repMsg.Type
			num       = repMsg.N
		)
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		ctx.ArgsInt = int64(quotaType)
		if quotaType == protocol.QuotaType_Quota_Lottery_Item {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Lottery_Item_Add)
			if quota <= 0 {
				assert.Assert(msg.AdSdkSkip, "not AdSdkSkip")
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Lottery_Item_Gem_Add)
			} else {
				mgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Lottery_Item_Add)
			}
		} else if quotaType == protocol.QuotaType_Quota_Duel {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Duel_Ad_Sdk)
			if quota <= 0 {
				assert.Assert(msg.AdSdkSkip, "not AdSdkSkip")
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Duel_Gem)
			} else {
				mgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Duel_Ad_Sdk)
			}
		} else if quotaType == protocol.QuotaType_Quota_Free_Fast_Venture {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Fast_Venture_Add)
			if quota <= 0 {
				assert.Assert(msg.AdSdkSkip, "not AdSdkSkip")
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Fast_Venture_Gem_Add)
			} else {
				mgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Fast_Venture_Add)
			}
		} else if quotaType == protocol.QuotaType_Quota_Paral {
			quota := gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Paral_Ad_Sdk)
			if quota <= 0 {
				assert.Assert(msg.AdSdkSkip, "not AdSdkSkip")
				gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Paral_Gem)
			} else {
				mgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Paral_Ad_Sdk)
			}
		}
		mgr.GiveQuota(ctx, roleId, num, quotaType)
	})

}

func (mgr *QuotaCdManager) getQuotaCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_QUOTA, roleId)
}

func (mgr *QuotaCdManager) getCdCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_CD, roleId)
}

func (mgr *QuotaCdManager) QuotaBy(ctx *data.Context, roleId int64) *data.Quota {
	cacheKey := mgr.getQuotaCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.Quota
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_QUOTA, roleId)
		if len(dataList) > 0 {
			return dataList[0], nil
		}
		return nil, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	if rv == nil {
		return nil
	}
	cache := rv.(*data.Quota)
	return cache
}

func (mgr *QuotaCdManager) CdBy(ctx *data.Context, roleId int64) *data.Cd {
	cacheKey := mgr.getCdCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.Cd
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_CD, roleId)
		if len(dataList) > 0 {
			return dataList[0], nil
		}
		return nil, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	cache := rv.(*data.Cd)
	return cache
}

func (mgr *QuotaCdManager) newQuotaCd(ctx *data.Context, roleId int64) {
	quota := &data.Quota{
		DailyCanLookAdSdk: 0,
	}
	cd := &data.Cd{}
	gSql.Begin().Clean(roleId)
	gSql.MustExcel(gConst.SQL_INSTER_QUOTA, roleId)
	gSql.MustExcel(gConst.SQL_INSTER_CD, roleId)
	gCache.Set(mgr.getQuotaCacheKey(roleId), quota, gConst.SERVER_DATA_CACHE_TIME_OUT)
	gCache.Set(mgr.getCdCacheKey(roleId), cd, gConst.SERVER_DATA_CACHE_TIME_OUT)
}

func (mgr *QuotaCdManager) GetForClient(ctx *data.Context, roleId int64) []*protocol.Quota {
	return []*protocol.Quota{
		{
			QuotaType:  protocol.QuotaType_Quota_Lottery_Item,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Lottery_Item),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Lottery_Item_Add,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Lottery_Item_Add),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Lottery_Item_Gem_Add,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Lottery_Item_Gem_Add),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Paral,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Paral),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Paral_Ad_Sdk,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Paral_Ad_Sdk),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Paral_Gem,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Paral_Gem),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Duel,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Duel),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Duel_Ad_Sdk,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Duel_Ad_Sdk),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Duel_Gem,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Duel_Gem),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Free_Fast_Pass,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Free_Fast_Pass),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Ad_Fast_Pass,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Ad_Fast_Pass),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Gem_Fast_Pass,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Gem_Fast_Pass),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Daily_Free_Lottery_Pet,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Free_Lottery_Pet),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Free_Refresh_Duel,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Free_Refresh_Duel),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Free_Fast_Venture,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Free_Fast_Venture),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Fast_Venture_Add,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Fast_Venture_Add),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Fast_Venture_Gem_Add,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Fast_Venture_Gem_Add),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Fight,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Pet_King_Free_Fight),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Pet_King_Ad_Fight,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Pet_King_Ad_Fight),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Pet_King_Free_Refresh,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Pet_King_Free_Refresh),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Lottery_Equip,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Lottery_Equip),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Daily_Ad_Sdk,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Ad_Sdk),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Daily_Can_Look_Ad_Sdk),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Free,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Super_Evolution_Free),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Super_Evolution_Ad_Sdk),
		},
		{
			QuotaType:  protocol.QuotaType_Quota_Super_Evolution_Item_Buy,
			QuotaValue: mgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Super_Evolution_Item_Buy),
		},
	}
}

func (mgr *QuotaCdManager) Notify(ctx *data.Context, roleId int64) {
	cd := mgr.CdBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdMsg(mgr.GetForClient(ctx, roleId), cd.GetForClient()))
}

func (mgr *QuotaCdManager) ResetQuota(ctx *data.Context, roleId int64) {
	quota := mgr.QuotaBy(ctx, roleId)

	quota.LotteryItem = 0
	quota.LotteryItemAdd = 0
	quota.LotteryItemGemAdd = 0
	quota.Paral = 0
	quota.ParalAdSdk = 0
	quota.ParalGem = 0
	quota.Duel = 0
	quota.DuelAdSdk = 0
	quota.DuelGem = 0
	quota.GameDropDiamond = 0
	quota.GameDropPet = 0
	quota.FreeFastPass = 0
	quota.AdFastPass = 0
	quota.GemFastPass = 0
	quota.DailyFreeLotteryPet = 0
	quota.FreeRefreshDuel = 0
	quota.VentureFreeFast = 0
	quota.VentureAddFast = 0
	quota.VentureGemAddFast = 0
	quota.PetKingFreeRefresh = 0
	quota.PetKingFreeFight = 0
	quota.PetKingAdFight = 0
	quota.DropBox = 0
	quota.DailyAdSdk = 0
	quota.DailyCanLookAdSdk = 0
	quota.DailyCanLookAdSdkAdd = 0
	quota.SuperEvolutionAdSdk = 0
	quota.SuperEvolutionFree = 0
	quota.SuperEvolutionItemBuy = 0

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_QUOTA_NEWDAY,
		quota.LotteryItem,
		quota.LotteryItemAdd,
		quota.LotteryItemGemAdd,
		quota.Paral,
		quota.ParalAdSdk,
		quota.ParalGem,
		quota.Duel,
		quota.DuelAdSdk,
		quota.DuelGem,
		quota.GameDropDiamond,
		quota.GameDropPet,
		quota.FreeFastPass,
		quota.AdFastPass,
		quota.GemFastPass,
		quota.DailyFreeLotteryPet,
		quota.VentureFreeFast,
		quota.VentureAddFast,
		quota.VentureGemAddFast,
		quota.FreeRefreshDuel,
		quota.PetKingFreeRefresh,
		quota.PetKingFreeFight,
		quota.PetKingAdFight,
		quota.DropBox,
		quota.DailyAdSdk,
		quota.DailyCanLookAdSdk,
		quota.DailyCanLookAdSdkAdd,
		quota.SuperEvolutionFree,
		quota.SuperEvolutionAdSdk,
		quota.SuperEvolutionItemBuy,
		roleId,
	)
	gRoleMgr.SendAtOnce(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg(mgr.GetForClient(ctx, roleId), nil))
}

// ************************ 次数上限查询 ****************************
func (mgr *QuotaRuleManager) quotaMaxBy(ctx *data.Context, roleId int64, quotaType protocol.QuotaType) int32 {
	f := quotaRuleMgr.QuotaFuncMap[quotaType].QuotaMaxBy
	if f != nil {
		return f(ctx, roleId)
	}
	return 0
}

// ************************ 次数上限查询 ****************************

// 返回剩余数量
func (mgr *QuotaCdManager) QuotaNumBy(ctx *data.Context, roleId int64, quotaType protocol.QuotaType) int32 {
	f := quotaRuleMgr.QuotaFuncMap[quotaType].QuotaNumBy
	if f != nil {
		max := quotaRuleMgr.quotaMaxBy(ctx, roleId, quotaType)
		return util.MaxInt32(0, f(ctx, roleId, max))
	}
	return 0
}

// ************************   扣除次数  *****************************
func (mgr *QuotaCdManager) TakeQuota(ctx *data.Context, roleId int64, n int32, quotaType protocol.QuotaType) {
	f := quotaRuleMgr.QuotaFuncMap[quotaType].TakeQuota
	if f != nil {
		f(ctx, roleId, n, quotaRuleMgr.quotaMaxBy(ctx, roleId, quotaType))
	}
}

// ****************************************************************************************************************

func (mgr *QuotaCdManager) GiveQuota(ctx *data.Context, roleId int64, n int32, quotaType protocol.QuotaType) {

	f := quotaRuleMgr.QuotaFuncMap[quotaType].GiveQuota
	if f != nil {
		f(ctx, roleId, n, quotaRuleMgr.quotaMaxBy(ctx, roleId, quotaType))
	}
}

// *********************************** cd 相关 ***********************************
func (mgr *QuotaCdManager) CleanCd(ctx *data.Context, roleId int64, nowSec int64, cdType protocol.CdType) {
	if cdType == protocol.CdType_CD_Lottery_Item_Quota_Cd {
		mgr.cleanLotteryItemCd(ctx, roleId, nowSec)
	}
}

func (mgr *QuotaCdManager) cleanLotteryItemCd(ctx *data.Context, roleId int64, nowSec int64) {
	roleCd := mgr.CdBy(ctx, roleId)
	left := (nowSec - int64(roleCd.LotteryItem)) % gConst.LOTTERY_ITEM_QUOTA_CD
	roleCd.LotteryItem = int32(nowSec - left + gConst.LOTTERY_ITEM_QUOTA_CD)
	gSql.Begin().Clean(roleId)
	gSql.MustExcel(gConst.SQL_UPDATE_CD_LOTTERY_ITEM, roleCd.LotteryItem, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg(nil, []*protocol.Cd{
		{
			CdType:  protocol.CdType_CD_Lottery_Item_Quota_Cd,
			CdValue: roleCd.LotteryItem,
		},
	}))
}

func (mgr *QuotaCdManager) BeginCd(ctx *data.Context, roleId int64, nowSec int64, cdType protocol.CdType) {
	if cdType == protocol.CdType_CD_Lottery_Item_Quota_Cd {
		mgr.beginLotteryItemCd(ctx, roleId, nowSec)
	} else if cdType == protocol.CdType_CD_Ad_Fast_Pass_Cd {
		mgr.beginAdFastPassCd(ctx, roleId, nowSec)
	}
}

func (mgr *QuotaCdManager) beginLotteryItemCd(ctx *data.Context, roleId int64, nowSec int64) {
	roleCd := mgr.CdBy(ctx, roleId)

	roleCd.LotteryItem = int32(nowSec + gConst.LOTTERY_ITEM_QUOTA_CD)
	gSql.Begin().Clean(roleId)
	gSql.MustExcel(gConst.SQL_UPDATE_CD_LOTTERY_ITEM, roleCd.LotteryItem, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg(nil, []*protocol.Cd{
		{
			CdType:  protocol.CdType_CD_Lottery_Item_Quota_Cd,
			CdValue: roleCd.LotteryItem,
		},
	}))
}

func (mgr *QuotaCdManager) beginAdFastPassCd(ctx *data.Context, roleId int64, nowSec int64) {
	roleCd := mgr.CdBy(ctx, roleId)
	assert.Assert(int64(roleCd.AdFastPass) <= ctx.NowSec, "AdFastPass cding, left:%d", int64(roleCd.AdFastPass)-ctx.NowSec)
	quota := mgr.QuotaBy(ctx, roleId)
	roleCd.AdFastPass = int32(nowSec + util.ChoiceInt64(quota.AdFastPass > 10, int64(quota.AdFastPass-10)*15, 15))
	gSql.Begin().Clean(roleId)
	gSql.MustExcel(gConst.SQL_UPDATE_CD_AD_FAST_PASS, roleCd.AdFastPass, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyQuotaCdUpdateMsg(nil, []*protocol.Cd{
		{
			CdType:  protocol.CdType_CD_Ad_Fast_Pass_Cd,
			CdValue: roleCd.AdFastPass,
		},
	}))
}
