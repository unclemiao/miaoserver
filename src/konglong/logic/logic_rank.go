package logic

import (
	"fmt"
	"sort"
	"sync"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/sql"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type RankManager struct {
	gamePassRank     []*data.Rank // 前 5000
	gamePassRoleRank map[int64]int32
	lock             sync.RWMutex
	sql              *sql.Sql
}

func init() {
	gRankMgr = &RankManager{
		gamePassRoleRank: make(map[int64]int32),
	}
}

func (mgr *RankManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		mgr.LoadGamePassRank(ctx, false)
	})

	gEventMgr.When(data.EVENT_MSGID_MINUTELY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventMinutely)
		if args.NowSec%(gConst.RANK_SORT_SUB_TIME_MINUTIL*time_helper.Minute) == 0 {
			go mgr.LoadGamePassRank(ctx, args.Now.Hour()+args.Now.Minute() == 0)
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_WinPassBoss, func(ctx *data.Context, msg *protocol.Envelope) {
		r := gRoleMgr.RoleBy(ctx, ctx.RoleId)

		myRank := mgr.MyRankBy(ctx, ctx.RoleId)
		mgr.UpdateRank(ctx, myRank, r.GamePass)

	}, 101)

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventRoleNewDay)
		role := gRoleMgr.RoleBy(ctx, args.RoleId)
		if role.Level < 8 {
			return
		}
		myRank := mgr.MyRankBy(ctx, args.RoleId)
		myRank.Awarded = false
		rank := mgr.GetRankIndex(ctx, args.RoleId)
		myRank.Rank = util.ChoiceInt32(rank > 0, rank, myRank.Rank)
		myRank.YesterDayBeforeRank = myRank.YesterDayRank
		myRank.YesterDayRank = myRank.Rank
		gSql.Begin().Clean(args.RoleId).MustExcel(gConst.SQL_UPDATE_RANK_GAME_PASS_YESTERDAY_RANK,
			myRank.YesterDayRank, myRank.YesterDayBeforeRank, false, myRank.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetRanking, mgr.HandlerGetRankList)

	gEventMgr.When(protocol.EnvelopeType_C2S_GetRankingAward, mgr.HandlerGetAward)
}

func (mgr *RankManager) LoadGamePassRank(ctx *data.Context, isDaily bool) {
	mgr.lock.Lock()
	defer func() {
		mgr.lock.Unlock()
		err := recover()
		if err != nil {
			logs.Error(err)
		}
	}()
	var rankList []*data.Rank
	rankSql, err := sql.LoadSql(&sql.Dsn{
		Driver:       "mysql",
		Host:         gAdminMgr.ServerCmd.SqlHost,
		Username:     gAdminMgr.ServerCmd.SqlRoot,
		Password:     gAdminMgr.ServerCmd.SqlPass,
		DatabaseName: gAdminMgr.ServerCmd.SqlName,
	})
	assert.Assert(err == nil, err)
	err = rankSql.Ping()
	assert.Assert(err == nil, err)
	defer rankSql.Close()

	rankSql.MustSelect(&rankList, gConst.SQL_SELECT_RANK_GAME_PASS)
	sort.Slice(rankList, func(i, j int) bool {
		if rankList[i].GamePass > rankList[j].GamePass {
			return true
		} else if rankList[i].GamePass == rankList[j].GamePass {
			return rankList[i].GamePassTime < rankList[j].GamePassTime
		}
		return false
	})
	mgr.gamePassRank = make([]*data.Rank, 1)

	mgr.gamePassRank = append(mgr.gamePassRank, rankList...)

	mgr.gamePassRoleRank = make(map[int64]int32)
	for index, rank := range rankList {
		rank.Rank = int32(index + 1)
		mgr.gamePassRoleRank[rank.RoleId] = rank.Rank

		var rlist []*data.Role
		rankSql.MustSelect(&rlist, gConst.SQL_SELECT_ROLE, rank.RoleId)
		if len(rlist) > 0 {
			r := rlist[0]
			rank.Avatar, rank.Name = r.Avatar, r.Name
		}

	}
}

func (mgr *RankManager) RangeGamePassRank(ctx *data.Context, min int32, max int32, f func(ctx *data.Context, rank *data.Rank) bool) {
	mgr.lock.RLock()
	defer mgr.lock.RUnlock()
	for i := min; i <= max; i++ {
		if i <= 0 || i >= int32(len(mgr.gamePassRank)) {
			break
		}
		if !f(ctx, mgr.gamePassRank[i]) {
			break
		}
	}
}

func (mgr *RankManager) GetRankIndex(ctx *data.Context, roleId int64) int32 {
	mgr.lock.RLock()
	defer mgr.lock.RUnlock()
	return mgr.gamePassRoleRank[roleId]
}

func (mgr *RankManager) getMyRankCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_MY_RANK, roleId)
}

func (mgr *RankManager) MyRankBy(ctx *data.Context, roleId int64) *data.Rank {
	cacheKey := mgr.getMyRankCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var rankList []*data.Rank
		gSql.MustSelect(&rankList, gConst.SQL_SELECT_MY_RANK, roleId)
		if len(rankList) == 0 {
			return nil, nil
		}
		return rankList[0], nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)

	mgr.lock.RLock()
	defer mgr.lock.RUnlock()
	rankIndex := mgr.gamePassRoleRank[roleId]
	if rv != nil {
		rank := rv.(*data.Rank)
		rank.Rank = util.ChoiceInt32(rankIndex > 0, rankIndex, gConst.RANK_AMOUNT+1)
		return rank
	} else {
		role := gRoleMgr.RoleBy(ctx, roleId)
		rank := mgr.NewRank(ctx, roleId, role.GamePass)
		return rank
	}
}

func (mgr *RankManager) NewRank(ctx *data.Context, roleId int64, pass int32) *data.Rank {
	rank := &data.Rank{
		RoleId:              roleId,
		Rank:                gConst.RANK_AMOUNT + 1,
		YesterDayRank:       gConst.RANK_AMOUNT + 1,
		YesterDayBeforeRank: gConst.RANK_AMOUNT + 1,
		GamePass:            pass,
		GamePassTime:        time_helper.NowMill(),
	}
	r := gRoleMgr.RoleBy(ctx, roleId)
	gCache.Set(mgr.getMyRankCacheKey(roleId), rank, gConst.SERVER_DATA_CACHE_TIME_OUT)
	mgr.gamePassRoleRank[roleId] = rank.Rank
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_RANK_GAME_PASS,
		rank.RoleId, r.Name, r.Avatar, rank.GamePass, rank.GamePassTime, rank.YesterDayRank, rank.YesterDayBeforeRank, false)
	return rank
}

func (mgr *RankManager) UpdateRank(ctx *data.Context, rank *data.Rank, pass int32) *data.Rank {
	rank.GamePass = pass
	rank.GamePassTime = time_helper.NowMill()
	gSql.Begin().Clean(rank.RoleId).MustExcel(gConst.SQL_UPDATE_RANK_GAME_PASS,
		rank.GamePass, rank.GamePassTime, rank.RoleId)
	return rank
}

func (mgr *RankManager) AwardRank(ctx *data.Context, roleId int64, sdkAd bool) {
	rank := mgr.MyRankBy(ctx, roleId)
	assert.Assert(!rank.Awarded, "ranking awarded")
	cfg := config.RankAwardCfgBy(rank.Rank)
	r := gRoleMgr.RoleBy(ctx, roleId)
	add := (int64(r.GamePass) / 100) * cfg.AddDiamonds
	gItemMgr.GiveItem(ctx, roleId, gConst.ITEM_CFG_ID_DIAMONDS, (cfg.Diamonds+add)*util.ChoiceInt64(sdkAd, 2, 1), protocol.ItemActionType_Action_Rank)
	rank.Awarded = true
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_RANK_GAME_PASS_AWARD, true, roleId)
}

// --------------------------------------- controllers -----------------------------------------
func (mgr *RankManager) HandlerGetRankList(ctx *data.Context, msg *protocol.Envelope) {
	rep := msg.GetC2SGetRanking()
	begin, end := rep.GetRankBegin(), rep.GetRankEnd()
	rankingList := make([]*protocol.RankInfo, 0, gConst.RANK_LIST_AMOUNT)
	if begin > 0 {
		mgr.RangeGamePassRank(ctx, begin, end, func(ctx *data.Context, rank *data.Rank) bool {
			rankingList = append(rankingList, rank.GetForClient())
			return true
		})
	}

	myRank := mgr.MyRankBy(ctx, ctx.RoleId)
	pb := myRank.GetSelfForClient()

	gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeGetRankingDoneMsg(rankingList, pb))
}

func (mgr *RankManager) HandlerGetAward(ctx *data.Context, msg *protocol.Envelope) {
	var (
		rep = msg.GetC2SGetRankingAward()
	)
	mgr.AwardRank(ctx, ctx.RoleId, rep.SdkAd)
}
