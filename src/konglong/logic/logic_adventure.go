package logic

import (
	"encoding/json"
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type VentureManager struct {
}

func init() {
	gVentureMgr = &VentureManager{}
}

func (mgr *VentureManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventMsg := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventMsg.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		eventMsg := msg.(*data.EventRoleNewDay)
		mgr.Daily(ctx, eventMsg.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetVenture, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.GetVenture(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_VentureWinPass, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.WinPass(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_VentureFastChapter, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SVentureFastChapter()
		)
		mgr.FastChapter(ctx, ctx.RoleId, rep.Chapter)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_VentureChoiceBuff, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SVentureChoiceBuff()
		)
		mgr.ChoiceBuff(ctx, ctx.RoleId, rep.Choice)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_VentureRefreshBuff, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			_ = msg.GetC2SVentureRefreshBuff()
		)

		// check ad_sdk
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.Refresh(ctx, ctx.RoleId)
	})
}

func (mgr *VentureManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_VENTURE, roleId)
}

func (mgr *VentureManager) VentureBy(ctx *data.Context, roleId int64) *data.Venture {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.Venture
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ADVENTURE, roleId)
		if len(dataList) > 0 {
			d := dataList[0]
			err2 := json.Unmarshal([]byte(d.BuffInfo), &d.BuffList)
			if err2 != nil {
				return nil, err2
			}

			err2 = json.Unmarshal([]byte(d.UngetBuffInfo), &d.UngetBuffList)
			if err2 != nil {
				return nil, err2
			}
			return d, nil
		}
		return nil, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)
	if rv == nil {
		return nil
	}
	return rv.(*data.Venture)
}

func (mgr *VentureManager) newVenture(ctx *data.Context, roleId int64) *data.Venture {
	venture := &data.Venture{
		RoleId:        roleId,
		Chapter:       1,
		BuffInfo:      "[]",
		UngetBuffInfo: "[]",
	}
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_ADVENTURE, venture.RoleId, venture.Chapter, venture.Passed, venture.BuffInfo, venture.UngetBuffInfo)
	gCache.Set(mgr.getCacheKey(roleId), venture, gConst.SERVER_DATA_CACHE_TIME_OUT)
	return venture
}

func (mgr *VentureManager) refreshChoiceBuffList(ctx *data.Context, roleId int64, pass int32) {
	venture := mgr.VentureBy(ctx, roleId)
	if venture == nil {
		return
	}
	ventureCfg := config.GetAdventurePassCfg(venture.Chapter, pass)
	venture.UngetBuffList = make([]int32, 3)
	// buff
	mgr.selectBuff(ctx, roleId, venture, ventureCfg.BuffDropId1, 0)
	mgr.selectBuff(ctx, roleId, venture, ventureCfg.BuffDropId2, 1)
	mgr.selectBuff(ctx, roleId, venture, ventureCfg.BuffDropId3, 2)

	bs, err := json.Marshal(venture.UngetBuffList)
	assert.Assert(err == nil, err)
	venture.UngetBuffInfo = string(bs)
}

func (mgr *VentureManager) selectBuff(ctx *data.Context, roleId int64, venture *data.Venture, dropId int32, index int32) {
	// buff
	buffSelecter := util.NewWeightSelecter()
	allBuffCfgList := config.GetBuffListCfg(dropId)
	for _, cfg := range allBuffCfgList {
		dropCfg := cfg.(*data.BuffDropCfg)
		had := false

		for _, hadCfg := range venture.BuffList {
			if hadCfg == dropCfg.BuffId {
				had = true
				break
			}
		}

		for _, hadCfg := range venture.UngetBuffList {
			if hadCfg == dropCfg.BuffId {
				had = true
				break
			}
		}

		if !had {
			buffSelecter.Add(cfg)
		}
	}

	weightItf := buffSelecter.Random()
	if weightItf != nil {
		venture.UngetBuffList[index] = weightItf.(*data.BuffDropCfg).BuffId
	}

}

func (mgr *VentureManager) Daily(ctx *data.Context, roleId int64) {
	venture := mgr.VentureBy(ctx, roleId)
	if venture == nil {
		return
	}
	chapterCfg := config.GetAdventureChapterCfg(venture.Chapter)
	if chapterCfg.IsLast && venture.Passed == chapterCfg.PassAmount {
		return
	}
	venture.Passed = 0
	venture.BuffList = venture.BuffList[:0]
	venture.UngetBuffList = venture.BuffList[:0]

	venture.UngetBuffInfo = "[]"
	venture.BuffInfo = "[]"

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ADVENTURE, venture.Chapter, venture.Passed,
		venture.BuffInfo, venture.UngetBuffInfo, venture.RoleId)
	mgr.NotifyVenture(ctx, roleId, venture)
}

func (mgr *VentureManager) NotifyVenture(ctx *data.Context, roleId int64, venture *data.Venture) {
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyVentureMsg(venture.GetForClient()))
}

func (mgr *VentureManager) GetVenture(ctx *data.Context, roleId int64) *data.Venture {
	venture := mgr.VentureBy(ctx, roleId)
	if venture == nil {
		venture = mgr.newVenture(ctx, roleId)
	}
	mgr.NotifyVenture(ctx, roleId, venture)
	return venture
}

func (mgr *VentureManager) WinPass(ctx *data.Context, roleId int64) {
	venture := mgr.VentureBy(ctx, roleId)
	assert.Assert(venture != nil, "invalid venture")
	// assert.Assert(len(venture.UngetBuffList) == 0, "buff not get")

	venture.Passed++

	ventureCfg := config.GetAdventurePassCfg(venture.Chapter, venture.Passed)
	chapterCfg := config.GetAdventureChapterCfg(venture.Chapter)
	gItemMgr.GiveAward(ctx, roleId, ventureCfg.Reward, 1, protocol.ItemActionType_Action_VenturePass)

	if venture.Passed == chapterCfg.PassAmount && !chapterCfg.IsLast {
		venture.BuffList, venture.UngetBuffList = venture.BuffList[:0], venture.UngetBuffList[:0]
		venture.BuffInfo, venture.UngetBuffInfo = "[]", "[]"
		venture.Passed = 0
		venture.Chapter++
	} else {
		mgr.refreshChoiceBuffList(ctx, roleId, venture.Passed)
	}

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ADVENTURE, venture.Chapter, venture.Passed,
		venture.BuffInfo, venture.UngetBuffInfo, venture.RoleId)

	gRoleMgr.Send(ctx, roleId, protocol.MakeWinVenturePassDoneMsg(ctx.EventMsg.GetEnvMsg(), venture.Passed, ventureCfg.Reward.GetForClient(), venture.UngetBuffList))

}

func (mgr *VentureManager) FastChapter(ctx *data.Context, roleId int64, chapter int32) {
	venture := mgr.VentureBy(ctx, roleId)
	assert.Assert(venture != nil, "invalid venture")
	chapterCfg := config.GetAdventureChapterCfg(chapter)
	assert.Assert(venture.Chapter > chapter || (chapterCfg.IsLast && venture.Chapter == chapter), "invalid chapter")
	gQuotaCdMgr.TakeQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Free_Fast_Venture)
	gItemMgr.GiveAward(ctx, roleId, chapterCfg.Reward, 1, protocol.ItemActionType_Action_VentureFast)
}

func (mgr *VentureManager) ChoiceBuff(ctx *data.Context, roleId int64, choice int32) {
	venture := mgr.VentureBy(ctx, roleId)
	assert.Assert(venture != nil, "invalid venture")
	assert.Assert(len(venture.UngetBuffList) > 0, "no buff can get")

	buffCfgId := venture.UngetBuffList[choice-1]

	venture.UngetBuffList = venture.UngetBuffList[:0]
	venture.UngetBuffInfo = "[]"

	venture.BuffList = append(venture.BuffList, buffCfgId)
	bs2, err2 := json.Marshal(venture.BuffList)
	assert.Assert(err2 == nil, err2)
	venture.BuffInfo = string(bs2)

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ADVENTURE, venture.Chapter, venture.Passed,
		venture.BuffInfo, venture.UngetBuffInfo, venture.RoleId)
	mgr.NotifyVenture(ctx, roleId, venture)

}

func (mgr *VentureManager) Refresh(ctx *data.Context, roleId int64) {
	venture := mgr.VentureBy(ctx, roleId)
	assert.Assert(venture != nil, "invalid venture")
	assert.Assert(len(venture.UngetBuffList) > 0, "no buff can get")

	mgr.refreshChoiceBuffList(ctx, roleId, venture.Passed)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ADVENTURE, venture.Chapter, venture.Passed,
		venture.BuffInfo, venture.UngetBuffInfo, venture.RoleId)

	gRoleMgr.Send(ctx, roleId, protocol.MakeVentureRefreshBuffDoneMsg(ctx.EventMsg.GetEnvMsg(), venture.UngetBuffList))
}
