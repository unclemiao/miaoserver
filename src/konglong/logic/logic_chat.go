package logic

import (
	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

const (
	CHAT_MSG_SEND_POOL_SIZE = 20
	CHAT_MSG_SAVE_POOL_SIZE = 30
)

type ChatManager struct {
	ChatList     [][]*protocol.GameMessage
	RoleChatType map[int64]protocol.MessageType
	RoleInChat   []map[int64]int64
}

func init() {
	gChatMgr = &ChatManager{
		ChatList:     make([][]*protocol.GameMessage, 3),
		RoleInChat:   make([]map[int64]int64, 3),
		RoleChatType: make(map[int64]protocol.MessageType),
	}
	for i := 0; i < 3; i++ {
		gChatMgr.RoleInChat[i] = make(map[int64]int64)
	}
}

func (mgr *ChatManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventOfflineRole)
		if eventArgs.SignOut {
			delete(mgr.RoleChatType, eventArgs.RoleId)
			for t, _ := range mgr.RoleInChat {
				delete(mgr.RoleInChat[t], eventArgs.RoleId)
			}
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_OpenChat, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SOpenChat()
		)
		mgr.Open(ctx, ctx.RoleId, rep.MsgType, rep.Time)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_Chat, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SChat()
		)
		mgr.Chat(ctx, ctx.RoleId, rep.ChatMsg)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_CloseChat, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Close(ctx, ctx.RoleId)
	})
}

func (mgr *ChatManager) Open(ctx *data.Context, roleId int64, msgType protocol.MessageType, time int64) {
	mgr.RoleChatType[roleId] = msgType
	allMsgList := mgr.ChatList[msgType]
	if len(allMsgList) == 0 {
		return
	}
	msgLen := util.MinInt(CHAT_MSG_SEND_POOL_SIZE, len(allMsgList))
	var msgList []*protocol.GameMessage

	mgr.RoleInChat[msgType][roleId] = 1
	if time > 0 {
		msgList = make([]*protocol.GameMessage, 0, msgLen)
		for i := 0; i < CHAT_MSG_SEND_POOL_SIZE; i++ {
			index := int32(len(allMsgList) - 1 - i)
			if index < 0 {
				break
			}
			getMsg := allMsgList[index]
			if getMsg.Time > time {
				msgList = append(msgList, getMsg)
			} else {
				break
			}
		}
	} else {
		msgList = make([]*protocol.GameMessage, msgLen)
		copy(msgList, allMsgList[util.MaxInt(0, len(allMsgList)-CHAT_MSG_SEND_POOL_SIZE):])
	}
	if len(msgList) > 0 {
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyChatMsg(msgType, msgList))
	}
}

func (mgr *ChatManager) Chat(ctx *data.Context, roleId int64, msg *protocol.GameMessage) {
	roleInChatLastTime := mgr.RoleInChat[protocol.MessageType_Msg_World][roleId]
	assert.Assert(ctx.NowSec-roleInChatLastTime >= 30*1000, "invalid chat")

	role := gRoleMgr.RoleBy(ctx, roleId)
	msg.RoleId = roleId
	msg.Name, msg.Time, msg.Avatar = role.Name, time_helper.UnixMill(ctx.NowTime), role.Avatar

	mgr.Broadcast(ctx, protocol.MessageType_Msg_World, msg, roleId)

	// 任务
	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_CHAT, 1)
}

func (mgr *ChatManager) Close(ctx *data.Context, roleId int64) {
	msgType := mgr.RoleChatType[roleId]
	if msgType > 0 {
		delete(mgr.RoleChatType, roleId)
		delete(mgr.RoleInChat[msgType], roleId)
	}
}

func (mgr *ChatManager) Broadcast(ctx *data.Context, msgType protocol.MessageType, msg *protocol.GameMessage, but int64) {
	if msgType == protocol.MessageType_Msg_System {
		msg.Time = ctx.NowMill
	}
	msgList := mgr.ChatList[msgType]
	if len(msgList) == 0 {
		msgList = make([]*protocol.GameMessage, 0, CHAT_MSG_SAVE_POOL_SIZE*2)
	}
	msgList = append(msgList, msg)
	if len(msgList) >= CHAT_MSG_SAVE_POOL_SIZE {
		msgList = msgList[len(msgList)-CHAT_MSG_SAVE_POOL_SIZE:]
	}

	mgr.ChatList[msgType] = msgList

	req := protocol.MakeNotifyChatMsg(msgType, []*protocol.GameMessage{msg})
	for roleId, _ := range mgr.RoleInChat[msgType] {
		if roleId == but {
			continue
		}
		gRoleMgr.SendAtOnce(ctx, roleId, req)
	}
}
