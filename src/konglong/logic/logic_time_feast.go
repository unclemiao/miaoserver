package logic

import (
	"encoding/json"
	"fmt"
	"math"
	"time"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
)

type TimeFeastManager struct {
	TimerMap map[int64]*time.Timer
}

func init() {
	gTimeFeastMgr = &TimeFeastManager{
		TimerMap: make(map[int64]*time.Timer),
	}
}

func (mgr *TimeFeastManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventOnlineRole)
		var (
			f      *data.TimeFeast
			roleId = eventArgs.RoleId
			nowSec = time.Now().Unix()
		)

		if eventArgs.IsNew {
			f = mgr.newTimeFeast(ctx, roleId)
			mgr.loopTimeFeast(ctx, roleId, false, true, nowSec)
		} else {
			f = mgr.TimeFeastBy(ctx, roleId)
			nowDay, lastDay := time_helper.NowDay(nowSec), time_helper.NowDay(f.LastSec)
			if nowDay != lastDay {
				lw := time_helper.DayWeek(lastDay)
				newWeek := (nowDay - lastDay) > (7 - lw)
				gEventMgr.Call(ctx, data.EVENT_MSGID_ROLE_NEWDAY, &data.EventRoleNewDay{
					RoleId:    roleId,
					IsOnline:  true,
					IsDaily:   false,
					IsNewWeek: newWeek,
					NowSec:    nowSec,
				})
			} else {
				mgr.loopTimeFeast(ctx, roleId, false, true, nowSec)
			}
		}

	}, 0)

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventRoleNewDay)
		eventArgs.LastSec = mgr.TimeFeastBy(ctx, eventArgs.RoleId).LastSec
		mgr.loopTimeFeast(ctx, eventArgs.RoleId, true, eventArgs.IsOnline, eventArgs.NowSec)
	})

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventOfflineRole)
		if !eventArgs.SignOut {
			mgr.loopTimeFeast(ctx, eventArgs.RoleId, false, false, time_helper.NowSec())
		}
	})
}

func (mgr *TimeFeastManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_TIME_FEAST, roleId)
}

func (mgr *TimeFeastManager) TimeFeastBy(ctx *data.Context, roleId int64) *data.TimeFeast {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {

		var dataList []*data.TimeFeast
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_TIME_FEAST, roleId)
		if len(dataList) > 0 {
			f := dataList[0]
			f.AwardInfo = &data.TimeFeastAward{
				DaySecAwarded: make(map[int64]int32),
				SecAwarded:    make(map[int64]int32),
				ViewHangAward: &data.AwardCfg{},
			}
			f = mgr.initTimeFeast(ctx, f)
			return f, nil
		}
		return nil, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	cache := rv.(*data.TimeFeast)
	return cache
}

func (mgr *TimeFeastManager) initTimeFeast(ctx *data.Context, f *data.TimeFeast) *data.TimeFeast {

	err := json.Unmarshal([]byte(f.AwardStr), f.AwardInfo)
	assert.Assert(err == nil, err)
	return f
}

func (mgr *TimeFeastManager) newTimeFeast(ctx *data.Context, roleId int64) *data.TimeFeast {
	now := time.Now()
	nowSec := time_helper.UnixSec(now)
	f := &data.TimeFeast{
		RoleId:  roleId,
		NewRole: now,
		LastSec: nowSec,
		AwardInfo: &data.TimeFeastAward{
			DaySecAwarded: make(map[int64]int32),
			SecAwarded:    make(map[int64]int32),
			ViewHangAward: &data.AwardCfg{},
		},
		AwardStr:           "{}",
		LastViewHangSec:    nowSec,
		LastHangAwardedSec: nowSec,
		Signin:             1,
		NextDropBoxTime:    180,
	}
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_TIME_FEAST, f.RoleId, f.NewRole, f.Ip, f.LastSec, f.Young,
		f.DaySec, f.WeekSec, f.LastWeekSec, f.WeekDay, f.Sec, f.Signin, f.NextDropBoxTime, f.LastViewHangSec, f.LastHangAwardedSec, f.AwardStr)
	gCache.Set(mgr.getCacheKey(roleId), f, gConst.SERVER_DATA_CACHE_TIME_OUT)
	return f
}

func (mgr *TimeFeastManager) loopTimeFeast(ctx *data.Context, roleId int64, newDay bool, online bool, nowSec int64) {
	if mgr.TimerMap[roleId] != nil {
		mgr.TimerMap[roleId].Stop()
		delete(mgr.TimerMap, roleId)
	}

	var (
		f       = mgr.TimeFeastBy(ctx, roleId)
		addSec  = util.ChoiceInt64(online, 0, util.MaxInt64(1, nowSec-f.LastSec))
		session = gRoleMgr.RoleSessionBy(ctx, roleId)
	)
	gSql.Begin().Clean(roleId)
	ctx.OldLastSec = f.LastSec
	f.DaySec, f.LastSec, f.Sec = f.DaySec+addSec, nowSec, f.Sec+addSec
	if newDay {
		f.DaySec = 0
		f.Signin++
		f.NextDropBoxTime = f.DaySec + 180
	}
	if session != nil {
		f.Young = util.ChoiceInt64(session.Young && (!online || nowSec-f.LastSec < int64(5*time.Hour)), f.Young+addSec, 0)
		f.Ip = session.Ip
		if f.DaySec >= f.NextDropBoxTime {
			count := gGamePassMgr.DropBox(ctx, roleId)
			f.NextDropBoxTime = f.DaySec + util.MinInt64(int64((math.Pow(float64(count), 1.1)+3)*60), 900)
		}
	}

	if newDay {
		gSql.MustExcel(gConst.SQL_UPDATE_TIME_FEAST_NEW_DAY, f.LastSec, f.DaySec, f.AwardStr, f.Sec, f.NextDropBoxTime, f.Signin, f.Young, f.Ip, f.RoleId)
	} else {
		gSql.MustExcel(gConst.SQL_UPDATE_TIME_FEAST_SEC, f.LastSec, f.DaySec, f.Sec, f.NextDropBoxTime, f.Young, f.Ip, f.RoleId)
	}

	if session != nil {
		if !online {
			gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_ROLE_ONLINE_TIME, int32(f.DaySec/time_helper.Minute))
		}
		gSessionMgr.Flush(session)
		mgr.TimerMap[roleId] = TimeAfter(nil, time.Duration(gConst.LOOP_TIME_FEAST_TIME)*time.Second, func(ctx *data.Context) {
			mgr.loopTimeFeast(ctx, roleId, false, false, nowSec+gConst.LOOP_TIME_FEAST_TIME*time_helper.Second)
		})
	}

}
