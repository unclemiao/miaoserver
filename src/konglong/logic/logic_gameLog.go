package logic

import (
	"time"

	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type GameLogManager struct {
}

var gGameLogMgr *GameLogManager

func init() {
	gGameLogMgr = &GameLogManager{}
}

func (mgr *GameLogManager) Init() {

}

func (mgr *GameLogManager) OnMallBuy(ctx *data.Context, role *data.Role,
	mallCfgId int32, n int32, gem int64, bgem int64, bill string, rmb int64) {
	gSql.Begin().Clean(role.RoleId).MustExcel("INSERT INTO `game_log` ("+
		"`id`, "+
		"`time`, "+
		"`account_id`,"+
		"`role_id`,"+
		"`op`,"+
		"`mall_cfg_id`,"+
		"`n`,"+
		"`gem`,"+
		"`bgem`,"+
		"`bill`,"+
		"`money`"+
		") "+
		"VALUES (?,?,?,?,?,?,?,?,?,?,?);",
		gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_GAME_LOG), time.Now(), role.AccountId, role.RoleId, "OnMallBuy",
		mallCfgId, n, gem, bgem, bill, rmb,
	)
}

func (mgr *GameLogManager) OnItemCost(ctx *data.Context, role *data.Role, item *data.Item, n int64, action protocol.ItemActionType) {
	if item.Cid == int64(protocol.CurrencyType_Gem) {
		mgr.OnGemCost(ctx, role, n, item.N, action)
	}
}

func (mgr *GameLogManager) OnGemCost(ctx *data.Context, role *data.Role, cost int64, bn int64, action protocol.ItemActionType) {

	gSql.Begin().Clean(role.RoleId).MustExcel("INSERT INTO `game_log` ("+
		"`id`, "+
		"`time`, "+
		"`account_id`,"+
		"`role_id`,"+
		"`op`,"+
		"`item_cfg_id`,"+
		"`n`,"+
		"`bn`,"+
		"`action`,"+
		"`sub_action`,"+
		"`args_int`,"+
		"`args_str`"+
		") "+
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?);",
		gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_GAME_LOG), time.Now(), role.AccountId, role.RoleId, "OnGemCost",
		int64(protocol.CurrencyType_Gem), cost, bn, action, ctx.MsgId.Int32(), ctx.ArgsInt, ctx.ArgsStr,
	)
}

func (mgr *GameLogManager) OnItemGive(ctx *data.Context, role *data.Role, item *data.Item, n int64, action protocol.ItemActionType) {
	if item.Cid == int64(protocol.CurrencyType_Gem) {
		mgr.OnGemGive(ctx, role, n, item.N, action)
	}
}

func (mgr *GameLogManager) OnGemGive(ctx *data.Context, role *data.Role, n int64, bn int64, action protocol.ItemActionType) {
	gSql.Begin().Clean(role.RoleId).MustExcel("INSERT INTO `game_log` ("+
		"`id`, "+
		"`time`, "+
		"`account_id`,"+
		"`role_id`,"+
		"`op`,"+
		"`item_cfg_id`,"+
		"`n`,"+
		"`bn`,"+
		"`action`"+
		") "+
		"VALUES (?,?,?,?,?,?,?,?,?);",
		gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_GAME_LOG), time.Now(), role.AccountId, role.RoleId, "OnGemGive",
		int64(protocol.CurrencyType_Gem), n, bn, action,
	)
}
