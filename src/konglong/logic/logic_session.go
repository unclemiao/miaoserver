package logic

import (
	"net/http"
	"sync"

	"github.com/gorilla/websocket"

	"goproject/engine/logs"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type SessionManager struct {
	SessionMap    map[*websocket.Conn]*data.Session
	SessionLock   sync.RWMutex
	SessionNumber int64

	// http
	TrackIdRoleMap map[string]int64
	RoleTrackIdMap map[int64]string
}

func init() {
	gSessionMgr = &SessionManager{
		SessionMap:     make(map[*websocket.Conn]*data.Session),
		TrackIdRoleMap: make(map[string]int64),
		RoleTrackIdMap: make(map[int64]string),
	}
}

func (mgr *SessionManager) NewSession(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	mgr.SessionNumber++
	session := &data.Session{
		Conn:      conn,
		Id:        mgr.SessionNumber,
		ProtoType: data.PROTO_TYPE_NET,
		Ip:        conn.RemoteAddr().String(),
		MsgBuff:   &data.MsgBuff{},
		WriteChan: make(chan *protocol.Envelope, gConst.SESSION_WRITE_CHAN_BUFF_SIZE),
		StopChan:  make(chan chan struct{}),
	}
	mgr.SessionMap[conn] = session
	go mgr.run(session)
	return session
}

func (mgr *SessionManager) DelSession(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	session := mgr.SessionMap[conn]
	delete(mgr.SessionMap, conn)
	if session != nil {
		mgr.Stop(session)
		mgr.SessionNumber--
	}
	return session
}

func (mgr *SessionManager) SessionBy(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.RLock()
	defer mgr.SessionLock.RUnlock()
	return mgr.SessionMap[conn]
}

func (mgr *SessionManager) run(s *data.Session) {
	if s.ProtoType == data.PROTO_TYPE_HTTP {
		return
	}
	s.IsStop = false
	defer func() {
		err := recover()
		if err != nil {
			logs.Error("session onSend err: ", err)
			go mgr.run(s)
		}
	}()
	for {
		select {
		case msg := <-s.WriteChan:
			if s == nil || s.Conn == nil {
				mgr.Stop(s)
			} else {
				mgr.writeMsg(s, msg)
			}

		case stopFinished := <-s.StopChan:
			s.IsStop = true
			if s.Conn != nil {
				err := s.Conn.Close()
				if err != nil {
					logs.Error("session conn close err: ", err)
				}
			}
			s.Conn = nil
			close(s.WriteChan)
			close(s.StopChan)
			stopFinished <- struct{}{}
			return
		}
	}
}

func (mgr *SessionManager) Stop(s *data.Session) {
	if s.ProtoType == data.PROTO_TYPE_HTTP {
		return
	}
	s.Lock.Lock()
	defer s.Lock.Unlock()
	if !s.IsStop {
		stopDone := make(chan struct{}, 1)
		s.StopChan <- stopDone
		<-stopDone
	}
}

func (mgr *SessionManager) writeMsg(s *data.Session, msg *protocol.Envelope) {
	if s.IsStop {
		return
	}
	bs, err := msg.Marshal()
	if err != nil {
		logs.Error("msg Marshal err: ", err)
		return
	}
	if msg.MsgType != protocol.EnvelopeType_S2C_Pong &&
		msg.MsgType != protocol.EnvelopeType_S2C_SigninPlayerDone &&
		msg.MsgType != protocol.EnvelopeType_S2C_GetRankingDone &&
		msg.MsgType != protocol.EnvelopeType_S2C_NotifyManual &&
		msg.MsgType != protocol.EnvelopeType_S2C_NotifyChat &&
		msg.MsgType != protocol.EnvelopeType_S2C_NotifyMailList {
		if gIsLog {
			logs.Debugf("writeMsg roleId:%d, %s, %+v", s.RoleId, msg.MsgType, msg.Payload)
		}
	}
	err = s.Conn.WriteMessage(websocket.BinaryMessage, bs)
	if err != nil {
		logs.Error("conn WriteMessage err: ", err)
		return
	}
}

func (mgr *SessionManager) Send(ctx *data.Context, s *data.Session, msg *protocol.Envelope) {
	if s == nil {
		return
	}
	if s.ProtoType == data.PROTO_TYPE_HTTP {
		return
	}
	if s.IsStop {
		return
	}
	s.Lock.Lock()
	defer s.Lock.Unlock()
	nowNode := &data.MsgNode{Msg: msg}
	if s.MsgBuff.Head == nil {
		s.MsgBuff.Head = nowNode
		s.MsgBuff.Last = nowNode
	} else {
		last := s.MsgBuff.Last
		last.NextNode = &data.MsgNode{Msg: msg}
		s.MsgBuff.Last = last.NextNode
	}
}

func (mgr *SessionManager) SendAtOnce(s *data.Session, msg *protocol.Envelope) {
	if s == nil || s.ProtoType == data.PROTO_TYPE_HTTP || s.IsStop {
		return
	}
	s.WriteChan <- msg
}

func (mgr *SessionManager) SendToHttp(s *data.Session, msg interface{}) {
	if s.ProtoType != data.PROTO_TYPE_HTTP {
		return
	}
	s.Lock.Lock()
	defer s.Lock.Unlock()

	SendToHttp(s.W, msg)
}

func (mgr *SessionManager) Flush(s *data.Session) {
	if s.ProtoType == data.PROTO_TYPE_HTTP {
		return
	}
	s.Lock.Lock()
	defer s.Lock.Unlock()
	if s.IsStop {
		return
	}
	for s.MsgBuff.Head != nil {
		nowNode := s.MsgBuff.Head
		s.WriteChan <- nowNode.Msg
		s.MsgBuff.Head = nowNode.NextNode
		nowNode.NextNode = nil
	}
}

func (mgr *SessionManager) Clean(s *data.Session) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	if s.MsgBuff == nil {
		return
	}
	s.MsgBuff.Head = nil
	s.MsgBuff.Last = nil
}

func (mgr *SessionManager) Range(f func(session *data.Session) bool) {
	mgr.SessionLock.RLock()
	defer mgr.SessionLock.RUnlock()
	for _, s := range mgr.SessionMap {
		if !f(s) {
			return
		}
	}
}

// *************************** http *******************************
func (mgr *SessionManager) NewHttpSession(w http.ResponseWriter, r *http.Request) *data.Session {

	session := &data.Session{
		Ip:        r.RemoteAddr,
		ProtoType: data.PROTO_TYPE_HTTP,
	}
	session.W, session.R, session.TrackId = w, r, r.Form.Get("TrackId")
	mgr.SessionLock.RLock()
	defer mgr.SessionLock.RUnlock()
	session.RoleId = mgr.TrackIdRoleMap[session.TrackId]
	return session
}

func (mgr *SessionManager) SessionSetRoleId(ss *data.Session, roleId int64) {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	ss.RoleId = roleId
	if ss.ProtoType != data.PROTO_TYPE_HTTP {
		return
	}
	old, find := mgr.RoleTrackIdMap[roleId]
	if find {
		delete(mgr.TrackIdRoleMap, old)
	}
	mgr.RoleTrackIdMap[roleId] = ss.TrackId
	mgr.TrackIdRoleMap[ss.TrackId] = roleId
}
