package logic

import (
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type SystemChatManager struct {
	Chats map[int32][]*data.SystemChat
}

var gSystemChatMgr *SystemChatManager

func init() {
	gSystemChatMgr = &SystemChatManager{
		Chats: make(map[int32][]*data.SystemChat),
	}
}

func (mgr *SystemChatManager) ReloadSystemChat(ctx *data.Context) {
	nowSec := time_helper.NowSec()
	gSql.MustExcel(gConst.SQL_DELETE_SYSTEM_CHAT, nowSec)
	var list []*data.SystemChat
	gSql.MustSelect(&list, gConst.SQL_SELECT_SYSTEM_CHAT)
	for _, chat := range list {
		if chat.EndTime <= nowSec {
			continue
		}
		chat.NextTime = 1
		mgr.Chats[chat.Kind] = append(mgr.Chats[chat.Kind], chat)
	}
}

func (mgr *SystemChatManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		mgr.ReloadSystemChat(ctx)
	})

	gEventMgr.When(data.EVENT_MSGID_MINUTELY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventMinutely)
		for _, chats := range mgr.Chats {
			for _, chat := range chats {
				if args.NowSec >= chat.BeginTime && args.NowSec <= chat.EndTime {
					if chat.NextTime > 0 && args.NowSec >= chat.NextTime {
						mgr.Notify(ctx, chat)
						chat.NextTime = util.ChoiceInt64(chat.SubTime > 0, args.NowSec+chat.SubTime, 0)

					}
				}
			}
		}
	})
}

func (mgr *SystemChatManager) AddNotifyChat(ctx *data.Context, sid int32, kind int32, msg string, beginTime int64, endTime int64, subTime int64) *data.SystemChat {
	chat := &data.SystemChat{
		Id:        gIdMgr.GenerateId(ctx, sid, gConst.ID_TYPE_SYSTEM_CHAT),
		Kind:      kind,
		Msg:       msg,
		BeginTime: beginTime,
		EndTime:   endTime,
		SubTime:   subTime,
		NextTime:  1,
	}
	gSql.Begin().MustExcel(gConst.SQL_INSERT_SYSTEM_CHAT, chat.Id, chat.Kind, chat.Msg, chat.BeginTime, chat.EndTime, chat.SubTime, chat.NextTime)
	mgr.Chats[kind] = append(mgr.Chats[kind], chat)
	return chat
}

func (mgr *SystemChatManager) Notify(ctx *data.Context, chat *data.SystemChat) {
	switch chat.Kind {
	case 1:
		gRoleMgr.Broadcast(protocol.MakeNotifyBannerMsg(chat.Msg))
	}
}
