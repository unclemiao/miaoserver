package logic

import (
	"goproject/src/konglong/protocol"
	"net/http"
)

type KingManager struct {
}

func init() {
	gKingMgr = &KingManager{}
}

func (mgr *KingManager) Init() {
	HandleFunc("/game-api/player/king/init", func(w http.ResponseWriter, r *http.Request) {
		SendToHttp(w, &protocol.S2C_InitKingDoneMsg{
			IsGo: false,
			Lq:   0,
			Self: &protocol.KingSelf{},
			Top3: nil,
		})
	})
}
