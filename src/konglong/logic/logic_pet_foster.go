package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

/*
	模块: 培养
*/

type PetFosterManager struct {
}

func init() {
	gPetFosterMgr = &PetFosterManager{}
}

func (mgr *PetFosterManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetPetFoster, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.NotifyPetFoster(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_BeginFoster, func(ctx *data.Context, msg *protocol.Envelope) {
		rep := msg.GetC2SBeginFoster()
		mgr.BeginPetFoster(ctx, ctx.RoleId, rep.PetId, rep.Grid)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_EndFoster, func(ctx *data.Context, msg *protocol.Envelope) {
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.GetExp(ctx, ctx.RoleId)
	})
}

func (mgr *PetFosterManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_PET_FOSTER, roleId)
}

func (mgr *PetFosterManager) AllPetFosterBy(ctx *data.Context, roleId int64) *data.AllPetFoster {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		allData := &data.AllPetFoster{
			FosterPetMap: make(map[int64]int32),
		}
		var dbList []*data.PetFoster
		gSql.MustSelect(&dbList, gConst.SQL_SELECT_FOSTER, roleId)
		for _, dbFoster := range dbList {
			allData.PetFosterList[dbFoster.Grid-1] = dbFoster
			if dbFoster.PetId > 0 {
				allData.FosterPetMap[dbFoster.PetId] = dbFoster.Grid
			}
		}
		return allData, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)
	return rv.(*data.AllPetFoster)
}

func (mgr *PetFosterManager) PetFosterBy(ctx *data.Context, roleId int64, grid int32) *data.PetFoster {
	all := mgr.AllPetFosterBy(ctx, roleId)
	return all.PetFosterList[grid-1]
}

func (mgr *PetFosterManager) NotifyPetFoster(ctx *data.Context, roleId int64) {
	allPetFoster := mgr.AllPetFosterBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetFosterMsg(allPetFoster.GetForClient()))
}

func (mgr *PetFosterManager) BeginPetFoster(ctx *data.Context, roleId int64, petId int64, grid int32) {
	allPetFoster := mgr.AllPetFosterBy(ctx, roleId)
	assert.Assert(grid > 0 && grid <= int32(len(allPetFoster.PetFosterList)))
	petFoster := allPetFoster.PetFosterList[grid-1]

	gSql.Begin().Clean(roleId)
	if petId > 0 {
		// 上阵
		assert.Assert(petId > 0, "invalid petId, petId:%d", petId)
		assert.Assert(allPetFoster.FosterPetMap[petId] == 0, "this pet has in foster, petId:%d", petId)
		gPetMgr.PetBy(ctx, roleId, petId) // 验证 petId 合法性

		if petFoster == nil {
			petFoster = &data.PetFoster{
				RoleId:    roleId,
				PetId:     petId,
				Grid:      grid,
				BeginTime: ctx.NowSec,
			}
			allPetFoster.PetFosterList[grid-1] = petFoster

			gSql.MustExcel(gConst.SQL_INSERT_FOSTER, petFoster.RoleId, petFoster.PetId, petFoster.Grid, petFoster.BeginTime)

		} else {
			assert.Assert(petFoster.PetId == 0 || ctx.NowSec-petFoster.BeginTime < 5*gConst.MINUTE, "foster had pet, grid:%d", grid)
			if petFoster.PetId != 0 {
				delete(allPetFoster.FosterPetMap, petFoster.PetId)
			}
			petFoster.PetId, petFoster.BeginTime = petId, ctx.NowSec

			gSql.MustExcel(gConst.SQL_UPDATE_FOSTER, petFoster.PetId, petFoster.BeginTime, roleId, grid)
		}

		allPetFoster.FosterPetMap[petId] = grid
	} else {
		// 下阵
		assert.Assert(petFoster != nil, "invalid petFoster, petId:%d", petId)
		assert.Assert(ctx.NowSec-petFoster.BeginTime < 5*gConst.MINUTE, "invalid time")
		oldPetId := petFoster.PetId
		petFoster.PetId, petFoster.BeginTime = 0, 0
		gSql.MustExcel(gConst.SQL_UPDATE_FOSTER, petFoster.PetId, petFoster.BeginTime, roleId, grid)
		delete(allPetFoster.FosterPetMap, oldPetId)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetFosterUpdateMsg([]*protocol.PetFoster{petFoster.GetForClient()}))
}

func (mgr *PetFosterManager) GetExp(ctx *data.Context, roleId int64) {
	allFoster := mgr.AllPetFosterBy(ctx, roleId)
	var changePetList []*protocol.Pet
	foodBookLv := gFoodBookMgr.FoodBookBy(ctx, roleId).Level
	foodCfgId := config.FoodBookCfgBy(foodBookLv)
	minuteExp := foodCfgId.FosterExp
	maxPetLv := gPetMgr.AllPetBy(ctx, roleId).MaxLv
	role := gRoleMgr.RoleBy(ctx, roleId)

	if minuteExp == 0 {
		minuteExp = 1
	}

	for _, f := range allFoster.PetFosterList {
		if f != nil && f.PetId > 0 {

			exp := minuteExp * uint64(util.MinInt64((ctx.NowSec-f.BeginTime)/gConst.MINUTE, gConst.HOUR*12/gConst.MINUTE))
			pet := gPetMgr.PetBy(ctx, roleId, f.PetId)
			gPetMgr.AddExp(ctx, pet, exp, maxPetLv, protocol.PetActionType_Pet_Action_Foster_Level, false)
			changePetList = append(changePetList, pet.GetForClient())
			f.BeginTime = ctx.NowSec
			gSql.MustExcel(gConst.SQL_UPDATE_FOSTER, f.PetId, f.BeginTime, roleId, f.Grid)
			if f.PetId == role.Horse {
				gAttrMgr.RoleAttrBy(ctx, role, true)
			}
		}
	}

	mgr.NotifyPetFoster(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeEndFosterDoneMsg(ctx.EventMsg.GetEnvMsg(), changePetList))
}
