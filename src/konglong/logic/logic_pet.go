package logic

import (
	"encoding/json"
	"fmt"
	"math"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type PetManager struct {
}

func init() {
	gPetMgr = &PetManager{}
}

func (mgr *PetManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetAllPet, mgr.HandlerGetAllPet)

	gEventMgr.When(protocol.EnvelopeType_C2S_FeedPet, mgr.HandlerFeedPet)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetStarBatch, mgr.HandlerPetStarBatch)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetMake, mgr.HandlerPetMake)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetResolve, mgr.HandlerPetResolve)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetStar, mgr.HandlerPetStar)

	gEventMgr.When(protocol.EnvelopeType_C2S_EvolutionPet, mgr.HandlerPetEvolution)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetLevelUpMax, mgr.HandlerPetLevelUpMax)

	gEventMgr.When(protocol.EnvelopeType_C2S_ViewOtherPlayerPet, mgr.HandlerViewOtherPlayerPet)

	gEventMgr.When(protocol.EnvelopeType_C2S_PetDead, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SPetDead()
		)
		update := make([]*protocol.Pet, 0, len(rep.DeadIdList))
		for _, id := range rep.DeadIdList {
			update = append(update, mgr.AddLoyal(ctx, ctx.RoleId, id, -2, false).GetForClient())
		}
		if len(update) > 0 {
			gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyPetUpdateMsg(nil, update, nil, protocol.PetActionType_Pet_Action_Unknow))
		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetAddLoyal, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SPetAddLoyal()
		)
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid sdk")
		var updateList []*protocol.Pet
		for _, pid := range rep.PetIdList {
			pet := mgr.PetBy(ctx, ctx.RoleId, pid)
			if pet.Loyal < 60 {
				updateList = append(updateList, mgr.AddLoyal(ctx, ctx.RoleId, pid, 80-pet.Loyal, false).GetForClient())
			} else {
				updateList = append(updateList, mgr.AddLoyal(ctx, ctx.RoleId, pid, 20, false).GetForClient())
			}
		}
		gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeNotifyPetUpdateMsg(nil, updateList, nil, protocol.PetActionType_Pet_Action_Add_Loyal))

	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ChangePetName, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SChangePetName()
		)
		mgr.ChangeName(ctx, ctx.RoleId, rep.GetPetId(), rep.Name)

	})

	gEventMgr.When(protocol.EnvelopeType_C2S_TryVariation, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2STryVariation()
		)
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")

		var petpb []*protocol.Pet
		for _, id := range rep.GetPetIdList() {
			petpb = append(petpb, mgr.TryVariation(ctx, ctx.RoleId, id, false).GetForClient())
		}
		if len(petpb) > 0 {
			gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeTryVariationDoneMsg(ctx.EventMsg.GetEnvMsg(), petpb))

		}
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_PetChangeStar, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SPetChangeStar()
		)
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.ChangeStar(ctx, ctx.RoleId, rep.FromPetId, rep.ToPetId)
	})

}

func (mgr *PetManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_PET, roleId)
}

func (mgr *PetManager) getViewPetCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_VIEW_PET, roleId)
}

// roleId = 查询者id 非 pet的主人id
func (mgr *PetManager) ViewPetBy(ctx *data.Context, roleId int64, petId int64) *data.Pet {
	var petList []*data.Pet
	gSql.MustSelect(&petList, gConst.SQL_SELECT_PET, petId)
	if len(petList) == 0 {
		return nil
	}
	pet := petList[0]
	if pet.SkillCfgId == 0 {
		pet.SkillCfgId = pet.CfgId
	}
	json.Unmarshal([]byte(pet.StudySkillCfgIdListStr), &pet.StudySkillCfgIdList)
	gAttrMgr.PetAttrBy(ctx, pet, false)
	return pet
}

func (mgr *PetManager) AllPetBy(ctx *data.Context, roleId int64) *data.AllPet {
	cacheKey := mgr.getCacheKey(roleId)
	var isTouch bool
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		allData := &data.AllPet{
			RoleId: roleId,
			PetMap: make(map[int64]*data.Pet),
		}
		var dataList []*data.Pet
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ALL_PET, roleId)
		isTouch = true
		for _, pet := range dataList {
			allData.PetMap[pet.Id] = pet
			allData.MaxLv = util.ChoiceInt32(pet.Level > allData.MaxLv, pet.Level, allData.MaxLv)
			if pet.SkillCfgId == 0 {
				pet.SkillCfgId = pet.CfgId
			}
			err2 := json.Unmarshal([]byte(pet.StudySkillCfgIdListStr), &pet.StudySkillCfgIdList)
			if err2 != nil {
				return nil, err2
			}
		}

		return allData, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)
	d := rv.(*data.AllPet)
	if isTouch {
		for _, pet := range d.PetMap {
			gAttrMgr.PetAttrBy(ctx, pet, false)
		}
	}

	return d
}

func (mgr *PetManager) PetBy(ctx *data.Context, roleId int64, petId int64) *data.Pet {
	allPet := mgr.AllPetBy(ctx, roleId)
	pet, find := allPet.PetMap[petId]
	assert.Assert(find, "pet not found,roleId:%d, petId:%d ", roleId, petId)
	return pet
}

func (mgr *PetManager) delPet(ctx *data.Context, roleId int64, petList []*data.Pet) {

	allPet := mgr.AllPetBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	for _, pet := range petList {
		if ok, _ := gStubMgr.InStub(ctx, roleId, pet.Id); ok {
			assert.Assert(false, "!恐龙[%d]在阵型中，禁止删除。", pet.Id)
		}
		gSql.MustExcel(gConst.SQL_DELETE_PET, pet.Id)
		delete(allPet.PetMap, pet.Id)
	}
	allPet.MaxLv = 0
	for _, p := range allPet.PetMap {
		allPet.MaxLv = util.MaxInt32(allPet.MaxLv, p.Level)
	}
	logs.Infof("len1 %d", len(allPet.PetMap))
}

func (mgr *PetManager) addPet(
	ctx *data.Context,
	roleId int64,
	petCfgId int32,
	variation bool,
	unVariation bool,
	superEvolution bool,
	action protocol.PetActionType,
) *data.Pet {
	allPet := mgr.AllPetBy(ctx, roleId)

	assert.Assert(len(allPet.PetMap) < gConst.PET_AMOUNT_MAX, "pet full")

	pet := mgr.NewPet(ctx, roleId, petCfgId, variation, unVariation, superEvolution, action)

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_PET, pet.Id, pet.RoleId, pet.CfgId, pet.Name, pet.Level,
		pet.Exp, pet.Evolution, pet.SuperEvolution, pet.Star, pet.Sex, pet.Variation, pet.TryVariation,
		pet.StubIndex, pet.HelpIndex,
		pet.AtkAdd, pet.DefAdd, pet.HpMaxAdd, pet.Loyal, pet.SkillCfgId, pet.StudySkillCfgIdListStr,
		pet.Breed,
	)

	allPet.PetMap[pet.Id] = pet
	mgr.OnPetUpdate(ctx, roleId, pet)

	return pet
}

func (mgr *PetManager) NewPet(ctx *data.Context, roleId int64, petCfgId int32, variation bool, unVariation bool, superEvolution bool, action protocol.PetActionType) *data.Pet {
	role := gRoleMgr.RoleBy(ctx, roleId)
	cfg := config.GetPetCfg(petCfgId)

	pet := &data.Pet{
		Id:                     gIdMgr.GenerateId(ctx, role.Sid, gConst.ID_TYPE_PET),
		RoleId:                 roleId,
		CfgId:                  petCfgId,
		Level:                  util.MaxInt32(1, ctx.PetLevel),
		Sex:                    util.Random32(1, 2),
		Attr:                   &data.Attr{},
		Loyal:                  70,
		SkillCfgId:             petCfgId,
		SuperEvolution:         superEvolution,
		StudySkillCfgIdListStr: "[]",
	}
	ctx.PetLevel = 0
	if cfg.Grade == 4 {
		pet.Breed = config.BreedSGradeSelect.Random().(*data.BreedWeight).Breed
	} else {
		pet.Breed = config.BreedSelect.Random().(*data.BreedWeight).Breed
	}

	if action == protocol.PetActionType_Pet_Action_Lottery {
		pet.Variation = util.RandomMatched(60)
	} else if action == protocol.PetActionType_Pet_Action_Breed {
		pet.Variation = util.RandomMatched(500)
	}

	if unVariation {
		pet.Variation = !unVariation
	}

	if variation {
		pet.Variation = variation
	}

	if ctx.IsFirstPet {
		pet.HpMaxAdd, pet.AtkAdd, pet.DefAdd = 66, 81, 43
		ctx.IsFirstPet = false
	} else if ctx.IsSecondPet {
		pet.HpMaxAdd, pet.AtkAdd, pet.DefAdd = 73, 48, 56
		ctx.IsSecondPet = false
	} else {
		pet.AtkAdd = util.CalcBonusRateUnit(cfg.AtkAdd, util.Random32(-50, 100), 1000)
		pet.DefAdd = util.CalcBonusRateUnit(cfg.DefAdd, util.Random32(-50, 100), 1000)
		pet.HpMaxAdd = cfg.HpMaxAdd + int64(float64(cfg.HpMaxAdd)*float64(util.Random64(-50, 100))/1000.0)
		if pet.Variation {
			pet.AtkAdd = util.CalcBonusRateUnit(pet.AtkAdd, util.Random32(cfg.AtkAddRate[0], cfg.AtkAddRate[1]), 1000)
			pet.DefAdd = util.CalcBonusRateUnit(pet.DefAdd, util.Random32(cfg.DefAddRate[0], cfg.DefAddRate[1]), 1000)
			pet.HpMaxAdd = pet.HpMaxAdd + int64(float64(pet.HpMaxAdd)*float64(util.Random64(cfg.HpMaxAddRate[0], cfg.HpMaxAddRate[1]))/1000.0)
		}
	}

	pet.AtkAdd = util.MaxInt32(pet.AtkAdd, 0)
	pet.DefAdd = util.MaxInt32(pet.DefAdd, 0)
	pet.HpMaxAdd = util.MaxInt64(pet.HpMaxAdd, 0)
	gAttrMgr.PetAttrBy(ctx, pet, false)
	return pet
}

func (mgr *PetManager) GivePet(ctx *data.Context, roleId int64, pet *data.Pet, action protocol.PetActionType) {

	allPet := mgr.AllPetBy(ctx, roleId)
	assert.Assert(len(allPet.PetMap) < gConst.PET_AMOUNT_MAX, "pet full")

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_PET, pet.Id, pet.RoleId, pet.CfgId, pet.Name, pet.Level,
		pet.Exp, pet.Evolution, pet.SuperEvolution, pet.Star, pet.Sex, pet.Variation, pet.TryVariation,
		pet.StubIndex, pet.HelpIndex,
		pet.AtkAdd, pet.DefAdd, pet.HpMaxAdd, pet.Loyal, pet.SkillCfgId, pet.StudySkillCfgIdListStr, pet.Breed)

	allPet.PetMap[pet.Id] = pet
	mgr.OnPetUpdate(ctx, roleId, pet)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg([]*protocol.Pet{pet.GetForClient()}, nil, nil, action))

}

func (mgr *PetManager) GivePetFromCfgId(
	ctx *data.Context,
	roleId int64,
	petCfgId int32,
	variation bool,
	unVariation bool,
	superEvolution bool,
	action protocol.PetActionType,
	isRemote bool,
) *data.Pet {
	pet := mgr.addPet(ctx, roleId, petCfgId, variation, unVariation, superEvolution, action)

	if isRemote && pet != nil {
		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg([]*protocol.Pet{pet.GetForClient()}, nil, nil, action))
	}
	return pet
}

func (mgr *PetManager) TakePet(ctx *data.Context, roleId int64, petList []*data.Pet, petIdList []int64, action protocol.PetActionType) {
	mgr.delPet(ctx, roleId, petList)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, nil, petIdList, action))
}

/*
	获取经验
	maxLv : 限制最高等级
*/
func (mgr *PetManager) AddExp(ctx *data.Context, pet *data.Pet, exp uint64, maxLv int32, why protocol.PetActionType, remote bool) {
	totalExp := pet.Exp + exp
	level := pet.Level
	levelCfg := config.PetLevelCfgByCanNil(level)
	role := gRoleMgr.RoleBy(ctx, pet.RoleId)

	for levelCfg != nil {
		if totalExp < levelCfg.Exp || role.Level+20 <= level || levelCfg.Exp == 0 {
			break
		}
		if maxLv > 0 && level >= maxLv {
			totalExp = 0
			break
		}
		level++
		totalExp -= levelCfg.Exp
		levelCfg = config.PetLevelCfgByCanNil(level)
	}
	gSql.Begin().Clean(pet.RoleId).MustExcel(gConst.SQL_UPDATE_PET, pet.Level, totalExp, pet.Evolution, pet.Star, pet.Breed, pet.Id)

	pet.Exp = totalExp

	if pet.Level != level {
		mgr.LevelUp(ctx, pet.RoleId, pet, level, false)
	}

	// send
	if remote {
		gRoleMgr.Send(ctx, pet.RoleId, protocol.MakeNotifyPetUpdateMsg(nil, []*protocol.Pet{pet.GetForClient()}, nil, why))
	}
}

func (mgr *PetManager) LevelUp(ctx *data.Context, roleId int64, pet *data.Pet, level int32, remote bool) {
	oldLevel := pet.Level
	pet.Level = util.MaxInt32(level, oldLevel)

	cfg := config.GetPetCfg(pet.CfgId)
	var evolution int32
	for _, lv := range cfg.EvolutionLvList {
		evolution = util.ChoiceInt32(pet.Level >= lv, evolution+1, evolution)
	}
	pet.Evolution = evolution

	allPet := mgr.AllPetBy(ctx, roleId)
	allPet.MaxLv = util.MaxInt32(pet.Level, allPet.MaxLv)

	gSql.Begin().Clean(pet.RoleId).MustExcel(gConst.SQL_UPDATE_PET, pet.Level, pet.Exp, pet.Evolution, pet.Star, pet.Breed, pet.Id)

	gAttrMgr.PetAttrBy(ctx, pet, false)
	mgr.OnPetUpdate(ctx, pet.RoleId, pet)
	gDailyQuestMgr.Done(ctx, pet.RoleId, gConst.QUEST_TYPE_PET_LEVEL_UP, pet.Level-oldLevel)

	role := gRoleMgr.RoleBy(ctx, roleId)
	if role.Horse == pet.Id {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}
	if remote {
		gRoleMgr.Send(ctx, pet.RoleId, protocol.MakeNotifyPetUpdateMsg(nil, []*protocol.Pet{pet.GetForClient()}, nil, protocol.PetActionType_Pet_Action_Level))
	}
}

func (mgr *PetManager) FeedPet(ctx *data.Context, roleId int64, petId int64, itemIdList []int64) *data.Pet {
	var (
		role = gRoleMgr.RoleBy(ctx, roleId)
		pet  = mgr.PetBy(ctx, roleId, petId)
	)
	gSql.Begin().Clean(roleId)
	assert.Assert(role.Level+20 > pet.Level, "invalid lv")

	var totalExp uint64
	var totalLoyal int32
	for _, itid := range itemIdList {
		item := gItemMgr.ItemBy(ctx, roleId, itid)
		itemCfg := config.GetItemCfg(item.Cid)
		totalExp += itemCfg.Exp
		var addLoyal int32

		if config.IsLikeFood(pet.CfgId, item.Cid) {
			addLoyal = config.PetLoyalRandom(1).Loyal
		} else if config.IsHateFood(pet.CfgId, item.Cid) {
			addLoyal = config.PetLoyalRandom(2).Loyal
		}
		totalLoyal += addLoyal

		gItemMgr.TakeItem(ctx, roleId, item, 0, 1, protocol.ItemActionType_Action_Cost)
	}

	mgr.AddLoyal(ctx, roleId, petId, totalLoyal, false)
	mgr.AddExp(ctx, pet, totalExp, 0, protocol.PetActionType_Pet_Action_Feed, true)
	return pet

}

func (mgr *PetManager) StarPet(ctx *data.Context, roleId int64, toPet *data.Pet, fromPetIdList []int64) {
	toPetCfg := config.GetPetCfg(toPet.CfgId)
	assert.Assert(toPet.Level >= toPetCfg.CanStarLevel, "star level not enough, level:", toPet.Level)
	gSql.Begin().Clean(roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	starCfg := config.PetStarCfgBy(toPet.CfgId, toPet.Star+1)
	delPetList := make([]*data.Pet, 0, len(fromPetIdList))
	checkArgs := &data.CheckPetStar{}

	for _, id := range fromPetIdList {
		assert.Assert(id != toPet.Id, "can not sutff self")
		delPet := mgr.PetBy(ctx, roleId, id)
		delPetCfg := config.GetPetCfg(delPet.CfgId)
		delPetList = append(delPetList, delPet)
		if delPet.CfgId == toPet.CfgId && checkArgs.SelfNum < starCfg.SelfNum {
			checkArgs.SelfNum++
			continue
		}
		if delPetCfg.Grade == 1 {
			checkArgs.CGradeNum++
		} else if delPetCfg.Grade == 2 {
			checkArgs.BGradeNum++
		} else if delPetCfg.Grade == 3 {
			checkArgs.AGradeNum++
		} else if delPetCfg.Grade == 4 {
			checkArgs.SGradeNum++
		}

	}

	assert.Assert(checkArgs.SelfNum == starCfg.SelfNum &&
		checkArgs.CGradeNum == starCfg.CGradeNum &&
		checkArgs.BGradeNum == starCfg.BGradeNum &&
		checkArgs.AGradeNum == starCfg.AGradeNum &&
		checkArgs.SGradeNum == starCfg.SGradeNum,
		"invalid from pet stuff num not enough",
	)

	gSql.MustExcel(gConst.SQL_UPDATE_PET, toPet.Level, toPet.Exp, toPet.Evolution, toPet.Star+1, toPet.Breed, toPet.Id)
	toPet.Star += 1
	gAttrMgr.PetAttrBy(ctx, toPet, false)
	mgr.OnPetUpdate(ctx, roleId, toPet)
	if toPet.Star > 4 {
		gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
			Msg: fmt.Sprintf(config.GetGameTextCfg(
				gConst.TEXT_CFG_ID_STAR_PET).Text,
				role.Name,
				role.RoleId,
				toPetCfg.Name,
				toPet.Id,
				toPet.Star,
			),
			MsgType: protocol.MessageType_Msg_System,
		}, 0)
	}

	mgr.delPet(ctx, roleId, delPetList)

	if role.Horse == toPet.Id {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}

	petList := []*protocol.Pet{toPet.GetForClient()}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, fromPetIdList, protocol.PetActionType_Pet_Action_Star))

}

func (mgr *PetManager) ResolvePet(ctx *data.Context, roleId int64, petIdList []int64, isAdSdk bool) {
	var (
		gives      = &data.AwardCfg{}
		delPetList []*data.Pet
	)

	for _, id := range petIdList {
		pet := mgr.PetBy(ctx, roleId, id)
		resolveCfg := config.PetResolveCfgBy(pet.Star)
		config.AwardCfgAdd(gives, resolveCfg.Reward, 1)
		delPetList = append(delPetList, pet)
	}

	mgr.delPet(ctx, roleId, delPetList)
	gItemMgr.GiveAward(ctx, roleId, gives, float64(util.ChoiceInt32(isAdSdk, 5, 1)), protocol.ItemActionType_Action_Recover)

}

func (mgr *PetManager) PetEvolution(ctx *data.Context, roleId int64, petIdList []int64, cancel bool) {

}

func (mgr *PetManager) AddLoyal(ctx *data.Context, roleId int64, petId int64, loyal int32, remote bool) *data.Pet {
	pet := mgr.PetBy(ctx, roleId, petId)
	role := gRoleMgr.RoleBy(ctx, roleId)

	pet.Loyal += loyal
	pet.Loyal = util.MinInt32(100, util.MaxInt32(pet.Loyal, 0))
	if pet.Loyal < 20 {
		if role.Horse == petId {
			gRoleMgr.Horse(ctx, roleId, 0)
		} else if ok, _ := gStubMgr.InStub(ctx, roleId, petId); ok {
			gStubMgr.LevelStub(ctx, roleId, petId, protocol.StubKind_StubNormal)
			gStubMgr.LevelStub(ctx, roleId, petId, protocol.StubKind_StubPetKing)
		}
	}
	mgr.OnPetUpdate(ctx, roleId, pet)

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_PET_LOYAL, pet.Loyal, pet.Id)

	if remote {

		petList := []*protocol.Pet{pet.GetForClient()}

		gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, nil, protocol.PetActionType_Pet_Action_Evolution))
	}

	return pet
}

func (mgr *PetManager) ChangeName(ctx *data.Context, roleId int64, petId int64, name string) {
	pet := mgr.PetBy(ctx, roleId, petId)
	gSql.Begin().Clean(roleId)
	pet.Name = name
	gSql.MustExcel(gConst.SQL_UPDATE_PET_NAME, pet.Name, pet.Id)
	petList := []*protocol.Pet{pet.GetForClient()}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, nil, protocol.PetActionType_Pet_Action_Change_Name))
}

func (mgr *PetManager) ChangeStar(ctx *data.Context, roleId int64, fromPetId int64, toPetId int64) {
	fpet := mgr.PetBy(ctx, roleId, fromPetId)
	tpet := mgr.PetBy(ctx, roleId, toPetId)
	assert.Assert(fpet.CfgId == tpet.CfgId, "must same pet")
	gSql.Begin().Clean(roleId)

	tpetStar := tpet.Star
	tpet.Star = fpet.Star
	fpet.Star = tpetStar

	gAttrMgr.PetAttrBy(ctx, tpet, false)
	gAttrMgr.PetAttrBy(ctx, fpet, false)
	mgr.OnPetUpdate(ctx, roleId, tpet)
	mgr.OnPetUpdate(ctx, roleId, fpet)

	gSql.MustExcel(gConst.SQL_UPDATE_PET, tpet.Level, tpet.Exp, tpet.Evolution, tpet.Star, tpet.Breed, tpet.Id)
	gSql.MustExcel(gConst.SQL_UPDATE_PET, fpet.Level, fpet.Exp, fpet.Evolution, fpet.Star, fpet.Breed, fpet.Id)
	petList := []*protocol.Pet{fpet.GetForClient(), tpet.GetForClient()}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, nil, protocol.PetActionType_Pet_Action_Change_Star))
}

func (mgr *PetManager) TryVariation(ctx *data.Context, roleId int64, petId int64, remote bool) *data.Pet {
	pet := mgr.PetBy(ctx, roleId, petId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	assert.Assert(!pet.Variation, "already variation, petId:%d", petId)
	assert.Assert(!pet.TryVariation, "already try_variation, petId:%d", petId)
	petCfg := config.GetPetCfg(pet.CfgId)

	totalTryVariationCount := gQuotaCdMgr.QuotaNumBy(ctx, 0, protocol.QuotaType_Quota_Try_Variation)
	var variation bool
	if totalTryVariationCount >= quotaRuleMgr.quotaMaxBy(ctx, roleId, protocol.QuotaType_Quota_Try_Variation) {
		variation = true
		gQuotaCdMgr.TakeQuota(ctx, 0, totalTryVariationCount, protocol.QuotaType_Quota_Try_Variation)
	} else {
		gQuotaCdMgr.GiveQuota(ctx, 0, 1, protocol.QuotaType_Quota_Try_Variation)
		variation = util.RandomMatched(100)
	}
	if variation {
		pet.Variation = true
		pet.AtkAdd = util.CalcBonusRateUnit(pet.AtkAdd, util.Random32(petCfg.AtkAddRate[0], petCfg.AtkAddRate[1]), 1000)
		pet.DefAdd = util.CalcBonusRateUnit(pet.DefAdd, util.Random32(petCfg.DefAddRate[0], petCfg.DefAddRate[1]), 1000)
		pet.HpMaxAdd = pet.HpMaxAdd + int64(float64(pet.HpMaxAdd)*float64(util.Random64(petCfg.HpMaxAddRate[0], petCfg.HpMaxAddRate[1]))/1000.0)
		gChatMgr.Broadcast(ctx, protocol.MessageType_Msg_System, &protocol.GameMessage{
			Msg: fmt.Sprintf(config.GetGameTextCfg(
				gConst.TEXT_CFG_ID_TRY_VARIATION_PET).Text,
				role.Name,
				role.RoleId,
				petCfg.Name,
				pet.Id,
			),
			MsgType: protocol.MessageType_Msg_System,
		}, 0)
	} else {
		sel := config.GetTryVariationCfgSelect(util.Random32(1, 3))
		cfg := sel.Random().(*data.TryVariationCfg)
		switch cfg.Kind {
		case 1:
			pet.HpMaxAdd += cfg.Add
		case 2:
			pet.AtkAdd += int32(cfg.Add)
		case 3:
			pet.DefAdd += int32(cfg.Add)
		}
	}
	pet.TryVariation = true
	gSql.MustExcel(gConst.SQL_UPDATE_PET_VARIATION, pet.AtkAdd, pet.DefAdd, pet.HpMaxAdd, pet.Variation, pet.TryVariation, pet.Id)
	gAttrMgr.PetAttrBy(ctx, pet, false)
	mgr.OnPetUpdate(ctx, roleId, pet)
	if role.Horse == pet.Id {
		gAttrMgr.RoleAttrBy(ctx, role, true)
	}
	if remote {
		gRoleMgr.Send(ctx, roleId, protocol.MakeTryVariationDoneMsg(ctx.EventMsg.GetEnvMsg(), []*protocol.Pet{pet.GetForClient()}))
	}
	return pet

}

func (mgr *PetManager) OnPetUpdate(ctx *data.Context, roleId int64, pet *data.Pet) {
	gManualMgr.OnPetUpdate(ctx, roleId, pet)
}

func (mgr *PetManager) ResetAllPetAttr(ctx *data.Context, roleId int64) {
	allPet := mgr.AllPetBy(ctx, roleId)
	list := make([]*protocol.Pet, 0, len(allPet.PetMap))
	for _, pet := range allPet.PetMap {
		gAttrMgr.PetAttrBy(ctx, pet, false)
		list = append(list, pet.GetForClient())
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetListMsg(list))
}

func (mgr *PetManager) NotifyPetList(ctx *data.Context, roleId int64) {
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetListMsg(mgr.GetAllPetForClient(ctx, roleId)))
}

func (mgr *PetManager) NotifyPetUpdate(ctx *data.Context, roleId int64, pet *data.Pet, action protocol.PetActionType) {
	petList := []*protocol.Pet{pet.GetForClient()}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, petList, nil, action))
}

func (mgr *PetManager) GetAllPetForClient(ctx *data.Context, roleId int64) []*protocol.Pet {
	allPet := mgr.AllPetBy(ctx, roleId)
	var petList []*protocol.Pet
	for _, pet := range allPet.PetMap {
		petList = append(petList, pet.GetForClient())
	}
	return petList
}

// **************************************** controllers ******************************************************
func (mgr *PetManager) HandlerGetAllPet(ctx *data.Context, msg *protocol.Envelope) {
	mgr.NotifyPetList(ctx, ctx.RoleId)
}

func (mgr *PetManager) HandlerFeedPet(ctx *data.Context, msg *protocol.Envelope) {
	repMsg := msg.GetC2SFeedPet()

	var (
		roleId     = ctx.RoleId
		petId      = repMsg.PetId
		itemIdList = repMsg.FoodIdList
	)

	mgr.FeedPet(ctx, roleId, petId, itemIdList)

}

func (mgr *PetManager) HandlerPetStar(ctx *data.Context, msg *protocol.Envelope) {
	repMsg := msg.GetC2SPetStar()

	var (
		roleId  = ctx.RoleId
		toPetId = repMsg.ToPetId
		toPet   = mgr.PetBy(ctx, roleId, toPetId)
	)

	mgr.StarPet(ctx, roleId, toPet, repMsg.FromPetIdList)

}

func (mgr *PetManager) HandlerPetStarBatch(ctx *data.Context, msg *protocol.Envelope) {

}

func (mgr *PetManager) HandlerPetMake(ctx *data.Context, msg *protocol.Envelope) {

	var (
		repMsg   = msg.GetC2SPetMake()
		roleId   = ctx.RoleId
		petCfgId = repMsg.PetCfgId
		petCfg   = config.GetPetCfg(petCfgId)
	)
	if repMsg.IsSelfPieces {
		gItemMgr.TakeAward(ctx, roleId, petCfg.SelfPiecesStuff, 1, protocol.ItemActionType_Action_PetMake)

	} else {
		take := &data.AwardCfg{}
		var needNum int64 // 需要的万能碎片数量
		for _, stuff := range petCfg.SelfPiecesStuff.AwardList {
			amountInfo := &data.ItemAmount{
				CfgId: stuff.CfgId,
				N:     stuff.N,
			}
			has := gItemMgr.ItemNum(ctx, roleId, stuff.CfgId)
			if has < stuff.N {
				amountInfo.N = has
				needNum += stuff.N - has
			}
			if amountInfo.N > 0 {
				take.AwardList = append(take.AwardList, amountInfo)
			}
		}
		if needNum > 0 {
			var needCfgId int64
			var has int64
			for _, piece := range petCfg.PiecesStuff.AwardList {
				needCfgId = piece.CfgId
				break
			}
			has = gItemMgr.ItemNum(ctx, roleId, needCfgId)
			assert.Assert(has >= needNum, "!万能碎片不足[%d]", needNum-has)
			take.AwardList = append(take.AwardList, &data.ItemAmount{
				CfgId: needCfgId,
				N:     needNum,
			})
		}

		gItemMgr.TakeAward(ctx, roleId, take, 1, protocol.ItemActionType_Action_PetMake)
	}

	mgr.GivePetFromCfgId(ctx, roleId, petCfgId, false, true, false, protocol.PetActionType_Pet_Action_Make, true)

}

// 回收
func (mgr *PetManager) HandlerPetResolve(ctx *data.Context, msg *protocol.Envelope) {
	var (
		repMsg    = msg.GetC2SPetResolve()
		roleId    = ctx.RoleId
		petIdList = repMsg.PetIdList
	)

	mgr.ResolvePet(ctx, roleId, petIdList, len(msg.AdSdkToken) > 0)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, nil, petIdList, protocol.PetActionType_Pet_Action_Resolve))

}

func (mgr *PetManager) HandlerPetEvolution(ctx *data.Context, msg *protocol.Envelope) {
	var (
		rep = msg.GetC2SEvolutionPet()
	)
	mgr.PetEvolution(ctx, ctx.RoleId, rep.PetIdList, rep.Cancel)

}

func (mgr *PetManager) HandlerViewOtherPlayerPet(ctx *data.Context, msg *protocol.Envelope) {
	var (
		rep = msg.GetC2SViewOtherPlayerPet()
	)
	pet := mgr.ViewPetBy(ctx, ctx.RoleId, rep.GetPetId())
	assert.Assert(pet != nil, "恐龙已不存在")
	gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeViewPetDoneMsg(pet.GetForClient()))
}

func (mgr *PetManager) HandlerPetLevelUpMax(ctx *data.Context, msg *protocol.Envelope) {
	var (
		rep = msg.GetC2SPetLevelUpMax()
	)
	assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")

	allPet := mgr.AllPetBy(ctx, ctx.RoleId)
	var maxLv int32
	for _, p := range allPet.PetMap {
		maxLv = util.MaxInt32(maxLv, p.Level)
	}
	var petPbList []*protocol.Pet
	for _, petId := range rep.PetIdList {
		pet := mgr.PetBy(ctx, ctx.RoleId, petId)

		mgr.LevelUp(ctx, pet.RoleId, pet, util.MaxInt32(1, int32(math.Ceil(float64(maxLv)*0.80))), false)
		petPbList = append(petPbList, pet.GetForClient())
	}
	gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakePetLevelUpMaxDoneMsg(ctx.EventMsg.GetEnvMsg(), petPbList))
}
