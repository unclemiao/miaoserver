package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type RoleAdSdkAwardManager struct {
	cache *cache.ShardedCache
}

var gRoleAdSdkAwardMgr *RoleAdSdkAwardManager

func init() {
	gRoleAdSdkAwardMgr = &RoleAdSdkAwardManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Duration(60)*time.Second, 2^4),
	}
}

func (mgr *RoleAdSdkAwardManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		rollMsg := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(rollMsg.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		TimeAfter(ctx, time.Second*3, func(ctx *data.Context) {
			mgr.Notify(ctx, args.RoleId)
		})
	}, 101)

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventRoleNewDay)
		mgr.daily(ctx, args.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_LookAdSdkAward, func(ctx *data.Context, msg *protocol.Envelope) {
		args := msg.GetC2SLookAdSdkAward()
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.Look(ctx, ctx.RoleId, args.GetAppId())
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardAdSdkAward, func(ctx *data.Context, msg *protocol.Envelope) {
		args := msg.GetC2SAwardAdSdkAward()
		mgr.Award(ctx, ctx.RoleId, args.GetAppId())
	})
}

func (mgr *RoleAdSdkAwardManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_AD_ADK_AWARD, roleId)
}

func (mgr *RoleAdSdkAwardManager) daily(ctx *data.Context, roleId int64) {
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_ADSDKAWARD_DAILY, roleId)
	roleAward := mgr.RoleAdSdkAwardBy(ctx, roleId)
	var list []*protocol.AdSdkAward
	for _, award := range roleAward.DataMap {
		award.State = 0
		list = append(list, &protocol.AdSdkAward{
			Id:    award.Id,
			AppId: award.AppId,
			State: award.State,
		})
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyAdSdkAwardMsg(list))
}

func (mgr *RoleAdSdkAwardManager) RoleAdSdkAwardBy(ctx *data.Context, roleId int64) *data.RoleAdSdkAwardInfo {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		var dataList []*data.AdSdkAwardInfo
		roleInfo := &data.RoleAdSdkAwardInfo{
			DataMap: make(map[int64]*data.AdSdkAwardInfo),
		}
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ADSDKAWARD, roleId)
		for _, d := range dataList {
			roleInfo.DataMap[d.Id] = d
		}
		return roleInfo, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	info := rv.(*data.RoleAdSdkAwardInfo)
	return info
}

func (mgr *RoleAdSdkAwardManager) getForClient(ctx *data.Context, roleId int64) []*protocol.AdSdkAward {
	roleAward := mgr.RoleAdSdkAwardBy(ctx, roleId)
	var list []*protocol.AdSdkAward
	for _, award := range roleAward.DataMap {
		list = append(list, &protocol.AdSdkAward{
			Id:    award.Id,
			AppId: award.AppId,
			State: award.State,
		})
	}
	return list
}

func (mgr *RoleAdSdkAwardManager) Notify(ctx *data.Context, roleId int64) {
	cfgList := gServerExtDataMgr.GetSlotsProto()
	gRoleMgr.SendAtOnce(ctx, roleId, protocol.MakeNotifyAdSdkSlotsCfgMsg(cfgList))

	awardList := mgr.getForClient(ctx, roleId)
	gRoleMgr.SendAtOnce(ctx, roleId, protocol.MakeNotifyAdSdkAwardMsg(awardList))
}

func (mgr *RoleAdSdkAwardManager) Look(ctx *data.Context, roleId int64, appId string) {
	roleAward := mgr.RoleAdSdkAwardBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	var find *data.AdSdkAwardInfo
	for _, ad := range roleAward.DataMap {
		if ad.AppId == appId {
			find = ad
			break
		}
	}
	if find == nil {
		find = &data.AdSdkAwardInfo{
			Id:    gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_AD_AWARD),
			AppId: appId,
			State: 1,
		}
		gSql.MustExcel(gConst.SQL_INSERT_ADSDKAWARD, find.Id, roleId, appId, 1)
		roleAward.DataMap[find.Id] = find
	} else {
		assert.Assert(find.State == 0, "invalid state")
		find.State = 1
		gSql.MustExcel(gConst.SQL_UPDATE_ADSDKAWARD, find.State, find.Id)
	}

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyAdSdkAwardMsg([]*protocol.AdSdkAward{{
		Id:    find.Id,
		AppId: find.AppId,
		State: find.State,
	}}))

}

func (mgr *RoleAdSdkAwardManager) Award(ctx *data.Context, roleId int64, appId string) {
	roleAward := mgr.RoleAdSdkAwardBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	var find *data.AdSdkAwardInfo
	for _, ad := range roleAward.DataMap {
		if ad.AppId == appId {
			find = ad
			break
		}
	}
	assert.Assert(find != nil, "award info not found")

	assert.Assert(find.State == 1, "invalid state")
	find.State = 2
	gSql.MustExcel(gConst.SQL_UPDATE_ADSDKAWARD, find.State, find.Id)

	gItemMgr.GiveItem(ctx, roleId, int64(protocol.CurrencyType_Diamonds), 50, protocol.ItemActionType_Action_Ad_Award)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyAdSdkAwardMsg([]*protocol.AdSdkAward{{
		Id:    find.Id,
		AppId: find.AppId,
		State: find.State,
	}}))

}
