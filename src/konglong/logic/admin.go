package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sync"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
)

type AdminManager struct {
	Lock      sync.RWMutex
	ServerCmd *data.ServerCmd
}

type ServerCmdHttpResp struct {
	Code int32
	Err  string
	Data *data.ServerCmd
}

func init() {
	gAdminMgr = &AdminManager{}
}

func (mgr *AdminManager) Init() {}

func (mgr *AdminManager) StarServer() {
	mgr.Init()
	mgr.getServerStarCnf()
}

func (mgr *AdminManager) getServerStarCnf() {
	form := make(url.Values)
	form.Set("id", fmt.Sprintf("%d", gAppCnf.Server.Id))
	form.Set("sid", fmt.Sprintf("%d", gAppCnf.Server.Sid))
	form.Set("platform", gAppCnf.Server.Platform)
	form.Set("channel", gAppCnf.Server.Channel)
	form.Set("port", fmt.Sprintf("%d", gAppCnf.Server.Port))
	form.Set("adminHost", gGameFlag.AdminHost)
	due := fmt.Sprintf("%d", time_helper.NowSec()+time_helper.Hour*4)
	form.Set("due", due)
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", gAppCnf.Server.Sid, gAppCnf.Server.Platform, gAppCnf.Server.Channel, due, "b7561aa298be84e4c3c2a7901e47c350"))
	form.Set("weiyingToken", md5)

	urlstr := gAppCnf.Admin.Host + "/server/start?" + form.Encode()
	logs.Infof("send: %s", urlstr)

	resp, err := http.Get(urlstr)
	assert.Assert(err == nil, err)
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	assert.Assert(err == nil, err)

	respData := &ServerCmdHttpResp{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		logs.Error(string(bs))
		assert.Assert(err == nil, err)
	}
	assert.Assert(respData.Err == "", respData.Err)
	logs.Infof("get star cmd %+v", respData)
	mgr.ServerCmd = respData.Data
	assert.Assert(mgr.ServerCmd.Sid > 0, "http get server err")

	logs.Debugf("httpResp %+v", mgr.ServerCmd)
}
