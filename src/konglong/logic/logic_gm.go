package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"reflect"
	"time"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type GmManager struct {
	Route map[string]func(*GmManager, url.Values, http.ResponseWriter, *http.Request) interface{}
}

func init() {
	gGmManager = &GmManager{
		Route: make(map[string]func(*GmManager, url.Values, http.ResponseWriter, *http.Request) interface{}),
	}
}

func (mgr *GmManager) Init() {
	HandleFunc("/GmFunc", func(w http.ResponseWriter, r *http.Request) {
		var (
			funcName = r.Form.Get("func")
		)
		if funcName != "Ping" &&
			funcName != "SendMail" &&
			funcName != "SendMailee" &&
			funcName != "RefreshTalent" &&
			funcName != "OnlineNum" &&
			funcName != "OpenLog" &&
			funcName != "LoadServerData" &&
			funcName != "StopServer" &&
			funcName != "ReloadExcel" &&
			funcName != "OpenLimitFeast" &&
			funcName != "AddSystemChat" &&
			funcName != "ReloadSystemChat" &&
			funcName != "Invite" &&
			gAdminMgr.ServerCmd.Platform != "test" {
			return
		}
		f, ok := mgr.Route[funcName]
		if ok {
			ret := f(mgr, r.Form, w, r)
			SendToHttp(w, ret)
		}
	})

	v := reflect.ValueOf(mgr)
	vType := v.Type()
	methodNum := vType.NumMethod()

	for i := 0; i < methodNum; i++ {
		if vType.Method(i).Name == "Init" {
			continue
		}
		fi, _ := vType.Method(i).Func.Interface().(func(*GmManager, url.Values, http.ResponseWriter, *http.Request) interface{})
		mgr.Route[vType.Method(i).Name] = fi
	}
}

func (mgr *GmManager) Ping(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {

	return "1.0.30"
}

func (mgr *GmManager) OpenLog(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	gIsLog = form.Get("isLog") == "true"
	return gIsLog
}

func (mgr *GmManager) ReloadExcel(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	excel.XlsConfigInit(filepath.Join("./", "common/excel"))
	ok := config.LoadConfig(logs.GetLogger())
	if !ok {
		return "reloadExcel err"
	}
	ok = config.CheckConfig()
	if !ok {
		return "checkConfig err"
	}
	return "ok"
}

func (mgr *GmManager) GetServerChanLen(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {

	return len(gServer.ReceiveChan)
}

func (mgr *GmManager) StopServer(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {

	os.Exit(1)
	return "ok"
}

func (mgr *GmManager) SignIn(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		accountId = form.Get("account_id")
		ctx       = &data.Context{}
	)
	args := &protocol.Envelope{
		MsgType: protocol.EnvelopeType_C2S_SigninPlayer,
		Error:   nil,
		Payload: &protocol.Envelope_C2SSigninPlayer{
			C2SSigninPlayer: &protocol.C2S_SigninPlayerMsg{
				AccountId: accountId,
				Snode:     999,
				From:      "",
				Young:     false,
				Due:       0,
				Token:     "",
			},
		},
	}
	gEventMgr.Call(ctx, protocol.EnvelopeType_C2S_SigninPlayer, &data.EventNet{Msg: args})
	return "ok"
}

func (mgr *GmManager) GiveItem(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId  = util.AtoInt64(form.Get("role_id"))
		itemCid = util.AtoInt64(form.Get("item_cfg_id"))
		num     = util.AtoInt64(form.Get("num"))
	)
	award := &data.AwardCfg{
		AwardList: []*data.ItemAmount{
			{CfgId: itemCid, N: num},
		},
		Why: 0,
	}
	gItemMgr.GiveAward(&data.Context{Sid: gServer.Node.Sid, RoleId: roleId}, roleId, award, 1.0, protocol.ItemActionType_Action_Gm)
	return "ok"
}

func (mgr *GmManager) TakeItem(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		itemId = util.AtoInt64(form.Get("item_id"))
		num    = util.AtoInt64(form.Get("num"))
		ctx    = &data.Context{RoleId: roleId}
	)
	item := gItemMgr.ItemBy(ctx, roleId, itemId)
	gItemMgr.TakeItem(ctx, roleId, item, 0, num, protocol.ItemActionType_Action_Gm)
	return "ok"
}

func (mgr *GmManager) TakeItemForCfgId(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId    = util.AtoInt64(form.Get("role_id"))
		itemCfgId = util.AtoInt64(form.Get("item_cfg_id"))
		num       = util.AtoInt64(form.Get("num"))
		ctx       = &data.Context{RoleId: roleId}
	)
	gItemMgr.TakeItem(ctx, roleId, nil, itemCfgId, num, protocol.ItemActionType_Action_Gm)
	return "ok"
}

func (mgr *GmManager) RoleAddExp(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		exp    = util.AtoInt64(form.Get("exp"))
		ctx    = &data.Context{RoleId: roleId}
	)

	gRoleMgr.AddExp(ctx, roleId, uint64(exp))

	return "ok"
}

func (mgr *GmManager) MakeItem(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	// var (
	// 	roleId  = util.AtoInt64(form.Get("role_id"))
	// 	itemCid = util.AtoInt64(form.Get("item_cfg_id"))
	// )

	return "ok"
}

func (mgr *GmManager) DelAllItem(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
	)
	ctx := &data.Context{RoleId: roleId}
	all := gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Role_Equip)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	return "ok"
}

func (mgr *GmManager) DelAllCurrency(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
	)
	ctx := &data.Context{RoleId: roleId, EventMsg: &data.EventMinutely{EventBase: data.EventBase{MsgId: data.EVENT_MSGID_INIT_SERVER}}}
	all := gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Currency)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	return "ok"
}

func (mgr *GmManager) DelAllStorage(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
	)
	ctx := &data.Context{RoleId: roleId}
	all := gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Storage_Item)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	all = gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Storage_Food)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	all = gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Storage_Tool)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	all = gItemMgr.ItemListByKit(ctx, roleId, protocol.KitType_Kit_Pieces)
	for _, it := range all {
		if it == nil {
			continue
		}
		gItemMgr.TakeItem(ctx, roleId, it, it.Cid, it.N, protocol.ItemActionType_Action_Gm)
	}

	return "ok"
}

func (mgr *GmManager) GivePet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId         = util.AtoInt64(form.Get("role_id"))
		cid            = util.AtoInt32(form.Get("pet_cfg_id"))
		star           = util.AtoInt32(form.Get("star"))
		level          = util.AtoInt32(form.Get("level"))
		variation      = form.Get("variation") == "true"
		superEvolution = form.Get("super_evolution") == "true"
		ctx            = &data.Context{Sid: gServer.Node.Sid, RoleId: roleId, TrySuperEvolution: true}
	)
	pet := gPetMgr.GivePetFromCfgId(ctx, roleId, cid, variation, false, false, protocol.PetActionType_Pet_Action_Make, true)

	pet.Star, pet.Level = star, level
	cfg := config.GetPetCfg(pet.CfgId)

	var evolution int32
	for _, lv := range cfg.EvolutionLvList {
		evolution = util.ChoiceInt32(pet.Level >= lv, evolution+1, evolution)
	}
	pet.Evolution = evolution
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_PET, pet.Level, pet.Exp, pet.Evolution, pet.Star, pet.Breed, pet.Id)
	if superEvolution {
		gPetSuperEvolutionMgr.SuperEvolution(ctx, roleId, pet.Id)
	}
	gAttrMgr.PetAttrBy(ctx, pet, true)
	gPetMgr.OnPetUpdate(ctx, roleId, pet)
	allPet := gPetMgr.AllPetBy(ctx, roleId)
	if pet.Level > allPet.MaxLv {
		allPet.MaxLv = pet.Level
	}
	return pet
}

func (mgr *GmManager) StarPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	// var (
	// 	roleId = util.AtoInt64(form.Get("role_id"))
	// 	toId   = util.AtoInt64(form.Get("to_pet_id"))
	// 	fromId = util.AtoInt64(form.Get("from_pet_id"))
	// )
	// tPet := gPetMgr.PetBy(nil, roleId, toId)
	// fPet := gPetMgr.PetBy(nil, roleId, fromId)
	// gPetMgr.StarPet(nil, roleId, tPet, fPet)
	return nil
}

func (mgr *GmManager) DelRole(formV url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(formV.Get("role_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	role := gRoleMgr.RoleBy(ctx, ctx.RoleId)
	role.Tag = role.Tag | gConst.ROLE_TAG_DEL
	gSql.Begin().Clean(ctx.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_TAG, role.Tag, role.RoleId)

	centerHost := gGameFlag.CenterHost

	plt, ch := gAdminMgr.ServerCmd.Platform, gAdminMgr.ServerCmd.Channel
	due := fmt.Sprintf("%d", ctx.NowSec+time_helper.Hour)
	form := make(url.Values)
	form.Add("accountId", role.AccountId)
	form.Add("platform", plt)
	form.Add("channel", ch)
	form.Add("due", due)

	md5 := util.Md5(fmt.Sprintf("-_-!!!%s%s%s%s%s", role.AccountId, plt, ch, due, "b7561aa298be84e4c3c2a7901e47c350"))
	form.Add("weiyingToken", md5)

	formUrl := centerHost + "/account/resetSid?" + form.Encode()
	logs.Infof("send resetSid : %s", formUrl)
	resp, err := http.Get(formUrl)
	if err != nil {
		logs.Error(err)
		return err
	}
	defer resp.Body.Close()
	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		return err
	}
	logs.Infof("send  resetSid response: %s", string(bs))
	return "ok"
}

func (mgr *GmManager) MakePet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId       = util.AtoInt64(form.Get("role_id"))
		petCfgId     = util.AtoInt32(form.Get("pet_cfg_id"))
		IsSelfPieces = form.Get("IsSelfPieces") == "true"
		petCfg       = config.GetPetCfg(petCfgId)
		ctx          = &data.Context{RoleId: roleId}
	)

	if IsSelfPieces {
		gItemMgr.TakeAward(ctx, roleId, petCfg.SelfPiecesStuff, 1, protocol.ItemActionType_Action_PetMake)

	} else {
		gItemMgr.TakeAward(ctx, roleId, petCfg.PiecesStuff, 1, protocol.ItemActionType_Action_PetMake)
	}

	pet := gPetMgr.GivePetFromCfgId(ctx, roleId, petCfgId, false, false, false, protocol.PetActionType_Pet_Action_Make, true)
	return pet
}

func (mgr *GmManager) DelAllPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
	)
	ctx := &data.Context{RoleId: roleId}
	allPet := gPetMgr.AllPetBy(ctx, roleId)
	role := gRoleMgr.RoleBy(ctx, roleId)
	var delId []int64

	for _, pet := range allPet.PetMap {
		if ok, _ := gStubMgr.InStub(ctx, roleId, pet.Id); ok {
			continue
		}
		if role.Horse > 0 && role.Horse == pet.Id {
			continue
		}
		delId = append(delId, pet.Id)
	}

	gPetMgr.ResolvePet(ctx, roleId, delId, false)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetUpdateMsg(nil, nil, delId, protocol.PetActionType_Pet_Action_Resolve))

	return "ok"
}

func (mgr *GmManager) ResolvePet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId    = util.AtoInt64(form.Get("role_id"))
		petIdList []int64
	)

	err := json.Unmarshal([]byte(form.Get("pet_id_list")), &petIdList)
	assert.Assert(err == nil, err)
	gPetMgr.ResolvePet(nil, roleId, petIdList, false)
	return "ok"
}

func (mgr *GmManager) FeedPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		petId  = util.AtoInt64(form.Get("pet_id"))
		foodId = util.AtoInt64(form.Get("item_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	pet := gPetMgr.FeedPet(ctx, roleId, petId, []int64{foodId})
	return pet
}

func (mgr *GmManager) StubPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId    = util.AtoInt64(form.Get("role_id"))
		petId     = util.AtoInt64(form.Get("pet_id"))
		stubIndex = util.AtoInt32(form.Get("stub_index"))
		ctx       = &data.Context{RoleId: roleId}
	)
	return gStubMgr.StubPet(ctx, roleId, petId, gConst.STUB_TYPE_NORMAL, stubIndex, false)
}

func (mgr *GmManager) HelpPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId    = util.AtoInt64(form.Get("role_id"))
		petId     = util.AtoInt64(form.Get("pet_id"))
		stubIndex = util.AtoInt32(form.Get("stub_index"))
		helpIndex = util.AtoInt32(form.Get("help_index"))
		ctx       = &data.Context{RoleId: roleId}
	)
	return gStubMgr.StubHelpPet(ctx, roleId, petId, gConst.STUB_TYPE_NORMAL, stubIndex, helpIndex, false)
}

func (mgr *GmManager) WinGamePass(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		isBoss = util.AtoInt32(form.Get("is_boss")) == 1
		ctx    = &data.Context{RoleId: roleId}
	)
	if isBoss {
		gGamePassMgr.WinBoss(ctx, roleId)
	} else {
		gGamePassMgr.WinMonster(ctx, roleId)
	}
	return "ok"
}

func (mgr *GmManager) GamePass(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId   = util.AtoInt64(form.Get("role_id"))
		gamePass = util.AtoInt32(form.Get("game_pass"))
		ctx      = &data.Context{RoleId: roleId}
	)
	role := gRoleMgr.RoleBy(ctx, roleId)
	oldPass := role.GamePass
	role.GamePass = gamePass
	gSql.Begin().Clean(role.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_PASS, gamePass, role.RoleId)
	gRoleMgr.SendAtOnce(&data.Context{RoleId: roleId}, roleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))

	// 竞技场上榜
	cfg := config.SystemOpenCfgBy(6 - 1)
	if cfg.UnLockType == 2 {
		if cfg.UnLockCond == 1 || (oldPass < cfg.UnLockCond-1 && role.GamePass >= cfg.UnLockCond-1) {
			gDuelMgr.SetDuelRank(&data.Context{RoleId: roleId}, role)
		}
	}
	return "ok"
}

func (mgr *GmManager) GetDuelInfo(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	duelList := gDuelMgr.NotifyDuelInfo(ctx, roleId)

	return duelList
}

func (mgr *GmManager) RefreshDuelInfo(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	duel := gDuelMgr.MyDuelBy(ctx, roleId)
	if duel == nil {
		return "duel not open"
	}
	gDuelMgr.RefreshFighList(ctx, duel)

	return duel.FightInfoRankList
}

func (mgr *GmManager) DuelWin(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		rank   = util.AtoInt32(form.Get("rank"))
		ctx    = &data.Context{RoleId: roleId}
	)
	gDuelMgr.Win(ctx, roleId, rank)

	return "ok"
}

func (mgr *GmManager) DuelLose(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		rank   = util.AtoInt32(form.Get("rank"))
		ctx    = &data.Context{RoleId: roleId}
	)
	gDuelMgr.Lose(ctx, roleId, rank)

	return "ok"
}

func (mgr *GmManager) ViewPet(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		petId  = util.AtoInt64(form.Get("pet_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	pet := gPetMgr.ViewPetBy(ctx, roleId, petId)

	return pet
}

func (mgr *GmManager) GetRankList(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		ctx    = &data.Context{RoleId: roleId}
	)
	rankingList := make([]*protocol.RankInfo, 0, gConst.RANK_LIST_AMOUNT)
	gRankMgr.RangeGamePassRank(ctx, 1, gConst.RANK_LIST_AMOUNT, func(ctx *data.Context, rank *data.Rank) bool {
		rankingList = append(rankingList, rank.GetForClient())
		return false
	})

	myRank := gRankMgr.MyRankBy(ctx, roleId)

	return protocol.MakeGetRankingDoneMsg(rankingList, myRank.GetSelfForClient())
}

func (mgr *GmManager) GetRankAward(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		ctx    = &data.Context{RoleId: roleId}
	)

	gRankMgr.AwardRank(ctx, roleId, false)

	return "ok"
}

func (mgr *GmManager) SendMail(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		name      = form.Get("name")
		title     = form.Get("title")
		body      = form.Get("body")
		awardStr  = form.Get("award")
		each      = form.Get("each")
		gift_code = form.Get("gift_code")
		now       = time.Now()
	)

	mail := &data.Mail{
		Name:     name,
		Title:    title,
		Time:     now,
		Body:     body,
		EachStr:  each,
		AwardStr: awardStr,
		GiftCode: gift_code,
		TimeOut:  now.Add(time.Hour * 24 * 7),
	}

	if form.Get("time_out") != "" {
		mail.TimeOut = time.Unix(util.AtoInt64(form.Get("time_out")), 0) // 秒
	}

	if mail.EachStr == "" {
		mail.EachStr = "[0]"
	}

	err := json.Unmarshal([]byte(mail.EachStr), &mail.Each)
	assert.Assert(err == nil, err)

	mail.Award = config.LoadAwardCfgStr(mail.AwardStr)

	gMailMgr.SendMail(&data.Context{}, mail)

	return "ok"
}

func (mgr *GmManager) ReLoadCfg(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	// 加载 excel
	excel.XlsConfigInit("./common/excel")
	// 加载游戏配置
	config.LoadConfig(logs.GetLogger())
	if !config.CheckConfig() {
		return "fail"
	}
	return "ok"
}

func (mgr *GmManager) SendMailee(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		name          = form.Get("name")
		title         = form.Get("title")
		body          = form.Get("body")
		awardStr      = form.Get("award")
		gift_code     = form.Get("gift_code")
		roleIdListStr = form.Get("role_id_list")
		roleIdList    []int64
	)

	err := json.Unmarshal([]byte(roleIdListStr), &roleIdList)
	assert.Assert(err == nil, err)
	now := time.Now()
	ctx := &data.Context{}
	timeOut := now.Add(time.Hour * 24 * 7)
	if form.Get("time_out") != "" {
		timeOut = time.Unix(util.AtoInt64(form.Get("time_out")), 0) // 秒
	}
	for _, roleId := range roleIdList {
		mailee := &data.Mailee{
			Name:      name,
			RoleId:    roleId,
			Title:     title,
			AwardStr:  awardStr,
			GiftCode:  gift_code,
			Body:      body,
			IsDel:     false,
			IsRead:    false,
			Time:      now,
			TimeOut:   timeOut,
			AwardTime: time.Time{},
		}
		mailee.Award = config.LoadAwardCfgStr(mailee.AwardStr)

		gMailMgr.SendMailee(ctx, mailee)
	}
	return "ok"
}

func (mgr *GmManager) Daily(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	now := time.Now()
	gEventMgr.Call(&data.Context{NowSec: now.Unix(), NowTime: now}, data.EVENT_MSGID_DAILY, &data.EventDaily{Week: 1, IsNewWeek: true, Now: time.Now()})
	return "ok"
}

func (mgr *GmManager) FoodBookAddLevel(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		lv     = util.AtoInt32(form.Get("level"))
		ctx    = &data.Context{RoleId: roleId}
	)

	gSql.Begin().Clean(roleId)
	book := gFoodBookMgr.FoodBookBy(ctx, roleId)
	config.FoodBookCfgBy(book.Level)
	book.Level = lv
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_FOOD_BOOK, book.Level, book.Exp, book.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyFoodBookMsg(book))

	return "ok"
}

func (mgr *GmManager) NotifyPetKing(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
	)

	return gFeastMgr.NotifyPetKingRankInfo(&data.Context{RoleId: roleId, NowSec: time_helper.NowSec()}, roleId, true)
}

func (mgr *GmManager) FastBreed(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		grid   = util.AtoInt32(form.Get("grid"))
		ctx    = &data.Context{RoleId: roleId}
	)
	breed := gPetBreedMgr.PetBreedBy(ctx, roleId, grid)
	gSql.Begin().Clean(roleId)
	assert.Assert(breed != nil && breed.BreedEndTime > 0 && breed.BreedEndTime >= ctx.NowSec, "not begining, grid: %d", grid)

	breed.BreedEndTime -= gConst.BREED_TIME_OUT

	gSql.MustExcel(gConst.SQL_UPDATE_FAST_BREED, breed.BreedFastNum, breed.BreedEndTime, roleId, grid)

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyPetBreedUpdateMsg([]*protocol.PetBreed{breed.GetForClient()}))

	return "ok"
}

func (mgr *GmManager) AddLoyal(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		petId  = util.AtoInt64(form.Get("pet_id"))
		add    = util.AtoInt32(form.Get("add"))
		ctx    = &data.Context{RoleId: roleId}
	)
	gPetMgr.AddLoyal(ctx, roleId, petId, add, true)
	return "ok"
}

func (mgr *GmManager) RefreshPetKingRank(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	gFeastMgr.RefreshPetKing(&data.Context{}, false)
	return "ok"
}

func (mgr *GmManager) RefreshTalent(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	ctx := &data.Context{
		EventMsg: &data.EventNet{
			EventBase: data.EventBase{MsgId: protocol.EnvelopeType_C2S_ResetRoleTalent},
			Msg:       &protocol.Envelope{},
		},
	}
	var all []*data.Role
	gSql.Begin().MustSelect(&all, "select * from `role` where `role_id`>0")

	for _, role := range all {
		gRoleTalentMgr.Reset(ctx, role.RoleId)
		n := gItemMgr.ItemNum(ctx, role.RoleId, int64(protocol.CurrencyType_TalentPoint))
		gItemMgr.TakeItem(ctx, role.RoleId, nil, int64(protocol.CurrencyType_TalentPoint), n, protocol.ItemActionType_Action_Gm)
		var add int64
		for i := int32(1); i <= role.Level; i++ {
			levlCfg := config.GetRoleLevelCfg(i)
			add += int64(levlCfg.TalentPoint)
		}
		if add > 0 {
			gItemMgr.GiveItem(ctx, role.RoleId, int64(protocol.CurrencyType_TalentPoint), add, protocol.ItemActionType_Action_Gm)
		}
	}
	return "ok"
}

func (mgr *GmManager) AddHangTime(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		sec    = util.AtoInt64(form.Get("sec"))
		ctx    = &data.Context{RoleId: roleId}
		tf     = gTimeFeastMgr.TimeFeastBy(ctx, roleId)
	)
	tf.LastViewHangSec = tf.LastViewHangSec - sec
	tf.LastHangAwardedSec = tf.LastHangAwardedSec - sec

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_TIME_FEAST_HANG, tf.LastViewHangSec, tf.LastHangAwardedSec, tf.AwardStr, tf.RoleId)

	return "ok"
}

func (mgr *GmManager) OnlineNum(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	num := gSessionMgr.SessionNumber

	return num
}

func (mgr *GmManager) LoadServerData(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	gServerExtDataMgr.LoadServerDataMgr()

	return "ok"
}

func (mgr *GmManager) GiveQuota(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId = util.AtoInt64(form.Get("role_id"))
		kind   = util.AtoInt32(form.Get("quota_kind"))
		add    = util.AtoInt32(form.Get("add"))
		ctx    = &data.Context{RoleId: roleId}
	)
	gQuotaCdMgr.GiveQuota(ctx, roleId, add, protocol.QuotaType(kind))
	return "ok"
}

func (mgr *GmManager) OpenLimitFeast(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		open = form.Get("is_open")
		ctx  = &data.Context{SendAtOnce: true}
	)
	gFeastMgr.LimitTimeFeastOpen = open == "true"
	gSessionMgr.Range(func(session *data.Session) bool {
		if session.RoleId <= 0 {
			return true
		}
		gFeastMgr.NotifyFeast(ctx, session.RoleId)
		return true
	})
	return "ok"
}

func (mgr *GmManager) AddSystemChat(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		kind      = util.AtoInt32(form.Get("kind"))
		beginTime = util.AtoInt64(form.Get("beginTime"))
		endTime   = util.AtoInt64(form.Get("endTime"))
		subTime   = util.AtoInt64(form.Get("subTime"))
		msg       = form.Get("msg")
		ctx       = &data.Context{Sid: gServer.Node.Sid}
	)
	gSystemChatMgr.AddNotifyChat(ctx, gServer.Node.Sid, kind, msg, beginTime, endTime, subTime)
	return "ok"
}

func (mgr *GmManager) ReloadSystemChat(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	gSystemChatMgr.ReloadSystemChat(&data.Context{Sid: gServer.Node.Sid})
	return "ok"
}

func (mgr *GmManager) Invite(form url.Values, w http.ResponseWriter, r *http.Request) interface{} {
	var (
		roleId          = util.AtoInt64(form.Get("role_id"))
		beInviterRoleId = util.AtoInt64(form.Get("inviter_roleId")) // 被邀请者 id
		beInviterAvater = form.Get("inviter_avater")                // 被邀请者 avater"
		beInviterName   = form.Get("inviter_name")                  // 被邀请者 name
		ctx             = &data.Context{Sid: gServer.Node.Sid, RoleId: roleId}
	)
	gInviteMgr.InvitesCallBack(ctx, roleId, beInviterRoleId, beInviterAvater, beInviterName)
	return "ok"
}
