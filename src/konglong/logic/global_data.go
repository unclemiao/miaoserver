package logic

import (
	"os"

	"goproject/engine/cache"
	"goproject/engine/sql"

	"goproject/src/konglong/base"
	"goproject/src/konglong/data"
)

var (
	gServer     *data.Server // 服务器信息
	gAppCnf     *data.AppCnf
	gCache      *cache.ShardedCache
	gEventMgr   *base.EventManager
	gSql        *sql.Sql
	gLogFile    *os.File
	gSessionMgr *SessionManager
	gIdMgr      *IdManager
	gAdminMgr   *AdminManager
	gIsLog      bool
)

var (
	gRoleMgr         *RoleManager
	gItemMgr         *ItemManager
	gPetMgr          *PetManager
	gPetBreedMgr     *PetBreedManager
	gMonMgr          *MonsterManger
	gMailMgr         *MailManager
	gKingMgr         *KingManager
	gGameMgr         *GameManager
	gAwardMgr        *AwardManager
	gShareMgr        *ShareManager
	gRankMgr         *RankManager
	gAttrMgr         *AttrManager
	gFoodBookMgr     *FoodBookManager
	gGamePassMgr     *GamePassManager
	gHangMgr         *HangManager
	gGmManager       *GmManager
	gStubMgr         *StubManager
	gLotteryPetMgr   *LotteryPetManager
	gLotteryItemMgr  *LotteryItemManager
	gLotteryEquipMgr *LotteryEquipManager
	gTimeFeastMgr    *TimeFeastManager
	gQuotaCdMgr      *QuotaCdManager
	gParal           *ParalManager
	gDailyQuestMgr   *DailyQuestManager
	gManualMgr       *ManualManager
	gDuelMgr         *DuelManager
	gFightMgr        *FightManager
	gPetFosterMgr    *PetFosterManager
	gVentureMgr      *VentureManager
	gFeastMgr        *FeastManager
	gChatMgr         *ChatManager
)
