package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

var gRoleTalentMgr *RoleTalentManager

type RoleTalentManager struct {
	cache *cache.ShardedCache
}

func init() {
	gRoleTalentMgr = &RoleTalentManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Minute, 2^4),
	}
}

func (mgr *RoleTalentManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventArgs := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetRoleTalent, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Notify(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_RoleTalentLevelUp, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SRoleTalentLevelUp()
		)
		mgr.LevelUp(ctx, ctx.RoleId, rep.CfgId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_ResetRoleTalent, func(ctx *data.Context, msg *protocol.Envelope) {
		assert.Assert(len(msg.AdSdkToken) > 0, "invalid adsdk")
		mgr.Reset(ctx, ctx.RoleId)
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_LEVEL, func(ctx *data.Context, msg interface{}) {
		var (
			args = msg.(*data.EventRoleLevel)
		)
		lvCfg := config.GetRoleLevelCfg(args.NewLevel)
		if lvCfg.TalentPoint > 0 {
			gItemMgr.GiveItem(ctx, args.Role.RoleId, int64(protocol.CurrencyType_TalentPoint), int64(lvCfg.TalentPoint), protocol.ItemActionType_Action_Role_Talent)
		}
	})

}

func (mgr *RoleTalentManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ROLE_TALENT, roleId)
}

func (mgr *RoleTalentManager) AllRoleTalentBy(ctx *data.Context, roleId int64) *data.RoleSkills {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := mgr.cache.GetOrStore(cacheKey, func() (interface{}, error) {
		roleSkills := &data.RoleSkills{
			SkillMap:    make(map[int64]*data.SkillRole),
			SkillCfgMap: make(map[int32]*data.SkillRole),
		}
		var dataList []*data.SkillRole
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ROLE_TALENT, roleId)
		for _, d := range dataList {
			roleSkills.SkillMap[d.Id] = d
			roleSkills.SkillCfgMap[d.CfgId] = d
		}

		return roleSkills, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	d := rv.(*data.RoleSkills)
	return d
}

func (mgr *RoleTalentManager) GetForClient(ctx *data.Context, roleTalent *data.RoleSkills) []*protocol.RoleTalent {
	var dataList []*protocol.RoleTalent
	for _, talent := range roleTalent.SkillMap {
		dataList = append(dataList, mgr.GetTalentForClient(ctx, talent))
	}
	return dataList

}

func (mgr *RoleTalentManager) GetTalentForClient(ctx *data.Context, talent *data.SkillRole) *protocol.RoleTalent {
	return &protocol.RoleTalent{
		Id:         talent.Id,
		CfgId:      talent.CfgId,
		Level:      talent.Lv,
		TotalPoint: talent.TotalPoint,
	}
}

func (mgr *RoleTalentManager) Notify(ctx *data.Context, roleId int64) {
	all := mgr.AllRoleTalentBy(ctx, roleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleTalentMsg(mgr.GetForClient(ctx, all)))
}

func (mgr *RoleTalentManager) Reset(ctx *data.Context, roleId int64) {
	allTalent := mgr.AllRoleTalentBy(ctx, roleId)
	var totalTalent int32
	for _, talent := range allTalent.SkillMap {
		totalTalent += talent.TotalPoint
	}
	if totalTalent > 0 {
		gItemMgr.GiveItem(ctx, roleId, int64(protocol.CurrencyType_TalentPoint), int64(totalTalent), protocol.ItemActionType_Action_Role_Talent)
	}
	mgr.cache.Delete(mgr.getCacheKey(roleId))
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_DELETE_ROLE_TALENT, roleId)

	gRoleMgr.Send(ctx, roleId, protocol.MakeResetRoleTalentDoneMsg(ctx.EventMsg.GetEnvMsg()))
}

func (mgr *RoleTalentManager) LevelUp(ctx *data.Context, roleId int64, cfgId int32) {
	roleTalents := mgr.AllRoleTalentBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	talent := roleTalents.SkillCfgMap[cfgId]
	if talent == nil {
		cfg := config.GetSkillRoleCfg(cfgId, 1)

		if cfg.Layer > 1 {
			var totalPoint int32
			for _, t := range roleTalents.SkillMap {
				tCfg := config.GetSkillRoleCfg(t.CfgId, t.Lv)
				if tCfg.Layer == cfg.Layer-1 {
					totalPoint += t.TotalPoint
				}
			}
			assert.Assert(totalPoint >= cfg.NeedPreSkillPoint, "preLayer point not enough, prePoint:%d", totalPoint)
		}

		talent = &data.SkillRole{
			Id:         gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_SKILL_ROLE),
			RoleId:     roleId,
			CfgId:      cfgId,
			Lv:         1,
			TotalPoint: cfg.Point,
		}
		gSql.MustExcel(gConst.SQL_INSERT_ROLE_TALENT, talent.Id, talent.RoleId, talent.CfgId, talent.Lv, talent.TotalPoint)
		gItemMgr.TakeAward(ctx, roleId, cfg.Stuff, 1, protocol.ItemActionType_Action_Role_Talent)

	} else {
		cfg := config.GetSkillRoleCfg(cfgId, talent.Lv+1)
		gItemMgr.TakeAward(ctx, roleId, cfg.Stuff, 1, protocol.ItemActionType_Action_Role_Talent)
		talent.Lv++
		talent.TotalPoint += cfg.Point

		gSql.MustExcel(gConst.SQL_UPDATE_ROLE_TALENT, talent.Lv, talent.TotalPoint, talent.Id)
	}
	roleTalents.SkillCfgMap[cfgId] = talent
	roleTalents.SkillMap[talent.Id] = talent
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleTalentMsg([]*protocol.RoleTalent{mgr.GetTalentForClient(ctx, talent)}))
}
