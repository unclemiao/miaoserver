package logic

import (
	"encoding/json"
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type DailyQuestManager struct {
}

func init() {
	gDailyQuestMgr = &DailyQuestManager{}
}

func (mgr *DailyQuestManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(args.RoleId))
	})

	gEventMgr.When(data.EVENT_MSGID_ROLE_NEWDAY, func(ctx *data.Context, msg interface{}) {
		newDay := msg.(*data.EventRoleNewDay)
		mgr.Reset(ctx, newDay.RoleId, false)
		if newDay.IsDaily {
			mgr.Notify(ctx, newDay.RoleId, nil)
		}
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		if args.IsNew {
			mgr.Reset(ctx, args.RoleId, false)
		}
		TimeAfter(ctx, time.Second*3, func(ctx2 *data.Context) {
			mgr.Done(ctx2, args.RoleId, gConst.QUEST_TYPE_SIGNIN, 1)
			mgr.Notify(ctx2, args.RoleId, nil)
		}, ctx.Session)

	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardDailyQuest, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg = msg.GetC2SAwardDailyQuest()
		)
		mgr.AwardQuest(ctx, ctx.RoleId, repMsg.CfgId, repMsg.SdkAd)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardDailyQuestAward, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			repMsg = msg.GetC2SAwardDailyQuestAward()
		)
		mgr.AwardQuestAward(ctx, ctx.RoleId, repMsg.Score, len(msg.AdSdkToken) > 0)
	})

}

func (mgr *DailyQuestManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_DAILY_QUEST, roleId)
}

func (mgr *DailyQuestManager) RoleDailyQuestBy(ctx *data.Context, roleId int64) *data.RoleDailyQuest {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		quests := &data.RoleDailyQuest{
			QuestMap: make(map[int32]*data.DailyQuest),
		}
		var dataList []*data.DailyQuest
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_ALL_DAILY_QUEST, roleId)
		for _, d := range dataList {
			quests.QuestMap[d.CfgId] = d
		}
		var awardList []*data.QuestAward
		gSql.MustSelect(&awardList, gConst.SQL_SELECT_DAILY_QUEST_AWARD, roleId)
		if len(awardList) > 0 {
			quests.QuestAward = awardList[0]
			err := json.Unmarshal([]byte(quests.QuestAward.AwardedStr), &quests.QuestAward.Awarded)
			if err != nil {
				return nil, err
			}
		}
		return quests, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	quest := rv.(*data.RoleDailyQuest)
	return quest
}

func (mgr *DailyQuestManager) QuestBy(ctx *data.Context, roleId int64, cfgId int32) *data.DailyQuest {
	info := mgr.RoleDailyQuestBy(ctx, roleId)
	return info.QuestMap[cfgId]
}

func (mgr *DailyQuestManager) Reset(ctx *data.Context, roleId int64, remote bool) {
	infos := mgr.RoleDailyQuestBy(ctx, roleId)
	r := gRoleMgr.RoleBy(ctx, roleId)
	rateCfgMap := config.RandRateDailyQuestMap()
	for _, cfg := range config.DailyQuestCfgMapBy() {
		quest := infos.QuestMap[cfg.CfgId]
		isNew := quest == nil
		if isNew {
			quest = &data.DailyQuest{
				Id:     gIdMgr.GenerateId(ctx, r.Sid, gConst.ID_TYPE_DAILY_QUEST),
				RoleId: roleId,
				CfgId:  cfg.CfgId,
			}
		}
		quest.Count, quest.Awarded, quest.IsRate = 0, false, false
		if rateCfgMap[quest.CfgId] != nil {
			quest.IsRate = true
		}

		if isNew {
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_DAILY_QUEST, quest.Id, quest.RoleId, quest.CfgId,
				quest.Count, quest.Awarded, quest.IsRate)
			infos.QuestMap[quest.CfgId] = quest
		} else {
			gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_DAILY_QUEST, quest.Count, quest.Awarded, quest.IsRate, quest.Id)
		}

	}
	if infos.QuestAward != nil {
		infos.QuestAward.Score = 0
		infos.QuestAward.Awarded = infos.QuestAward.Awarded[:0]
		infos.QuestAward.AwardedStr = "[]"
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_RESET_DAILY_QUEST_AWARD, "[]", roleId)
	} else {
		infos.QuestAward = &data.QuestAward{
			RoleId:     roleId,
			Score:      0,
			AwardedStr: "[]",
		}
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSTER_DAILY_QUEST_AWARD, roleId, 0, "[]")
	}

	if remote {
		mgr.Notify(ctx, roleId, infos)
	}
}

func (mgr *DailyQuestManager) Done(ctx *data.Context, roleId int64, cfgId int32, count int32) {
	questInfo := mgr.RoleDailyQuestBy(ctx, roleId)
	quest := questInfo.QuestMap[cfgId]
	if quest == nil {
		logs.Warn("no quest, roleId:%d, cfgId:%d", roleId, cfgId)
		return
	}
	if quest.Awarded {
		return
	}
	questCfg := config.DailyQuestCfgBy(cfgId)
	if quest.Count >= questCfg.Count {
		return
	}

	if cfgId == gConst.QUEST_TYPE_ROLE_ONLINE_TIME {
		quest.Count = util.ChoiceInt32(count >= questCfg.Count, questCfg.Count, count)
	} else {
		quest.Count = util.ChoiceInt32(quest.Count+count >= questCfg.Count, questCfg.Count, quest.Count+count)
	}

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_DAILY_QUEST, quest.Count, quest.Awarded, quest.IsRate, quest.Id)

	mgr.NotifyQuestUpdate(ctx, roleId, quest)

	if quest.Count == questCfg.Count && cfgId != gConst.QUEST_TYPE_FINISH_ALL {
		mgr.Done(ctx, roleId, gConst.QUEST_TYPE_FINISH_ALL, 1)
	}

}

func (mgr *DailyQuestManager) AwardQuest(ctx *data.Context, roleId int64, cfgId int32, sdkAd bool) {
	quest := mgr.QuestBy(ctx, roleId, cfgId)
	assert.Assert(quest != nil, "daily quest can not award, id:%d", quest.Id)
	assert.Assert(!quest.Awarded, "daily quest awarded, id:%d", quest.Awarded)
	gSql.Begin().Clean(roleId)
	cfg := config.DailyQuestCfgBy(cfgId)
	quest.Awarded = true
	gSql.MustExcel(gConst.SQL_UPDATE_DAILY_QUEST, quest.Count, quest.Awarded, quest.IsRate, quest.Id)

	gItemMgr.GiveAward(ctx, roleId, cfg.Award, float64(util.ChoiceInt32(quest.IsRate, 5, 1)), protocol.ItemActionType_Action_Task)

	mgr.NotifyQuestUpdate(ctx, roleId, quest)

	// score
	questInfo := mgr.RoleDailyQuestBy(ctx, roleId)
	questInfo.QuestAward.Score += util.ChoiceInt32(sdkAd, cfg.AddExp*5, cfg.AddExp)
	gSql.MustExcel(gConst.SQL_UPDATE_DAILY_QUEST_AWARD, questInfo.QuestAward.Score, questInfo.QuestAward.AwardedStr, roleId)

	mgr.NotifyQuestAwardUpdate(ctx, roleId, questInfo.QuestAward)
}

func (mgr *DailyQuestManager) AwardQuestAward(ctx *data.Context, roleId int64, index int32, adSdk bool) {
	questInfo := mgr.RoleDailyQuestBy(ctx, roleId)
	gSql.Begin().Clean(roleId)
	cfg := config.DailyQuestAwardCfgBy(index)
	for _, i := range questInfo.QuestAward.Awarded {
		assert.Assert(index != i, "awarded")
	}
	assert.Assert(questInfo.QuestAward.Score >= cfg.Score, "score not enough")
	questInfo.QuestAward.Awarded = append(questInfo.QuestAward.Awarded, index)
	bs, err := json.Marshal(questInfo.QuestAward.Awarded)
	assert.Assert(err == nil, err)

	questInfo.QuestAward.AwardedStr = string(bs)
	gSql.MustExcel(gConst.SQL_UPDATE_DAILY_QUEST_AWARD, questInfo.QuestAward.Score, questInfo.QuestAward.AwardedStr, roleId)

	gItemMgr.GiveAward(ctx, roleId, cfg.Award, float64(util.ChoiceInt32(adSdk, 5, 1)), protocol.ItemActionType_Action_Quest_Award)

	mgr.NotifyQuestAwardUpdate(ctx, roleId, questInfo.QuestAward)

}

func (mgr *DailyQuestManager) Notify(ctx *data.Context, roleId int64, info *data.RoleDailyQuest) {
	if info == nil {
		info = mgr.RoleDailyQuestBy(ctx, roleId)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyDailyQuestMsg(info.GetForClient(), info.QuestAward.GetForClient()))
}

func (mgr *DailyQuestManager) NotifyQuestUpdate(ctx *data.Context, roleId int64, updata *data.DailyQuest) {
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyDailyQuestUpdateMsg([]*protocol.DailyQuest{updata.GetForClient()}))
}

func (mgr *DailyQuestManager) NotifyQuestAwardUpdate(ctx *data.Context, roleId int64, updata *data.QuestAward) {
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyDailyQuestAwardUpdateMsg(updata.GetForClient()))
}
