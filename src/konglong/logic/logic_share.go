package logic

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"sync"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/time_helper"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type ShareManager struct {
	AllSubscribe  map[int64]*data.RoleSubscribe
	SubscribeTime map[int64]map[string]*time.Timer
	Token         *data.WXAccessToken
	Lock          sync.RWMutex
	WxKindId      map[string]string
}

type WxSubscribeResp struct {
	Errcode int32  `json:"errcode"`
	Errmsg  string `json:"errmsg"`
}

func init() {
	gShareMgr = &ShareManager{
		AllSubscribe:  make(map[int64]*data.RoleSubscribe),
		SubscribeTime: make(map[int64]map[string]*time.Timer),
		WxKindId:      make(map[string]string),
	}
	gShareMgr.WxKindId["1"] = "Ko-kyBe8wa9_Y6I3b23J_IMi4Q6cE4Dyvx8KhK7yANg" // 挂机收益
	gShareMgr.WxKindId["2"] = "YaZeYNFib47k3krlnc6YpouefYosK7JOCW5_Z9TpXvI" // 恐龙争霸赛
	gShareMgr.WxKindId["3"] = "9bVlx4rZH-xVB1Lkw68T8e5mYdX0jT6j-pUuzWQI334" // 竞技场
}

func (mgr *ShareManager) Init() {
	gEventMgr.When(protocol.EnvelopeType_C2S_Share, func(ctx *data.Context, msg *protocol.Envelope) {
		gDailyQuestMgr.Done(ctx, ctx.RoleId, gConst.QUEST_TYPE_SHARE, 1)
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_Collect, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.Collection(ctx, ctx.RoleId)
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_CollectAward, func(ctx *data.Context, msg *protocol.Envelope) {
		mgr.CollectionAward(ctx, ctx.RoleId)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetSubscribe, func(ctx *data.Context, msg *protocol.Envelope) {
		var pb []*protocol.Subscribe
		roleSubscribe := mgr.AllSubscribe[ctx.RoleId]
		if roleSubscribe != nil {
			for _, s := range roleSubscribe.SubscribeMap {
				pb = append(pb, &protocol.Subscribe{
					Kind:  s.Kind,
					State: s.State,
				})
			}
		}
		gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeNotifySubscribeMsg(pb))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_SetSubscribe, func(ctx *data.Context, msg *protocol.Envelope) {
		req := msg.GetC2SSetSubscribe()
		mgr.Subscribe(ctx, ctx.RoleId, req.GetOpenId(), req.GetKind(), req.GetState())
	})

	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(ctx *data.Context, msg interface{}) {
		var dataList []*data.Subscribe
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_SUBSCRIBE_ALL)
		for _, d := range dataList {
			roleSubscribe := mgr.AllSubscribe[d.RoleId]
			if roleSubscribe == nil {
				roleSubscribe = &data.RoleSubscribe{SubscribeMap: make(map[string]*data.Subscribe)}
			}
			roleSubscribe.SubscribeMap[d.Kind] = d
			mgr.AllSubscribe[d.RoleId] = roleSubscribe
		}
	})

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOfflineRole)
		if args.RoleId == 0 || !args.SignOut {
			return
		}
		nowSec := time_helper.NowSec()
		roleSubscribe := mgr.AllSubscribe[args.RoleId]
		if roleSubscribe == nil {
			return
		}
		subscribe := roleSubscribe.SubscribeMap[gConst.SUBSCRIBE_FAST_PASS_ID]
		if subscribe == nil || subscribe.State == 0 {
			return
		}
		tf := gTimeFeastMgr.TimeFeastBy(ctx, args.RoleId)
		if tf.LastHangAwardedSec+gConst.HOUR*8 <= nowSec {
			return
		}
		sub := tf.LastHangAwardedSec + gConst.HOUR*8 - nowSec
		tr := time.AfterFunc(time.Duration(sub)*time.Second, func() {
			// send to wx
			token := mgr.GetToken()
			dataStr := &data.WXSubscribeSendData{
				Thing1: &data.WXSubscribeSendDataSub{Value: "大量龙骨"},
				Thing2: &data.WXSubscribeSendDataSub{Value: "8小时挂机已完成，快回来领取收益吧！"},
				Thing5: &data.WXSubscribeSendDataSub{Value: "点击领取"},
			}
			sendData := &data.WXSubscribeSend{
				AccessToken: token,
				Touser:      subscribe.AccountName,
				TemplateId:  mgr.WxKindId[gConst.SUBSCRIBE_FAST_PASS_ID],
				Data:        dataStr,
			}
			bs1, _ := json.Marshal(sendData)
			postUrl := "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token
			resp, err := http.Post(postUrl, "application/json", strings.NewReader(string(bs1)))
			logs.Info("post: ", postUrl)
			if err != nil {
				logs.Error(err)
				return
			}
			defer resp.Body.Close()
			bs, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				logs.Error(err)
				return
			}
			wxresp := &WxSubscribeResp{}
			json.Unmarshal(bs, wxresp)
			logs.Info(string(bs))
		})
		if mgr.SubscribeTime[args.RoleId] == nil {
			mgr.SubscribeTime[args.RoleId] = make(map[string]*time.Timer)
		}
		mgr.SubscribeTime[args.RoleId][gConst.SUBSCRIBE_FAST_PASS_ID] = tr
	}, 100)

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		if mgr.SubscribeTime[args.RoleId] == nil {
			return
		}
		tr := mgr.SubscribeTime[args.RoleId][gConst.SUBSCRIBE_FAST_PASS_ID]
		if tr == nil {
			return
		}
		tr.Stop()
		delete(mgr.SubscribeTime[args.RoleId], gConst.SUBSCRIBE_FAST_PASS_ID)
	})

}

// 收藏
func (mgr *ShareManager) Collection(ctx *data.Context, roleId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	assert.Assert(role.Collect == 0, "!已收藏")
	gSql.Begin().Clean(roleId)
	role.Collect = 1
	gSql.MustExcel(gConst.SQL_UPDATE_COLLECT, role.Collect, role.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
}

// 领取收藏奖励
func (mgr *ShareManager) CollectionAward(ctx *data.Context, roleId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	assert.Assert(role.Collect == 1, "!未收藏或已领取")
	gSql.Begin().Clean(roleId)
	role.Collect = 2
	gSql.MustExcel(gConst.SQL_UPDATE_COLLECT, role.Collect, role.RoleId)
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))
	gItemMgr.GiveItem(ctx, roleId, int64(protocol.CurrencyType_Diamonds), 300, protocol.ItemActionType_Action_Collect)
}

// ======================================= 订阅 =======================================

func (mgr *ShareManager) Subscribe(ctx *data.Context, roleId int64, accountName string, kind string, state int32) {
	gSql.Begin().Clean(roleId)
	roleSubscribe := mgr.AllSubscribe[roleId]
	if roleSubscribe == nil {
		roleSubscribe = &data.RoleSubscribe{SubscribeMap: make(map[string]*data.Subscribe)}
	}

	subscribe := roleSubscribe.SubscribeMap[kind]

	if subscribe == nil {
		subscribe = &data.Subscribe{
			Id:          gIdMgr.GenerateId(ctx, ctx.Sid, gConst.ID_TYPE_SUBSCRIBE),
			RoleId:      roleId,
			AccountName: accountName,
			Kind:        kind,
			State:       state,
		}
		gSql.MustExcel(gConst.SQL_INSERT_SUBSCRIBE, subscribe.Id,
			subscribe.RoleId, subscribe.AccountName, subscribe.Kind, subscribe.State)

	} else {
		subscribe.State = state
		gSql.MustExcel(gConst.SQL_UPDATE_SUBSCRIBE, subscribe.State, subscribe.Id)
	}

	roleSubscribe.SubscribeMap[kind] = subscribe

	mgr.AllSubscribe[roleId] = roleSubscribe

	var pb []*protocol.Subscribe
	pb = append(pb, &protocol.Subscribe{
		Kind:  subscribe.Kind,
		State: subscribe.State,
	})
	gRoleMgr.Send(ctx, ctx.RoleId, protocol.MakeNotifySubscribeMsg(pb))
}

func (mgr *ShareManager) GetToken() string {
	tnow := time_helper.NowSec()
	if mgr.Token != nil && mgr.Token.ExpiresIn > tnow {
		return mgr.Token.AccessToken
	}
	form := make(url.Values)
	form.Add("grant_type", "client_credential")
	form.Add("appid", "wx7af5401255a73f75")
	form.Add("secret", "6089deddd919b4acd29b048308545861")
	form.Add("gameId", fmt.Sprintf("%d", gAdminMgr.ServerCmd.JavaLogGameId))

	var host string
	if gAdminMgr.ServerCmd.Platform == "test" {
		host = "https://hygae.qcinterfacet.com"
	} else {
		host = "https://hyga-pro.qcinterfacet.com"
	}

	getUrl := host + "/game-manage/wechat/open/getAccessToken?" + form.Encode()
	resp, err := http.Get(getUrl)

	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			// mgr.GetToken()
		})
		return ""
	}
	defer resp.Body.Close()

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			// mgr.GetToken()
		})
		return ""
	}
	respData := &protocol.HttpResponse{}
	err = json.Unmarshal(bs, respData)
	if err != nil {
		logs.Error(err)
		time.AfterFunc(time.Minute, func() {
			// mgr.GetToken()
		})
		return ""
	}
	accessToken := ""
	t, ok := respData.Data.(string)
	if ok {
		accessToken = t
	}
	if accessToken == "" {
		logs.Error(respData.ErrMsg)
		time.AfterFunc(time.Minute, func() {
			// mgr.GetToken()
		})
		return ""
	}
	if mgr.Token == nil {
		mgr.Token = &data.WXAccessToken{}
	}
	mgr.Token.AccessToken = accessToken
	mgr.Token.ExpiresIn = tnow + time_helper.Minute*30
	return mgr.Token.AccessToken
}

func (mgr *ShareManager) OnPetKingOn() {
	for _, roleSubscribe := range mgr.AllSubscribe {
		subscribe := roleSubscribe.SubscribeMap[gConst.SUBSCRIBE_PET_KING_BEGIN_ID]
		if subscribe == nil || subscribe.State == 0 {
			continue
		}
		go func(openId string) {
			// send to wx
			token := mgr.GetToken()
			dataStr := &data.WXSubscribeSendData{
				Thing1: &data.WXSubscribeSendDataSub{Value: "原始争霸赛事马上开始！点击进入"},
				Thing2: &data.WXSubscribeSendDataSub{Value: "5V5恐龙对战"},
				Thing6: &data.WXSubscribeSendDataSub{Value: "海量碎片，符文"},
			}
			sendData := &data.WXSubscribeSend{
				AccessToken: token,
				Touser:      openId,
				TemplateId:  mgr.WxKindId[gConst.SUBSCRIBE_PET_KING_BEGIN_ID],
				Data:        dataStr,
			}
			bs1, _ := json.Marshal(sendData)
			postUrl := "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token
			resp, err := http.Post(postUrl, "application/json", strings.NewReader(string(bs1)))
			logs.Info("post: ", postUrl)
			if err != nil {
				logs.Error(err)
				return
			}
			defer resp.Body.Close()
			bs, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				logs.Error(err)
				return
			}
			logs.Info(string(bs))

		}(subscribe.AccountName)
	}
}

func (mgr *ShareManager) OnDuelLost(roleId int64, OldRank int32, newRank int32) {
	roleSubscribe := mgr.AllSubscribe[roleId]
	if roleSubscribe == nil {
		return
	}
	subscribe := roleSubscribe.SubscribeMap[gConst.SUBSCRIBE_DUEL_LOSE_ID]
	if subscribe == nil || subscribe.State == 0 {
		return
	}
	go func(openId string) {
		// send to wx
		token := mgr.GetToken()
		dataStr := &data.WXSubscribeSendData{
			Number2: &data.WXSubscribeSendDataSub{Value: OldRank},
			Number3: &data.WXSubscribeSendDataSub{Value: newRank},
			Thing6:  &data.WXSubscribeSendDataSub{Value: "快回来！刚刚差一点就赢了！"},
			Thing7:  &data.WXSubscribeSendDataSub{Value: "困兽场挑战"},
		}
		sendData := &data.WXSubscribeSend{
			AccessToken: token,
			Touser:      openId,
			TemplateId:  mgr.WxKindId[gConst.SUBSCRIBE_DUEL_LOSE_ID],
			Data:        dataStr,
		}
		bs1, _ := json.Marshal(sendData)
		postUrl := "https://api.weixin.qq.com/cgi-bin/message/subscribe/send?access_token=" + token
		resp, err := http.Post(postUrl, "application/json", strings.NewReader(string(bs1)))
		logs.Info("post: ", postUrl)
		if err != nil {
			logs.Error(err)
			return
		}
		defer resp.Body.Close()
		bs, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			logs.Error(err)
			return
		}
		logs.Info(string(bs))
	}(subscribe.AccountName)
}
