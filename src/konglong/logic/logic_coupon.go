package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/cache"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type CouponManager struct {
	cache *cache.ShardedCache
}

var gCouponManager *CouponManager

func init() {
	gCouponManager = &CouponManager{
		cache: cache.NewSharded(gConst.SERVER_DATA_CACHE_TIME_OUT, time.Minute, 2^4),
	}
}

func (mgr *CouponManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, msg interface{}) {
		eventMsg := msg.(*data.EventSqlRollBack)
		mgr.cache.Delete(mgr.getCacheKey(eventMsg.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_GetCoupon, func(ctx *data.Context, msg *protocol.Envelope) {
		var (
			rep = msg.GetC2SGetCoupon()
		)
		mgr.GetCoupon(ctx, ctx.RoleId, rep.CouponCode)
	})
}

func (mgr *CouponManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_COUPON, roleId)
}

func (mgr *CouponManager) CouponBy(ctx *data.Context, roleId int64, cfgId int32) *data.Coupon {
	var dataList []*data.Coupon
	gSql.MustSelect(&dataList, gConst.SQL_SELECT_ALL_COUPON, roleId, cfgId)
	if len(dataList) > 0 {
		return dataList[0]
	}
	return nil
}

func (mgr *CouponManager) GetCoupon(ctx *data.Context, roleId int64, code string) {
	cfg := config.GetCouponCfgByCode(code)
	assert.Assert(cfg != nil, "该兑换码不存在")
	coupon := mgr.CouponBy(ctx, roleId, cfg.CfgId)
	assert.Assert(coupon == nil, "该兑换码已领取")

	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_COUPON, roleId, cfg.CfgId)
	gItemMgr.GiveAward(ctx, roleId, cfg.Reward, 1, protocol.ItemActionType_Action_Coupon)
}
