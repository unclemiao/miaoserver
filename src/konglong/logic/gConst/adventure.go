package gConst

const (
	CACHE_KEY_VENTURE = "cache_venture_%d"
)

const (
	SQL_SELECT_ADVENTURE = "SELECT * FROM `adventure` where `role_id` = ?"
	SQL_UPDATE_ADVENTURE = "UPDATE `adventure` SET `chapter`=?, `passed`=?, `buff_info`=?, `unget_buff_info`=? where `role_id` = ?"
	SQL_INSERT_ADVENTURE = "INSERT INTO `adventure` ( " +
		"`role_id`, " +
		"`chapter`, " +
		"`passed`, " +
		"`buff_info`," +
		"`unget_buff_info`" +
		") " +
		"VALUES (?,?,?,?,?);"
)
