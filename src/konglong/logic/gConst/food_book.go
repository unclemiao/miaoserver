package gConst

const (
	CACHE_KEY_FOOD_BOOK = "cache_food_book_%d"
)

const (
	SQL_SELECT_FOOD_BOOK = "SELECT * FROM `food_book` where `role_id` = ?"
	SQL_UPDATE_FOOD_BOOK = "UPDATE `food_book` SET `level`=?, `exp`=? where `role_id` = ?"
	SQL_UPDATE_HIGH_BUY  = "UPDATE `food_book` SET `high_end_time`=? where `role_id` = ?"

	SQL_INSTER_FOOD_BOOK = "INSERT INTO `food_book` (" +
		"`role_id`, " +
		"`level`," +
		"`exp`," +
		"`high_end_time`" +
		") " +
		"VALUES (?,?,?,0);"
)
