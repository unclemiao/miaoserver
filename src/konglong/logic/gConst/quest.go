package gConst

const (
	CACHE_KEY_DAILY_QUEST = "cache_daily_quest_%d"
)

const (
	SQL_SELECT_ALL_DAILY_QUEST   = "SELECT * FROM `daily_quest` where `role_id` = ?"
	SQL_UPDATE_RESET_DAILY_QUEST = "UPDATE `daily_quest` SET `count`=0, `awarded`=false, `is_rate`=? where `id` = ?"
	SQL_UPDATE_DAILY_QUEST       = "UPDATE `daily_quest` SET `count`=?, `awarded`=?, `is_rate`=? where `id` = ?"
	SQL_INSTER_DAILY_QUEST       = "INSERT INTO `daily_quest` (`id`, " +
		"`role_id`, " +
		"`cfg_id`, " +
		"`count`, " +
		"`awarded`, " +
		"`is_rate`" +
		") " +
		"VALUES (?,?,?,?,?,?);"
)

const (
	SQL_SELECT_DAILY_QUEST_AWARD       = "SELECT * FROM `daily_quest_award` where `role_id` = ?"
	SQL_UPDATE_RESET_DAILY_QUEST_AWARD = "UPDATE `daily_quest_award` SET `score`=0, `awarded_str`=? where `role_id` = ?"
	SQL_UPDATE_DAILY_QUEST_AWARD       = "UPDATE `daily_quest_award` SET `score`=?, `awarded_str`=? where `role_id` = ?"
	SQL_INSTER_DAILY_QUEST_AWARD       = "INSERT INTO `daily_quest_award` (" +
		"`role_id`, " +
		"`score`, " +
		"`awarded_str`" +
		") " +
		"VALUES (?,?,?);"
)

const (
	QUEST_TYPE_FINISH_ALL           = 1
	QUEST_TYPE_SIGNIN               = 2
	QUEST_TYPE_LOTTERY_PET          = 3
	QUEST_TYPE_MAKE_ITEM            = 4
	QUEST_TYPE_FAST_PASS            = 5
	QUEST_TYPE_GAME_PASS_FIGHT_BOSS = 6
	QUEST_TYPE_PET_LEVEL_UP         = 7
	QUEST_TYPE_ROLE_ONLINE_TIME     = 8
	QUEST_TYPE_LOOK_AD              = 9
	QUEST_TYPE_SHARE                = 10
	QUEST_TYPE_CHAT                 = 1101
)
