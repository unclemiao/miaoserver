package gConst

const (
	SUBSCRIBE_FAST_PASS_ID      = "1"
	SUBSCRIBE_PET_KING_BEGIN_ID = "2"
	SUBSCRIBE_DUEL_LOSE_ID      = "3"
)

const (
	SQL_SELECT_SUBSCRIBE_ALL = "SELECT * FROM `subscribe`;"
	SQL_SELECT_SUBSCRIBE     = "SELECT * FROM `subscribe` where `role_id`=?;"
	SQL_UPDATE_SUBSCRIBE     = "update `subscribe` set `state`=? where `id`=?"
	SQL_INSERT_SUBSCRIBE     = "INSERT INTO `subscribe` (" +
		"`id` " +
		",`role_id` " +
		",`account_name` " +
		",`kind` " +
		",`state` " +
		") " +
		"VALUES (?,?,?,?,?);"
)
