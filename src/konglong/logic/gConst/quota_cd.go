package gConst

const (
	CACHE_KEY_QUOTA            = "cache_quota_%d"
	CACHE_KEY_CD               = "cache_cd_%d"
	QUOTA_FREE_FAST_PASS_MAX   = 1
	QUOTA_AD_FAST_PASS_MAX     = 20
	QUOTA_FREE_REFRESH_DUEL    = 5
	QUOTA_LOTTERY_ITEM_ADD_MAX = 10

	QUOTA_VENTURE_FREE_FAST = 2 // 每日免费扫荡次数
	QUOTA_VENTURE_ADD_FAST  = 3 // 每日增加扫荡次数的次数

	QUOTA_PET_KING_FREE_REFRESH = 5
	QUOTA_PET_KING_FREE_FIGHT   = 5
	QUOTA_PET_KING_AD_FIGHT     = 5
)

const (
	SQL_SELECT_QUOTA                           = "SELECT * FROM `quota` where `role_id` = ?"
	SQL_UPDATE_QUOTA_LOTTERY_ITEM              = "UPDATE `quota` SET `lottery_item`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_TRY_VARIATION             = "UPDATE `quota` SET `try_variation`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_LOTTERY_ITEM_ADD          = "UPDATE `quota` SET `lottery_item_add`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_LOTTERY_ITEM_GEM_ADD      = "UPDATE `quota` SET `lottery_item_gem_add`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PARAL                     = "UPDATE `quota` SET `paral`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PARAL_AD_SDK              = "UPDATE `quota` SET `paral_ad_sdk`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PARAL_GEM                 = "UPDATE `quota` SET `paral_gem`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DUEL                      = "UPDATE `quota` SET `duel`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DUEL_AD_SDK               = "UPDATE `quota` SET `duel_ad_sdk`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DUEL_GEM                  = "UPDATE `quota` SET `duel_gem`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_GAME_PASS_EXT_AWARD       = "UPDATE `quota` SET `game_pass_ext_award`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_GAME_DROP_DIAMOND         = "UPDATE `quota` SET `game_drop_diamond`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_GAME_DROP_PET             = "UPDATE `quota` SET `game_drop_pet`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_FREE_FAST_PASS            = "UPDATE `quota` SET `free_fast_pass`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_AD_FAST_PASS              = "UPDATE `quota` SET `ad_fast_pass`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_GEM_FAST_PASS             = "UPDATE `quota` SET `gem_fast_pass`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DAILY_FREE_LOTTERY_PET    = "UPDATE `quota` SET `daily_free_lottery_pet`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_FREE_REFRESH_DUEL         = "UPDATE `quota` SET `free_refresh_duel`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_LOTTERY_EQUIP             = "UPDATE `quota` SET `lottery_equip`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_VENTURE_FREE_FAST         = "UPDATE `quota` SET `venture_free_fast`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_VENTURE_ADD_FAST          = "UPDATE `quota` SET `venture_add_fast`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_VENTURE_GEM_ADD_FAST      = "UPDATE `quota` SET `venture_gem_add_fast`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PET_KING_FREE_REFRESH     = "UPDATE `quota` SET `pet_king_free_refresh`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PET_KING_FREE_FIGHT       = "UPDATE `quota` SET `pet_king_free_fight`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_PET_KING_AD_FIGHT         = "UPDATE `quota` SET `pet_king_ad_fight`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DROP_BOX                  = "UPDATE `quota` SET `drop_box`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DAILY_AD_SDK              = "UPDATE `quota` SET `daily_ad_sdk`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK     = "UPDATE `quota` SET `daily_can_look_ad_sdk`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_DAILY_CAN_LOOK_AD_SDK_ADD = "UPDATE `quota` SET `daily_can_look_ad_sdk_add`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_SUPER_EVOLUTION_FREE      = "UPDATE `quota` SET `super_evolution_free`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_SUPER_EVOLUTION_AD_SDK    = "UPDATE `quota` SET `super_evolution_ad_sdk`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_SUPER_EVOLUTION_ITEM_BUY  = "UPDATE `quota` SET `super_evolution_item_buy`=? where `role_id` = ?"
	SQL_UPDATE_QUOTA_NEWDAY                    = "UPDATE `quota` SET " +
		"`lottery_item`=? " +
		",`lottery_item_add`=? " +
		",`lottery_item_gem_add`=? " +
		",`paral`=? " +
		",`paral_ad_sdk`=? " +
		",`paral_gem`=? " +
		",`duel`=? " +
		",`duel_ad_sdk`=?" +
		",`duel_gem`=?" +
		",`game_drop_diamond`=? " +
		",`game_drop_pet`=? " +
		",`free_fast_pass`=?" +
		",`ad_fast_pass`=? " +
		",`gem_fast_pass`=? " +
		",`daily_free_lottery_pet`=?" +
		",`venture_free_fast`=?" +
		",`venture_add_fast`=?" +
		",`venture_gem_add_fast`=?" +
		",`free_refresh_duel`=?" +
		",`pet_king_free_refresh`=?" +
		",`pet_king_free_fight`=?" +
		",`pet_king_ad_fight`=?" +
		",`drop_box`=?" +
		",`daily_ad_sdk`= ?" +
		",`daily_can_look_ad_sdk`= ?" +
		",`daily_can_look_ad_sdk_add`= ?" +
		",`super_evolution_free`= ?" +
		",`super_evolution_ad_sdk`= ?" +
		",`super_evolution_item_buy`= ?" +
		" where `role_id` = ?"

	SQL_INSTER_QUOTA = "INSERT INTO `quota` (" +
		"`role_id` " +
		",`lottery_item`" +
		",`lottery_item_add`" +
		",`lottery_item_gem_add` " +
		",`paral`" +
		",`paral_ad_sdk`" +
		",`paral_gem`" +
		",`duel`" +
		",`duel_ad_sdk`" +
		",`duel_gem`" +
		",`game_pass_ext_award`" +
		",`game_drop_diamond`" +
		",`game_drop_pet`" +
		",`free_fast_pass` " +
		",`ad_fast_pass` " +
		",`gem_fast_pass` " +
		",`daily_free_lottery_pet`" +
		",`free_refresh_duel` " +
		",`venture_free_fast`" +
		",`venture_add_fast`" +
		",`venture_gem_add_fast`" +
		",`lottery_equip`" +
		",`pet_king_free_refresh`" +
		",`pet_king_free_fight`" +
		",`pet_king_ad_fight`" +
		",`drop_box`" +
		",`try_variation`" +
		",`daily_ad_sdk`" +
		",`daily_can_look_ad_sdk`" +
		",`daily_can_look_ad_sdk_add`" +
		",`super_evolution_free`" +
		",`super_evolution_ad_sdk`" +
		",`super_evolution_item_buy`" +
		") " +
		"VALUES (?,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);"
)

const (
	SQL_SELECT_CD              = "SELECT * FROM `cd` where `role_id` = ?"
	SQL_UPDATE_CD              = "UPDATE `cd` SET `lottery_item_quota_cd`=? where `role_id` = ?"
	SQL_UPDATE_CD_LOTTERY_ITEM = "UPDATE `cd` SET `lottery_item_quota_cd`=? where `role_id` = ?"
	SQL_UPDATE_CD_AD_FAST_PASS = "UPDATE `cd` SET `ad_fast_pass`=? where `role_id` = ?"
	SQL_INSTER_CD              = "INSERT INTO `cd` (" +
		"`role_id`, " +
		"`lottery_item_quota_cd`," +
		"`ad_fast_pass`" +
		") " +
		"VALUES (?,0,0);"
)
