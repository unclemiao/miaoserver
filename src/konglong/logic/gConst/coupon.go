package gConst

const (
	CACHE_KEY_COUPON = "cache_coupon_%d"
)

const (
	SQL_SELECT_ALL_COUPON = "SELECT * FROM `coupon` WHERE `role_id` = ? and `cfg_id` = ?"
	SQL_INSERT_COUPON     = "INSERT INTO `coupon` (" +
		"`role_id`, " +
		"`cfg_id` " +
		") " +
		"VALUES (?,?);"
)
