package gConst

import "goproject/src/konglong/protocol"

const (
	CACHE_KEY_STUB = "cache_stub_%d"

	STUB_NUM_MAX int32 = 4
	HELP_NUM_MAX int32 = 2
)

const (
	STUB_TYPE_NORMAL   = protocol.StubKind_StubNormal
	STUB_TYPE_PET_KING = protocol.StubKind_StubPetKing
)

const (
	SQL_SELECT_STUB = "select * from `stub` where `role_id`=?"
	SQL_UPDATE_STUB = "update `stub` set `pet_id`=?, `help_id_1`=?, `help_id_2`=?  where `id`=?"
	SQL_INSERT_STUB = "INSERT INTO `stub` (" +
		"`id` " +
		",`role_id` " +
		",`kind` " +
		",`stub_index` " +
		",`pet_id` " +
		",`help_id_1`" +
		",`help_id_2`" +
		") " +
		"VALUES (?,?,?,?,?,?,?);"
)
