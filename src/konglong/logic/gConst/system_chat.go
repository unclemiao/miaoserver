package gConst

const (
	SQL_DELETE_SYSTEM_CHAT = "DELETE FROM `system_chat` where `end_time` <= ?;"
	SQL_SELECT_SYSTEM_CHAT = "select * from `system_chat`"
	SQL_INSERT_SYSTEM_CHAT = "INSERT INTO `system_chat` (" +
		"`id` " +
		",`kind` " +
		",`msg` " +
		",`begin_time` " +
		",`end_time`" +
		",`sub_time`" +
		",`next_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?);"
)
