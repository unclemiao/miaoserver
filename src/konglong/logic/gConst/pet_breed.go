package gConst

const (
	CACHE_KEY_PET_BREED = "cache_pet_breed_%d"
)

const (
	BREED_TIME_OUT = HOUR * 20 // 20 hour
)

const (
	SQL_SELECT_BREED = "SELECT * FROM `pet_breed` where `role_id` = ?"
	SQL_UPDATE_BREED = "UPDATE `pet_breed` SET" +
		"`pet1` = ?" +
		", `pet2` = ?" +
		", `breed_end_time` = ?" +
		", `breed_fast_num` = ?" +
		" where `role_id` = ? and `grid` = ?"

	SQL_UPDATE_FAST_BREED = "UPDATE `pet_breed` SET `breed_fast_num` = ?, `breed_end_time`=? where `role_id` = ? and `grid` = ?"

	SQL_UPDATE_BREED_DAILY = "UPDATE `pet_breed` SET `breed_fast_num` = 0 where `role_id` = ?"

	SQL_INSERT_BREED = "INSERT INTO `pet_breed` (" +
		"`role_id` " +
		",`grid` " +
		",`pet1` " +
		",`pet2`" +
		", `breed_end_time`" +
		", `breed_fast_num`" +
		") " +
		"VALUES (?,?,?,?,?,?);"
)
