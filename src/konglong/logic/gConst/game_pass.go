package gConst

const (
	CACHE_KEY_NEWBIE_BOSS           = "cache_newbie_boss_%d"
	GAME_PASS_EXT_AWARD_STEP_MIN    = 3
	GAME_PASS_EXT_AWARD_STEP_MAX    = 5
	BOSS_PASS_FREE_EXT_AWARD_CFG_ID = 2
)

const (
	SQL_SELECT_NEWBIE_BOSS = "SELECT * FROM `newbie_boss` where `role_id` = ?"
	SQL_UPDATE_NEWBIE_BOSS = "UPDATE `newbie_boss` SET `awarded`=?, `ext_awarded`=?, `passed`=? where `id`=?"
	SQL_INSERT_NEWBIE_BOSS = "INSERT INTO `newbie_boss` (`id`, " +
		"`role_id`, " +
		"`cfg_index`," +
		"`awarded`," +
		"`ext_awarded`," +
		"`passed`" +
		") " +
		"VALUES (?,?,?,?,?,?);"
)
