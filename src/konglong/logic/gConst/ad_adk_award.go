package gConst

const (
	CACHE_KEY_AD_ADK_AWARD = "cache_ad_adk_award_%d"
)

const (
	SQL_SELECT_ADSDKAWARD       = "SELECT * FROM `ad_sdk_award` where `role_id` = ?"
	SQL_UPDATE_ADSDKAWARD       = "UPDATE `ad_sdk_award` SET `state`=? where `id` = ?"
	SQL_UPDATE_ADSDKAWARD_DAILY = "UPDATE `ad_sdk_award` SET `state`=0 where role_id=?;"
	SQL_INSERT_ADSDKAWARD       = "INSERT INTO `ad_sdk_award` ( " +
		"`id`, " +
		"`role_id`, " +
		"`appid`, " +
		"`state`" +
		") " +
		"VALUES (?,?,?,?);"
)
