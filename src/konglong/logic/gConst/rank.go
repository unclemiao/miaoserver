package gConst

const (
	CACHE_KEY_MY_RANK = "cache_my_rank_%d"

	RANK_AMOUNT = 5000

	RANK_LIST_AMOUNT = 100

	RANK_SORT_SUB_TIME_MINUTIL = 10
)

const (
	SQL_SELECT_MY_RANK                       = "SELECT * FROM `rank` WHERE `role_id` = ?"
	SQL_SELECT_RANK_GAME_PASS                = "SELECT `role_id`, `name`, `avatar`, `game_pass`, `game_pass_time` FROM `rank` order by `game_pass` desc, `game_pass_time` limit 5000"
	SQL_UPDATE_RANK_GAME_PASS                = "UPDATE `rank` SET `game_pass` = ?, `game_pass_time` = ? WHERE `role_id` = ? "
	SQL_UPDATE_RANK_GAME_PASS_AWARD          = "UPDATE `rank` SET `awarded` = ? WHERE `role_id` = ? "
	SQL_UPDATE_RANK_GAME_PASS_YESTERDAY_RANK = "UPDATE `rank` SET `yesterday_rank` = ?, `yesterday_before_rank` = ?, `awarded` = ? WHERE `role_id` = ? "
	SQL_INSERT_RANK_GAME_PASS                = "INSERT INTO `rank` (" +
		"`role_id`, " +
		"`name`, " +
		"`avatar`, " +
		"`game_pass`, " +
		"`game_pass_time`, " +
		"`yesterday_rank`, " +
		"`yesterday_before_rank`, " +
		"`awarded`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?);"
)
