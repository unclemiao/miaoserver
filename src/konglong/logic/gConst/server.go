package gConst

import "time"

const (
	SERVER_RECEIVE_SIZE        = 65535
	SERVER_DATA_CACHE_TIME_OUT = time.Duration(10) * time.Minute // 缓存数据超时时间 毫秒单位

)
