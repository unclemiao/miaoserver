package gConst

const (
	CACHE_KEY_MANUAL = "cache_manual_%d"
)

const (
	SQL_SELECT_MANUAL = "SELECT * FROM `manual` where `role_id`=?"
	SQL_UPDATE_MANUAL = "UPDATE `manual` SET " +
		"`evolution_lv`=?, " +
		"`star_max`=?," +
		"`has_normal`=?, " +
		"`has_variation`=?," +
		"`variation_evolution_lv`=?," +
		"`variation_star_max`=?," +
		"`has_super_evolution`=?," +
		"`has_variation_super_evolution`=?" +
		" where `id`=?"

	SQL_UPDATE_MANUAL_AWARD = "UPDATE `manual` SET " +
		"`awarded`=?" +
		" where `id`=?"

	SQL_INSERT_MANUAL = "INSERT INTO `manual` (" +
		"`id`, " +
		"`role_id`, " +
		"`kind`, " +
		"`awarded`, " +
		"`item_cfg_id`, " +
		"`pet_cfg_id`, " +
		"`evolution_lv`, " +
		"`star_max`," +
		"`has_normal`, " +
		"`has_variation`," +
		"`variation_evolution_lv`," +
		"`variation_star_max`," +
		"`has_super_evolution`," +
		"`has_variation_super_evolution`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
)
