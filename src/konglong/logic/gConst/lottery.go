package gConst

import "goproject/src/konglong/protocol"

const (
	CACHE_KEY_LOTTERY_PET = "cache_lottery_pet_%d"
)
const (
	LOTTERY_PET_COST_ITEM_CFG_ID     = int64(protocol.CurrencyType_Diamonds)
	LOTTERY_PET_COST_ITEM_CFG_ID_TEN = int64(protocol.CurrencyType_Gem)
	LOTTERY_PET_COST_ITEM_AMOUNT     = 300
	LOTTERY_PET_COST_ITEM_AMOUNT_TEN = 1500

	LOTTERY_ITEM_QUOTA_CD = HOUR * 3
)

const (
	SQL_SELECT_LOTTERY_PET        = "SELECT * FROM `lottery_pet` where `role_id` = ?;"
	SQL_UPDATE_LOTTERY_PET        = "UPDATE `lottery_pet` SET `score`=?, `award_str`=?, `super_evolution_buy_time`=?, `super_evolution_buy_amount`=? where `role_id` = ?"
	SQL_UPDATE_LOTTERY_PET_FIRST  = "UPDATE `lottery_pet` SET `is_first`=? where `role_id` = ?"
	SQL_UPDATE_LOTTERY_PET_SECOND = "UPDATE `lottery_pet` SET `is_second`=? where `role_id` = ?"
	SQL_UPDATE_LOTTERY_DAILY_LUCK = "UPDATE `lottery_pet` SET `daily_lottery_pet_luck`=? where `role_id` = ?"
	SQL_INSERT_LOTTERY_PET        = "INSERT INTO `lottery_pet` (" +
		"`role_id`, " +
		"`score`, " +
		"`award_str`," +
		"`is_first`," +
		"`is_second`," +
		"`super_evolution_buy_time`," +
		"`daily_lottery_pet_luck`," +
		"`super_evolution_buy_amount`" +
		") " +
		"VALUES (?,?,?,?,?,0,0,0);"
)
