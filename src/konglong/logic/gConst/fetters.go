package gConst

const (
	CACHE_KEY_FETTERS = "cache_fetters_%d"
)

const (
	SQL_SELECT_FETTERS = "SELECT * FROM `fetters` where `role_id` = ?"
	SQL_UPDATE_FETTERS = "UPDATE `fetters` SET `cfg_id`=?, `awarded`=? where `id` = ?"

	SQL_INSTER_FETTERS = "INSERT INTO `fetters` (`id`, " +
		"`role_id`, " +
		"`cfg_id`," +
		"`awarded`" +
		") " +
		"VALUES (?,?,?,?);"
)
