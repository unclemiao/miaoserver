package gConst

const (
	CACHE_KEY_FASHION = "cache_fashion_%d"
)

const (
	SQL_SELECT_FASHION = "SELECT * FROM `fashion` where `role_id`=?"

	SQL_UPDATE_FASHION = "UPDATE `fashion` SET `state`=? where `id`=?"

	SQL_INSERT_FASHION = "INSERT INTO `fashion` (" +
		"`id`, " +
		"`role_id`, " +
		"`cfg_id`, " +
		"`state`" +
		") " +
		"VALUES (?,?,?,?);"
)
