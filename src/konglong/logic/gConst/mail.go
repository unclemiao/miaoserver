package gConst

const (
	CACHE_KEY_MAILEE = "cache_mailee_%d"
)

const (
	SQL_SELECT_MAIL             = "SELECT * FROM `mail` where `time_out` >= ?"
	SQL_SELECT_MAILEE           = "SELECT * FROM `mailee` where `role_id` = ? and `time_out` >= ?"
	SQL_SELECT_MAILEE_FROM_ROLE = "SELECT * FROM `mailee` where `role_id` = ? and `name` = ? "

	SQL_UPDATE_MAILEE_AWARD        = "UPDATE `mailee` SET `award_time` = ?,  `is_read` = ? where `role_id` = ? and `name` = ?"
	SQL_UPDATE_MAILEE_READ         = "UPDATE `mailee` SET `is_read` = ? where `role_id` = ? and `name` = ?"
	SQL_UPDATE_MAILEE_DEL          = "UPDATE `mailee` SET `is_del` = ? where `role_id` = ? and `name` = ?"
	SQL_UPDATE_MAILEE_DEL_ROLE_ALL = "UPDATE `mailee` SET `is_del` = ? where `role_id` = ?"

	SQL_UPDATE_MAILEE_DEL_ALL = "DELETE FROM `mailee` WHERE `time_out` <= ?"

	SQL_INSERT_MAIL = "INSERT INTO `mail` (" +
		"`name`, " +
		"`title`, " +
		"`award`," +
		"`gift_code`," +
		"`each`," +
		"`time`," +
		"`time_out`," +
		"`body`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?);"

	SQL_INSERT_MAILEE = "INSERT INTO `mailee` (" +
		"`role_id`, " +
		"`name`, " +
		"`title`, " +
		"`award`," +
		"`gift_code`," +
		"`time`," +
		"`body`," +
		"`is_del`," +
		"`is_read`," +
		"`time_out`," +
		"`award_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?);"
)
