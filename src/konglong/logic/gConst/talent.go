package gConst

const (
	CACHE_KEY_ROLE_TALENT = "cache_role_talent_%d"
)

const (
	SQL_SELECT_ROLE_TALENT = "select * from `role_talent` where `role_id`=?"
	SQL_UPDATE_ROLE_TALENT = "update `role_talent` set `lv`=?, `total_point`=?  where `id`=?"
	SQL_DELETE_ROLE_TALENT = "DELETE FROM `role_talent` WHERE `role_id` = ?"
	SQL_INSERT_ROLE_TALENT = "INSERT INTO `role_talent` (" +
		"`id` " +
		",`role_id` " +
		",`cfg_id` " +
		",`lv` " +
		",`total_point` " +
		") " +
		"VALUES (?,?,?,?,?);"
)
