package gConst

const (
	CACHE_KEY_INVITE = "cache_invite_%d"
)

const (
	SQL_SELECT_INVITE       = "SELECT * FROM `invite` where `role_id` = ?"
	SQL_UPDATE_INVITE_AWARD = "UPDATE `invite` SET `awarded_amount`=? where `id` = ?"
	SQL_INSERT_INVITE       = "INSERT INTO `invite` (" +
		"`id`, " +
		"`role_id`, " +
		"`invite_role_id`," +
		"`invite_avater`," +
		"`invite_name`," +
		"`awarded_amount`," +
		"`time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?);"
)
