package gConst

const (
	DUEL_RANK_MAX             = 1000 // 竞技场上榜数
	DUEL_RAN_RANK_INFO_AMOUNT = 30   // 竞技场匹配范围
	DUEL_GET_RANK_INFO_AMOUNT = 3    // 竞技场匹配人数
)

const (
	CACHE_MY_DUEL        = "cache_my_duel_%d"
	CACHE_VIEW_DUEL_LIST = "cache_view_duel_list_%d"
)

const (
	SQL_SELECT_ALL_DUEL        = "select * from `duel`"
	SQL_SELECT_DUEL            = "select * from `duel` where `role_id` = ?"
	SQL_UPDATE_DULE_AVATAR     = "update `duel` set `name`=?, `avatar`=?, `duel_time`=? where `role_id`=?"
	SQL_UPDATE_DULE_RANK       = "update `duel` set `rank`=?  where `role_id`=?"
	SQL_UPDATE_DULE_FIGHT_INFO = "update `duel` set `fight_info_json`=? where `role_id`=?"
	SQL_INSERT_DUEL            = "INSERT INTO `duel` (" +
		"`rank`, " +
		"`role_id`, " +
		"`name`, " +
		"`is_robot`, " +
		"`avatar`, " +
		"`fight_info_json`," +
		"`duel_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?);"
)

const (
	SQL_SELECT_DUEL_WINNER_RECORD = "select * from `duel_record` where winner_id=?"
	SQL_SELECT_DUEL_LOSER_RECORD  = "select * from `duel_record` where loser_id=?"
	SQL_DELETE_DUEL_RECORD        = "delete from `duel_record` where duel_time <= ?"
	SQL_INSERT_DUEL_RECORD        = "INSERT INTO `duel_record` (" +
		"`id`, " +
		"`winner_id`, " +
		"`winner_name`, " +
		"`winner_avatar`, " +
		"`winner_is_robot`, " +
		"`loser_id`, " +
		"`loser_name`, " +
		"`loser_avatar`, " +
		"`loser_is_robot`, " +
		"`rank`, " +
		"`is_win`, " +
		"`duel_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?);"
)
