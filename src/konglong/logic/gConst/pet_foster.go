package gConst

const (
	CACHE_KEY_PET_FOSTER = "cache_pet_foster_%d"
)

const (
	FOSTER_TIME_MAX = int64(HOUR * 12)
)

const (
	SQL_SELECT_FOSTER = "SELECT * FROM `pet_foster` where `role_id` = ?"
	SQL_UPDATE_FOSTER = "UPDATE `pet_foster` SET" +
		"`pet_id` = ?" +
		", `begin_time` = ?" +
		" where `role_id` = ? and `grid` = ?"

	SQL_INSERT_FOSTER = "INSERT INTO `pet_foster` (" +
		"`role_id` " +
		",`pet_id` " +
		",`grid` " +
		",`begin_time`" +
		") " +
		"VALUES (?,?,?,?);"
)
