package gConst

const (
	CACHE_KEY_PET      = "cache_pet_%d"
	CACHE_KEY_VIEW_PET = "cache_view_pet_%d"

	PET_AMOUNT_MAX = 300 // 恐龙上限
)

const (
	SQL_SELECT_ALL_PET             = "SELECT * FROM `pet` where `role_id` = ?"
	SQL_SELECT_PET                 = "SELECT * FROM `pet` where `id` = ?"
	SQL_SELECT_STUB_PET            = "SELECT * FROM `pet` where `role_id` = ? and `stub_index` > 0"
	SQL_UPDATE_PET                 = "UPDATE `pet` SET `level`=?, `exp`=?, `evolution`=?, `star`=?, `breed`=? where `id` = ?"
	SQL_UPDATE_PET_NAME            = "UPDATE `pet` SET `name`=? where `id` = ?"
	SQL_UPDATE_PET_LOYAL           = "UPDATE `pet` SET `loyal`=? where `id` = ?"
	SQL_UPDATE_PET_EVOLUTION       = "UPDATE `pet` SET `evolution`=? where `id` = ?"
	SQL_UPDATE_PET_SUPER_EVOLUTION = "UPDATE `pet` SET `super_evolution`=? where `id` = ?"
	SQL_UPDATE_PET_STUB            = "UPDATE `pet` SET `stub_index`=?, `help_index`=? where `id` = ?"
	SQL_UPDATE_PET_BREED           = "UPDATE `pet` SET `breed`=? where `id` = ?"
	SQL_UPDATE_PET_STUDY_SKILL     = "UPDATE `pet` SET `study_skill_cfg_id_list_str`=? where `id` = ?"
	SQL_UPDATE_PET_VARIATION       = "UPDATE `pet` SET " +
		"`atk_add`=?," +
		"`def_add`=?," +
		"`hp_max_add`=?," +
		"`variation`=?," +
		"`try_variation`=?" +
		" where id = ?"

	SQL_INSTER_PET = "INSERT INTO `pet` (`id`, " +
		"`role_id`, " +
		"`cfg_id`, " +
		"`name`, " +
		"`level`, " +
		"`exp`, " +
		"`evolution`, " +
		"`super_evolution`, " +
		"`star`," +
		"`sex`," +
		"`variation`," +
		"`try_variation`," +
		"`stub_index`," +
		"`help_index`," +
		"`atk_add`," +
		"`def_add`," +
		"`hp_max_add`," +
		"`loyal`," +
		"`skill_cfg_id`," +
		"`study_skill_cfg_id_list_str`," +
		"`breed`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
	SQL_DELETE_PET = "DELETE FROM `pet` where `id` = ?"
)
