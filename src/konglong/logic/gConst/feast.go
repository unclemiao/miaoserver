package gConst

const (
	FEAST_CFG_ID_LOTTERY_PET        = 1110
	FEAST_CFG_ID_ROLE_ONLINE        = 1120
	FEAST_CFG_ID_NEWBIE_GIFT_PET    = 1130
	FEAST_CFG_ID_NEWBIE_GIFT_ONLINE = 1140
	FEAST_CFG_ID_REPEAT_PAY         = 4101
	FEAST_CFG_ID_MONTH_CARD         = 5101
)
const (
	CACHE_KEY_FEAST_LIMIT_TIME    = "cache_feast_limit_time_%d"
	CACHE_KEY_FEAST_FAST_RANK     = "cache_feast_fast_rank_%d"
	CACHE_KEY_FEAST_PET_KING      = "cache_feast_pet_king_%d"
	CACHE_KEY_FEAST_PAY_REPEAT    = "cache_feast_pay_%d"
	CACHE_KEY_FEAST_MONTH_CARD    = "cache_feast_month_card_%d"
	CACHE_KEY_FEAST_PET_KING_RANK = "cache_feast_pet_king_rank"
)

const (
	SQL_SELECT_ALL_FEAST_DONE            = "select * from `feast_done` where `role_id`=? and `kind` = ?"
	SQL_UPDATE_FEAST_DONE                = "update `feast_done` set `done`=?, `awarded`=?  where `id`=?"
	SQL_UPDATE_FEAST_AWARD               = "update `feast_done` set `awarded`=?, `award_num`=? where `id`=?"
	SQL_UPDATE_FEAST_DONE_FIGHT_INFO     = "update `feast_done` set `fight_info_json`=? where `id`=?"
	SQL_UPDATE_FEAST_DONE_ALL_FIGHT_INFO = "update `feast_done` set `done`=?, `awarded`=? where `kind`=?"
	SQL_INSERT_FEAST_DONE                = "INSERT INTO `feast_done` (" +
		"`id` " +
		",`role_id` " +
		",`cfg_id` " +
		",`kind` " +
		",`done` " +
		",`awarded`" +
		",`award_num`" +
		",`fight_info_json`" +
		",`fight_win_amount`" +
		",`fight_lose_amount`" +
		",`update_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,0,?,0,0,?);"
)

const (
	PET_KING_RANK_MAX             = 1000 // 争霸赛上榜数
	PET_KING_RANK_INIT_SCORE      = 1000 // 争霸赛初始积分
	PET_KING_GET_RANK_INFO_AMOUNT = 3    // 争霸赛匹配人数
)

const (
	SQL_SELECT_ALL_FEAST_DONE_PET_KING_RANK = "select * from `feast_done`  where `role_id` > 0 and `kind` = ?"
	SQL_SELECT_FEAST_DONE_PET_KING_RANK     = "select * from `feast_done` where `role_id`=0 and `kind` = ?"
	SQL_SELECT_ALL_PET_KING_ROLE            = "select `role_id`, `level` from `role`"
	SQL_UPDATE_ALL_PET_KING_AWARD           = "update `feast_done` set `awarded`=false where `kind`=?"
	SQL_UPDATE_PET_KING_SCORE               = "update `feast_done` set `done`=?, `fight_win_amount`=?, `fight_lose_amount`=?, `update_time`=? where `id`=?"
)

const (
	SQL_SELECT_PET_KING_ATKER_RECORD = "select * from `pet_king_record` where `atker_id`=?"
	SQL_SELECT_PET_KING_DEFER_RECORD = "select * from `pet_king_record` where `defer_id`=?"
	SQL_DELETE_PET_KING_RECORD       = "delete from `pet_king_record` where `create_time` <= ?"
	SQL_INSERT_PET_KING_RECORD       = "INSERT INTO `pet_king_record` (" +
		"`id`, " +
		"`atker_id`, " +
		"`atker_name`, " +
		"`atker_avatar`, " +
		"`atker_score`, " +
		"`defer_id`, " +
		"`defer_name`, " +
		"`defer_avatar`, " +
		"`defer_score`, " +
		"`create_time`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?);"
)
