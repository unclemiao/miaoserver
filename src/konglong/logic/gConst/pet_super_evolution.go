package gConst

const (
	CACHE_PET_SUPER_EVOLUTION = "cache_pet_super_evolution_%d"
)

const (
	SQL_SELECT_SUPER_EVOLUTION = "select * from `pet_super_evolution` where `role_id`=?"
	SQL_UPDATE_SUPER_EVOLUTION = "update `pet_super_evolution` set `progress`=? where `id`=?"
	SQL_INSERT_SUPER_EVOLUTION = "INSERT INTO `pet_super_evolution` (" +
		"`id`, " +
		"`role_id`, " +
		"`cfg_id`, " +
		"`progress` " +
		") " +
		"VALUES (?,?,?,?);"
)
