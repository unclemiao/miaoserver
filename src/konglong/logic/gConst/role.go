package gConst

const (
	CACHE_KEY_ROLE        = "cache_role_%d"
	ROLE_OFFLINE_TIME_OUT = 5 * SECOND // sec

	HANG_CURRENCY_SUB_SEC = 1 * SECOND
	HANG_ITEM_SUB_SEC     = MINUTE
)

// 角色标记
const (
	ROLE_TAG_INVALID = 0
	ROLE_TAG_DEL     = 0b001
	ROLE_TAG_LOCK    = 0b010
)

const (
	SQL_SELECT_ROLE_ALL                = "SELECT * FROM `role`;"
	SQL_SELECT_ROLE_BY_ACCOUNT         = "SELECT * FROM `role` where `account_id` = ? and `sid` = ?;"
	SQL_SELECT_ROLE                    = "SELECT * FROM `role` where `role_id` = ?"
	SQL_UPDATE_ROLE_PASS               = "UPDATE `role` SET `game_pass` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_PASS_AND_EXT_AWARD = "UPDATE `role` SET `game_pass` = ?, `last_game_pass_ext_award` = ?, `game_pass_ext_award_amount`=? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_LOTTERY_ITEM_RATE  = "UPDATE `role` SET `lottery_item_rate` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_SETTING            = "UPDATE `role` SET `audio` = ?, `shake` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_SEVEN_SIGN         = "UPDATE `role` SET `seven_sign` = ?, `next_seven_sign_time` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_NEWBOE             = "UPDATE `role` SET `newbie` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_SYSTEM_OPEN_AWARD  = "UPDATE `role` SET `system_awarded` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_LEVEL              = "UPDATE `role` SET `level`=?, `exp`=? where `role_id` = ?"
	SQL_UPDATE_ROLE_TAG                = "UPDATE `role` SET `tag`=? where `role_id` = ?"
	SQL_UPDATE_ROLE_INFO               = "UPDATE `role` SET `name` = ?, `avatar` = ?, `language`=? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_HORSE              = "UPDATE `role` SET `horse` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_MODEL              = "UPDATE `role` SET `model_cfg_id` = ?, `model_name`=? WHERE `role_id` = ?"
	SQL_UPDATE_PRIVATE_PROTO           = "UPDATE `role` SET `private_proto` = ? WHERE `role_id` = ?"
	SQL_UPDATE_COLLECT                 = "UPDATE `role` SET `collect` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_INVITE_NOTIFY      = "UPDATE `role` SET `invite_notify` = ? WHERE `role_id` = ?"
	SQL_UPDATE_ROLE_PAY                = "UPDATE `role` SET `payn` = ?, `payall`=? WHERE `role_id` = ?"
	SQL_INSERT_ROLE                    = "INSERT INTO `role` (" +
		"`role_id`, " +
		"`account_id`, " +
		"`account_name`, " +
		"`channel`, " +
		"`sid`," +
		"`tag`," +
		"`name`," +
		"`avatar`," +
		"`level`," +
		"`exp`," +
		"`game_pass`," +
		"`last_game_pass_ext_award`," +
		"`game_pass_ext_award_amount`," +
		"`lottery_item_rate`," +
		"`newbie`," +
		"`seven_sign`," +
		"`next_seven_sign_time`," +
		"`system_awarded`," +
		"`help_index_unlock_info`," +
		"`horse`," +
		"`model_cfg_id`," +
		"`model_name`," +
		"`private_proto`," +
		"`collect`," +
		"`audio`," +
		"`shake`," +
		"`language`," +
		"`payn`," +
		"`payall`," +
		"`invite_id`," +
		"`invite_sid`," +
		"`invite_notify`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,1,0,?,0,0,?,0,0,0,0,?,?,?,?,?,0,?,?,?,0,0,?,?,0);"
)
