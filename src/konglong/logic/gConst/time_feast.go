package gConst

import (
	"goproject/engine/time_helper"
)

const (
	CACHE_KEY_TIME_FEAST = "cache_time_feast_%d"
	LOOP_TIME_FEAST_TIME = time_helper.Minute
)

const (
	SQL_SELECT_TIME_FEAST         = "SELECT * FROM `time_feast` where `role_id` = ?"
	SQL_UPDATE_TIME_FEAST_SEC     = "UPDATE `time_feast` SET `last_sec`=?, `day_sec`=?, `sec`=?, `next_drop_box_time`=?, `young`=?,`ip`=? where `role_id` = ?"
	SQL_UPDATE_TIME_FEAST_NEW_DAY = "UPDATE `time_feast` SET `last_sec`=?, `day_sec`=?, `award_str`=?, `sec`=?, `next_drop_box_time`=?, `signin`=?, `young`=?, `ip`=? where `role_id` = ?"
	SQL_UPDATE_TIME_FEAST_HANG    = "UPDATE `time_feast` SET `last_view_hang_sec`=?, `last_hang_awarded_sec`=?, `award_str`=? where `role_id` = ?"
	SQL_INSTER_TIME_FEAST         = "INSERT INTO `time_feast` (" +
		"`role_id`, " +
		"`new_role`, " +
		"`ip`, " +
		"`last_sec`, " +
		"`young`, " +
		"`day_sec`," +
		"`week_sec`," +
		"`last_week_sec`," +
		"`week_day`," +
		"`sec`," +
		"`signin`," +
		"`next_drop_box_time`," +
		"`last_view_hang_sec`," +
		"`last_hang_awarded_sec`," +
		"`award_str`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
)
