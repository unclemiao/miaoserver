package gConst

const (
	CACHE_KEY_ROLE_MALL = "cache_role_mall_%d"
)

const (
	SQL_SELECT_ROLE_MALL = "SELECT * FROM `role_mall` where `role_id` = ?"
	SQL_UPDATE_ROLE_MALL = "UPDATE `role_mall` SET `amount`=? where `id` = ?"

	SQL_INSTER_ROLE_MALL = "INSERT INTO `role_mall` (" +
		"`id`, " +
		"`role_id`, " +
		"`cfg_id`," +
		"`amount`" +
		") " +
		"VALUES (?,?,?,?);"
)
