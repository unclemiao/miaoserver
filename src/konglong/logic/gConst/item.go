package gConst

const (
	CACHE_KEY_ALL_ITEM = "cache_all_item_%d"

	FOOD_KIT_AMOUNT_MAX     = 18  // 食物栏上限
	PIECES_KIT_AMOUNT_MAX   = 100 // 碎片栏上限
	CURRENCY_KIT_AMOUNT_MAX = 100 // 货币上限
	TOOL_KIT_AMOUNT_MAX     = 18  // 工具栏上限
	EQUIP_KIT_AMOUNT_MAX    = 60  // 装备栏上限

	STORAGE_ITEM_KIT_AMOUNT_MAX = 50  // 道具仓库上限
	STORAGE_FOOD_KIT_AMOUNT_MAX = 50  // 食物仓库上限
	STORAGE_TOOL_KIT_AMOUNT_MAX = 250 // 工具仓库上限

	LOTTERY_EQUIP_COST_ONE = 300
	LOTTERY_EQUIP_COST_TEN = 2700
)

const (
	ITEM_CFG_ID_DIAMONDS = 1
	ITEM_CFG_ID_GOLD     = 2
	ITEM_CFG_ID_EXP      = 3
)

const (
	SQL_SELECT_ALL_ITEM          = "SELECT * FROM `item` where `role_id` = ?"
	SQL_SELECT_ITEM              = "SELECT * FROM `item` where `id` = ?"
	SQL_UPDATE_ITEM_AMOUNT       = "UPDATE `item` SET `n`=? where `id` = ?"
	SQL_UPDATE_ITEM_KIT          = "UPDATE `item` SET `kit`=? where `id` = ?"
	SQL_UPDATE_ITEM_GRID         = "UPDATE `item` SET `grid`=? where `id` = ?"
	SQL_UPDATE_ITEM_KIT_AND_GRID = "UPDATE `item` SET `kit`=?, `grid`=? where `id` = ?"
	SQL_UPDATE_ITEM_FOR_EQUIP    = "UPDATE `item` SET `kit`=?, `grid`=?, `owner_id`=? where `id` = ?"
	SQL_DEL_ITEM_CLEAN_KIT       = "DELETE FROM `item` where `role_id` = ? and `kit` = 1;"
	SQL_UPDATE_ITEM_STAR         = "UPDATE `item` SET `star`=? where `id` = ?"

	SQL_UPDATE_ITEM = "UPDATE `item` Set  " +
		"`owner_id`=?, " +
		"`cid`=?, " +
		"`n`=?," +
		"`grid`=?," +
		"`kit`=?," +
		"`skill_cfg_id`=?," +
		"`open_cfg_id`=?," +
		"`hp_max_base`=?," +
		"`atk_base`=?," +
		"`def_base`=?," +
		"`grade`=?," +
		"`star`=?," +
		"`rand_attr_str`=?," +
		"`due`=? " +
		"where id=? "

	SQL_INSERT_ITEM = "INSERT INTO `item` (`id`, " +
		"`role_id`, " +
		"`owner_id`, " +
		"`cid`, " +
		"`n`," +
		"`grid`," +
		"`kit`," +
		"`skill_cfg_id`," +
		"`open_cfg_id`," +
		"`hp_max_base`," +
		"`atk_base`," +
		"`def_base`," +
		"`grade`," +
		"`star`," +
		"`rand_attr_str`," +
		"`due`" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);"
)
