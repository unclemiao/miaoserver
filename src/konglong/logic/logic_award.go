package logic

import (
	"goproject/src/konglong/protocol"
	"net/http"
)

type AwardManager struct {
}

func init() {
	gAwardMgr = &AwardManager{}
}

func (mgr *AwardManager) Init() {
	HandleFunc("/game-api/player/award/all", func(w http.ResponseWriter, r *http.Request) {
		awardList := make([]*protocol.Award, 0)
		SendToHttp(w, awardList)
	})
}
