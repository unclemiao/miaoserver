package logic

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/protocol"
)

type PayManager struct {
}

var gPayMgr *PayManager

func init() {
	gPayMgr = &PayManager{}
}

func (mgr *PayManager) Init() {
	HandleFunc("/pay", func(w http.ResponseWriter, r *http.Request) {
		form := r.Form
		var (
			ctx = &data.Context{Sid: gServer.Node.Sid}
			// 服务器号
			sid = util.AtoInt32(form.Get("sid"))
			// 角色id
			accountId = form.Get("account_id")
			// 购买获得符文金额
			pay = util.AtoInt32(form.Get("pay"))
			// 实际支付rmb金额
			money = util.AtoInt64(form.Get("money"))
			// 订单号
			bill = util.AtoInt32(form.Get("bill"))
			// due
			due = util.AtoInt64(form.Get("due"))
			// token
			token = form.Get("token")
		)
		_, _ = mgr.givePay(ctx, sid, accountId, pay, money, bill, due, token)

	})

	HandleFunc("/pay_mall", func(w http.ResponseWriter, r *http.Request) {
		form := r.Form
		var (
			// 服务器号
			sid = util.AtoInt32(form.Get("sid"))
			// 角色id
			accountId = form.Get("account_id")
			// 购买获得符文金额
			mallId = util.AtoInt32(form.Get("mall_id"))
			// 实际支付rmb金额
			money = util.AtoInt64(form.Get("money"))
			// 订单号
			bill = form.Get("bill")
			// due
			due = util.AtoInt64(form.Get("due"))
			// token
			token = form.Get("token")

			ctx = &data.Context{Sid: sid}

			nowSec = time.Now().Unix()

			httpResp = &protocol.HttpResponse{
				Status:   200,
				DateTime: nowSec * 1000,
			}
		)

		if gAdminMgr.ServerCmd.Platform != "test" || sid == 998 {
			if nowSec > due {
				httpResp.Status = 502
				httpResp.ErrMsg = "token time out"
				bs, _ := json.Marshal(httpResp)
				w.Write(bs)
				return
			}
			md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%d%d%s%d%s", sid, accountId, mallId, money, bill, due, "s1yb8wa298be84e4c3c2a7901e47c350"))

			if md5 != token {
				httpResp.Status = 501
				httpResp.ErrMsg = "token err"
				bs, _ := json.Marshal(httpResp)
				w.Write(bs)
				return
			}
		}
		code, err := mgr.giveMall(ctx, sid, accountId, mallId, money, bill, due, token)
		assert.Assert(err == nil, err)
		httpResp.Status = code
		httpResp.Msg = "success"
		bs, _ := json.Marshal(httpResp)
		w.Write(bs)
	})
}

func (mgr *PayManager) givePay(ctx *data.Context, sid int32, accountId string, pay int32, money int64, bill int32, due int64, token string) (int32, error) {
	var sidOk bool
	for _, n := range gServer.NodeList {
		if sid == n.Sid {
			sidOk = true
			break
		}
	}
	if !sidOk || pay == 0 || money == 0 {
		ctx.Stop = true
		return 400, fmt.Errorf("params err")
	}

	r, _ := gRoleMgr.RoleByAccountId(ctx, accountId, sid)
	if r == nil {
		ctx.Stop = true
		return 404, fmt.Errorf("accountId or sid err")
	}
	return 200, nil
}

func (mgr *PayManager) giveMall(ctx *data.Context, sid int32, accountId string, mallId int32, money int64, bill string, due int64, token string) (code int32, e error) {
	defer func() {
		err := recover()
		if err != nil {
			code = 400
			e = fmt.Errorf("%+v", err)
		}
	}()
	var sidOk bool
	for _, n := range gServer.NodeList {
		if sid == n.Sid {
			sidOk = true
			break
		}
	}
	if !sidOk || mallId == 0 || money == 0 {
		ctx.Stop = true
		code, e = 400, fmt.Errorf("params err")
		return
	}

	r, _ := gRoleMgr.RoleByAccountId(ctx, accountId, sid)
	if r == nil {
		ctx.Stop = true
		code, e = 400, fmt.Errorf("accountId or sid err")
		return
	}
	logs.Infof("giveMall sid:%d accountId:%s mallId:%d money:%d bill:%s", sid, accountId, mallId, money, bill)
	ctx.Session = gRoleMgr.RoleSessionBy(ctx, r.RoleId)
	gMallMgr.Buy(ctx, r.RoleId, mallId, money, bill)
	if ctx.Session != nil {
		gSessionMgr.Flush(ctx.Session)
	}
	code, e = 200, nil
	return
}
