package logic

import (
	"fmt"
	"sort"
	"time"

	"goproject/engine/assert"
	"goproject/engine/util"
	"goproject/src/konglong/data"
	"goproject/src/konglong/logic/config"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

type GamePassManager struct{}

func init() {
	gGamePassMgr = &GamePassManager{}
}

func (mgr *GamePassManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(ctx *data.Context, args interface{}) {
		msg := args.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(msg.RoleId))
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_WinPassMonster, mgr.HandlerWinMonster)
	gEventMgr.When(protocol.EnvelopeType_C2S_WinPassBoss, mgr.HandlerWinBoss)
	gEventMgr.When(protocol.EnvelopeType_C2S_LosePassMonster, mgr.HandlerLoseMonster)
	gEventMgr.When(protocol.EnvelopeType_C2S_LosePassBoss, mgr.HandlerLoseBoss)

	gEventMgr.When(protocol.EnvelopeType_C2S_WinNewbieBoss, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			repMsg = args.GetC2SWinNewbieBoss()
		)
		mgr.WinNewbieBoss(ctx, ctx.RoleId, repMsg.Index)
	})

	gEventMgr.When(protocol.EnvelopeType_C2S_AwardExtNewbieBoss, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			repMsg = args.GetC2SAwardExtNewbieBoss()
		)
		assert.Assert(len(args.AdSdkToken) > 0, "invalid adsdk")
		ctx.ArgsInt = int64(repMsg.Index)
		mgr.GetNewbieBossAwardExt(ctx, ctx.RoleId, repMsg.Index)
	})
	gEventMgr.When(protocol.EnvelopeType_C2S_GetGamePassExtAward, func(ctx *data.Context, args *protocol.Envelope) {
		var (
			repMsg = args.GetC2SGetGamePassExtAward()
		)
		ctx.ArgsInt = int64(repMsg.Pass)
		mgr.GetExtAward(ctx, ctx.RoleId, repMsg.Pass)
	})

	gEventMgr.When(data.EVENT_MSGID_ONLINE_ROLE, func(ctx *data.Context, msg interface{}) {
		args := msg.(*data.EventOnlineRole)
		TimeAfter(nil, time.Second*2, func(ctx2 *data.Context) {
			mgr.Notify(ctx2, args.RoleId, nil)
		})
	})
}

func (mgr *GamePassManager) DropBox(ctx *data.Context, roleId int64) int32 {
	role := gRoleMgr.RoleBy(ctx, roleId)
	foodBook := gFoodBookMgr.FoodBookBy(ctx, roleId)
	gives := &data.AwardCfg{}
	cfgList, _ := config.GetGameDropCfgList()
	selecter := util.NewWeightSelecter()

	for _, cfg := range cfgList {
		if cfg.PassNum == 1 && foodBook.Level >= cfg.FoodLv {
			if cfg.CfgId == 2 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Diamond) > 0 {
					selecter.Add(cfg)
				}
			} else if cfg.CfgId == 3 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Pet) > 0 {
					selecter.Add(cfg)
				}
			} else {
				selecter.Add(cfg)
			}
		} else if cfg.PassNum == 2 && role.GamePass >= cfg.FoodLv {
			if cfg.CfgId == 2 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Diamond) > 0 {
					selecter.Add(cfg)
				}
			} else if cfg.CfgId == 3 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Pet) > 0 {
					selecter.Add(cfg)
				}
			} else {
				selecter.Add(cfg)
			}
		} else if cfg.PassNum == 3 && role.Level >= cfg.FoodLv {
			if cfg.CfgId == 2 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Diamond) > 0 {
					selecter.Add(cfg)
				}
			} else if cfg.CfgId == 3 {
				if gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Drop_Pet) > 0 {
					selecter.Add(cfg)
				}
			} else {
				selecter.Add(cfg)
			}
		}
	}
	dropCfgItf := selecter.Random()
	if dropCfgItf != nil {
		dropCfg := dropCfgItf.(*data.GameDropCfg)
		gives.AwardList = append(gives.AwardList, &data.ItemAmount{
			CfgId: dropCfg.ItemCfgId,
			N:     1,
		})
		gItemMgr.GiveAward(ctx, role.RoleId, gives, 1, protocol.ItemActionType_Action_Guaji)
		gQuotaCdMgr.GiveQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Drop_Box)
		if dropCfg.CfgId == 2 {
			gQuotaCdMgr.TakeQuota(ctx, role.RoleId, 1, protocol.QuotaType_Quota_Game_Drop_Diamond)
		}
	}

	return gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Drop_Box)
}

func (mgr *GamePassManager) WinMonster(ctx *data.Context, roleId int64) {

}

func (mgr *GamePassManager) WinBoss(ctx *data.Context, roleId int64) {
	role := gRoleMgr.RoleBy(ctx, roleId)
	passCfg := config.GamePassCfgBy(role.GamePass)
	assert.Assert(role.Level >= passCfg.RoleLevel, "!角色达%d级后可挑战Boss！", passCfg.RoleLevel)
	randAward := config.RandAwardCfg(passCfg.BossAward)
	gItemMgr.GiveAward(ctx, role.RoleId, randAward, 1, protocol.ItemActionType_Action_Guaji)
	ext := (role.GamePass - role.LastGamePassExtAward) >= gConst.GAME_PASS_EXT_AWARD_STEP_MAX
	if !ext && (role.GamePass-role.LastGamePassExtAward) >= gConst.GAME_PASS_EXT_AWARD_STEP_MIN {
		ext = util.RandomMatched(2000)
	}
	if role.GamePass <= 2 || role.GamePass == 5 {
		ext = true
	}
	role.LastGamePassExtAward = util.ChoiceInt32(ext, role.GamePass, role.LastGamePassExtAward)
	role.GamePass += 1

	role.GamePassExtAwardAmount = util.ChoiceInt32(ext, 0, role.GamePassExtAwardAmount)

	gSql.Begin().Clean(role.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_PASS_AND_EXT_AWARD,
		role.GamePass, role.LastGamePassExtAward, role.GamePassExtAwardAmount, role.RoleId)

	// gRoleMgr.Send(ctx, role.RoleId, protocol.MakeNotifyRoleUpdateMsg(role.GetForClient()))

	gRoleMgr.Send(ctx, role.RoleId, protocol.MakeWinPassBossDoneMsg(role.GamePass-1, randAward.GetForClient(), ext))

	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_GAME_PASS_FIGHT_BOSS, 1)

}

func (mgr *GamePassManager) LoseBoss(ctx *data.Context, roleId int64) {
	gDailyQuestMgr.Done(ctx, roleId, gConst.QUEST_TYPE_GAME_PASS_FIGHT_BOSS, 1)
}

func (mgr *GamePassManager) GetExtAward(ctx *data.Context, roleId int64, pass int32) {
	var (
		role                   = gRoleMgr.RoleBy(ctx, roleId)                                                             // 角色对象
		extAwardN              = gQuotaCdMgr.QuotaNumBy(ctx, roleId, protocol.QuotaType_Quota_Game_Pass_Ext_Award_Amount) // 已经领取的额外奖励数量
		awardList, totalWeight = config.NoramlGamePassExtAwardCfgListBy()
		passExtAwardCfg        *data.GamePassExtAwardCfg
	)
	assert.Assert(role.GamePass >= pass, "!未通过[%d]", pass)
	assert.Assert(role.LastGamePassExtAward <= pass, "invalid pass[%d]", pass)
	assert.Assert(role.GamePassExtAwardAmount < 2, "invalid amount[%d]", role.GamePassExtAwardAmount)
	awardCfg := &data.AwardCfg{}
	if role.GamePass <= 3 {
		awardCfg = config.GetFirstAwardCfg()
	} else {
		if extAwardN < 3 {
			awardList, totalWeight = config.FirstGamePassExtAwardCfgListBy()
		}

		// 给奖励
		hit := util.Random64(0, totalWeight-1)
		for _, cfg := range awardList {
			if hit <= util.ChoiceInt64(extAwardN < 3, cfg.FirstWeight, cfg.NormalWeight) {
				passExtAwardCfg = cfg
				break
			}
		}
		config.AwardCfgAdd(awardCfg, passExtAwardCfg.Award, float64(util.Random32(passExtAwardCfg.AmountRange[0], passExtAwardCfg.AmountRange[1])))
	}
	gQuotaCdMgr.GiveQuota(ctx, roleId, 1, protocol.QuotaType_Quota_Game_Pass_Ext_Award_Amount)

	gItemMgr.GiveAward(ctx, roleId, awardCfg, 1, protocol.ItemActionType_Action_AdSDKTask)
	role.GamePassExtAwardAmount++
	gSql.Begin().Clean(role.RoleId).MustExcel(gConst.SQL_UPDATE_ROLE_PASS_AND_EXT_AWARD,
		role.GamePass, role.LastGamePassExtAward, role.GamePassExtAwardAmount, role.RoleId)

	gRoleMgr.Send(ctx, roleId, protocol.MakeGetGamePassExtAwardDoneMsg(ctx.EventMsg.GetEnvMsg(), awardCfg.GetForClient()))

}

// **************************** controllers ***************************************
func (mgr *GamePassManager) HandlerWinMonster(ctx *data.Context, msg *protocol.Envelope) {
	// mgr.WinMonster(ctx, ctx.RoleId)
}

func (mgr *GamePassManager) HandlerWinBoss(ctx *data.Context, msg *protocol.Envelope) {
	mgr.WinBoss(ctx, ctx.RoleId)
}

func (mgr *GamePassManager) HandlerLoseMonster(ctx *data.Context, msg *protocol.Envelope) {

}

func (mgr *GamePassManager) HandlerLoseBoss(ctx *data.Context, msg *protocol.Envelope) {

	mgr.LoseBoss(ctx, ctx.RoleId)
}

// **************************************** 新手 boss 相关 ************************************
func (mgr *GamePassManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_NEWBIE_BOSS, roleId)
}

func (mgr *GamePassManager) NewbieBossBy(ctx *data.Context, roleId int64) *data.NewbieBoss {
	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		newbieBoss := &data.NewbieBoss{
			RoleId: roleId,
		}
		var dataList []*data.NewbieBossPass
		gSql.Begin().Clean(roleId)
		gSql.MustSelect(&dataList, gConst.SQL_SELECT_NEWBIE_BOSS, roleId)
		for _, dbData := range dataList {
			newbieBoss.PassList = append(newbieBoss.PassList, dbData)
		}
		sort.Slice(newbieBoss.PassList, func(i, j int) bool {
			return newbieBoss.PassList[i].CfgIndex < newbieBoss.PassList[j].CfgIndex
		})
		return newbieBoss, nil

	}, gConst.SERVER_DATA_CACHE_TIME_OUT)
	assert.Assert(err == nil, err)
	return rv.(*data.NewbieBoss)
}

func (mgr *GamePassManager) NewbieBossPassBy(ctx *data.Context, roleId int64, cfgIndex int32) *data.NewbieBossPass {
	info := mgr.NewbieBossBy(ctx, roleId)
	for _, pass := range info.PassList {
		if pass.CfgIndex == cfgIndex {
			return pass
		}
	}
	return nil
}

func (mgr *GamePassManager) Notify(ctx *data.Context, roleId int64, info *data.NewbieBoss) {
	if info == nil {
		info = mgr.NewbieBossBy(ctx, roleId)
	}
	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyNewbieBossPassMsg(info.GetForClient()))
}

func (mgr *GamePassManager) NotifyUpdate(ctx *data.Context, roleId int64, info *data.NewbieBossPass) {

	gRoleMgr.Send(ctx, roleId, protocol.MakeNotifyNewbieBossPassUpdateMsg([]*protocol.NewbieBossPass{info.GetForClient()}))
}

func (mgr *GamePassManager) WinNewbieBoss(ctx *data.Context, roleId int64, cfgIndex int32) {
	newbieBossInfo := mgr.NewbieBossBy(ctx, roleId)
	pass := mgr.NewbieBossPassBy(ctx, roleId, cfgIndex)
	r := gRoleMgr.RoleBy(ctx, roleId)
	if pass == nil {
		config.GetNewbieBossCfg(cfgIndex)
		pass = &data.NewbieBossPass{
			Id:       gIdMgr.GenerateId(ctx, r.Sid, gConst.ID_TYPE_NEWBIE_BOSS_PASS),
			RoleId:   roleId,
			CfgIndex: cfgIndex,
			Passed:   true,
		}
		newbieBossInfo.PassList = append(newbieBossInfo.PassList, pass)
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_INSERT_NEWBIE_BOSS,
			pass.Id,
			pass.RoleId,
			pass.CfgIndex,
			pass.Awarded,
			pass.ExtAwarded,
			pass.Passed,
		)
	} else {
		assert.Assert(!pass.Passed, "!新手BOSS已通关")
		pass.Passed = true
		gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_NEWBIE_BOSS, pass.Awarded, pass.ExtAwarded, pass.Passed, pass.Id)
	}
	mgr.GetAward(ctx, roleId, cfgIndex)

	mgr.NotifyUpdate(ctx, roleId, pass)
}

func (mgr *GamePassManager) GetAward(ctx *data.Context, roleId int64, cfgIndex int32) {
	pass := mgr.NewbieBossPassBy(ctx, roleId, cfgIndex)
	assert.Assert(pass != nil && pass.Passed, "!boss[%d]关卡未通关", cfgIndex)
	assert.Assert(!pass.Awarded, "!奖励已领取")
	pass.Awarded = true
	cfg := config.GetNewbieBossCfg(cfgIndex)
	gSql.Begin().Clean(roleId).MustExcel(gConst.SQL_UPDATE_NEWBIE_BOSS, pass.Awarded, pass.ExtAwarded, pass.Passed, pass.Id)
	gItemMgr.GiveAward(ctx, roleId, cfg.Award, 1, protocol.ItemActionType_Action_Boss)
	mgr.NotifyUpdate(ctx, roleId, pass)
}

func (mgr *GamePassManager) GetNewbieBossAwardExt(ctx *data.Context, roleId int64, cfgIndex int32) {
	pass := mgr.NewbieBossPassBy(ctx, roleId, cfgIndex)
	assert.Assert(pass != nil && pass.Passed, "!boss[%d]关卡未通关", cfgIndex)
	assert.Assert(!pass.ExtAwarded, "!奖励已领取")
	gSql.Begin().Clean(roleId)
	pass.ExtAwarded = true
	cfg := config.GetNewbieBossCfg(cfgIndex)
	gSql.MustExcel(gConst.SQL_UPDATE_NEWBIE_BOSS, pass.Awarded, pass.ExtAwarded, pass.Passed, pass.Id)
	gItemMgr.GiveAward(ctx, roleId, cfg.ExtAward, 1, protocol.ItemActionType_Action_Boss_Ext)
	mgr.NotifyUpdate(ctx, roleId, pass)

}
