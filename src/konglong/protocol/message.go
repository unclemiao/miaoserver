package protocol

func (m *Envelope) IsNetEvent() bool {
	return true
}

func (m *Envelope) GetEnvMsg() *Envelope {
	return m
}

func (m *Envelope) GetMsgId() EnvelopeType {
	return m.MsgType
}

func (m *Envelope) SetMsgId(id EnvelopeType) {
	m.MsgType = id
}

func (x EnvelopeType) Int32() int32 {
	return int32(x)
}
