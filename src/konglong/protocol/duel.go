package protocol

func MakeNotifyDuelInfoMsg(myduel *DuelInfo, duelInfo []*DuelInfo) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyDuelInfo,
		Payload: &Envelope_S2CNotifyDuelInfo{S2CNotifyDuelInfo: &S2C_NotifyDuelInfoMsg{
			MyDuelInfo:   myduel,
			DuelInfoList: duelInfo,
		}},
	}
}

func MakeGetDuelRankListDoneMsg(rep *Envelope, duelInfo []*DuelRankInfo) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_GetDuelRankListDone,
		Payload: &Envelope_S2CGetDuelRankListDone{S2CGetDuelRankListDone: &S2C_GetDuelRankListDoneMsg{
			DuelRankInfoList: duelInfo,
		}},
	}
}

func MakeGetDuelFightRecordListDoneMsg(rep *Envelope, duelRecordInfo []*DuelFightRecord) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_GetDuelFightRecordListDone,
		Payload: &Envelope_S2CGetDuelFightRecordListDone{S2CGetDuelFightRecordListDone: &S2C_GetDuelFightRecordListDoneMsg{
			DuelFightRecordList: duelRecordInfo,
		}},
	}
}
