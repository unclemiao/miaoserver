package protocol

type HttpResponse struct {
	Result   int32       `json:"result"`
	Msg      string      `json:"msg"`
	ErrMsg   string      `json:"errMsg"`
	Status   int32       `json:"status"`
	DateTime int64       `json:"dateTime"`
	Data     interface{} `json:"models"`
}

func MakePongMsg(env *Envelope) *Envelope {
	return &Envelope{
		SeqId:   env.SeqId,
		MsgType: EnvelopeType_S2C_Pong,
		Payload: &Envelope_S2CPong{S2CPong: &S2C_PongMsg{}},
	}
}
