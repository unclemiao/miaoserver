package protocol

func MakeNotifyInviteListMsg(list []*Invite, award int32) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyInviteList,
		Payload: &Envelope_S2CNotifyInviteList{S2CNotifyInviteList: &S2C_NotifyInviteListMsg{
			InviteList: list,
			Awarded:    award,
		}},
	}
}
