package protocol

func MakeGetRankingDoneMsg(rankList []*RankInfo, myRank *SelfRankInfo) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_GetRankingDone,
		Payload: &Envelope_S2CGetRankingDone{S2CGetRankingDone: &S2C_GetRankingDoneMsg{
			RankInfoList: rankList,
			MyRankInfo:   myRank,
		}},
	}
}
