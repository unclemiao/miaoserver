package protocol

func MakeNotifyFashionListMsg(list []*Fashion) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyFashionList,
		Payload: &Envelope_S2CNotifyFashionList{S2CNotifyFashionList: &S2C_NotifyFashionListMsg{
			FashionList: list,
		}},
	}
}
