package protocol

func MakeLotteryItemDoneMsg(index int32, rate int32, award []*ItemAmount) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_LotteryItemDone,
		Payload: &Envelope_S2CLotteryItemDone{S2CLotteryItemDone: &S2C_LotteryItemDoneMsg{
			CfgIndex: index,
			Rate:     rate,
			Award:    award,
		}},
	}
}

func MakeLotteryEquipDoneMsg(rep *Envelope, count int32, equipList []*Item) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_LotteryEquipDone,
		Payload: &Envelope_S2CLotteryEquipDone{S2CLotteryEquipDone: &S2C_LotteryEquipDoneMsg{
			Count:     count,
			EquipList: equipList,
		}},
	}
}
