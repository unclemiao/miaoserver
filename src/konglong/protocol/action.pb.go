// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: action.proto

package protocol

import (
	fmt "fmt"
	proto "github.com/gogo/protobuf/proto"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

type SubActionType int32

const (
	SubActionType_Action_Of_Unknow      SubActionType = 0
	SubActionType_Action_Of_FastHangAdd SubActionType = 1
)

var SubActionType_name = map[int32]string{
	0: "Action_Of_Unknow",
	1: "Action_Of_FastHangAdd",
}

var SubActionType_value = map[string]int32{
	"Action_Of_Unknow":      0,
	"Action_Of_FastHangAdd": 1,
}

func (x SubActionType) String() string {
	return proto.EnumName(SubActionType_name, int32(x))
}

func (SubActionType) EnumDescriptor() ([]byte, []int) {
	return fileDescriptor_59885c909ad4dfd3, []int{0}
}

func init() {
	proto.RegisterEnum("goproject.src.konglong.protocol.SubActionType", SubActionType_name, SubActionType_value)
}

func init() { proto.RegisterFile("action.proto", fileDescriptor_59885c909ad4dfd3) }

var fileDescriptor_59885c909ad4dfd3 = []byte{
	// 154 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x49, 0x4c, 0x2e, 0xc9,
	0xcc, 0xcf, 0xd3, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x92, 0x4f, 0xcf, 0x2f, 0x28, 0xca, 0xcf,
	0x4a, 0x4d, 0x2e, 0xd1, 0x2b, 0x2e, 0x4a, 0xd6, 0xcb, 0xce, 0xcf, 0x4b, 0xcf, 0xc9, 0xcf, 0x4b,
	0x87, 0xc8, 0x26, 0xe7, 0xe7, 0x68, 0x39, 0x70, 0xf1, 0x06, 0x97, 0x26, 0x39, 0x82, 0xf5, 0x84,
	0x54, 0x16, 0xa4, 0x0a, 0x89, 0x70, 0x09, 0x40, 0x78, 0xf1, 0xfe, 0x69, 0xf1, 0xa1, 0x79, 0xd9,
	0x79, 0xf9, 0xe5, 0x02, 0x0c, 0x42, 0x92, 0x5c, 0xa2, 0x08, 0x51, 0xb7, 0xc4, 0xe2, 0x12, 0x8f,
	0xc4, 0xbc, 0x74, 0xc7, 0x94, 0x14, 0x01, 0x46, 0x27, 0xa5, 0x13, 0x8f, 0xe4, 0x18, 0x2f, 0x3c,
	0x92, 0x63, 0x7c, 0xf0, 0x48, 0x8e, 0x71, 0xc2, 0x63, 0x39, 0x86, 0x0b, 0x8f, 0xe5, 0x18, 0x6e,
	0x3c, 0x96, 0x63, 0x88, 0xe2, 0x80, 0xd9, 0x92, 0xc4, 0x06, 0x66, 0x19, 0x03, 0x02, 0x00, 0x00,
	0xff, 0xff, 0x84, 0xbe, 0xe6, 0xc0, 0x9d, 0x00, 0x00, 0x00,
}
