package protocol

func MakeViewHangAwardDoneMsg(req *Envelope, award []*ItemAmount, hangTime int64) *Envelope {
	return &Envelope{
		SeqId:   req.SeqId,
		MsgType: EnvelopeType_S2C_ViewHangAwardDone,
		Payload: &Envelope_S2CViewHangeAwardDone{S2CViewHangeAwardDone: &S2C_ViewHangAwardDoneMsg{
			ItemAmountList: award,
			HangTime:       hangTime,
		}},
	}
}

func MakeGetHangAwardDoneMsg(req *Envelope, award []*ItemAmount) *Envelope {
	return &Envelope{
		SeqId:   req.SeqId,
		MsgType: EnvelopeType_S2C_GetHangAwardDone,
		Payload: &Envelope_S2CGetHangeAwardDone{S2CGetHangeAwardDone: &S2C_GetHangAwardDoneMsg{
			ItemAmountList: award,
		}},
	}
}
