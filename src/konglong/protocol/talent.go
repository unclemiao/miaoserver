package protocol

func MakeNotifyRoleTalentMsg(dataList []*RoleTalent) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyRoleTalent,
		Payload: &Envelope_S2CNotifyRoleTalent{S2CNotifyRoleTalent: &S2C_NotifyRoleTalentMsg{RoleTalentList: dataList}},
	}
}

func MakeResetRoleTalentDoneMsg(rep *Envelope) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_ResetRoleTalentDone,
		Payload: &Envelope_S2CResetRoleTalentDone{S2CResetRoleTalentDone: &S2C_ResetRoleTalentDoneMsg{}},
	}
}
