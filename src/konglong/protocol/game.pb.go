// Code generated by protoc-gen-gogo. DO NOT EDIT.
// source: game.proto

package protocol

import (
	fmt "fmt"
	github_com_gogo_protobuf_proto "github.com/gogo/protobuf/proto"
	proto "github.com/gogo/protobuf/proto"
	io "io"
	math "math"
	math_bits "math/bits"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.GoGoProtoPackageIsVersion3 // please upgrade the proto package

// 随机获取一张游戏分享素材
type C2S_GetGameShareMaterialMsg struct {
}

func (m *C2S_GetGameShareMaterialMsg) Reset()         { *m = C2S_GetGameShareMaterialMsg{} }
func (m *C2S_GetGameShareMaterialMsg) String() string { return proto.CompactTextString(m) }
func (*C2S_GetGameShareMaterialMsg) ProtoMessage()    {}
func (*C2S_GetGameShareMaterialMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_38fc58335341d769, []int{0}
}
func (m *C2S_GetGameShareMaterialMsg) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *C2S_GetGameShareMaterialMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_C2S_GetGameShareMaterialMsg.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *C2S_GetGameShareMaterialMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_C2S_GetGameShareMaterialMsg.Merge(m, src)
}
func (m *C2S_GetGameShareMaterialMsg) XXX_Size() int {
	return m.Size()
}
func (m *C2S_GetGameShareMaterialMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_C2S_GetGameShareMaterialMsg.DiscardUnknown(m)
}

var xxx_messageInfo_C2S_GetGameShareMaterialMsg proto.InternalMessageInfo

// 随机获取一张游戏分享素材
type S2C_GetGameShareMaterialDoneMsg struct {
	ShareImage string `protobuf:"bytes,1,req,name=shareImage" json:"shareImage"`
	ShareTitle string `protobuf:"bytes,2,req,name=shareTitle" json:"shareTitle"`
}

func (m *S2C_GetGameShareMaterialDoneMsg) Reset()         { *m = S2C_GetGameShareMaterialDoneMsg{} }
func (m *S2C_GetGameShareMaterialDoneMsg) String() string { return proto.CompactTextString(m) }
func (*S2C_GetGameShareMaterialDoneMsg) ProtoMessage()    {}
func (*S2C_GetGameShareMaterialDoneMsg) Descriptor() ([]byte, []int) {
	return fileDescriptor_38fc58335341d769, []int{1}
}
func (m *S2C_GetGameShareMaterialDoneMsg) XXX_Unmarshal(b []byte) error {
	return m.Unmarshal(b)
}
func (m *S2C_GetGameShareMaterialDoneMsg) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	if deterministic {
		return xxx_messageInfo_S2C_GetGameShareMaterialDoneMsg.Marshal(b, m, deterministic)
	} else {
		b = b[:cap(b)]
		n, err := m.MarshalToSizedBuffer(b)
		if err != nil {
			return nil, err
		}
		return b[:n], nil
	}
}
func (m *S2C_GetGameShareMaterialDoneMsg) XXX_Merge(src proto.Message) {
	xxx_messageInfo_S2C_GetGameShareMaterialDoneMsg.Merge(m, src)
}
func (m *S2C_GetGameShareMaterialDoneMsg) XXX_Size() int {
	return m.Size()
}
func (m *S2C_GetGameShareMaterialDoneMsg) XXX_DiscardUnknown() {
	xxx_messageInfo_S2C_GetGameShareMaterialDoneMsg.DiscardUnknown(m)
}

var xxx_messageInfo_S2C_GetGameShareMaterialDoneMsg proto.InternalMessageInfo

func (m *S2C_GetGameShareMaterialDoneMsg) GetShareImage() string {
	if m != nil {
		return m.ShareImage
	}
	return ""
}

func (m *S2C_GetGameShareMaterialDoneMsg) GetShareTitle() string {
	if m != nil {
		return m.ShareTitle
	}
	return ""
}

func init() {
	proto.RegisterType((*C2S_GetGameShareMaterialMsg)(nil), "goproject.src.konglong.protocol.C2S_GetGameShareMaterialMsg")
	proto.RegisterType((*S2C_GetGameShareMaterialDoneMsg)(nil), "goproject.src.konglong.protocol.S2C_GetGameShareMaterialDoneMsg")
}

func init() { proto.RegisterFile("game.proto", fileDescriptor_38fc58335341d769) }

var fileDescriptor_38fc58335341d769 = []byte{
	// 184 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe2, 0xe2, 0x4a, 0x4f, 0xcc, 0x4d,
	0xd5, 0x2b, 0x28, 0xca, 0x2f, 0xc9, 0x17, 0x92, 0x4f, 0xcf, 0x2f, 0x28, 0xca, 0xcf, 0x4a, 0x4d,
	0x2e, 0xd1, 0x2b, 0x2e, 0x4a, 0xd6, 0xcb, 0xce, 0xcf, 0x4b, 0xcf, 0xc9, 0xcf, 0x4b, 0x87, 0xc8,
	0x26, 0xe7, 0xe7, 0x28, 0xc9, 0x72, 0x49, 0x3b, 0x1b, 0x05, 0xc7, 0xbb, 0xa7, 0x96, 0xb8, 0x27,
	0xe6, 0xa6, 0x06, 0x67, 0x24, 0x16, 0xa5, 0xfa, 0x26, 0x96, 0xa4, 0x16, 0x65, 0x26, 0xe6, 0xf8,
	0x16, 0xa7, 0x2b, 0xe5, 0x72, 0xc9, 0x07, 0x1b, 0x39, 0x63, 0x95, 0x76, 0xc9, 0xcf, 0x4b, 0xf5,
	0x2d, 0x4e, 0x17, 0x52, 0xe1, 0xe2, 0x2a, 0x06, 0x89, 0x7b, 0xe6, 0x26, 0xa6, 0xa7, 0x4a, 0x30,
	0x2a, 0x30, 0x69, 0x70, 0x3a, 0xb1, 0x9c, 0xb8, 0x27, 0xcf, 0x10, 0x84, 0x24, 0x0e, 0x57, 0x15,
	0x92, 0x59, 0x92, 0x93, 0x2a, 0xc1, 0x84, 0xa1, 0x0a, 0x2c, 0xee, 0xa4, 0x74, 0xe2, 0x91, 0x1c,
	0xe3, 0x85, 0x47, 0x72, 0x8c, 0x0f, 0x1e, 0xc9, 0x31, 0x4e, 0x78, 0x2c, 0xc7, 0x70, 0xe1, 0xb1,
	0x1c, 0xc3, 0x8d, 0xc7, 0x72, 0x0c, 0x51, 0x1c, 0x30, 0x17, 0x03, 0x02, 0x00, 0x00, 0xff, 0xff,
	0x8c, 0x97, 0x54, 0xec, 0xdf, 0x00, 0x00, 0x00,
}

func (m *C2S_GetGameShareMaterialMsg) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *C2S_GetGameShareMaterialMsg) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *C2S_GetGameShareMaterialMsg) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	return len(dAtA) - i, nil
}

func (m *S2C_GetGameShareMaterialDoneMsg) Marshal() (dAtA []byte, err error) {
	size := m.Size()
	dAtA = make([]byte, size)
	n, err := m.MarshalToSizedBuffer(dAtA[:size])
	if err != nil {
		return nil, err
	}
	return dAtA[:n], nil
}

func (m *S2C_GetGameShareMaterialDoneMsg) MarshalTo(dAtA []byte) (int, error) {
	size := m.Size()
	return m.MarshalToSizedBuffer(dAtA[:size])
}

func (m *S2C_GetGameShareMaterialDoneMsg) MarshalToSizedBuffer(dAtA []byte) (int, error) {
	i := len(dAtA)
	_ = i
	var l int
	_ = l
	i -= len(m.ShareTitle)
	copy(dAtA[i:], m.ShareTitle)
	i = encodeVarintGame(dAtA, i, uint64(len(m.ShareTitle)))
	i--
	dAtA[i] = 0x12
	i -= len(m.ShareImage)
	copy(dAtA[i:], m.ShareImage)
	i = encodeVarintGame(dAtA, i, uint64(len(m.ShareImage)))
	i--
	dAtA[i] = 0xa
	return len(dAtA) - i, nil
}

func encodeVarintGame(dAtA []byte, offset int, v uint64) int {
	offset -= sovGame(v)
	base := offset
	for v >= 1<<7 {
		dAtA[offset] = uint8(v&0x7f | 0x80)
		v >>= 7
		offset++
	}
	dAtA[offset] = uint8(v)
	return base
}
func (m *C2S_GetGameShareMaterialMsg) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	return n
}

func (m *S2C_GetGameShareMaterialDoneMsg) Size() (n int) {
	if m == nil {
		return 0
	}
	var l int
	_ = l
	l = len(m.ShareImage)
	n += 1 + l + sovGame(uint64(l))
	l = len(m.ShareTitle)
	n += 1 + l + sovGame(uint64(l))
	return n
}

func sovGame(x uint64) (n int) {
	return (math_bits.Len64(x|1) + 6) / 7
}
func sozGame(x uint64) (n int) {
	return sovGame(uint64((x << 1) ^ uint64((int64(x) >> 63))))
}
func (m *C2S_GetGameShareMaterialMsg) Unmarshal(dAtA []byte) error {
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowGame
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: C2S_GetGameShareMaterialMsg: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: C2S_GetGameShareMaterialMsg: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		default:
			iNdEx = preIndex
			skippy, err := skipGame(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthGame
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthGame
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func (m *S2C_GetGameShareMaterialDoneMsg) Unmarshal(dAtA []byte) error {
	var hasFields [1]uint64
	l := len(dAtA)
	iNdEx := 0
	for iNdEx < l {
		preIndex := iNdEx
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return ErrIntOverflowGame
			}
			if iNdEx >= l {
				return io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= uint64(b&0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		fieldNum := int32(wire >> 3)
		wireType := int(wire & 0x7)
		if wireType == 4 {
			return fmt.Errorf("proto: S2C_GetGameShareMaterialDoneMsg: wiretype end group for non-group")
		}
		if fieldNum <= 0 {
			return fmt.Errorf("proto: S2C_GetGameShareMaterialDoneMsg: illegal tag %d (wire type %d)", fieldNum, wire)
		}
		switch fieldNum {
		case 1:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ShareImage", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowGame
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthGame
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthGame
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ShareImage = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
			hasFields[0] |= uint64(0x00000001)
		case 2:
			if wireType != 2 {
				return fmt.Errorf("proto: wrong wireType = %d for field ShareTitle", wireType)
			}
			var stringLen uint64
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return ErrIntOverflowGame
				}
				if iNdEx >= l {
					return io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				stringLen |= uint64(b&0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			intStringLen := int(stringLen)
			if intStringLen < 0 {
				return ErrInvalidLengthGame
			}
			postIndex := iNdEx + intStringLen
			if postIndex < 0 {
				return ErrInvalidLengthGame
			}
			if postIndex > l {
				return io.ErrUnexpectedEOF
			}
			m.ShareTitle = string(dAtA[iNdEx:postIndex])
			iNdEx = postIndex
			hasFields[0] |= uint64(0x00000002)
		default:
			iNdEx = preIndex
			skippy, err := skipGame(dAtA[iNdEx:])
			if err != nil {
				return err
			}
			if skippy < 0 {
				return ErrInvalidLengthGame
			}
			if (iNdEx + skippy) < 0 {
				return ErrInvalidLengthGame
			}
			if (iNdEx + skippy) > l {
				return io.ErrUnexpectedEOF
			}
			iNdEx += skippy
		}
	}
	if hasFields[0]&uint64(0x00000001) == 0 {
		return github_com_gogo_protobuf_proto.NewRequiredNotSetError("shareImage")
	}
	if hasFields[0]&uint64(0x00000002) == 0 {
		return github_com_gogo_protobuf_proto.NewRequiredNotSetError("shareTitle")
	}

	if iNdEx > l {
		return io.ErrUnexpectedEOF
	}
	return nil
}
func skipGame(dAtA []byte) (n int, err error) {
	l := len(dAtA)
	iNdEx := 0
	depth := 0
	for iNdEx < l {
		var wire uint64
		for shift := uint(0); ; shift += 7 {
			if shift >= 64 {
				return 0, ErrIntOverflowGame
			}
			if iNdEx >= l {
				return 0, io.ErrUnexpectedEOF
			}
			b := dAtA[iNdEx]
			iNdEx++
			wire |= (uint64(b) & 0x7F) << shift
			if b < 0x80 {
				break
			}
		}
		wireType := int(wire & 0x7)
		switch wireType {
		case 0:
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowGame
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				iNdEx++
				if dAtA[iNdEx-1] < 0x80 {
					break
				}
			}
		case 1:
			iNdEx += 8
		case 2:
			var length int
			for shift := uint(0); ; shift += 7 {
				if shift >= 64 {
					return 0, ErrIntOverflowGame
				}
				if iNdEx >= l {
					return 0, io.ErrUnexpectedEOF
				}
				b := dAtA[iNdEx]
				iNdEx++
				length |= (int(b) & 0x7F) << shift
				if b < 0x80 {
					break
				}
			}
			if length < 0 {
				return 0, ErrInvalidLengthGame
			}
			iNdEx += length
		case 3:
			depth++
		case 4:
			if depth == 0 {
				return 0, ErrUnexpectedEndOfGroupGame
			}
			depth--
		case 5:
			iNdEx += 4
		default:
			return 0, fmt.Errorf("proto: illegal wireType %d", wireType)
		}
		if iNdEx < 0 {
			return 0, ErrInvalidLengthGame
		}
		if depth == 0 {
			return iNdEx, nil
		}
	}
	return 0, io.ErrUnexpectedEOF
}

var (
	ErrInvalidLengthGame        = fmt.Errorf("proto: negative length found during unmarshaling")
	ErrIntOverflowGame          = fmt.Errorf("proto: integer overflow")
	ErrUnexpectedEndOfGroupGame = fmt.Errorf("proto: unexpected end of group")
)
