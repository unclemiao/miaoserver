package protocol

func MakeNotifyBannerMsg(msg string) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyBanner,
		Payload: &Envelope_S2CNotifyBanner{S2CNotifyBanner: &S2C_NotifyBannerMsg{Msg: msg}},
	}
}
