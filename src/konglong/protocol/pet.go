package protocol

func MakeNotifyPetListMsg(petList []*Pet) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetList,
		Payload: &Envelope_S2CNotifyPetList{S2CNotifyPetList: &S2C_NotifyPetListMsg{
			PetList: petList,
		}},
	}
}

func MakeNotifyPetUpdateMsg(addList []*Pet, updateList []*Pet, delIdList []int64, action PetActionType) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetUpdate,
		Payload: &Envelope_S2CNotifyPetUpdate{S2CNotifyPetUpdate: &S2C_NotifyPetUpdateMsg{
			ActionType: action,
			AddList:    addList,
			UpdateList: updateList,
			DelIdList:  delIdList,
		}},
	}
}

func MakeViewPetDoneMsg(pet *Pet) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_ViewOtherPlayerPetDone,
		Payload: &Envelope_S2CViewOtherPlayerPetDone{S2CViewOtherPlayerPetDone: &S2C_ViewOtherPlayerPetDoneMsg{
			Pet: pet,
		}},
	}
}

func MakePetLevelUpMaxDoneMsg(rep *Envelope, petList []*Pet) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_PetLevelUpMaxDone,
		Payload: &Envelope_S2CPetLevelUpMaxDone{S2CPetLevelUpMaxDone: &S2C_PetLevelUpMaxDoneMsg{
			PetList: petList,
		}},
	}
}

func MakeTryVariationDoneMsg(rep *Envelope, petList []*Pet) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_TryVariationDone,
		Payload: &Envelope_S2CTryVariationDone{S2CTryVariationDone: &S2C_TryVariationDoneMsg{
			PetList: petList,
		}},
	}
}
