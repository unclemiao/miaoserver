package protocol

func MakeNotifyStubMsg(stub *StubInfo) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyStub,
		Payload: &Envelope_S2CNotifyStub{S2CNotifyStub: &S2C_NotifyStubMsg{StubInfo: stub}},
	}
}

func MakeNotifyStubUpdateMsg(stub *StubInfo) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyStubUpdate,
		Payload: &Envelope_S2CNotifyStubUpdate{S2CNotifyStubUpdate: &S2C_NotifyStubUpdateMsg{StubInfo: stub}},
	}
}

func MakeUnlockStubHelpDoneMsg(req *Envelope, stubIndex int32, helpIndex int32, kind StubKind) *Envelope {
	return &Envelope{
		SeqId:   req.SeqId,
		MsgType: EnvelopeType_S2C_UnlockStubHelpDone,
		Payload: &Envelope_S2CUnlockStubHelpDone{S2CUnlockStubHelpDone: &S2C_UnlockStubHelpDoneMsg{
			StubIndex: stubIndex,
			HelpIndex: helpIndex,
			Kind:      kind,
		}},
	}
}
