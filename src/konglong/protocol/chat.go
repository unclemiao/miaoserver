package protocol

func MakeNotifyChatMsg(chatType MessageType, msgList []*GameMessage) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyChat,
		Payload: &Envelope_S2CNotifyChat{S2CNotifyChat: &S2C_NotifyChatMsg{
			ChatMsg: msgList,
			MsgType: chatType,
		}},
	}
}
