package protocol

type FoodBookItf interface {
	GetForClient() *FoodBook
}

func MakeNotifyFoodBookMsg(foodBook FoodBookItf) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyFoodBook,
		Payload: &Envelope_S2CNotifyFoodBook{S2CNotifyFoodBook: &S2C_NotifyFoodBookMsg{
			FoodBook: foodBook.GetForClient(),
		}},
	}
}
