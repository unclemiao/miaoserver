package protocol

import (
	"goproject/engine/time_helper"
)

type RoleData interface {
	GetForClient() *Role
}

func MakeSignInDoneMsg(
	env *Envelope,
	role *Role,
	itemList []*Item,
	petList []*Pet,
	offSec int64,
	tf *TimeFeast,
	stubInfo *StubInfo,
) *Envelope {
	return &Envelope{
		SeqId:   env.SeqId,
		MsgType: EnvelopeType_S2C_SigninPlayerDone,
		Payload: &Envelope_S2CSigninPlayerDone{S2CSigninPlayerDone: &S2C_SigninPlayerDoneMsg{
			Role:       role,
			ItemList:   itemList,
			PetList:    petList,
			OffSec:     int32(offSec),
			ServerTime: time_helper.NowMill(),
			TimeFeast:  tf,
			Stub:       stubInfo,
		}},
	}
}

func MakeNotifyRoleUpdateMsg(role *Role) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_RoleUpdate,
		Payload: &Envelope_S2CNotifyRoleUpdate{S2CNotifyRoleUpdate: &S2C_NotifyRoleUpdateMsg{
			Role: role,
		}},
	}
}

func MakeResetRoleDoneMsg(rep *Envelope) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_ResetRoleDone,
		Payload: &Envelope_S2CResetRoleDone{S2CResetRoleDone: &S2C_ResetRoleDoneMsg{}},
	}
}

func MakeKickRoleMsg() *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_KickRole,
		Payload: &Envelope_S2CKickRole{S2CKickRole: &S2C_KickRoleMsg{}},
	}
}
