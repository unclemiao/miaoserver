package protocol

func MakeNotifyManualMsg(list []*Manual, fettersList []*Fetters) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyManual,
		Payload: &Envelope_S2CNotifyManual{S2CNotifyManual: &S2C_NotifyManualMsg{
			ManualList:  list,
			FettersList: fettersList,
		}},
	}
}

func MakeNotifyManualUpdateMsg(list []*Manual) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyManualUpdate,
		Payload: &Envelope_S2CNotifyManualUpdate{S2CNotifyManualUpdate: &S2C_NotifyManualUpdateMsg{
			ManualUpdateList: list,
		}},
	}
}

func MakeNotifyFettersMsg(list []*Fetters) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyFetters,
		Payload: &Envelope_S2CNotifyFetters{S2CNotifyFetters: &S2C_NotifyFettersMsg{
			FettersList: list,
		}},
	}
}

func MakeFettersActiveDoneMsg(rep *Envelope, f *Fetters) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_FettersActiveDone,
		Payload: &Envelope_S2CFettersActiveDone{S2CFettersActiveDone: &S2C_FettersActiveDoneMsg{
			Fetters: f,
		}},
	}
}
