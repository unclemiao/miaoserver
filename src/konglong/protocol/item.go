package protocol

func MakeNotifyItemList(kit KitType, itemList []*Item) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyItemList,
		Payload: &Envelope_S2CNotifyItemList{S2CNotifyItemList: &S2C_NotifyItemListMsg{
			KitType:  kit,
			ItemList: itemList,
		}},
	}
}

func MakeNotifyItemUpdate(addList []*Item, updateList []*Item, delIdList []int64, action ItemActionType) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyUpdateItemList,
		Payload: &Envelope_S2CNotifyItemUpdate{S2CNotifyItemUpdate: &S2C_NotifyItemUpdateMsg{
			ActionType: action,
			AddList:    addList,
			UpdateList: updateList,
			DelIdList:  delIdList,
		}},
	}
}

func MakeMoveItemDoneMsg(repMsg *Envelope, from int32, to int32, changeList []*Item) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_MoveItemDone,
		SeqId:   repMsg.SeqId,
		Payload: &Envelope_S2CMoveItemDone{S2CMoveItemDone: &S2C_MoveItemDoneMsg{
			FromGrid:       from,
			ToGrid:         to,
			ChangeItemList: changeList,
		}},
	}
}

func MakeMakeItemDoneMsg(repMsg *Envelope, state int32, itemCfgId int64) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_MakeItemDone,
		SeqId:   repMsg.SeqId,
		Payload: &Envelope_S2CMakeItemDone{S2CMakeItemDone: &S2C_MakeItemDoneMsg{
			State:     state,
			ItemCfgId: itemCfgId,
		}},
	}
}

func MakeEquipInheritDoneMsg(repMsg *Envelope, fromId int64, toId int64) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_EquipInheritDone,
		SeqId:   repMsg.SeqId,
		Payload: &Envelope_S2CEquipInheritDone{S2CEquipInheritDone: &S2C_EquipInheritDoneMsg{
			FromEquipId: fromId,
			ToEquipId:   toId,
		}},
	}
}
