package protocol

func MakeNotifyLotteryPetMsg(lotteryPet *LotteryPet) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyLotteryPet,
		Payload: &Envelope_S2CNotifyLotteryPet{S2CNotifyLotteryPet: &S2C_NotifyLotteryPetMsg{
			LotteryPet: lotteryPet,
		}},
	}
}

func MakeNotifyLotteryPetUpdateMsg(lotteryPet *LotteryPet) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyLotteryPetUpdate,
		Payload: &Envelope_S2CNotifyLotteryPetUpdate{S2CNotifyLotteryPetUpdate: &S2C_NotifyLotteryPetUpdateMsg{
			LotteryPet: lotteryPet,
		}},
	}
}

func MakeLotteryPetDoneMsg(rep *Envelope, count int32, petList []*Pet) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_LotteryPetDone,
		Payload: &Envelope_S2CLotteryPetDone{S2CLotteryPetDone: &S2C_LotteryPetDoneMsg{
			Count:   count,
			PetList: petList,
		}},
	}
}
