package protocol

type MonAndBossJsonData struct {
	Monster interface{} `json:"monster"`
	Boss    interface{} `json:"boss"`
}
