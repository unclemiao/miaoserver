package protocol

func MakeNotifyPetBreedMsg(breedList []*PetBreed) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetBreed,
		Payload: &Envelope_S2CNotifyPetBreed{S2CNotifyPetBreed: &S2C_NotifyPetBreedMsg{
			BreedList: breedList,
		}},
	}
}

func MakeNotifyPetBreedUpdateMsg(breedList []*PetBreed) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetBreedUpdate,
		Payload: &Envelope_S2CNotifyPetBreedUpdate{S2CNotifyPetBreedUpdate: &S2C_NotifyPetBreedUpdateMsg{
			BreedList: breedList,
		}},
	}
}

func MakeEndBreedDoneMsg(rep *Envelope, grid int32, pet *Pet) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_EndBreedDone,
		Payload: &Envelope_S2CEndBreedDone{S2CEndBreedDone: &S2C_EndBreedDoneMsg{
			Grid: grid,
			Pet:  pet,
		}},
	}
}
