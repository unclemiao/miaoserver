package protocol

func MakeNotifySuperEvolutionMsg(list []*PetSuperEvolutionLockInfo) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifySuperEvolution,
		Payload: &Envelope_S2CNotifySuperEvolution{S2CNotifySuperEvolution: &S2C_NotifySuperEvolutionMsg{
			InfoList: list,
		}},
	}
}
