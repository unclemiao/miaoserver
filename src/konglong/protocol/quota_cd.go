package protocol

func MakeNotifyQuotaCdMsg(quotaList []*Quota, cdList []*Cd) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyQuotaCd,
		Payload: &Envelope_S2CNotifyQuotaCd{S2CNotifyQuotaCd: &S2C_NotifyQuotaCdMsg{
			QuotaList: quotaList,
			CdList:    cdList,
		}},
	}
}

func MakeNotifyQuotaCdUpdateMsg(quotaList []*Quota, cdList []*Cd) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyQuotaCdUpdate,
		Payload: &Envelope_S2CNotifyQuotaCdUpdate{S2CNotifyQuotaCdUpdate: &S2C_NotifyQuotaCdUpdateMsg{
			QuotaList: quotaList,
			CdList:    cdList,
		}},
	}
}
