package protocol

func MakeNotifySubscribeMsg(list []*Subscribe) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifySubscribe,
		Payload: &Envelope_S2CNotifySubscribe{S2CNotifySubscribe: &S2C_NotifySubscribeMsg{
			SubscribeList: list,
		}},
	}
}
