package protocol

func MakeNotifyVentureMsg(venture *Venture) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyVenture,
		Payload: &Envelope_S2CNotifyVenture{S2CNotifyVenture: &S2C_NotifyVentureMsg{Venture: venture}},
	}
}

func MakeWinVenturePassDoneMsg(rep *Envelope, pass int32, amountList []*ItemAmount, buffList []int32) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_VentureWinPassDone,
		Payload: &Envelope_S2CVentureWinPassDone{S2CVentureWinPassDone: &S2C_VentureWinPassDoneMsg{
			Pass:           pass,
			ItemAmountList: amountList,
			BuffCfgIdList:  buffList,
		}},
	}
}

func MakeVentureRefreshBuffDoneMsg(rep *Envelope, buffIdList []int32) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_VentureRefreshBuffDone,
		Payload: &Envelope_S2CVentureRefreshBuffDone{S2CVentureRefreshBuffDone: &S2C_VentureRefreshBuffDoneMsg{
			BuffCfgIdList: buffIdList,
		}},
	}
}
