package protocol

func MakeGetFeastDoneMsg(rep *Envelope, list []*FeastDone) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_GetFeastDone,
		Payload: &Envelope_S2CGetFeastDone{S2CGetFeastDone: &S2C_GetFeastDoneMsg{
			FeastDoneList: list,
		}},
	}
}

func MakeNotifyFeastDoneMsg(list []*FeastDone) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_GetFeastDone,
		Payload: &Envelope_S2CGetFeastDone{S2CGetFeastDone: &S2C_GetFeastDoneMsg{
			FeastDoneList: list,
		}},
	}
}

func MakeFeastAwardDoneMsg(rep *Envelope, id int64) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_FeastAwardDone,
		Payload: &Envelope_S2CFeastAwardDone{S2CFeastAwardDone: &S2C_FeastAwardDoneMsg{
			Id: id,
		}},
	}
}

func MakeNotifyFeastMsg(fs []*Feast) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyFeast,
		Payload: &Envelope_S2CNotifyFeast{S2CNotifyFeast: &S2C_NotifyFeastMsg{
			FeastList: fs,
		}},
	}
}

func MakeNotifyFeastPetKingInfoMsg(myRank *FeastPetKingRankInfo, rankInfo []*FeastPetKingRankInfo, feastInfo *FeastPetKing) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyFeastPetKingInfo,
		Payload: &Envelope_S2CNotifyFeastPetKingInfo{S2CNotifyFeastPetKingInfo: &S2C_NotifyFeastPetKingInfoMsg{
			MyRankInfo:   myRank,
			RankInfoList: rankInfo,
			FeastInfo:    feastInfo,
		}},
	}
}

func MakeGetFeastPetKingRankListDoneMsg(rep *Envelope, rankInfo []*FeastPetKingRankInfo) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_GetFeastPetKingRankListDone,
		Payload: &Envelope_S2CGetFeastPetKingRankListDone{S2CGetFeastPetKingRankListDone: &S2C_GetFeastPetKingRankListDoneMsg{
			RankInfoList: rankInfo,
		}},
	}
}

func MakeWinPetKingDoneMsg(rep *Envelope,
	atker *FeastPetKingRankInfo, atkerScore int32,
	def *FeastPetKingRankInfo, defScore int32,
	rank int32,
) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_WinPetKingDone,
		Payload: &Envelope_S2CWinPetKingDone{S2CWinPetKingDone: &S2C_WinPetKingDoneMsg{
			Atker:              atker,
			AtkerChangeScore:   atkerScore,
			Definer:            def,
			DefinerChangeScore: defScore,
			Rank:               rank,
		}},
	}
}

func MakeLosePetKingDoneMsg(rep *Envelope,
	atker *FeastPetKingRankInfo, atkerScore int32,
	def *FeastPetKingRankInfo, defScore int32,
	rank int32,
) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_LosePetKingDone,
		Payload: &Envelope_S2CLosePetKingDone{S2CLosePetKingDone: &S2C_LosePetKingDoneMsg{
			Atker:              atker,
			AtkerChangeScore:   atkerScore,
			Definer:            def,
			DefinerChangeScore: defScore,
			Rank:               rank,
		}},
	}
}

func MakeGetPetKingFightRecordListDoneMsg(rep *Envelope, recordList []*PetKingFightRecord) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_GetPetKingFightRecordListDone,
		Payload: &Envelope_S2CGetPetKingFightRecordListDone{S2CGetPetKingFightRecordListDone: &S2C_GetPetKingFightRecordListDoneMsg{
			RecordList: recordList,
		}},
	}
}

func MakePetKingAwardDoneMsg(rep *Envelope) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_PetKingAwardDone,
		Payload: &Envelope_S2CPetKingAwardDone{S2CPetKingAwardDone: &S2C_PetKingAwardDoneMsg{}},
	}
}
