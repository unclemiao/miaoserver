package protocol

func MakeNotifyMallCfgMsg(mallCfgList []*MallCfg) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyMallCfg,
		Payload: &Envelope_S2CNotifyMallCfg{S2CNotifyMallCfg: &S2C_NotifyMallCfgMsg{
			MallCfgList: mallCfgList,
		}},
	}
}

func MakeNotifyRoleMallMsg(roleMallList []*RoleMall) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyRoleMall,
		Payload: &Envelope_S2CNotifyRoleMall{S2CNotifyRoleMall: &S2C_NotifyRoleMallMsg{
			RoleMallList: roleMallList,
		}},
	}
}
