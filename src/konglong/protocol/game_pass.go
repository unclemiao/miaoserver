package protocol

func MakeWinPassBossDoneMsg(pass int32, award []*ItemAmount, extAward bool) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_WinPassBossDone,
		Payload: &Envelope_S2CWinPassBossDone{S2CWinPassBossDone: &S2C_WinPassBossDoneMsg{
			Pass:           pass,
			ItemAmountList: award,
			ExtAward:       extAward,
		}},
	}
}

func MakeNotifyNewbieBossPassMsg(passList []*NewbieBossPass) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyNewbieBossPass,
		Payload: &Envelope_S2CNotifyNewbieBossPass{S2CNotifyNewbieBossPass: &S2C_NotifyNewbieBossPassMsg{
			PassList: passList,
		}},
	}
}

func MakeNotifyNewbieBossPassUpdateMsg(passList []*NewbieBossPass) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyNewbieBossPassUpdate,
		Payload: &Envelope_S2CNotifyNewbieBossPassUpdate{S2CNotifyNewbieBossPassUpdate: &S2C_NotifyNewbieBossPassUpdateMsg{
			UpdateList: passList,
		}},
	}
}

func MakeGetGamePassExtAwardDoneMsg(rep *Envelope, itemAmountList []*ItemAmount) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_GetGamePassExtAwardDone,
		SeqId:   rep.SeqId,
		Payload: &Envelope_S2CGetGamePassExtAwardDone{S2CGetGamePassExtAwardDone: &S2C_GetGamePassExtAwardDoneMsg{
			ItemAmountList: itemAmountList,
		}},
	}
}
