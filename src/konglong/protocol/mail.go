package protocol

func MakeNotifyMailListMsg(mailList []*Mail) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyMailList,
		Payload: &Envelope_S2CNotifyMailList{S2CNotifyMailList: &S2C_NotifyMailListMsg{
			MailList: mailList,
		}},
	}
}

func MakeAwardMailDoneMsg(name string) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_AwardMailDone,
		Payload: &Envelope_S2CAwardMailDone{S2CAwardMailDone: &S2C_AwardMailDoneMsg{
			Name: name,
		}},
	}
}

func MakeReadMailDoneMsg(name string) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_ReadMailDone,
		Payload: &Envelope_S2CReadMailDone{S2CReadMailDone: &S2C_ReadMailDoneMsg{
			Name: name,
		}},
	}
}

func MakeDelMailDoneMsg(name string, nameList []string) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_DelMailDone,
		Payload: &Envelope_S2CDelMailDone{S2CDelMailDone: &S2C_DelMailDoneMsg{
			Name:         name,
			DelAllOfName: nameList,
		}},
	}
}
