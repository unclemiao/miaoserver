package protocol

func MakeNotifyDailyQuestMsg(questList []*DailyQuest, questAward *DailyQuestAward) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyDailyQuest,
		Payload: &Envelope_S2CNotifyDailyQuest{S2CNotifyDailyQuest: &S2C_NotifyDailyQuestMsg{
			QuestList:  questList,
			QuestAward: questAward,
		}},
	}
}

func MakeNotifyDailyQuestUpdateMsg(questList []*DailyQuest) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyDailyQuestUpdate,
		Payload: &Envelope_S2CNotifyDailyQuestUpdate{S2CNotifyDailyQuestUpdate: &S2C_NotifyDailyQuestUpdateMsg{
			QuestUpdateList: questList,
		}},
	}
}

func MakeNotifyDailyQuestAwardUpdateMsg(questAward *DailyQuestAward) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyDailyQuestAwardUpdate,
		Payload: &Envelope_S2CNotifyDailyQuestAwardUpdate{S2CNotifyDailyQuestAwardUpdate: &S2C_NotifyDailyQuestAwardUpdateMsg{
			QuestAward: questAward,
		}},
	}
}
