package protocol

func MakeNotifyPetFosterMsg(fosterList []*PetFoster) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetFoster,
		Payload: &Envelope_S2CNotifyPetFoster{S2CNotifyPetFoster: &S2C_NotifyPetFosterMsg{
			FosterList: fosterList,
		}},
	}
}

func MakeNotifyPetFosterUpdateMsg(fosterList []*PetFoster) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyPetFosterUpdate,
		Payload: &Envelope_S2CNotifyPetFosterUpdate{S2CNotifyPetFosterUpdate: &S2C_NotifyPetFosterUpdateMsg{
			FosterList: fosterList,
		}},
	}
}

func MakeEndFosterDoneMsg(rep *Envelope, petList []*Pet) *Envelope {
	return &Envelope{
		SeqId:   rep.SeqId,
		MsgType: EnvelopeType_S2C_EndFosterDone,
		Payload: &Envelope_S2CEndFosterDone{S2CEndFosterDone: &S2C_EndFosterDoneMsg{
			PetList: petList,
		}},
	}
}
