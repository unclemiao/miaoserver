package protocol

func MakeNotifyAdSdkSlotsCfgMsg(msgList []*AdSdkSlotsCfg) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyAdSdkSlotsCfg,
		Payload: &Envelope_S2CNotifyAdSdkSlotsCfg{S2CNotifyAdSdkSlotsCfg: &S2C_NotifyAdSdkSlotsCfgMsg{
			SlotList: msgList,
		}},
	}
}

func MakeNotifyAdSdkAwardMsg(msgList []*AdSdkAward) *Envelope {
	return &Envelope{
		MsgType: EnvelopeType_S2C_NotifyAdSdkAward,
		Payload: &Envelope_S2CNotifyAdSdkAward{S2CNotifyAdSdkAward: &S2C_NotifyAdSdkAwardMsg{
			AwardList: msgList,
		}},
	}
}
