package test

import (
	"fmt"
	"testing"

	"goproject/src/konglong/data"
	"goproject/src/konglong/logic"
	"goproject/src/konglong/logic/gConst"
	"goproject/src/konglong/protocol"
)

func init() {
	LoadConfig()
}

func Benchmark_TestSignIn(b *testing.B) {
	for i := 0; i < b.N; i++ {
		session := &data.Session{
			ProtoType: data.PROTO_TYPE_NET,
			MsgBuff:   &data.MsgBuff{},
			WriteChan: make(chan *protocol.Envelope, gConst.SESSION_WRITE_CHAN_BUFF_SIZE),
			StopChan:  make(chan chan struct{}),
			IsStop:    true,
		}
		args := &protocol.Envelope{
			MsgType: protocol.EnvelopeType_C2S_SigninPlayer,
			Error:   nil,
			Payload: &protocol.Envelope_C2SSigninPlayer{
				C2SSigninPlayer: &protocol.C2S_SigninPlayerMsg{
					AccountId: fmt.Sprint(i),
					Snode:     999,
					From:      "",
					Young:     false,
					Due:       0,
					Token:     "",
				},
			},
		}
		logic.OnEvent(&data.Context{
			MsgId:    int32(protocol.EnvelopeType_C2S_SigninPlayer),
			EventMsg: args,
			Session:  session,
		})

	}

}
