package test

import (
	"sync"

	"goproject/src/konglong/logic"
)

func init() {
	LoadConfig()
}

var once sync.Once

func LoadConfig() {
	once.Do(func() {
		logic.InitServer("../")
		logic.LoadServer()
		logic.LoadConfig()
		logic.AfterConfig()

	})
}
