package test

import (
	"fmt"
	"testing"

	"goproject/engine/sql"
	"goproject/src/konglong/logic/gConst"

	. "github.com/smartystreets/goconvey/convey"
)

var gSql *sql.Sql

func TestLoadSql(t *testing.T) {
	Convey("TestLoadSql ShouldBeTrue when gSql == nil", t, func() {
		So(gSql, ShouldNotBeNil)
	})
}

func TestInsertSql(t *testing.T) {
	//fmt.Println("TestInsertSql")
	//
	//err := LoadCnf()
	//Convey("TestLoadCnf ShouldBeTrue when cnf.Unmarshal.err == nil", t, func() {
	//	So(err, ShouldBeNil)
	//})
	//fmt.Printf("APP %+v", gApp.Sql)
	//
	//gSql := sql.LoadSql(&sql.Dsn{
	//	Driver:       gApp.Sql.Driver,
	//	Host:         gApp.Sql.Host,
	//	Port:         gApp.Sql.Port,
	//	Username:     gApp.Sql.Root,
	//	Password:     gApp.Sql.Pass,
	//	DatabaseName: gApp.Sql.Name,
	//})
	//
	//Convey("TestLoadSql ShouldBeTrue when gSql == nil", t, func() {
	//	So(gSql, ShouldNotBeNil)
	//})
	//
	//err = gSql.Ping()
	//Convey("TestSqlPing ShouldBeTrue when err == nil", t, func() {
	//	So(err, ShouldBeNil)
	//})
	//
	//gSql.Begin().Clean(123)
	//defer func() {
	//	err := recover()
	//	if err != nil {
	//		gSql.Rollback()
	//	} else {
	//		gSql.Commit()
	//	}
	//}()
	//
	//gSql.MustExcel(gConst.SQL_ID_INSERT, 2, 1)
}

func TestIdManagerUpdateSql(t *testing.T) {
	fmt.Println("TestIdManagerUpdateSql")
	err := LoadCnf()
	Convey("TestLoadCnf ShouldBeTrue when cnf.Unmarshal.err == nil", t, func() {
		So(err, ShouldBeNil)
	})
	fmt.Printf("APP %+v\n", gApp.Sql)
	gSql := sql.LoadSql(&sql.Dsn{
		Driver:       gApp.Sql.Driver,
		Host:         gApp.Sql.Host,
		Port:         gApp.Sql.Port,
		Username:     gApp.Sql.Root,
		Password:     gApp.Sql.Pass,
		DatabaseName: gApp.Sql.Name,
	})

	//gSql.Begin()

	//defer func() {
	//	err := recover()
	//	if err != nil {
	//		gSql.Rollback()
	//	} else {
	//		gSql.Commit()
	//	}
	//}()
	err = gSql.Ping()
	fmt.Println("EEEEEE ", err)
	res, err2 := gSql.Excel(gConst.SQL_UPDATE_ITEM_AMOUNT, 2, 1)
	n, err3 := res.RowsAffected()
	fmt.Println("dddddd ", n, err2, err3)
	//gSql.MustExcel(gConst.SQL_ID_UPDATE, 2, 4)
	//gSql.MustExcel(gConst.SQL_ID_UPDATE, 2, 5)

}
