package logic

import (
	"fmt"
	"sync"
	"time"

	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/beego/v2/server/web/filter/cors"

	"goproject/engine/logs"
	"goproject/src/admin/models"
)

var gServerMgr *ServerManager
var AppFlag *models.AppFlag

type ServerManager struct {
	Server   *models.Server
	Lock     sync.RWMutex
	LastTime time.Time
}

func NewServerManager() *ServerManager {
	gServerMgr = &ServerManager{}
	return gServerMgr
}

func GetServerManager() *ServerManager {
	return gServerMgr
}

func (mgr *ServerManager) Init() {
	LogInit()
	mgr.initFlag()
	if beego.BConfig.RunMode == "dev" {
		beego.BConfig.WebConfig.DirectoryIndex = true
		beego.BConfig.WebConfig.StaticDir["/swagger"] = "swagger"
	}
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		// 允许访问所有源
		AllowAllOrigins: true,
		// 可选参数"GET", "POST", "PUT", "DELETE", "OPTIONS" (*为所有)
		AllowMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		// 指的是允许的Header的种类
		AllowHeaders: []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		// 公开的HTTP标头列表
		ExposeHeaders: []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		// 如果设置，则允许共享身份验证凭据，例如 cookie
		AllowCredentials: true,
	}))

	logs.Info("server init finish")
}

func (mgr *ServerManager) Star() {

	server := &models.Server{Id: 1}
	mgr.Server = server
	time.AfterFunc(3*time.Second, func() {
		PingToCenter()
	})
	// mgr.DoCmd(&models.StarCmd{
	// 	Id:         0,
	// 	Sid:        999,
	// 	Platform:   "test",
	// 	Channel:    "weiying",
	// 	CenterHost: "http://127.0.0.1:8080",
	// 	AdminHost:  "http://127.0.0.1:8081",
	// 	IsMaster:   "true",
	// })
	mgr.heartbeat()
	beego.Run(fmt.Sprintf(":%d", AppFlag.HttpPort))

}

func (mgr *ServerManager) Stop() {

}

func (mgr *ServerManager) heartbeat() {
	time.AfterFunc(time.Minute, mgr.heartbeat)
	mgr.Lock.Lock()
	defer mgr.Lock.Unlock()
	now := time.Now()
	if mgr.LastTime.Day() != now.Day() {
		LogInit()
	}
	mgr.LastTime = now
}
