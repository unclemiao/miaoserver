package logic

import (
	"path/filepath"

	"goproject/engine/assert"
	"goproject/engine/excel"
	"goproject/engine/logs"
	"goproject/src/admin/models"
)

var Items []*models.Item

func init() {
	excel.XlsConfigInit(filepath.Join("./", "common/excel"))
}

func LoadExcel() {
	logs.Infof("======================= LoadItemCfg =======================")
	Items = Items[:0]
	var citems []*models.Item
	err := excel.XlsConfigLoad("XL_PROPS", &citems, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)
	Items = append(Items, citems...)

	citems = citems[:0]
	err = excel.XlsConfigLoad("XL_EQUIP", &citems, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)
	Items = append(Items, citems...)

	citems = citems[:0]
	err = excel.XlsConfigLoad("XL_FOODITEM", &citems, func(state *excel.XlsConfigReaderState) {})
	assert.Assert(err == nil, err)
	Items = append(Items, citems...)

}

func GetItems() []*models.Item {
	return Items
}
