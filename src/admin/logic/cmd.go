package logic

import (
	"fmt"
	"os"
	"os/exec"
	"runtime"

	"goproject/engine/logs"
	"goproject/src/admin/models"
)

func (mgr *ServerManager) DoCmd(starCmd *models.StarCmd) error {
	pathStr, _ := os.Getwd()
	pathStr = pathStr + "/script/"
	fileName := ""
	if runtime.GOOS == "windows" {
		fileName = "start.bat"
	} else {
		if AppFlag.IsDocker {
			fileName = "start.sh"
		} else {
			fileName = "linux_start.sh"
		}
	}
	pathStr = pathStr + fileName

	logs.Infof("Do Script:%s ", pathStr)
	processName := fmt.Sprintf("game%03d", starCmd.Id)
	cmd := exec.Command(pathStr, starCmd.DockerName, processName,
		fmt.Sprintf("%d", starCmd.Id),
		fmt.Sprintf("%d", starCmd.Sid),
		starCmd.Platform,
		starCmd.Channel,
		starCmd.CenterHost,
		starCmd.AdminHost,
		starCmd.IsMaster,
		fmt.Sprintf("%d", starCmd.Port),
		fmt.Sprintf("/home/mini-game/logs/game%03d", starCmd.Id),
	)

	err := cmd.Run()

	if err != nil {
		logs.Error(err.Error())
		return err
	}

	logs.Infof("DoCmd: %+v", starCmd)
	output, _ := cmd.Output()
	logs.Infof("DoCmd: %s", string(output))
	return nil
}
