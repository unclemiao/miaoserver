package logic

import (
	"goproject/engine/logs"
)

func LogInit() {
	logs.InitLog("./logs")
	LoadExcel()
}
