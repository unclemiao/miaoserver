package logic

import (
	"flag"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/src/admin/models"
)

func (mgr *ServerManager) initFlag() {
	AppFlag = new(models.AppFlag)
	flag.Int64Var(&AppFlag.HttpPort, "httpPort", beego.AppConfig.DefaultInt64("httpport", 8081), "http端口")
	flag.StringVar(&AppFlag.Token, "token", beego.AppConfig.DefaultString("token", "*"), "token")
	flag.StringVar(&AppFlag.CenterHost, "centerHost", beego.AppConfig.DefaultString("centerhost", "http://127.0.0.1:8080"), "中心服url")
	flag.StringVar(&AppFlag.Host, "host", beego.AppConfig.DefaultString("host", "http://192.168.10.178:8081"), "本机url")
	flag.BoolVar(&AppFlag.IsDocker, "isDocker", beego.AppConfig.DefaultBool("isDocker", false), "是否使用docker启动game服务")
	flag.Parse()
	logs.Infof("initFlag:%+v", AppFlag)
}
