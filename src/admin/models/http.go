package models

type Response struct {
	Code int32
	Err  string
	Data interface{}
}

type PingResponse struct {
	Code int32
	Err  string
	Data interface{}
}
