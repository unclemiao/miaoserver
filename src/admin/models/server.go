package models

import "time"

type Server struct {
	Id      int64     `orm:"pk"`
	Version string    `orm:"size(128)"`
	Token   string    `orm:"size(512)"`
	Created time.Time `orm:"auto_now_add;type(datetime)"`
	Updated time.Time `orm:"auto_now;type(datetime)"`
}

type StarCmd struct {
	Id         int64
	Sid        int64
	Platform   string
	Channel    string
	CenterHost string
	AdminHost  string
	Port       int64
	IsMaster   string
	DockerName string
}
