package models

type Item struct {
	Id   int32  `json:"index"`
	Name string `json:"name"`
}
