package models

type AppFlag struct {
	HttpPort   int64
	CenterHost string
	Token      string
	Host       string
	IsDocker   bool
}
