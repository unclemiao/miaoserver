package main

import (
	"goproject/src/admin/logic"
	_ "goproject/src/admin/routers"
)

func main() {
	server := logic.NewServerManager()
	server.Init()
	server.Star()
}
