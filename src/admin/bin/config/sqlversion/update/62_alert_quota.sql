ALTER TABLE `quota` ADD `paral_ad_sdk` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '副本看视频使用次数' AFTER `paral`;
ALTER TABLE `quota` ADD `paral_gem` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '副本宝石使用次数' AFTER `paral_ad_sdk`;
