ALTER TABLE `quota` ADD `lottery_item_gem_add` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '转盘宝石增加次数的次数' AFTER `lottery_item_add`;
ALTER TABLE `quota` ADD `gem_fast_pass` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '宝石加速使用次数' AFTER `ad_fast_pass`;
ALTER TABLE `quota` ADD `venture_gem_add_fast` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '宝石增加免费探险扫荡使用次数的次数' AFTER `venture_add_fast`;
ALTER TABLE `quota` ADD `duel_gem` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '竞技场看广告增加次数的次数' AFTER `duel_ad_sdk`;
