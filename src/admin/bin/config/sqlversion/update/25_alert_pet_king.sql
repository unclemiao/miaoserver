ALTER TABLE `pet_king_record`
    CHANGE COLUMN `atker_score` `atker_score` INT NOT NULL COMMENT '攻击方积分变化' ,
    CHANGE COLUMN `defer_score` `defer_score` INT NOT NULL COMMENT '防守方积分变化' ;
