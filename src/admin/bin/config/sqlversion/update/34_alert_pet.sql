ALTER TABLE `pet` ADD `name`  varchar(128) NOT NULL COMMENT '名字' AFTER `cfg_id`;
ALTER TABLE `pet` ADD `try_variation` tinyint(1)  NOT NULL COMMENT '是否尝试变异' AFTER `variation`;

ALTER TABLE `quota` ADD `try_variation`  int(11) UNSIGNED NOT NULL COMMENT '尝试变异次数' AFTER `drop_box`;

