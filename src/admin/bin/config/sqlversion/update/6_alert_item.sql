ALTER TABLE `item` ADD `hp_max_base` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '血量上限基础值' AFTER `kit`;
ALTER TABLE `item` ADD `atk_base` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '攻击基础值' AFTER `hp_max_base`;
ALTER TABLE `item` ADD `def_base` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '防御基础值' AFTER `atk_base`;
ALTER TABLE `item` ADD `grade` int(11) UNSIGNED NOT NULL DEFAULT 0 COMMENT '品阶' AFTER `def_base`;