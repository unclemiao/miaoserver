ALTER TABLE `invite` ADD `awarded_amount` int(11) UNSIGNED NOT NULL COMMENT '奖励领取数量' AFTER `invite_name`;
ALTER TABLE `invite` ADD `time` bigint(20) UNSIGNED NOT NULL COMMENT '邀请的时间' AFTER `awarded_amount`;

