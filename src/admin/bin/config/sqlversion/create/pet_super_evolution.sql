CREATE TABLE `pet_super_evolution` (
   `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
   `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
   `cfg_id` int(11) UNSIGNED NOT NULL COMMENT '配置id',
   `progress` int(11) NOT NULL COMMENT '进度',
   PRIMARY KEY (`id`),
   KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
