-- 邮件
create table mail(
    `name`	varchar(60) not null COMMENT '邮件名称'
,	`title`	varchar(15) not null COMMENT '标题'
,	`award`	varchar(255) not null COMMENT '奖励内容'
,   `gift_code`     varchar(255) not null COMMENT '礼包码'
,	`each`	Json NOT NULL COMMENT '是否给全服发 [0] 表示全服邮件， [] 表示个人邮件， [1,2,3]表示发给某个服的玩家'
,	`time`	timestamp not null COMMENT '发送时间'
,   `time_out`    timestamp not null COMMENT '到期时间'
,	`body`	varchar(255) not null COMMENT '内容'
,	primary key (`name`)
,   KEY `INDEX_TIME` (`time_out`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- 邮件接收者
create table mailee(
    `role_id`       bigint(20) UNSIGNED NOT NULL COMMENT '玩家 id'
,	`name`	        varchar(60) not null COMMENT '邮件名称'
,	`title`	        varchar(15) not null COMMENT '标题'
,	`award`	        varchar(255) not null COMMENT '奖励内容'
,	`time`	        timestamp not null COMMENT '发送时间'
,	`body`	        varchar(255) not null COMMENT '内容'
,   `gift_code`     varchar(255) not null COMMENT '礼包码'
,   `is_del`        tinyint(1) UNSIGNED NOT NULL COMMENT '是否已经删除'
,   `is_read`       tinyint(1) UNSIGNED NOT NULL COMMENT '是否已读'
,   `time_out`      timestamp not null COMMENT '到期时间'
,	`award_time`    timestamp COMMENT '领奖时间'
,	primary key (`role_id`, `name`)
,   KEY `INDEX_TIME` (`time`) USING BTREE
,   KEY `INDEX_ROLE_TIME` (`role_id`, `time_out`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

