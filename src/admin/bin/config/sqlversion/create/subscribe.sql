CREATE TABLE `subscribe` (
 `id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `account_name`  varchar(256) NOT NULL COMMENT '账号'
,    `kind` varchar(256)  NOT NULL COMMENT '订阅类型ID'
,    `state` int(11) UNSIGNED NOT NULL COMMENT '订阅状态 0 = 关， 1=开'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

