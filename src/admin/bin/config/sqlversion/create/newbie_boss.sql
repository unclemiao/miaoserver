CREATE TABLE `newbie_boss` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `cfg_index` int(11) UNSIGNED NOT NULL COMMENT '关卡序号',
    `awarded` tinyint(1) UNSIGNED NOT NULL COMMENT '是否领取奖励',
    `ext_awarded` tinyint(1) UNSIGNED NOT NULL COMMENT '是否领取额外奖励',
    `passed` tinyint(1) UNSIGNED NOT NULL COMMENT '是否通关',
    PRIMARY KEY (`id`),
    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


