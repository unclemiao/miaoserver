CREATE TABLE `duel` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id',
    `rank` int(11) UNSIGNED NOT NULL COMMENT '排名',
    `name`  varchar(128) NOT NULL COMMENT '名字',
    `is_robot`  tinyint(1) NOT NULL COMMENT '是否机器人',
    `avatar`  varchar(1024) NOT NULL COMMENT '头像链接',
    `fight_info_json`  varchar(128) NOT NULL COMMENT '赛选出要战斗的排名列表',
    `duel_time` timestamp NOT NULL COMMENT '最后竞技时间',
    PRIMARY KEY (`role_id`),
    KEY `INDEX_ROLE_ID` (`rank` asc) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `duel_record` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `winner_id` bigint(20) UNSIGNED NOT NULL COMMENT '攻击方id',
    `winner_name` varchar(128)  NOT NULL COMMENT '攻击方名字',
    `winner_avatar` varchar(1024) NOT NULL COMMENT '攻击方头像',
    `winner_is_robot`  tinyint(1) NOT NULL COMMENT '攻击方是否机器人',
    `loser_id` bigint(20) UNSIGNED NOT NULL COMMENT '防守方id',
    `loser_name` varchar(128) NOT NULL COMMENT '防守方名字',
    `loser_avatar` varchar(1024) NOT NULL COMMENT '防守方头像',
    `loser_is_robot`  tinyint(1) NOT NULL COMMENT '防守方是否机器人',
    `rank` int(11) UNSIGNED NOT NULL COMMENT '攻击方排名变化',
    `is_win` tinyint(1) NOT NULL DEFAULT 0 COMMENT '攻击方是否胜利',
    `duel_time` timestamp NOT NULL COMMENT '竞技时间',
    PRIMARY KEY (`id`),
    KEY `INDEX_WINNER_ID` (`winner_id`) USING BTREE,
    KEY `INDEX_LOSER_ID` (`loser_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
