CREATE TABLE `rank` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `name`  varchar(128) NOT NULL COMMENT '名字',
    `avatar`  varchar(1024) NOT NULL COMMENT '头像链接',
    `game_pass` int(11) UNSIGNED NOT NULL COMMENT '关卡号',
    `game_pass_time` bigint(20) UNSIGNED NOT NULL COMMENT '通关时间',
    `yesterday_rank` int(11) UNSIGNED NOT NULL COMMENT '昨日排名',
    `yesterday_before_rank` int(11) UNSIGNED NOT NULL COMMENT '前天排名',
    `awarded` tinyint(1)  NOT NULL COMMENT '是否已领取',
    PRIMARY KEY (`role_id`),
    KEY `INDEX_GAME_PASS_AND_TIME` (`game_pass` desc, `game_pass_time`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
