CREATE TABLE `daily_quest_award` (
   `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
   `score` int(11) UNSIGNED NOT NULL COMMENT '当前积分',
   `awarded_str` varchar(1024) NOT NULL default '[]' COMMENT '已领取的奖励信息',
   PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
