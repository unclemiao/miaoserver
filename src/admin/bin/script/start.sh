#!/bin/sh
echo star ${1} ${2} ${3} ${4} ${5} ${6} ${7} ${8} ${9} ${10}

docker run -itd --name ${2} -e id=${3} -e sid=${4} -e platform=${5} -e channel=${6} -e centerHost=${7} -e adminHost=${8} -e isMaster=${9} -e port=${10} -p ${10}:${10} ${1}

