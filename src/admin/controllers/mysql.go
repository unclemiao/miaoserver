package controllers

import (
	"net"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/admin/models"
)

type MysqlController struct {
	beego.Controller
}

func (node *MysqlController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()

	ip, _ := net.ResolveIPAddr("ip", "weixin-mysql-kl2.qcinterfacet.com")
	resp.Data = ip.String()
}
