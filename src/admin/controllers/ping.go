package controllers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/admin/models"
)

type PingController struct {
	beego.Controller
}

func (node *PingController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
}
