package controllers

import (
	"encoding/json"
	"fmt"

	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/engine/util"
	"goproject/src/admin/logic"
	"goproject/src/admin/models"
)

type CmdController struct {
	beego.Controller
}

func (node *CmdController) Post() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	data := node.GetString("data", "")
	starCmd := &models.StarCmd{}
	err := json.Unmarshal([]byte(data), starCmd)
	if err != nil {
		resp.Code = 501
		resp.Err = err.Error()
		return
	}

	due := node.GetString("due")
	token := node.GetString("weiyingToken")
	md5 := util.Md5(fmt.Sprintf("-_-!!!%d%s%s%s%s", starCmd.Sid, starCmd.Platform, starCmd.Channel, due, "b7561aa298be84e4c3c2a7901e47c350"))
	if token != md5 {
		resp.Code = 501
		resp.Err = "token err"
		logs.Error(resp.Err)
		return
	}

	err = logic.GetServerManager().DoCmd(starCmd)
	if err != nil {
		resp.Code = 501
		resp.Err = err.Error()
		return
	}
}

func (node *CmdController) Get() {
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	starCmd := &models.StarCmd{
		Id:         996,
		Sid:        996,
		Platform:   "test",
		Channel:    "weiying",
		CenterHost: "https://dev-center.qcinterface.com",
		AdminHost:  "",
		Port:       30996,
		IsMaster:   "true",
	}
	err := logic.GetServerManager().DoCmd(starCmd)
	if err != nil {
		resp.Code = 501
		resp.Err = err.Error()
		return
	}
}

func (node *CmdController) Delete() {

}
