package controllers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/engine/logs"
	"goproject/src/admin/logic"
	"goproject/src/admin/models"
)

type ItemController struct {
	beego.Controller
}

func (node *ItemController) Get() {
	logs.Infof("Get................")
	resp := &models.Response{Code: 200}
	defer func() {
		node.Data["json"] = resp
		_ = node.ServeJSON()
	}()
	itemCfgList := logic.GetItems()
	resp.Data = itemCfgList
}
