package routers

import (
	beego "github.com/beego/beego/v2/server/web"

	"goproject/src/admin/controllers"
)

func init() {
	ns := beego.NewNamespace("/*/server",
		beego.NSRouter("/cmd",
			&controllers.CmdController{},
		),
	)
	beego.AddNamespace(ns)

	beego.Router("/*/ping",
		&controllers.PingController{},
	)
	beego.Router("/*/item",
		&controllers.ItemController{},
	)
	beego.Router("/*/mysqlIp",
		&controllers.MysqlController{},
	)
}
