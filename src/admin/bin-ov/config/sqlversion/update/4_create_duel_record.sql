CREATE TABLE IF NOT EXISTS `duel_record`  (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `winner_id` bigint(20) UNSIGNED NOT NULL COMMENT '胜利方id',
    `winner_name` varchar(128)  NOT NULL COMMENT '胜利方名字',
    `winner_avatar` varchar(128)  NOT NULL COMMENT '胜利方头像',
    `loser_id` bigint(20) UNSIGNED NOT NULL COMMENT '失败方id',
    `loser_name` varchar(128)  NOT NULL COMMENT '失败方名字',
    `loser_avatar` varchar(128)  NOT NULL COMMENT '失败方头像',
    `rank` int(11) UNSIGNED NOT NULL COMMENT '胜利方排名变化',
    `duel_time` timestamp NOT NULL COMMENT '竞技时间',
    PRIMARY KEY (`id`),
    KEY `INDEX_WINNER_ID` (`winner_id`) USING BTREE,
    KEY `INDEX_LOSER_ID` (`loser_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
