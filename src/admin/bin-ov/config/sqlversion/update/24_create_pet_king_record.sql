CREATE TABLE `pet_king_record` (
   `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
   `atker_id` bigint(20) UNSIGNED NOT NULL COMMENT '攻击方id',
   `atker_name` varchar(128)  NOT NULL COMMENT '攻击方名字',
   `atker_avatar` varchar(128) NOT NULL COMMENT '攻击方头像',
   `atker_score` int(11) UNSIGNED NOT NULL COMMENT '攻击方积分变化',
   `defer_id` bigint(20) UNSIGNED NOT NULL COMMENT '防守方id',
   `defer_name` varchar(128) NOT NULL COMMENT '防守方名字',
   `defer_avatar` varchar(128) NOT NULL COMMENT '防守方头像',
   `defer_score` int(11) UNSIGNED NOT NULL COMMENT '防守方积分变化',
   `create_time` timestamp NOT NULL COMMENT '竞技时间',
   PRIMARY KEY (`id`),
   KEY `INDEX_ATKER_ID` (`atker_id`) USING BTREE,
   KEY `INDEX_DEFER_ID` (`defer_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;