CREATE TABLE `food_book` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `level`  int(11) UNSIGNED NOT NULL COMMENT '等级',
    `exp` bigint(20) UNSIGNED NOT NULL COMMENT '熟练度',
    `high_end_time` int(11) UNSIGNED NOT NULL COMMENT '高级购买开启',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

