CREATE TABLE `server_node` (
  `sid` bigint(20) UNSIGNED NOT NULL COMMENT '服务器id',
  `is_merge` Tinyint(1) UNSIGNED NOT NULL COMMENT '是否被合服',
  `begin_time` bigint(20) UNSIGNED NOT NULL COMMENT '开服时间',
  `last_minute` bigint(20) UNSIGNED NOT NULL COMMENT '最后一次心跳分钟数',
  `sql_version` bigint(20) UNSIGNED NOT NULL COMMENT '数据库版本',
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;