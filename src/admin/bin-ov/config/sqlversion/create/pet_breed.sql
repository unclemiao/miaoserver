CREATE TABLE `pet_breed` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '角色id'
,   `grid` int(11) UNSIGNED NOT NULL COMMENT '格子'
,   `pet1` bigint(20) UNSIGNED NOT NULL COMMENT '第一个宠物位id'
,   `pet2` bigint(20) UNSIGNED NOT NULL COMMENT '第二个宠物位id'
,   `breed_end_time` bigint(20) UNSIGNED NOT NULL COMMENT '繁殖完成时间'
,   `breed_fast_num` bigint(20) UNSIGNED NOT NULL COMMENT '繁殖加速次数'
,   PRIMARY KEY (`role_id`, `grid`)
,   KEY `INDEX_ROLE` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;