CREATE TABLE `id_manager` (
  `id_type` int(11) UNSIGNED NOT NULL,
  `generate_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;