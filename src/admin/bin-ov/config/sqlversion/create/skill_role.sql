CREATE TABLE `role_talent` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id'
,    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id'
,    `cfg_id` int(11) UNSIGNED NOT NULL COMMENT '天赋配置id'
,    `lv` int(11) UNSIGNED NOT NULL COMMENT '等级'
,    `total_point`  int(11) UNSIGNED NOT NULL COMMENT '总消耗点数'
,    PRIMARY KEY (`id`)
,    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
