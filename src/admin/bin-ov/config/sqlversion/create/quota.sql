CREATE TABLE `quota` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `lottery_item` int(11) UNSIGNED NOT NULL COMMENT '转盘使用次数',
    `lottery_item_add` int(11) UNSIGNED NOT NULL COMMENT '转盘增加次数的次数',
    `paral` int(11) UNSIGNED NOT NULL COMMENT '副本使用次数',
    `duel` int(11) UNSIGNED NOT NULL COMMENT '竞技场使用次数',
    `game_pass_ext_award` int(11) UNSIGNED NOT NULL COMMENT '关卡额外奖励次数',
    `game_drop_diamond` int(11) UNSIGNED NOT NULL COMMENT '游戏掉落钻石宝箱次数',
    `game_drop_pet` int(11) UNSIGNED NOT NULL COMMENT '游戏掉落孵化蛋次数',
    `free_fast_pass` int(11) UNSIGNED NOT NULL COMMENT '免费加速使用次数',
    `ad_fast_pass` int(11) UNSIGNED NOT NULL COMMENT '看视频加速使用次数',
    `daily_free_lottery_pet` int(11) UNSIGNED NOT NULL COMMENT '每日免费孵化次数',
    `free_refresh_duel` int(11) UNSIGNED NOT NULL COMMENT '每日免费刷新竞技场次数',
    `lottery_equip` int(11) UNSIGNED NOT NULL COMMENT '装备抽卡次数',
    `venture_free_fast` int(11) UNSIGNED NOT NULL COMMENT '免费探险扫荡使用次数',
    `venture_add_fast` int(11) UNSIGNED NOT NULL COMMENT '增加免费探险扫荡使用次数的次数',
    `pet_king_free_refresh` int(11) UNSIGNED NOT NULL COMMENT '恐龙争霸赛免费刷新次数',
    `pet_king_free_fight` int(11) UNSIGNED NOT NULL COMMENT '恐龙争霸赛免费战斗次数',
    `pet_king_ad_fight` int(11) UNSIGNED NOT NULL COMMENT '每日恐龙争霸赛看广告战斗次数',
    `drop_box` int(11) UNSIGNED NOT NULL COMMENT '宝箱掉落次数',
    `try_variation` int(11) UNSIGNED NOT NULL COMMENT '尝试变异次数',
    `daily_ad_sdk` int(11) UNSIGNED NOT NULL COMMENT '当日观看视频次数',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

CREATE TABLE `cd` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `lottery_item_quota_cd` int(11) UNSIGNED NOT NULL COMMENT '上次转盘使用时间',
    `ad_fast_pass` int(11) UNSIGNED NOT NULL COMMENT '上次转盘使用时间',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;