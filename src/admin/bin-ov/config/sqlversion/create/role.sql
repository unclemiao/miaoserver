CREATE TABLE `role` (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
  `account_id`  varchar(128) NOT NULL COMMENT '账号id',
  `channel`  varchar(128) NOT NULL COMMENT '渠道',
  `sid` int(11) UNSIGNED NOT NULL COMMENT '服务器号',
  `tag`  int(11) UNSIGNED NOT NULL COMMENT '标识',
  `name`  varchar(128) NOT NULL COMMENT '名字',
  `avatar`  varchar(1024) NOT NULL COMMENT '头像链接',

  `level` int(11) UNSIGNED NOT NULL COMMENT '等级',
  `exp` bigint(20) UNSIGNED NOT NULL COMMENT '经验',
  `game_pass` int(11) UNSIGNED NOT NULL COMMENT '关卡数',
  `last_game_pass_ext_award` int(11) UNSIGNED NOT NULL COMMENT '最后一次领取额外奖励的关卡数',
  `lottery_item_rate` int(11) NOT NULL COMMENT '转盘倍数',
  `seven_sign` int(11) UNSIGNED NOT NULL COMMENT '七日签到',
  `next_seven_sign_time` int(11) UNSIGNED NOT NULL COMMENT '下次可签到时间',
  `system_awarded`  bigint(20) UNSIGNED NOT NULL COMMENT '系统开启奖励',
  `help_index_unlock_info` int(11) UNSIGNED NOT NULL COMMENT '助阵解锁信息，低位运算 0b00001101',
  `horse` bigint(20) UNSIGNED NOT NULL COMMENT '坐骑id',
  `model_cfg_id` int(11) UNSIGNED NOT NULL COMMENT '模型配置id',
  `model_name` varchar(64) NOT NULL COMMENT '模型名字',

  `private_proto` tinyint(1) UNSIGNED NOT NULL COMMENT '是否同意隐私协议',
  `newbie` int(11) UNSIGNED NOT NULL COMMENT '新手引导步骤',
  `audio` tinyint(1) UNSIGNED NOT NULL COMMENT '声音开关',
  `shake` tinyint(1) UNSIGNED NOT NULL COMMENT '震动开关',
  `language`  varchar(32) NOT NULL COMMENT '语言',
  PRIMARY KEY (`role_id`),
  KEY `INDEX_ACCOUNTID_AND_SID` (`account_id`, `sid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

