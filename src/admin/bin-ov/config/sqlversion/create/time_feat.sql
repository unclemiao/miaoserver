CREATE TABLE `time_feast` (
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `new_role` timestamp NOT NULL COMMENT '创角时间',
    `ip`  varchar(128) NOT NULL COMMENT '上次登录ip',
    `last_sec` int(11) UNSIGNED NOT NULL COMMENT '上次更新时刻 秒',
    `young`  int(11) UNSIGNED NOT NULL COMMENT '防沉迷在线时间  秒',
    `day_sec`  int(11) UNSIGNED NOT NULL COMMENT '今日在线时间 秒',
    `week_sec` int(11) UNSIGNED NOT NULL COMMENT '本周在线时间 秒',
    `last_week_sec` int(11) UNSIGNED NOT NULL COMMENT '上周在线时间 秒',
    `week_day` int(11) UNSIGNED NOT NULL COMMENT '本周在线天数 秒',
    `sec` int(11) UNSIGNED NOT NULL COMMENT '总在线时间 秒',
    `last_view_hang_sec` int(11) UNSIGNED NOT NULL COMMENT '上次查看在线奖励时间 秒',
    `last_hang_awarded_sec` int(11) UNSIGNED NOT NULL COMMENT '上次领取挂机奖励时间 秒',
    `award_str` Json NOT NULL COMMENT '在线奖励信息',
    `signin` int(11) UNSIGNED NOT NULL COMMENT '登录天数',
    `next_drop_box_time` bigint(20) UNSIGNED NOT NULL COMMENT '下次宝箱掉落时间',
    PRIMARY KEY (`role_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

