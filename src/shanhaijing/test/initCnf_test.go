package test

import (
	"testing"

	"goproject/src/shanhaijing/base"

	. "github.com/smartystreets/goconvey/convey"
)

type Server struct {
	Id      uint64
	Name    string
	Channel string
	SignKey string
	Host    string
}

type Sql struct {
	Driver string
	Host   string
	Port   int32
	Root   string
	Pass   string
	Name   string
}
type App struct {
	Server *Server
	Sql    *Sql
}

var gApp *App

func LoadCnf() error {
	base.InitServerCnf("../config/")
	cnf := base.GetAppCfg()
	err := cnf.Unmarshal(&gApp)
	return err
}

func TestAppCnf(t *testing.T) {
	err := LoadCnf()

	Convey("TestAppCnf ShouldBeTrue when cnf.Unmarshal.err != nil", t, func() {
		So(err, ShouldBeNil)
	})
	Convey("TestAppCnf ShouldBeTrue when app.Server != nil", t, func() {
		So(gApp.Server, ShouldNotBeNil)
	})
	Convey("TestAppCnf ShouldBeTrue when app.Sql != nil", t, func() {
		So(gApp.Sql, ShouldNotBeNil)
	})

}
