package test

import (
	"testing"
	"time"

	"goproject/engine/assert"
	"goproject/engine/sql"

	. "github.com/smartystreets/goconvey/convey"
)

func TestLoadSql(t *testing.T) {
	err := LoadCnf()
	Convey("TestLoadCnf ShouldBeTrue when cnf.Unmarshal.err == nil", t, func() {
		So(err, ShouldBeNil)
	})

	gSql := sql.LoadSql(&sql.Dsn{
		Driver:       gApp.Sql.Driver,
		Host:         gApp.Sql.Host,
		Port:         gApp.Sql.Port,
		Username:     gApp.Sql.Root,
		Password:     gApp.Sql.Pass,
		DatabaseName: gApp.Sql.Name,
	})

	Convey("TestLoadSql ShouldBeTrue when gSql == nil", t, func() {
		So(gSql, ShouldNotBeNil)
	})
}

func TestInsertSql(t *testing.T) {
	err := LoadCnf()
	Convey("TestLoadCnf ShouldBeTrue when cnf.Unmarshal.err == nil", t, func() {
		So(err, ShouldBeNil)
	})

	gSql := sql.LoadSql(&sql.Dsn{
		Driver:       gApp.Sql.Driver,
		Host:         gApp.Sql.Host,
		Port:         gApp.Sql.Port,
		Username:     gApp.Sql.Root,
		Password:     gApp.Sql.Pass,
		DatabaseName: gApp.Sql.Name,
	})
	Convey("TestLoadSql ShouldBeTrue when gSql == nil", t, func() {
		So(gSql, ShouldNotBeNil)
	})

	err = gSql.Ping()
	Convey("TestSqlPing ShouldBeTrue when err == nil", t, func() {
		So(err, ShouldBeNil)
	})

	gSql.Begin().Clean(123)
	defer func() {
		err := recover()
		if err != nil {
			gSql.Rollback()
		} else {
			gSql.Commit()
		}
	}()

	gSql.MustExcel("INSERT INTO server_start_count (type_id, count) VALUES (?, ?)", time.Now().Unix(), 20)
	gSql.SetRollBackEventFunc(func(id int64) {
	})
	assert.Assert(false, "assert")
}
