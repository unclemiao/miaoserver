package data

import (
	"sync"

	"github.com/gorilla/websocket"
	"goproject/src/shanhaijing/protocol"
)

type MsgNode struct {
	Msg      *protocol.Envelope
	NextNode *MsgNode
}

type MsgBuff struct {
	Head *MsgNode
	Last *MsgNode
}

type Session struct {
	Conn       *websocket.Conn
	Id         int64
	Ip         string
	AccountId  string
	RoleId     int64
	From       string
	Young      bool
	Sid        int32
	SignInTime int64

	// 消息缓冲
	MsgBuff *MsgBuff
	// 消息发送管道
	WriteChan chan *protocol.Envelope
	StopChan  chan chan struct{}
	IsStop    bool

	Lock sync.RWMutex
}
