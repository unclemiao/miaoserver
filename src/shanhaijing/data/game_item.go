package data

import (
	"goproject/src/shanhaijing/protocol"
)

type AllItem struct {
	RoleId        int64
	ItemMap       map[int64]*Item
	ItemMapForKit map[protocol.KitType]map[int64]*Item
	OwnerItems    map[int64]map[int64]*Item
}

type Item struct {
	Id      int64            `db:"id"`       // id
	RoleId  int64            `db:"role_id"`  // 角色id
	OwnerId int64            `db:"owner_id"` // 拥有者id
	Cid     int32            `db:"cid"`      // 配置id
	N       int32            `db:"n"`        // 数量
	Grid    int32            `db:"grid"`     // 格子号
	Kit     protocol.KitType `db:"kit"`      // 栏位
}

type CItem struct {
	Cid         int32  `json:"cid"`
	Kind        int32  `json:"kind"`
	Part        int32  `json:"part"`
	Lv          int32  `json:"lv"`
	N9          int32  `json:"n9"`
	AttrStr     string `json:"attr_str"`
	ApplyTryStr string `json:"applytry_str"`
}

func (item *Item) GetForClient() *protocol.Role {
	return nil
}
