package data

type IdData struct {
	IdType   int32 `db:"id_type"`
	IdAmount int64 `db:"id_amount"`
}
