package data

import (
	"fmt"
	"goproject/src/shanhaijing/protocol"
)

type RoleChangeType int32

type Role struct {
	RoleId    int64  `db:"role_id"`    // 玩家id
	Name      string `db:"name"`       // 玩家名字
	Channel   string `db:"channel"`    // 玩家渠道
	AccountId string `db:"account_id"` // 账号id
	Sid       int32  `db:"sid"`        // 服务器id
	Tag       int32  `db:"tag"`        // 禁号标识

	Cid int32 `db:"cid"`
	Lv  int32 `db:"lv"`
	Exp int32 `db:"exp"`
}

func (role *Role) GetForClient() *protocol.Role {
	return &protocol.Role{
		RoleId: fmt.Sprint(role.RoleId),
		Name:   role.Name,
		Lv:     role.Lv,
		Tag:    role.Tag,
	}
}
