package data

type ServerNode struct {
	Sid        int32  `db:"sid"`
	IsMerge    bool   `db:"is_merge"`
	BeginTime  int64  `db:"begin_time"`
	LastMinute int64  `db:"last_minute"`
	DataVer    int32  `db:"data_ver"`
	Host       string `db:"host"`
}

type Server struct {
	Node *ServerNode

	// 节点数据
	NodeList     []*ServerNode
	NodeIdList   []int32
	NodeRolesMap map[int32]int32 // 所有节点的玩家数

	// 动态数据----------
	IsStarted   bool
	Stop        chan struct{}
	ReceiveChan chan *EventArgs
	StopChan    chan struct{}
}
