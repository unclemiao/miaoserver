package data

type EventTimer struct {
	EventBase
	Func func()
}
