package data

type EventMinutely struct {
	EventBase
	NowSec int64
}

type EventHourly struct {
	EventBase
	Hour int64
}

type EventDaily struct {
	EventBase
	Week      int32
	IsNewWeek bool
}

type EventSqlRollBack struct {
	EventBase
	RoleId int64
}
