package data

type AppCnf struct {
	Server *ServerCnf
	Sql    *SqlCnf
}

type ServerCnf struct {
	Id      int32
	Name    string
	Channel string
	SignKey string
	Host    string
	Young   bool
}

type SqlCnf struct {
	Driver string
	Host   string
	Port   int32
	Root   string
	Pass   string
	Name   string
}
