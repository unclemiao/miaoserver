package data

import "goproject/src/shanhaijing/protocol"

type EventItf interface {
	IsNetEvent() bool
	GetEnvMsg() *protocol.Envelope
}

type EventBase struct {
}

func (m *EventBase) IsNetEvent() bool {
	return false
}

func (m *EventBase) GetEnvMsg() *protocol.Envelope {
	return nil
}

type EventFunc struct {
	MsgId int32
	Order int32
	Func  func(msg *EventArgs)
}

type EventArgs struct {
	MsgId     int32    // 事件枚举
	EventMsg  EventItf // 消息参数结构
	Session   *Session // 链接
	Sid       int32
	AccountId string // 账号id
	RoleId    int64  // 角色id
	Stop      bool   // 是否停止后续事件
	BeginTime int64  // 事件开始时间(毫秒)
	Now       int64  // 事件发生时间(毫秒)
	EndTime   int64  // 事件结束时间(毫秒)

}
