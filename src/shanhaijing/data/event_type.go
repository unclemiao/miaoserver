package data

type EventType int32

const (
	EVENT_MSGID_INVALID EventType = iota

	// ---------- 服务器基础事件 --------------
	// 数据库回滚
	EVENT_MSGID_SQL_ROLLBACK
	// 加载完所有配置后事件
	EVENT_MSGID_AFTER_CONFIG
	// 定时器事件
	EVENT_MSGID_TIMER
	// 每分钟事件
	EVENT_MSGID_MINUTELY
	// 每小时事件
	EVENT_MSGID_HOURLY
	// 每天事件
	EVENT_MSGID_DAILY

	// --------- 角色事件 -------------------
	EVENT_MSGID_OFFLINE_ROLE
	EVENT_MSGID_ONLINE_ROLE
)
