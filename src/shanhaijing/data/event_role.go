package data

type EventOfflineRole struct {
	EventBase
	RoleId  int64
	SignOut bool
}

type EventOnlineRole struct {
	EventBase
	RoleId int64
	SignIn bool
}
