package Logic

import (
	"encoding/json"
	"log"
	"sync"
	"time"

	"github.com/gorilla/websocket"
)

type NetMsg struct {
	MsgId   int32  `json:"msg_id"`
	PayLoad string `json:"pay_load"`
}

type Conn struct {
	WsConn      *websocket.Conn
	AccountName string
	PlayerId    string
	Lock        sync.RWMutex
}

func OnConn(conn *Conn) {
	go Ping(conn)
}

func Ping(conn *Conn) {
	Send(conn, &NetMsg{
		MsgId:   123,
		PayLoad: "ping",
	})
	time.AfterFunc(time.Second*10, func() {
		Ping(conn)
	})
}

func OnData(conn *Conn, accountName string, datas []byte) {
	netMsg := &NetMsg{}
	err := json.Unmarshal(datas, netMsg)
	if err != nil {
		log.Println(err)
		return
	}
	log.Println(netMsg)
}

func OnClose(conn *Conn) {
	conn.WsConn.Close()
}

func Send(conn *Conn, message *NetMsg) {

	payload, err := json.Marshal(message)
	if err == nil {
		err = conn.WsConn.WriteMessage(websocket.BinaryMessage, payload)
		if err != nil {
			log.Println(err)
		}
	} else {
		log.Println(err)
	}
}
