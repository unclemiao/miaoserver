package ClientNet

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"goproject/src/shanhaijing/client/Logic"

	"github.com/gorilla/websocket"
)

func RandString(len int) string {
	r := rand.New(rand.NewSource(time.Now().Unix()))
	bytes := make([]byte, len)
	for i := 0; i < len; i++ {
		b := r.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return string(bytes)
}

func Star() {

	log.SetFlags(log.Lshortfile | log.Ldate)
	host := fmt.Sprintf("ws://%s?token=", "127.0.0.1:30993/ws")
	Dail(fmt.Sprintf("%s%s", host, "123"), "name") //ready服
}

func Dail(urlstr string, accountName string) {
	var dialer *websocket.Dialer

	conn, _, err := dialer.Dial(urlstr, nil)

	if err != nil {
		log.Printf("Dial[%s] err:%v\n", urlstr, err)
		return
	}
	ws := &Logic.Conn{
		WsConn:      conn,
		AccountName: accountName,
	}
	defer func() {
		Logic.OnClose(ws)
	}()
	Logic.OnConn(ws)
	for {
		_, message, err := conn.ReadMessage()
		if err != nil {
			log.Println("read err:", err)
			return
		}
		Logic.OnData(ws, accountName, message)
	}

}
