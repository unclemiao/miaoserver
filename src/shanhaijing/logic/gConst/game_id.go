package gConst

const (
	ID_TYPE_INVALID int32 = iota
	ID_TYPE_ROLE
)

const (
	SQL_ID_SELECT_ALL = "SELECT * FROM `id_manager`"
	SQL_ID_SELECT     = "SELECT * FROM `id_manager` WHERE `id_type` = ?"
	SQL_ID_INSERT     = "INSERT INTO `id_manager` (`id_type`, `id_amount`) VALUES (?, ?);"
	SQL_ID_UPDATE     = "UPDATE `id_manager` SET `id_amount` = ? WHERE `id_type` = ?;"
)
