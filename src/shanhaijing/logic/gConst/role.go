package gConst

import (
	"goproject/src/shanhaijing/data"
)

const (
	CACHE_KEY_ROLE        = "cache_role_%d"
	ROLE_OFFLINE_TIME_OUT = 300
)

// 角色标记
const (
	ROLE_TAG_INVALID = 0
	ROLE_TAG_DEL     = 0b001
	ROLE_TAG_LOCK    = 0b010
)

const (
	ROLE_CHANGE_TYPE_INVALID data.RoleChangeType = iota
	ROLE_CHANGE_TYPE_ROLE
	ROLE_CHANGE_TYPE_ROLE_ATTR
)

const (
	SQL_ROLE_SELECT_ALL        = "SELECT * FROM `role`;"
	SQL_ROLE_SELECT_BY_ACCOUNT = "SELECT * FROM `role` where `account_id` = ? and `snode` = ?;"
	SQL_ROLE_SELECT            = "SELECT * FROM `role` where `role_id` = ?"
	SQL_ROLE_INSERT            = "INSERT INTO `role` (`role_id`, " +
		"`account_id`, " +
		"`channel`, " +
		"`sid`," +
		"`tag`," +
		"name," +
		"cid," +
		"lv," +
		"exp" +
		") " +
		"VALUES (?,?,?,?,?,?,?,?);"
)
