package logic

import (
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/gConst"
	"time"
)

type IdManager struct {
	IdMap           map[int32]*data.IdData
	CacheCleanTimer time.Ticker
}

func init() {
	gIdMgr = &IdManager{
		IdMap: make(map[int32]*data.IdData),
	}
}

func (mgr *IdManager) LoadConfig() {

}

func (mgr *IdManager) Stop() {
	mgr.CacheCleanTimer.Stop()
}

func (mgr *IdManager) idBy(idType int32) *data.IdData {

	idData := mgr.IdMap[idType]

	if idData == nil {
		var idDataList []*data.IdData

		gSql.MustSelect(&idDataList, gConst.SQL_ID_SELECT, idType)

		if len(idDataList) > 0 {
			idData = idDataList[0]
		}
	}
	return idData
}

func (mgr *IdManager) GenerateId(sid int32, idType int32) int64 {
	idData := mgr.idBy(idType)

	if idData == nil {
		// 初始化数据
		gSql.MustExcel(gConst.SQL_ID_INSERT, idType, 1)
		idData = &data.IdData{
			IdType:   idType,
			IdAmount: 1,
		}
	} else {
		// 更新
		gSql.MustExcel(gConst.SQL_ID_UPDATE, idType, idData.IdAmount+1)
		idData.IdAmount++
	}

	mgr.IdMap[idType] = idData

	id := idData.IdAmount*1000 + int64(sid)

	return id
}
