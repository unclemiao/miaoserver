package logic

import (
	"runtime/debug"
	"sync"

	"github.com/gorilla/websocket"

	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/gConst"
	"goproject/src/shanhaijing/protocol"
)

type SessionManager struct {
	SessionMap    map[*websocket.Conn]*data.Session
	SessionLock   sync.RWMutex
	SessionNumber int64
}

func init() {
	gSessionMgr = &SessionManager{
		SessionMap: make(map[*websocket.Conn]*data.Session),
	}
}

func (mgr *SessionManager) NewSession(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	mgr.SessionNumber++
	session := &data.Session{
		Conn:      conn,
		Id:        mgr.SessionNumber,
		Ip:        conn.RemoteAddr().String(),
		MsgBuff:   &data.MsgBuff{},
		WriteChan: make(chan *protocol.Envelope, gConst.SESSION_WRITE_CHAN_BUFF_SIZE),
		StopChan:  make(chan chan struct{}),
	}
	mgr.SessionMap[conn] = session
	go mgr.run(session)
	return session
}

func (mgr *SessionManager) DelSession(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	session := mgr.SessionMap[conn]
	delete(mgr.SessionMap, conn)
	mgr.Stop(session)
	return session
}

func (mgr *SessionManager) SessionBy(conn *websocket.Conn) *data.Session {
	mgr.SessionLock.RLock()
	defer mgr.SessionLock.RUnlock()
	return mgr.SessionMap[conn]
}

func (mgr *SessionManager) run(s *data.Session) {
	s.IsStop = false
	defer func() {
		err := recover()
		if err != nil {
			logs.Error("session onSend err: ", err)
			logs.Error(string(debug.Stack()))
			go mgr.run(s)
		}
	}()
	for {
		select {
		case msg := <-s.WriteChan:
			if s == nil || s.Conn == nil || s.IsStop {
				mgr.Stop(s)
			}
			mgr.writeMsg(s, msg)

		case stopFinished := <-s.StopChan:
			s.IsStop = true
			if s.Conn != nil {
				err := s.Conn.Close()
				if err != nil {
					logs.Error("session conn close err: ", err)
				}
			}
			s.Conn = nil
			close(s.WriteChan)
			close(s.StopChan)
			stopFinished <- struct{}{}
			return
		}
	}
}

func (mgr *SessionManager) Stop(s *data.Session) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	if !s.IsStop {
		stopDone := make(chan struct{}, 1)
		s.StopChan <- stopDone
		<-stopDone
	}
}

func (mgr *SessionManager) writeMsg(s *data.Session, msg *protocol.Envelope) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	if s.IsStop {
		return
	}

	err := s.Conn.WriteJSON(msg)
	if err != nil {
		logs.Error("json Marshal err: ", err)
		return
	}
}

func (mgr *SessionManager) Send(s *data.Session, msg *protocol.Envelope) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	nowNode := &data.MsgNode{Msg: msg}
	if s.MsgBuff.Head == nil {
		s.MsgBuff.Head = nowNode
		s.MsgBuff.Last = nowNode
	} else {
		last := s.MsgBuff.Last
		last.NextNode = &data.MsgNode{Msg: msg}
		s.MsgBuff.Last = last.NextNode
	}
}

func (mgr *SessionManager) Flush(s *data.Session) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	for s.MsgBuff.Head != nil {
		nowNode := s.MsgBuff.Head
		s.WriteChan <- nowNode.Msg
		s.MsgBuff.Head = nowNode.NextNode
		nowNode.NextNode = nil
	}
}

func (mgr *SessionManager) Clean(s *data.Session) {
	s.Lock.Lock()
	defer s.Lock.Unlock()
	s.MsgBuff.Head = nil
	s.MsgBuff.Last = nil
}

func (mgr *SessionManager) Range(f func(session *data.Session) bool) {
	mgr.SessionLock.Lock()
	defer mgr.SessionLock.Unlock()
	for _, s := range mgr.SessionMap {
		if !f(s) {
			return
		}
	}
}
