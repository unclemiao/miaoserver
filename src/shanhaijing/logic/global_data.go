package logic

import (
	"os"

	"github.com/kataras/golog"

	"goproject/engine/cache"
	"goproject/engine/sql"

	"goproject/src/shanhaijing/base"
	"goproject/src/shanhaijing/data"
)

var (
	gServer     *data.Server // 服务器信息
	gConfig     *data.Config // 策划配置信息
	gAppCnf     *data.AppCnf
	gCache      *cache.ShardedCache
	gEventMgr   *base.EventManager
	gSql        *sql.Sql
	logs        *golog.Logger
	gLogFile    *os.File
	gSessionMgr *SessionManager
	gIdMgr      *IdManager
)

var (
	gRoleMgr *RoleManager
	gItemMgr *ItemManager
)
