package config

import (
	"goproject/engine/assert"
	util "goproject/engine/excel"
	"goproject/src/shanhaijing/data"
)

var CItemMap map[int32]*data.CItem

func LoadCItem() bool {
	logs.Infof("======================= LoadCItem =======================")
	var citems []*data.CItem
	CItemMap = make(map[int32]*data.CItem)
	err := util.XlsConfigLoad("XL_ITEM", &citems, nil)
	assert.Assert(err == nil, err)

	for _, cfg := range citems {
		logs.Debugf("citem %+v", cfg)
		CItemMap[cfg.Cid] = cfg
	}
	return true
}

func CheckCItem() bool {
	logs.Infof("======================= CheckCItem =======================")
	return true
}

func CItemBy(cid int32) *data.CItem {
	cfg, find := CItemMap[cid]
	assert.Assert(find, "item cfg not found, cid:%d", cid)
	return cfg
}

func CItemByNotErr(cid int32) *data.CItem {
	return CItemMap[cid]
}
