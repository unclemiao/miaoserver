package config

import (
	"github.com/kataras/golog"
)

var logs *golog.Logger

func LoadConfig(logger *golog.Logger) bool {
	logs = logger
	LoadCItem()
	return true
}

func CheckConfig() bool {
	CheckCItem()
	return true
}
