package logic

import (
	"time"

	"goproject/src/shanhaijing/data"
)

func TimeAfter(
	t time.Duration,
	f func(),
) *time.Timer {
	if t == 0 {
		gServer.ReceiveChan <- newEventArgs(data.EVENT_MSGID_TIMER, &data.EventTimer{Func: f}, nil)
		return nil

	} else {
		return time.AfterFunc(t, func() {
			gServer.ReceiveChan <- newEventArgs(data.EVENT_MSGID_TIMER, &data.EventTimer{Func: f}, nil)
		})
	}
}

func TimerLoadConfig() {
	gEventMgr.When(
		data.EVENT_MSGID_TIMER,
		func(args *data.EventArgs) {
			if args.EventMsg != nil {
				f, _ := args.EventMsg.(*data.EventTimer)
				if f.Func != nil {
					f.Func()
				}
			}
		},
	)
}
