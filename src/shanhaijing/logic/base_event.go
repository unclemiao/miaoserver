package logic

import "goproject/src/shanhaijing/data"

func newEventArgs(msgId data.EventType, msg data.EventItf, session *data.Session, args ...int64) *data.EventArgs {
	eventArgs := &data.EventArgs{
		MsgId:    int32(msgId),
		Session:  session,
		EventMsg: msg,
	}
	if len(args) > 0 {
		eventArgs.BeginTime = args[0]
		eventArgs.Now = args[0]
	}
	return eventArgs
}
