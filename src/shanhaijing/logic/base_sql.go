package logic

import (
	"io/ioutil"
	"os"
	"path/filepath"

	_ "github.com/go-sql-driver/mysql"
	"goproject/engine/assert"
	"goproject/engine/sql"
	"goproject/src/shanhaijing/data"
)

// 初始化sql
func InitSql(cnf *data.AppCnf) {
	gSql = sql.LoadSql(&sql.Dsn{
		Driver:       cnf.Sql.Driver,
		Host:         cnf.Sql.Host,
		Port:         cnf.Sql.Port,
		Username:     cnf.Sql.Root,
		Password:     cnf.Sql.Pass,
		DatabaseName: cnf.Sql.Name,
	})
	gSql.SetRollBackEventFunc(func(id int64, args ...interface{}) {
		var session *data.Session
		if len(args) > 0 {
			session = args[0].(*data.Session)
		}
		gEventMgr.Call(newEventArgs(
			data.EVENT_MSGID_SQL_ROLLBACK,
			&data.EventSqlRollBack{RoleId: id},
			session,
		))
	})
	InitScript()
}

// 加载游戏
func InitScript() {
	var nodes []*data.ServerNode
	err := gSql.Select(&nodes, "select * from server_node")
	if err != nil {
		initSqlTable()
	} else {
		updateSqlTable()
	}
}

// 初始化数据库
func initSqlTable() {
	// 获取数据库脚本路径
	appPath, err := filepath.Abs(os.Args[0])
	assert.Assert(err == nil, err)
	appDir := filepath.Dir(appPath)
	cfgPath := appDir + "\\config\\sqlversion\\create"
	logs.Infof("initSqlTable[%s]", cfgPath)

	doSqlTableFromDir(cfgPath)

}

// 更新数据库
func updateSqlTable() {
	// 获取数据库脚本路径
	appPath, err := filepath.Abs(os.Args[0])
	assert.Assert(err == nil, err)
	appDir := filepath.Dir(appPath)
	cfgPath := appDir + "\\config\\sqlversion\\update"
	logs.Infof("updateSqlTable[%s]", cfgPath)

	doSqlTableFromDir(cfgPath)
}

func doSqlTableFromDir(dirPath string) {
	// 获取文件夹下所有文件
	fileList, err := ioutil.ReadDir(dirPath)
	assert.Assert(err == nil, err, dirPath)

	// 执行所有.sql文件脚本
	for _, file := range fileList {

		filePath := dirPath + "\\" + file.Name()
		sqlBytes, _ := ioutil.ReadFile(filePath)

		_, err = gSql.Excel(string(sqlBytes))
		assert.Assert(err == nil, err)

		logs.Infof("Excel [%s] finish", filePath)
	}
}
