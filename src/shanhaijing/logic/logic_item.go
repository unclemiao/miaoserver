package logic

import (
	"fmt"

	"goproject/engine/assert"
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/gConst"
	"goproject/src/shanhaijing/protocol"
)

type ItemManager struct {
}

func init() {
	gItemMgr = &ItemManager{}
}

func (mgr *ItemManager) Init() {

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(msg *data.EventArgs) {
		eventArgs := msg.EventMsg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getCacheKey(eventArgs.RoleId))
	})

}

func (mgr *ItemManager) getCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ALL_ITEM, roleId)
}

func (mgr *ItemManager) AllItemBy(roleId int64) *data.AllItem {

	cacheKey := mgr.getCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		allItem := &data.AllItem{
			RoleId:        roleId,
			ItemMap:       make(map[int64]*data.Item),
			ItemMapForKit: make(map[protocol.KitType]map[int64]*data.Item),
			OwnerItems:    make(map[int64]map[int64]*data.Item),
		}
		var itemList []*data.Item
		gSql.MustSelect(&itemList, gConst.SQL_ALL_ITEM_SELECT, roleId)
		mgr.initAllItem(allItem, itemList)
		return allItem, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	assert.Assert(err == nil, err)

	allItem := rv.(*data.AllItem)
	gCache.Touch(cacheKey, gConst.SERVER_DATA_CACHE_TIME_OUT)

	return allItem
}

func (mgr *ItemManager) initAllItem(allItem *data.AllItem, itemList []*data.Item) {
	assert.Assert(allItem != nil, "invalid allItem")
	for _, it := range itemList {
		allItem.ItemMap[it.Id] = it

		if allItem.ItemMapForKit[it.Kit] == nil {
			allItem.ItemMapForKit[it.Kit] = make(map[int64]*data.Item)
		}
		allItem.ItemMapForKit[it.Kit][it.Id] = it

		if allItem.OwnerItems[it.OwnerId] == nil {
			allItem.OwnerItems[it.OwnerId] = make(map[int64]*data.Item)
		}
		allItem.OwnerItems[it.OwnerId][it.Id] = it
	}
}

func (mgr *ItemManager) ItemBy(roleId int64, itemId int64) *data.Item {
	allItem := mgr.AllItemBy(roleId)
	it := allItem.ItemMap[itemId]
	assert.Assert(it != nil, "item not found, itemId: ", itemId)
	return it
}

func (mgr *ItemManager) ItemByOwner(roleId int64, ownerId int64, itemId int64, kit protocol.KitType) *data.Item {
	it := mgr.ItemBy(roleId, itemId)
	assert.Assert(it.OwnerId == ownerId && kit == it.Kit, "item not found, itemId: %d, ownerId: %d, kit: %s", itemId, ownerId, kit)
	return it
}

func (mgr *ItemManager) ItemNum(roleId int64, kit protocol.KitType) int32 {
	allItem := mgr.AllItemBy(roleId)
	return int32(len(allItem.ItemMapForKit[kit]))
}

func (mgr *ItemManager) ItemNumByOwner(roleId int64, ownerId int64) int32 {
	allItem := mgr.AllItemBy(roleId)
	return int32(len(allItem.OwnerItems[ownerId]))
}

func (mgr *ItemManager) BagNBy() int32 {
	return 100
}

// findGrids free只找空格子 onlyOne 只找一个格子 -> { [grid]=+n空格子 -n要叠放的格子 }
func (mgr *ItemManager) findGrids(roleId int64, cid int32, n int32, free bool, onlyOne bool) map[int32]int32 {

}
