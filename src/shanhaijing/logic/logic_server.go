package logic

import (
	"encoding/json"
	"runtime/debug"
	"time"

	"goproject/engine/assert"
	"goproject/engine/netWork"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/shanhaijing/base"
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/gConst"
	"goproject/src/shanhaijing/protocol"

	"github.com/gorilla/websocket"
)

func logFile() {
	InitLog()
}

// 事件开始前调用函数
func beforeCall(eventArgs *data.EventArgs) {
	eventArgs.BeginTime = time_helper.NowMill()
	eventArgs.Now = eventArgs.BeginTime
}

// 防攻击
func antiDos() {

}

// 消息结束后
func afterCall(eventArgs *data.EventArgs) {
	eventArgs.EndTime = time_helper.NowMill()
	subMs := eventArgs.EndTime - eventArgs.BeginTime
	if subMs > 30*1000 {
		logs.Warnf("event call over than %d ms", subMs)
	}
}

// 消息产生错误是回调
func errCall(eventArgs *data.EventArgs, err interface{}) {

	if err != nil {
		gSql.Rollback()
		if eventArgs.Session != nil {
			gSessionMgr.Clean(eventArgs.Session)
		}
		logs.Error(err)
		logs.Error(string(debug.Stack()))
	} else {
		gSql.Commit()
		if eventArgs.Session != nil {
			gSessionMgr.Flush(eventArgs.Session)
		}
	}
}

func beginNet() {
	// 网络初始化
	netServer := netWork.NewWebSocketServer("0.0.0.0", 30993)
	netServer.SetOnConnectFunc(onConnect)
	netServer.SetOnDataFunc(onData)
	netServer.SetOnCloseFunc(onClose)
	netServer.SetReadTimeOut(time.Second * 10)
	netServer.SetWriteTimeOut(time.Second)
	netServer.SetLog(logs)

	go func() {
		err := netServer.Run()
		if err != nil {
			logs.Error("netServer run err", err)
		}
	}()
}

func heartbeat() {
	gSql.Begin()
	nowMinute := time_helper.NowMin()
	for gServer.Node.LastMinute < nowMinute {
		gServer.Node.LastMinute++
		gSql.MustExcel("update server_node set last_minute = ? where sid = ?", gServer.Node.LastMinute, gServer.Node.Sid)
		minutely(gServer.Node.LastMinute * gConst.MINUTE)
	}
	TimeAfter(time.Duration(gConst.MINUTE-time.Now().Local().Second())*time.Second, heartbeat)
}

func initServer() {
	// 初始化
	gServer = &data.Server{
		IsStarted:    false,
		Stop:         nil,
		ReceiveChan:  make(chan *data.EventArgs, gConst.SERVER_RECEIVE_SIZE),
		StopChan:     make(chan struct{}),
		NodeRolesMap: make(map[int32]int32),
	}
	gEventMgr = new(base.EventManager)
	gEventMgr.Init()
	gConfig = new(data.Config)

	logFile()

	InitConfig()
	// 加载sql文件
	InitSql(gAppCnf)

	logs.Infof("server init finish")
}

func beginServer() {
	var (
		nodeList []*data.ServerNode
		master   *data.ServerNode
	)

	gSql.MustSelect(&nodeList, "select * from server_node where is_merge = ?", false)

	// 初始化node
	if len(nodeList) == 0 {
		// 第一次开服
		master = &data.ServerNode{
			Sid:        gAppCnf.Server.Id,
			IsMerge:    false,
			BeginTime:  time_helper.NowSec(),
			LastMinute: time_helper.NowMin(),
		}
		gSql.MustExcel("insert into server_node (sid, is_merge, begin_time, last_minute) values (?, ?, ?, ?)",
			master.Sid, master.IsMerge, master.BeginTime, master.LastMinute)
	} else if len(nodeList) == 1 {
		master = nodeList[0]
		// TODO 检测域名 host 是否改变
		// 检测服务器号是否不一致
		assert.Assert(master.Sid == gAppCnf.Server.Id, "invalid server_node: %d", master.Sid)

	} else {
		assert.Assert(false, "invalid server_node in database")
	}
	gServer.Node = master
	gServer.NodeList = nodeList
	for _, node := range nodeList {
		gServer.NodeIdList = append(gServer.NodeIdList, node.Sid)
		gServer.NodeRolesMap[node.Sid] = 0
	}

	// 更新 node
	assert.Assert(master != nil, "invalid master")
	master.BeginTime = time_helper.NowSec()

	master.LastMinute = util.MaxInt64(
		master.LastMinute,
		time_helper.Eod(time.Now().AddDate(0, 0, -1)).Unix()/gConst.MINUTE,
	)
	TimeAfter(1, heartbeat)
}

func run() {

	for {
		select {
		case bs := <-gServer.ReceiveChan:
			func() {
				defer func() {
					err := recover()
					errCall(bs, err)
				}()
				beforeCall(bs)
				gEventMgr.Call(bs)
				afterCall(bs)
			}()

		default:

		}

	}

}

func Stop() {
	err := gLogFile.Close()
	if err != nil {
		logs.Error(err)
	}
}

func onConnect(conn *websocket.Conn) {
	gSessionMgr.NewSession(conn)

}

func onData(conn *websocket.Conn, raw []byte) {
	session := gSessionMgr.SessionBy(conn)
	msg := &protocol.Envelope{}
	err := json.Unmarshal(raw, msg)
	if err != nil {
		logs.Error(err.Error())
		return
	}
	gServer.ReceiveChan <- &data.EventArgs{
		Session:  session,
		MsgId:    int32(msg.GetMsgType()),
		EventMsg: msg,
	}
}

func onClose(conn *websocket.Conn) {
	session := gSessionMgr.SessionBy(conn)
	if session != nil {
		roleId := session.RoleId
		session.RoleId, session.AccountId = 0, ""
		gSessionMgr.DelSession(conn)

		if roleId > 0 && gRoleMgr.IsOnline(roleId) {
			gServer.ReceiveChan <- &data.EventArgs{
				Session:  session,
				MsgId:    int32(data.EVENT_MSGID_OFFLINE_ROLE),
				EventMsg: &data.EventOfflineRole{RoleId: roleId, SignOut: false},
			}
		}
	}
}

func loadConfig() {
	LoadConfig()
}

func afterConfig() {
	AfterConfig()
}

func StarServer() {
	defer func() {
		err := recover()
		if err != nil {
			logs.Error(err)
			logs.Error(string(debug.Stack()))
		}
	}()

	// 初始化服务器
	initServer()

	loadConfig()

	// 开始运行服务器
	beginServer()

	afterConfig()

	// 网络初始化
	beginNet()

	go run()

	<-gServer.StopChan
}

func minutely(now int64) {
	logs.Info("========================== server minutely ==========================")
	gEventMgr.Call(newEventArgs(
		data.EVENT_MSGID_MINUTELY,
		&data.EventMinutely{NowSec: now},
		nil,
		now,
	))
	if now%gConst.HOUR == 0 {
		hourly(now, now/gConst.HOUR)
	}

}

func hourly(now int64, hour int64) {
	logs.Info("========================== server hourly ==========================")
	gEventMgr.Call(newEventArgs(
		data.EVENT_MSGID_HOURLY,
		&data.EventHourly{Hour: hour},
		nil,
		now,
	))
	if hour%gConst.DAY == 0 {
		daily(now, (int32(now/gConst.DAY)-3)%7)
	}
}

func daily(now int64, week int32) {
	logs.Info("========================== server daily ==========================")
	gEventMgr.Call(newEventArgs(
		data.EVENT_MSGID_DAILY,
		&data.EventDaily{Week: week, IsNewWeek: week == 1},
		nil,
		now,
	))
}
