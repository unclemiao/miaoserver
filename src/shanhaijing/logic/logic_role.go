package logic

import (
	"fmt"
	"time"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/engine/util"
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/gConst"
	"goproject/src/shanhaijing/protocol"
)

type RoleManager struct {
	roleOns  map[int64]*data.Session       // 已登录的角色集合
	roleOffs map[int64]int64               // 临时断线的集合
	changes  map[int64]data.RoleChangeType // 标记角色有改变的脏标记
}

func init() {
	gRoleMgr = &RoleManager{
		roleOns:  make(map[int64]*data.Session),
		roleOffs: make(map[int64]int64),
		changes:  make(map[int64]data.RoleChangeType),
	}
}

func (mgr *RoleManager) Init() {
	gEventMgr.When(data.EVENT_MSGID_AFTER_CONFIG, func(msg *data.EventArgs) {
		mgr.sessTimeout()
	})

	gEventMgr.When(data.EVENT_MSGID_SQL_ROLLBACK, func(msg *data.EventArgs) {
		eventArgs := msg.EventMsg.(*data.EventSqlRollBack)
		gCache.Delete(mgr.getRoleCacheKey(eventArgs.RoleId))
	})

	gEventMgr.When(protocol.EnvelopeType_MsgIdSigninPlayer, mgr.SignIn)

	gEventMgr.When(protocol.EnvelopeType_MsgIdNewRole, mgr.NewRole)

	gEventMgr.When(protocol.EnvelopeType_MsgIdSigninRole, mgr.SignInRole)

	gEventMgr.When(data.EVENT_MSGID_OFFLINE_ROLE, mgr.OnOfflineRole, 0)

}

func (mgr *RoleManager) getRoleCacheKey(roleId int64) string {
	return fmt.Sprintf(gConst.CACHE_KEY_ROLE, roleId)
}

func (mgr *RoleManager) RoleByRoleIdCanNull(roleId int64) (*data.Role, error) {
	var role *data.Role
	cacheKey := mgr.getRoleCacheKey(roleId)
	rv, err := gCache.GetOrStore(cacheKey, func() (interface{}, error) {
		var roleList []*data.Role
		gSql.MustSelect(&roleList, gConst.SQL_ROLE_SELECT, roleId)
		if len(roleList) > 0 {
			role = roleList[0]
		} else {
			return nil, fmt.Errorf("role[%d] not found", roleId)
		}
		return role, nil
	}, gConst.SERVER_DATA_CACHE_TIME_OUT)

	if err != nil {
		return nil, err
	}

	role = rv.(*data.Role)
	gCache.Touch(cacheKey, gConst.SERVER_DATA_CACHE_TIME_OUT)

	return role, nil
}

func (mgr *RoleManager) RoleBy(roleId int64) *data.Role {
	role, err := mgr.RoleByRoleIdCanNull(roleId)
	assert.Assert(err == nil, err)
	assert.Assert(role != nil, "role[%d] not found", roleId)
	return role
}

func (mgr *RoleManager) RoleByRoleIdAndAccountId(roleId int64, accountId string) *data.Role {
	role, err := mgr.RoleByRoleIdCanNull(roleId)
	assert.Assert(err == nil, err)
	assert.Assert(role != nil, "role[%d] not found", roleId)
	assert.Assert(role.AccountId == accountId, "role[%d] not found", roleId)
	return role
}

func (mgr *RoleManager) RoleByAccountId(accountId string, sid int32) *data.Role {
	var roleList []*data.Role
	gSql.MustSelect(&roleList, gConst.SQL_ROLE_SELECT_BY_ACCOUNT, accountId, sid)
	if len(roleList) > 0 {
		return roleList[0]
	} else {
		return nil
	}
}

func (mgr *RoleManager) SignIn(args *data.EventArgs) {
	envMsg := args.EventMsg.(*protocol.Envelope)
	reqMsg := envMsg.GetSigninPlayer()
	session := args.Session

	var (
		accountId     = reqMsg.GetAccountId()
		sid           = reqMsg.GetSnode()
		from          = reqMsg.GetFrom()
		young         = reqMsg.GetYoung()
		due           = reqMsg.GetDue()
		token         = reqMsg.GetToken()
		serverCfg     = gAppCnf.Server
		_, isThisNode = gServer.NodeRolesMap[sid]
	)

	// 检测各个参数
	assert.Assert(sid > 0 && sid < 10000, "invalid snode")
	assert.Assert(isThisNode, "!非本服玩家")
	assert.Assert(len(from) <= 64, "!渠道名太长")
	assert.Assert(len(accountId) > 0, "!帐号不能为空")

	// 检测 token
	if serverCfg.SignKey != "*" {
		assert.Assert(time_helper.NowSec() <= due, "token time out")
		md5 := util.Md5(fmt.Sprintf("%s%s%d%s", accountId, from, due, serverCfg.SignKey))
		assert.Assert(md5 == token, "invalid token")
	} else {
		young = serverCfg.Young
	}

	role := mgr.RoleByAccountId(accountId, sid)
	assert.Assert(role != nil, "role not found account:%s, sid:%d", accountId, sid)
	session.AccountId, session.From, session.Young, session.Sid = accountId, from, young, sid

	gSessionMgr.Send(session, protocol.MakeSignInDoneMsg(envMsg, role))
}

func (mgr *RoleManager) checkName(name string) {

}

func (mgr *RoleManager) NewRole(args *data.EventArgs) {
	envMsg := args.EventMsg.(*protocol.Envelope)
	reqMsg := envMsg.GetNewRole()
	session := args.Session

	var (
		account = reqMsg.GetAccountId()
		name    = reqMsg.GetName()
		cid     = reqMsg.GetCfgId()
		sid     = session.Sid
	)

	r := mgr.RoleByAccountId(account, sid)
	assert.Assert(r == nil, "role exist")
	mgr.checkName(name)
	name = fmt.Sprintf("[%03d]%s", session.Sid, name)
	r = &data.Role{
		RoleId:    gIdMgr.GenerateId(session.Sid, gConst.ID_TYPE_ROLE),
		Name:      name,
		Channel:   session.From,
		AccountId: account,
		Sid:       session.Sid,
		Tag:       0,
		Lv:        1,
		Cid:       cid,
	}

	gSql.Begin().MustExcel(gConst.SQL_ROLE_INSERT, r.RoleId, r.AccountId, r.Channel, r.Sid, r.Tag, r.Name, r.Cid, r.Lv, r.Exp)

	args.RoleId = r.RoleId

	gSessionMgr.Send(session, protocol.MakeNewRoleDoneMsg(envMsg, r))
}

func (mgr *RoleManager) SignInRole(args *data.EventArgs) {

	var (
		envMsg  = args.EventMsg.GetEnvMsg()
		reqMsg  = envMsg.GetSigninRole()
		roleId  = util.AtoInt64(reqMsg.GetRoleId())
		session = args.Session
		signIn  = true
	)

	role := mgr.RoleByRoleIdAndAccountId(roleId, args.AccountId)
	// todo 验证tag
	assert.Assert(role.Tag&gConst.ROLE_TAG_DEL == 0, "角色%s已被重置", role.Name)
	assert.Assert(role.Tag&gConst.ROLE_TAG_LOCK == 0, "角色%s已被锁定, 请联系客服", role.Name)

	if mgr.roleOns[roleId] != nil {
		assert.Assert(mgr.roleOns[roleId] != args.Session, "duplicated signIn")
		gEventMgr.Call(newEventArgs(data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: false}, session))
	}

	if mgr.roleOffs[roleId] > 0 {
		// 临时短暂断线
		delete(mgr.roleOffs, roleId)
		signIn = false
	}

	mgr.roleOns[roleId], session.RoleId = session, roleId
	gServer.NodeRolesMap[role.Sid] += 1
	gSessionMgr.Send(session, protocol.MakeSignInDoneMsg(envMsg, role))
	gEventMgr.Call(newEventArgs(data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOnlineRole{RoleId: roleId, SignIn: signIn}, session))
	args.Stop = true
}

func (mgr *RoleManager) OnOfflineRole(args *data.EventArgs) {
	var (
		envMsg  = args.EventMsg.(*data.EventOfflineRole)
		roleId  = envMsg.RoleId
		session = args.Session
	)

	if envMsg.SignOut {
		if mgr.roleOns[roleId] != nil {
			gEventMgr.Call(newEventArgs(data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: false}, session))
			delete(mgr.roleOffs, roleId)
		}

	} else {
		session := mgr.roleOns[roleId]
		assert.Assert(session != nil, "role not online")
		delete(mgr.roleOns, roleId)
		gServer.NodeRolesMap[session.Sid]--
		gSessionMgr.Stop(session)
		mgr.roleOffs[roleId] = time_helper.NowSec() + gConst.ROLE_OFFLINE_TIME_OUT
	}
}

func (mgr *RoleManager) sessTimeout() {
	TimeAfter(time.Duration(time.Now().Unix())+time.Second+time.Duration(15), func() {
		mgr.sessTimeout()
	})
	nowSec := time_helper.NowSec()
	for roleId, timeOut := range mgr.roleOffs {
		if nowSec > timeOut {
			gEventMgr.Call(newEventArgs(data.EVENT_MSGID_OFFLINE_ROLE, &data.EventOfflineRole{RoleId: roleId, SignOut: true}, nil))
		}
	}
}

func (mgr *RoleManager) IsOnline(roleId int64) bool {
	return true
}
