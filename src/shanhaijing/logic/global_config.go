package logic

import (
	"goproject/engine/assert"
	"goproject/engine/cache"
	util "goproject/engine/excel"
	"goproject/src/shanhaijing/base"
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/logic/config"

	"time"
)

func InitConfig() {
	// 加载服务器启动配置
	base.InitServerCnf("./config/")
	cnf := base.GetAppCfg()
	err := cnf.Unmarshal(&gAppCnf)
	assert.Assert(err == nil, err)
	// 加载 excel
	util.XlsConfigInit("./common/excel")

}

func LoadConfig() {
	gCache = cache.NewSharded(cache.NoExpiration, time.Duration(60)*time.Second, 2^4)
	// 加载游戏配置
	config.LoadConfig(logs)
	// 初始化模块
	TimerLoadConfig()
	AuthLoadConfig()
	gIdMgr.LoadConfig()
	gRoleMgr.Init()
	assert.Assert(checkConfig(), "配置检测错误")

}

func AfterConfig() {
	gEventMgr.Call(newEventArgs(data.EVENT_MSGID_AFTER_CONFIG, &data.EventBase{}, nil))
}

func checkConfig() bool {
	if !config.CheckConfig() {
		return false
	}
	return true
}
