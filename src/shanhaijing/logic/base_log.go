package logic

import (
	"os"
	"time"

	"github.com/kataras/golog"
	"goproject/engine/assert"
)

func InitLog() {
	if logs == nil {
		logs = golog.New()
	}
	wdPath, err := os.Getwd()
	logPath := wdPath + "/logs/"
	err = os.MkdirAll(logPath, 0755)
	assert.Assert(err == nil, err)

	gLogFile = newLogFile(logPath)
	logs.SetOutput(gLogFile)
	logs.AddOutput(os.Stderr)
	logs.SetLevel("debug")

	logs.Infof("InitLog finish")
}

// get a filename based on the date, file logs works that way the most times
// but these are just a sugar.
func todayFilename() string {
	today := time.Now().String()[:10]
	return today + ".log"
}

func newLogFile(path string) *os.File {
	filename := path + todayFilename()
	// open an output file, this will append to the today's file if server restarted.
	f, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0666)
	assert.Assert(err == nil, err)

	return f
}
