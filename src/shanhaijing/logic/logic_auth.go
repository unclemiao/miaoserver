package logic

import (
	"goproject/engine/assert"
	"goproject/src/shanhaijing/data"
	"goproject/src/shanhaijing/protocol"
)

func authPlayer(args *data.EventArgs) {
	session := args.Session
	assert.Assert(session.AccountId != "", "player offline")
	args.AccountId = session.AccountId
	args.Sid = session.Sid
}

func authRole(args *data.EventArgs) {
	session := args.Session
	assert.Assert(session.RoleId > 0, "role offline")
	args.AccountId = session.AccountId
	args.RoleId = session.RoleId
	args.Sid = session.Sid
}

func AuthLoadConfig() {
	gEventMgr.When(protocol.EnvelopeType_MsgIdNewRole, authPlayer)
	gEventMgr.When(protocol.EnvelopeType_MsgIdSigninRole, authPlayer)
}
