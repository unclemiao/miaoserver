package base

import (
	"goproject/src/shanhaijing/protocol"
	"sort"

	"goproject/engine/assert"
	"goproject/engine/time_helper"
	"goproject/src/shanhaijing/data"
)

const (
	EVENT_MSG_BUFF_SIZE = 65535
	NET_MSG_BUFF_SIZE   = 65535
)

type EventType int32

type EventManager struct {
	Nets   [][]*data.EventFunc
	Events [][]*data.EventFunc
}

func (mgr *EventManager) Init() {
	mgr.Events = make([][]*data.EventFunc, EVENT_MSG_BUFF_SIZE)
	mgr.Nets = make([][]*data.EventFunc, NET_MSG_BUFF_SIZE)
}

func (mgr *EventManager) When(eventType interface{}, f func(msg *data.EventArgs), orderList ...int32) {
	var order int32 = 1
	if len(orderList) > 0 {
		order = orderList[0]
	}
	switch t := eventType.(type) {
	case EventType:
		mgr.whenEvent(int32(t), order, f)
	case protocol.EnvelopeType:
		mgr.whenNet(int32(t), order, f)
	}
}

func (mgr *EventManager) whenNet(msgId int32, order int32, funList ...func(msg *data.EventArgs)) {
	for _, f := range funList {
		msg := &data.EventFunc{MsgId: msgId, Func: f}

		msg.Order = order

		assert.Assert(msgId > 0 && msgId < int32(len(mgr.Nets)), "msgId[%d] err", msgId)
		mgr.Nets[msgId] = append(mgr.Nets[msgId], msg)
	}

	sort.Slice(mgr.Nets[msgId], func(i, j int) bool {
		return mgr.Nets[msgId][i].Order < mgr.Nets[msgId][j].Order
	})
}

func (mgr *EventManager) whenEvent(msgId int32, order int32, funList ...func(msg *data.EventArgs)) {
	for _, f := range funList {
		msg := &data.EventFunc{MsgId: msgId, Func: f}
		msg.Order = order
		assert.Assert(msgId > 0 && msgId < int32(len(mgr.Events)), "msgId[%d] err", msgId)
		mgr.Events[msgId] = append(mgr.Events[msgId], msg)
	}

	sort.Slice(mgr.Events[msgId], func(i, j int) bool {
		return mgr.Events[msgId][i].Order < mgr.Events[msgId][j].Order
	})
}

func (mgr *EventManager) Call(args *data.EventArgs) {
	if args.Now == 0 {
		args.Now = time_helper.NowMill()
	}
	if args.EventMsg.IsNetEvent() {
		mgr.callNet(args)
	} else {
		mgr.callEvent(args)
	}
}

func (mgr *EventManager) callNet(args *data.EventArgs) {
	assert.Assert(args.MsgId > 0 && int32(args.MsgId) < int32(len(mgr.Nets)), "msgId[%d] err", args.MsgId)
	for _, event := range mgr.Nets[args.MsgId] {
		event.Func(args)
		if args.Stop {
			break
		}
	}
	args.EndTime = time_helper.NowMill()
}

func (mgr *EventManager) callEvent(args *data.EventArgs) {
	assert.Assert(args.MsgId > 0 && int32(args.MsgId) < int32(len(mgr.Events)), "msgId[%d] err", args.MsgId)
	for _, event := range mgr.Events[args.MsgId] {
		event.Func(args)
		if args.Stop {
			break
		}
	}
	args.EndTime = time_helper.NowMill()
}
