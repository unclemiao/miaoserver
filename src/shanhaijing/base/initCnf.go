package base

import (
	"goproject/engine/assert"

	"github.com/spf13/viper"
)

var (
	App *viper.Viper
)

func InitServerCnf(path string) {
	viper.SetConfigName("server")
	viper.AddConfigPath(path)
	err := viper.ReadInConfig()
	assert.Assert(err == nil, err)
	App = viper.Sub("app")
}

func GetAppCfg() *viper.Viper {
	return App
}
