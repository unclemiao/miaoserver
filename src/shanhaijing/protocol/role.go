package protocol

func MakeSignInDoneMsg(envMsg *Envelope, role RoleData) *Envelope {
	var roleClientList []*Role
	if role != nil {
		roleClientList = append(roleClientList, role.GetForClient())
	}
	return &Envelope{
		SeqId:   envMsg.SeqId,
		Payload: &Envelope_SigninPlayerDone{SigninPlayerDone: &SigninPlayerDone{RoleList: roleClientList}},
	}
}

func MakeNewRoleDoneMsg(envMsg *Envelope, role RoleData) *Envelope {
	return &Envelope{
		SeqId:   envMsg.SeqId,
		Payload: &Envelope_NewRoleDone{NewRoleDone: &NewRoleDone{Role: role.GetForClient()}},
	}
}
