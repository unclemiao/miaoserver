package protocol

type RoleData interface {
	GetForClient() *Role
}
