CREATE TABLE `role` (
  `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
  `account_id`  varchar(128) NOT NULL COMMENT '账号id',
  `channel`  varchar(128) NOT NULL COMMENT '渠道',
  `sid` int(11) UNSIGNED NOT NULL COMMENT '服务器号',
  `tag`  int(11) UNSIGNED NOT NULL COMMENT '标识',
  `name`  varchar(128) NOT NULL COMMENT '名字',
  `cid`  int(11) UNSIGNED NOT NULL COMMENT '配置id',
  `lv`  int(11) UNSIGNED NOT NULL COMMENT '等级',
  `exp`  int(11) UNSIGNED NOT NULL COMMENT '经验',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `INDEX_NAME` (`name`) USING BTREE,
  KEY `INDEX_ACCOUNTID_AND_SID` (`account_id`, `sid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

