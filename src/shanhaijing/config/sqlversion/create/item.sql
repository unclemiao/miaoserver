CREATE TABLE `item` (
    `id` bigint(20) UNSIGNED NOT NULL COMMENT 'id',
    `role_id` bigint(20) UNSIGNED NOT NULL COMMENT '玩家id',
    `cid` int(11) UNSIGNED NOT NULL COMMENT '配置id',
    `n`  int(11) UNSIGNED NOT NULL COMMENT '数量',
    `grid` int(11) UNSIGNED NOT NULL COMMENT '所在格子号',
    `kit`  int(11) UNSIGNED NOT NULL COMMENT '所在位置，0=垃圾箱，1=背包，2=装备栏',
    PRIMARY KEY (`id`),
    KEY `INDEX_ROLE_ID` (`role_id`) USING BTREE,
    KEY `INDEX_KIT` (`kit`) USING BTREE,
    KEY `INDEX_ROLE_ID_AND_KIT` (`role_id`, `kit`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

