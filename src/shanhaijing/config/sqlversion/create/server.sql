CREATE TABLE `server_node` (
  `sid` bigint(20) UNSIGNED NOT NULL,
  `is_merge` Tinyint(1) UNSIGNED NOT NULL,
  `begin_time` bigint(20) UNSIGNED NOT NULL,
  `last_minute` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`sid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;