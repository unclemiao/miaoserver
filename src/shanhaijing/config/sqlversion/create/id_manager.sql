CREATE TABLE `id_manager` (
  `id_type` int(11) UNSIGNED NOT NULL,
  `id_amount` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`id_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;