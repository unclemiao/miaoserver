package read

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"path/filepath"
	"strings"

	"github.com/tealeg/xlsx"
	"goproject/engine/assert"
	"goproject/engine/logs"
)

type ExcelInfo struct {
	// map[FileName] = path
	FileMap map[string]string
	// map[SheetName] = FileName
	SheetMap map[string]string
}

var sExcelInfo *ExcelInfo

func init() {
	sExcelInfo = &ExcelInfo{
		FileMap:  make(map[string]string),
		SheetMap: make(map[string]string),
	}
}

func GetReadExcelInfo() *ExcelInfo {
	return sExcelInfo
}

func Load(path string) map[string]string {
	// resultStr[filePath] = string
	resultStr := make(map[string]string)

	files, err := ioutil.ReadDir(path)
	assert.Assert(err == nil, err)

	for _, file := range files {
		fname := file.Name()
		if filepath.Ext(fname) == ".xlsx" && !strings.HasPrefix(fname, "~") {
			ffname := filepath.Join(path, fname)
			logs.Infof("Load File:%s", ffname)
			xlFile, err := xlsx.OpenFile(ffname)
			assert.Assert(err == nil, err)
			for _, sheet := range xlFile.Sheets {
				resultStr[sheet.Name] = loadFromSheet(sheet)
			}
		}
	}

	return resultStr
}

func loadFromSheet(sheet *xlsx.Sheet) string {
	// 头信息
	head := sheet.Rows[2]
	// { 列 = 字段名 }
	names := make(map[int]string)
	// { 字段名 = 字段类型 }
	kinds := make(map[string]string)
	// 输出内容
	outPutStr := "[\n"

	// 解析第3行的字段名
	for i := 0; i < len(head.Cells); i++ {
		cell := head.Cells[i]
		if cell.Type() != xlsx.CellTypeString {
			break
		}

		splits := strings.Split(strings.TrimSpace(cell.String()), "#")
		assert.Assert(len(splits) != 0, fmt.Errorf("load [%s] error: row [%d] col [%d] cannot be empty", sheet.Name, 3, i+1))

		name := strings.TrimSpace(splits[0])
		name = strings.ToLower(strings.Replace(name, "_", "", -1))
		if len(name) == 0 {
			break
		}

		names[i] = name
		if len(splits) > 1 {
			kind := strings.TrimSpace(splits[1])
			kind = strings.ToLower(kind)
			kind = strings.Trim(kind, " ")
			kinds[name] = kind
		}

	}

	for i := 3; i < sheet.MaxRow; i++ {
		row := sheet.Rows[i]
		if len(row.Cells) == 0 || len(row.Cells[0].String()) == 0 {
			break
		}
		outPut := make(map[string]interface{})

		for j := 0; j < len(row.Cells); j++ {
			cell := row.Cells[j]

			cellName := names[j]
			if cellName == "" {
				continue
			}

			cellKind := kinds[cellName]
			if cellKind == "comment" {
				// 注释不导入
				continue
			}

			valueInt64, err := cell.Int64()
			if err == nil {
				// 处理int64
				outPut[cellName] = valueInt64

			} else {
				// 处理string
				outPut[cellName] = cell.String()
			}
		}

		str, err := json.Marshal(outPut)
		assert.Assert(err == nil, err)

		outPutStr = outPutStr + string(str) + "," + "\n"

	}
	outPutStr = strings.TrimRight(outPutStr, ",\n")
	outPutStr = outPutStr + "\n]"
	return string(outPutStr)
}
