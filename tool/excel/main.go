package main

import (
	"fmt"
	"io/ioutil"
	"os"

	"goproject/engine/assert"
	"goproject/engine/logs"
	"goproject/tool/excel/read"
)

func main() {
	defer func() {
		err := recover()
		if err != nil {
			logs.TraceError("%+v", err)
		}
	}()

	// 输入输出目录
	var inPath, outPath string
	// 根据启动参数获取输入输出参数
	if len(os.Args) > 1 {
		inPath = os.Args[1]
	}
	if inPath == "" {
		inPath, _ = os.Getwd()
	}

	if len(os.Args) > 2 {
		outPath = os.Args[2]
	}
	if outPath == "" {
		outPath = outPath + "\\output\\"
	}

	logs.Infof("inPath: %s, outPath: %s", inPath, outPath)

	jsonMap := read.Load(inPath)

	for fileName, str := range jsonMap {
		err := ioutil.WriteFile(outPath+fileName+".json", []byte(str), os.ModeDir)
		assert.Assert(err == nil, err)
	}

	fmt.Printf("Press any key to exit...")
	b := make([]byte, 1)
	os.Stdin.Read(b)
}
